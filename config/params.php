<?php

return [
    'adminEmail' => 'admin@simplywishes.com',
	'supportEmail' => 'simplywishes2021@gmail.com',
    'hostInfo' => 'http://localhost',
	'user.passwordResetTokenExpire' => 3600,
    'apiuser_id'=>''
];
