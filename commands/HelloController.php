<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Cronjobs;
use app\models\Donation;
use app\models\MailContent;
use app\models\Message;
use app\models\User;
use app\models\Wish;
use Codeception\Lib\Connector\Yii1;
use yii\console\Controller;
use yii\helpers\Html;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
    public function actionAutomaticcompletedonation()
    {

        $donations = \app\console\models\Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->all();
       // echo "<pre>";print_r($donations);exit;
        foreach($donations as $singledonation=>$value)
        {
            $your_date = '';
            $day = '';
            $datediff = '';
            $now = time(); // or your date as well
            $your_date =  strtotime($value->process_granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));

            if ($day >= '1'){
                $value->granted_by = $value->process_granted_by;
                $value->granted_date = date('Y-m-d H:i:s');
                $value->process_granted_by = "";
                $value->process_status = 0;

                if($value->save(false))
                {
                    $this->sendEmailToAccepted($value->granted_by, $value);
                    $mailcontent = MailContent::find()->where(['m_id' => 34])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject1 = $mailcontent->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user2 = \app\console\models\User::findOne([
                        'status' => \app\console\models\User::STATUS_ACTIVE,
                        'id' => $value->created_by,
                    ]);
                    $user_email1 = $user2->email;
                    $message1 = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'completeDonation-html'],
                            ['user' => $user2, 'editmessage' => $editmessage, 'donation' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message1->send();
                    //return $this->redirect(['wish/view','id'=>$w_id]);
                }
            }
        }
        $cronjobs = new Cronjobs();
        $cronjobs->name = 'actionAutomaticcompletedonation';
        $cronjobs->created_at = date('Y-m-d H:i:s');
        $cronjobs->save(false);

    }
    public function actionAutomaticcompletewish()
    {

        $wishes = \app\console\models\Wish::find()->where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])->all();

        foreach($wishes as $singlewish=>$value)
        {
            $your_date = '';
            $day = '';
            $now = time(); // or your date as well
            $your_date =  strtotime($value->process_granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day >= '1'){
                $value->granted_by = $value->process_granted_by;
                $value->granted_date = date('Y-m-d H:i:s');
                $value->process_granted_by = "";
                //$wish->process_granted_date = "00-00-0000";
                $value->process_status = 0;

                if($value->save(false))
                {
                    $this->sendEmailToGranter($value->wished_by, $value);
                    $this->sendEmailToWisher($value->granted_by, $value);
                    //$this->sendEmailToGranter($value->granted_by, $value);
                    $happystories = \Yii::$app->urlManager->createAbsoluteUrl(['happy-stories/index']);
                    $url = Html::a(Html::encode('Happy Stories'), $happystories);
                    $details = "Congratulations! Your wish ".$value->wish_title." has been fulfilled and is now considered granted.
                            Consider sharing your story on our ".$url." .";
                    $msg = $details;


                        $message = new Message();
                        $message->sender_id =  $value->granted_by;
                        $message->recipient_id =  $value->wished_by;
                        $message->parent_id = 0;
                        $message->read_text = 0;
                        $message->text = $msg;
                        $message->created_at = date("Y-m-d H:i:s");
                        $message->save(false);

                }
            }
        }
        //exit;
        $cronjobs = new Cronjobs();
        $cronjobs->name = 'actionAutomaticcompletewish';
        $cronjobs->created_at = date('Y-m-d H:i:s');
        $cronjobs->save(false);
    }
    private function sendEmailToGranter($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>14])->one();

        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        /* @var $user User */
        $user = \app\console\models\User::findOne([
            'status' => \app\console\models\User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $message = \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    public function sendEmailToWisher($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>33])->one();

        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        /* @var $user User */
        $user = \app\console\models\User::findOne([
            'status' => \app\console\models\User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $message = \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccesstowisher-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    private function sendEmailToAccepted($id, $donation)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>24])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        /* @var $user User */
        $user = \app\console\models\User::findOne([
            'status' => \app\console\models\User::STATUS_ACTIVE,
            'id' => $id,
        ]);


        if (!$user) {
            return false;
        }


        $message = \Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccessDonation-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation ]
            )
            ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);
        // print_r($donation);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    public function actionTendayswishinprogress(){
        //where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])
        $wishes = \app\console\models\Wish::find()->where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])->all();
        $wishesdata = array();
        foreach($wishes as $singlewish=>$value) {
            $now = time(); // or your date as well
            $your_date = strtotime($value->granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day ==	 1) {


                //$mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();


                /* @var $user User */
                $user = \app\console\models\User::findOne([
                    'status' => \app\console\models\User::STATUS_ACTIVE,
                    'id' => $value->wished_by,
                ]);
                $user2 = \app\console\models\User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $value->process_granted_by,
                ]);

                if (!empty($user) && !empty($user2)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 36])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject = $mailcontent->mail_subject;
                    if (empty($subject))
                        $subject = 'SimplyWishes ';
                    $user_email = $user->email;
                    $message = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourwishaccepted-html'],
                            ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'wish' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email)
                        ->setSubject($subject);
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $send1 = $message->send();
                   // var_dump($send1);

                }
                if(!empty($user2) && !empty($user)){
                    $mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();
                    $editmessage1 = $mailcontent1->mail_message;
                    $subject1 = $mailcontent1->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user_email1 = $user2->email;
                    $message1 = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourwishaccepted-html'],
                            ['user' => $user2, 'user2' => $user, 'editmessage' => $editmessage1, 'wish' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                  $send =  $message1->send();
                    //var_dump($send);
                }
                //  return $message->send();
            }
        }
        $cronjobs = new Cronjobs();
        $cronjobs->name = 'actionTendayswishinprogress';
        $cronjobs->created_at = date('Y-m-d H:i:s');
        $cronjobs->save(false);
       // exit;
        //echo "<pre>";print_r($wishesdata);exit;
    }
    public function actionTendaysdonationinprogress(){
        //where(['status'=>0,'process_status'=>1,'granted_by'=> null])
        $donations = \app\console\models\Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->all();

        foreach($donations as $singlewish=>$value) {
            $now = time(); // or your date as well
            $your_date = strtotime($value->granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day ==	 1) {


                //$mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();


                /* @var $user User */
                $user = \app\console\models\User::findOne([
                    'status' => \app\console\models\User::STATUS_ACTIVE,
                    'id' => $value->created_by,
                ]);
                $user2 = \app\console\models\User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $value->process_granted_by,
                ]);

                if (!empty($user) && !empty($user2)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 38])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject = $mailcontent->mail_subject;
                    if (empty($subject))
                        $subject = 'SimplyWishes ';
                    $user_email = $user->email;
                    $message = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourdonationaccepted-html'],
                            ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'wish' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email)
                        ->setSubject($subject);
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message->send();

                }
                if (!empty($user) && !empty($user2)) {
                    $mailcontent1 = MailContent::find()->where(['m_id' => 39])->one();
                    $editmessage1 = $mailcontent1->mail_message;
                    $subject1 = $mailcontent1->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user_email1 = $user2->email;
                    $message1 = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourdonationaccepted-html'],
                            ['user' => $user2, 'user2' => $user, 'editmessage' => $editmessage1, 'wish' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message1->send();
                }
                //  return $message->send();
            }
        }
        $cronjobs = new Cronjobs();
        $cronjobs->name = 'actionTendaysdonationinprogress';
        $cronjobs->created_at = date('Y-m-d H:i:s');
        $cronjobs->save(false);
        //echo "<pre>";print_r($wishesdata);exit;
    }

    public function actionfourteendaysdonationGranting(){
        /*$donations = \app\console\models\Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null,'granted_date'=> null])->andWhere(['not', ['process_granted_by' => null]])->limit(1)->all();*/
        $donations = \app\console\models\Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->all();

        foreach($donations as $singlewish=>$value) {
            $now = time(); // or your date as well
            $your_date = strtotime($value->granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day ==	 1) {

                $donations->granted_by = $value->process_granted_by;
                $donations->granted_date = date('Y-m-d H:i:s');
                $donations->process_granted_by = "";
                //$donation->process_granted_date = date('Y-m-d');
                $donations->process_status = 0;

                //$mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();


                /* @var $user User */
                $user = \app\console\models\User::findOne([
                    'status' => \app\console\models\User::STATUS_ACTIVE,
                    'id' => $value->created_by,
                ]);
                $user2 = \app\console\models\User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $value->process_granted_by,
                ]);

                if (!empty($user) && !empty($user2)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 24])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject = $mailcontent->mail_subject;
                    if (empty($subject))
                        $subject = 'SimplyWishes ';
                    $user_email = $user->email;
                    $message = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'fullfilledSuccessDonation-html'],
                            ['user' => $user, 'editmessage' => $editmessage, 'donation' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email)
                        ->setSubject($subject);
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message->send();

                }
                if (!empty($user) && !empty($user2)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 34])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject1 = $mailcontent->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user_email1 = $user2->email;
                    $message1 = \Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'completeDonation-html'],
                            ['user' => $user, 'editmessage' => $editmessage, 'donation' => $value]
                        )
                        ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message1->send();
                }
                //  return $message->send();
            }
        }
        $cronjobs = new Cronjobs();
        $cronjobs->name = 'actionfourteendaysdonationGranting';
        $cronjobs->created_at = date('Y-m-d H:i:s');
        $cronjobs->save(false);


    }
}
