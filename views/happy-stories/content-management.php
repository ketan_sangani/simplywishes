<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

use app\models\HappyStoriesComments;
use app\models\WishComments;
use app\models\DonationComments;
use app\models\Donation;
use app\models\Wish;
use app\models\EditorialComments;


/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchEditorial */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Content Management';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="editorial-index">

    <h3 class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

			[
				'attribute' => 'fullname',
				'label' => 'Report user',
				'value' => 'reportUser.fullname',	
				'enableSorting' => true,					
			],	
			
			[
				'attribute' => 'reported_user',
				'label' => 'Reported user',
				'value' => 'reportedUser.fullname',
				'enableSorting' => true,					
			],	    				
			[
				'attribute' => 'comment',
				'label' => 'Comment',
				'value' => 'comment',
				'enableSorting' => false,		
			],	 
			[
                'attribute'=>'report_type',
				'value' => 'report_type',
                'filter'=>Html::activeDropDownList($searchModel, 'report_type', array("wish"=>"wish","user"=>"user","comment"=>"comment","message"=>"message","wish_comment"=>"wish_comment","article_comment"=>"article_comment","article"=>"article","donation"=>"donation"),['class'=>'form-control','prompt' => '--Select type--']),
				'enableSorting' => true,
            ],
			
			[
                'attribute'=>'blocked',
				'value' => function($model, $key, $index)
				{   
						if($model->blocked == '0')
						{
							return 'Not Blocked';
						}
						else
						{   
							return 'Blocked';
						}
				},
                'filter'=>Html::activeDropDownList($searchModel, 'blocked', array("0"=>"Not blocked","1"=>"Blocked"),['class'=>'form-control','prompt' => '--Select Status--']),
				'enableSorting' => false,
            ],
	
			[
  'class' => 'yii\grid\ActionColumn',
  'template' => '{view}{remove}{delete}',
  'buttons' => [
    'view' => function ($url, $model) {
		$url="";
		switch ($model->report_type){
			case 'wish':
				$url = Url::to(['wish/view','id'=>$model->content_id]);
				break;
			case 'donation':
				$url = Url::to(['donation/view','id'=>$model->content_id]);
				break;				
			case 'donation_comment':
				$donation = DonationComments::find()->where(['d_comment_id'=>$model->content_id])->one();
				$url = Url::to(['/donation/view','id'=>$donation->d_id]);
				break;				
			case 'wish_comment':
				$wish = WishComments::find()->where(['w_comment_id'=>$model->content_id])->one();
				if (isset($wish->w_id)){
                    $url = Url::to(['/wish/view','id'=>$wish->w_id]);
                }
				break;				
			case 'user':
				$url = Url::to(['account/profile','id'=>$model->reported_user]);
				break;
			// case 'message':
			// 	$url = Url::to(['account/inbox-user-message','id'=>$model->report_user]);
			// 	break;				
			case 'editorial':
				$url = Url::to(['/editorial/editorial-page','id'=>$model->content_id, 'class'=>'article']);
				break;	
			case 'editorial_comment':
				$editorial = EditorialComments::find()->where(['e_comment_id'=>$model->content_id])->one();
				$url = Url::to(['/editorial/editorial-page','id'=>$editorial->e_id]);
				break;					
			case 'happy_stories':
				$url = Url::to(['/happy-stories/story-details','id'=>$model->content_id]);
				break;	
			case 'happy_stories_comment':
				$hs = HappyStoriesComments::find()->where(['hs_comment_id'=>$model->content_id])->one();
				$url = Url::to(['/happy-stories/story-details','id'=>$hs->hs_id]);
				break;					
			default :
				$url=""	;
				break;
		}
		
        return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-eye-open"></span>', $url, [
			'class' => 'viewcheck',	
			'id'=>$model->content_id, 	
			'type'=>$model->report_type, 	
			'title' => Yii::t('app', 'View content'),
        ]);
    },
	  'remove' => function ($url, $model) {
		  return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-trash"></span>', '/content-management/remove?id='.$model->id, [
			  'class' => 'removecheck',
			  'title' => Yii::t('app', 'Delete content'),
		  ]);
	  },
	// 'block' => function ($url, $model) {
    //     return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-ban-circle"></span>', '/content-management/block?id='.$model->id, [
    //                 'title' => Yii::t('app', 'Block content'),
    //     ]);
    // },
	'delete' => function ($url, $model) {
         return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-minus-sign"></span>', '#', [
					'class' => 'deletecheck',
					'for'=>$model->id, 
                    'title' => Yii::t('app', 'Delete from reports (takes no action regarding content)'),					
        ]); 
		
    },
  ],
],


        ],
    ]); ?>
</div>

<script>

$(document).on('click', '.removecheck', function(){ 
	var checkmsg = confirm("Are you sure you want to delete this item?");	
	if(checkmsg == false)
	{
		return false;
	}
});

$(document).on('click', '.deletecheck', function(){ 
		var checkmsg = confirm("Are you sure you want to delete this item?");	
		if(checkmsg == false)
		{
			return false;
		}
		
		var id = $(this).attr('for');
		$.ajax({
			url : '<?=Url::to(['content-management/delete'])?>',
			type: 'POST',
			data: { id:id },
			success:function(data){
				console.log(data);
				// location.reload();
			}
		});
		
	}); 

	
</script>

