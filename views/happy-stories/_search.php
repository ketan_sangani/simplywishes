<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\SearchEditorial */
/* @var $form yii\widgets\ActiveForm */
?>


<div class=" <?php $detect = new Mobile_Detect;
echo (!$detect->isMobile() && !$detect->isTablet()) ?  'editorial-story' : "editorial-story-mobile" ; ?>">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'story_text')->textInput(['placeholder'=>'Search...'])->label(false); ?>
    <?php // echo $form->field($model, 'created_at') ?>

    <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span>', ['class' => ' btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
