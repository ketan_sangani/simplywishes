<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Tell Your Happy Story';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
//Yii::$app->controller->renderPartial('account/_profilenew');
?>
<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>
<div class=" col-md-8 happy-stories-create" id="scroll-evt">

    <h3 class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
<script>
    $(window).on('load', function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#scroll-evt').offset().top - 100
        }, 'slow');
    });
</script>
