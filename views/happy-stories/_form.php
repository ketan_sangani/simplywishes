<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Wish;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Editorial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="happy-stories-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data'], 'id' => 'happy_form']); ?>

        <?php
		$user_id = \Yii::$app->user->id;
		$wish = ArrayHelper::map(Wish::find()->where(['wished_by'=>$user_id])->andwhere(['!=','granted_by',''])->orderBy('wish_title')->all(), 'w_id', 'wish_title');
			echo $form->field($model, 'wish_id')->dropDownList(
            $wish,           // Flat array ('id'=>'label')
            ['prompt'=>'--Wishes List--']    // options
        )->label('Wishes <span class="valid-star-color" >*</span>');  ?>
    
    <?= $form->field($model, 'story_text')->widget(CKEditor::className(), [
        'preset' => 'basic',
        'clientOptions' => ['height' => 100]
    ])->label("Story Text <span class='valid-star-color' >*</span> "); ?>		 
   
	<?php if(!empty($model->story_image)) {  ?>
            <img src="<?= \Yii::$app->homeUrl.'web/'.$model->story_image;?>" width="150" />
	<?php } ?>	 
	 <?php 		 
            echo $form->field($model, 'story_image')->fileInput(['class'=>'form-control'])->label('Story Image');		
        ?>
      <span>Or, Choose One from the Default Images below</span>            
      <div class="gravatar thumbnail">
            <a class="profilelogo" for="images/happy-story/1.jpg" ><img class="selected" src="<?=Yii::$app->homeUrl?>web/images/happy-story/1.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/2.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/3.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/4.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/5.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/5.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/6.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/7.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/8.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/9.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/10.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/11.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/12.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/13.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/14.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/15.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/16.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/17.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/17.jpg"/></a>
            <a class="profilelogo" for="images/happy-story/18.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/happy-story/18.jpg"/></a>
      </div>
	  
	
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

	<?= $form->field($model, 'dulpicate_image')->hiddenInput(['value'=>($model->story_image)?$model->story_image:'images/happy-story/1.jpg'])->label(false) ?>	
	
    <?php ActiveForm::end(); ?>

</div>
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
 <script type="text/javascript"> 
$(function(){	
    /* $('#happystories-story_image').change( function(event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            $("#imagesorce").fadeIn("fast").attr('src',tmppath);
    }); */

    $('.profilelogo').click(function(){
             $('.profilelogo').find( "img" ).removeClass('selected'); 
              var val = $(this).attr('for');
              $(this).find( "img" ).addClass('selected'); 
              $("#happystories-dulpicate_image").val(val);
    });

    $( "#happy_form" ).on('beforeSubmit', function( event ) {
        $('#overlay').show();
    });

});  
  </script>