<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Happy Stories';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="editorial-create smp-mg-bottom">
        <?php $detect = new Mobile_Detect; ?>
        <?php if (!$detect->isMobile() && !$detect->isTablet()) { ?>
        <div class="row tell-ur-story">
            <div class="col-sm-6"><h3 class="fnt-skyblue">Happy Stories</h3></div>
            <div class="col-sm-6">
                <div class="pull-right newtest" style="margin-bottom : -30px;">
                    <a class='btn btn-success' href="<?=Yii::$app->homeUrl?>happy-stories/create">Tell Your Happy Story</a>
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <h4 class='smp_about_slide'>Stories written by our users' whose wishes have been granted.</h4>
        <?php }else{?>
        <div class="row tell-ur-story">
            <div class="col-sm-4"><h3 class="fnt-skyblue"  style="text-align: center;">Happy Stories</h3></div>
            <br>
            <div class="newtest" style="text-align: center !important;">
                <h4 class='smp_about_slide'>Stories written by our users' whose wishes have been granted.</h4>
            </div>
            <div class="col-sm-4" style="padding-bottom: 50px;">
                <div class="pull-left" style="height: 45px;">
                    <a class='btn btn-success' href="<?=Yii::$app->homeUrl?>happy-stories/create">Tell Your Happy Story</a>
                </div>
            </div>
            <div class="col-sm-4" style="margin-right: 50px;">
                <div class="pull-left">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php Pjax::begin() ?>
    <div id="gridheight">
	<?php
	
	if(!empty($dataProvider->models))
	{
            foreach($dataProvider->models as $story)
            {
                $class  =   '';	
                $wish_details = $story->wish;
                
                if($story->likesCount > 0)
                    $class    =   'likesView';
                else
                    $class     =    'hide';
                
		      $profile = UserProfile::find()->where(['user_id'=>$story->user_id])->one();
                if(!empty($wish_details)) {
                    ?>
                    <div class="happystory smp-mg-bottom">
                        <div class="media">
                            <div class="media-left sharefull-list">
                                <img alt="story" src="<?= Yii::$app->homeUrl ?>web/<?= $story->story_image; ?>"
                                     class="media-object" style="width: 200px;border: solid 2px #000;">
                                <p>
                                    <span data-id="<?= $story->hs_id ?>" class="likesBlk <?php echo $class ?>">
                                        <span id="likecmt_<?= $story->hs_id ?>"> <?= $story->likesCount ?>  </span>
                                        <?php echo $story->likesCount > 1 ? 'Loves' : 'Love'; ?>
                                    </span> &nbsp;
                                    <?php

                                    if (!$story->isLiked(\Yii::$app->user->id))
                                        echo '<span title="Love it" data-w_id="' . $story->hs_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                                    else
                                        echo '<span title="You loved it" data-w_id="' . $story->hs_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                                    ?>
                                    <!--<i class="fa fa-save txt-smp-orange"></i> &nbsp;
                                    <i class="fa fa-thumbs-o-up txt-smp-green"></i>-->

                                    <span title="Share it" data-placement="top" data-popover-content=""><img
                                            data-placement="top" class="listesinside"
                                            src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"/></span>
                                <div class="shareIcons hide"
                                     data_text="<?php echo (isset($wish_details->wish_title)) ? $wish_details->wish_title : ''; ?>"
                                     data_url="<?= Url::to(['happy-stories/story-details', 'id' => $story->hs_id], true) ?>"></div>
                                </p>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo (isset($wish_details->wish_title)) ? $wish_details->wish_title : ''; ?></h4>
                                <a href="<?= Url::to(["account/profile", "id" => $story->user_id]) ?>">By: <?= $story->author->fullname; ?></a>
                                <p> <?= substr($story->story_text, 0, 450) ?></p>
                                <a href="<?= Yii::$app->homeUrl ?>happy-stories/story-details?id=<?= $story->hs_id; ?>">
                                    <h5>Read More</h5></a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            } else {
                echo "<div style='text-align: center;margin: 30px;'>Sorry, no Happy Stories found!</div>";
            }?>
    </div>
    <div>
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination'=>$dataProvider->pagination,
        ]);?> 
    </div>
    <?php Pjax::end() ?>
</div>	
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    $(".shareIcons").each(function(){
		var elem = $(this);
		elem.jsSocials({
                    showLabel: false,
                    showCount: true,
                    shares: ["facebook",{
                        share: "pinterest",           // name of share
                        via: "simplywishes5244",
                        media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    },
                    {
                            share: "twitter",           // name of share
                            via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                            hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    }],
                    url : elem.attr("data_url"),
                    text: elem.attr("data_text")
		});
	});
        
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                
                $.ajax({
                    url : '<?=Url::to(['happy-stories/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
        
        $(document).on('click', '.like-wish', function(){ 
		var s_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['happy-stories/like'])?>',
			type: 'GET',
			data: {s_id:s_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
                                                $("#likecmt_"+s_id).parent('.likesBlk').removeClass('hide');
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
                                                if(likecmt == 0)
                                                    $("#likecmt_"+s_id).parent('.likesBlk').addClass('hide');
					}
				}
			}
		});
	});
        });
    var isVisible = false;
var clickedAway = false;
$(document).on("click", ".listesinside", function() {
    $(function(){
        $('.listesinside').popover({   
            html: true,
            content: function () {
                var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
                return clone;
            }
        }).click(function(e) {
            e.preventDefault();
            clickedAway = false;
            isVisible = true;
        });
    });
 
});

$(document).click(function (e) {
    if (isVisible & clickedAway) {
        $('.listesinside').popover('hide');
        isVisible = clickedAway = false;
    } else {
        clickedAway = true;
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});
	
</script>