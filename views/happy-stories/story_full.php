<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use app\controllers\Utils;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$profile = UserProfile::find()->where(['user_id'=>$model->user_id])->one();
$wish_details = $model->wish;

if(\Yii::$app->user->id == $model->user_id)	
	$this->title = 'My Happy Story';
else	
	$this->title = ucfirst($profile->firstname)."'s".' Happy Story ';

$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\Yii::$app->view->registerMetaTag([
            'name' => 'og:title',
            'property' => 'og:title',
            'content' =>$wish_details->wish_title
]);
\Yii::$app->view->registerMetaTag([
        'name' => 'og:description',
        'property' => 'og:description',
        'content' =>strip_tags($model->story_text)
]);
\Yii::$app->view->registerMetaTag([
        'name' => 'og:image',
        'property' => 'og:image',
        'content' =>Url::to(['web/'.$model->story_image],true)
]);

$class  =   '';	

if($model->likesCount > 0)
    $class    =   'likesView';
else
    $class      =   'hide';
?>

<div>
    <div class=" my-profile" style="overflow: hidden;">
	<div class="col-md-12 smp-mg-bottom">
            <?php if (Yii::$app->session->hasFlash('success_happystory')): ?>
            <div class="alert alert-success" style="margin-top: 20px;">
                Your Story has been Updated Successfully.	
            </div>
            <?php endif; ?>	
            <h3 class="smp-mg-bottom fnt-skyblue"><?=$this->title?></h3>
	
            <div class="col-md-3 happystory sharefull-list">
                <img src="<?=Yii::$app->homeUrl?>web/<?php echo $model->story_image; ?>"   class="img-responsive" alt="my-profile-Image"><br>
                <p><span data-id="<?= $model->hs_id?>" class="likesBlk <?php echo $class ?>"> <span id="likecmt_<?= $model->hs_id ?>">  <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span> &nbsp;
                 
                    <?php

                      if(!$model->isLiked(\Yii::$app->user->id))
                            echo  '<span title="Love it" data-w_id="'.$model->hs_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                      else
                            echo  '<span title="You loved it" data-w_id="'.$model->hs_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                    ?>
                    <?php
                    $isReported = Utils::checkIsReported($model->hs_id, 'happy_stories');
                    $btnDisabled = ($isReported) ? 'comment' : '';
                    $disabled = ($isReported) ? 'disabled' : '';
                    $user = \Yii::$app->user->id;
                    if (\Yii::$app->user->id!=$model->user_id) {
                    if ($disabled) {
                        echo "<button class='fa fa-flag txt-smp-green $btnDisabled report-btn report-hs sdfg' style='background-color: red;   float: None !important;border-radius: 50%;font-size:11px;' for='$model->hs_id' data='$user' ></button>";
                    }else{
                        echo "<button class='fa fa-flag txt-smp-green $btnDisabled report-btn report-hs btn-disable' title='Report' style='background-color: black;   float: None !important;border-radius: 50%;font-size:11px;' for='$model->hs_id' data='$user' ></button>";
                    }
                    }
                    ?>
                    <!--<i class="fa fa-save txt-smp-orange"></i> &nbsp;
                    <i class="fa fa-thumbs-o-up txt-smp-green"></i>--> 

                    <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" class="listesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>					
                    <div class="shareIcons hide" data_text="<?= $wish_details->wish_title; ?>" data_url="<?= Url::to(['happy-stories/story-details','id'=>$model->hs_id],true)?>" ></div>

                </p>
                    <!--<div class="shareIcons" data_text="Happy Story" data_url="<?= Url::to(['happy-stories/story-details','id'=>$model->hs_id],true)?>" ></div>-->
            </div>
            <div class="col-md-8">
                <h4 class="media-heading"><?= $wish_details->wish_title; ?></h4>
                <p><?php echo Html::a('Author: '.$profile->fullname, ['account/profile', 'id' => $model->user_id])?> <?php echo $model->story_text; ?> </p>
                <?php if(\Yii::$app->user->id == $model->user_id){ ?>
                     <?= Html::a('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Update', ['update', 'id' => $model->hs_id], ['class' => 'btn btn-warning ']) ?>
                     <?= Html::a('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Delete', ['delete', 'id' => $model->hs_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                        ]
                    ]) ?>

                      <!--<button class="btn btn-danger deletecheck" for="<?= $model->hs_id ?>" ><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Delete </button>-->		 
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
    <h3 class="left fnt-skyblue" >Comments:</h3>

    <?php $form = ActiveForm::begin(['action' =>['happy-stories/happy-stories-comments']]); ?>
        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 4])->label(false) ?>
        <?= $form->field($listcomments, 'hs_id')->hiddeninput(['value'=>$model->hs_id])->label(false) ?>
       <div class="form-group">
           <?= Html::submitButton('Post', ['class' =>'btn btn-primary']) ?>
       </div>
    <?php ActiveForm::end(); ?>

    <?php
    if(isset($comments) && !empty($comments)){ 
    foreach($comments as $user)
    {
        $profile = UserProfile::find()->where(['user_id'=>$user->user_id])->one();?>
        <div class="row">
            <div class="form-group dpic floatLeft">
                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $profile->profile_image.'?v='.strtotime('now'); ?>"/>				
            </div>
            <div class="form-group floatLeft col-md-10 paddingLeft">	
                <span id="<?php echo "comment_".$user->hs_comment_id ?>">
                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $profile->user_id ?>" ><?= $profile->firstname.' '.$profile->lastname ?></a>					
                    <?= $user->comments ?><?php if($user->user_id === \Yii::$app->user->id) { ?>
                    <i class="glyphicon glyphicon-pencil editComment" data-id="<?= $user->hs_comment_id ?>" aria-hidden="true"></i>
                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $user->hs_comment_id], [
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this Comment?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php } ?>
                </span>
                <div  style="display:none; margin-top:10px" id="<?php echo "editCommentBlk_".$user->hs_comment_id ?>">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['happy-stories/update-comment?id='.$user->hs_comment_id]]); ?>
                        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 6, 'value' => $user->comments])->label(false) ?>			
                        <?= $form->field($listcomments, 'hs_id')->hiddeninput(['value'=>$model->hs_id])->label(false) ?>
                       <div class="form-group">
                           <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                       </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <p class="commentOptions">
                    <?php
                        if(!$user->isLiked(\Yii::$app->user->id))
                              echo  '<span title="Love it" class="loveComment" for="'.$user->hs_comment_id.'">Love</span>';
                        else
                              echo  '<span title="Love it" class="loveComment" for="'.$user->hs_comment_id.'" style="color:#B23535;">Love</span>';
                        
                        if($user->likesCount > 0)
                            $classCmt   =   'cmtLikesView';
                        else
                            $classCmt   =   '';
                        
                        $isReported = Utils::checkIsReported($user->hs_comment_id, 'happy_stories_comment');
                        $btnDisabled = ($isReported) ? 'comment action-disabled' : '';
                        $disabled = ($isReported) ? 'disabled' : ''; 
                        if (\Yii::$app->user->id == $user->user_id) {
                            $btnDisabled = 'comment action-disabled';
                            $disabled = 'disabled';
                        } 
                    ?>
                    <span class="on-reply" for="<?= $user->hs_comment_id ?>">Reply</span>
                    <span class="on-report <?= $btnDisabled ?>" for="<?= $user->hs_comment_id ?>" <?= $disabled ?> comment="<?= $user->comments ?>"" data="<?= $user->user_id ?>">Report</span>

                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $user->hs_comment_id ?>'></span>
                    <span id="cmtNum_<?= $user->hs_comment_id ?>"> <?= $user->likesCount ?></span>
                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($user->created_at)) ?></span>
                </p>

                <div  style="display:none; margin-top:10px" id="<?php echo "replylist_".$user->hs_comment_id ?>" class="comment-form2 reply full" data-plugin="comment-reply">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['happy-stories/commentreply']]); ?>				 
                     <?= $form->field($listcomments, 'comments')->textarea(['rows' => 3])->label(false) ?>			
                     <?= $form->field($listcomments, 'hs_id')->hiddeninput(['value'=>$model->hs_id])->label(false) ?>			
                     <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$user->hs_comment_id])->label(false) ?>
                     <div class="form-group">
                            <?= Html::submitButton('Reply-Post', ['class' =>'btn btn-primary']) ?>
                    </div>					
                    <?php ActiveForm::end(); ?>
                </div>
                <?php 
                $replycomments = \app\models\HappyStoriesComments::find()->where(['parent_id'=>$user->hs_comment_id, 'status'=>0])->orderBy('hs_comment_id Desc')->all();
                if($replycomments)
                {
                    foreach($replycomments as $replyuser)
                    {
                        $replyprofile = UserProfile::find()->where(['user_id'=>$replyuser->user_id])->one();	
                        ?>
                        <div class="row">		
                            <div class="form-group dpic floatLeft">
                                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $replyprofile->profile_image.'?v='.strtotime('now'); ?>"/>				
                            </div>
                            <div class="form-group floatLeft col-md-10 paddingLeft">	
                                <span id="<?php echo "replyComment_".$replyuser->hs_comment_id ?>">
                                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $replyprofile->user_id ?>" ><?= $replyprofile->firstname.' '.$replyprofile->lastname; ?></a>
                                    <?= $replyuser->comments ?><?php if($replyuser->user_id === \Yii::$app->user->id) { ?>
                                    <i class="glyphicon glyphicon-pencil editReplyComment" data-id="<?= $replyuser->hs_comment_id ?>" aria-hidden="true"></i>
                                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteReplyComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $replyuser->hs_comment_id], [
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this Comment?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                    <?php } ?>
                                </span>
                                <p class="commentOptions">
                                    <?php
                                        if(!$replyuser->isLiked(\Yii::$app->user->id))
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->hs_comment_id.'">Love</span>';
                                        else
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->hs_comment_id.'" style="color:#B23535;">Love</span>';
                                    
                                         if($replyuser->likesCount > 0)
                                            $classCmt   =   'cmtLikesView';
                                        else
                                            $classCmt   =   '';
                                    ?>
                                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $replyuser->hs_comment_id ?>'></span>
                                    <span id="cmtNum_<?= $replyuser->hs_comment_id ?>"> <?= $replyuser->likesCount ?></span>
                                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($replyuser->created_at)) ?></span>
                                </p>
                            </div>
                            <div  style="display:none; margin-top:10px" id="<?php echo "editReplyBlk_".$replyuser->hs_comment_id ?>">	
                                <!--<a class="close" data-action="comment-close">X</a> -->
                                <?php $form = ActiveForm::begin(['action' =>['happy-stories/update-comment?id='.$replyuser->hs_comment_id]]); ?>				 
                                 <?= $form->field($listcomments, 'comments')->textarea(['rows' => 3, 'value'=> $replyuser->comments ])->label(false) ?>			
                                 <?= $form->field($listcomments, 'hs_id')->hiddeninput(['value'=>$replyuser->hs_id])->label(false) ?>			
                                 <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$replyuser->parent_id])->label(false) ?>
                                 <div class="form-group">
                                        <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                                </div>					
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>	
                        <?php
                    }
                }?>
            </div>
        </div>
    <?php } } ?>
    </div>
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" >

$(document).ready(function(){
    $(".on-reply").click(function(){ 
		var id = $(this).attr("for");
		
		if($("#replylist_"+id).is(':hidden'))
		{
			$("#replylist_"+id).show();			
		} else {
			$("#replylist_"+id).hide();	
		}  	
		
    });


    $(".on-report").click(function(){ 
		var id = $(this).attr("for");
        var userId = $(this).attr("data");
        var reportButton = $(this);
        var comment = $(this).attr("comment");
        console.log(comment);
        if (reportButton.hasClass("action-disabled")){
            console.log("false");
            return false;
        }
        $.ajax({
            url : '<?=Url::to(['wish/report-content-post'])?>',
            type: 'POST',
            data: {
                comment_id:id,
                comment: comment,
                reported_user: userId,
                report_type: 'happy_stories_comment',
            },
            success:function(data){
                console.log(data);
                if(data){
                    console.log("saved");
                    reportButton.addClass("action-disabled");
                }	
            }
        });
    });


    $(".report-hs").click(function(){ 
		var id = $(this).attr("for");
        var userId = $(this).attr("data");
        var reportButton = $(this);
        console.log(id);
        console.log(userId);
        if (reportButton.hasClass("sdfg")){
            console.log("false");
            return false;
        }
        $.ajax({
            url : '<?=Url::to(['happy-stories/report-story'])?>',
            type: 'POST',
            data: {
                hs_id:id,
                reported_user:userId,
            },
            success:function(data){
                console.log(data);
                if(data){
                    console.log("saved");
                    reportButton.removeClass("btn-warning");
                    reportButton.addClass("btn-danger");
                    //reportButton.addClass("action-disabled");
                }	
            }
        });
    });
        
    $('.editComment').on('click', function(){
        var id = $(this).data("id");
		
        $('#comment_'+id).hide();
        if($("#editCommentBlk_"+id).is(':hidden'))
        {
            $("#editCommentBlk_"+id).show();			
        } else {
            $("#editCommentBlk_"+id).hide();	
        }  	
    });
    
    $('.editReplyComment').on('click', function(){
        var id = $(this).data("id");
		
        $('#replyComment_'+id).hide();
        if($("#editReplyBlk_"+id).is(':hidden'))
        {
            $("#editReplyBlk_"+id).show();			
        } else {
            $("#editReplyBlk_"+id).hide();	
        }  
    });
    $('#back').on('click', function(){
        window.location.href =   document.referrer;
    });
    $(".shareIcons").each(function(){
            var elem = $(this);
            elem.jsSocials({
                showLabel: false,
                showCount: true,
                shares: ["facebook",{
                    share: "pinterest",           // name of share
                    via: "simplywishes5244",
                    media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                    hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                },
                {
                        share: "twitter",           // name of share
                        via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                }],
                url : elem.attr("data_url"),
                text: elem.attr("data_text")
            });
});
});
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                
                $.ajax({
                    url : '<?=Url::to(['happy-stories/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
	
	$(document).on('click', '.like-wish', function(){ 
		var s_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['happy-stories/like'])?>',
			type: 'GET',
			data: {s_id:s_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
                                                $("#likecmt_"+s_id).parent('.likesBlk').removeClass('hide');
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
                                                if(likecmt == 0)
                                                    $("#likecmt_"+s_id).parent('.likesBlk').addClass('hide');
					}
				}
			}
		});
	});
	
	$(document).on('click', '.deletecheck', function(){ 
		var checkmsg = confirm("Are Sure To Delete this Happy Story ?");	
		if(checkmsg == false)
		{
			return false;
		}
		
		var id = $(this).attr('for');
		$.ajax({
			url : '<?=Url::to(['happy-stories/delete'])?>',
			type: 'POST',
			data: { id:id },
			success:function(data){
				window.location.href = "<?= Url::to('my-story')?>"
			}
		});
		
	}); 
        
$(document).on('click', '.cmtLikesView', function(){
    var val =   $(this).next('span').html();
    if(val <= 0)
        return false;

    var wish_id = $(this).data('id');
    if($.trim(wish_id) !== "" )
    {
        if(Wish.ajax)
            return false;

        Wish.ajax   =   true;
        $.ajax({
            url : '<?=Url::to(['happy-stories/comment-likes-view'])?>',
            type: 'GET',
            data: {w_id:wish_id},
            success:function(data){
                if(data)
                {
                    var tpl =   '';
                    var items   =   JSON.parse(data);
                    $.each(items, function(k,v){
                       tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                    });

                    $('.modal-body').append(tpl);
                    $('#likeModal').modal({backdrop: 'static', keyboard: false});
                    $('#likeModal').modal('show');

                    $('.closeModal').on('click', function(e){
                        e.preventDefault();
                        $('.modal-body').html('');
                        $('#likeModal').modal('hide');
                    });
                }
            },
            complete: function(){
                Wish.ajax   =   false;
            }
        });
    }
});


$(".btn-disable").click(function(){
    $(".btn-disable").css('background-color', '#FF0000');
});


$(document).on('click', '.loveComment', function(){
    var s_id = $(this).attr("for");
    var elem = $(this);
    $.ajax({
        url : '<?=Url::to(['happy-stories/like-comment'])?>',
        type: 'GET',
        data: {s_id:s_id},
        success:function(data){
            if(data == "added"){
                elem.css('color', '#B23535');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) + parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
            if(data == "removed"){
                elem.css('color', '#66cc66');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) - parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
        }
    });
});


var isVisible = false;
var clickedAway = false;

$(document).ready(function(){	
	$(function(){
		$('.listesinside').popover({   
			html: true,
			content: function () {
				var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
				return clone;
			}
		}).click(function(e) {
			e.preventDefault();
			clickedAway = false;
			isVisible = true;
		});
	});
	
/* 	$(document).on('click','.jssocials-shares',function(){
		 $('.listesinside').popover('hide');
	}); */
	
$(document).click(function (e) {
    if (isVisible & clickedAway) {
        $('.listesinside').popover('hide');
        isVisible = clickedAway = false;
    } else {
        clickedAway = true;
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});

});
</script>