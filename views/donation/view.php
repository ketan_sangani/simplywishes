<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use app\models\DonationComments;
use app\controllers\Utils;
/* @var $this yii\web\View */
/* @var $model app\models\Wish */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\Yii::$app->view->registerMetaTag([
    'name' => 'og:title',
    'property' => 'og:title',
    'content' =>$model->title
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:description',
    'property' => 'og:description',
    'content' => strip_tags($model->description)
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:image',
    'property' => 'og:image',
    'content' =>Url::to(['web/'.$model->image],true)
]);

$class  =   '';
if($model->likesCount > 0)
    $class    =   'likesView';
else
    $class  =   'hide';
?>
<div class="wish-view">
    <div class="row my-profile">
	<div class="col-md-12 smp-mg-bottom">
        <?php if (Yii::$app->session->hasFlash('messageSent')): ?>
            <div class="alert alert-success">
                Your message has been sent successfully.
            </div>
        <?php endif; ?>
            <h3 class="fnt-skyblue "><?=$this->title?></h3>
            <p id="success_txt" class="text-success hide"></p>
            <div class="col-md-3 happystory sharefull-list">
                <img src="<?=\Yii::$app->homeUrl.'web/'.$model->image.'?v='.strtotime('now')?>"  class="img-responsive" alt="my-profile-Image"><br>
                <p>
                    <span data-id="<?= $model->id ?>" class="likesBlk <?php echo $class; ?>"> <span id="likecmt_<?= $model->id ?>"> <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span> &nbsp;
                    <!--<a class="report-img" title="Report" data-id="<?= $model->id ?>"><img  src="<?= Yii::$app->homeUrl ?>images/report.png" alt=""></a>-->
                    <?php
                      if(!$model->isFaved(\Yii::$app->user->id))
                            echo '<span title="Save this donation" data-id="'.$model->id.'" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span>&nbsp;';
                      else
                            echo  '<span title="You saved it" data-id="'.$model->id.'" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span>&nbsp;';

                      if(!$model->isLiked(\Yii::$app->user->id))
                            echo  '<span title="Love it" data-id="'.$model->id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                      else
                            echo  '<span title="You loved it" data-id="'.$model->id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

                    if (\Yii::$app->user->isGuest){
                        echo ' <button class="fa fa-flag txt-smp-green" onclick="test()" style="background-color: black; float: None !important;border-radius: 50%;font-size:11px;"></button>';
                    }else{
                      if (\Yii::$app->user->id!=$model->created_by) {
                          $isReported = Utils::checkIsReported($model->id, 'donation');

                          $btnDisabled = ($isReported) ? 'btn-disabled' : '';
                          $disabled = ($isReported) ? 'disabled=disabled' : '';
                          if ($disabled) {
                              echo ' <button class="fa fa-flag txt-smp-green report-wish' . $btnDisabled . '" ' . $disabled . ' style="background-color: red; float: None !important;border-radius: 50%;font-size:11px;"></button>';
                          } else {
                              echo ' <button class="fa fa-flag txt-smp-green report-wish' . $btnDisabled . '" ' . $disabled . ' style="background-color: black; float: None !important;border-radius: 50%;font-size:11px;" title="Report"></button>';
                          }
                      }

                    }
                    ?>
                    <!--<i class="fa fa-save txt-smp-orange"></i> &nbsp;
                    <i class="fa fa-thumbs-o-up txt-smp-green"></i>-->
                    <?php if (\Yii::$app->user->isGuest){ ?>
                        <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" onclick='test()' class="listesinside removelistesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>
                        <div class="shareIcons hide"></div>
                    <?php }else{ ?>
                        <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" class="listesinside removelistesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>
                        <div class="shareIcons hide" data_image="<?php echo Url::base(true).'/web/'.$model->image;?>" data_text="<?= $model->title ?>" data_url="<?= Url::to(['donation/view','id'=>$model->id],true)?>" ></div>
                    <?php }?>
                </p>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
                <div class="row"><p class="col-md-4"><b>Name: </b></p><p class="col-md-8"><a href="<?=Url::to(['account/profile','id'=>$model->created_by])?>"><?=$model->donorName?></a></p></div>
                <div class="row"><p class="col-md-4"><b>Donation Description: </b></p><div class="col-md-8"><?=$model->description?></div></div>
                <div class="row"><p class="col-md-4"><b>Donation Type: </b></p><p>
                        <?php if($model->non_pay_option == 0 ){ ?>
                            <span class="col-md-8">Financial </span>
                        <?php } else if($model->non_pay_option == 1 ){ ?>
                            <span class="col-md-8">Non-Financial</span>
                        <?php } ?>
                    </p></div>
                <?php if($model->non_pay_option == 0 ){ ?>
                    <div class="row"><p class="col-md-4"><b>Donation Expected Cost: </b></p><p><span class="col-md-8">$<?=$model->expected_cost ?></span></p></div>
                    <div class="row"><p class="col-md-4"><b> Financial assistance: </b></p><p><span class="col-md-8"><?=$model->financial_assistance;
                                echo ($model->financial_assistance=='Other')?$model->financial_assistance_other:'';?></span></p></div>
                <?php } ?>
                <?php if($model->non_pay_option == 1 ){ ?>
                <div class="row"><p class="col-md-4"><b>Delivery Type: </b></p><div class="col-md-8"><?=$model->way_of_donation;?> (<?=$model->description_of_way;?>)</div></div>
                <?php } ?>

                <?php if(is_null($model->granted_by) && !\Yii::$app->user->isGuest  && \Yii::$app->user->id!=$model->created_by){ ?>
                    <?php if(!empty($model->process_granted_by)){ ?>
                          <button class="btn btn-success inProgress">In Progress</button>					
                        <?php } else { ?>
            <div class="col-md-12" style="padding: 0px !important;" >
                
                <a href="#messagemodalAccept" data-toggle="modal" ><button class="btn btn-success" style="padding: 5px 5px !important;" >Accept This Donation </button></a>
            
                <?php } ?>
                <a href="<?=Url::to(['account/inbox-message'])?>"><button class="btn btn-warning" style="margin-left: 15px;padding: 5px 5px !important;">Send a Message </button></a>
               <?php } ?>

                <?php if(!is_null($model->granted_by)){ ?>	
                    <div class="row"><p class="col-md-4"><b>Donation accepted on: </b></p><p><span class="col-md-8"><?php echo $model->process_granted_date; ?></span></p></div>
                    <div class="row"><p class="col-md-4"><b>Donation accepted by: </b></p><p><span class="col-md-8"><a href="<?=Url::to(['account/profile','id'=>$model->granted_by])?>"><span><?=$model->grantedDonorName?></span></a></span></p></div>		
                <?php } ?>			
              
                <?php if((is_null($model->granted_by)) && (!\Yii::$app->user->isGuest) && (\Yii::$app->user->id==$model->created_by) && (!empty($model->process_status)))
                {
                    // echo '<button class="btn btn-info" id="grant_progress_wishes" >Fulfilled </button>';
                    // echo '<button class="btn btn-danger"  style="margin-left:15px" id="Resubmit_wishes"> Re-submit my donation </button>'; 
                    echo '<button  class="btn btn-info" id="complete_progress_donation" style="padding: 5px 5px !important;">Complete Donation </button>';
                   // echo '&nbsp;&nbsp;<a href="'.Url::to(['donation/update','id'=>$model->id]).'"><button class="btn btn-success">Update Donation</button></a>';
                    // echo '<a href="'.Url::to(['donation/complete','id'=>$model->id]).'"><button class="btn btn-info">Complete donation</button></a>';
//                   echo "&nbsp;&nbsp;&nbsp;"; echo Html::a('Delete', ['delete-donation', 'id' => $model->id], [
//                    'class' => 'btn btn-danger',
//                    'data' => [
//                        'confirm' => 'Are you sure you want to delete this item?',
//                        'method' => 'post',
//                    ],
//                ]);
                }  
                else if( (!\Yii::$app->user->isGuest) && (\Yii::$app->user->id==$model->created_by)) {

                    if (is_null($model->process_granted_by) && is_null($model->granted_by)) {
                        echo '<a href="'.Url::to(['donation/update','id'=>$model->id]).'"><button class="btn btn-info">Update Donation</button></a>';
                    } else {
                        //echo '<a  href="'.Url::to(['donation/update','id'=>$model->id]).'"><button class="btn btn-info">Complete Donation</button></a>';
                    }
                    ?>
                    <?= Html::a('Delete', ['delete-donation', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                        ],
                    ]) ?>
                <?php } ?>
                <!-- <a class="btn btn-success" id="back">Back</a>    -->

                </div>
                </div> 
                </div>
            </div>
    
    
<div class="row">
    <div class="col-md-6">

    <h3 class="left fnt-skyblue" >Comments:</h3>

    <?php if (\Yii::$app->user->isGuest){ $form = ActiveForm::begin(['action' =>['site/login']]);}else {$form = ActiveForm::begin(['action' =>['donation/donation-comments']]);} ?>
        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 4])->label(false) ?>
        <?= $form->field($listcomments, 'd_id')->hiddeninput(['value'=>$model->id])->label(false) ?>
       <div class="form-group">
           <?= Html::submitButton('Post', ['class' =>'btn btn-primary']) ?>
       </div>
    <?php ActiveForm::end(); ?>
        </div>
</div>
<?php
$wishUserId = 0;

if(isset($comments) && !empty($comments)){ 
    foreach($comments as $user)
    {
        $profile = UserProfile::find()->where(['user_id'=>$user->user_id])->one();
        ?>
        <div class="row" id="parent_comment_<?= $user->d_comment_id ?>">		
            <div class="dpic floatLeft">
                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $profile->profile_image.'?v='.strtotime('now'); ?>"/>				
            </div>
            <div class="floatLeft col-md-10 paddingLeft">	
                <span id="<?php echo "comment_".$user->d_comment_id ?>">
                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $profile->user_id ?>" ><?= $profile->firstname.' '.$profile->lastname ?></a>					
                
                    <?= $user->comments ?><?php if($user->user_id === \Yii::$app->user->id) { ?>
                    <i class="glyphicon glyphicon-pencil editComment" data-id="<?= $user->d_comment_id ?>" aria-hidden="true"></i>
                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $user->d_comment_id], [
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this comment?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php } ?>
                </span>
                <div  style="display:none; margin-top:10px" id="<?php echo "editCommentBlk_".$user->d_comment_id ?>">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['donation/update-comment?id='.$user->d_comment_id]]); ?>
                        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2, 'value' => $user->comments])->label(false) ?>			
                        <?= $form->field($listcomments, 'id')->hiddeninput(['value'=>$model->id])->label(false) ?>
                       <div class="form-group">
                           <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                       </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <p class="commentOptions">
                    <?php
                        if(!$user->isLiked(\Yii::$app->user->id))
                              echo  '<span title="Love it" class="loveComment" for="'.$user->d_comment_id.'">Love</span>';
                        else
                              echo  '<span title="Love it" class="loveComment" for="'.$user->d_comment_id.'" style="color:#B23535;">Love</span>';
                        
                        if($user->likesCount > 0)
                            $classCmt   =   'cmtLikesView';
                        else
                            $classCmt   =   '';
                         
                        $disabled = "";

                        $isReported = Utils::checkIsReported($user->d_comment_id, 'donation_comment');
                        // $btnDisabled = ($isReported) ? 'btn-disabled' : '';
                        $disabled = ($isReported) ? ' disabled' : '';
                        if (\Yii::$app->user->id == $user->user_id ){
                            $disabled = " disabled";
                        } 
                    ?>
                    <span class="on-reply" for="<?= $user->d_comment_id ?>">Reply</span>
                    <!-- todoreportcomment -->
                    <span class="report-comment<?=$disabled?>" <?=$disabled?> comment="<?=$user->comments ?>" by="<?= $user->user_id ?>" for="<?= $user->d_comment_id ?>">Report</span>
                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $user->d_comment_id ?>'></span>
                    <span id="cmtNum_<?= $user->d_comment_id ?>"> <?= $user->likesCount ?></span>
                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($user->created_at)) ?></span>
                </p>
                
                <div  style="display:none; margin-top:10px" id="<?php echo "replylist_".$user->d_comment_id ?>" class="comment-form2 reply full" data-plugin="comment-reply">	
                    <!--<a class="close" data-action="comment-close">X</a> --> 
                    <?php $form = ActiveForm::begin(['action' =>['donation/commentreply']]); ?>				 
                     <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2])->label(false) ?>			
                     <?= $form->field($listcomments, 'd_id')->hiddeninput(['value'=>$model->id])->label(false) ?>			
                     <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$user->d_comment_id])->label(false) ?>
                     <div class="form-group">
                            <?= Html::submitButton('Reply-Post', ['class' =>'btn btn-primary']) ?>
                    </div>					
                    <?php ActiveForm::end(); ?>
                </div>
                <?php 
                $replycomments = DonationComments::find()->where(['parent_id'=>$user->d_comment_id, 'status'=>0])->orderBy('d_comment_id Desc')->all();
                if($replycomments)
                {
                    foreach($replycomments as $replyuser)
                    {
                        $replyprofile = UserProfile::find()->where(['user_id'=>$replyuser->user_id])->one();
                        ?>
                        <div class="row">		
                            <div class="dpic floatLeft">
                                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?php echo $replyprofile->profile_image.'?v='.strtotime('now'); ?>"/>				
                            </div>
                            <div class="floatLeft col-md-10 paddingLeft">	
                                <span id="<?php echo "replyComment_".$replyuser->d_comment_id ?>">    
                                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $replyprofile->user_id ?>" ><?= $replyprofile->firstname.' '.$replyprofile->lastname; ?></a>						
                                 
                                    <?= $replyuser->comments ?><?php if($replyuser->user_id === \Yii::$app->user->id) { ?>
                                    <i class="glyphicon glyphicon-pencil editReplyComment" data-id="<?= $replyuser->d_comment_id ?>" aria-hidden="true"></i>
                                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteReplyComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $replyuser->d_comment_id], [
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this Comment?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                    <?php } ?>
                                </span>
                                <p class="commentOptions">
                                    <?php
                                        if(!$replyuser->isLiked(\Yii::$app->user->id))
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->d_comment_id.'">Love</span>';
                                        else
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->d_comment_id.'" style="color:#B23535;">Love</span>';

                                        if($replyuser->likesCount > 0)
                                            $classCmt   =   'cmtLikesView';
                                        else
                                            $classCmt   =   '';

                                    ?>
                                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $replyuser->d_comment_id ?>'></span>
                                    <span id="cmtNum_<?= $replyuser->d_comment_id ?>"> <?= $replyuser->likesCount ?></span>
                                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($replyuser->created_at)) ?></span>
                                </p>
                            </div>
                            <div  style="display:none; margin-top:10px" id="<?php echo "editReplyBlk_".$replyuser->d_comment_id ?>">	
                                <!--<a class="close" data-action="comment-close">X</a> -->
                                <?php $form = ActiveForm::begin(['action' =>['donation/update-comment?id='.$replyuser->d_comment_id]]); ?>				 
                                 <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2, 'value'=> $replyuser->comments ])->label(false) ?>			
                                 <?= $form->field($listcomments, 'd_id')->hiddeninput(['value'=>$replyuser->d_id])->label(false) ?>			
                                 <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$replyuser->parent_id])->label(false) ?>
                                 <div class="form-group">
                                        <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                                </div>					
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>	
                        <?php
                    }
                }?>
            </div>
        </div>
    <?php } } ?>
</div>
<!-- modal NON financial Starts -->
<div class="modal fade" id="messagemodalAccept" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?php if($model->non_pay_option == 0 ){ ?>Financial <?php }else{ echo "Non-Financial";} ?></h4>
            </div>
            <div class="modal-body" style="height: auto;">
                <div class="media">
                    <div class="media-left list-icon">

                    </div>
                    <div class="media-body">


                    </div>
                </div>
                </br>
                <div class="form-group">
                    <input type="checkbox" class="msg-check" name="i_agree_non_fulfilled" id="i_agree_non_fulfilled" value="1" > I agree that the donor will have 14 days to fulfill this donation before it's consider granted.In the meantime,it will be marked as "In Progress" and after 14 days,it will be marked as "Fulfilled".If the donation has not been fulfilled after 14 days,the donor may re-submit it to our website.In addition,any financial transaction arranged with the donor must happen outside of the realm of SimplyWishes.</input>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="send-msg-check btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- modal Ends -->




<!-- modal DEcider <div class="modal fade" id="messagemodal2" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Decide Later</h4>
          </div>
          <div class="modal-body">
                <div class="media">
                  <div class="media-left list-icon">

                  </div>
                  <div class="media-body">
                        <h4 class="media-heading">“ Wisher has not specified a delivery method for this wish. Please message the wisher to arrange wish delivery ”</h4>
                  </div>
                </div>
                </br>
                <div class="form-group">
                <label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
                <textarea class="msg form-control" name="msg-textarea" id="msg-textarea" rows="4"></textarea>
                </div>
                <div class="form-group">

                <input type="checkbox" class="msg" name="i_agree_decide" id="i_agree_decide" value="1" > I agree to fulfill this wish in the manner specified by the wisher and within one month of the date that I accept it as a grantor. In the meanwhile, this wish will be marked as "In Progress" and after one month, it will be marked as "Fulfilled". The Wisher should update or ressubmit their wish if it has not been fulfilled after one month. </input>
                </div>
          </div>
          <div class="modal-footer">
                <button type="button" class="send-msg-check-decide btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
  </div>
</div>
<!-- modal Ends -->
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="messagemodalOne"  tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id='project-form'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body" style="height: auto;">
                    <div class="media">
                        <input type="hidden" name="donationid" id="donationid"  value="<?php echo $model->id;?>"/>

                        <input type="hidden" name="senduserid" id="senduserid"  value="<?php echo $model->created_by;?>"/>
                    </div>
                    </br>
                    <div class="form-group" style="width: 35% !important;">
                        <label for="subject">Subject <span class='valid-star-color' >*</span></label>
                        <input type="text" id="subject" class="form-control" value="<?php echo $model->title; ?>" rows="4" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
                        <textarea id="msgOne" class="form-control" rows="4"></textarea>
                    </div>
                    <div style="text-align: center">
                        <b>
                            (Please be as detailed as possible when messaging about a specific post)
                        </b>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="send-msgOne btn btn-primary">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://momentjs.com/downloads/moment.min.js"></script>
<script>
    function test() {
        var url = '<?=Url::to(['site/login'])?>';
        window.location.href=url;
    }
</script>
<script>
    $(".send-msgOne").on("click",function(){
        var send_to = $("#senduserid").val();
        if($.trim(send_to) === "")
        {
            alert("Please Select To Sender Name.");
            return false;

        }
        var subject = $('#subject').val();
        if($.trim(subject) === "")
        {
            alert("Please check the subject.");
            return false;

        }
        var msg = $('#msgOne').val();
        if($.trim(msg) === "")
        {
            alert("Please check the message.");
            return false;

        }

        ///var send_to = $("#senduserid").val();
        var donation_id = $('#donationid').val();
        var send_from = "<?=\Yii::$app->user->id?>";
        $.ajax({
            url : '<?=Url::to(['account/send-message-inbox'])?>',
            type : 'POST',
            data : {
                subject:subject,
                msg:msg,
                donation_id:donation_id,
                send_from:send_from,
                send_to:send_to,
                send_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            },
            success: function(response){
                location.reload();
            }
        });
    });
    $(document).ready(function(){
        var url =   document.referrer;
        if(url)
        {
            if(url.match(/.com(.[^]+)/))
            {
                var referrer =  url.match(/.com(.[^]+)/)[1];
                if(referrer == '/wish/update?id=<?=$model->id?>')
                    $('#success_txt').text('Your Wish Has Been Updated Successfully!').removeClass('hide');
                setTimeout(function(){
                    $('#success_txt').text('').addClass('hide');
                }, 5000);
            }
        }
        
        $(".shareIcons").each(function(){
            console.log(1);
            var elem = $(this);
            elem.jsSocials({
                showLabel: false,
                showCount: true,
                shares: ["facebook"
                    ,{
                        share: "pinterest",           // name of share
                        via: "simplywishes5244",
                        media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    },
                    {
                        share: "twitter",           // name of share
                        via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    }],
                url : elem.attr("data_url"),
                text: elem.attr("data_text")
            });
        });
        /*if(document.referrer == 'http://localhost/simplywishes/wish/update?id')
            $('#success_txt').text('Your Wish Has Been Updated Successfully!').removeClass('hide');
        setTimeout(function(){
            $('#success_txt').text('').addClass('hide');
        }, 5000);*/
    
        $(".on-reply").click(function(){ 
                    var id = $(this).attr("for");

                    if($("#replylist_"+id).is(':hidden'))
                    {
                            $("#replylist_"+id).show();			
                    } else {
                            $("#replylist_"+id).hide();	
                    }  	

        });
        $('.editComment').on('click', function(){
            var id = $(this).data("id");

            $('#comment_'+id).hide();
            if($("#editCommentBlk_"+id).is(':hidden'))
            {
                $("#editCommentBlk_"+id).show();			
            } else {
                $("#editCommentBlk_"+id).hide();	
            }  	
        });

        $('.editReplyComment').on('click', function(){
            var id = $(this).data("id");

            $('#replyComment_'+id).hide();
            if($("#editReplyBlk_"+id).is(':hidden'))
            {
                $("#editReplyBlk_"+id).show();			
            } else {
                $("#editReplyBlk_"+id).hide();	
            }  
        });
	/* $(".close").click(function(){ 
		$(this).parent().hide();
        }); */
    });
    
    $('#back').on('click', function(){
        var url =   document.referrer;
        
        if(url && url.match(/.com(.[^]+)/))
        {
            var referrer =  url.match(/.com(.[^]+)/)[1];
            var url1 =   "/donation/view?id=<?= $model->id ?>";
            var url2 =   "/donation/update?id=<?= $model->id ?>";

            if (referrer == url1 || referrer == url2) {
                window.location.href = "<?=\Yii::$app->homeUrl?>wish/index#search_wish";
            }
            else
                window.location.href =   document.referrer;
        }
        else
            window.location.href = "<?=\Yii::$app->homeUrl?>wish/index#search_wish";
    });

    $('.report-wish').on('click', function(){
        var check = confirm( " Are you sure you want to report this donation? ");//Reporting this wish will hide this and any other wishes from this author as well as the author’s profile from you and will trigger a full review by SimplyWishes.");
		if(check){
            console.log("report");
            var d_id = <?=$model->id?>; //12;//$(this).attr("data-id");
            
            
            $.ajax({
                url : '<?=Url::to(['donation/report-content'])?>',
                type: 'GET',
                data: {
                    w_id:d_id,
                    content_type:'donation',
                    report_user:<?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>,
                    reported_user:<?=$model->created_by?>,
                    content_id:d_id,
                },
                success:function(data){
                    console.log(data);
                    if (data == "savedok"){
                        window.location.reload();
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    console.log("Status: " + textStatus);
                    console.log("Error: " + errorThrown);
                }  
            });
        }
    });


    
    $('.report-comment').on('click', function(){
        var comment_id = $(this).attr("for");
        var user_id = $(this).attr("by");
        var comment = $(this).attr("comment");
        var loggedUser = <?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>;

        if (user_id == loggedUser){
            return false;
        }

        var reportButton = $(this);
        if (reportButton.hasClass("disabled")){
            return false;
        }
        var check = confirm( " Are you sure you want to report this comment? \n Reporting this comment will hide this and any other comments from this author as well as \n the author’s profile from you and will trigger a full review by SimplyWishes.");
		if(check){
            $.ajax({
                url : '<?=Url::to(['wish/report-content'])?>',
                type: 'GET',
                data: {
                    w_id:comment_id,
                    content_type:'donation_comment',
                    report_user:<?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>,
                    reported_user:user_id,
                    content_id:comment_id,
                    comment: comment,
                },
                success:function(data){
                    console.log(data);
                    if (data == "savedok"){
                        reportButton.addClass("disabled");
                        // $("div#parent_comment_"+comment_id).remove();					
                    }
                }
            });
        }
    });
    

	
    $(document).on('click', '.deletecheck', function(){ 
        if(confirm("Are Sure To Delete this Wish ?"))
        {
            $.ajax({
                url : '<?=Url::to(['wish/ajax-delete'])?>',
                type: 'POST',
                data: {id:<?= $model->id ?>},
                success:function(data){				
                    window.location.href="<?= Url::to(['account/my-account'],true); ?>"; 
                }	
            });

        }
        else{
            return false;
        }
    });	
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.cmtLikesView', function(){
    var val =   $(this).next('span').html();
    if(val <= 0)
        return false;

    var d_id = $(this).data('id');
    if($.trim(d_id) !== "" )
    {
        if(Wish.ajax)
            return false;

        Wish.ajax   =   true;
        $.ajax({
            url : '<?=Url::to(['donation/comment-likes-view'])?>',
            type: 'GET',
            data: {id:d_id},
            success:function(data){
                if(data)
                {
                    var tpl =   '';
                    var items   =   JSON.parse(data);
                    $.each(items, function(k,v){
                       tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                    });

                    $('.modal-body').append(tpl);
                    $('#likeModal').modal({backdrop: 'static', keyboard: false});
                    $('#likeModal').modal('show');

                    $('.closeModal').on('click', function(e){
                        e.preventDefault();
                        $('.modal-body').html('');
                        $('#likeModal').modal('hide');
                    });
                }
            },
            complete: function(){
                Wish.ajax   =   false;
            }
        });
    }
});

$(document).on('click', '.loveComment', function(){
    var s_id = $(this).attr("for");
    var elem = $(this);
    $.ajax({
        url : '<?=Url::to(['donation/like-comment'])?>',
        type: 'GET',
        data: {s_id:s_id},
        success:function(data){
            console.log(data);
            if(data == "added"){
                elem.css('color', '#B23535');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) + parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
            if(data == "removed"){
                elem.css('color', '#66cc66');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) - parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
        }
    });
});

    var isVisible = false;
    var clickedAway = false;

    $(document).on("click", ".listesinside", function() {
        console.log("clicked");
        $(function(){
            console.log("clicked inside");
            $('.listesinside').popover({   
                html: true,
                content: function () {
                    var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
                    return clone;
                }
            }).click(function(e) {
                e.preventDefault();
                clickedAway = false;
                isVisible = true;
            });
        });
    });

    $(document).click(function (e) {
        if (isVisible & clickedAway) {
            $('.listesinside').popover('hide');
            isVisible = clickedAway = false;
        } else {  
            clickedAway = true;
        }
    });

    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });

    $("#complete_progress_donation").on("click", function(){
        var donation_id = "<?=$model->id?>";
        if (confirm('Are you sure your donation has been fulfilled?')) {
            $.ajax({
            url : '<?=Url::to(['donation/complete'])?>',
            type : 'POST',
            data : {donation_id:donation_id},
            success: function(response){
                alert("This Donation has been fulfilled successfully.");
                location.reload();	
                }
            }); 
        }
    });
    
    $(".send-msg-check").on("click",function(){

        if(Donation.ajax)
            return false;
        
        var processstatus = 0;

        if($('#i_agree_non_fulfilled').prop("checked") == true){
            var processstatus = 1;		
        }
        else if($('#i_agree_non_fulfilled').prop("checked") == false){
            alert("Please Accept The Agreement");
            return false;
        }

        var donation_id = "<?=$model->id?>";
        var send_to = "<?=$model->created_by?>";
        var send_from = "<?=\Yii::$app->user->id?>";
        var send_message = 1;

        Donation.ajax   =   true;
        $('#overlay').show();
        
        $.ajax({
           url : '<?=Url::to(['donation/process-donation'])?>',
           type : 'POST',
           data : {processstatus:processstatus,donation_id:donation_id,send_message:send_message},
           success: function(response){
               console.log(response);
                $.ajax({
                   url : '<?=Url::to(['account/send-message-donations-contact-details'])?>',
                   type : 'POST',
                   data : {send_from:send_from,send_to:send_to,donation_id:donation_id },
                   success: function(response){
                       console.log(response);
                        $('#messagemodalAccept').modal('hide');
                        alert("This donation has been accepted successfully.");
                        location.reload();
                    }    
                }); 
            },
            complete: function(){
                Donation.ajax   =   false;
                $('#overlay').hide();
            }
        });

    });

    /*$(".send-msg-check-decide").on("click",function(){
        if(Wish.ajax)
            return false;
        
        var processstatus = 0;
        if($('#i_agree_decide').prop("checked") == true){
            var processstatus = 1;					
        }
        else if($('#i_agree_decide').prop("checked") == false){
            alert("Please Accept The Agreement");
            return false;
        }
        var wish_id = ;
        var msg = $('#msg-textarea').val();
        var send_to = ;
        var send_from =;
        var send_message = 0;
        if($.trim(msg) === "")
        {
            alert("Please check the message.");
            return false;				
        }

        Wish.ajax   =   true;
        $('#overlay').show();
        
        $.ajax({
            url :  ,
            type : 'POST',
            data : {processstatus:processstatus,wish_id:wish_id,send_message:send_message},
            success: function(response){
                $.ajax({
                    url :  ,
                    type : 'POST',
                    data : {msg:msg,send_from:send_from,send_to:send_to,send_message:send_message},
                    success: function(response){
                        alert("This Wish Has Been Granted Successfully.");
                        $('#messagemodal2').modal('hide');
                        location.reload();
                    }
                });
            },
            complete: function(){
                Wish.ajax   =   false;
                $('#overlay').hide();
            }
        });
    });	*/



    $("#Resubmit_wishes").on("click",function(){
        var wish_id = "<?=$model->id?>";
        var userid  = "<?=\Yii::$app->user->id?>";
        if (confirm('Are You Sure to Re-Submit this wish again.')) {
            $.ajax({
                url : '<?=Url::to(['wish/resubmit-process-wish'])?>',
                type : 'POST',
                data : { wish_id:wish_id,userid:userid},
                success: function(response){
                    alert("This Wish Has Been Re-Submitted Successfully.");
                    location.reload();
                }
            });
        } 
    });	

    $("#grant_progress_wishes").on("click",function(){
        var wish_id = "<?=$model->id?>";
        var userid  = "<?=\Yii::$app->user->id?>";

        if (confirm('Are You Sure Your Wish Has been Fulfilled?')) {
            $.ajax({
                url : '<?=Url::to(['wish/grant-process-wish'])?>',
                type : 'POST',
                data : { wish_id:wish_id,userid:userid},
                success: function(response){
                    alert("This Wish Has Been Fulfilled Successfully.");
                    location.reload();	
                }
           }); 
        }
    });
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
        var val =   $(this).find('span').html();
        if(val <= 0)
            return false;

        var donation_id = $(this).data('id');
        if($.trim(donation_id) !== "" )
        {
            if(Donation.ajax)
                return false;

                Donation.ajax   =   true;
            $.ajax({
                url : '<?=Url::to(['donation/likes-view'])?>',
                type: 'GET',
                data: {id:donation_id},
                success:function(data){
                    if(data)
                    {
                        var tpl =   '';
                        var items   =   JSON.parse(data);
                        $.each(items, function(k,v){
                           tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                        });

                        $('.modal-body').append(tpl);
                        $('#likeModal').modal({backdrop: 'static', keyboard: false});
                        $('#likeModal').modal('show');

                        $('.closeModal').on('click', function(e){
                            e.preventDefault();
                            $('.modal-body').html('');
                            $('#likeModal').modal('hide');
                        });
                    }
                },
                complete: function(){
                    Donation.ajax   =   false;
                }
            });
        }
    });
        
    var Donation    =   {
        ajax    :   false
    };
	
</script>
<script>
    $(document).on('click', '.like-wish, .fav-wish', function(){

        var d_id = $(this).attr("data-id");
        var type = $(this).attr("data-a_type");
        var elem = $(this);
        $.ajax({
            url : '<?=Url::to(['donation/like'])?>',
            type: 'GET',
            data: {id:d_id,type:type},
            success:function(data){
                console.log(data);
                if(data == "added"){
                    if(type=="fav"){
                        elem.removeClass("txt-smp-orange");
                        elem.addClass("txt-smp-blue");
                    }
                    if(type=="like"){
                        elem.css('color', '#B23535');
                        var likecmt = $("#likecmt_"+d_id).text();
                        likecmt = parseInt(likecmt) + parseInt(1);
                        $("#likecmt_"+d_id).text(likecmt);
                        $("#likecmt_"+d_id).parent('.likesBlk').removeClass('hide');
                    }
                }
                if(data == "removed"){
                    if(type=="fav"){
                        elem.addClass("txt-smp-orange");
                        elem.removeClass("txt-smp-blue");
                    }
                    if(type=="like"){
                        elem.css('color', '#fff');
                        var likecmt = $("#likecmt_"+d_id).text();
                        likecmt = parseInt(likecmt) - parseInt(1);
                        $("#likecmt_"+d_id).text(likecmt);
                        if(likecmt == 0)
                            $("#likecmt_"+d_id).parent('.likesBlk').addClass('hide');
                    }
                }
            }
        });
    });
</script>

