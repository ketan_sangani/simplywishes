<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchEditorial */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'My Drafts';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class="col-md-8 editorial-index" id="scroll-evt">

    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div style="margin-bottom:15px">
            <a class="btn btn-danger pull-right" style="margin-bottom: 10px;" id="multi_delete" name="multi_delete" >Multi Delete</a>
	</div>	
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			
			[	
					'class' => 'yii\grid\CheckboxColumn',
					    'checkboxOptions' => function ($data){
						   return ['checked' =>false,'value'=>$data['id']];
					       },
								
			],
			
              'title',
          
            	[
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-eye-open"></span>', 'view-draft?id='.$model->id, [
                                        'title' => Yii::t('app', 'View'),
                            ]);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-pencil"></span>', 'update-draft?id='.$model->id, [
                                        'title' => Yii::t('app', 'Update'),
                            ]);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a('<span style="margin-left:5px" class="glyphicon glyphicon-trash"></span>', 'delete-draft?id='.$model->id, [
                                        'title' => Yii::t('app', 'Delete'),
                                        'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                    ],
                            ]);
                        },
                    ],
                ],
        ],
    ]); ?>
    
</div>
</div>

 <script type = "text/javascript">
 
$(document).ready(function(){
    $("#multi_delete").click(function(){
        var id = $.map($('input[name="selection[]"]:checked'), function(c){return c.value; });
        if($.trim(id) === "")
        {
            alert("Please Select the Checkbox to Delete!!!.");
            return false;
        }
        
        var r = confirm("Are you Sure To Delete!");
        if (r == true) {
            $.ajax({
                url: '<?=Yii::$app->homeUrl."donation/multi-delete-donations"?>',
                type: 'POST',
                data: {  id: id},
                success: function(data) {		
                    location.reload();
                }
            }); 
        }		
    });
});
$('html, body').animate({
    scrollTop: $('#scroll-evt').offset().top - 100
}, 'slow');
  </script> 
  