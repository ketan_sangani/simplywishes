<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
$wishstatus = array('0'=>"Active",'1'=>"In-Active");
$payoption = array('0'=>"Financial",'1'=>"Non-financial");
if($model->non_pay_option)
    $non_pay_option =   $payoption[$model->non_pay_option];
else
    $non_pay_option =   null;
?>

<div class="editorial-view">

    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>

    <p>
        <?= Html::a('Update', ['update-draft', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete-draft', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
		
		 <?= Html::a('Back', ['my-drafts',], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [  
		  
		  'title',
			
			[
                'attribute'=>'image',
				'value'=>!empty($model->image)?Yii::$app->homeUrl.'web/'.$model->image:'',
				 'format' => !empty($model->image)?['image',['height'=>'200px','width'=>'300px']]:'text',
				 
            ],

			[
				'label'=>'Donation Type',
				'value' =>($model->non_pay_option == 1)?'Non-Financial':'Financial',
				//'visible' => (($model->non_pay_option == 1) && ($model->show_mail_status == 1))?true:false,
			],
			[
				'label'=>'Donation Expected Cost',
				'value' =>$model->expected_cost,
				'visible' => (($model->non_pay_option == 0))?true:false,
			],
			[
				'label'=>'Financial Assitance',
				'value' =>$model->financial_assistance,
				'visible' => (($model->non_pay_option == 0))?true:false,
			],
			[
				'label'=>'Delivery Type',
				'value' =>$model->way_of_donation.'('.$model->description_of_way.')',
				'visible' => (($model->non_pay_option == 1))?true:false,
			],
		  [
				 'label'=>'Address',
				 'value' =>($model->show_mail_status == 1)?$model->show_mail:'-',
				 'visible' => (($model->non_pay_option == 1) && ($model->show_mail_status == 1))?true:false,
		  ],


		  [
				 'label'=>'Status',
				 'value' =>$wishstatus[$model->status],
		  ],
		  
        ],
    ]) ?>

</div>
