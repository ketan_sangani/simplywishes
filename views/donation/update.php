<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Wish */

$this->title = 'Update Donation: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class=" col-md-8 wish-update" id="scroll-evt">

    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?> 

</div>
</div>
<script type="text/javascript">
 var donation_upload_img    =   '<?= $model->image ?>';
 
 $( document ).ready(function() {
    $( "#save_draft" ).click(function() {
        alert('Hello');
        if($('#donation_image').val() !== donation_upload_img)
            upload();
        else {
        
            var auto_id = $("#donation-id").val();

            if($('#donation_title').val() !== '')
            {
                if($.trim(auto_id) === "")
                {
                    $.post( "donation-autosave", $("#draft_form").serialize(), function( data ) {
                        $("#donation-id").val(data);
                        window.location.href    =   'my-drafts';
                    });	
                }
                else
                {
                    $.post("donation-autosave", $("#draft_form").serialize(), function(){
                        window.location.href    =   'my-drafts';
                    });
                }
            }
            else
            {
                $('#draft_error').removeClass('hide');
                
                setTimeout(function(){
                    $('#draft_error').addClass('hide');
                }, 8000);
            }
        }
    });
    
    function upload(){
        var image_file = $('#donation_image').val();
        
        $.ajax({
            url: 'upload-file',
            type: "POST",
            data: {image : image_file, id : $("#donation-id").val()},
            success: function(json){      
                $("#donation-image").val(json);
                
                var auto_id = $("#donation-id").val();

                if($('#donation-title').val() !== '')
                {
                    if($.trim(auto_id) === "")
                    {
                        $.post( "donation-autosave", $("#draft_form").serialize(), function( data ) {
                            $("#donation-id").val(data);
                            window.location.href    =   'my-drafts';
                        });	
                    }
                    else
                    {
                        $.post("donation-autosave", $("#draft_form").serialize(), function(){
                            window.location.href    =   'my-drafts';
                        });
                    }
                }
            }
      });
    }
 });
 
 $(window).on('load', function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
});

$('#cancel').on('click', function(){
    window.location.href =   document.referrer;
});
 
</script>
