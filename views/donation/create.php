<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Donation */

$this->title = 'Make a Donation';
$this->params['breadcrumbs'][] = ['label' => 'Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class= " col-md-8 donation-create" id="scroll-evt">

    <h3  class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile
    ]) ?>

</div>


<script type="text/javascript">
 
 $( document ).ready(function() {
//    $( "#save_draft" ).click(function() {
//
//        if($('#donation-image').val())
//        {
//            var auto_id = $("#donation-id").val();
//
//            if($('#donation-title').val() !== '')
//            {
//                if($.trim(auto_id) === "")
//                {
//
//                    $.post( "donation-autosave", $("#draft_form").serialize(), function( data ) {
//                        $("#donation-id").val(data);
//                        upload();
//                    });
//                }
//                else
//                {
//                    $.post("donation-autosave", $("#draft_form").serialize(), function(){
//                        upload();
//                    });
//                }
//            }
//            else
//            {
//                $('#draft_error').removeClass('hide');
//
//                setTimeout(function(){
//                    $('#draft_error').addClass('hide');
//                }, 3000);
//            }
//        }
//        else {
//
//            var auto_id = $("#donation-id").val();
//            // console.log("auto_id: " + auto_id);
//            // console.log("auto_id: " + $('#donation-title').val());
//
//            if($('#donation-title').val() !== '')
//            {
//                if($.trim(auto_id) === "")
//                {
//
//                    // return false;
//                    $.post( "donation-autosave", $("#draft_form").serialize(), function( data ) {
//
//                        $("#donation-id").val(data);
//                        window.location.href    =   'my-drafts';
//                    });
//                }
//                else
//                {
//                    console.log("else");
//                    return false;
//                    $.post("donation-autosave", $("#draft_form").serialize(), function(){
//                        window.location.href    =   'my-drafts';
//                    });
//                }
//            }
//            else
//            {
//                $('#draft_error').removeClass('hide');
//
//                setTimeout(function(){
//                    $('#draft_error').addClass('hide');
//                }, 8000);
//            }
//        }
//    });
    
    function upload(){
        var image_file = $('#donation-image').val();
        
        $.ajax({
            url: 'upload-file',
            type: "POST",
            data: {image : image_file, id : $("#donation-id").val()},
            success: function(json){      
                $("#donation-image").val(json);
                
                var auto_id = $("#donation-id").val();

                if($('#donation-title').val() !== '')
                {
                    if($.trim(auto_id) === "")
                    {
                        $.post( "donation-autosave", $("#draft_form").serialize(), function( data ) {
                            $("#donation-id").val(data);
                            window.location.href    =   'my-drafts';
                        });	 
                    }
                    else
                    {
                        $.post("donation-autosave", $("#draft_form").serialize(), function(){
                            window.location.href    =   'my-drafts';
                        });
                    }
                }
            }
      });
    }
 });
 $(window).on('load', function () {
     console.log("load");
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
    
    $("li[data-id=post_donation]").addClass("active");
});
 
</script>
