<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Wish */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Donation: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Donations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';

$wishstatus = array('0'=>"Active",'1'=>"In-Active");
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>
<link rel="stylesheet" type="text/css" href="<?=Yii::$app->homeUrl?>web/css/croppie.css">
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>web/src/croppie.js"></script>
<div class=" col-md-8 wish-update">
    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'draft_form','enableAjaxValidation' => true]); ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label("Donation Title <span class='valid-star-color' >*</span> ") ?>

            <?php // $form->field($model, 'summary_title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'preset' => 'basic',
                'clientOptions' => ['height' => 100]
            ]); ?>
            <p><label>Donation Image</label></p>
            <p><span>Choose an image that you own. Do not upload any copyright images such as images from movie characters or known company products or your post could be deleted without warning.</span></p>

            <div id="upload-img" class="hide"></div>
            <div class="range-btn hide">
                <input type="button" class="btn btn-primary zoomOut" id="zoomOut" style="position: relative;bottom: 4px;left: 336px;" value="-">
                <input type="button" class="btn btn-primary zoomIn" id="zoomIn" value="+" style="position: relative;left: 342px;bottom: 4px;">
            </div>
            <?php if(!empty($model->image)) {  ?>
                <div>
                    <img class='image_block' id="image" src="<?= \Yii::$app->homeUrl.'web/'.$model->image.'?v='.strtotime('now')?>" width="300" />
                    <a href="#" class="removeImage">Remove</a>
                </div>
            <?php } else { ?>
                <div>
                    <img class='image_block hide' id='image' width="300"/>
                    <a href="#" class="removeImage hide">Remove</a>
                </div>
            <?php } ?>
            <div class="actions">

                <a class="btn file-btn btn btn-primary">
                    <span>Choose an Image from your Files</span>
                    <input type="file" id="upload" value="Choose a file" accept="image/*" />
                </a>

            </div>
            <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'image_name')->hiddenInput()->label(false); ?>
            <span>Or, Choose One from the Default Images below</span>  
            <div class="gravatar wishDefault">
                <a class="profilelogo" for="images/wish_default/1.jpg" ><img class="selected" src="<?=Yii::$app->homeUrl?>web/images/wish_default/1.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/2.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/3.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/4.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/5.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/5.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/6.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/7.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/8.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/9.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/10.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/11.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/12.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/13.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/14.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/15.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/16.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/17.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/17.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/18.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/18.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/19.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/19.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/20.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/20.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/21.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/21.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/22.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/22.jpg" width="100"/></a>
            </div>


            <?php  echo $form->field($model, 'non_pay_option')->radioList([
                '0'=>'Yes (Financial)','1'=>'No (Non-Financial)'],[
                'onclick' => "$(this).val($('input:radio:checked').val())"])->label("Does your Donation require direct funding? <span class='valid-star-color' >*</span>"); ?>

            <!-- Financial Begin --->
            <div class="edit" id="financial" style="display:none;">

                <?php

                echo $form->field($model, 'financial_assistance')->radioList([
                    'Paypal'=>'Paypal',
                    'Venmo'=>'Venmo',
                    'CashApp'=>'CashApp',
                    'Google/Apple Pay'=>'Google/Apple Pay',
                    'GoFundMe'=>'GoFundMe',
                    'Zelle'=>'Zelle',
                    'Other'=>'Other'
                ],
                    ['itemOptions' => [
                        'id' => 'dt',
                        'class' =>'deltype'
                    ]]
                )->label("How would you like to give the financial assistance? <span class='valid-star-color' >*</span>"); ?>

                <div class="edit" id="other" style="display:none;">
                    <h6 id="other_txt">

                        Please specify the Financial Assitance details here.

                    </h6>
                    <?= $form->field($model, 'financial_assistance_other')->textInput(['maxlength' => true])->label("<span class='valid-star-color' >*</span> ") ?>
                    <div id="delivery_other_error" class="delivery_other_error-class" style="display:none; color:#a94442;" >Specify the Financial Assistance Other here. </div>

                </div>

                <?= $form->field($model, 'expected_cost')->textInput(['maxlength' => true])->label("Expected Cost (USD) <span class='valid-star-color' >*</span> ");?>
                <div id="expected_cost_check" style="display:none; color:#a94442;" >Expected Cost Is empty check</div>
            </div>
            <div class="edit" id="non_financial" style="display:none;">
                <?php

                echo $form->field($model, 'way_of_donation')->radioList([
                    'Online order'=>'Online order',
                    'Drop-off/Pick up Location'=>'Drop-off/Pick up Location',
                    'Send in the mail'=>'Send in the mail',
                    'Other'=>'Other',
                ],
                    ['itemOptions' => [
                        'class' =>'description_of_way'
                    ]]
                )->label("How would you like to give this Donation? <span class='valid-star-color' >*</span>"); ?>
                <h6>


                </h6>
                <?= $form->field($model, 'description_of_way')->textarea(['maxlength' => true])->label("",['class'=>'descwaylabel']) ?>
                <div id="dropofdesc"  style="display:none; color:#a94442;" >*We recommend that you always meet in a public space and if bring a buddy whenever possible.  </div>


            </div>

            
            <?= $form->field($model, 'acknowledgement')->checkbox(['value' =>'1','label'=>'I understand that once the donation is accepted by a Wisher, I have 14 days to fulfill it. In offering this Donation, I agree to work with the Wisher to fulfill this Donation within 14 days, during which time this Donation will be marked as In Progress. After 14 days, it will be marked as Fulfilled. I will not be able to delete or update my donation once someone has accepted my offer.<span class="valid-star-color" >*</span>.','class'=>"checkall",'for'=>'acknowledgement-class']);	?>
            <div id="acknowledgment" class="acknowledgement-class" style="display:none; color:#a94442;margin-top: -12px;
    margin-bottom: 12px;" >Acknowledge you accept this condition</div>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Post this Donation', ['class' => $model->isNewRecord ? 'btn btn-success upload-result' : 'btn btn-primary upload-result', 'id'=>'save_donation']) ?>
                <a id="cancel" class="btn btn-primary">Cancel</a>
            </div>

            <?php 
                if($model->isNewRecord)
                    echo $form->field($model, 'auto_id')->hiddenInput()->label(false)  
            ?>
	
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<script type="text/javascript" >

    $('.deltype').change(function () {
        var option;

        option = this.value;
        if(option == 'Other')
        {
            if ($("#other").is(":visible")){

                $('#other').hide();
            } else {
                $('#other').show();
            }
        }else{
            $('#other').hide();
        }

    });
    $("#donation-acknowledgement").change(function() {
        if(this.checked) {
            $("#acknowledgment").hide();
        }else{
            $("#acknowledgment").show();
        }
    });
    $('.description_of_way').change(function () {
        var option;

        option = this.value;
        if(option=='Online order'){
            $('.descwaylabel').html('');
            $('.descwaylabel').html('Link to webpage<span class="valid-star-color" >*</span>');
            ('#dropofdesc').hide();
        }else if(option=='Drop-off/Pick up Location'){
            $('.descwaylabel').html('');
            $('.descwaylabel').html('Drop-off/Pick up Location<span class="valid-star-color" >*</span>');
            $('#dropofdesc').show();
        }else if(option=='Send in the mail'){
            $('.descwaylabel').html('');
            $('.descwaylabel').html('Send in the mail (FedEx, UPS, DHL, USPS, etc)<span class="valid-star-color" >*</span>');
            $('#dropofdesc').hide();
        }else if(option=='Other'){
            $('.descwaylabel').html('');
            $('.descwaylabel').html('');
            $('#dropofdesc').hide();
        }



    });
$( document ).ready(function() {
    // $("#donation-delivery_type").on("click",function(){
    //     var option = $("input[name='Donation[delivery_type]']:checked").val();   
 
    //     if(option == 4)
    //     {
    //         $('#other').show();
    //     } else
    //     {
    //         $('#other').hide();

    //     }	
    // });
    $("#donation-non_pay_option").on("click",function(){
        var pay_option = $("#donation-non_pay_option").val();
        $('.edit').hide();
        if(pay_option == 0)
        {
            $('#financial').show();
        } else if(pay_option == 1)
        {
            $('#non_financial').show();

        }
    });
    if($('.deltype').is(':checked')){
        if ($('.deltype:checked').val()=='Other' ){
            $('#other').show();
        }
    }

    $("#save_donation").on('click', function(e){
        var canCreate = '<?php echo $can_create; ?>';
        console.log(canCreate);
        if (canCreate == ''){
            alert("You already have the maximum number of allowed donations");
            return false;
        }
    
        if ($("#other").is(":visible")){

            if ($("#donation-delivery_other").val() == ''){
                $("#delivery_other_error").show();
                
                return false;
            } else {
                $("#delivery_other_error").hide();
            }
        } 
        if (!$("#donation-acknowledgement").is(":checked")){
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }


    });

    <?php 
        if((!$model->isNewRecord) && ($model->non_pay_option === 0))
        { ?>
            $('#financial').show();
	<?php } else if((!$model->isNewRecord) && ($model->non_pay_option == 1)){ ?>
            $('#non_financial').show();
	<?php } ?>
	
	$("#donation-non_pay_option").val(<?= $model->non_pay_option ?>);
        
        $( "#draft_form" ).on('beforeSubmit', function( event ) {
			
		if($("#imageclass").length)
		{
			var check = false;
			var imagecheck = $("#imageclass").val();
			if($.trim(imagecheck)  == "")
			{
				$('#imageclass_check').show();
				check = true;
			} else {
				$('#imageclass_check').hide();
			}				
			
			if(check == true)
			{	
                            $('html, body').animate({
                                scrollTop: $('#imageclass').offset().top - 100
                            }, 1000);
                            return false;
			}
			
		}
		var pay_option = $("#donation-non_pay_option").val();
		
		if(parseInt(pay_option) == 1)
		{	
			//********************* Begin *******************/	
			var check = false;
			var show_mail = $("#wish-show_mail").val();
			if($.trim(show_mail) !== "")
			{
				if($("#wish-show_mail_status").prop("checked") == false){
					$("#wish-show_mail_status_check").show();
					check = true;
				}
			}
			
			var show_person_street = $("#wish-show_person_street").val();
                        var show_person_city = $("#wish-show_person_city").val();
                        var show_person_state = $("#wish-show_person_state").val();
                        var show_person_zip = $("#wish-show_person_zip").val();
                        var show_person_country = $("#wish-show_person_country").val();

                        if(($.trim(show_person_street) !== "") || ($.trim(show_person_city) !== "") || ($.trim(show_person_state) !== "") || ($.trim(show_person_zip) !== "") || ($.trim(show_person_country) !== "") )
                        {
                            if($("#wish-show_person_status").prop("checked") == false){
                                $("#wish-show_person_status_check").show();
                                check = true;
                            }
                        }
		
			var show_other_specify = $("#wish-show_other_specify").val();
			if($.trim(show_other_specify) !== "")
			{
				if($("#wish-show_other_status").prop("checked") == false){
					$("#wish-show_other_status_check").show();
					check = true;
				}
			}								
			if($("#i_agree_decide").prop("checked") == false){
				$("#i_agree_decide_req").show();
				check = true;
			}
					
			
			
			//********************* End *******************/
			//********************* Begin *******************/
			
		
			
			if($("#wish-show_mail_status").prop("checked") == true)
			{
				var show_mail = $("#wish-show_mail").val();
				if($.trim(show_mail) == "")
				{
					$("#wish-show_mail"+"_check").show();
					check = true;
				}
			}
			
			if($("#wish-show_person_status").prop("checked") == true)
                        {
                            if($.trim(show_person_street) == "")
                            {
                                $("#wish-show_person_street"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_city) == "")
                            {
                                $("#wish-show_person_city"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_state) == "")
                            {
                                $("#wish-show_person_state"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_zip) == "")
                            {
                                $("#wish-show_person_zip"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_country) == "")
                            {
                                $("#wish-show_person_country"+"_check").show();
                                check = true;
                            }
                        }
			
			
			if($("#wish-show_other_status").prop("checked") == true)
			{
				var show_other_specify = $("#wish-show_other_specify").val();
				if($.trim(show_other_specify) == "")
				{
					$("#wish-show_other_specify"+"_check").show();
					check = true;
				}
			}

			
			//********************* End *******************/
			
		} 
		else if(parseInt(pay_option) == 0)
		{
			var expected_cost = $("#wish-expected_cost").val();
			if($.trim(expected_cost) == ""){
                            $('html, body').animate({
                                scrollTop: $('#wish-non_pay_option').first()
                            }, 1000);
                            $("#expected_cost_check").show();
                            return true;
			}
                        else
                            $('#overlay').show();
						
		}
		
	});
        
        $('#draft_form').on('afterValidate', function (event, messages) {
            if(typeof $('.has-error').first().offset() !== 'undefined') {
                $('html, body').animate({
                    scrollTop: $('.has-error').first().offset().top - 200
                }, 1000);
            }
        });
		
		 $("#wish-expected_cost").change(function(){
			var expected_cost = $(this).val();
			if($.trim(expected_cost) != ""){
				$("#expected_cost_check").hide();
			}						 
	    });
		
	    $("#i_agree_decide2").change(function(){
			if($("#i_agree_decide2").prop("checked") == true){
						$("#i_agree_decide_req2").hide();
			}						 
	    });
	
	    $("#i_agree_decide").change(function(){
			if($("#i_agree_decide").prop("checked") == true){
						$("#i_agree_decide_req").hide();
			}						 
	    });
	

		
		$(".check_test").change(function(){
			var id = $(this).attr("id");
			if($("#"+id).val() != ""){
				$("#"+id+"_check").hide();
			}						 
	    });
            
            $uploadCrop = $('#upload-img').croppie({
        enableExif: true,
        viewport: {
            width: 300,
            height: 200
        },
        boundary: {
            width: 400,
            height: 300
        },
        enableOrientation: true
    });


    $('#upload').on('change', function () { readFile(this); });

    function readFile(input) {
        $('#upload-img').removeClass('hide');
        $('#loader').removeClass('hide');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload-img').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    $('#loader').addClass('hide');
                    $('.upload-buttons').removeClass('hide');
                    $("#userprofile-dulpicate_image").val('');
                    $(".range-btn").removeClass('hide');
                    $(".cr-slider").hide();
                    $('.profilelogo').find( "img" ).removeClass('selected');
                });
            };

            reader.readAsDataURL(input.files[0]);
        }
        else
            $('#loader').addClass('hide');
    }
    /*++*/
    var zoomvalue = 0.3000000000000001;
    $('#zoomIn').on('click', function(){
        if (zoomvalue >= 1.500000000000001){
            return false;
        }
        console.log(zoomvalue);
        var zoomin = zoomvalue + 0.01;
        var val = $uploadCrop.croppie('setZoom', zoomin);
        zoomvalue = zoomvalue + 0.01;

    });
    /*--*/
    $('#zoomOut').on('click', function(){
        /* var value = $uploadCrop.get();
         console.log(value);*/
        /*console.log(value[0].clientWidth);*/
        if (zoomvalue <= 0.3000000000000001){
            return false;
        }
        console.log(zoomvalue);
        var zoomout = zoomvalue - 0.01;
        var val =  $uploadCrop.croppie('setZoom', zoomout);
        zoomvalue = zoomvalue - 0.01;
    });

    $('.upload-result').on('click', function(e){
        console.log(1);
        e.preventDefault();
        if ($("#other").is(":visible")){
            if ($("#donation-delivery_other").val() == ''){
                $("#delivery_other_error").show();
                return false;
            } else {
                $("#delivery_other_error").hide();
            }
        }



        if (!$("#donation-acknowledgement").is(":checked")){
            console.log('ack');
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            console.log(resp);
            //$('#image_name').val(resp);
            $('#donation-image').val(resp);
            $('#image').attr('src', resp).removeClass('hide');
            $('.removeImage').removeClass('hide');
//            $('html, body').animate({
//                scrollTop: $('.field-donation-description').offset().top - 100
//            }, 'slow');
        });
        $('#draft_form').submit();

    });


    $('.upload-rotate').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
    });

    $('.removeImage').on('click', function(e){
        e.preventDefault();
        $('#wish-primary_image').val('');
        $('#image').attr('src', '').addClass('hide');
        $(this).addClass('hide');
        $('.default-img').addClass('selected');
        $(this).addClass('hide');
    });
		
});
$(function(){				
    $('.profilelogo').click(function(){
        $('.profilelogo').find( "img" ).removeClass('selected'); 
        var val = $(this).attr('for');
        $(this).find( "img" ).addClass('selected'); 
        $("#wish-primary_image_name").val(val);
    });
}); 

$('#cancel').on('click', function(){
    window.location.href =   document.referrer;
});
</script>

<style>
#wish-non_pay_option label{
	margin-left : 25px  !important
}

</style>