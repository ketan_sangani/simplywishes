<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker; 
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Wish */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Donations';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?=Yii::$app->homeUrl?>web/css/croppie.css">
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>web/src/croppie.js"></script>
<div class="row">
    <div class="col-md-12">
        <?php
        $form = ActiveForm::begin(['id' => 'draft_form','enableAjaxValidation' => ($model->isNewRecord)?true:false]); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true,'onkeydown'=>"return (event.keyCode!=13);"])->label("Donation Title <span class='valid-star-color' >*</span> ") ?>
        <?php // $form->field($model, 'summary_title')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'preset' => 'basic',
            'clientOptions' => ['height' => 100]
        ]); ?>
        <p><label>Donation Image</label></p>
        <p><span>Choose an image that you own. Do not upload any copyright images such as images from movie characters or known company products or your post could be deleted without warning.</span></p>
        <div id="upload-img" class="hide"></div>
        <div class="range-btn hide">
            <input type="button" class="btn btn-primary zoomOut" id="zoomOut" style="position: relative;bottom: 4px;left: 336px;" value="-">
            <input type="button" class="btn btn-primary zoomIn" id="zoomIn" value="+" style="position: relative;left: 342px;bottom: 4px;">
        </div>
        <?php if(!empty($model->image)) {  ?>
            <div>
                <img class='image_block' id="image" src="<?= \Yii::$app->homeUrl.'web/'.$model->image.'?v='.strtotime('now')?>" width="300" />
                <a href="#" class="removeImage">Remove</a>
            </div>
        <?php } else { ?>
            <div>
                <img class='image_block hide' id='image' width="300"/>
                <a href="#" class="removeImage hide">Remove</a>
            </div>
        <?php } ?>
        <div class="actions">

            <a class="btn file-btn btn btn-primary">
                <span>Choose an Image from your Files</span>
                <input type="file" id="upload" value="Choose a file" accept="image/*" />
            </a>

        </div> 
        <?= $form->field($model, 'image')->hiddenInput()->label(false) ?>
        <?php
        if($model->isNewRecord){
            $model->image_name = 'images/wish_default/1.jpg';
        }
        ?>
        <?= $form->field($model, 'image_name')->hiddenInput()->label(false); ?>
	<span>Or, Choose One from the Default Images below</span>  
        <div class="gravatar wishDefault">
            <a class="profilelogo" for="images/wish_default/1.jpg" ><img class="selected" src="<?=Yii::$app->homeUrl?>web/images/wish_default/1.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/2.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/3.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/4.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/5.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/5.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/6.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/7.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/8.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/9.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/10.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/11.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/12.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/13.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/14.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/15.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/16.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/20.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/20.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/21.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/21.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/22.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/22.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/23.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/23.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/24.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/24.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/25.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/25.jpg" width="100"/></a>
            <a class="profilelogo" for="images/wish_default/26.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/26.jpg" width="100"/></a>
        </div>
	
	


        <?php  echo $form->field($model, 'non_pay_option')->radioList(
            ['0'=>'Yes (Financial)',
             '1'=>'No (Non-Financial)'],
            ['itemOptions' => [
                'class' =>'nonpaydonation'
            ]])->label("Does your Donation require direct funding? <span class='valid-star-color' >*</span>"); ?>

        <!-- Financial Begin --->
        <div class="edit" id="financial" style="display:none;">

                <?php

                echo $form->field($model, 'financial_assistance')->radioList([
                    'Paypal'=>'Paypal',
                    'Venmo'=>'Venmo',
                    'CashApp'=>'CashApp',
                    'Google/Apple Pay'=>'Google/Apple Pay',
                    'GoFundMe'=>'GoFundMe',
                    'Zelle'=>'Zelle',
                    'Other'=>'Other'
                ],
                    ['itemOptions' => [
                        'id' => 'dt',
                        'class' =>'deltype'
                    ]]
                )->label("How would you like to give the financial assistance? <span class='valid-star-color' >*</span>"); ?>

                <div class="edit" id="other" style="display:none;">
                    <h6 id="other_txt">

                            Please specify the Financial Assitance details here.

                    </h6>
                    <?= $form->field($model, 'financial_assistance_other')->textInput(['maxlength' => true])->label("<span class='valid-star-color' >*</span> ") ?>
                    <div id="delivery_other_error" class="delivery_other_error-class" style="display:none; color:#a94442;" >Specify the Financial Assistance Other here. </div>

                </div>

            <?= $form->field($model, 'expected_cost')->textInput(['maxlength' => true])->label("Expected Cost (USD) <span class='valid-star-color' >*</span> ");?>
            <div id="expected_cost_check" style="display:none; color:#a94442;" >Expected Cost Is empty check</div>
        </div>
        <div class="edit" id="non_financial" style="display:none;">
            <?php

            echo $form->field($model, 'way_of_donation')->radioList([
                'Online order'=>'Online order',
                'Drop-off/Pick up Location'=>'Drop-off/Pick up Location',
                'Send in the mail'=>'Send in the mail',
                'Other'=>'Other',
            ],
                ['itemOptions' => [
                    'class' =>'description_of_way'
                ]]
            )->label("How would you like to give this Donation? <span class='valid-star-color' >*</span>"); ?>
            <h6>


            </h6>
            <?= $form->field($model, 'description_of_way')->textarea(['maxlength' => true])->label("",['class'=>'descwaylabel']) ?>
            <div id="dropofdesc"  style="display:none; color:#a94442;" >*We recommend that you always meet in a public space and bring a buddy whenever possible.  </div>


        </div>
    <?= $form->field($model, 'acknowledgement')->checkbox(['value' =>'1','label'=>' I understand that once the donation is accepted by a Wisher, I have 14 days to fulfill it. In offering this Donation, I agree to work with the Wisher to fulfill this Donation within 14 days, during which time this Donation will be marked as In Progress. After 14 days, it will be marked as Fulfilled. I will not be able to delete or update my donation once someone has accepted my offer.<span class="valid-star-color" >*</span>','class'=>"check-all",'for'=>'acknowledgement-class']);	?>
    <div id="acknowledgment" class="acknowledgement-class" style="display:none; color:#a94442;margin-top: -12px;
    margin-bottom: 12px;" >Please acknowledge you accept this condition</div>

        
	
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => 'btn btn-primary upload-result', 'id'=>'save_donation']) ?>
            <?php
            echo Html::a('Save As Drafts', ['donation-autosave'], [
                'class' => 'btn btn-primary saveasdraft upload-result-draft',
                'data' => [
                    'method' => 'post'
                ]
            ]);?>
<!--            <a id="save_draft" class="btn btn-primary">Save As Drafts</a>-->
            <?php if(!$model->isNewRecord) { ?>
                <a id="cancel" class="btn btn-primary">Cancel</a>
            <?php } ?>
        </div>
        <p id="draft_error" class="text-danger hide">Please enter at least the title of the donation to save as draft !</p>

	<?= $form->field($model, 'id')->hiddenInput()->label(false) ?> 
        <?php ActiveForm::end(); ?>
    </div>
</div>
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<script type="text/javascript" >

$('.deltype').change(function () {
    var option;

        option = this.value; 
        if(option == 'Other')
        {
            if ($("#other").is(":visible")){

                $('#other').hide();
            } else {
                $('#other').show();
            }
        }else{
            $('#other').hide();
        }

});
$("#donation-acknowledgement").change(function() {
    if(this.checked) {
        $("#acknowledgment").hide();
    }else{
        $("#acknowledgment").show();
    }
});
$('.description_of_way').change(function () {
    var option;

    option = this.value;
    if(option=='Online order'){
        $('.descwaylabel').html('');
        $('.descwaylabel').html('Link to webpage<span class="valid-star-color" >*</span>');
        $('#dropofdesc').hide();
    }else if(option=='Drop-off/Pick up Location'){
        $('.descwaylabel').html('');
        $('.descwaylabel').html('Drop-off/Pick up Location<span class="valid-star-color" >*</span>');
        $('#dropofdesc').show();
    }else if(option=='Send in the mail'){
        $('.descwaylabel').html('');
        $('.descwaylabel').html('Send in the mail (FedEx, UPS, DHL, USPS, etc)<span class="valid-star-color" >*</span>');
        $('#dropofdesc').hide();
    }else if(option=='Other'){
        $('.descwaylabel').html('');
        $('.descwaylabel').html('');
        $('#dropofdesc').hide();
    }



});
$( document ).ready(function() {
    if($('.deltype').is(':checked')){
        if ($('.deltype:checked').val()=='Other' ){
            $('#other').show();  
        }
    }
    $('.nonpaydonation').change(function () {
    //$(".nonpaydonation").on("change",function(){
        var pay_option = this.value;
        if(pay_option == 0)
        {
            $('#financial').show();
            $('#non_financial').hide();
        } else if(pay_option == 1)
        {
            $('#non_financial').show();
            $('#financial').hide();

        }

    });

    <?php 
        if((!$model->isNewRecord) && ($model->non_pay_option === 0))
        { ?>
            $('#financial').show();
	<?php } else if((!$model->isNewRecord) && ($model->non_pay_option == 1)){ ?>
            $('#non_financial').show();
	<?php } ?>
        

    var Form    =   {
        busy    :   false
    };

    $("#save_donation").on('click', function(e){


        if ($("#other").is(":visible")){
            if ($("#donation-financial_assistance_other").val() == ''){
                $("#delivery_other_error").show();
                    return false;
                } else {
                    $("#delivery_other_error").hide();
                }
        } 



        if (!$("#donation-acknowledgement").is(":checked")){
            //console.log('ack');
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }
    });
    $(".saveasdraft").on('click', function(e){


        if ($("#other").is(":visible")){
            if ($("#donation-financial_assistance_other").val() == ''){
                $("#delivery_other_error").show();
                return false;
            } else {
                $("#delivery_other_error").hide();
            }
        }



        if (!$("#donation-acknowledgement").is(":checked")){
            //console.log('ack');
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }
    });

		



		
    $(".check_test").change(function(){
        var id = $(this).attr("id");
        if($("#"+id).val() != ""){
                $("#"+id+"_check").hide();
        }						 
    });

    $uploadCrop = $('#upload-img').croppie({
        enableExif: true,
        mouseWheelZoom : false,
        viewport: {
            width: 360,
            height: 240
        },
        boundary: {
            width: 400,
            height: 340
        },
        enableOrientation: true
    });

    $('#upload').on('change', function () { readFile(this); });

    function readFile(input) {
        $('#upload-img').removeClass('hide');
        $('#loader').removeClass('hide');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload-img').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    $('#loader').addClass('hide');
                    $('.upload-buttons').removeClass('hide');
                    $("#userprofile-dulpicate_image").val('');
                    $(".range-btn").removeClass('hide');
                    $(".cr-slider").hide();
                    $('.profilelogo').find( "img" ).removeClass('selected');

                });
            };

            reader.readAsDataURL(input.files[0]);
        }
        else
            $('#loader').addClass('hide');
    }
    /*++*/
    var zoomvalue = 0.3000000000000001;
    $('#zoomIn').on('click', function(){
        if (zoomvalue >= 1.500000000000001){
            return false;
        }
        console.log(zoomvalue);
        var zoomin = zoomvalue + 0.01;
        var val = $uploadCrop.croppie('setZoom', zoomin);
        zoomvalue = zoomvalue + 0.01;

    });
    /*--*/
    $('#zoomOut').on('click', function(){
        /* var value = $uploadCrop.get();
         console.log(value);*/
        /*console.log(value[0].clientWidth);*/
        if (zoomvalue <= 0.3000000000000001){
            return false;
        }
        console.log(zoomvalue);
        var zoomout = zoomvalue - 0.01;
        var val =  $uploadCrop.croppie('setZoom', zoomout);
        zoomvalue = zoomvalue - 0.01;
    });
    $('.upload-result').on('click', function(e){
        e.preventDefault();
        if ($("#other").is(":visible")){
            if ($("#donation-delivery_other").val() == ''){
                $("#delivery_other_error").show();
                return false;
            } else {
                $("#delivery_other_error").hide();
            }
        }



        if (!$("#donation-acknowledgement").is(":checked")){
            //console.log('ack');
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            console.log(resp);
            //$('#image_name').val(resp);
            $('#donation-image').val(resp);
            $('#image').attr('src', resp).removeClass('hide');
            $('.removeImage').removeClass('hide');
//            $('html, body').animate({
//                scrollTop: $('.field-donation-description').offset().top - 100
//            }, 'slow');
        });
        $('#draft_form').submit();

    });


    $('.upload-result-draft').on('click', function(e){
        e.preventDefault();
        if ($("#other").is(":visible")){
            if ($("#donation-delivery_other").val() == ''){
                $("#delivery_other_error").show();
                return false;
            } else {
                $("#delivery_other_error").hide();
            }
        }



        if (!$("#donation-acknowledgement").is(":checked")){
            //console.log('ack');
            $("#acknowledgment").show();
            return false;
        } else {
            $("#acknowledgment").hide();
        }
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            console.log(resp);
            //$('#image_name').val(resp);
            $('#donation-image').val(resp);
            $('#image').attr('src', resp).removeClass('hide');
            $('.removeImage').removeClass('hide');
//            $('html, body').animate({
//                scrollTop: $('.field-donation-description').offset().top - 100
//            }, 'slow');
        });
    });
    
    $('.upload-rotate').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
    });

    $('.removeImage').on('click', function(e){
        e.preventDefault();
        $('#wish-primary_image').val('');
        $('#image').attr('src', '').addClass('hide');
        $(this).addClass('hide');
        $('.default-img').addClass('selected');
        $(this).addClass('hide');
    });
});

$(function(){				
    $('.profilelogo').click(function(){
        console.log("profilelog");
        $('.profilelogo').find( "img" ).removeClass('selected'); 
        var val = $(this).attr('for');
        $(this).find( "img" ).addClass('selected'); 
        $("#donation-image_name").val(val);
        console.log($("#donation-image_name").val());
    });
});  
</script>

<style>
#wish-non_pay_option label{
	margin-left : 25px  !important
}

</style>
