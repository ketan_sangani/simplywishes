<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Update: ' . $model->e_title;
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->e_id, 'url' => ['view', 'id' => $model->e_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="editorial-update">

    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_formupdate', [
        'model' => $model,
        'class' => $class,
        'error' => $error, 'msg' => $msg,
        'tab' => $tab
    ]) ?>

</div>
