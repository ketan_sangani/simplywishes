<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Editorial */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile(\Yii::$app->homeUrl."web/css/waitingfor.js");
if($class === 'article')
{
?>

<div role="tabpanel" class="tab-pane active" id="article">
    <div class="editorial-form">
        <?php $form = ActiveForm::begin(['id' => 'article_form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    
        <?= $form->field($model, 'e_title')->textInput(['maxlength' => true])->label("Title <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($model, 'e_text')->widget(CKEditor::className(), [
            'preset' => 'basic',
            'clientOptions' => ['height' => 100]
        ])->label("Write or Insert your article in the space provided below <span class='valid-star-color' >*</span> "); ?>		

        <p>
            <?php
            if($model->featured_video_url != ''){ 
                $url = $model->featured_video_url; 
                if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                    $domain = parse_url($url, PHP_URL_HOST);
                    $pos    =   strpos($domain, 'simplywishes');

                    if($pos == false)
                    {
                        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                        if(isset($match[1]))
                        {
                            $youtube_id = $match[1];
                            echo "<div style='width: 60%;'><div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://www.youtube.com/embed/".$youtube_id."' controls></iframe></div></div>";

                        }
                        else
                            echo "<div style='width: 60%;'><div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='".$url."' controls></iframe></div></div>";
                    }
                    else
                        echo "<div style='width: 70%'><video poster='".Yii::$app->homeUrl."web/".$model->e_image."' controls autoplay><source src=".$url." type='video/mp4'>Your browser does not support the video tag.</video></div>";
                } else {
                    echo $url;
                }
            } ?>
        </p>
        <?php if($model->article_image != '' ){ ?>
                    <div class="form-group field-company-logo" >
                        <label class="control-label" >Upload an Image for your Article <span class='valid-star-color' >*</span></b></label><br>
                        <img id='imagesorce1' src="<?=Yii::$app->homeUrl.'web/'.$model->article_image ?>" height="100px" />
                        &nbsp;<a style="cursor:pointer" class="removearticleimage" >Change Image</a>
                    </div>
                    <?php
                    echo $form->field($model, 'articleimage')->fileInput(['class'=>'form-control','style'=>'display:none'])->label(false);
               }else{ ?>
            <p><b>Upload an Image for your Article <span class='valid-star-color' >*</span></b>  </p>

            <?php echo $form->field($model, 'articleimage')->fileInput(['class'=>'form-control'])->label(false);
                }?>


        
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php }
else if($class == 'video')
{ ?>
<div role="tabpanel" class="tab-pane" id="video">
    <div class="editorial-form">
        <?php $form = ActiveForm::begin(['id' => 'video_form', 'options' => ['enctype'=>'multipart/form-data']]); ?>
    
        <?= $form->field($model, 'e_title')->textInput(['maxlength' => true])->label("Title <span class='valid-star-color' >*</span> ") ?>

        <p>
            <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                'preset' => 'basic',
                'clientOptions' => ['height' => 100]
            ]); ?>
            <?php
            if($model->featured_video_url != ''){ 
                $url = $model->featured_video_url; 
                if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                    $domain = parse_url($url, PHP_URL_HOST);
                    $pos    =   strpos($domain, 'simplywishes');

                    if($pos == false)
                    {
                        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                        if(isset($match[1]))
                        {
                            $youtube_id = $match[1];
                            echo "<div style='width: 60%;'><div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://www.youtube.com/embed/".$youtube_id."' controls></iframe></div></div>";

                        }
                        else
                            echo "<div style='width: 60%;'><div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='".$url."' controls></iframe></div></div>";
                    }
                    else
                        echo "<div style='width: 70%'><video poster='".Yii::$app->homeUrl."web/".$model->e_image."' controls autoplay><source src=".$url." type='video/mp4'>Your browser does not support the video tag.</video></div>";
                } else {
                    echo $url;
                }
            } ?>
        </p>
        <div class="row">
            <div class="col-md-5">
                <p>Insert youtube video url in order to upload</p>
                <?= $form->field($model, 'featured_video_url')->textInput(['class'=>'form-control','onChange'=>'saveVideoUrl(this)','id'=>'video_url',  'placeholder' => 'http:// or https://'])->label(false) ?>
            </div>
            <div class="col-md-1"> ( Or ) </div>
            <div class="col-md-6">
                <p>Upload from your Files</p>
                <?php echo $form->field($model, 'featured_video_upload')->fileInput(['onChange'=>'saveFile(this)','class'=>'form-control'])->label(false) ?>
            </div>
        </div>
             <?php if($model->isNewRecord){ 
                echo $form->field($model, 'e_image')->fileInput(['class'=>'form-control'])->label("Upload a Thumbnail Image for your Video <span class='valid-star-color' >*</span>");
            }
            else if($model->e_image != '' ){ ?>	
                <div class="form-group field-company-logo" >
                    <label class="control-label" >Image</label>
                    <img id='imagesorce' src="<?=Yii::$app->homeUrl.'web/'.$model->e_image ?>" height="100px" />
                    <a style="cursor:pointer" class="removelogo" >Change Image</a>
                </div>	
                <?php 	
                echo $form->field($model, 'e_image')->fileInput(['class'=>'form-control','style'=>'display:none'])->label(false);
            } else { 	
                echo $form->field($model, 'e_image')->fileInput(['class'=>'form-control']);
            } ?>
        <?= $form->field($model, 'is_video_only')->hiddenInput(['value' => 1])->label(false); ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<?php } ?>
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<script type="text/javascript" >
$(".removelogo").click(function() {	
    $( "#editorial-e_image" ).click();
});
$(".removearticleimage").click(function() {
    $( "#editorial-articleimage" ).click();
});
$('#editorial-e_image').change( function(event) {
    var tmppath = URL.createObjectURL(event.target.files[0]);
    $("#imagesorce").fadeIn("fast").attr('src',tmppath);
});
$('#editorial-articleimage').change( function(event) {
    var tmppath1 = URL.createObjectURL(event.target.files[0]);
    $("#imagesorce1").fadeIn("fast").attr('src',tmppath1);
});
<?php 
    if($error == 1)
    { 
        if($tab == 1)
            $id_val =   '#video_form';
        else
            $id_val =   '#article_form'; ?>
        $('<?= $id_val ?> .field-video_url').addClass('has-error');
        $('<?= $id_val ?> #video_url').siblings('.help-block').text('<?php echo $msg ?>').show();
    <?php }
?>
    
$( "#article_form" ).on('beforeSubmit', function( event ) {
    var check = false;
    
    if( ! $('#article_form #editorial-e_text').val())
    {
        $('.field-editorial-e_text').addClass('has-error');
        $('#editorial-e_text').siblings('.help-block').text('Text cannot be blank').show();
        check = true;
    }
    
    if($('#article_form #video_url').val())
    {
        var host    =   extractHostname($('#article_form #video_url').val());
        
        if( ! isUrlValid($('#article_form #video_url').val()))
        {
            $('#article_form .field-video_url').addClass('has-error');
            $('#article_form #video_url').siblings('.help-block').text('Invalid Url').show();
            check = true;
        }
        else if(host != 'www.youtube.com' && host != 'youtu.be' && host != 'test.simplywishes.com' && host != 'simplywishes.com')
        {
            $('#article_form .field-video_url').addClass('has-error');
            $('#article_form #video_url').siblings('.help-block').text('Insert only the Youtube url').show();
            check = true;
        }
    }
    
    if($('#article_form #editorial-featured_video_upload').val())
    {
        if(! $('#article_form #editorial-e_image').val() && ! $('#article_form #editorial-e_image').attr('value'))
        {
            $('#article_form .field-editorial-e_image').addClass('has-error');
            $('#article_form #editorial-e_image').siblings('.help-block').text('Please upload the thumbnail image for video').show();
            check = true;
        }  
    }
    
    if($('#article_form #editorial-e_image').val())
    {
        if( ! $('#article_form #video_url').val() && ! $('#article_form #editorial-featured_video_upload').val())
        {
            $('#article_form .field-video_url').addClass('has-error');
            $('#article_form .field-editorial-featured_video_upload').addClass('has-error');
            $('#article_form #video_url').siblings('.help-block').text('Please upload the video').show();
            check = true;
        }
    }
    
    if(check === true)
    {
        return false;
    }
    else
        $('#overlay').show();
});
$( "#video_form" ).on('beforeSubmit', function( event ) {
    var check = false;
    
    if($('#video_form #video_url').val())
    {
        var host    =   extractHostname($('#video_form #video_url').val());
        
        if( ! isUrlValid($('#video_form #video_url').val()))
        {
            $('#video_form .field-video_url').addClass('has-error');
            $('#video_form #video_url').siblings('.help-block').text('Invalid Url').show();
            check = true;
        }
        else if(host != 'www.youtube.com' && host != 'youtu.be' && host != 'test.simplywishes.com' && host != 'simplywishes.com')
        {
            $('#video_form .field-video_url').addClass('has-error');
            $('#video_form #video_url').siblings('.help-block').text('Insert only the Youtube url').show();
            check = true;
        }
    }
    
    if( ! $('#video_form #video_url').val() && ! $('#video_form #editorial-featured_video_upload').val())
    {
        $('#video_form .field-video_url').addClass('has-error');
        $('#video_form .field-editorial-featured_video_upload').addClass('has-error');
        $('#video_form #video_url').siblings('.help-block').text('Please upload the video').show();
        check = true;
    }
    
    if(! $('#video_form #editorial-e_image').val() && ! $('#video_form #editorial-e_image').attr('value'))
    {
        $('#video_form .field-editorial-e_image').addClass('has-error');
        $('#video_form #editorial-e_image').siblings('.help-block').text('Please upload the thumbnail image for video').show();
        check = true;
    }
    
    if(check === true)
    {
        return false;
    }
    else
        $('#overlay').show();
});

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}
function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

function saveFile(input){
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
    file = input.files[0];
    var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();	
    
    if(file != undefined){
        waitingDialog.show('Uploading..');
        formData= new FormData();
        if(ext == "mp4" || ext == "mov" || ext == "wmv"){
            formData.append("media", file); 
            $.ajax({
                url: "<?=Url::to(['editorial/upload'])?>",
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                success: function(data){
                    waitingDialog.hide();
                    var id = $(input).parents('form').attr('id');
                    //$(input).attr('src', data);
                    $('#'+id+' #video_url').val(data);
                },
                error: function(){
                    waitingDialog.hide();
                }
            }); 
        }else{
            alert("Extension not supported");
            waitingDialog.hide();
            return false;
        } 
    } else {
        alert("file Input Error");
    }
}

function saveVideoUrl(input){
    var url = $(input).val();
    
    if(url !== '' && isUrlValid(url))
    {
        waitingDialog.show('Fetching..');
        $.ajax({
            url: "<?=Url::to(['editorial/embed'])?>?url="+url,
            type: "GET",
            processData: false,
            contentType: false,
            success: function(data){
                waitingDialog.hide();
                var data    =   JSON.parse(data);
                if(data.error === 1)
                {
                    var id = $(input).parents('form').attr('id');
                    $('#'+id+' .field-video_url').addClass('has-error');
                    $('#'+id+' #video_url').siblings('.help-block').text(data.msg).show();
                }
            },
            error:function(data){
                var data    =   JSON.parse(data);
                var id = $(input).parents('form').attr('id');
                $('#'+id+' .field-video_url').addClass('has-error');
                $('#'+id+' #video_url').siblings('.help-block').text(data.msg).show();
                waitingDialog.hide();
            } 
        });		
    }
}
</script>

