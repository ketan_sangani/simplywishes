<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\UserProfile;
use app\controllers\Utils;
use app\models\ReportContent;
?>
<div class="row" style="margin-top: 50px;">
    <?php echo $this->render('_profilenew', ['user' => $user, 'profile' => $profile]) ?>
    <div class="col-md-6 main-container" style="padding:0;">
        <div class="messaging">
            <div class="inbox_msg row-cols-2">
                <div class="col-md-6 user-list-name-container" style="padding: 0px;">
                    <div class="inbox_people" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                        <div class="headind_srch row row-cols-3" style="overflow: visible;display: flex;justify-content: space-between;">
                            <div class="recent_heading col-md-9">
                                <h4>Messaging</h4>
                            </div>
                            <div>
                            <div class="btn-group dropend" style="padding:0;">
                                <div>
                                    <span type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </span>
                                    <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                                        <span>Messaging</span>
                                        <div class="form-check" style="margin-left: 10px;">
                                            <input class="form-check-input dropdown-item" type="radio" name="massage" id="flexRadioDefault1" onchange="userSelection(this)" value="Connections">
                                            <label class="form-check-label" for="flexRadioDefault1">
                                                Friend Only
                                            </label>
                                        </div>
                                        <div class="form-check" style="margin-left: 10px;">
                                            <input class="form-check-input dropdown-item" type="radio" name="massage" id="flexRadioDefault2" checked onchange="userSelection(this)" value="Everyone">
                                            <label class="form-check-label" for="flexRadioDefault2">
                                                Everyone
                                            </label>
                                        </div>
                                        <!-- <div class="form-check form-switch">
                                            <label class="form-check-label" for="switchBtn">Toggle Switch</label>
                                            <input class="form-check-input" type="checkbox" role="switch" id="switchBtn">
                                        </div> -->
                                    </div>
                                </div>
                                </div>
                                <div class="btn-group dropend" style="padding:0;">
                                    <div>
                                        <span type="button" class="btn btn-secondary edit-dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="fas fa-edit"></i>
                                        </span>
                                        <div class="dropdown-menu dropdown-menu-end all-users-list-dropdown">
                                            <div class="search-container-all-users">
                                                <div style="display: flex;">
                                                <button type="submit" id="search-button">
                                                    <i class="fa fa-search" aria-hidden="true"></i>
                                                </button>
                                                <input type="text" id="search-input" class="search-user" placeholder="Search a user">
                                                </div>
                                            </div>
                                            <div id="usersList">
                                                
                                            </div>
                                            <div id="next-button-container"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="search-container message-search-container justify-content-center">
                            <div>
                                <button type="submit" id="search-button">
                                    <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                                <input type="text" id="search-input" class="search-message" placeholder="Search User">
                            </div>
                            <div>
                                <span type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-sliders" aria-hidden="true"></i>
                                </span>
                                <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                                
                                    <div class="form-check" style="margin-left: 10px;">
                                        <input class="form-check-input dropdown-item" id="all-msg" type="radio" name="filter" value="all" onchange="chatFilter(this)" checked>
                                        <label class="form-check-label" for="all-msg">
                                            All Messages
                                        </label>
                                    </div>
                                    <div class="form-check" style="margin-left: 10px;">
                                        <input class="form-check-input dropdown-item" id="read-msg" type="radio" value="read" name="filter" onchange="chatFilter(this)">
                                        <label class="form-check-label" for="read-msg">
                                            Read
                                        </label>
                                    </div>
                                    <div class="form-check" style="margin-left: 10px;">
                                        <input class="form-check-input dropdown-item" id="unread-msg" type="radio"  value="unread" name="filter" onchange="chatFilter(this)">
                                        <label class="form-check-label" for="unread-msg">
                                            Unread
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inbox_chat">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-6 user-msg-chat-container">
                    <div class="inbox_people" id="userChatBox" style="width: 100%; padding-left: 0px; padding-right: 0px;display: none;overflow: visible;">
                        <div class="headind_srch row row-cols-3" id="rightChatDiv" style="overflow: visible;display: flex;justify-content: space-between;">
                            <div class="recent_heading col-md-8" style="width:100%;">
                                <h4 id="userNameChatBox"></h4>
                                <div class="dropend">
                                <span type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                                <!-- <div class="form-check" style="margin-left: 10px;">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        Mute Notifications
                                    </label>
                                </div> -->
                                <div class="form-check" style="margin-left: 10px;">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                    <label style="cursor: pointer;" class="form-check-label" for="flexRadioDefault2" onclick="clearChat()">
                                        Clear chat
                                    </label>
                                </div>
                                <div class="form-check" style="margin-left: 10px;">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                    <label style="cursor: pointer;" class="form-check-label" for="flexRadioDefault1" onclick="deleteChat()">
                                        Delete chat
                                    </label>
                                </div>
                                <div class="form-check" style="margin-left: 10px;">
                                    <label style="cursor: pointer;" class="form-check-label" for="flexRadioDefault2" onclick="reportUser()">
                                        Report
                                    </label>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mesgs" style="width: 100%; padding-left: 0px; padding-right: 0px;">
                        <div class="msg_history" id="msg_history" style="scroll-behavior:smooth;">
                            
                        </div>
                        <div class="type_msg" id="type_msg">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-3 user-profile-container" style="margin-bottom: 15px; width: 23%;">
        <div class="card shadow-sm" id="userProfileDiv">
            <div class="card-header bg-transparent text-center mt-3">
                <img class="rounded-circle" id="userProfileImg" src="https://media.istockphoto.com/id/1311084168/photo/overjoyed-pretty-asian-woman-look-at-camera-with-sincere-laughter.jpg?s=612x612&amp;w=0&amp;k=20&amp;c=akS4eKR3suhoP9cuk7_7ZVZrLuMMG0IgOQvQ5JiRmAg=" alt="" style="
                    height: 200px;
                    width: 200px;
                    border-radius: 50%;
                    object-fit: cover;
                ">
            </div>
            <div class="card-body text-center">
                <h4 id="userProfileName">San Johns</h4>
                <a id="userProfileUrl" href="" class="btn btn-primary rounded-pill">View Profile</a>
            </div>
        </div>
    </div>
</div>

<script src="https://momentjs.com/downloads/moment.min.js"></script>

<script>
    $("#userProfileDiv").hide();
    var profileImageUrl = '<?php echo \Yii::$app->homeUrl.'web/uploads/users/' ?>';
    var baseUrl = '<?php echo \Yii::$app->homeUrl?>';
    var reportMessageUrl = '<?=Url::to(['account/report-message'])?>';
    const userid = "<?= $user->sendbird_user_id ?>";
    window.userId = userid;
    let page = 1;
    let maxPage;

    let senderUserImg = '<?php echo \Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image ?>';
    window.senderUserImg = senderUserImg;
    
    let defaultSelection = 'Everyone';
    const usersList = document.getElementById("usersList");
    const nextBtnContainer = document.getElementById('next-button-container');

    selection();

    let allUsers;

    const userSelection = (e) => {
        const value = e.value;
        defaultSelection = value;
        selection();
    }

    async function selection() {
        if(defaultSelection === "Everyone"){
            const users = await fetchUsers(page);
            allUsers = users.data;
            if(allUsers && allUsers.length){
                appendUsers(users.data)
            } else {
                usersList.innerHTML = "<h4>No users found</h4>";
                nextBtnContainer.innerHTML = "";
            }
        } else {
            const users = await fetchConnections();
            allUsers = users.data;
            if(allUsers && allUsers.length){
                appendUsers(users.data)
            } else {
                usersList.innerHTML = "<h4>No users found</h4>";
                nextBtnContainer.innerHTML = "";
            }
        }
    }

    async function fetchUsers(page) {
        const formData = new FormData();

        formData.append("type","Everyone")
        formData.append("page",page)
        
        const response = await fetch('<?=Url::to(['account/chat-users'])?>', {
            method: "POST",
            body: formData
        });

        const data = await response.json();
        maxPage = data.totalpages;
        return data;
    }

    async function fetchConnections() {
        const formData = new FormData();

        formData.append("type","Connections")
        formData.append("page",page)
        
        const response = await fetch('<?=Url::to(['account/chat-users'])?>', {
            method: "POST",
            body: formData
        });

        const data = await response.json();
        maxPage = data.totalpages;
        return data;
    } 

    function appendUsers(users){
        usersList.innerHTML = "";
        users.forEach((user) => {
            if (user.sendbird_user_id !== userid) {
            usersList.insertAdjacentHTML(
                "beforeend",
                `
            <div style="margin-top: 8px; background-color: #f6f6f6;cursor: pointer;" class="userListName" id="${
                user.sendbird_user_id
            }">
                <div class="user-container">
                    <div class="chat_img" >
                    <img id="userImg-${
                        user.sendbird_user_id
                        }" src="${user.profile_image !== '' ? profileImageUrl+user.profile_image : profileImageUrl+'web/uploads/users/images/default_profile.png'}">
                        </div>
                        <div class="chat_ib">
                        <h4 id="userName-${
                            user.sendbird_user_id
                        }" data-id="${user.id}">${user.firstname !== "" ? user.firstname + " " + user.lastname : "Unknown"}</h4>
                    </div>
                </div>
            </div>
            `
            );
            }
        });

        const next_button = document.getElementById("next_button");
        if(next_button){
            next_button.remove();
        }
        const previous_button = document.getElementById("previous_button");
        if(previous_button){
            previous_button.remove();
        }

        if(page < maxPage){
            appendNextButton();
        }
        if(page > 1){
            appendPreviousButton();
        }
    }

    function appendNextButton(){
        if(usersList.children && usersList.children.length) {
            const div = document.createElement('div');
            div.id = "next_button";
            div.innerHTML = `
            <button class="btn btn-success d-flex">
                Next <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </button>
            `;
            nextBtnContainer.appendChild(div);

            const nextButton = document.getElementById("next_button");

            nextButton.addEventListener('click',async (e)=>{
                e.stopPropagation();
                page = page + 1;
                
                const users = await fetchUsers(page)
                allUsers = users.data;
                appendUsers(users.data)
            })
        }
    }

    function appendPreviousButton(){
        const div = document.createElement('div');
        div.id = "previous_button";
        div.innerHTML = `
        <button class="btn btn-success d-flex">
            <i class="fa fa-chevron-left" aria-hidden="true"></i> Previous
        </button>
        `;
        nextBtnContainer.insertBefore(div, nextBtnContainer.firstChild);

        const previousButton = document.getElementById("previous_button");

        previousButton.addEventListener('click',async (e)=>{
            e.stopPropagation();
            page = page - 1;
            
            const users = await fetchUsers(page)
            allUsers = users.data;
            appendUsers(users.data)
        })
    }

    const searchUserElem = document.querySelector(".search-user");

    if (searchUserElem) {
    searchUserElem.addEventListener("keyup", (event) => {
        const users = allUsers;

        setTimeout(() => {
        usersList.innerHTML = "";
        const filterUsers = users.filter((user) => {
            return user.firstname.toLowerCase().includes(event.target.value) || user.lastname.toLowerCase().includes(event.target.value);
        });
        appendUsers(filterUsers)
        }, 1000);
    });
    }
    

</script>
<!-- <script type="module" src="../../views/account/sendbird/index.js"></script> -->
<script src="<?= Yii::$app->homeUrl?>web/src/sendbird/index.js" type="module" charset="utf-8"></script>

<script>    
    $("#sendmessage").on("click", function() {
        $("#addusers").val("");
    });

    $("#select_all").click(function() {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });

    $(".send-msgOne").on("click", function() {
        var send_to = $("#senduserid").val();
        if ($.trim(send_to) === "") {
            alert("Please Select To Sender Name.");
            return false;

        }
        var subject = $('#subject').val();
        if ($.trim(subject) === "") {
            alert("Please check the subject.");
            return false;

        }
        var msg = $('#msgOne').val();
        if ($.trim(msg) === "") {
            alert("Please check the message.");
            return false;

        }

        ///var send_to = $("#senduserid").val();
        var send_from = "<?= \Yii::$app->user->id ?>";
        $.ajax({
            url: '<?= Url::to(['account/send-message-inbox']) ?>',
            type: 'POST',
            data: {
                subject: subject,
                msg: msg,
                send_from: send_from,
                send_to: send_to,
                send_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            },
            success: function(response) {
                location.reload();
            }
        });
    });



    $("#multi_delete").click(function() {
        var msg_id = $.map($('input[name="selection[]"]:checked'), function(c) {
            return c.value;
        })
        if ($.trim(msg_id) === "") {
            alert("Please select a checkbox for the message you want to delete.");
            return false;
        }
        var r = confirm("Are you sure you want to delete?");
        if (r == true) {
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/multi-delete-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id,
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    });

    $(document).ready(function() {
        $('#action_menu_btn').click(function() {
            $('.action_menu').toggle();
        });
    });
    $(".remove_delete").click(function() {
        var r = confirm("Are you sure you want to delete?");
        if (r == true) {
            var msg_id = $(this).attr("for");
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/delete-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id,
                },
                success: function(data) {
                    $("#li_list_" + msg_id).hide();
                }
            });
        }
    });

    $(".newmsg").click(function() {
        var flag = $(this).data('flag');
        var _this = $(this);
        if (flag == 0) {
            var msg_id = $(this).attr("for");
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/read-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id
                },
                success: function(data) {
                    $("#read_" + msg_id).parents('li').removeClass("unread");
                    $("#read_" + msg_id).parents('li').addClass("readedmsg");
                    _this.data('flag', 1);
                    _this.removeClass('newmsg');
                    //	$("#readicon_"+msg_id).remove();
                    if ($('.inboxNotification').html()) {
                        $('.inboxNotification, .notification').text('');
                        var num = parseInt($('.inboxNotification').text()) - 1;
                        if (num >= 1)
                            $('.inboxNotification, .notification').text(num);
                        else
                            $('.inboxNotification, .notification').remove();
                    }
                }
            });
        }
    });
    $(".shareIcons").jsSocials({
        showLabel: false,
        showCount: true,
        shares: ["facebook", "twitter", "linkedin"]
    });
    $(".smp_expand").on("click", function() {
        $(this).next().slideToggle(200);

        // $(this).parent().siblings().children().next().slideUp();   Arivazahgan test
        //$('li').removeClass('active');
        //$(this).parent('li').addClass('active');
    });

    $('.msg-close').on("click", function() {
        $(this).parent('.collapse.detail').slideToggle(200);
    });

    $('.msg-report').on("click", function() {
        console.log('report');
        var reportMsg = $(this);
        if (reportMsg.hasClass("disabled")) {
            return false;
        }
        var check = confirm(" Are you sure you want to report this message? "); //Reporting this wish will hide this and any other wishes from this author as well as the author’s profile from you and will trigger a full review by SimplyWishes.");
        if (check) {
            console.log("report");
            var msg_id = $(this).attr("for");
            var comment = $(this).attr("comment");
            var sender = $(this).attr("sender");
            console.log(sender);
            console.log(comment);
            $.ajax({
                url: '<?= Url::to(['wish/report-content']) ?>',
                type: 'GET',
                data: {
                    w_id: msg_id,
                    content_type: 'message',
                    report_user: <?= Yii::$app->user->id ?>,
                    reported_user: sender,
                    comment: comment,
                    content_id: msg_id,
                },
                success: function(data) {

                    if (data == "savedok") {
                        reportMsg.addClass("disabled");
                        // window.history.back();
                    }
                }
            });
        }
    });
    // $(window).on('load', function() {
    //     // Handler for .ready() called.
    //     $('html, body').animate({
    //         scrollTop: $('#scroll-evt').offset().top - 100
    //     }, 'slow');
    // });

    //});
</script>