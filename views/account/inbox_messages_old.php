<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\UserProfile;
?>	

<?php echo $this->render('_profilenew',['user'=>$user,'profile'=>$profile])?>
<div class="col-md-8" id="scroll-evt">
    <div class="marginPos">
    <ul class="nav nav-tabs smp-mg-bottom" role="tablist">
        <li role="presentation" class="active">
            <a>Inbox</a>
        </li>
        <li role="presentation" >
           <a href="<?=\Yii::$app->homeUrl?>account/sent-message" role="tab" >Sent Mail</a>
         </li> 
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active grid" id="inboxmailtab">
            <li class="mainTab">
                <input type="checkbox" id="select_all" style="margin-right:10px"  ></input>My Conversations	
                <div class="pull-right">
                    <a href="#messagemodalOne" id="sendmessage" class="btn btn-primary" data-toggle="modal">
                        Compose
                    </a>
                    <a href="#" class="btn btn-danger" id="multi_delete" >Delete</a>
                </div>
            </li>
            <?php 
            $current_user = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();

            if(! empty($messages))
            { ?>
            <div class="message">
                <ul class="list-group msg-blk">
                    <?php
                        foreach($messages as $key=>$msg)
                        {
                            $reply="";
                            if(isset($msg['threads']) && !empty($msg['threads']))
                            {		
                                $profile = Userprofile::find()->where(['user_id'=>$msg['recipient_id']])->one();					
                            }else{
                                $profile = Userprofile::find()->where(['user_id'=>$msg['sender_id']])->one();
                            }

                            if($msg['read_text'] == 0 && ($msg['reply_recipient_id'] == \Yii::$app->user->id ||$msg['reply_recipient_id'] == 0)) 
                                $color_var = "unread";
                            else 
                                $color_var = "readedmsg";

                            echo '<li class="list-group-item '.$color_var.' "  id="li_list_'.$msg['m_id'].'" >
                                    <input type="checkbox" class="checkBoxClass" name="selection[]" value="'.$msg['m_id'].'" ></input>	
                                    <span style="cursor:pointer" class="pull-right remove_delete" title="Remove"  for="'.$msg['m_id'].'"><i class="glyphicon glyphicon-trash" aria-hidden="true"> </i></span>

                                    <div class="list-icon">
                                        <a href="'.Url::to(['profile','id'=>$profile->user_id]).'" target="_blank" ><img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'" alt=""></a>
                                    </div>

                                    <div class="smp_expand pointerclass" data-toggle="collapse" title="Click here To View Conversation" >';

                            if($msg['read_text'] == 0 && ($msg['reply_recipient_id'] == \Yii::$app->user->id || $msg['reply_recipient_id'] == 0)) 
                            {
                                echo '<div id="read_'.$msg['m_id'].'" data-flag="0" class="list-group-item-heading newmsg" for="'.$msg['m_id'].'" ><p class="Mname">'.$reply.$profile->fullname.'</p> <span id="readicon_'.$msg['m_id'].'" >- '.substr($msg['text'],0,10).'</span></div>';
                            }
                            else 
                            {
                                echo '<div class="list-group-item-heading" ><p class="Mname">'.$reply.$profile->fullname.'</p><span>- '.substr($msg['text'],0,10).'</span></div>';
                            }

                            echo '<p class="list-group-item-text">
                                    <span class="pull-right">'.date("F j Y g:i a",strtotime($msg['created_at'])).'</span></p>
                            </div>
                            <ul class="collapse detail">';							
                                    echo '<span style="margin: 0px;" class="glyphicon glyphicon-remove pull-right msg-close"></span><li class="media">						  
                                      <div>						
                                            <p class="list-group-item-text">'.nl2br(htmlspecialchars($msg['text'])).'</p>							
                                      </div>
                                    </li>';

                            if(isset($msg['threads']) && !empty($msg['threads']))
                            {						
                                    //arsort($msg['threads']);
                                    foreach($msg['threads'] as $key2=>$thread){
                                            $profile = Userprofile::find()->where(['user_id'=>$thread['send_by']])->one();
                                            echo '<li class="media">
                                              <div class="media-left list-icon">
                                                     <img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'" alt="">
                                              </div>
                                              <div>
                                                    <h6 class="media-heading">'.$profile->fullname.'</h6>
                                                    <p class="list-group-item-text">'.nl2br(htmlspecialchars($thread['text'])).'<span class="label pull-right">'.date("F j Y g:i a",strtotime($thread['created_at'])).'</span></p>

                                              </div>
                                            </li>';
                                    } 	 
                            }
                            
                            echo '<li class="media media_textbox">
                                        <label for="message">Enter Your Message</label>
                                        <textarea id="'.$key.'_msg" class="form-control" rows="2" style="width:85%;display: inline-block;"></textarea>
                                        <button type="button" id="rpy_'.$msg['m_id'].'" data-send_to="'.$msg['recipient_id'].'" data-msg_id ="'.$msg['m_id'].'" class="send-msg btn btn-primary pull-right">Reply</button>
                                    </li>';

                        echo '</ul></li>';
                    } ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="sentmailtab"> </div>
    </div>
</div>
</div>
</div>

<!-- modal Starts -->
<div class="modal fade" id="messagemodalOne"  tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
    <form id='project-form'>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Message</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <label>To <span class="valid-star-color" >*</span> </label>
                    <?php 
                    $data =  Userprofile::find()->select(['CONCAT(firstname," ",lastname) as value', 'CONCAT(firstname," ",lastname) as  label', 'lastname as desc','user_id as id'])->where(['!=','user_id',\Yii::$app->user->id])->asArray()->all();	

                    echo AutoComplete::widget([
                        'name' => 'adduser',
                        'id' => 'adduser',
                        'options' => ['class' => 'form-control','placeholder'=>'Search Name'],						
                        'clientOptions' => [
                            'appendTo'=>'#project-form',
                            'source' => new JsExpression("function( request, response ) {
                                var matcher = new RegExp( '^' + $.ui.autocomplete.escapeRegex( request.term ), 'i' );
                                response( $.grep( ".json_encode($data).", function( item ){
                                    return matcher.test( item.label ) || matcher.test(item.desc);
                                }) );			
                            }"),
                            'autoFill'=>true,
                            'select' => new JsExpression("function( event, ui ) {
                                $('#senduserid').val(ui.item.id);			
                            }"),
                        ],
                    ]); ?>
                    <input type="hidden" name="senduserid" id="senduserid" />
                </div>
                </br>
                <div class="form-group">
                    <label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
                    <textarea id="msgOne" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="send-msgOne btn btn-primary">Send</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </form>
    </div>
</div>
<!-- modal Ends -->
<script>
    $("#sendmessage").on("click",function(){
            $("#addusers").val("");
    });

    $("#select_all").click(function () {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });
	
		$(".send-msgOne").on("click",function(){
			var send_to = $("#senduserid").val();
			if($.trim(send_to) === "")
			{
				alert("Please Select To Sender Name.");
				return false;
				
			}
			var msg = $('#msgOne').val();
			if($.trim(msg) === "")
			{
				alert("Please check the message.");
				return false;
				
			}
			
			///var send_to = $("#senduserid").val();
			var send_from = "<?=\Yii::$app->user->id?>";
			$.ajax({
				url : '<?=Url::to(['account/send-message-inbox'])?>',
				type : 'POST',
				data : {msg:msg,send_from:send_from,send_to:send_to},
				success: function(response){
					location.reload();
				}
			});
		});
	
	
	
		$("#multi_delete").click(function(){				 
		    var r = confirm("Are you Sure To Delete!");
		    if (r == true) {
				var msg_id = $.map($('input[name="selection[]"]:checked'), function(c){return c.value; })
				if($.trim(msg_id) === "")
				 {
					alert("Please Select the Checkbox to Delete.");
					return false;
				  }				
				 $.ajax({
				   url: '<?=Yii::$app->homeUrl."account/multi-delete-inbox-message"?>',
				   type: 'POST',
				   data: {  msg_id: msg_id,
				   },
				   success: function(data) {					
						location.reload();
				   }
				 }); 
				}		
			 });
		$(".remove_delete").click(function(){	
		 var r = confirm("Are you Sure To Delete!");
		   if (r == true) {
			 var msg_id = $(this).attr("for");			 
			 $.ajax({
				   url: '<?=Yii::$app->homeUrl."account/delete-inbox-message"?>',
				   type: 'POST',
				   data: {  msg_id: msg_id,
				   },
				   success: function(data) {		
						$("#li_list_"+msg_id).hide();
				   }
				 }); 
		   }				 
		});	
			
		$(".newmsg").click(function(){	
                    var flag    =   $(this).data('flag');
                    var _this    =   $(this);
                    if(flag == 0)
                    {
                        var msg_id = $(this).attr("for");
                        $.ajax({
                            url: '<?=Yii::$app->homeUrl."account/read-inbox-message"?>',
                            type: 'POST',
                            data: {  msg_id: msg_id},
                            success: function(data) {		
                                $("#read_"+msg_id).parents('li').removeClass("unread");	
                                $("#read_"+msg_id).parents('li').addClass("readedmsg");
                                _this.data('flag', 1);
                                _this.removeClass('newmsg');
                                 //	$("#readicon_"+msg_id).remove();
                                if($('.inboxNotification').html())
                                {
                                    var num =   parseInt($('.inboxNotification').text()) - 1;
                                    if(num >= 1)
                                        $('.inboxNotification, .notification').text(num);
                                    else
                                        $('.inboxNotification, .notification').remove();
                                }
                            }
                        }); 	
                    }
		});
		$(".shareIcons").jsSocials({
			showLabel: false,
			showCount: true,
			shares: ["facebook", "twitter","linkedin"]
		});
		$(".smp_expand").on( "click", function() {
			$(this).next().slideToggle(200);
                        
			// $(this).parent().siblings().children().next().slideUp();   Arivazahgan test
			//$('li').removeClass('active');
			//$(this).parent('li').addClass('active');
		});
                
                $('.msg-close').on( "click", function() {
			$(this).parent('.collapse.detail').slideToggle(200);
		});

		$(".send-msg").on("click",function(){		
                    var id = $(this).attr('id');

                    var send_to = $(this).attr('data-send_to');
                    var msg_id = $(this).attr('data-msg_id');
                    var msg = $('#'+msg_id+'_msg').val();
                    if($.trim(msg) === "")
                    {
                        alert("Please check the message.");
                        return false;
                    }
                    var prof_image = "<?=\Yii::$app->homeUrl.'web/uploads/users/'.$current_user->profile_image.'?v='.strtotime('now')?>";
                    var fullname = "<?=$current_user->fullname?>";
                    var elem = $(this);
                    var send_from = "<?=\Yii::$app->user->id?>";

                    $("#"+id).attr('disabled','disabled');

                    $.ajax({
                        url : '<?=Url::to(['account/reply-message'])?>',
                        type : 'POST',
                        data : {msg:msg,send_from:send_from,send_to:send_to,msg_id:msg_id},
                        success: function(response){
                            var data = $.parseJSON(response);
                            if(data.status){
                                $('#'+msg_id+'_msg').val("");
                                var html = '<li class="media"><div class="media-left list-icon"><img  src="'+prof_image+'" alt=""></div><div><h6 class="media-heading">'+fullname+'</h6><p class="list-group-item-text">'+msg+'<span class="label pull-right">Just Now</span></p></div></li>';
                                $( html ).insertAfter( $(elem).parent('li').prev('li'));
                                $("#"+id).removeAttr('disabled');
                            }
                        },
                        error: function (response) {
                            $("#"+id).removeAttr('disabled');
                        }	
                    });	
		});
$(window).on('load', function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
});

	//});
	</script>
