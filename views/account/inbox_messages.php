<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\models\UserProfile;
use app\controllers\Utils;
use app\models\ReportContent;
?>

<?php echo $this->render('_profilenew', ['user' => $user, 'profile' => $profile]) ?>
<div class="col-md-8" id="scroll-evt">
    <div class="marginPos">
        <ul class="nav nav-tabs smp-mg-bottom" role="tablist">
            <li role="presentation" class="active">
                <a>Inbox</a>
            </li>
            <li role="presentation">
                <a href="<?= \Yii::$app->homeUrl ?>account/sent-message" role="tab">Sent Mail</a>
            </li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active grid" id="inboxmailtab">
                <li class="mainTab">
                    <inpu type="checkbox" id="select_all" style="margin-right:10px">My Conversations
                        <div class="pull-right">
                            <a href="#messagemodalOne" id="sendmessage" data-toggle="modal">
                                <button class="btn btn-primary" style="margin-top: 0px !important;">Compose</button>
                            </a>
                            <button class="btn btn-danger" id="multi_delete">Delete</button>
                        </div>
                </li>
                <?php
                $current_user = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                $selectReported = (ReportContent::find()->select('reported_user')->where(
                    [
                        'report_user' => \Yii::$app->user->id
                    ]
                ));
                if (!empty($messages)) { ?>
                    <div class="message">
                        <ul class="list-group msg-blk">
                            <?php

                            foreach ($messages as $key => $msg) {
                                $reply = "";
                                if (isset($msg['threads']) && !empty($msg['threads'])) {
                                    // echo "1".$msg['recipient_id'];
                                    $profile = UserProfile::find()->where(['user_id' => $msg['recipient_id']])->one(); //->where(['not in','user_id', $selectReported])->one();
                                } else {
                                    // echo "2".$msg['sender_id'];
                                    $profile = UserProfile::find()->where(['user_id' => $msg['sender_id']])->one(); //->where(['not in','user_id', $selectReported]);//->one();
                                    // echo   $profile->createCommand()->getRawSql();exit;
                                    // print_r($profile);
                                }
                                // print_r($msg);
                                //print_r($profile);
                                if (!empty($profile)) {
                                    if ($msg['read_text'] == 0 && ($msg['reply_recipient_id'] == \Yii::$app->user->id || $msg['reply_recipient_id'] == 0))
                                        $color_var = "unread";
                                    else
                                        $color_var = "readedmsg";

                                    echo '<li class="list-group-item ' . $color_var . ' "  id="li_list_' . $msg['m_id'] . '"  style="border: 0px solid #383838 !important;" >
                                    <input type="checkbox" style="margin-top: 15px !important;" class="checkBoxClass" name="selection[]" value="' . $msg['m_id'] . '" ></input>	
                                    <span style="cursor:pointer" class="pull-right remove_delete" title="Remove"  for="' . $msg['m_id'] . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"> </i></span>

                                    <div class="list-icon" style="margin-top: 0px !important; margin-right: 5px;">
                                        <a href="' . Url::to(['profile', 'id' => $profile->user_id]) . '" target="_blank" ><img  src="' . \Yii::$app->homeUrl . 'web/uploads/users/' . $profile->profile_image . '?v=' . strtotime('now') . '" alt=""></a>
                                    </div>

                                    <div class="smp_expand pointerclass" data-toggle="collapse" title="Click here To View Conversation" >';
                                    // print_r($msg);
                                    if ($msg['read_text'] == 0 && ($msg['reply_recipient_id'] == \Yii::$app->user->id || $msg['reply_recipient_id'] == 0)) {
                                        if (isset($msg['subject']) && $msg['subject'] != '') {
                                            echo '<div id="read_' . $msg['m_id'] . '" data-flag="0" style="margin-top: 1%;" class="list-group-item-heading newmsg" for="' . $msg['m_id'] . '" ><p style="color: #ffcc33;">' . $reply . $profile->fullname . '<span  style="margin-left: 45px; color:white;" id="readicon_' . $msg['m_id'] . '" > ' . $msg['subject'] . '</span></p> </div>';
                                        } else {
                                            echo '<div id="read_' . $msg['m_id'] . '" data-flag="0" style="margin-top: 1%;" class="list-group-item-heading newmsg" for="' . $msg['m_id'] . '" ><p style="color: #ffcc33;">' . $reply . $profile->fullname . '<span  style="margin-left: 45px; color:white;" id="readicon_' . $msg['m_id'] . '" >-' . Utils::trunc($msg['text'], 4) . '</span></p> </div>';
                                        }
                                    } else {
                                        if (isset($msg['subject']) && $msg['subject'] != '') {
                                            echo '<div class="list-group-item-heading" style="margin-top: 1%;"><p style="color: #ffcc33;">' . $reply . $profile->fullname . '<span style="margin-left: 45px; color:white;">' . $msg['subject'] . '</span></p></div>';
                                        } else {
                                            echo '<div class="list-group-item-heading" style="margin-top: 1%;"><p style="color: #ffcc33;">' . $reply . $profile->fullname . '<span style="margin-left: 45px;color:white;">' . Utils::trunc($msg['text'], 4) . '</span></p></div>';
                                        }
                                    }

                                    echo '<p class="list-group-item-text">
                                    <span class="pull-right">' . date("F j Y g:i a", strtotime($msg['created_at'])) . '</span></p>
                            </div>
                            <ul class="collapse detail">';
                                    $disabled = "";

                                    $isReported = Utils::checkIsReported($msg['m_id'], 'message');
                                    $disabled = ($isReported) ? ' disabled' : '';
                                    // if (\Yii::$app->user->id == $msg['m_id'] ){
                                    //     $disabled = " disabled";
                                    // }
                                    echo '<span style="margin: 0px;" class="glyphicon glyphicon-remove pull-right msg-close"></span>
                                    <span style="padding-right:3px; margin: 0px; cursor:pointer" class="glyphicon glyphicon-flag pull-right ' . $disabled . ' msg-report" sender="' . $msg['sender_id'] . '" comment="' . nl2br(htmlspecialchars($msg['text'])) . '" ' . $disabled . ' for="' . $msg['m_id'] . '"></span>	
                                    <li class="media">		
                                      <div>						
                                            <p class="list-group-item-text">' . nl2br($msg['text']) . '</p>							
                                      </div>
                                    </li>';

                                    if (isset($msg['threads']) && !empty($msg['threads'])) {
                                        //arsort($msg['threads']);
                                        foreach ($msg['threads'] as $key2 => $thread) {
                                            $profile = UserProfile::find()->where(['user_id' => $thread['send_by']])->one();
                                            echo '<li class="media">
                                              <div class="media-left list-icon">
                                                     <img  src="' . \Yii::$app->homeUrl . 'web/uploads/users/' . $profile->profile_image . '?v=' . strtotime('now') . '" alt="">
                                              </div>
                                              <div>
                                                    <h6 class="media-heading">' . $profile->fullname . '</h6>
                                                    <p class="list-group-item-text">' . nl2br(htmlspecialchars($thread['text'])) . '<span class="label pull-right">' . date("F j Y g:i a", strtotime($thread['created_at'])) . '</span></p>

                                              </div>
                                            </li>';
                                        }
                                    }

                                    echo '<li class="media media_textbox">
                                        <label for="message">Enter Your Message</label>
                                        <textarea id="' . $key . '_msg" class="form-control" rows="2" style="width:85%;display: inline-block;"></textarea>
                                        <button type="button" id="rpy_' . $msg['m_id'] . '" data-send_to="' . $msg['recipient_id'] . '" data-msg_id ="' . $msg['m_id'] . '" class="send-msg btn btn-primary pull-right">Reply</button>
                                    </li>';

                                    echo '</ul></li>';
                                }
                            }
                            //exit;
                            ?>
                        </ul>
                    </div>
                <?php } ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="sentmailtab"> </div>
        </div>
    </div>
</div>

<!-- modal Starts -->
<div class="modal fade" id="messagemodalOne" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id='project-form'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body">
                    <div class="media" style="padding-left: 0;">
                        <label>To <span class="valid-star-color">*</span> </label>
                        <?php
                        $selectReported = (ReportContent::find()->select('reported_user')->where(
                            [
                                'report_user' => \Yii::$app->user->id,
                                'report_type' => 'user',
                            ]
                        ));

                        $data =  Userprofile::find()->select(['CONCAT(firstname," ",lastname) as value', 'CONCAT(firstname," ",lastname) as  label', 'lastname as desc', 'user_id as id'])
                            ->where(['!=', 'user_id', \Yii::$app->user->id])
                            ->where(['not in', 'user_id', $selectReported])
                            ->asArray()->all();

                        echo AutoComplete::widget([
                            'name' => 'adduser',
                            'id' => 'adduser',
                            'options' => ['class' => 'form-control', 'placeholder' => 'Search Name'],
                            'clientOptions' => [
                                'appendTo' => '#project-form',
                                'source' => new JsExpression("function( request, response ) {
                                var matcher = new RegExp( '^' + $.ui.autocomplete.escapeRegex( request.term ), 'i' );
                                response( $.grep( " . json_encode($data) . ", function( item ){
                                    return matcher.test( item.label ) || matcher.test(item.desc);
                                }) );			
                            }"),
                                'autoFill' => true,
                                'select' => new JsExpression("function( event, ui ) {
                                $('#senduserid').val(ui.item.id);			
                            }"),
                            ],
                        ]); ?>
                        <input type="hidden" name="senduserid" id="senduserid" />
                    </div>
                    </br>
                    <div class="form-group">
                        <label for="subject">Subject <span class='valid-star-color'>*</span></label>
                        <input type="text" id="subject" class="form-control" rows="4" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Enter Your Message <span class='valid-star-color'>*</span></label>
                        <textarea id="msgOne" class="form-control" rows="4"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="send-msgOne btn btn-primary">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- modal Ends -->
<script src="https://momentjs.com/downloads/moment.min.js"></script>

<script>
    $("#sendmessage").on("click", function() {
        $("#addusers").val("");
    });

    $("#select_all").click(function() {
        $(".checkBoxClass").prop('checked', $(this).prop('checked'));
    });

    $(".send-msgOne").on("click", function() {
        var send_to = $("#senduserid").val();
        if ($.trim(send_to) === "") {
            alert("Please Select To Sender Name.");
            return false;

        }
        var subject = $('#subject').val();
        if ($.trim(subject) === "") {
            alert("Please check the subject.");
            return false;

        }
        var msg = $('#msgOne').val();
        if ($.trim(msg) === "") {
            alert("Please check the message.");
            return false;

        }

        ///var send_to = $("#senduserid").val();
        var send_from = "<?= \Yii::$app->user->id ?>";
        $.ajax({
            url: '<?= Url::to(['account/send-message-inbox']) ?>',
            type: 'POST',
            data: {
                subject: subject,
                msg: msg,
                send_from: send_from,
                send_to: send_to,
                send_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            },
            success: function(response) {
                location.reload();
            }
        });
    });



    $("#multi_delete").click(function() {
        var msg_id = $.map($('input[name="selection[]"]:checked'), function(c) {
            return c.value;
        })
        if ($.trim(msg_id) === "") {
            alert("Please select a checkbox for the message you want to delete.");
            return false;
        }
        var r = confirm("Are you sure you want to delete?");
        if (r == true) {
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/multi-delete-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id,
                },
                success: function(data) {
                    location.reload();
                }
            });
        }
    });
    $(".remove_delete").click(function() {
        var r = confirm("Are you sure you want to delete?");
        if (r == true) {
            var msg_id = $(this).attr("for");
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/delete-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id,
                },
                success: function(data) {
                    $("#li_list_" + msg_id).hide();
                }
            });
        }
    });

    $(".newmsg").click(function() {
        var flag = $(this).data('flag');
        var _this = $(this);
        if (flag == 0) {
            var msg_id = $(this).attr("for");
            $.ajax({
                url: '<?= Yii::$app->homeUrl . "account/read-inbox-message" ?>',
                type: 'POST',
                data: {
                    msg_id: msg_id
                },
                success: function(data) {
                    $("#read_" + msg_id).parents('li').removeClass("unread");
                    $("#read_" + msg_id).parents('li').addClass("readedmsg");
                    _this.data('flag', 1);
                    _this.removeClass('newmsg');
                    //	$("#readicon_"+msg_id).remove();
                    if ($('.inboxNotification').html()) {
                        $('.inboxNotification, .notification').text('');
                        var num = parseInt($('.inboxNotification').text()) - 1;
                        if (num >= 1)
                            $('.inboxNotification, .notification').text(num);
                        else
                            $('.inboxNotification, .notification').remove();
                    }
                }
            });
        }
    });
    $(".shareIcons").jsSocials({
        showLabel: false,
        showCount: true,
        shares: ["facebook", "twitter", "linkedin"]
    });
    $(".smp_expand").on("click", function() {
        $(this).next().slideToggle(200);

        // $(this).parent().siblings().children().next().slideUp();   Arivazahgan test
        //$('li').removeClass('active');
        //$(this).parent('li').addClass('active');
    });

    $('.msg-close').on("click", function() {
        $(this).parent('.collapse.detail').slideToggle(200);
    });

    $('.msg-report').on("click", function() {
        console.log('report');
        var reportMsg = $(this);
        if (reportMsg.hasClass("disabled")) {
            return false;
        }
        var check = confirm(" Are you sure you want to report this message? "); //Reporting this wish will hide this and any other wishes from this author as well as the author’s profile from you and will trigger a full review by SimplyWishes.");
        if (check) {
            console.log("report");
            var msg_id = $(this).attr("for");
            var comment = $(this).attr("comment");
            var sender = $(this).attr("sender");
            console.log(sender);
            console.log(comment);
            $.ajax({
                url: '<?= Url::to(['wish/report-content']) ?>',
                type: 'GET',
                data: {
                    w_id: msg_id,
                    content_type: 'message',
                    report_user: <?= Yii::$app->user->id ?>,
                    reported_user: sender,
                    comment: comment,
                    content_id: msg_id,
                },
                success: function(data) {

                    if (data == "savedok") {
                        reportMsg.addClass("disabled");
                        // window.history.back();
                    }
                }
            });
        }
    });

    $(".send-msg").on("click", function() {
        var id = $(this).attr('id');

        var send_to = $(this).attr('data-send_to');
        var msg_id = $(this).attr('data-msg_id');
        var msg = $('#' + msg_id + '_msg').val();
        if ($.trim(msg) === "") {
            alert("Please check the message.");
            return false;
        }
        var prof_image = "<?= \Yii::$app->homeUrl . 'web/uploads/users/' . $current_user->profile_image . '?v=' . strtotime('now') ?>";
        var fullname = "<?= $current_user->fullname ?>";
        var elem = $(this);
        var send_from = "<?= \Yii::$app->user->id ?>";


        $("#" + id).attr('disabled', 'disabled');

        $.ajax({
            url: '<?= Url::to(['account/reply-message']) ?>',
            type: 'POST',
            data: {
                msg: msg,
                send_from: send_from,
                send_to: send_to,
                msg_id: msg_id,
                send_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            },
            success: function(response) {
                var data = $.parseJSON(response);
                if (data.status) {
                    $('#' + msg_id + '_msg').val("");
                    var html = '<li class="media"><div class="media-left list-icon"><img  src="' + prof_image + '" alt=""></div><div><h6 class="media-heading">' + fullname + '</h6><p class="list-group-item-text">' + msg + '<span class="label pull-right">Just Now</span></p></div></li>';
                    $(html).insertAfter($(elem).parent('li').prev('li'));
                    $("#" + id).removeAttr('disabled');
                }
            },
            error: function(response) {
                $("#" + id).removeAttr('disabled');
            }
        });
    });
    $(window).on('load', function() {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#scroll-evt').offset().top - 100
        }, 'slow');
    });

    //});
</script>