<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\UserProfile;
use yii\jui\AutoComplete;
use yii\web\JsExpression;


$this->title = 'My Friends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact" >
		<?php echo $this->render('_profilenew',['user'=>$user,'profile'=>$profile])?>
<div class="col-md-8" id="scroll-evt">
    <h3 class="fnt-skyblue">Friends</h3>
    <ul class="nav nav-tabs smp-mg-bottom" role="tablist">
        <li class="dropdown active" role="presentation">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">List<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?=\Yii::$app->homeUrl?>account/my-friend">My Friends</a></li>
                <li><a href="<?=\Yii::$app->homeUrl?>account/my-friend?type=requests">Requests</a></li>
            </ul>
        </li>
            <li role="presentation">
                    <a href="<?=\Yii::$app->homeUrl?>follow/active" role="tab">Active Wishes & Donations</a>
            </li>
            <li role="presentation">
                    <a href="<?=\Yii::$app->homeUrl?>follow/fulfilled" role="tab">Granted Wishes & Donations</a>
            </li>
            <li role="presentation">
                    <a href="<?=\Yii::$app->homeUrl?>follow/progress" role="tab">In Progress Wishes & Donations</a>
            </li>
    </ul>

   <div class="tab-content smp-mg-bottom">
		
        <div role="tabpanel" class="tab-pane active grid" id="fullfilledwish">

            <div class="input-group col-md-6" style="margin-bottom:10px" >
            <?php 
             $data =  \app\models\Userprofile::find()->select(['CONCAT(firstname," ",lastname) as value'])->where(['!=','user_id',\Yii::$app->user->id])->groupBy('value')->asArray()->all();	

                                    echo AutoComplete::widget([
                                            'name' => 'searh_field',
                                            'id' => 'searh_field',
                                            'options' => ['class' => 'form-control','placeholder'=>'Search for your friends'],
                                            'value'	=> $findfriends,					
                                            'clientOptions' => [
                                                    'source' => $data, 
                                                    'autoFill'=>true,							 
                                                            ],
                                                    ]); 
            ?>					
                            <!--<input name="searh_field" id="searh_field" type="text" class="form-control" value="<?= $findfriends ?>" placeholder="Search for your friends">-->
                            <span class="input-group-btn">
                              <button class="search-wish btn btn-default" type="button">
                                    <span class="glyphicon glyphicon-search"></span>
                              </button>
                            </span>
            </div>
		
            <div id="frienduser" name="frienduser" >
                <?php if( ! $findfriends && $type!='requests')
                {
                    $profileA = UserProfile::find()->where(['user_id'=>1])->one();
                ?>
                    <div class="col-md-6 col-sm-6 col-xs-12 grid-item" id="parent_div_<?= $profileA->user_id; ?>"> 
                        <div class="smp_inline thumbnail friendprofile_img">
                            <?php 
                            if($profileA->profile_image!='') 
                                echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profileA->profile_image.'?v='.strtotime('now').'" class="img-responsive" alt="my-profile-Image">';
                            else 
                                echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png"   class="img-responsive" alt="my-profile-Image">';
                            ?>							
                        </div>
                        <div class="smp_inline_requests">
                            <p><span><?= Html::a($profileA->firstname.' '.$profileA->lastname, Url::to(['account/profile','id'=>$profileA->user_id],true)) ?></span></p>						

                        </div>
                    </div>	 
                <?php } 
                //echo "<pre>";print_r($followlist);exit;
            if(isset($followlist) && !empty($followlist))
            {	
                foreach($followlist as $user)
                {
                    if($findfriends){
                        $userid = $user->user_id;
                    }else {
                        if ($user->requested_to == Yii::$app->user->id)
                            $userid = $user->requested_by;
                        else
                            $userid = $user->requested_to;
                    }



                    $profile = UserProfile::find()->where(['user_id'=>$userid])->one();
                    if(!empty($profile)){
                ?>
                <div class="col-md-6 col-sm-6 col-xs-12 grid-item" id="parent_div_<?= $profile->user_id; ?>">
                    <div class="smp_inline thumbnail friendprofile_img">
                        <?php
                        if($profile->profile_image!='')
                            echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'" class="img-responsive" alt="my-profile-Image">';
                        else
                            echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png"   class="img-responsive" alt="my-profile-Image">';
                        ?>
                    </div>
                    <div class="smp_inline_requests">
                        <p><span><b><?= Html::a($profile->firstname.' '.$profile->lastname, Url::to(['account/profile','id'=>$profile->user_id],true)) ?></b></span></p>
                         <?php
                         if(empty($findfriends))
                         {
                         if ($user->status == 0) {
                                if ($user->f_id == \Yii::$app->user->id){?>
                                    <?= Html::a('Delete Request', ['friend/request-rejectedemail','id'=>$user->f_id], ['class' => 'glyphicon glyphicon-remove btn btn-danger btn-sm']) ?>
                               <?php }else { ?>

                            <span id="frd_<?=$profile->user_id  ?>" class="btn btn-success btn-sm accept-request remove-unfollow" for="<?php echo $user->f_id ?>"> Accept</span>
                                    <a href="<?php echo Url::to(['friend/request-rejectedemail','id'=>$user->f_id]); ?>" class="btn btn-danger btn-sm" > Delete</a>

                                    <?php //Html::a('Delete', ['friend/request-rejectedemail','id'=>$user->f_id], ['class' => 'glyphicon glyphicon-remove btn btn-danger btn-sm']) ?>
                                <?php } ?>
                            <?php }
                         }
                            if(empty($findfriends))
                            {
                                if($user->status==1)
                                {
                            ?>
                            <span id="frd_<?=$profile->user_id  ?>" class="btn btn-danger btn-sm unfollowfrd remove-unfollow" for="<?php echo $user->f_id ?>"> Unfriend</span>

                            <?php }
                            } else {
                                if($profile->user_id != 1)
                                {
                                    if(in_array($userid,$follows))
                                    {
                                     $friend = \app\models\FriendRequest::find()->where("(requested_by = ".$userid." AND requested_to = ".\Yii::$app->user->id.") OR (requested_by = ".\Yii::$app->user->id." AND requested_to = ".$userid.")")->andWhere(['status'=>1])->one();
                                ?>
                                <span id="frd_<?=$profile->user_id  ?>" class="btn btn-danger btn-sm unfollowfrd remove-unfollow" for="<?php echo $friend->f_id ?>"> Unfriend</span>
                                <?php } else { ?>
                                <span id="frd_<?=$profile->user_id  ?>" class="btn btn-success btn-sm followfrd remove-unfollow" for="<?php echo $profile->user_id ?>">  Add Friend</span>
                            <?php }  }} ?>


                            <!-- todoreport -->
                            <a href="#reportusermodalAccept" data-toggle="modal" data-for="<?php echo $profile->user_id ?>"><span id="frd_<?=$profile->user_id  ?>" class="btn btn-warning btn-sm" style="letter-spacing: 1.5px;"> Report</span></a>

                    </div>
                </div>
                <?php }} } else {
                        if($type!='' && $type=='requests'){
                            echo "Sorry, no more friend requests.";
                        }else{
                            echo "Sorry, no more friends.";
                        }

                } ?>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="modal fade" id="reportusermodalAccept" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Report User</h4>
            </div>
            <div class="modal-body">
                <div class="media">
                    <div class="media-left list-icon">

                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">
                        </h4>

                    </div>
                </div>
                </br>
                <div class="form-group">
                    <label for="subject">Please explain why you are reporting this user for violating our terms of service <span class='valid-star-color' >*</span></label>
                    <textarea type="text" id="reason" class="form-control"  rows="4" required></textarea>
                    <input type="hidden" id="profile_id" name="profile_id" value="">
                </div>
                <div class="form-group">
                    <input type="checkbox" class="msg-check" name="i_agree_report" id="i_agree_report" value="1"  required> Are you sure that you want to report this user? * Reporting this user will trigger a full review by SimplyWishes and may cause the user and all relevant content to be removed from our site. You can also unfriend this person.   </input>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="reportuserform btn btn-primary">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#reportusermodalAccept').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var recipient = button.data('for'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('.modal-body #profile_id').val(recipient);
    });
$(document).ready(function(){
	$(".unfollowfrd").click(function(){
		var check = confirm( "Are sure to Unfriend from the List? ");
		if(check)
		{
			var request_id = $(this).attr("for");
			console.log(request_id);
			$.ajax({
				url : '<?=Url::to(['friend/cancel-friend'])?>',
				type : 'POST',
				data : {requestid:request_id},
				success: function(response){

					if(response == true)
					{					   					  
					   $("#parent_div_"+request_id).remove();					
					}
                    window.location.reload();
				}
			});
			
			
		} else {
			return false;
		}
	});
});

$(document).ready(function(){
    $(".accept-request").click(function(){
        var check = confirm( "Are sure to Accept This Request? ");
        if(check)
        {
            var request_id = $(this).attr("for");
            console.log(request_id);
            $.ajax({
                url : '<?=Url::to(['friend/accept-friend'])?>',
                type : 'POST',
                data : {requestid:request_id},
                success: function(response){
                    var res = $.parseJSON(response);
                    if(res.status=='1'){
                        alert('You have accepted friend request successfully.');
                    }else{
                        alert('Something went wrong.Please try after sometimes.');
                    }
                    window.location.reload();
                }
            });


        } else {
            return false;
        }
    });
});

	$(".followfrd").click(function(){		
		var send_from = <?= \Yii::$app->user->id ?>;		
		var send_to = $(this).attr("for");		
		var frd_id = $(this).attr("id");		
		$.ajax({
				url : '<?=Url::to(['follow/follow-request'])?>',
				type : 'POST',
				data : {
					send_from:send_from,
					send_to:send_to,
					},
				success: function(response){					
					if(response == 'follow')
					{
						$("#"+frd_id).removeClass("btn-success");
						$("#"+frd_id).addClass("btn-danger");
						$("#"+frd_id).html(" Unfriend ");
					}
					else if(response == 'unfollow')
					{
						$("#"+frd_id).removeClass("btn-danger");
						$("#"+frd_id).addClass("btn-success");
						$("#"+frd_id).html("<i class='fa fa-check fa-lg'></i> Add Friend ");
					}
				}
		}); 
    });
    
    $('.reportuserform').on('click', function(){
        var reason = $('#reason').val();
        if($.trim(reason) === "")
        {
            alert("Please check the reason.");
            return false;

        }
        if(!$('#i_agree_report').is(":checked")){
            alert("Please check checbox of confirmation.");
            return false;
        }

            console.log("report");
            var user_id = $('#profile_id').val();
            $.ajax({
                url : '<?=Url::to(['wish/report-content'])?>',
                type: 'GET',
                data: {
                    w_id:0,
                    content_type:'user',
                    report_user:<?=Yii::$app->user->id?>,
                    reported_user:user_id,
                    comment:reason,
                },
                success:function(data){

                    if (data == "savedok"){
                        var url = "<?=Url::to(['account/my-friend'])?>";
                        window.location.href = url;
                    }
                }
            });

    });

	//search srcipt
	$(".search-wish").on("click",function(){				
			var url = "<?=Url::to(['account/my-friend'])?>";
			window.location.href = url+"?findfriends="+$("input[name=searh_field]").val();					
	});
	
	$('#searh_field').keypress(function (e) {
		   if (e.which == 13) {
				$( ".search-wish" ).trigger( "click" );
		  } 
	});
	$(window).on('load', function () {
            $('html, body').animate({
                scrollTop: $('#scroll-evt').offset().top - 100
            }, 'slow');
        });
</script>


