<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\FriendRequest;
use app\models\Message;
use app\controllers\Utils;
?>	

<div class="row">
    <!-- start alert messages -->
    <?php if (Yii::$app->session->hasFlash('messageSent')): ?>
        <div class="alert alert-success">
            Your message has been sent successfully.
        </div>
        
    <?php endif; ?>
    <!-- end alert messages -->
    <div class="col-md-3"  style="text-align: center;">
        <div>
            <h3 class="fnt-green" style="text-align: center;"><?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $user->id)
                    echo "My Profile";
                else
                    echo $profile->firstname."'s Profile"; 
                ?>  
                <?php /* if (!Yii::$app->user->isGuest && Yii::$app->user->id == $user->id): ?> 
                        <a href="<?=\Yii::$app->homeUrl?>account/edit-account"><button class="btn btn-info">Edit Profile</button></a>
                <?php endif; */?>
            </h3>
            <div>
                <?php 
                if($profile->profile_image!='') 
                    echo '<img src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'" class="img-responsive const-img-size-test" alt="my-profile-Image">';
                else 
                    echo '<img src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png" class="img-responsive const-img-size-test" alt="my-profile-Image">';
                ?>
            </div>
        </div>
        <!--<div class="col-md-8">
            <div class="">
                <p>Name : <span><?=$profile->firstname." ".$profile->lastname?></span></p>
                <p>Location : <span><?=$profile->location?></span></p>
                <p>About Me : <span><?=$profile->about?> </span></p>
                <?php if (!Yii::$app->user->isGuest && $user->id != \Yii::$app->user->id){ ?>
                <a href="#messagemodal" data-toggle="modal"><button class="btn btn-warning">Send Me A Message</button></a>
                <?php } else if ($user->id != \Yii::$app->user->id){ ?>
                <a href="<?=\Yii::$app->homeUrl?>/site/login" data-toggle="modal"><button class="btn btn-warning">Send Me A Message</button></a>
                <?php } ?>
                <?php if (!Yii::$app->user->isGuest && $user->id != \Yii::$app->user->id){ 
                    $checkfriendlist = FriendRequest::find()->where(["requested_by"=>\Yii::$app->user->id,"requested_to"=>$user->id])->orWhere(["requested_to"=>\Yii::$app->user->id,"requested_by"=>$user->id])->one();

                    if(!$checkfriendlist)
                            echo '<a class="btn btn-info friendrequest ">Add as Friend</a>';
                    else if($checkfriendlist->status == 0 && $checkfriendlist->requested_by == \Yii::$app->user->id )
                            echo '<a class="btn btn-info friendrequest ">Friend Request Sent</a>';
                    else if($checkfriendlist->status == 0 && $checkfriendlist->requested_to == \Yii::$app->user->id )
                            echo '<a id="accept_fnds" class="btn btn-info" for="'. $checkfriendlist->f_id.'">Accept</a>';
                    else if($checkfriendlist->status == 1)
                            echo '<a class="btn btn-success">Friends</a>';
                } ?>
				
            </div>
        </div>-->
    <?php 
    $properties = Utils::getUserProperties($user->id);

    if (!Yii::$app->user->isGuest && Yii::$app->user->id == $user->id)
        {	
            $deleteduser = ",".\Yii::$app->user->id.",";	

            $inbox_messages = Message::find()
            ->select(['COUNT(*) AS cnt'])
            ->where(['parent_id'=>0, 'read_text'=>0])
            ->andwhere(['NOT LIKE','delete_status',$deleteduser ])
            ->andwhere(['or',['and',['recipient_id' => \Yii::$app->user->id],['reply_recipient_id' =>0 ]],['reply_recipient_id'=>\Yii::$app->user->id]])
            ->all();
        ?> 
            <div class="row link-thumb-contain">
                <div class="col-sm-3">
                        <a href="<?=Yii::$app->homeUrl?>account/edit-account"><i class="fa fa-id-card fa-10x fnt-green" aria-hidden="true"></i>Account Info</a>
                </div>
                <div class="col-sm-3">
                        <a type='wish' <?php if (!$properties['can_create_wish']) echo ' class="btn-disabled" " ';?>  href="<?=Yii::$app->homeUrl?>wish/create"><i class="fa fa-pencil-square-o fa-10x fnt-pink" aria-hidden="true"></i>Add Wish</a>
                </div>
                <div class="col-sm-3">
                        <a type='donation' <?php if (!$properties['can_create_donation']) echo ' class="btn-disabled" " ';?>  href="<?=Yii::$app->homeUrl?>donation/create"><i class="fas fa-hand-holding-heart fa-10x fnt-sea" aria-hidden="true"></i>Add Donation</a>
                </div>
                <div class="col-sm-3">
                        <a href="<?=Yii::$app->homeUrl?>account/inbox-message"><i class="fa fa-comments fa-10x fnt-orange" aria-hidden="true"></i>Inbox
                        <?php if($inbox_messages[0]['cnt'] > 0) { ?>
                            <span class="inboxNotification"><?php echo $inbox_messages[0]['cnt']; ?></span>
                         <?php } ?>
                        </a>
                </div>

            </div>
            <div class="row link-thumb-contain">
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>happy-stories/create"><i class="fa fa-newspaper-o fa-10x fnt-orange" aria-hidden="true"></i>Tell Your Story</a>
                </div>
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>happy-stories/my-story"><i class="fa fa-vcard fa-10x " aria-hidden="true" style="color: #fbfc3e !important;"></i>My Happy Stories</a>
                </div>
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>account/my-friend"><i class="fa fa-group fa-10x fnt-green" aria-hidden="true"></i>Friends</a>
                </div>

            </div>
            <div class="row link-thumb-contain">
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>account/my-account"><i class="fa fa-tasks fa-10x fnt-blue" aria-hidden="true"></i>Wishes & Donations</a>
                </div>
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>wish/my-drafts"><i class="fa fa-window-restore fa-10x fnt-brown" aria-hidden="true"></i>Wish Drafts</a>
                </div>
                <div class="col-sm-4">
                    <a href="<?=Yii::$app->homeUrl?>donation/my-drafts"><i class="fa fa-window-restore fa-10x" style="color: #301934 !important;" aria-hidden="true"></i>Donation Drafts</a>
                </div>
            </div>
        <?php } ?>		
    </div>
    <button onclick="topFunction()" id="myBtn" title="Go to top"><img width="40" src="<?=Yii::$app->homeUrl?>web/images/icon/scroll-up-arrow.gif"></button>
	<script>
		$(".send-msg").on("click",function(){
			var msg = $('.msg').val();
			var send_to = "<?=$user->id?>";
			var send_from = "<?=\Yii::$app->user->id?>";
			$.ajax({
				url : '<?=Url::to(['account/send-message'])?>',
				type : 'POST',
				data : {msg:msg,send_from:send_from,send_to:send_to},
				success: function(response){
				}
			});
		});
	
	
	$(".friendrequest").on("click",function(){			
			var send_to = "<?=$user->id?>";
			var send_from = "<?=\Yii::$app->user->id?>";
			$.ajax({
				url : '<?=Url::to(['friend/friend-request'])?>',
				type : 'POST',
				data : {send_from:send_from,send_to:send_to},
				success: function(response){	
					$(".friendrequest").html("Friend Request Sent");					
				}
			});
		});
		
		
		
	$("#accept_fnds").on("click",function(){	
			var request_id = $(this).attr("for");
			if($.trim(request_id) == "")
				return false;
			 $.ajax({
				url : '<?=Url::to(['friend/request-accepted'])?>',
				type : 'POST',
				data : {requestid:request_id},
				success: function(response){	
					if(response == true)
					{					   					  
					 $("#accept_fnds").html("Friends");
					 $("#accept_fnds").removeAttr("for");
					 $("#accept_fnds").removeClass("btn-info");
					 $("#accept_fnds").addClass('btn-success');
					 
					}						
				}
			});		 
		});
                
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $("html, body").animate({ scrollTop: "0px" }, 'slow');
    //document.body.scrollTop = 0; // For Chrome, Safari and Opera 
    //document.documentElement.scrollTop = 0; // For IE and Firefox
}
        


    $(".btn-disabled").click(function(e){
        e.preventDefault();
        // console.log($(this).attr('type'));
        var type = $(this).attr('type');
        if (type=='donation'){
            alert("You reached the maximum of "+ <?=$properties['max_no_donations']?> +" allowed donations.");
        } else if (type=='wish'){
            alert("You reached the maximum of "+ <?=$properties['max_no_wishes']?> +" allowed wishes.");
        }
        return false;
    });

</script>