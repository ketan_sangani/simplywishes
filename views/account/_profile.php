<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\FriendRequest;
use app\models\FollowRequest;
?>

<div class="col-md-12">

	<!-- start alert messages -->
	<?php if (Yii::$app->session->hasFlash('messageSent')): ?>
		<div class="alert alert-success">
			Your message has been sent successfully.
		</div>
	<?php endif; ?>

	<!-- end alert messages -->

	<h3 class="fnt-green" ><?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $user->id)
			echo "My Profile";
		else
			echo $profile->firstname."'s Profile";
		?>
		<?php if (!Yii::$app->user->isGuest && Yii::$app->user->id == $user->id): ?>
			<a href="<?=\Yii::$app->homeUrl?>account/edit-account"><button class="btn btn-info">Edit Profile</button></a>
		<?php endif; ?>
	</h3>

	<div class="col-md-3">
		<div class="thumbnail">
			<?php
			if($profile->profile_image!='')
				echo '<img src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
			else
				echo '<img src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
			?>
		</div>
	</div>
	<div class="col-md-8">
		<div class="">
			<p><b>Name : </b><span><?=$profile->firstname." ".$profile->lastname?></span></p>
			<p><b>Location : </b><span><?=$profile->location?></span></p>
			<p><b>About Me : </b><span><?=$profile->about?> </span></p>
			<?php if (!Yii::$app->user->isGuest && $user->id != \Yii::$app->user->id){ ?>
				<a href="<?=Url::to(['account/inbox-message'])?>" data-toggle="modal"><button class="btn btn-warning">Send Me A Message</button></a>
			<?php } else if ($user->id != \Yii::$app->user->id){ ?>
				<a href="<?= Yii::$app->homeUrl?>site/login" data-toggle="modal"><button class="btn btn-warning">Send Me A Message</button></a>
			<?php } ?>
			<?php /* if (!Yii::$app->user->isGuest && $user->id != \Yii::$app->user->id){
							$checkfriendlist = FriendRequest::find()->where(["requested_by"=>\Yii::$app->user->id,"requested_to"=>$user->id])->orWhere(["requested_to"=>\Yii::$app->user->id,"requested_by"=>$user->id])->one();
							
							if(!$checkfriendlist)
								echo '<a class="btn btn-info friendrequest ">Add as Friend</a>';
							else if($checkfriendlist->status == 0 && $checkfriendlist->requested_by == \Yii::$app->user->id )
								echo '<a class="btn btn-info friendrequest ">Friend Request Sent</a>';
							else if($checkfriendlist->status == 0 && $checkfriendlist->requested_to == \Yii::$app->user->id )
								echo '<a id="accept_fnds" class="btn btn-info" for="'. $checkfriendlist->f_id.'">Accept</a>';
							else if($checkfriendlist->status == 1)
								echo '<a class="btn btn-success">Friends</a>';
							
						} */ ?>

			<?php if (!Yii::$app->user->isGuest && $user->id != \Yii::$app->user->id && $user->id != 1){

				$checkfollowlist = FriendRequest::find()->where("(requested_by = ".\Yii::$app->user->id." AND requested_to = ".$user->id.") OR (requested_by = ".$user->id." AND requested_to = ".\Yii::$app->user->id.")")->orderBy(['f_id'=>SORT_DESC])->one();
               if(!empty($checkfollowlist)) {
				   if($checkfollowlist->status=='1') {
					   echo '<a class="btn btn-danger unfollowfrd" for="'.$checkfollowlist->f_id.'">Unfriend</a>';
				   }elseif ($checkfollowlist->status=='0'){
					   echo '<a class="btn btn-success friendrequest">Friend Request Sent</a>';
				   }elseif ($checkfollowlist->status=='3'){
					   echo '<a class="btn btn-success friendrequest ">Add Friend</a>';
				   }
			   }
				else {
					echo '<a class="btn btn-success friendrequest ">Add Friend</a>';
				}

			} ?>


			<?php
			$btnDisabled = ($isUserReportedByMe) ? 'btn-disabled disabled-btn-color' : '';
			$disabled = ($isUserReportedByMe) ? 'disabled disabled-btn-color' : '';
			//.$btnDisabled = ($isUserReportedByMe) ? 'btn-disabled' : '';
			//$disabled = ($isUserReportedByMe) ? 'disabled' : '';
			//var_dump($disabled);exit;
			if ($disabled) {
				echo ' <button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="fa fa-ellipsis-h dropdown-toggle txt-smp-green" style="background-color: white;color:black;    border: none; font-size:11px;"></button>';
			}else{
				echo ' <button data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"  class="fa fa-ellipsis-h dropdown-toggle txt-smp-green ' . $btnDisabled . '" ' . $disabled . ' style="background-color: white;color:black;    border: none; font-size:11px;"></button>';
			}

			//echo '<a class="btn btn-warning report"'.$btnDisabled.' "  '.$disabled.' >Report</a>';


			?>
			<div class="dropdown-menu" style="margin: -15px 241px 0;">
				<?php if ($disabled) { ?>
				<a href="#reportusermodalAccept" data-toggle="modal" ><button class="dropdown-item <?php //echo $btnDisabled;?>" style="
    display: block;
    width: 100%;
    padding: .25rem 1.5rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
" href="#">Report</button></a>
				<?php }else{?>
				<a href="#reportusermodalAccept" data-toggle="modal" ><button class="dropdown-item <?php //echo $btnDisabled;?>" style="
    display: block;
    width: 100%;
    padding: .25rem 1.5rem;
    clear: both;
    font-weight: 400;
    color: #212529;
    text-align: inherit;
    white-space: nowrap;
    background-color: transparent;
    border: 0;
" href="#">Report</button></a>
				<?php }?>

			</div>

		</div>
	</div>
</div>
<!-- modal Starts -->
<div class="modal fade" id="messagemodal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Message</h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left list-icon">
						<?php
						if($profile->profile_image!='')
							echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'"  alt="my-profile-Image">';
						else
							echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png" alt="my-profile-Image">';
						?>
						<!--<img src="./images/man1.jpg" alt="">-->
					</div>
					<div class="media-body">
						<h4 class="media-heading"><?=$profile->firstname." ".$profile->lastname?></h4>
					</div>
				</div>
				</br>
				<div class="form-group">
					<label for="subject">Subject <span class='valid-star-color' >*</span></label>
					<input type="text" id="subject" class="form-control"  rows="4" required>
				</div>
				<div class="form-group">
					<label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
					<textarea class="msg form-control" rows="4"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="send-msgs btn btn-primary">Send</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="reportusermodalAccept" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Report User</h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left list-icon">

					</div>
					<div class="media-body">
						<h4 class="media-heading">
						</h4>

					</div>
				</div>
				</br>
				<div class="form-group">
					<label for="subject">Please explain why you are reporting this user for violating our terms of service <span class='valid-star-color' >*</span></label>
					<textarea type="text" id="reason" class="form-control"  rows="4" required></textarea>
				</div>
				<div class="form-group">
					<input type="checkbox" class="msg-check" name="i_agree_report" id="i_agree_report" value="1"  required> Are you sure that you want to report this user? * Reporting this user will trigger a full review by SimplyWishes and may cause the user and all relevant content to be removed from our site. You can also unfriend this person.   </input>
				</div>

			</div>
			<div class="modal-footer">
				<button type="button" class="reportuserform btn btn-primary">Ok</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- modal Ends -->
<script>

	$(".send-msgs").on("click",function(){
		var msg = $('.msg').val();
		var subject = $('#subject').val();
		if($.trim(subject) === "")
		{
			alert("Please check the subject.");
			return false;

		}
		if($.trim(msg) === "")
		{
			alert("Please check the message.");
			return false;

		}
		var send_to = "<?=$user->id?>";
		var send_from = "<?=\Yii::$app->user->id?>";
		$.ajax({
			url : '<?=Url::to(['account/send-message'])?>',
			type : 'POST',
			data : {msg:msg,send_from:send_from,subject:subject,send_to:send_to},
			success: function(response){
			}
		});
	});
	$(".unfollowfrd").click(function(){
		var check = confirm( "Are sure to Unfriend from the List? ");
		if(check)
		{
			var request_id = $(this).attr("for");
			console.log(request_id);
			$.ajax({
				url : '<?=Url::to(['friend/cancel-friend'])?>',
				type : 'POST',
				data : {requestid:request_id},
				success: function(response){

					if(response == true)
					{
						$("#parent_div_"+request_id).remove();
					}
					window.location.reload();
				}
			});


		} else {
			return false;
		}
	});

	$(".report").on("click",function(){
		var check = confirm( " Are you sure you want to report this user? Reporting this user will hide this and any other content from this author as well as the author’s profile from you and will trigger a full review by SimplyWishes.");
		if (check){

			$.ajax({
				url : '<?=Url::to(['wish/report-content'])?>',
				type: 'GET',
				data: {
					w_id:0,
					content_type:'user',
					report_user:<?=Yii::$app->user->id?>,
					reported_user:"<?=$user->id?>",
					content_id:0,
				},
				success:function(data){
					console.log(data);
					if (data == "savedok"){
						window.location.href="/";
					}
				}
			});
		}
	});
	$(".reportuserform").on("click",function(){
		var reason = $('#reason').val();
		if($.trim(reason) === "")
		{
			alert("Please check the reason.");
			return false;

		}
		if(!$('#i_agree_report').is(":checked")){
			alert("Please check checbox of confirmation.");
			return false;
		}

		$.ajax({
			url : '<?=Url::to(['wish/report-content'])?>',
			type: 'GET',
			data: {
				w_id:0,
				content_type:'user',
				report_user:<?=Yii::$app->user->id?>,
				reported_user:"<?=$user->id?>",
				content_id:0,
				comment:reason,
			},
			success:function(data){
				console.log(data);
				if (data == "savedok"){
					window.location.href="/";
				}
			}
		});

	});


	$(".friendrequest").on("click",function(){
		var send_to = "<?=$user->id?>";
		var send_from = "<?=\Yii::$app->user->id?>";
		$.ajax({
			url : '<?=Url::to(['friend/friend-request'])?>',
			type : 'POST',
			data : {send_from:send_from,send_to:send_to},
			success: function(response){
			    var res = $.parseJSON(response);
			    if(res.status=='1'){
                    $(".friendrequest").html("Friend Request Sent");
                }else{
			        alert('You have already sent friend request.');
                }

			}
		});
	});



	$("#accept_fnds").on("click",function(){
		var request_id = $(this).attr("for");
		if($.trim(request_id) == "")
			return false;
		$.ajax({
			url : '<?=Url::to(['friend/request-accepted'])?>',
			type : 'POST',
			data : {requestid:request_id},
			success: function(response){
				if(response == true)
				{
					$("#accept_fnds").html("Friends");
					$("#accept_fnds").removeAttr("for");
					$("#accept_fnds").removeClass("btn-info");
					$("#accept_fnds").addClass('btn-success');

				}
			}
		});
	});


	$(".followrequest").on("click",function(){
		var send_to = "<?=$user->id?>";
		var send_from = "<?=\Yii::$app->user->id?>";
		$.ajax({
			url : '<?=Url::to(['follow/follow-request'])?>',
			type : 'POST',
			data : {send_from:send_from,send_to:send_to},
			success: function(response){
				if($.trim(response) == "follow")
				{
					$(".followrequest").html("Unfriend");
					$( ".followrequest" ).removeClass( "btn-success" );
					$( ".followrequest" ).addClass( "btn-danger" );
				}
				else if($.trim(response) == "unfollow")
				{
					$(".followrequest").html("Add Friend");
					$( ".followrequest" ).removeClass( "btn-danger" );
					$( ".followrequest" ).addClass( "btn-success" );
				}
			}
		});
	});

</script>