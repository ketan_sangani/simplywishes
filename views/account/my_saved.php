<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/masonry.js" type="text/javascript"></script>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/imagesloaded.js" type="text/javascript"></script>	
	<?php echo $this->render('_profilenew',['user'=>$user,'profile'=>$profile])?>
	<!-- To replace tab as link remove data-toggle=tab and replace href with link-->
<div class="col-md-8" id="scroll-evt">
    <div class="marginPos">
	<ul class="nav nav-tabs smp-mg-bottom" role="tablist">
	  <li role="presentation">
		<a href="<?=\Yii::$app->homeUrl?>account/my-account" role="tab">My Active Wishes & Donations</a>
	  </li>
	   <li class="dropdown">
            <a href="<?=\Yii::$app->homeUrl?>account/my-progress" role="tab">In Progress <span class="caret"></span></a>
        </li>
	  <li class="dropdown">
            <a href="<?=\Yii::$app->homeUrl?>account/my-fullfilled" role="tab">Granted <span class="caret"></span></a>
        </li>
	  <li role="presentation" class="active">
		<a href="#savedwish" role="tab" data-toggle="tab" >My Saved Wishes & Donations</a>
	  </li>	  
	</ul>
	<div class="tab-content">
            <div role="tabpanel" class="tab-pane" id="activewish">
            </div>
            <div role="tabpanel" class="tab-pane active hide grid" id="savedwish">
                    <?php 
                      if(empty($dataProviderDonation->models) && empty($dataProvider->models)){
                          echo "<p>No Saved Wishes!</p>";

                      }else {
                          if (!empty($dataProviderDonation->models)) {
                              foreach ($dataProviderDonation->models as $donation) {
                                  echo $donation->htmlForProfileSaved;
                              }
                          } else if(empty($dataProviderDonation->models) && empty($dataProvider->models)) {
                              echo "<p>No Saved Wishes!</p>";
                          }

                          if (!empty($dataProvider->models)) {
                              foreach ($dataProvider->models as $wish) {
                                  echo $wish->htmlForProfileSaved;
                              }
                          } else if(empty($dataProviderDonation->models) && empty($dataProvider->models)) {
                              echo "<p>No Saved Wishes!</p>";
                          }
                      }
                    ?>			
            </div>
            <div align="center" id="loader_img" ><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif"></div>
        </div>
   </div>
</div>
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script>
    $(window).on('load', function () {
        $("#loader_img").hide();
        $(".shareIcons").each(function(){
            var elem = $(this);
            elem.jsSocials({
                showLabel: false,
                showCount: true,
                shares: ["facebook" , {
                    share: "pinterest",           // name of share
                    via: "simplywishes5244",
                    media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                    hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                },
                    {
                        share: "twitter",           // name of share
                        via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    }],
                url : elem.attr("data_url"),
                text: elem.attr("data_text"),
            });
        });
        $('html, body').animate({
            scrollTop: $('#scroll-evt').offset().top - 100
        }, 'slow');
        
        var win = $(window);
        var page = 1;
        var page2 = 1;
        var $container = $('.grid');
        $('#savedwish').removeClass('hide');
        $container.masonry();
        // Each time the user scrolls
        win.scroll(function() {
            var scroll_top = Math.round(win.scrollTop());
            // End of the document reached?
            if(parseInt(page) == parseInt(page2)){
            //if ($(document).height() - win.height()-1 == scroll_top ) {
            //  previous line if($(win).scrollTop() + $(win).height() == $(document).height()){
            if($(win).scrollTop() + $(win).height() + parseInt(50) >= $(document).height()){
                page2 = page2+1;
                $("#loader_img").show();
                $.ajax({
                    url: '<?=Url::to(['account/scroll-my-saved'], true);?>',
                    dataType: 'html',
                    data: {'page':page},
                    success: function(html) {
                        var el = $(html);
                        //$(".grid").append(el).masonry( 'appended', el, false );
                        var $newElems = $( html ).css({ opacity: 0 });
                        $newElems.imagesLoaded(function(){
                            $newElems.animate({ opacity: 1 });
                            $(".grid").append(el);
                            $(".shareIcons").each(function(){
                                var elem = $(this);
                                elem.jsSocials({
                                    showLabel: false,
                                    showCount: true,
                                    shares: ["facebook" , {
                                        share: "pinterest",           // name of share
                                        via: "simplywishes5244",
                                        media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                                    },
                                    {
                                            share: "twitter",           // name of share
                                            via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                                            hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                                    }],
                                    url : elem.attr("data_url"),
                                    text: elem.attr("data_text"),
                                });
                            });
                            $(".grid").masonry( 'appended', el, true )
                        });
                        if(html == '')
                                page = false;
                        else
                                page = page+1;

                        $("#loader_img").hide();
                    },
                    error:function(){
                        $("#loader_img").hide();
                    }
                });
            }}
        });
    });
    
    var Wish    =   {
        ajax    :   false
    };
		
    $(document).on('click', '.like-wish, .fav-wish', function(){ 
        //$(".like-wish, .fav-wish").on("click",function(){
        var wish_id = $(this).attr("data-w_id");
        var type = $(this).attr("data-a_type");
        var elem = $(this);
        $.ajax({
                url : '<?=Url::to(['wish/like'])?>',
                type: 'GET',
                data: {w_id:wish_id,type:type},
                success:function(data){
                        if(data == "added"){
                                if(type=="fav"){
                                        elem.removeClass("txt-smp-orange");
                                        elem.addClass("txt-smp-blue");
                                }
                                if(type=="like"){
                                        elem.css('color', '#B23535');
                                        var likecmt = $("#likecmt_"+wish_id).text();
                                        likecmt = parseInt(likecmt) + parseInt(1);
                                        $("#likecmt_"+wish_id).text(likecmt);
                                }
                        }
                        if(data == "removed"){
                                if(type=="fav"){
                                        elem.addClass("txt-smp-orange");
                                        elem.removeClass("txt-smp-blue");
                                }
                                if(type=="like"){
                                        elem.css('color', '#fff');
                                        var likecmt = $("#likecmt_"+wish_id).text();
                                        likecmt = parseInt(likecmt) - parseInt(1);
                                        $("#likecmt_"+wish_id).text(likecmt);
                                }
                        }
                }
        });
    });	
    var isVisible = false;
    var clickedAway = false;

    $(document).on("click", ".listesinside", function() {
            $(function(){
                    $('.listesinside').popover({   
                            html: true,
                            content: function () {
                                    var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
                                    return clone;
                            }
                    }).click(function(e) {
                            e.preventDefault();
                            clickedAway = false;
                            isVisible = true;
                    });
            });

    });

    $(document).click(function (e) {
        if (isVisible & clickedAway) {
            $('.listesinside').popover('hide');
            isVisible = clickedAway = false;
        } else {
            clickedAway = true;
        }
    });

    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });
    $(document).on('click', '.likesView', function(){ 
        var val =   $(this).find('span').html();
        if(val <= 0)
            return false;

        var wish_id = $(this).data('id');
        if($.trim(wish_id) !== "" )
        {
            if(Wish.ajax)
                return false;

            Wish.ajax   =   true;
            
            $.ajax({
                url : '<?=Url::to(['wish/likes-view'])?>',
                type: 'GET',
                data: {w_id:wish_id},
                success:function(data){
                    if(data)
                    {
                        var tpl =   '';
                        var items   =   JSON.parse(data);
                        $.each(items, function(k,v){
                           tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                        });

                        $('.modal-body').append(tpl);
                        $('#likeModal').modal({backdrop: 'static', keyboard: false});
                        $('#likeModal').modal('show');

                        $('.closeModal').on('click', function(e){
                            e.preventDefault();
                            $('.modal-body').html('');
                            $('#likeModal').modal('hide');
                        });
                    }
                },
                complete: function(){
                    Wish.ajax   =   false;
                }
            });
        }
    });

</script>