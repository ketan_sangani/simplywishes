<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact Us';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-contact">
    <h3 class="fnt-skyblue"><?= Html::encode($this->title) ?></h3>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')){ ?>

        <div class="alert alert-success">
            Thanks For contacting Us. We will reach you very soon.
        </div>

    <?php } else { ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label("Name <span class='valid-star-color' >*</span> ") ?>

                    <?= $form->field($model, 'email')->label("Email <span class='valid-star-color' >*</span> ") ?>


                    <?= $form->field($model, 'subject')->label("Subject <span class='valid-star-color' >*</span> ") ?>

                    <?= $form->field($model, 'body')->textarea(['rows' => 6])->label("Tell
us about your experience <span class='valid-star-color' >*</span> ") ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'imageOptions' => [
                            'id' => 'my-captcha-image'
                        ],
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-1" style="margin-top: 10px; "><i class="fa fa-refresh" id="refresh-captcha"></i></div>
                        <div class="row col-lg-5" style="margin: 0">{input}</div>',

                        ])->label("Verification Code <span class='valid-star-color' >*</span>") ?>
                    <?php $this->registerJs("
                    $('#refresh-captcha').on('click', function(e){
                        e.preventDefault();
                    $('#my-captcha-image').yiiCaptcha('refresh');})"); ?>
                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

        <?php
    }
    ?>

</div>
</div>
