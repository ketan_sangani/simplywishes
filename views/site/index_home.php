<?php 
use yii\helpers\Html;
use app\models\Message;

if(Yii::$app->session->getFlash('success')!='') {?>
    <div class="alert alert-success" role="alert">
        <strong> <?= Yii::$app->session->getFlash('success'); ?>.</strong>
    </div>
<?php } ?>
		
<div class="row page-header">
    <div class="container my-profile">
	<div class="row">
            <h3 class="fnt-green" style="text-align: center;" >My Profile</h3>
            <div class="col-md-3">
                <div>
                    <?php 
                    if($profile->profile_image!='') 
                        echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
                    else 
                        echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
                    ?>
                </div>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
                <div class="">
                    <p><b>Name : </b><span><?=$profile->firstname." ".$profile->lastname?></span></p>
                    <p><b>Location : </b><span><?=$profile->location?></span></p>
                    <p><b>About Me : </b><span style="white-space: pre-wrap;"><?=$profile->about?> </span></p>
                    <p>
                        <?php  echo Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                        '<i class="glyphicon glyphicon-log-out"></i> Logout',
                        ['class' => 'a-button fnt-blue']
                        )
                        . Html::endForm();  ?>
                    </p> 
                </div>
            </div>
	</div>
	<div class="row smp-mg-bottom"></div>
	<div class="row link-thumb-contain" style="text-align: center;">
            <div class="col-sm-2">
                    <a href="<?=Yii::$app->homeUrl?>account/edit-account"><i class="fa fa-id-card fa-10x fnt-green" aria-hidden="true"></i>Account Info</a>
            </div>
            <div class="col-sm-2">
           
                    <a  <?php if (!$properties['can_create_wish']) echo ' class="btn-disabled" " ';?>  href="<?=Yii::$app->homeUrl?>wish/create"><i class="fa fa-pencil-square-o fa-10x fnt-pink" aria-hidden="true"></i>Add Wish</a>
            </div>
            <div class="col-sm-2">
                <a  <?php if (!$properties['can_create_donation']) echo ' class="btn-disabled" " ';?>  href="<?=Yii::$app->homeUrl?>donation/create"><i class="fas fa-hand-holding-heart  fa-10x fnt-sea" aria-hidden="true"></i>Add Donation</a>
            </div>
            <div class="col-sm-2">
                <?php
                     $deleteduser = ",".\Yii::$app->user->id.",";
                    $inbox_messages = Message::find()
                                ->select(['COUNT(*) AS cnt'])
                                ->where(['parent_id'=>0, 'read_text'=>0])
                                ->andwhere(['NOT LIKE','delete_status',$deleteduser ])
                                ->andwhere(['reply_recipient_id' => \Yii::$app->user->id])
                                ->andwhere(['!=','reply_recipient_id', '0' ])//['and',['recipient_id'=>\Yii::$app->user->id],['reply_recipient_id' => 0 ]]])
                                ->all();
                                // ->andwhere(
                                    // ['OR',
                                    //     ['reply_recipient_id' => \Yii::$app->user->id],
                                    //     ['reply_sender_id' => \Yii::$app->user->id],
                                    //     ['and',
                                    //         ['recipient_id'=>\Yii::$app->user->id],
                                    //         ['reply_recipient_id' => 0 ]
                                    //     ]
                                    // ])
                                
                                // echo $inbox_messages->createCommand()->sql;

                                ?>
                <a href="<?=Yii::$app->homeUrl?>account/inbox-message"><i class="fa fa-comments fa-10x fnt-orange" aria-hidden="true"></i>Inbox
                    <?php if($inbox_messages[0]['cnt'] > 0) { ?>
                    <span class="inboxNotification"><?php echo $inbox_messages[0]['cnt']; ?></span>
                    <?php } ?>
                </a>
            </div>
            <div class="col-sm-2">
                <a href="<?=Yii::$app->homeUrl?>happy-stories/create"><i class="fa fa-newspaper-o fa-10x fnt-orange" aria-hidden="true"></i>Tell Your Story</a>
            </div>
        </div>
        <div class="row link-thumb-contain" style="text-align: center;">
            <div class="col-sm-2">
                <a href="<?=Yii::$app->homeUrl?>happy-stories/my-story"><i class="fa fa-vcard fa-10x fnt-brown"  style="color: #fbfc3e !important;" aria-hidden="true"></i>My Happy Stories</a>
            </div>
            <div class="col-sm-2">
                <a href="<?=Yii::$app->homeUrl?>account/my-friend"><i class="fa fa-group fa-10x fnt-green " aria-hidden="true"></i>Friends</a>
            </div>
            <div class="col-sm-2">
                    <a href="<?=Yii::$app->homeUrl?>account/my-account"><i class="fa fa-tasks fa-10x fnt-blue" aria-hidden="true"></i>Wishes & Donations</a>
            </div>        
            <div class="col-sm-2">
                    <a href="<?=Yii::$app->homeUrl?>wish/my-drafts"><i class="fa fa-window-restore fa-10x fnt-brown" aria-hidden="true"></i>Wish Drafts</a>
            </div>
            <div class="col-sm-2">
                    <a href="<?=Yii::$app->homeUrl?>donation/my-drafts"><i class="fa fa-window-restore fa-10x" style="color: #301934 !important;" aria-hidden="true"></i>Donation Drafts</a>
            </div>
        </div>

        <div class="row link-thumb-contain" style="text-align: center;">
       

        </div>
	
	<div class="col-md-12 smp-mg-bottom"></div>
    </div>
</div>

<script>
$(".btn-disabled").click(function(e){
    e.preventDefault();
    alert("You reached the maximum of "+ <?=$properties['max_no_wishes']?> +" allowed wishes.");
    return false;
});

</script>

