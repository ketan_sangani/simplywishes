<div style='margin: auto; width: 60%; padding-top: 5%'>
    <p>Thank you <b><?= $profile->firstname ?></b>. Your registration has been submitted.</p>
    <p>
        An email has been sent to <?= $user->email ?>.You will receive an email with instructions on the next steps in your Inbox.</p>
      <p>  Please check your Spam or Junk Box if you do not receive the e-mail within the next 10 minutes (usually instantly).</p>
    <p>Otherwise,you can <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['site/resendverificationlink', 'user' => urlencode($user->email)]);
        ?>">Resend the Verification Link</a>, or <a href="<?php echo Yii::$app->urlManager->createAbsoluteUrl(['site/contact']);
        ?>">Contact Us </a> for further assistance.
    </p>
</div>


