<?php 
use yii\helpers\Url;
use app\models\UserProfile;
use yii\helpers\Html;

$this->title = 'About Us';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-f">
	<div class="row" style="margin-right: 0 ;margin-left: 0;">
		<div class="col-lg-4" >
			<img style="display: none;" class="about-img"  src="<?= Yii::$app->homeUrl ?>web/images/2621g (1).jpg" alt="Aboutus"/>
		</div>
		<div class="col-lg-6">
			<div class="site-about smp-mg-bottom">
				<h3 class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>
				<?php if(!Yii::$app->user->isGuest && \Yii::$app->user->identity->role == 'admin'){?>
					<p>
						<?= Html::a('Update', ['page/update', 'id' => $model->p_id], ['class' => 'btn btn-primary']) ?>
					</p>
				<?php } ?>
                <div style="font-size: 17px;">
				<p>SimplyWishes.com makes wishes come true. This is a free platform for passionate well wishers and wish-makers to practice the art of giving.</p>
				<p>By sharing gifts and granting wishes, we can make the world a kinder, happier and more connected place – as in life, what you give, you receive.</p>
				<P>Do you have a wish? Can someone out there make your wish come true? A wish can be anything from a  monetary gift, a concert ticket, or a ride from the airport.</P>
				<p>Don’t have a gift, wish or dream right now? Take five minutes to share a funny story or post words of encouragement. SimplyWishes.com is all about bringing more light and<br> joy into the world, and that goal starts with you. </p>
				<!--<div class="row">
        <div class="col-md-6"><b><?/*= $model->content*/?></b></div>
    </div>--> 
                <h3>Who We Are…</h3>
                <p style="font-style: italic;"> We are entrepreneurs, inventors,<br>
                    physicists, engineers, software<br>
                    developers, environmentalists,<br>
                    journalists, social activists,<br>
                    story tellers, health practitioners,<br>
                    mothers, fathers, daughters,<br>
                    sons, soccer players,<br>
                    roller-derby athletes, water polo<br>
                    players, martial art masters,<br>
                    homesteaders, animal lovers,<br>
                    beekeepers, farmers, city<br>
                    slickers, do-gooders, wish<br>
                    makers, and well-wishers!
                </p>
                </div>
				<br>
			</div>
		</div>
        <div class="col-lg-2">
        </div>
	</div>
</div>


<!--------------- SLIDER CHECK Function ---------------------->
  <link rel="stylesheet" type="text/css" href="<?= Yii::$app->homeUrl?>web/src/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?= Yii::$app->homeUrl?>web/src/slick/slick-theme.css"> 
  <script src="<?= Yii::$app->homeUrl?>web/src/slick/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="<?= Yii::$app->homeUrl?>web/src/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  
  
  
  <script type="text/javascript">
  var js = $.noConflict();
   js(document).on('ready', function() {
      js(".regular").slick({
         dots: false,
	 infinite: true,
	 speed: 300,
	 slidesToShow: 5,
	 slidesToScroll: 1,
	 responsive: [
		{
		 breakpoint: 1024,
		 settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: false
		 }
		},
		{
		 breakpoint: 600,
		 settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		 }
		},
		{
		 breakpoint: 480,
		 settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		 }
		}
	 ]
      });
     
	 
    });
  </script>
  
 
  
  <!--------------- SLIDER CHECK Function END ---------------->
 
