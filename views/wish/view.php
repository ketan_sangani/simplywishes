<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use app\models\WishComments;
use app\controllers\Utils;
/* @var $this yii\web\View */
/* @var $model app\models\Wish */

$this->title = $model->wish_title;
$this->params['breadcrumbs'][] = ['label' => 'Wishes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\Yii::$app->view->registerMetaTag([
    'name' => 'og:title',
    'property' => 'og:title',
    'content' =>$model->wish_title
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:description',
    'property' => 'og:description',
    'content' => strip_tags($model->wish_description)
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:image',
    'property' => 'og:image',
    'content' =>Url::to(['web/'.$model->primary_image],true)
]);

$class  =   '';
if($model->likesCount > 0)
    $class    =   'likesView';
else
    $class  =   'hide';
?>
<div class="wish-view">
    <div class="row my-profile">
	<div class="col-md-12 smp-mg-bottom">
        <?php if (Yii::$app->session->hasFlash('messageSent')): ?>
            <div class="alert alert-success">
                Your message has been sent successfully.
            </div>
        <?php endif; ?>
            <h3 class="fnt-skyblue "><?=$this->title?></h3>
            <p id="success_txt" class="text-success hide"></p>
            <div class="col-md-3 happystory sharefull-list">
                <img src="<?=\Yii::$app->homeUrl.'web/'.$model->primary_image.'?v='.strtotime('now')?>"  class="img-responsive" alt="my-profile-Image"><br>
                <p>
                    <span data-id="<?= $model->w_id ?>" class="likesBlk <?php echo $class; ?>"> <span id="likecmt_<?= $model->w_id ?>"> <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span> &nbsp;
                    <!--<a class="report-img" title="Report" data-id="<?= $model->w_id ?>"><img  src="<?= Yii::$app->homeUrl ?>images/report.png" alt=""></a>-->
                    <?php
                      if(!$model->isFaved(\Yii::$app->user->id))

                            echo '<span title="Save this wish" data-w_id="'.$model->w_id.'" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span>&nbsp;';
                      else
                            echo  '<span title="You saved it" data-w_id="'.$model->w_id.'" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span>&nbsp;';

                      if(!$model->isLiked(\Yii::$app->user->id))
                            echo  '<span title="Love it" data-w_id="'.$model->w_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                      else
                            echo  '<span title="You loved it" data-w_id="'.$model->w_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                      if (\Yii::$app->user->isGuest){
                          echo ' <button class="fa fa-flag txt-smp-green" onclick="test()" style="background-color: black; float: None !important;border-radius: 50%;font-size:11px;"></button>';
                      }else{
                          if (\Yii::$app->user->id!=$model->wished_by) {
                              $btnDisabled = ($isReported) ? 'btn-disabled disabled-btn-color' : '';
                              $disabled = ($isReported) ? 'disabled disabled-btn-color' : '';
                              if ($disabled) {
                                  echo ' <button class="fa fa-flag txt-smp-green report-wish' . $btnDisabled . '" ' . $disabled . ' style="background-color: red; float: None !important;border-radius: 50%;font-size:11px;"></button>';
                              }else{
                                  echo ' <button class="fa fa-flag txt-smp-green report-wish' . $btnDisabled . '" ' . $disabled . ' style="background-color: black; float: None !important;border-radius: 50%;font-size:11px;" title="Report"></button>';
                              }
                          }
                      }

                    ?>
                    <?php if (\Yii::$app->user->isGuest){ ?>
                        <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" onclick='test()' class="listesinside removelistesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>
                        <div class="shareIcons hide"></div>
                   <?php }else{ ?>
                        <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" class="listesinside removelistesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>
                        <div class="shareIcons hide" data_image="<?php echo Url::base(true).'/web/'.$model->primary_image;?>" data_text="<?= $model->wish_title ?>" data_url="<?= Url::to(['wish/view','id'=>$model->w_id],true)?>" ></div>
                    <?php }?>
                </p>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
                <div class="row"><p class="col-md-4"><b>Name: </b></p><p class="col-md-8"><a href="<?=Url::to(['account/profile','id'=>$model->wished_by])?>"><?=$model->wisherName?></a></p></div>
                <div class="row"><p class="col-md-4"><b>Wish Description: </b></p><div class="col-md-8"><?=$model->wish_description?></div></div>
                <div class="row"><p class="col-md-4"><b>Date - I would like my wish to be granted: </b></p><p class="col-md-8"><span><?php echo $model->expected_date; ?></span></p></div>
                <div class="row"><p class="col-md-4"><b>Wish Type: </b></p><p>
                    <?php if($model->non_pay_option == 0 ){ ?>
                        <span class="col-md-8">Financial (You need a PayPal account in order to grant a Financial Wish)</span>
                    <?php } else if($model->non_pay_option == 1 ){ ?>
                        <span class="col-md-8">Non-Financial</span>
                    <?php } ?>
                        </p></div>
                <?php if($model->non_pay_option == 0 ){ ?>
                <div class="row"><p class="col-md-4"><b>Wish Expected Cost: </b></p><p><span class="col-md-8">$<?=$model->expected_cost ?></span></p></div>
                    <div class="row"><p class="col-md-4"><b> Financial assistance: </b></p><p><span class="col-md-8"><?=$model->financial_assistance;
                                echo ($model->financial_assistance=='Other')?$model->financial_assistance_other:'';?></span></p></div>
                <?php } ?>
                <?php if($model->non_pay_option == 1 ){ ?>
                    <div class="row"><p class="col-md-4"><b>Delivery Type: </b></p><div class="col-md-8"><?=$model->way_of_wish;?> (<?=$model->description_of_way;?>)</div></div>
                <?php } ?>
                <?php if(!is_null($model->granted_by)){ ?>	
                    <div class="row"><p class="col-md-4"><b>Wish granted on: </b></p><p><span class="col-md-8"><?php echo $model->granted_date; ?></span></p></div>			
                    <div class="row"><p class="col-md-4"><b>Wish granted by: </b></p><p><span class="col-md-8"><a href="<?=Url::to(['account/profile','id'=>$model->granted_by])?>"><span><?=$model->GrantedWisherName?></span></a></span></p></div>		
                <?php } ?>			
                <?php if(is_null($model->granted_by) && !\Yii::$app->user->isGuest  && \Yii::$app->user->id!=$model->wished_by){ ?>
                    <?php if($model->non_pay_option == 0 ){ ?>
                        <!--<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">-->
<!--                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_form" style="display:inline;"> -->
                            <!-- Identify your business so that you can collect the payments. -->
                            <!--<input type="hidden" name="business" value="dency@abacies.com">-->
                            <input type="hidden" name="business" value="<?=$model->wisher->email?>">
                            <!-- Specify a Buy Now button. -->
                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="return" value="<?=Url::to(['wish/fullfilled','w_id'=>$model->w_id],true)?>">
                            <input type="hidden" name="notify_url" value="<?=Url::to(['wish/verify-granted'],true)?>">
                            <!-- Specify details about the item that buyers will purchase. -->
                            <input type="hidden" name="item_name" value="<?=$model->wish_title?>">
                            <input type="hidden" name="item_number" value="<?=$model->w_id?>">
                            <input type="hidden" name="amount" value="<?=$model->expected_cost?>">
                            <input type="hidden" name="currency_code" value="USD">

                            <!-- Display the payment button. -->
                            <!--<input type="image" name="submit" border="0" 
                            src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_paynow_cc_144x47.png"
                            alt="Buy Now">-->
                <?php if(!empty($model->process_granted_by)){ ?>
                    <button class="btn btn-success inProgress">In Progress</button>
                <?php } else { ?>
                <div class="row">
                    <a href="#messagemodal1" data-toggle="modal"><button class="btn btn-success" style="margin-left:15px; ">Grant This Wish </button></a>
                    <?php } ?>
<!--                            <img alt="" border="0" width="1" height="1"-->
<!--                            src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >-->


<!--                        </form>-->
                        
                    <?php } else if($model->non_pay_option == 1 ){ ?>
                        <?php if(!empty($model->process_granted_by)){ ?>
                          <button class="btn btn-success inProgress">In Progress</button>					
                        <?php } else { ?>
                            <div class="row">
                          <a href="#messagemodal1" data-toggle="modal"><button class="btn btn-success" style="margin-left:15px; ">Grant This Wish </button></a>
                        <?php } ?>
                    <?php } ?>
                <?php } else if(!is_null($model->granted_by)&& \Yii::$app->user->isGuest){ ?>
                    <a href="<?=Url::to(['site/login','red_url'=>Yii::$app->homeUrl.'wish/view?id='.$model->w_id])?>"><button class="btn btn-success" style="margin-left:15px; ">Grant This Wish </button></a>
                <?php } else if(\Yii::$app->user->isGuest) { ?>
                    <a href="<?=Url::to(['site/login','red_url'=>Yii::$app->homeUrl.'wish/view?id='.$model->w_id])?>"><button class="btn btn-success" style="margin-left:15px; ">Grant This Wish </button></a>
                <?php } if(\Yii::$app->user->id!=$model->wished_by && !\Yii::$app->user->isGuest){ ?>
                                <a href="<?=Url::to(['account/inbox-message'])?>" data-toggle="modal" ><button class="btn btn-warning" style="margin-left:15px; ">Send a Message </button></a>
                                <?php }?>
                <?php if((is_null($model->granted_by)) && (!\Yii::$app->user->isGuest) && (\Yii::$app->user->id==$model->wished_by) && (!empty($model->process_status)))
                {
                    echo '<button class="btn btn-info" id="grant_progress_wishes" >Fulfilled </button>';
                    /*echo '<button class="btn btn-danger"  style="margin-left:15px" id="Resubmit_wishes"> Re-submit My Wish </button>'; */
                } 
                else if((is_null($model->granted_by)) && (!\Yii::$app->user->isGuest) && (\Yii::$app->user->id==$model->wished_by))
                {
                    echo '<a href="'.Url::to(['wish/update','id'=>$model->w_id]).'"><button class="btn btn-info">Update Wish</button></a>'; ?>
                    <?= Html::a('Delete', ['delete-wish', 'id' => $model->w_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                        ],
                    ]) ?>
                <?php } ?>
                <!-- <a class="btn btn-success" id="back">Back</a> -->
                </div>
            </div>
	</div>
    </div>
<div class="row" style="margin-left: 20px;">
    <div class="col-md-6">
    <h3 class="left fnt-skyblue" >Comments:</h3>

    <?php if (\Yii::$app->user->isGuest){ $form = ActiveForm::begin(['action' =>['site/login']]);}else {$form = ActiveForm::begin(['action' =>['wish/wish-comments']]);}  ?>
        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 4])->label(false) ?>
        <?= $form->field($listcomments, 'w_id')->hiddeninput(['value'=>$model->w_id])->label(false) ?>
       <div class="form-group">
           <?= Html::submitButton('Post', ['class' =>'btn btn-primary']) ?>
       </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php
$wishUserId = 0;

if(isset($comments) && !empty($comments)){ 
    foreach($comments as $user)
    {
        $profile = UserProfile::find()->where(['user_id'=>$user->user_id])->one();
        if(!empty($profile)) {?>

        <div class="row" id="parent_comment_<?= $user->w_comment_id ?>">		
            <div class="dpic floatLeft">
                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $profile->profile_image.'?v='.strtotime('now'); ?>"/>				
            </div>
            <div class="floatLeft col-md-10 paddingLeft">	
                <span id="<?php echo "comment_".$user->w_comment_id ?>">
                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $profile->user_id ?>" ><?= $profile->firstname.' '.$profile->lastname ?></a>					
                
                    <?= $user->comments ?><?php if($user->user_id === \Yii::$app->user->id) { ?>
                    <i class="glyphicon glyphicon-pencil editComment" data-id="<?= $user->w_comment_id ?>" aria-hidden="true"></i>
                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $user->w_comment_id], [
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this Comment?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php } ?>
                </span>
                <div  style="display:none; margin-top:10px" id="<?php echo "editCommentBlk_".$user->w_comment_id ?>">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['wish/update-comment?id='.$user->w_comment_id]]); ?>
                        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2, 'value' => $user->comments])->label(false) ?>			
                        <?= $form->field($listcomments, 'w_id')->hiddeninput(['value'=>$model->w_id])->label(false) ?>
                       <div class="form-group">
                           <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                       </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <p class="commentOptions">
                    <?php
                        if(!$user->isLiked(\Yii::$app->user->id))
                              echo  '<span title="Love it" class="loveComment" for="'.$user->w_comment_id.'">Love</span>';
                        else
                              echo  '<span title="Love it" class="loveComment" for="'.$user->w_comment_id.'" style="color:#B23535;">Love</span>';
                        
                        if($user->likesCount > 0)
                            $classCmt   =   'cmtLikesView';
                        else
                            $classCmt   =   '';
                        
                        $disabled = "";

                        $isReported = Utils::checkIsReported($user->w_comment_id, 'wish_comment');
                        // $btnDisabled = ($isReported) ? 'btn-disabled' : '';
                        $disabled = ($isReported) ? ' disabled' : '';
                        if (\Yii::$app->user->id == $user->user_id ){
                            $disabled = " disabled";
                        } 
                    ?>
                    <span class="on-reply" for="<?= $user->w_comment_id ?>">Reply</span>
                    <!-- todoreportcomment -->
                    <span class="report-comment<?=$disabled?>" <?=$disabled?> comment="<?=$user->comments ?>" by="<?= $user->user_id ?>" for="<?= $user->w_comment_id ?>">Report</span>
                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $user->w_comment_id ?>'></span>
                    <span id="cmtNum_<?= $user->w_comment_id ?>"> <?= $user->likesCount ?></span>
                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($user->created_at)) ?></span>
                </p>
                
                <div  style="display:none; margin-top:10px" id="<?php echo "replylist_".$user->w_comment_id ?>" class="comment-form2 reply full" data-plugin="comment-reply">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['wish/commentreply']]); ?>				 
                     <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2])->label(false) ?>			
                     <?= $form->field($listcomments, 'w_id')->hiddeninput(['value'=>$model->w_id])->label(false) ?>			
                     <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$user->w_comment_id])->label(false) ?>
                     <div class="form-group">
                            <?= Html::submitButton('Reply-Post', ['class' =>'btn btn-primary']) ?>
                    </div>					
                    <?php ActiveForm::end(); ?>
                </div>
                <?php 
                $replycomments = WishComments::find()->where(['parent_id'=>$user->w_comment_id, 'status'=>0])->orderBy('w_comment_id Desc')->all();
                if($replycomments)
                {
                    foreach($replycomments as $replyuser)
                    {
                        $replyprofile = UserProfile::find()->where(['user_id'=>$replyuser->user_id])->one();	
                        ?>
                        <div class="row">		
                            <div class="dpic floatLeft">
                                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $replyprofile->profile_image.'?v='.strtotime('now'); ?>"/>				
                            </div>
                            <div class="floatLeft col-md-10 paddingLeft">	
                                <span id="<?php echo "replyComment_".$replyuser->w_comment_id ?>">    
                                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $replyprofile->user_id ?>" ><?= $replyprofile->firstname.' '.$replyprofile->lastname; ?></a>						
                                 
                                    <?= $replyuser->comments ?><?php if($replyuser->user_id === \Yii::$app->user->id) { ?>
                                    <i class="glyphicon glyphicon-pencil editReplyComment" data-id="<?= $replyuser->w_comment_id ?>" aria-hidden="true"></i>
                                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteReplyComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $replyuser->w_comment_id], [
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this Comment?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                    <?php } ?>
                                </span>
                                <p class="commentOptions">
                                    <?php
                                        if(!$replyuser->isLiked(\Yii::$app->user->id))
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->w_comment_id.'">Love</span>';
                                        else
                                              echo  '<span title="Love it" class="loveComment" for="'.$replyuser->w_comment_id.'" style="color:#B23535;">Love</span>';

                                        if($replyuser->likesCount > 0)
                                            $classCmt   =   'cmtLikesView';
                                        else
                                            $classCmt   =   '';

                                    ?>
                                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $replyuser->w_comment_id ?>'></span>
                                    <span id="cmtNum_<?= $replyuser->w_comment_id ?>"> <?= $replyuser->likesCount ?></span>
                                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($replyuser->created_at)) ?></span>
                                </p>
                            </div>
                            <div  style="display:none; margin-top:10px" id="<?php echo "editReplyBlk_".$replyuser->w_comment_id ?>">	
                                <!--<a class="close" data-action="comment-close">X</a> -->
                                <?php $form = ActiveForm::begin(['action' =>['wish/update-comment?id='.$replyuser->w_comment_id]]); ?>				 
                                 <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2, 'value'=> $replyuser->comments ])->label(false) ?>			
                                 <?= $form->field($listcomments, 'w_id')->hiddeninput(['value'=>$replyuser->w_id])->label(false) ?>			
                                 <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$replyuser->parent_id])->label(false) ?>
                                 <div class="form-group">
                                        <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                                </div>					
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>	
                        <?php
                    }
                }?>
            </div>
        </div>
    <?php } } } ?>
</div>

<!-- modal NON financial Starts -->
<div class="modal fade" id="messagemodal1" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"> <?php if($model->non_pay_option == 0 ){ ?>Financial <?php }else{ echo "Non-Financial";} ?></h4>
            </div>
            <div class="modal-body" style="height: auto;">
                <div class="media" style="padding-left: 0px !important;">
                    <div class="media-left list-icon" style="padding-right: 0px ;">

                    </div>
                    <div class="media-body">
                        <!--<h4 class="media-heading">“ Wisher would like to receive this wish via ”
                        </h4>-->
                        <br>
                        <?php if($model->show_mail_status == 1){ ?>
                                <p>Mail to this address : <b><?=$model->show_mail?></b> </p>
                        <?php } ?>	
                        <?php if(($model->show_mail_status == 1) && ($model->show_person_status == 1)){ ?>
                            <p>or </p>
                        <?php } ?>	
                        <?php if($model->show_person_status == 1){ ?>
                            <p>In Person at this location :  <b><?=$model->show_person_street?> <?= $model->show_person_city ?>
                                <?= $model->show_person_state ?> <?= $model->show_person_zip ?> <?= $model->show_person_country ?></b></p>
                        <?php } ?>	
                        <?php if((($model->show_mail_status == 1) || ($model->show_person_status == 1) )&& ($model->show_other_status == 1)){ ?>
                                <p>or </p>
                        <?php } ?>	
                        <?php if($model->show_other_status == 1){ ?>
                                <p>Other : <b><?=$model->show_other_specify ?></b></p>
                        <?php } ?>	

                    </div>
                </div>
                </br>
                <div class="form-group">
                    <input type="checkbox" class="msg-check" name="i_agree_non_fulfilled" id="i_agree_non_fulfilled" value="1" > In accepting to grant this Wish, I agree to work with the Wisher to fulfill their Wish within 14 days, during which time this Wish will be marked as In Progress. After 14 days, it will be marked as Fulfilled.
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="send-msg-check btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- modal Ends -->




<!-- modal DEcider <div class="modal fade" id="messagemodal2" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Decide Later</h4>
          </div>
          <div class="modal-body">
                <div class="media">
                  <div class="media-left list-icon">

                  </div>
                  <div class="media-body">
                        <h4 class="media-heading">“ Wisher has not specified a delivery method for this wish. Please message the wisher to arrange wish delivery ”</h4>
                  </div>
                </div>
                </br>
                <div class="form-group">
                <label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
                <textarea class="msg form-control" name="msg-textarea" id="msg-textarea" rows="4"></textarea>
                </div>
                <div class="form-group">

                <input type="checkbox" class="msg" name="i_agree_decide" id="i_agree_decide" value="1" > I agree to fulfill this wish in the manner specified by the wisher and within one month of the date that I accept it as a grantor. In the meanwhile, this wish will be marked as "In Progress" and after one month, it will be marked as "Fulfilled". The Wisher should update or ressubmit their wish if it has not been fulfilled after one month. </input>
                </div>
          </div>
          <div class="modal-footer">
                <button type="button" class="send-msg-check-decide btn btn-primary">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
  </div>
</div>
<!-- modal Ends -->
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="messagemodalOne"  tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form id='project-form'>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Message</h4>
                </div>
                <div class="modal-body" style="height: auto;">
                    <div class="media">
                        <input type="hidden" name="wishid" id="wishid"  value="<?php echo $model->w_id;?>"/>

                        <input type="hidden" name="senduserid" id="senduserid"  value="<?php echo $model->wished_by;?>"/>
                    </div>
                    </br>
                    <div class="form-group" style="width: 35% !important;">
                        <label for="subject">Subject <span class='valid-star-color' >*</span></label>
                        <input type="text" id="subject" class="form-control" value="<?php echo $model->wish_title; ?>" rows="4" required>
                    </div>
                    <div class="form-group">
                        <label for="message">Enter Your Message <span class='valid-star-color' >*</span></label>
                        <textarea id="msgOne" class="form-control" rows="4"></textarea>
                    </div>
                    <div style="text-align: center">
                        <b>
                        (Please be as detailed as possible when messaging about a specific post)
                        </b>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="send-msgOne btn btn-primary">Send</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="https://momentjs.com/downloads/moment.min.js"></script>
<script>
    function test() {
        var url = '<?=Url::to(['site/login'])?>';
        window.location.href=url;
    }
</script>
<script>
    $(".send-msgOne").on("click",function(){
        var send_to = $("#senduserid").val();
        if($.trim(send_to) === "")
        {
            alert("Please Select To Sender Name.");
            return false;

        }
        var subject = $('#subject').val();
        if($.trim(subject) === "")
        {
            alert("Please check the subject.");
            return false;

        }
        var msg = $('#msgOne').val();
        if($.trim(msg) === "")
        {
            alert("Please check the message.");
            return false;

        }

        ///var send_to = $("#senduserid").val();
        var send_from = "<?=\Yii::$app->user->id?>";
        var wish_id = $('#wishid').val();
        $.ajax({
            url : '<?=Url::to(['account/send-message-inbox'])?>',
            type : 'POST',
            data : {
                subject:subject,
                msg:msg,
                send_from:send_from,
                send_to:send_to,
                wish_id:wish_id,
                send_date: moment().format('YYYY-MM-DD HH:mm:ss'),
            },
            success: function(response){
                location.reload();
            }
        });
    });
    $(document).ready(function(){
        var url =   document.referrer;
        if(url)
        {
            if(url.match(/.com(.[^]+)/))
            {
                var referrer =  url.match(/.com(.[^]+)/)[1];
                if(referrer == '/wish/update?id=<?=$model->w_id?>')
                    $('#success_txt').text('Your Wish Has Been Updated Successfully!').removeClass('hide');
                setTimeout(function(){
                    $('#success_txt').text('').addClass('hide');
                }, 5000);
            }
        }

        $(".shareIcons").each(function(){
            var elem = $(this);
            elem.jsSocials({
                showLabel: false,
                showCount: true,
                shares: ["facebook"
                    ,{
                        share: "pinterest",           // name of share
                        via: "simplywishes5244",
                        media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    },
                    {
                        share: "twitter",           // name of share
                        via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                        hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                    }],
                url : elem.attr("data_url"),
                text: elem.attr("data_text")
            });
        });

        $(".on-reply").click(function(){
                    var id = $(this).attr("for");

                    if($("#replylist_"+id).is(':hidden'))
                    {
                            $("#replylist_"+id).show();
                    } else {
                            $("#replylist_"+id).hide();
                    }

        });
        $('.editComment').on('click', function(){
            var id = $(this).data("id");

            $('#comment_'+id).hide();
            if($("#editCommentBlk_"+id).is(':hidden'))
            {
                $("#editCommentBlk_"+id).show();
            } else {
                $("#editCommentBlk_"+id).hide();
            }
        });

        $('.editReplyComment').on('click', function(){
            var id = $(this).data("id");

            $('#replyComment_'+id).hide();
            if($("#editReplyBlk_"+id).is(':hidden'))
            {
                $("#editReplyBlk_"+id).show();
            } else {
                $("#editReplyBlk_"+id).hide();
            }
        });
	/* $(".close").click(function(){
		$(this).parent().hide();
        }); */
    });

    $('#back').on('click', function(){
        var url =   document.referrer;

        if(url && url.match(/.com(.[^]+)/))
        {
            var referrer =  url.match(/.com(.[^]+)/)[1];
            var url1 =   "/wish/view?id=<?= $model->w_id ?>";
            var url2 =   "/wish/update?id=<?= $model->w_id ?>";

            if (referrer == url1 || referrer == url2) {
                window.location.href = "<?=\Yii::$app->homeUrl?>wish/index#search_wish";
            }
            else
                window.location.href =   document.referrer;
        }
        else
            window.location.href = "<?=\Yii::$app->homeUrl?>wish/index#search_wish";
    });

    $('.report-wish').on('click', function(){
        var check = confirm( " Are you sure you want to report this wish? ");//Reporting this wish will hide this and any other wishes from this author as well as the author’s profile from you and will trigger a full review by SimplyWishes.");
		if(check){
            console.log("report");
            var wish_id = <?=$model->w_id?>; //12;//$(this).attr("data-w_id");

            var user_id = <?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>;
            var wished_by = <?= $model->wished_by ?>;
            $.ajax({
                url : '<?=Url::to(['wish/report-content'])?>',
                type: 'GET',
                data: {
                    w_id:wish_id,
                    content_type:'wish',
                    report_user: user_id,
                    reported_user:wished_by,
                    content_id:wish_id,
                },
                success:function(data){
                    console.log(data);
                    if (data == "savedok"){
                        window.location.reload();
                    }
                }
            });
        }
    });



    $('.report-comment').on('click', function(){
        var comment_id = $(this).attr("for");
        var user_id = $(this).attr("by");
        var comment = $(this).attr("comment");
        var loggedUser = <?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>;

        if (user_id == loggedUser){
            return false;
        }

        var reportButton = $(this);
        if (reportButton.hasClass("disabled")){
            return false;
        }
        var check = confirm( "Are you sure you want to report this comment? \n Reporting this comment will hide this and any other comments from this author as well as \n the author’s profile from you and will trigger a full review by SimplyWishes.");
		if(check){
            $.ajax({
                url : '<?=Url::to(['wish/report-content'])?>',
                type: 'GET',
                data: {
                    w_id:comment_id,
                    content_type:'wish_comment',
                    report_user:<?= (\Yii::$app->user->isGuest) ?  0 :  \Yii::$app->user->id  ?>,
                    reported_user:user_id,
                    content_id:comment_id,
                    comment: comment,
                },
                success:function(data){
                    console.log(data);
                    if (data == "savedok"){
                        reportButton.addClass("disabled");
                        // $("div#parent_comment_"+comment_id).remove();
                    }
                }
            });
        }
    });


	
    $(document).on('click', '.deletecheck', function(){ 
        if(confirm("Are Sure To Delete this Wish ?"))
        {
            $.ajax({
                url : '<?=Url::to(['wish/ajax-delete'])?>',
                type: 'POST',
                data: {id:<?= $model->w_id ?>},
                success:function(data){				
                    window.location.href="<?= Url::to(['account/my-account'],true); ?>"; 
                }	
            });

        }
        else{
            return false;
        }
    });	
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.cmtLikesView', function(){
    var val =   $(this).next('span').html();
    if(val <= 0)
        return false;

    var wish_id = $(this).data('id');
    if($.trim(wish_id) !== "" )
    {
        if(Wish.ajax)
            return false;

        Wish.ajax   =   true;
        $.ajax({
            url : '<?=Url::to(['wish/comment-likes-view'])?>',
            type: 'GET',
            data: {w_id:wish_id},
            success:function(data){
                if(data)
                {
                    var tpl =   '';
                    var items   =   JSON.parse(data);
                    $.each(items, function(k,v){
                       tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                    });

                    $('.modal-body').append(tpl);
                    $('#likeModal').modal({backdrop: 'static', keyboard: false});
                    $('#likeModal').modal('show');

                    $('.closeModal').on('click', function(e){
                        e.preventDefault();
                        $('.modal-body').html('');
                        $('#likeModal').modal('hide');
                    });
                }
            },
            complete: function(){
                Wish.ajax   =   false;
            }
        });
    }
});

$(document).on('click', '.loveComment', function(){
    var s_id = $(this).attr("for");
    var elem = $(this);
    $.ajax({
        url : '<?=Url::to(['wish/like-comment'])?>',
        type: 'GET',
        data: {s_id:s_id},
        success:function(data){
            if(data == "added"){
                elem.css('color', '#B23535');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) + parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
            if(data == "removed"){
                elem.css('color', '#66cc66');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) - parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
        }
    });
});

    var isVisible = false;
    var clickedAway = false;

    $(document).on("click", ".listesinside", function() {
        $(function(){
            $('.listesinside').popover({   
                html: true,
                content: function () {
                    var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
                    return clone;
                }
            }).click(function(e) {
                e.preventDefault();
                clickedAway = false;
                isVisible = true;
            });
        });
    });

    $(document).click(function (e) {
        if (isVisible & clickedAway) {
            $('.listesinside').popover('hide');
            isVisible = clickedAway = false;
        } else {
            clickedAway = true;
        }
    });

    $('body').on('hidden.bs.popover', function (e) {
        $(e.target).data("bs.popover").inState.click = false;
    });

    $(".send-msg-check").on("click",function(){
        if(Wish.ajax)
            return false;
        
        var processstatus = 0;

        if($('#i_agree_non_fulfilled').prop("checked") == true){
            var processstatus = 1;		
        }
        else if($('#i_agree_non_fulfilled').prop("checked") == false){
            alert("Please Accept The Agreement");
            return false;
        }

        var wish_id = "<?=$model->w_id?>";
        var send_to = "<?=$model->wished_by?>";
        var send_from = "<?=\Yii::$app->user->id?>";
        var send_message = 1;

        Wish.ajax   =   true;
        $('#overlay').show();
        
        $.ajax({
           url : '<?=Url::to(['wish/process-wish'])?>',
           type : 'POST',
           data : {processstatus:processstatus,wish_id:wish_id,send_message:send_message},
           success: function(response){
                $.ajax({
                   url : '<?=Url::to(['account/send-message-wishes-contact-details'])?>',
                   type : 'POST',
                   data : {send_from:send_from,send_to:send_to,wish_id:wish_id },
                   success: function(response){
                        $('#messagemodal1').modal('hide');
                        location.reload();
                    }    
                }); 
            },
            complete: function(){
                Wish.ajax   =   false;
                $('#overlay').hide();
            }
        });

    });


    $("#Resubmit_wishes").on("click",function(){
        var wish_id = "<?=$model->w_id?>";
        var userid  = "<?=\Yii::$app->user->id?>";
        if (confirm('Are You Sure to Re-Submit this wish again.')) {
            $.ajax({
                url : '<?=Url::to(['wish/resubmit-process-wish'])?>',
                type : 'POST',
                data : { wish_id:wish_id,userid:userid},
                success: function(response){
                    alert("This Wish Has Been Re-Submitted Successfully.");
                    location.reload();
                }
            });
        } 
    });	

    $("#grant_progress_wishes").on("click",function(){
        var wish_id = "<?=$model->w_id?>";
        var userid  = "<?=\Yii::$app->user->id?>";

        if (confirm('Are You Sure Your Wish Has been Fulfilled?')) {
            $.ajax({
                url : '<?=Url::to(['wish/grant-process-wish'])?>',
                type : 'POST',
                data : { wish_id:wish_id,userid:userid},
                success: function(response){
                    alert("This Wish Has Been Fulfilled Successfully.");
                    location.reload();	
                }
           }); 
        }
    });
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
        var val =   $(this).find('span').html();
        if(val <= 0)
            return false;

        var wish_id = $(this).data('id');
        if($.trim(wish_id) !== "" )
        {
            if(Wish.ajax)
                return false;

            Wish.ajax   =   true;
            $.ajax({
                url : '<?=Url::to(['wish/likes-view'])?>',
                type: 'GET',
                data: {w_id:wish_id},
                success:function(data){
                    if(data)
                    {
                        var tpl =   '';
                        var items   =   JSON.parse(data);
                        $.each(items, function(k,v){
                           tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                        });

                        $('.modal-body').append(tpl);
                        $('#likeModal').modal({backdrop: 'static', keyboard: false});
                        $('#likeModal').modal('show');

                        $('.closeModal').on('click', function(e){
                            e.preventDefault();
                            $('.modal-body').html('');
                            $('#likeModal').modal('hide');
                        });
                    }
                },
                complete: function(){
                    Wish.ajax   =   false;
                }
            });
        }
    });
        
    var Wish    =   {
        ajax    :   false
    };
</script>
<script>
    $(document).on('click', '.like-wish, .fav-wish', function(){
        console.log('dasd');
        var wish_id = $(this).attr("data-w_id");
        var type = $(this).attr("data-a_type");
        var elem = $(this);
        $.ajax({
            url : '<?=Url::to(['wish/like'])?>',
            type: 'GET',
            data: {w_id:wish_id,type:type},
            success:function(data){
                console.log(data);
                if(data == "added"){
                    if(type=="fav"){
                        elem.removeClass("txt-smp-orange");
                        elem.addClass("txt-smp-blue");
                    }
                    if(type=="like"){
                        elem.css('color', '#B23535');
                        var likecmt = $("#likecmt_"+wish_id).text();
                        likecmt = parseInt(likecmt) + parseInt(1);
                        $("#likecmt_"+wish_id).text(likecmt);
                        $("#likecmt_"+wish_id).parent('.likesBlk').removeClass('hide');
                    }
                }
                if(data == "removed"){
                    if(type=="fav"){
                        elem.addClass("txt-smp-orange");
                        elem.removeClass("txt-smp-blue");
                    }
                    if(type=="like"){
                        elem.css('color', '#fff');
                        var likecmt = $("#likecmt_"+wish_id).text();
                        likecmt = parseInt(likecmt) - parseInt(1);
                        $("#likecmt_"+wish_id).text(likecmt);
                        if(likecmt == 0)
                            $("#likecmt_"+wish_id).parent('.likesBlk').addClass('hide');
                    }
                }
            }
        });
    });
</script>

