<?php
use yii\helpers\Url;
?>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/masonry.js" type="text/javascript"></script>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/imagesloaded.js" type="text/javascript"></script>
<?php 
if( ! empty($user) && ! empty($profile))    
    echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile]);
else
    echo '<div class="col-md-3"></div>'
?>
    <div class="col-md-8" id="scroll-evt">
        <div class="row" style="margin: 3%;">
            <div class="col-md-6 pull-right">
                <div class="input-group">
                    <input name="searh_field" id="searh_field" type="text" class="form-control" placeholder="search by keyword or location...">
                    <span class="input-group-btn">
                        <button class="search-wish btn btn-default" type="button">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs smp-mg-bottom" role="tablist">
            <li><a href="<?=\Yii::$app->homeUrl?>wish/popular#search_wish">Most Popular</a></li>
            <li><a href="<?=\Yii::$app->homeUrl?>wish/granted#search_wish">Granted</a></li>
            <li><a href="<?=\Yii::$app->homeUrl?>wish/progress#search_wish">In Progress</a></li>
            <?php 
            if($cat_id == 'financial' &&  $type=='wish')
            { ?>
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true">Current Wishes <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index#search_wish">All Wishes</a></li>
                        <li class="active"><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=financial#search_wish">Financial Wishes</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=non-financial#search_wish">Non-Financial Wishes</a></li>                       
                    </ul>
                </li>			
            <?php }
            else if($cat_id == 'non-financial' && $type=='wish')
            { ?>
                <li class="dropdown <?php echo(Yii::$app->controller->id=='wish')?'active':''; ?>">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true">Current Wishes <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index#search_wish">All Wishes</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=financial#search_wish">Financial Wishes</a></li>
                        <li class="active"><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=non-financial#search_wish">Non-Financial Wishes</a></li>                       
                    </ul>
                </li>
            <?php }
            else if($type=='wish')
            { ?>
                <li class="dropdown active">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true">Current Wishes <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="active"><a href="<?=\Yii::$app->homeUrl?>wish/index#search_wish">All Wishes</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=financial#search_wish">Financial Wishes</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>wish/index?cat_id=non-financial#search_wish">Non-Financial Wishes</a></li>                       
                    </ul>
                </li>
            <?php } ?>
            <?php
            if($cat_id == 'financial' &&  $type=='donation')
            { ?>
                <li class="dropdown active"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true">Current Donations <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li ><a href="<?=\Yii::$app->homeUrl?>donation/index#search_donation">All Donations</a></li>
                        <li class="active"><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=financial#search_donation">Financial Donations</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=non-financial#search_donation">Non-Financial Donations</a></li>
                    </ul>
                </li>
            <?php }elseif($cat_id == 'non-financial' &&  $type=='donation'){?>
                <li class="dropdown active"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true">Current Donations <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=\Yii::$app->homeUrl?>donation/index#search_donation">All Donations</a></li>
                        <li><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=financial#search_donation">Financial Donations</a></li>
                        <li class="active"><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=non-financial#search_donation">Non-Financial Donations</a></li>
                    </ul>
                </li>
            <?php }else {?>
            <li class="dropdown <?php echo(Yii::$app->controller->id=='donation')?'active':''; ?>">
                <a class="dropdown-toggle" data-toggle="dropdown"  href="#" aria-haspopup="true">Current Donations <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li class="active"><a href="<?=\Yii::$app->homeUrl?>donation/index#search_donation">All Donations</a></li>
                    <li><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=financial#search_donation">Financial Donations</a></li>
                    <li><a href="<?=\Yii::$app->homeUrl?>donation/index?cat_id=non-financial#search_donation">Non-Financial Donations</a></li>
                </ul>
                </li>
            <?php }?>
	    </ul>
        <div class="tab-content">
            <div class="tab-pane" id="mostpopular">					
            </div>
            <div class="tab-pane" id="fullfilled">
            </div>
            <div class="tab-pane active"  id="current">
                <div class="grid hide"  data-masonry='{ "itemSelector": ".grid-item" }' id="wishes_blk">
                    <?php
                    if( ! empty($dataProvider->models))
                    {
                        foreach($dataProvider->models as $wish){
                            echo $wish->wishAsCard;
                        }
                    }
                    else
                        echo "No Wishes available in this category!";
?>
                </div>
            </div>

        </div>
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
	<script>
  $(window).on('load', function() {
        $("#loader_img").hide();
  	var win = $(window);
  	var page = 1;
	var page2 = 1;
  	var $container = $('.grid');
        $('#wishes_blk').removeClass('hide');
  	$container.masonry();
  	// Each time the user scrolls
  	win.scroll(function() {
		var scroll_top = Math.round(win.scrollTop());
  		// End of the document reached?
		if(parseInt(page) == parseInt(page2)){
  		//if ($(document).height() - win.height()-1 == scroll_top ) {
		//  previous line if($(win).scrollTop() + $(win).height() == $(document).height()){
		if($(win).scrollTop() + $(win).height() + parseInt(50) >= $(document).height()){
			page2 = page2+1;
			$("#loader_img").show();
  			$.ajax({
  				url: '<?=Url::to(['wish/scroll','cat_id'=>$cat_id], true);?>',
  				dataType: 'html',
  				data: {'page':page},
  				success: function(html) {
  					var el = $(html);
  					//$(".grid").append(el).masonry( 'appended', el, false );
  					var $newElems = $( html ).css({ opacity: 0 });
  					$newElems.imagesLoaded(function(){
  						$newElems.animate({ opacity: 1 });
  						$(".grid").append(el);
						$(".shareIcons").each(function(){
                                                    var elem = $(this);
                                                            elem.jsSocials({
                                                            showLabel: false,
                                                            showCount: true,
                                                            shares: ["facebook",{
                                                                share: "pinterest",           // name of share
                                                                via: "simplywishes5244",
                                                                media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                                                                hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                                                            },
                                                                {
                                                                    share: "twitter",           // name of share
                                                                    via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                                                                    hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
                                                                }],
                                                            url : elem.attr("data_url"),
                                                            text: elem.attr("data_text"),
                                                    });
                                                });
						$(".grid").masonry( 'appended', el, true )
  					});
					if(html == '')
						page = false;
					else
						page = page+1;
					
					$("#loader_img").hide();
  				},
  				error:function(){
					$("#loader_img").hide();
  				}
  			});
			
  			//$container.masonry();
  			//page = page+1;
	}}
		});
                
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');

	});
        
$("li[data-id='search_wish']").addClass("active");
  // var imageToShare = jQuery('div.content p img:first').attr('src');
  //
  // jsSocials.setDefaults("pinterest", {
  //     media: imageToShare
  // });
$(".shareIcons").each(function(){
    var elem = $(this);
        elem.jsSocials({
        showLabel: false,
            showCount: true,
        shares: [
            "facebook"
            ,{
                share: "pinterest",           // name of share
                via: "simplywishes5244",
                media:elem.attr("data_image"),// custom twitter sharing param 'via' (optional)
                hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
            },
            {
                    share: "twitter",           // name of share
                    via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                    hashtags: "SimplyWishes,Wish,Dream,Connect"    // custom twitter sharing param 'hashtags' (optional)
            }],
        url : elem.attr("data_url"),
        text: elem.attr("data_text"),

    });
});
	$(document).on('click', '.like-wish, .fav-wish', function(){ 
	//$(".like-wish, .fav-wish").on("click",function(){
		var wish_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['wish/like'])?>',
			type: 'GET',
			data: {w_id:wish_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+wish_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+wish_id).text(likecmt);
                        $("#likecmt_"+wish_id).parent('.likesBlk').removeClass('hide');
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+wish_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+wish_id).text(likecmt);
                                                if(likecmt == 0)
                                                    $("#likecmt_"+wish_id).parent('.likesBlk').addClass('hide');
					}
				}
			}
		});
	});
	var cat_id = '<?=$cat_id?>';
        cat_id = parseInt(cat_id);
	if(Number.isInteger(cat_id)){
            $('#demo').addClass('in');
            $('#cat_'+cat_id).addClass('selected');
	}
	//search srcipt
	$(".search-wish").on("click",function(){		
		if($("input[name=searh_field]").val() != ''){
			var url = "<?=Url::to(['wish/search'])?>";
			window.location.href = url+"?match="+$("input[name=searh_field]").val();
		}
		else{
			var url = "<?=Url::to(['wish/index#search_wish'])?>";
			window.location.href = url;			
		}		
	});
	
	$('#searh_field').keypress(function (e) {
		  if (e.which == 13) {
				$( ".search-wish" ).trigger( "click" );
		  }
	});
	
var isVisible = false;
var clickedAway = false;
$(document).on("click", ".listesinside", function() {
    $(function(){
        $('.listesinside').popover({   
            html: true,
            content: function () {
                var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
                return clone;
            }
        }).click(function(e) {
            e.preventDefault();
            clickedAway = false;
            isVisible = true;
        });
    });
 
});

$(document).click(function (e) {
    if (isVisible & clickedAway) {
        $('.listesinside').popover('hide');
        isVisible = clickedAway = false;
    } else {
        clickedAway = true;
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                $.ajax({
                    url : '<?=Url::to(['wish/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });

        $('ul.nav li.dropdown').click(function () {
            if($(this).find('.dropdown-menu').is(":visible")) {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            } else {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }
            
        });       

/* $(document).on('click','.jssocials-shares',function(){
		 $('.listesinside').popover('hide');
	}); */
	
/* $(document).on("click", ".report-img", function() {
	var wish_id = $(this).attr("data-id");
	 if($.trim(wish_id) !== "" )
	 {
		$.ajax({
			url : '<?=Url::to(['wish/report'])?>',
			type: 'GET',
			data: {w_id:wish_id},
			success:function(data){
				if($.trim(data) == "added")
				{
					alert(" Thanks For your Report. ");
				}
				console.log(data);
			}
		});
	 }
}); */

</script>
