<?php 
use yii\helpers\Url;
use app\models\UserProfile;
?>

<div role="tabpanel" class="tab-pane active smp-igrant">
    <div class="row iwish_tpl">
        <ul class="nav nav-pills nav-justified smp-mg-bottom" role="tablist">
            <li role="presentation">
                <a href="<?=\Yii::$app->homeUrl?>wish/top-wishers#top_wishers" role="tab">Wishers</a>
            </li>
             <li role="presentation">
                <a href="<?=\Yii::$app->homeUrl?>wish/top-granters#top_wishers" role="tab">Granters</a>
            </li>
            <li role="presentation">
                <a href="<?=\Yii::$app->homeUrl?>wish/top-donors#top_donors" role="tab">Donors</a>
            </li>
        </ul>
        <div class='tab-content' id='top-wishers'>
            <div class="input-group smp-mg-bottom">
                <input name="searh_field" id="searh_field" type="text" class="form-control" placeholder="search..">
                <span class="input-group-btn">
                  <button class="search-wish btn btn-default" type="button">
                        <span class="glyphicon glyphicon-search"></span>
                  </button>
                </span>
            </div>
            <div>
                <?php
                if(count($dataProvider->models) == 0)
                    echo "No user found";
                else
                {
                    foreach($dataProvider->models as $model){
                        $userProfile = UserProfile::find()->where(['user_id'=>$model->wished_by])->one();
                        echo '<div class="iwishItems"><a href="'.Url::to(['account/profile','id'=>$userProfile->user_id]).'"><img src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$userProfile->profile_image.'"/></a>';
                        echo '<p>'.$userProfile->firstname.'</p></div>';
                    }
                }?>
            </div>
        </div>
    </div>
</div>

<script>
    //search srcipt
    $(".search-wish").on("click",function(){		
        if($("input[name=searh_field]").val() != ''){
            var url = "<?=Url::to(['wish/search-user'])?>";
            window.location.href = url+"?match="+$("input[name=searh_field]").val();
        }
        else{
            var url = "<?=Url::to(['wish/top-wishers#top_wishers'])?>";
            window.location.href = url;			
        }		
    });

    $('#searh_field').keypress(function (e) {
        if (e.which == 13) {
            $( ".search-wish" ).trigger( "click" );
        }
    });
</script>