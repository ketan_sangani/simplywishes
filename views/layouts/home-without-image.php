
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\models\UserProfile;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Message;
use app\controllers\Utils;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title>SimplyWishes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" type="image/png" href="<?= Yii::$app->homeUrl ?>web/images/favicon.png?v=1"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
</head>
<style>

    @media screen and (min-width: 767px) and (max-width: 991px) {
        .nav > li > a {

            padding: 6px 8px !important
        }
        .smp-head {
            height: 136px !important;
        }
        .navbar-collapse {
            margin-top: 14%;
        }
    }
</style>
<body>
<?php $this->beginBody() ?>
<!--***** Header Starts*****-->
<div class="navbar navbar-fixed-top smp-head">
    <div class="container nav-without-image-hmepge">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"
                    aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="margin: -7px 8px;" href="<?= Yii::$app->homeUrl ?>"><img
                        class="img-responsive" src="<?= Yii::$app->homeUrl ?>web/images/logo.png" width="150"></a>
        </div>
        <?php $detect = new Mobile_Detect; ?>
        <div class="collapse navbar-collapse" style="max-height: 509px !important;">
            <ul class="nav navbar-nav navbar-right smp-pills">
                <?php
                $properties = Utils::getUserProperties(\Yii::$app->user->id);

                if (!$detect->isMobile() && !$detect->isTablet()) { ?>
                    <li data-id="home"><a href="<?= Yii::$app->homeUrl ?>" style="height: 30px;"><span
                                    style="margin-top: -10px;" class="glyphicon glyphicon-home"></span></a></li>
                <?php } ?>
                <li data-id="post_wish">

                    <a class=" post-wish <?php if (!$properties['can_create_wish']) echo ' disabled '; ?>"
                       href="<?= Yii::$app->homeUrl ?>wish/create#post_wish">Make a Wish</a>

                </li>

                <?php if (\Yii::$app->user->id == 590 || \Yii::$app->user->id == 559 || 1 === 1) { ?>
                    <li data-id="post_donation">

                        <a class="post-donation  <?php if (!$properties['can_create_donation']) echo 'disabled '; ?>"
                           href="<?= Yii::$app->homeUrl ?>donation/create#post_donation">Donate an Item</a>

                    </li>
                <?php } ?>


                <li data-id="search_wish"><a href="<?= Yii::$app->homeUrl ?>wish/index#search_wish">Active Wishes &
                        Donations</a></li>
                <li data-id="forum">
                    <?php if (isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')) { ?>
                        <a href="<?= Yii::$app->homeUrl ?>editorial/index#forum">Forum</a>
                    <?php } else { ?>
                        <a href="<?= Yii::$app->homeUrl ?>editorial/editorial#forum">Forum</a>
                    <?php } ?>
                </li>
                <?php if (!\Yii::$app->user->isGuest) {
                    $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one(); if (!$detect->isMobile() && !$detect->isTablet()) { ?>
                    <li data-id="edt_home" class="dropdown mainNav" class="active"><?php } ?>

                        <?php
                        $detect = new Mobile_Detect;
                    $selectReported = (\app\models\ReportContent::find()->select('reported_user')->where(
                        [
                            'report_user' => \Yii::$app->user->id,
                            'report_type' => 'user'
                        ]
                    ));
                    $inbox_messages1 = Message::find()
                        ->where(['parent_id'=>0])
                        //->andwhere(['NOT LIKE','delete_status',$deleteduser])
                        ->andwhere(['OR',['reply_recipient_id' => \Yii::$app->user->id],['reply_sender_id' => \Yii::$app->user->id],['and',['recipient_id'=>\Yii::$app->user->id],['reply_recipient_id' => 0 ]]])
                        ->andwhere(['not in','recipient_id', $selectReported])
                        ->orderBy('created_at DESC')
                        //echo  $inbox_messages->createCommand()->getRawSql();exit;
                        ->all();
                    $i = 0;
                    if(!empty($inbox_messages1)) {
                        foreach ($inbox_messages1 as $messages) {
                            $deleteusers1 = $messages->delete_status;
                            $deleteusers = explode(',', $messages->delete_status);

                            if ($deleteusers1 == '') {
                                $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                                $threadsmsg = [];
                                if (isset($thread_messages) && !empty($thread_messages)) {
                                    foreach ($thread_messages as $messages2) {
                                        $threadsmsg[$messages2->m_id] = [
                                            'm_id' => $messages2->m_id,
                                            'text' => $messages2->text,
                                            'created_at' => $messages2->created_at,
                                            'send_by' => $messages2->sender_id,
                                        ];
                                    }
                                }
                                $other_id = $messages->sender_id;
                                if ($messages->reply_recipient_id != 0) {
                                    if ($messages->reply_recipient_id != \Yii::$app->user->id)
                                        $other_id = $messages->reply_recipient_id;
                                    else if ($messages->reply_sender_id != \Yii::$app->user->id)
                                        $other_id = $messages->reply_sender_id;
                                }

                                $threads[$messages->m_id] = [
                                    'm_id' => $messages->m_id,
                                    'w_id' => $messages->w_id,
                                    'donation_id' => $messages->donation_id,
                                    'read_text' => $messages->read_text,
                                    'subject' => $messages->subject,
                                    'text' => $messages->text,
                                    'created_at' => $messages->created_at,
                                    'sender_id' => $messages->sender_id,
                                    'recipient_id' => $other_id,
                                    'reply_recipient_id' => $messages->reply_recipient_id
                                ];

                                if (isset($thread_messages) && !empty($thread_messages)) {
                                    $threads[$messages->m_id]['threads'] = $threadsmsg;
                                }
                            } else {
                                if (!in_array(\Yii::$app->user->id, $deleteusers)) {
                                    $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                                    $threadsmsg = [];
                                    if (isset($thread_messages) && !empty($thread_messages)) {
                                        foreach ($thread_messages as $messages2) {
                                            $threadsmsg[$messages2->m_id] = [
                                                'm_id' => $messages2->m_id,
                                                'text' => $messages2->text,
                                                'created_at' => $messages2->created_at,
                                                'send_by' => $messages2->sender_id,
                                            ];
                                        }
                                    }
                                    $other_id = $messages->sender_id;
                                    if ($messages->reply_recipient_id != 0) {
                                        if ($messages->reply_recipient_id != \Yii::$app->user->id)
                                            $other_id = $messages->reply_recipient_id;
                                        else if ($messages->reply_sender_id != \Yii::$app->user->id)
                                            $other_id = $messages->reply_sender_id;
                                    }

                                    $threads[$messages->m_id] = [
                                        'm_id' => $messages->m_id,
                                        'w_id' => $messages->w_id,
                                        'donation_id' => $messages->donation_id,
                                        'subject' => $messages->subject,
                                        'read_text' => $messages->read_text,
                                        'text' => $messages->text,
                                        'created_at' => $messages->created_at,
                                        'sender_id' => $messages->sender_id,
                                        'recipient_id' => $other_id,
                                        'reply_recipient_id' => $messages->reply_recipient_id
                                    ];

                                    if (isset($thread_messages) && !empty($thread_messages)) {
                                        $threads[$messages->m_id]['threads'] = $threadsmsg;
                                    }
                                }


                            }

                        }
                        if(isset($threads)) {
                            foreach ($threads as $key => $msg) {
                                $reply = "";
                                if (isset($msg['threads']) && !empty($msg['threads'])) {
                                    // echo "1".$msg['recipient_id'];
                                    $profile = UserProfile::find()->where(['user_id' => $msg['recipient_id']])->one();//->where(['not in','user_id', $selectReported])->one();
                                } else {
                                    // echo "2".$msg['sender_id'];
                                    $profile = UserProfile::find()->where(['user_id' => $msg['sender_id']])->one();//->where(['not in','user_id', $selectReported]);//->one();
                                    // echo   $profile->createCommand()->getRawSql();exit;
                                    // print_r($profile);
                                }
                                // print_r($msg);
                                //print_r($profile);
                                if (!empty($profile)) {
                                    if ($msg['read_text'] == 0 && ($msg['reply_recipient_id'] == \Yii::$app->user->id || $msg['reply_recipient_id'] == 0)) {
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                        // echo $inbox_messages->createCommand()->getRawSql();exit;

                        if ($i > 0) { ?>
                            <span class="notification"><?php echo $i; ?></span>
                        <?php }
                        if (!$detect->isMobile() && !$detect->isTablet()) { ?>
                            <a href="<?= Yii::$app->homeUrl ?>site/index-home#edt_home"
                               style="display: inline-block; padding-right: 0px;">
                                Hi, <?php
                                $user_id = \Yii::$app->user->identity->id;
                                $find_fname = UserProfile::find()->where(['user_id'=>$user_id])->select('firstname')->one();
                                ?><?php echo  $find_fname->firstname;  ?><span class="caret"></span></a>
                            <?php $profile1 = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one(); if (!empty($profile1->profile_image)) { ?>
                                <img src="<?= \Yii::$app->homeUrl ?>web/uploads/users/<?php echo $profile1->profile_image ?>?v=<?= strtotime('now') ?>"
                                     class="profile-img" width='20px' height='20px'>
                            <?php } ?>
                            <ul class="dropdown-menu nav nav-stacked" style="background-color: #014e76 !important;">
                                <li><a href="<?= Yii::$app->homeUrl ?>account/inbox-message"><i
                                                class="fa fa-inbox fa-lg"></i>
                                        Inbox
                                        <?php if ($i > 0) { ?>
                                            <span class="inboxNotification"><?php echo $i; ?></span>
                                        <?php } ?>
                                    </a></li>
                                <li><a href="<?= Yii::$app->homeUrl ?>account/my-account"><i class="fa fa-heart"></i>My
                                        Wishes & Donations</a></li>
                                <li><a href="<?= Yii::$app->homeUrl ?>wish/my-drafts"><i
                                                class="fa fa-window-restore"></i>My Wish Drafts</a></li>
                                <li><a href="<?= Yii::$app->homeUrl ?>donation/my-drafts"><i
                                                class="fa fa-window-restore"></i>My Donation Drafts</a></li>
                                <li><a href="<?= Yii::$app->homeUrl ?>site/index-home#edt_home"><i class="fa fa-user"
                                                                                          aria-hidden="true"></i>My Profile</a>
                                <!--<li><a href="<?/*= Yii::$app->homeUrl */?>account/my-friend"><i class="fa fa-users"></i>Friends</a>
                                </li>
                                <li><a href="<?/*= Yii::$app->homeUrl */?>account/my-saved"><i class="fa fa-save fa-lg"></i>Saved
                                        Wishes & Donations</a></li>
                                <li><a href="<?/*= Yii::$app->homeUrl */?>happy-stories/create"><i
                                                class="fa fa-commenting"></i>Tell Your Happy Story</a></li>
                                <li><a href="<?/*= Yii::$app->homeUrl */?>happy-stories/my-story"><i
                                                class="fa fa-smile-o"></i>My Happy Stories</a></li>-->
                                <?php if (isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')) { ?>
                                    <li><a href="<?= Yii::$app->homeUrl ?>happy-stories/permission"><i
                                                    class="fa fa-list-alt"></i>Stories Approval</a></li>
                                    <li><a href="<?= Yii::$app->homeUrl ?>happy-stories/content-management"><i
                                                    class="fa fa-list-alt"></i>Content Management</a></li>
                                    <li><a href="<?= Yii::$app->homeUrl ?>mail-content/index"><i
                                                    class="fa fa-clipboard"></i>Mail Content</a></li>
                                <?php } ?>
                                <!--<li><a href="<?/*= Yii::$app->homeUrl */?>account/edit-account"><i
                                                class="fa fa-user-circle"></i> Account Info</a></li>-->
                                <li>
                                    <a href="#">
                                        <?php echo Html::beginForm(['/site/logout'], 'post')
                                            . Html::submitButton(
                                                '<i class="glyphicon glyphicon-log-out"></i>Logout',
                                                ['class' => 'a-button','style'=>'margin-left:-3px;']
                                            )
                                            . Html::endForm(); ?>
                                    </a>
                                </li>
                            </ul>
                        <?php } else {?>
                            <li class="dropdown explore">
                                <a class="myprofile-dropdown" href="<?= Yii::$app->homeUrl ?>site/index-home#edt_home"
                                   style="display: inline-block; padding-right: 0px;">
                                    Hi, <?php
                                    $user_id = \Yii::$app->user->identity->id;
                                    $find_fname = UserProfile::find()->where(['user_id'=>$user_id])->select('firstname')->one();
                                    ?><?php echo  $find_fname->firstname;  ?><span class="caret"></span></a>
                                <ul class="myprofile-option" style="background-color: #014e76 !important;">
                                    <li class="myprofileop"><a  class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>account/inbox-message"><i
                                                    class="fa fa-inbox fa-lg"></i>
                                            Inbox
                                            <?php if ($i > 0) { ?>
                                                <span class="inboxNotification"><?php echo $i; ?></span>
                                            <?php } ?>
                                        </a></li>
                                    <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>editorial/editorial#forum"><i
                                                    class="fa fa-lightbulb-o"></i>Forum</a></li>

                                    <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>account/my-account"><i class="fa fa-heart"></i>My
                                            Wishes & Donations</a></li>
                                    <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>wish/my-drafts"><i
                                                    class="fa fa-window-restore"></i>My Wish Drafts</a></li>
                                    <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>donation/my-drafts"><i
                                                    class="fa fa-window-restore"></i>My Donation Drafts</a></li>
                                    <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>site/index-home#edt_home"><i class="fa fa-user"
                                                                                              aria-hidden="true"></i>My Profile</a>
                                    <!--<li><a href="<?/*= Yii::$app->homeUrl */?>account/my-friend"><i class="fa fa-users"></i>Friends</a>
                                    </li>
                                    <li><a href="<?/*= Yii::$app->homeUrl */?>account/my-saved"><i class="fa fa-save"></i>Saved
                                            Wishes & Donations</a></li>
                                    <li><a href="<?/*= Yii::$app->homeUrl */?>happy-stories/create"><i
                                                    class="fa fa-commenting"></i>Tell Your Happy Story</a></li>
                                    <li><a href="<?/*= Yii::$app->homeUrl */?>happy-stories/my-story"><i
                                                    class="fa fa-smile-o"></i>My Happy Stories</a></li>-->
                                    <?php if (isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')) { ?>
                                        <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>happy-stories/permission"><i
                                                        class="fa fa-list-alt"></i>Stories Approval</a></li>
                                        <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>happy-stories/content-management"><i
                                                        class="fa fa-list-alt"></i>Content Management</a></li>
                                        <li class="myprofileop"><a class="myprofileophyp" href="<?= Yii::$app->homeUrl ?>mail-content/index"><i
                                                        class="fa fa-clipboard"></i>Mail Content</a></li>
                                    <?php } ?>
                                    <!--<li><a href="<?/*= Yii::$app->homeUrl */?>account/edit-account"><i
                                                    class="fa fa-user-circle"></i> Account Info</a></li>-->
                                    <li class="myprofileop">
                                        <a href="#" class="myprofileophyp" style="height: 35px !important;padding: 0 !important;">
                                            <?php echo Html::beginForm(['/site/logout'], 'post')
                                                . Html::submitButton(
                                                    '<i class="glyphicon glyphicon-log-out"></i>Logout',
                                                    ['class' => 'a-button','style'=>'margin-left:-3px;height: 30px !important;']
                                                )
                                                . Html::endForm(); ?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    </li>
                <?php } else { ?>
                    <li data-id="signup"><a href="<?= Yii::$app->homeUrl ?>site/sign-up#signup">Sign Up</a></li>
                    <li data-id="login"><a href="<?= Yii::$app->homeUrl ?>site/login#login">Log In</a></li>
                    <!--<li data-id="edt_home" class="dropdown" class="active">
                        <div class="btn-group pull-right btngroup">
                            <a class="login" href="<?= Yii::$app->homeUrl ?>site/login">
                                <button class="btn btn-smp-blue smpl-brdr-left" type="button">Login</button>
                            </a>
                            <a class="join" href="<?= Yii::$app->homeUrl ?>site/sign-up">
                                <button class="btn btn-smp-green smpl-brdr-right" type="button">Join Today</button>
                            </a>
                        </div>
                    </li>-->
                <?php }
                if (!$detect->isMobile() && !$detect->isTablet()) { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span
                                    class="glyphicon glyphicon-menu-hamburger"
                                    style="margin-top: -13px; margin-left: 20px; font-size: 21px;"></span></a>
                        <ul class="dropdown-menu nav nav-stacked" style="background-color: #014e76 !important;">
                            <li><a  href="<?= Yii::$app->homeUrl ?>site/index#home"><i class="fa fa-home"
                                                                                      aria-hidden="true"></i>Home</a>
                            </li>
                            <li><a href="<?= Yii::$app->homeUrl ?>site/about#abt"><i class="fa fa-user"
                                                                                     aria-hidden="true"></i>About Us</a>
                            </li>
                            <li><a href="<?= Yii::$app->homeUrl ?>wish/top-wishers#top_wishers"><i class="fa fa-gift"
                                                                                                   aria-hidden="true"></i>Wishers,Granters & Donors</a></li>
                            <li><a href="<?= Yii::$app->homeUrl ?>happy-stories/index#i_wish"><i class="fa fa-smile-o"
                                                                                                 aria-hidden="true"></i>Happy
                                    Stories</a></li>

                        </ul>
                    </li>
                <?php } else { ?>
                    <li class="dropdown explore">
                        <a class="dropdown-toggle menu-dropdown" data-toggle="dropdown" href="#">Explore
                            <span class="caret"></span></a>
                        <ul class="menu-option" style="background-color: #014e76 !important;">
                            <li class="myprofileop"><a href="<?= Yii::$app->homeUrl ?>site/index#home" class="fnt-gold myprofileophyp"><i
                                            class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                            <li class="myprofileop"><a href="<?= Yii::$app->homeUrl ?>site/about#abt" class="fnt-gold myprofileophyp"><i class="fa fa-user"
                                                                                                      aria-hidden="true"></i>About
                                    Us</a></li>
                            <li class="myprofileop"><a href="<?= Yii::$app->homeUrl ?>wish/top-wishers#top_wishers" class="fnt-gold myprofileophyp"><i
                                            class="fa fa-gift" aria-hidden="true"></i>Wishers,Granters & Donors</a></li>
                            <li class="myprofileop"><a href="<?= Yii::$app->homeUrl ?>happy-stories/index#i_wish" class="fnt-gold myprofileophyp"><i
                                            class="fa fa-smile-o" aria-hidden="true"></i>Happy Stories</a></li>

                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<!--***** Header Ends*****-->
<div class="wrapper">
    <div class="<?php if (Yii::$app->controller->action->id == 'about') { ?>  <?php } elseif (Yii::$app->controller->action->id == 'inbox-message') { ?> container-fluid <?php }  else{ ?>container<?php } ?>">
        <?= $content ?>
    </div>
</div>
<!--***** Footer Starts*****-->
<div class="smp-foot" <?php if(Yii::$app->controller->action->id=='contact'){?>style="margin-top: 45px;"<?php  }else { ?>style="margin-top: 0px !important;"<?php  } ?>>
    <footer class="container">
        <div class="row">
            <div class="col-md-4">
                <p> &copy; SimplyWishes 2021, All Rights Reserved.</p>
            </div>
            <div class="col-md-8">
                <div class="smp-footer-links">
                    <a href="<?= \Yii::$app->homeUrl ?>page/view?id=1">
                        <li>Privacy Policy</li>
                    </a>
                    <a href="<?= \Yii::$app->homeUrl ?>page/view?id=2">
                        <li>Terms Of Use</li>
                    </a>
                    <a href="<?= \Yii::$app->homeUrl ?>page/view?id=3">
                        <li>Community Guidelines</li>
                    </a>
                    <!--<a href="<?= \Yii::$app->homeUrl ?>site/about"><li>About Us</li></a>-->
                    <a href="<?= \Yii::$app->homeUrl ?>site/contact">
                        <li>Contact Us</li>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!--                <p>SimplyWishes is available for iOS on the App Store, and Android on the Play Store.</p>
                --> <a class="applinks" href='https://itunes.apple.com/us/app/simplywishes/id1321973376?ls=1&mt=8' target="_blank">
                    <img class="appIcon" alt='Get it on App store'
                         src='<?= Yii::$app->homeUrl ?>web/images/icon/appstore.png'/>
                </a>
                <a href='https://play.google.com/store/apps/details?id=com.simply.wishes' target="_blank">
                    <img class="appIcon" alt='Get it on Google Play'
                         src='<?= Yii::$app->homeUrl ?>web/images/icon/google-play-badge.png'/>
                </a>
            </div>
            <div class="col-md-6">
                <div style="padding-bottom: 5px; text-align: center;">
                    <a style="text-decoration: none;" href="https://www.facebook.com/SimplyWishescom-1121671277927963/"
                       target="_blank">
                        <img class="shareicon-home" style="width:33px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/facebook.png" alt="Facebook"/>
                    </a>

                    <a style="text-decoration: none;" href="https://www.instagram.com/simplywishes777/" target="_blank">
                        <img class="shareicon-home" style="width:35px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/instagram.png" alt="Instagram"/>
                    </a>
                    <a style="text-decoration: none;" href="https://www.linkedin.com/company/simply-wishes/"
                       target="_blank">
                        <img class="shareicon-home" style="width:40px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/Linkedin.png" alt="Linkedin"/>
                    </a>
                    <a style="text-decoration: none;" href="https://www.pinterest.com/simplywishe5244/" target="_blank">
                        <img class="shareicon-home" style="width:30px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/Pinterest.png" alt="Pinterest"/>
                    </a>
                    <a style="text-decoration: none;" href="https://www.reddit.com/user/simplywishes/" target="_blank">
                        <img class="shareicon-home" style="width:30px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/reddit.png" alt="Reddit"/>
                    </a>
                    <a style="text-decoration: none;" href="https://twitter.com/simply_wishes" target="_blank">
                        <img class="shareicon-home" style="width:40px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/twitter.png" alt="Twitter"/>
                    </a>
                    <a style="text-decoration: none;" href="https://www.youtube.com/channel/UC9oY1A49aO1ZQxjdGyJ3Z3Q"
                       target="_blank">
                        <img class="shareicon-home" style="width:30px"
                             src="<?= Yii::$app->homeUrl ?>web/images/icon/youtube-icon.png" alt="Youtube"/>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!--***** Footer Ends *****-->
<?php $this->endBody() ?>
</body>
<script>
    $(".myprofile-option").css("display","none");
    $(".menu-option").css("display","none");
    jQuery(document).ready(function () {

        $('ul.nav li.dropdown').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

        $('.smp-pills li ul li a').find('i').css('color', '#66cc66');
        $('.smp-pills li ul li a').css('color', '#ffcc33');

        var hash = window.location.hash;
        hash = hash.replace('#', '');

        if ($.trim(hash) != "") {
            $("li[data-id=" + hash + "]").addClass("active");
        }
    });

    $(".post-wish").click(function (e) {

        if ($(this).hasClass("disabled")) {
            e.preventDefault();
            alert("You reached the maximum of " + <?=$properties['max_no_wishes']?> +" allowed wishes.");
            return false;
        }
    });

    $(".post-donation").click(function (e) {

        if ($(this).hasClass("disabled")) {
            e.preventDefault();
            alert("You reached the maximum of " + <?=$properties['max_no_donations']?> +" allowed donations.");
            return false;
        }
    });

    $(".myprofile-dropdown").click(function(){
        $(".myprofile-option").toggle();

        $(".menu-option").css("display","none");

    });
    $(".menu-dropdown").click(function(){
        $(".menu-option").toggle();
        $(".myprofile-option").css("display","none");
    });
</script>
</html>
<?php $this->endPage() ?>
