<?php

namespace app\console\models;

use Yii;
use yii\helpers\Url;
use app\models\ReportContent;
use app\models\custom\CustomWish;

/**
 * This is the model class for table "donations".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $granted_by
 * @property string $title
 * @property string $summary_title
 * @property string $description
 * @property string $image
 * @property integer $state
 * @property integer $country
 * @property integer $city
 */
class Donation extends  \yii\db\ActiveRecord
{
    public $auto_id;
    public $image_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donations';
    }

        /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['category', 'wish_title','state', 'country', 'city','expected_cost','expected_date'], 'required'],
            [['title'], 'required'],

            [['non_pay_option'], 'required','on'=>'create','message' => 'You must select one of these options.'],
            [['financial_assistance'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['expected_cost'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['way_of_donation'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }"],
            [['description_of_way'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }",'message' => 'You must fill this details.'],
            //['delivery_type', 'required', 'message' => 'You must choose at least one {attribute}', 'on'=>'update'],

            [['created_by', 'granted_by'], 'integer'],
            [['description', 'image','way_of_donation','description_of_way'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['summary_title','who_can'], 'string', 'max' => 150],
            [['i_agree_decide2','i_agree_decide'], 'integer'],
            [['auto_id','status','image_name'], 'safe'],

    //	[['expected_cost'], 'in','range'=>range(100,1000),'message'=>'Expected Cost(USD) Range In 100 to 1000' ],
            [['show_mail_status','show_person_status','show_other_status'], 'integer'],
            [['show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','financial_assistance','financial_assistance_other'], 'string'],
            ['show_mail','email','message' => 'Enter valid email address'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'D ID',
            'created_by' => 'Created By',
            'granted_by' => 'Granted By',
            'title' => 'Donation Title',
            // 'summary_title' => 'Wish Summary',
            'description' => 'Donation Description',
            'image' => 'Primary Image',

               'delivery_type'=>'Delivery Type ',			
  

                'show_mail_status'=>'E-mail',
                'show_person_status'=>'Postal',
                'show_other_status'=>'Other',
                'show_mail'=>'Enter your email address',
                'show_person_street'=>'Street',
                'show_person_city'=>'City',
                'show_person_state'=>'State',
                'show_person_zip'=>'Zip code',
                'show_person_country'=>'Country',
                'show_other_specify'=>'Please specify the other means of contact',
            'financial_assistance'=>'How would you like to give the financial assistance?'
        ]; 
    }


}
