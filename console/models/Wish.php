<?php

namespace app\console\models;

use Yii;
use yii\helpers\Url;
use app\models\ReportContent;
use app\models\custom\CustomWish;

/**
 * This is the model class for table "wishes".
 *
 * @property integer $w_id
 * @property integer $wished_by
 * @property integer $granted_by
 * @property integer $category
 * @property string $wish_title
 * @property string $summary_title
 * @property string $wish_description
 * @property string $primary_image
 * @property integer $state
 * @property integer $country
 * @property integer $city
 */
class Wish extends \yii\db\ActiveRecord
{
    public $auto_id; 
    public $primary_image_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wishes';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['category', 'wish_title','state', 'country', 'city','expected_cost','expected_date'], 'required'],
            [['wish_title','expected_date'], 'required'],
			
            [['non_pay_option'], 'required','on'=>'create','message' => 'You must select one of these options.'],
            [['financial_assistance'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['show_mail'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['expected_cost'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['way_of_wish'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }"],
            [['description_of_way'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }",'message' => 'You must fill this details.'],
            //	['primary_image', 'required', 'message' => '{attribute} can\'t be blank', 'on'=>'create'],
            [['wished_by', 'granted_by'], 'integer'],
            [['wish_description', 'primary_image'], 'string'],       
            [['wish_title','way_of_wish'], 'string', 'max' => 100],
            [['summary_title','who_can'], 'string', 'max' => 150],
            [['expected_cost'], 'double'],
            [['non_pay_option','i_agree_decide2','i_agree_decide'], 'integer'],

            [['auto_id','wish_status','primary_image_name'], 'safe'],

    //	[['expected_cost'], 'in','range'=>range(100,1000),'message'=>'Expected Cost(USD) Range In 100 to 1000' ],
            [['show_mail_status','show_person_status','show_other_status'], 'integer'],
            [['show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','financial_assistance','financial_assistance_other','description_of_way'], 'string'],
            ['show_mail','email','message' => 'Enter valid email address'],
            ['expected_date', 'validateDate','on' => 'updatedraft'],

        ];
    }



    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'w_id' => 'W ID',
            'wished_by' => 'Wished By',
            'granted_by' => 'Granted By',
            'wish_title' => 'Wish Title',
            'summary_title' => 'Wish Summary',
            'wish_description' => 'Wish Description',
            'primary_image' => 'Primary Image',
                'expected_cost'=>'Expected Cost(USD)', 

               'expected_date'=>'Date I would like my wish to be granted',
               'non_pay_option'=>'Wishes Type ',			
               'who_can'=>'Who can potentialy help me',

                'show_mail_status'=>'E-mail',
                'show_person_status'=>'Postal',
                'show_other_status'=>'Other',
                'show_mail'=>'Enter your email address',
                'show_person_street'=>'Street',
                'show_person_city'=>'City',
                'show_person_state'=>'State',
                'show_person_zip'=>'Zip code',
                'show_person_country'=>'Country',
                'show_other_specify'=>'Please specify the other means of contact',
        ]; 
    }



	
	
}
