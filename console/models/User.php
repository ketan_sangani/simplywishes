<?php

namespace app\console\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord
{
	public $password;
	public $verify_password;

	
    const STATUS_DELETED = 12;
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 13;
 
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    public function rules()
    {
        return [
            //[['username', 'email'],'required'],
            [['email','password','verify_password'], 'required', 'on'=>'sign-up'],
            [['username'], 'string', 'max' => 255],
            ['email', 'email'],
            [['fb_id'], 'integer'],
            //[['username'], 'unique','targetClass' => '\app\models\User', 'message' => 'This Username has already been taken.'],

            [['email'], 'unique','targetClass' => '\app\models\User', 'message' => 'This email address has already been taken.','on'=>['sign-up','updatecheck'] ],  

            //['email', 'uniqueEmail','on'=>'updatecheck'],

            [['password','verify_password'], 'string','min'=>6,'max'=>15],
            [['verify_password'],'compare','compareAttribute'=>'password','message'=>'Password do not match'],
        ];
    }
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['username', 'email','password','verify_password'];
        $scenarios['apply_forgotpassword'] = ['email'];//Scenario Values Only Accepted
        $scenarios['updatecheck'] = ['email','password','verify_password'];//Scenario Values Only Accepted
        return $scenarios;
    }

    
    
	
	/*  public function uniqueEmail($attribute, $params)
    {
	
        $user = \app\models\User::find()->where('email=:email',array('email'=>$this->email))->all();
		$check = "start";
		
		if(isset($user) && !empty($user))
		{
			foreach($user as $tmp)
			{
				if(\Yii::$app->user->id != $tmp->id)
				{					
					$check = "end";
					
				}
			}
		}
		
		if(	$check == "end")
		{
		
            return $this->addError("email", 'Email already test  exists!');
		} 
		

		
    }  */
	
}
