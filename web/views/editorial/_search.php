<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\SearchEditorial */
/* @var $form yii\widgets\ActiveForm */
?>

<div>

    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'e_title')->textInput(['placeholder'=>'Search...'])->label(false); ?>
    <?php // echo $form->field($model, 'created_at') ?>

    <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span>', ['class' => ' btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>