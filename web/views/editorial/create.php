<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Create Article';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>
    <div class="col-md-9 marginPos">
        <ul class="nav nav-tabs" role="tablist">
            <li><a href="<?=Yii::$app->homeUrl?>editorial/editorial#articles" >Articles</a></li>
            <li><a href="<?=Yii::$app->homeUrl?>editorial/editorial#videos" >Videos</a></li>
            <li class="dropdown active">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contribute <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#article" role="tab" data-toggle="tab">Article</a></li>
                    <li><a href="#video" role="tab" data-toggle="tab">Video</a></li>                       
                </ul>
            </li>
	</ul>
        
         <div class="tab-content">
            <?= $this->render('_form', [
                'model' => $model,
                'error' => $error,
                'msg' => $msg
            ]) ?>
         </div>
    </div>

</div>
</div>
<script>
    var hash = document.location.hash;
    if (hash) {
        $('.nav-tabs a[href="'+hash+'"]').tab('show');
    }
    $("li[data-id='forum']").addClass("active");
    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
      window.location.hash = e.target.hash;
    });
</script>
