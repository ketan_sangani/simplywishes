<?php

use yii\helpers\Html;
use app\models\UserProfile;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SearchEditorial */
/* @var $model app\models\Editorial */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
    <?php 
    if( ! empty($user) && ! empty($profile))    
        echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile]);
    else
        echo '<div class="col-md-3"></div>'
    ?>
    <div class="col-md-9">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6 tell-ur-story">
                <div class="pull-right newtest" style="margin-bottom: -30px;">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
        <ul class="nav nav-tabs smp-mg-bottom" role="tablist">
            <li class="active"><a href="#articles" role="tab" data-toggle="tab">Articles</a></li>
            <li><a href="#videos" role="tab" data-toggle="tab">Videos</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Contribute <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?=Yii::$app->homeUrl?>editorial/create#article">Article</a></li>
                    <li><a href="<?=Yii::$app->homeUrl?>editorial/create#video">Video</a></li>                       
                </ul>
            </li>
	</ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane grid" id="videos">
                <?php Pjax::begin() ?>
                <div class="edit" style="overflow: auto;">
                    <?php
                    if(isset($modelVideos) && !empty($modelVideos))
                    {
                        foreach($modelVideos as $tmp)
                        {?>
                            <div class="col-sm-6 editV">
                                <p><a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=<?php echo $tmp->e_id; ?>&class=video" style="color: #000;"><?php echo $tmp->e_title; ?></a></p>
                                <a class='video-icon' href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=<?php echo $tmp->e_id; ?>&class=video">
                                    <img width="250" height="150" class='img-responsive vImage' src="<?=Yii::$app->homeUrl?>web/<?php echo $tmp->e_image; ?>"/>
                                    <span class='glyphicon glyphicon-play-circle'></span>
                                </a>
                            </div>
                            <?php
                        }
                    }
                    else
                    {?>
                        <div class="row editV">
                            <p>No Video Found</p>
                        </div>
                    <?php
                    }?>
                </div>
                <div>
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $paginationVideo,
                    ]);?>
                </div>
                <?php Pjax::end() ?>
            </div>
            <div role="tabpanel" class="tab-pane grid active" id="articles">
                <?php Pjax::begin() ?>
                <?php
                if(isset($modelArticles) && !empty($modelArticles))
                {
                    foreach($modelArticles as $tmp)
                    {
                        $profile = UserProfile::find()->where(['user_id'=>$tmp->created_by])->one();?>
                        <div class="row edit">
                            <div class="form-group">
                                <p><a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=<?php echo $tmp->e_id; ?>&class=article" style="color: #000;"><?php echo $tmp->e_title; ?></a></p>
                                <p><img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?php echo $profile->profile_image.'?v='.strtotime('now') ?>"/> <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $tmp->created_by ?>" >&nbsp;By: &nbsp;<?php echo $profile->Fullname ?></a></p>
                                <p>Date: &nbsp;<?php echo date("m-d-Y",strtotime($tmp->created_at)); ?></p>					
                                <p><?php echo substr($tmp->e_text,0,400)?> ...</p>
                                <a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=<?php echo $tmp->e_id; ?>&class=article"><h5>Read More</h5></a>
                            </div>
                        </div>
                        <?php
                    }
                }
                else
                {?>
                    <div class="row edit" style="margin-right: 0px;">
                        <div class="form-group col-md-9">
                            <p>No Article Found</p>
                        </div>
                    </div>
                <?php
                }?>
                <div>
                    <?php
                    echo \yii\widgets\LinkPager::widget([
                        'pagination' => $paginationArticle,
                    ]);?>
                </div>
                <?php Pjax::end() ?>
            </div>
        </div> 
    </div>
</div>
</div>
<script>
    $(".shareIcons").each(function(){
        var elem = $(this);
            elem.jsSocials({
            showLabel: false,
            showCount: true,
            shares: ["facebook","googleplus", "linkedin",
            {
                share: "twitter",           // name of share
                via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
            }],
            url : elem.attr("data_url"),
            text: elem.attr("data_text")
        });
    });
    
    var hash = document.location.hash;
    if (hash) {
        $('.nav-tabs a[href="'+hash+'"]').tab('show');
    }
    $("li[data-id='forum']").addClass("active");
    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
      window.location.hash = e.target.hash;
    });
</script>

