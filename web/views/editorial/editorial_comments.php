<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use app\models\EditorialComments;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Editorials';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\Yii::$app->view->registerMetaTag([
    'name' => 'og:title',
    'property' => 'og:title',
    'content' =>$model->e_title
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:description',
    'property' => 'og:description',
    'content' =>$model->e_text
]);
\Yii::$app->view->registerMetaTag([
    'name' => 'og:image',
    'property' => 'og:image',
    'content' =>Url::to(['web/'.$model->e_image],true)
]);

?>
<div class="editorial-create">
    <?php if (Yii::$app->session->hasFlash('error_comments')): ?>
        <div class="alert alert-success">
            Oops!Something went wrong. Please try again later.
        </div>
	<?php endif; ?>	
    <?php if(Yii::$app->session->hasFlash('login_to_comment')): ?>
        <div class="alert alert-success">
            Please login to post a comment!
        </div>
    <?php endif; ?>		
	<?php
    if(isset($model) && !empty($model))
    {		
        $profile = UserProfile::find()->where(['user_id'=>$model->created_by])->one();
        if($class === 'video')
            $hash   =   '#videos';
        else
            $hash   =   '#articles';
        ?>
        <div class="row">
            <div class="col-md-6"><h3 class="fnt-skyblue" ><?= $model->e_title; ?></h3></div>
            <div class="col-md-offset-5 col-md-1"><a class="btn btn-success" style="margin-top: 22px;" href="<?=Yii::$app->homeUrl?>editorial/editorial<?= $hash ?>">Back</a></div>
        </div>
        <div class="row edit">
            <div class="articleBlock">
            <?php
            if($class === 'video')
            {?>
                <div cass="row">
                    <div style="text-align: center;">
                        <?php 
                        if($model->featured_video_url != ''){ 
                            $url = $model->featured_video_url; 
                            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                                $domain = parse_url($url, PHP_URL_HOST);
                                $pos    =   strpos($domain, 'simplywishes');
                                
                                if($pos == false)
                                {
                                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                                    
                                    if(isset($match[1]))
                                    {
                                        $youtube_id = $match[1];
                                        echo "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://www.youtube.com/embed/".$youtube_id."' controls></iframe></div>";
                                        
                                    }
                                    else
                                        echo "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='".$url."' controls></iframe></div>";
                                }
                                else
                                    echo "<video poster='".Yii::$app->homeUrl."web/".$model->e_image."' controls autoplay><source src=".$url." type='video/mp4'>Your browser does not support the video tag.</video>";
                            } else {
                                echo $url;
                            }
                        }
                        ?>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-md-8">
                            <p class="dpic">
                                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?php echo $profile->profile_image.'?v='.strtotime('now'); ?>"/></a> <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $model->created_by ?>" >&nbsp;Posted by: &nbsp;<?php echo $profile->Fullname; ?></a>
                                
                            </p>
                            <?php 
                            $likeclass  =   '';	

                            if($model->likesCount > 0)
                                $likeclass    =   'likesView';
                            ?>
                            <span data-id="<?= $model->e_id?>" class="<?php echo $likeclass ?>"> <span id="likecmt_<?= $model->e_id ?>">  <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span>
                            <?php

                                if(!$model->isLiked(\Yii::$app->user->id))
                                      echo  '<span title="Like it" data-w_id="'.$model->e_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                                else
                                      echo  '<span title="You liked it" data-w_id="'.$model->e_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                            ?>
                        </div>
                        <div class="col-md-4 shareIcons" data_text="<?php echo $model->e_title; ?>" data_url="<?= Url::to(['editorial/editorial-page','id'=>$model->e_id,'class'=>'video'],true) ?>" ></div>
                    </div>
                </div>
            <?php }
            else if($class === 'article')
            { ?>
                <div class="row form-group">
                    <div class="col-md-8">
                        <p class="dpic"><img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?php echo $profile->profile_image.'?v='.strtotime('now'); ?>"/></a> <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $model->created_by ?>" >&nbsp;By: &nbsp;<?php echo $profile->Fullname; ?></a></p>
                        <p>Date: &nbsp;<?php echo date("m-d-Y",strtotime($model->created_at)); ?></p>
                        <?php 
                        $likeclass  =   '';	

                        if($model->likesCount > 0)
                            $likeclass    =   'likesView';
                        ?>
                        <span data-id="<?= $model->e_id?>" class="<?php echo $likeclass ?>"> <span id="likecmt_<?= $model->e_id ?>">  <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span>
                            
                        <?php

                            if(!$model->isLiked(\Yii::$app->user->id))
                                  echo  '<span title="Like it" data-w_id="'.$model->e_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                            else
                                  echo  '<span title="You liked it" data-w_id="'.$model->e_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                        ?>
                    </div>
                    <div class="col-md-4 shareIcons" data_text="<?php echo $model->e_title; ?>" data_url="<?= Url::to(['editorial/editorial-page','id'=>$model->e_id,'class'=>'article'],true) ?>" ></div>
                </div>	
                
                <div class="form-group">
                    <p><?= $model->e_text ?></p>					
                </div>
                <div style="text-align: center;">
                    <?php 
                    if($model->featured_video_url != ''){ 
                        $url = $model->featured_video_url; 
                        if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
                            $domain = parse_url($url, PHP_URL_HOST);
                            $pos    =   strpos($domain, 'simplywishes');

                            if($pos == false)
                            {
                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
                                if(isset($match[1]))
                                {
                                    $youtube_id = $match[1];
                                    echo "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='https://www.youtube.com/embed/".$youtube_id."' controls></iframe></div>";

                                }
                                else
                                    echo "<div class='embed-responsive embed-responsive-16by9'><iframe class='embed-responsive-item' src='".$url."' controls></iframe></div>";
                            }
                            else
                                echo "<video poster='".Yii::$app->homeUrl."web/".$model->e_image."' controls autoplay><source src=".$url." type='video/mp4'>Your browser does not support the video tag.</video>";
                        } else {
                            echo $url;
                        }
                    }
                    ?>
                </div>
            <?php } ?>
            </div>
        </div>
        
        <div class="row">
            <h3 class="left fnt-skyblue" >Comments:</h3>

            <?php $form = ActiveForm::begin(['action' =>['editorial/editorial-comments?class='.$class]]); ?>
                <?= $form->field($listcomments, 'comments')->textarea(['rows' => 2])->label(false) ?>			
                <?= $form->field($listcomments, 'e_id')->hiddeninput(['value'=>$model->e_id])->label(false) ?>
               <div class="form-group">
                   <?= Html::submitButton('Post', ['class' =>'btn btn-primary']) ?>
               </div>
            <?php ActiveForm::end(); ?>
        </div>
<?php
if(isset($comments) && !empty($comments)){ 
    foreach($comments as $user)
    {
        $profile = UserProfile::find()->where(['user_id'=>$user->user_id])->one();?>
        <div class="row">		
            <div class="form-group dpic floatLeft">
                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $profile->profile_image.'?v='.strtotime('now'); ?>"/>				
            </div>
            <div class="form-group floatLeft col-md-10 paddingLeft">	
                <span id="<?php echo "comment_".$user->e_comment_id ?>">
                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $profile->user_id ?>" ><?= $profile->firstname.' '.$profile->lastname ?></a>					
                    <?= $user->comments ?><?php if($user->user_id === \Yii::$app->user->id) { ?>
                    <i class="glyphicon glyphicon-pencil editComment" data-id="<?= $user->e_comment_id ?>" aria-hidden="true"></i>
                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $user->e_comment_id, 'class' => $class], [
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this Comment?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    <?php } ?>
                </span>
                <div  style="display:none; margin-top:10px" id="<?php echo "editCommentBlk_".$user->e_comment_id ?>">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['editorial/update-comment?id='.$user->e_comment_id.'&class='.$class]]); ?>
                        <?= $form->field($listcomments, 'comments')->textarea(['rows' => 6, 'value' => $user->comments])->label(false) ?>			
                        <?= $form->field($listcomments, 'e_id')->hiddeninput(['value'=>$model->e_id])->label(false) ?>
                       <div class="form-group">
                           <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                       </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <p class="commentOptions">
                    <?php
                        if(!$user->isLiked(\Yii::$app->user->id))
                              echo  '<span title="Like it" class="loveComment" for="'.$user->e_comment_id.'">Love</span>';
                        else
                              echo  '<span title="Like it" class="loveComment" for="'.$user->e_comment_id.'" style="color:#B23535;">Love</span>';
                        
                        if($user->likesCount > 0)
                            $classCmt   =   'cmtLikesView';
                        else
                            $classCmt   =   '';
                        
                    ?>
                    <span class="on-reply" for="<?= $user->e_comment_id ?>">Reply</span>
                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $user->e_comment_id ?>'></span>
                    <span id="cmtNum_<?= $user->e_comment_id ?>"> <?= $user->likesCount ?></span>
                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($user->created_at)) ?></span>
                </p>

                <div  style="display:none; margin-top:10px" id="<?php echo "replylist_".$user->e_comment_id ?>" class="comment-form2 reply full" data-plugin="comment-reply">	
                    <!--<a class="close" data-action="comment-close">X</a> -->
                    <?php $form = ActiveForm::begin(['action' =>['editorial/commentreply?class='.$class]]); ?>				 
                     <?= $form->field($listcomments, 'comments')->textarea(['rows' => 3])->label(false) ?>			
                     <?= $form->field($listcomments, 'e_id')->hiddeninput(['value'=>$model->e_id])->label(false) ?>			
                     <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$user->e_comment_id])->label(false) ?>
                     <div class="form-group">
                            <?= Html::submitButton('Reply-Post', ['class' =>'btn btn-primary']) ?>
                    </div>					
                    <?php ActiveForm::end(); ?>
                </div>
                <?php 
                $replycomments = EditorialComments::find()->where(['parent_id'=>$user->e_comment_id, 'status'=>0])->orderBy('e_comment_id Desc')->all();
                if($replycomments)
                {
                    foreach($replycomments as $replyuser)
                    {
                        $replyprofile = UserProfile::find()->where(['user_id'=>$replyuser->user_id])->one();	
                        ?>
                        <div class="row">		
                            <div class="form-group dpic floatLeft">
                                <img src="<?=Yii::$app->homeUrl?>web/uploads/users/<?= $replyprofile->profile_image.'?v='.strtotime('now'); ?>"/>				
                            </div>
                            <div class="form-group floatLeft col-md-10 paddingLeft">	
                                <span id="<?php echo "replyComment_".$replyuser->e_comment_id ?>">
                                    <a class="atagcolor" href="<?=Yii::$app->homeUrl?>account/profile?id=<?php echo $replyprofile->user_id ?>" ><?= $replyprofile->firstname.' '.$replyprofile->lastname; ?></a>
                                    <?= $replyuser->comments ?><?php if($replyuser->user_id === \Yii::$app->user->id) { ?>
                                    <i class="glyphicon glyphicon-pencil editReplyComment" data-id="<?= $replyuser->e_comment_id ?>" aria-hidden="true"></i>
                                    <?= Html::a('<i class="glyphicon glyphicon-trash deleteReplyComment" aria-hidden="true"></i>', ['delete-comment', 'id' => $replyuser->e_comment_id, 'class' => $class], [
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this Comment?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                    <?php } ?>
                                </span>
                                <p class="commentOptions">
                                    <?php
                                        if(!$replyuser->isLiked(\Yii::$app->user->id))
                                              echo  '<span title="Like it" class="loveComment" for="'.$replyuser->e_comment_id.'">Love</span>';
                                        else
                                              echo  '<span title="Like it" class="loveComment" for="'.$replyuser->e_comment_id.'" style="color:#B23535;">Love</span>';
                                    
                                         if($replyuser->likesCount > 0)
                                            $classCmt   =   'cmtLikesView';
                                        else
                                            $classCmt   =   '';
                                    ?>
                                    <span class="glyphicon glyphicon-heart commentHeart <?= $classCmt ?>" data-id='<?= $replyuser->e_comment_id ?>'></span>
                                    <span id="cmtNum_<?= $replyuser->e_comment_id ?>"> <?= $replyuser->likesCount ?></span>
                                    <span class="commentDate"><?= date("F j Y g:i a", strtotime($replyuser->created_at)) ?></span>
                                </p>
                            </div>
                            <div  style="display:none; margin-top:10px" id="<?php echo "editReplyBlk_".$replyuser->e_comment_id ?>">	
                                <!--<a class="close" data-action="comment-close">X</a> -->
                                <?php $form = ActiveForm::begin(['action' =>['editorial/update-comment?id='.$replyuser->e_comment_id.'&class='.$class]]); ?>				 
                                 <?= $form->field($listcomments, 'comments')->textarea(['rows' => 3, 'value'=> $replyuser->comments ])->label(false) ?>			
                                 <?= $form->field($listcomments, 'e_id')->hiddeninput(['value'=>$replyuser->e_id])->label(false) ?>			
                                 <?= $form->field($listcomments, 'parent_id')->hiddeninput(['value'=>$replyuser->parent_id])->label(false) ?>
                                 <div class="form-group">
                                        <?= Html::submitButton('Update', ['class' =>'btn btn-primary']) ?>
                                </div>					
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>	
                        <?php
                    }
                }?>
            </div>
        </div>
    <?php } } ?>
<?php			
}?>	
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
    $(".on-reply").click(function(){ 
		var id = $(this).attr("for");
		
		if($("#replylist_"+id).is(':hidden'))
		{
			$("#replylist_"+id).show();			
		} else {
			$("#replylist_"+id).hide();	
		}  	
		
    });
    
    $('.editComment').on('click', function(){
        var id = $(this).data("id");
		
        $('#comment_'+id).hide();
        if($("#editCommentBlk_"+id).is(':hidden'))
        {
            $("#editCommentBlk_"+id).show();			
        } else {
            $("#editCommentBlk_"+id).hide();	
        }  	
    });
    
    $('.editReplyComment').on('click', function(){
        var id = $(this).data("id");
		
        $('#replyComment_'+id).hide();
        if($("#editReplyBlk_"+id).is(':hidden'))
        {
            $("#editReplyBlk_"+id).show();			
        } else {
            $("#editReplyBlk_"+id).hide();	
        }  
    });
    
	/* $(".close").click(function(){ 
		$(this).parent().hide();
    }); */
});
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                $.ajax({
                    url : '<?=Url::to(['editorial/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
        
$(document).on('click', '.cmtLikesView', function(){
    var val =   $(this).next('span').html();
    if(val <= 0)
        return false;

    var wish_id = $(this).data('id');
    if($.trim(wish_id) !== "" )
    {
        if(Wish.ajax)
            return false;

        Wish.ajax   =   true;
        $.ajax({
            url : '<?=Url::to(['editorial/comment-likes-view'])?>',
            type: 'GET',
            data: {w_id:wish_id},
            success:function(data){
                if(data)
                {
                    var tpl =   '';
                    var items   =   JSON.parse(data);
                    $.each(items, function(k,v){
                       tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                    });

                    $('.modal-body').append(tpl);
                    $('#likeModal').modal({backdrop: 'static', keyboard: false});
                    $('#likeModal').modal('show');

                    $('.closeModal').on('click', function(e){
                        e.preventDefault();
                        $('.modal-body').html('');
                        $('#likeModal').modal('hide');
                    });
                }
            },
            complete: function(){
                Wish.ajax   =   false;
            }
        });
    }
});

$(document).on('click', '.loveComment', function(){
    var s_id = $(this).attr("for");
    var elem = $(this);
    $.ajax({
        url : '<?=Url::to(['editorial/like-comment'])?>',
        type: 'GET',
        data: {s_id:s_id},
        success:function(data){
            if(data == "added"){
                elem.css('color', '#B23535');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) + parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
            if(data == "removed"){
                elem.css('color', '#66cc66');
                var likecmt = $("#cmtNum_"+s_id).text();
                likecmt = parseInt(likecmt) - parseInt(1);
                $("#cmtNum_"+s_id).text(likecmt);
            }
        }
    });
});

$(document).on('click', '.like-wish', function(){ 
		var s_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['editorial/like'])?>',
			type: 'GET',
			data: {s_id:s_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
					}
				}
			}
		});
	});
        
	$(".shareIcons").each(function(){
		var elem = $(this);
			elem.jsSocials({
			showLabel: false,
			showCount: true,
			shares: ["facebook","googleplus", "linkedin",
			{
				share: "twitter",           // name of share
				via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
				hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
			}],
			url : elem.attr("data_url"),
			text: elem.attr("data_text")
		});
	});
</script>
<style>
iframe{
	max-width: 100%;
}
</style>

