<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Wish */

$this->title = 'Make a Wish';
$this->params['breadcrumbs'][] = ['label' => 'Wishes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class= " col-md-8 wish-create" id="scroll-evt">

    <h3  class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
        'profile' => $profile
    ]) ?>

</div>

<script type="text/javascript">
 
 $( document ).ready(function() {
    $( "#save_draft" ).click(function() {
        if($('#wish-primary_image').val())
        {
            var auto_id = $("#wish-w_id").val();

            if($('#wish-wish_title').val() !== '')
            {
                if($.trim(auto_id) === "")
                {
                    $.post( "wish-autosave", $("#draft_form").serialize(), function( data ) {
                        $("#wish-w_id").val(data);
                        upload();
                    });	
                }
                else
                {
                    $.post("wish-autosave", $("#draft_form").serialize(), function(){
                        upload();
                    });
                }
            }
            else
            {
                $('#draft_error').removeClass('hide');
                
                setTimeout(function(){
                    $('#draft_error').addClass('hide');
                }, 3000);
            }
        }
        else {
        
            var auto_id = $("#wish-w_id").val();

            if($('#wish-wish_title').val() !== '')
            {
                if($.trim(auto_id) === "")
                {
                    $.post( "wish-autosave", $("#draft_form").serialize(), function( data ) {
                        $("#wish-w_id").val(data);
                        window.location.href    =   'my-drafts';
                    });	
                }
                else
                {
                    $.post("wish-autosave", $("#draft_form").serialize(), function(){
                        window.location.href    =   'my-drafts';
                    });
                }
            }
            else
            {
                $('#draft_error').removeClass('hide');
                
                setTimeout(function(){
                    $('#draft_error').addClass('hide');
                }, 8000);
            }
        }
    });
    
    function upload(){
        var image_file = $('#wish-primary_image').val();
        
        $.ajax({
            url: 'upload-file',
            type: "POST",
            data: {image : image_file, id : $("#wish-w_id").val()},
            success: function(json){      
                $("#wish-primary_image").val(json);
                
                var auto_id = $("#wish-w_id").val();

                if($('#wish-wish_title').val() !== '')
                {
                    if($.trim(auto_id) === "")
                    {
                        $.post( "wish-autosave", $("#draft_form").serialize(), function( data ) {
                            $("#wish-w_id").val(data);
                            window.location.href    =   'my-drafts';
                        });	
                    }
                    else
                    {
                        $.post("wish-autosave", $("#draft_form").serialize(), function(){
                            window.location.href    =   'my-drafts';
                        });
                    }
                }
            }
      });
    }
 });
 $(window).on('load', function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
    
    $("li[data-id=post_wish]").addClass("active");
});
 
</script>
