<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model app\models\Wish */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Wish: ' . $model->wish_title;
$this->params['breadcrumbs'][] = ['label' => 'Wishes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->w_id, 'url' => ['view', 'id' => $model->w_id]];
$this->params['breadcrumbs'][] = 'Update';

$wishstatus = array('0'=>"Active",'1'=>"In-Active");
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>
<link rel="stylesheet" type="text/css" href="<?=Yii::$app->homeUrl?>web/css/croppie.css">
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>web/src/croppie.js"></script>
<div class=" col-md-8 wish-update">
    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'draft_form']); ?>

            <?= $form->field($model, 'wish_title')->textInput(['maxlength' => true])->label("Wish Title <span class='valid-star-color' >*</span> ") ?>

            <?php // $form->field($model, 'summary_title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'wish_description')->widget(CKEditor::className(), [
                'preset' => 'basic',
                'clientOptions' => ['height' => 75]
            ]); ?>
            <p><label>Wish Image</label></p>

            <div id="upload-img" class="hide"></div>
            <?php if(!empty($model->primary_image)) {  ?>
                <div>
                    <img class='image_block' id="image" src="<?= \Yii::$app->homeUrl.'web/'.$model->primary_image.'?v='.strtotime('now')?>" width="300" />
                    <a href="#" class="removeImage">Remove</a>
                </div>
            <?php } else { ?>
                <div>
                    <img class='image_block hide' id='image' width="300"/>
                    <a href="#" class="removeImage hide">Remove</a>
                </div>
            <?php } ?>
            <div class="actions">
                <a class="btn file-btn btn btn-primary">
                    <span>Choose an Image from your Files</span>
                    <input type="file" id="upload" value="Choose a file" accept="image/*" />
                </a>
                <img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loader" class="hide" width="30">
                <div class="upload-buttons hide">
                    <button class="upload-result btn btn-primary">Upload</button>
                    <button class="upload-rotate btn btn-primary" data-deg="-90">Rotate Left</button>
                    <button class="upload-rotate btn btn-primary" data-deg="90">Rotate Right</button>
                </div>
            </div>
            <?= $form->field($model, 'primary_image')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'primary_image_name')->hiddenInput()->label(false); ?>
            <span>Or, Choose One from the Default Images below</span>  
            <div class="gravatar wishDefault">
                <a class="profilelogo" for="images/wish_default/1.jpg" ><img class="selected" src="<?=Yii::$app->homeUrl?>web/images/wish_default/1.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/2.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/3.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/4.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/5.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/5.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/6.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/7.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/8.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/9.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/10.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/11.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/12.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/13.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/14.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/15.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/16.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/17.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/17.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/18.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/18.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/19.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/19.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/20.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/20.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/21.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/21.jpg" width="100"/></a>
                <a class="profilelogo" for="images/wish_default/22.jpg" ><img src="<?=Yii::$app->homeUrl?>web/images/wish_default/22.jpg" width="100"/></a>
            </div>	
            
            <div class="row">
                <div class="col-lg-6">
		    <?= $form->field($model, 'expected_date')->widget(
                        DatePicker::className(),
                        [
                            'attribute' => 'expected_date',
                            'options' => ['placeholder' => 'Select issue date ...'],
                            'pluginOptions' => [
                                'format' => 'mm-dd-yyyy',
                                'todayHighlight' => true,
                                'autoclose'=>true,
                                'startDate' => '+0d'
                            ]
                        ]
                    )->label("Date - I would like my wish to be granted! <span class='valid-star-color' >*</span> "); ?>
                </div>
                <div class="col-lg-6"></div>	
            </div>

            <?php  echo $form->field($model, 'non_pay_option')->radioList([
                '0'=>'Financial','1'=>'Non-financial'],[
                    'onclick' => "$(this).val($('input:radio:checked').val())"])->label(false); ?>
	
            <!-- Financial Begin --->
            <div class="edit" id="financial" style="display:none;">
                <h6 id="financial_txt">You need a PayPal account in order to get grant for a Financial Wish.</h6>
                <?= $form->field($model, 'expected_cost')->textInput(['maxlength' => true])->label("Expected Cost(USD) <span class='valid-star-color' >*</span> ");?>
                <div id="expected_cost_check" style="display:none; color:#a94442;" >Expected Cost Is empty check</div>
            </div>
            <!-- Financial End --->

            <!-- NON Financial Begin --->
            <div class="edit" id="non_financial" style="display:none;">
                <h5>How Wisher to be contacted? (Check as many as applicable)</h5>
                <?php 
                    if($model->isNewRecord){ 
                        $model->show_mail_status = "0"; 
                        $model->show_person_status = "0"; 
                        $model->show_other_status = "0"; 
                    }	
                ?>

                <?= $form->field($model, 'show_mail_status')->checkbox(['value' =>'1','class'=>"checkall",'for'=>'show_mail_class']);	?>
                <div id="wish-show_mail_status_check" style="display:none; color:#a94442;" >Please Check this field </div>
                <?= $form->field($model, 'show_mail')->textInput(['maxlength' => true,'class'=>'form-control check_test','placeholder' => 'xxx@abc.com']) ?>
                <div id="wish-show_mail_check" class="show_mail_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                <?= $form->field($model, 'show_person_status')->checkbox(['value' => '1','class'=>"checkall",'for'=>'show_person_class']);	?>
                <div id="wish-show_person_status_check" style="display:none; color:#a94442;" >Please Check this field </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($model, 'show_person_street')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                        <div id="wish-show_person_street_check" class="show_person_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'show_person_city')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                        <div id="wish-show_person_city_check" class="show_person_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'show_person_state')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                        <div id="wish-show_person_state_check" class="show_person_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'show_person_zip')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                        <div id="wish-show_person_zip_check" class="show_person_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                    </div>
                    <div class="col-lg-6">
                        <?= $form->field($model, 'show_person_country')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                        <div id="wish-show_person_country_check" class="show_person_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                    </div>
                </div>
                <?= $form->field($model, 'show_other_status')->checkbox(['value' => '1','class'=>"checkall",'for'=>"show_other_class"]);	?>
                <div id="wish-show_other_status_check" style="display:none; color:#a94442;" >Please Check this field </div>
                <?= $form->field($model, 'show_other_specify')->textInput(['maxlength' => true,'class'=>'form-control check_test']) ?>
                <div id="wish-show_other_specify_check" class="show_other_class" style="display:none; color:#a94442; margin-left:50px" >Please Fill this field </div>
                <div class="form-group" id="agree_check" >	
                    <div id="i_agree_decide_req" style="display:none; color:#a94442;" >Please Check this field </div>
                    <input type="checkbox"  class="msg" name="Wish[i_agree_decide]" id="i_agree_decide" value="1" <?php echo ($model->i_agree_decide == 1)?"checked":"" ?>> I understand that the grantor will fulfill this wish in the manner specified by you within one month of the date that the grantor accepts this wish. In the meanwhile, this wish will be marked as "In Progress" and after one month, it will be marked as "Fulfilled". You should update or ressubmit your wish if it has not been fulfilled after one month. </input>
                </div>
            </div>
            <!-- NON Financial End --->
           
            <!-- Decide Later <div class="edit" id="decide_later" style="display:none;">
                <div class="form-group" id="agree_check2" >	
                    <div id="i_agree_decide_req2" style="display:none; color:#a94442;" >Please Check this field </div>
                    <input type="checkbox"  class="msg" name="Wish[i_agree_decide2]" id="i_agree_decide2" value="1" <?php echo ($model->i_agree_decide2 == 1)?"checked":"" ?> > I understand that the grantor will fulfill this wish in the manner specified by you within one month of the date that the grantor accepts this wish. In the meanwhile, this wish will be marked as "In Progress" and after one month, it will be marked as "Fulfilled". You should update or ressubmit your wish if it has not been fulfilled after one month. </input>
                </div>
            </div>
            <!-- Decide Later End --->

            <?= $form->field($model, 'who_can')->textArea()?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Post this Wish', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <a id="cancel" class="btn btn-primary">Cancel</a>
            </div>

            <?php 
                if($model->isNewRecord)
                    echo $form->field($model, 'auto_id')->hiddenInput()->label(false)  
            ?>
	
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
<div id="overlay"><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loading"></div>
<script type="text/javascript" >
$( document ).ready(function() {
    $("#wish-non_pay_option").on("click",function(){
        var pay_option = $("#wish-non_pay_option").val();
        $('.edit').hide();
        if(pay_option == 0)
        {
            $('#financial').show();
        } else if(pay_option == 1)
        {
            $('#non_financial').show();

        }	
    });
    <?php 
        if((!$model->isNewRecord) && ($model->non_pay_option === 0))
        { ?>
            $('#financial').show();
	<?php } else if((!$model->isNewRecord) && ($model->non_pay_option == 1)){ ?>
            $('#non_financial').show();
	<?php } ?>
	
	$("#wish-non_pay_option").val(<?= $model->non_pay_option ?>);
        
        $( "#draft_form" ).on('beforeSubmit', function( event ) {
			
		if($("#imageclass").length)
		{
			var check = false;
			var imagecheck = $("#imageclass").val();
			if($.trim(imagecheck)  == "")
			{
				$('#imageclass_check').show();
				check = true;
			} else {
				$('#imageclass_check').hide();
			}				
			
			if(check == true)
			{	
                            $('html, body').animate({
                                scrollTop: $('#imageclass').offset().top - 100
                            }, 1000);
                            return false;
			}
			
		}
		var pay_option = $("#wish-non_pay_option").val();
		
		if(parseInt(pay_option) == 1)
		{	
			//********************* Begin *******************/	
			var check = false;
			var show_mail = $("#wish-show_mail").val();
			if($.trim(show_mail) !== "")
			{
				if($("#wish-show_mail_status").prop("checked") == false){
					$("#wish-show_mail_status_check").show();
					check = true;
				}
			}
			
			var show_person_street = $("#wish-show_person_street").val();
                        var show_person_city = $("#wish-show_person_city").val();
                        var show_person_state = $("#wish-show_person_state").val();
                        var show_person_zip = $("#wish-show_person_zip").val();
                        var show_person_country = $("#wish-show_person_country").val();

                        if(($.trim(show_person_street) !== "") || ($.trim(show_person_city) !== "") || ($.trim(show_person_state) !== "") || ($.trim(show_person_zip) !== "") || ($.trim(show_person_country) !== "") )
                        {
                            if($("#wish-show_person_status").prop("checked") == false){
                                $("#wish-show_person_status_check").show();
                                check = true;
                            }
                        }
		
			var show_other_specify = $("#wish-show_other_specify").val();
			if($.trim(show_other_specify) !== "")
			{
				if($("#wish-show_other_status").prop("checked") == false){
					$("#wish-show_other_status_check").show();
					check = true;
				}
			}								
			if($("#i_agree_decide").prop("checked") == false){
				$("#i_agree_decide_req").show();
				check = true;
			}
					
			
			
			//********************* End *******************/
			//********************* Begin *******************/
			
		
			
			if($("#wish-show_mail_status").prop("checked") == true)
			{
				var show_mail = $("#wish-show_mail").val();
				if($.trim(show_mail) == "")
				{
					$("#wish-show_mail"+"_check").show();
					check = true;
				}
			}
			
			if($("#wish-show_person_status").prop("checked") == true)
                        {
                            if($.trim(show_person_street) == "")
                            {
                                $("#wish-show_person_street"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_city) == "")
                            {
                                $("#wish-show_person_city"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_state) == "")
                            {
                                $("#wish-show_person_state"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_zip) == "")
                            {
                                $("#wish-show_person_zip"+"_check").show();
                                check = true;
                            }
                            if($.trim(show_person_country) == "")
                            {
                                $("#wish-show_person_country"+"_check").show();
                                check = true;
                            }
                        }
			
			
			if($("#wish-show_other_status").prop("checked") == true)
			{
				var show_other_specify = $("#wish-show_other_specify").val();
				if($.trim(show_other_specify) == "")
				{
					$("#wish-show_other_specify"+"_check").show();
					check = true;
				}
			}
			
                        if($('.checkall:checkbox:checked').length == 0)
			{
				alert("Please Choose any field For Contact.")
				check = true;
			}
                        
			if(check == true)
			{
                            $('html, body').animate({
                                scrollTop: $('#wish-non_pay_option').first().offset().top - 100
                            }, 1000);
                            return false;
			}
                        else
                            $('#overlay').show();
			
			//********************* End *******************/
			
		} 
		else if(parseInt(pay_option) == 0)
		{
			var expected_cost = $("#wish-expected_cost").val();
			if($.trim(expected_cost) == ""){
                            $('html, body').animate({
                                scrollTop: $('#wish-non_pay_option').first().offset().top - 100
                            }, 1000);
                            $("#expected_cost_check").show();
                            return false;
			}
                        else
                            $('#overlay').show();
						
		}
		
	});
        
        $('#draft_form').on('afterValidate', function (event, messages) {
            if(typeof $('.has-error').first().offset() !== 'undefined') {
                $('html, body').animate({
                    scrollTop: $('.has-error').first().offset().top - 200
                }, 1000);
            }
        });
		
		 $("#wish-expected_cost").change(function(){
			var expected_cost = $(this).val();
			if($.trim(expected_cost) != ""){
				$("#expected_cost_check").hide();
			}						 
	    });
		
	    $("#i_agree_decide2").change(function(){
			if($("#i_agree_decide2").prop("checked") == true){
						$("#i_agree_decide_req2").hide();
			}						 
	    });
	
	    $("#i_agree_decide").change(function(){
			if($("#i_agree_decide").prop("checked") == true){
						$("#i_agree_decide_req").hide();
			}						 
	    });
	
	
		$(".checkall").change(function(){
			var id = $(this).attr("id");
			var forid = $(this).attr("for");
			if($("#"+id).prop("checked") == true){
				$("#"+id+"_check").hide();
                                $("#"+id).closest('div').nextAll('div').find('input').first().focus();
			} else 
			{					
				$("."+forid).hide();
			}				
	    });
		
		$(".check_test").change(function(){
			var id = $(this).attr("id");
			if($("#"+id).val() != ""){
				$("#"+id+"_check").hide();
			}						 
	    });
            
            $uploadCrop = $('#upload-img').croppie({
        enableExif: true,
        viewport: {
            width: 300,
            height: 200
        },
        boundary: {
            width: 400,
            height: 300
        },
        enableOrientation: true
    });

    $('#upload').on('change', function () { readFile(this); });

    function readFile(input) {
        $('#upload-img').removeClass('hide');
        $('#loader').removeClass('hide');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload-img').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    $('#loader').addClass('hide');
                    $('.upload-buttons').removeClass('hide');
                });
            };

            reader.readAsDataURL(input.files[0]);
        }
        else
            $('#loader').addClass('hide');
    }

    $('.upload-result').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $('#wish-primary_image').val(resp);
            $('#image').attr('src', resp).removeClass('hide');
            $('.removeImage').removeClass('hide');
            $('html, body').animate({
                scrollTop: $('.field-wish-expected_date').offset().top - 100
            }, 'slow');
        });
    });
    
    $('.upload-rotate').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
    });

    $('.removeImage').on('click', function(e){
        e.preventDefault();
        $('#wish-primary_image').val('');
        $('#image').attr('src', '').addClass('hide');
        $(this).addClass('hide');
    });
		
});
$(function(){				
    $('.profilelogo').click(function(){
        $('.profilelogo').find( "img" ).removeClass('selected'); 
        var val = $(this).attr('for');
        $(this).find( "img" ).addClass('selected'); 
        $("#wish-primary_image_name").val(val);
    });
}); 

$('#cancel').on('click', function(){
    window.location.href =   document.referrer;
});
</script>

<style>
#wish-non_pay_option label{
	margin-left : 25px  !important
}

</style>