<?php 
use yii\helpers\Html;
use app\models\Message;

if(Yii::$app->session->getFlash('success')!='') {?>
    <div class="alert alert-success" role="alert">
        <strong> <?= Yii::$app->session->getFlash('success'); ?>.</strong>
    </div>
<?php } ?>
		
<div class="row page-header">
    <div class="container my-profile">
	<div class="row">
            <h3 class="fnt-green" style="text-align: center;" >My Profile</h3>
            <div class="col-md-3">
                <div>
                    <?php 
                    if($profile->profile_image!='') 
                        echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
                    else 
                        echo '<img  src="'.\Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png"  class="img-responsive const-img-size-test" alt="my-profile-Image">';
                    ?>
                </div>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
                <div class="">
                    <p><b>Name : </b><span><?=$profile->firstname." ".$profile->lastname?></span></p>
                    <p><b>Location : </b><span><?=$profile->location?></span></p>
                    <p><b>About Me : </b><span style="white-space: pre-wrap;"><?=$profile->about?> </span></p>
                    <p>
                        <?php  echo Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                        '<i class="glyphicon glyphicon-log-out"></i> Logout',
                        ['class' => 'a-button fnt-blue']
                        )
                        . Html::endForm();  ?>
                    </p>
                </div>
            </div>
	</div>
	<div class="row smp-mg-bottom"></div>
	<div class="row link-thumb-contain" style="text-align: center;">
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>account/edit-account"><i class="fa fa-id-card fa-10x fnt-green" aria-hidden="true"></i>Account Info</a>
            </div>
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>wish/create"><i class="fa fa-pencil-square-o fa-10x fnt-pink" aria-hidden="true"></i>Add Wish</a>
            </div>
            <div class="col-sm-3">
                <?php
                     $deleteduser = ",".\Yii::$app->user->id.",";
                    $inbox_messages = Message::find()
                                ->select(['COUNT(*) AS cnt'])
                                ->where(['parent_id'=>0, 'read_text'=>0])
                                ->andwhere(['NOT LIKE','delete_status',$deleteduser ])
                                ->andwhere(['OR',['reply_recipient_id' => \Yii::$app->user->id],['reply_sender_id' => \Yii::$app->user->id],['and',['recipient_id'=>\Yii::$app->user->id],['reply_recipient_id' => 0 ]]])
                                ->all(); ?>
                <a href="<?=Yii::$app->homeUrl?>account/inbox-message"><i class="fa fa-comments-o fa-10x fnt-orange" aria-hidden="true"></i>Inbox
                    <?php if($inbox_messages[0]['cnt'] > 0) { ?>
                    <span class="inboxNotification"><?php echo $inbox_messages[0]['cnt']; ?></span>
                    <?php } ?>
                </a>
            </div>
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>account/my-account"><i class="fa fa-tasks fa-10x fnt-blue" aria-hidden="true"></i>Wishes</a>
            </div>
        </div>
        <div class="row link-thumb-contain" style="text-align: center;">
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>wish/my-drafts"><i class="fa fa-window-restore fa-10x fnt-grey" aria-hidden="true"></i>Drafts</a>
            </div>
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>account/my-friend"><i class="fa fa-group fa-10x fnt-grn-yellow " aria-hidden="true"></i>Friends</a>
            </div>
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>happy-stories/my-story"><i class="fa fa-vcard-o fa-10x fnt-brown" aria-hidden="true"></i>Happy Story</a>
            </div>
            <div class="col-sm-3">
                    <a href="<?=Yii::$app->homeUrl?>happy-stories/create"><i class="fa fa-newspaper-o fa-10x fnt-sea" aria-hidden="true"></i>Tell Story</a>
            </div>
	</div>
	<div class="col-md-12 smp-mg-bottom"></div>
    </div>
</div>

