<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Log In';
$this->params['breadcrumbs'][] = $this->title;
?>
<script>
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            testAPI();
        }
    }

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '726220308095495',
      cookie     : true,  // enable cookies to allow the server to access 
                          // the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v2.10' // use graph api version 2.8
    });
  };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    FB.api('/me?fields=email,first_name,last_name,picture.width(200).height(200)', function(response) {
        $.ajax({
            url : 'fb-sign-up',
            type: 'GET',
            data: {'res' : JSON.stringify(response)},
            success:function(data){
                $('#error').text(data);
            }
        }); 		
    });
}
</script>
<div class="site-login">
    <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>
      
    <p>Please fill out the following fields to login:</p>
    <p id="error" style="color: #a94442;"></p>
<?php 
	$sessioncheck = Yii::$app->session->getFlash('success');
	if(isset($sessioncheck) && !empty($sessioncheck)) { ?>
	<div id="w3-success-0" class="alert-success alert fade in">
	<button class="close" type="button" data-dismiss="alert" aria-hidden="true">×</button>
	<?= Yii::$app->session->getFlash('success'); ?>
	</div>
<?php } ?>

 <?php if (Yii::$app->session->hasFlash('activeCheckmail')){ ?>

        <div class="alert alert-success">
            Your E-Mail Validation Is Completed, Please Login Again. 
        </div>

    <?php } else if (Yii::$app->session->hasFlash('RegisterFormSubmitted')){ ?>

        <div class="alert alert-success">
            Your Registration Has Been Successfully Completed,  Please Check Your E-mail for validation. 
        </div>

    <?php } else if(Yii::$app->session->hasFlash('RegisterEmailNot')){ ?>
		<div  class="alert-danger alert ">
			Oops, Sorry Your E-Mail Is Not validated, Please Check Your E-Mail 
		</div>
	<?php } ?>
	
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

		<div class="form-group">
		
		<div class="col-lg-1"></div>
		<div class="col-lg-8">
		<div style="color:#999;">
                    Forgotten Password? reset it <?= Html::a('here', ['site/request-password-reset']) ?>.
                </div>
		</div>
		</div>
		 
				
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11" style="margin-bottom: 10px;">
                <?= Html::submitButton('Log In', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <?= Html::a('Sign Up', ['site/sign-up#signup'], ['class' => 'btn btn-success joinToday']) ?>
            </div>
            <fb:login-button class="col-lg-offset-1 col-lg-11" data-size="large" data-button-type="login_with" scope="public_profile,email" onlogin="checkLoginState();"></fb:login-button>
        </div>
        
    <?php ActiveForm::end(); ?>
</div>
