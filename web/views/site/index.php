<?php 
use yii\helpers\Url;
?>
<div class='intro_text'>
    <p class="homeIntroText">Make a Wish and Post it. Grant a Wish and Make Someone Happy. <br>Contribute to our Forum and Be Heard.</p>
    <div class='row intro_videos'>
        <div class="col-md-4 homeWishTpl">
            <a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=6&class=video">
                <img class='img-responsive' src="<?=Yii::$app->homeUrl?>web/uploads/editorial/Connect_Thumb_V2.jpg "/>
                <span class='glyphicon glyphicon-play-circle'></span>
            </a>
        </div>
        <div class="col-md-4 homeWishTpl">
            <a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=5&class=video">
                <img class='img-responsive' src="<?=Yii::$app->homeUrl?>web/uploads/editorial/Do You Have A Wish_Thumb.png"/>
                <span class='glyphicon glyphicon-play-circle'></span>
            </a>
        </div>
        <div class="col-md-4 homeWishTpl">
            <a href="<?=Yii::$app->homeUrl?>editorial/editorial-page?id=4&class=video">
                <img class='img-responsive' src="<?=Yii::$app->homeUrl?>web/uploads/editorial/Social Networking Thumb.png"/>
                <span class='glyphicon glyphicon-play-circle'></span>
            </a>
        </div>
    </div>
</div>

<div class="simply-head"><h3 class="fnt-skyblue home_heading">Current Wishes</h3></div>

<div>    
    <section class="row">
        <?php foreach($current as $model){
            $class  =   '';
        echo '<div class="col-md-4 homeWishTpl"><div class="tplColor homeTplBlk">';
        echo '<div><a href="'.Url::to(['wish/view','id'=>$model->w_id]).'"><img src="'.\Yii::$app->homeUrl.'web/'.$model->primary_image.'?v='.strtotime('now').'" class="img-responsive wishImage" alt="Image"></a></div>';
          if($model->likesCount > 0)
              $class    =   'likesView';
          echo  '<div class="smp-wish-desc">';
                echo  '<p><div class="list-icon">
                            <img src="'.$model->wisherPic.'" alt="" />
                            <a href="'.Url::to(['account/profile','id'=>$model->wished_by]).'"><span>'.$model->wisherName.'</span></a>
                        </div></p>
                <p class="desc">'.substr($model->wish_title,0,50).'..</p>
                <p><a class="fnt-green" href="'.Url::to(['wish/view','id'=>$model->w_id]).'">+Read More</a>
                &nbsp;<span data-id="'.$model->w_id.'" class="'.$class.'"> <span id="likecmt_'.$model->w_id.'"  >'.$model->likesCount.'</span> '.($model->likesCount > 1 ? "Loves" : "Love").'</span></p>';

          echo  '</div>';

          echo  '</div></div>';
        }?>
    </section>
</div>
<div class="simply-head pull-right">
    <a href="<?=\Yii::$app->homeUrl?>wish/index"><button class="btn btn-smp-green smpl-brdr" type="button">SEE MORE CURRENT WISHES</button></a>
</div>

<div class="simply-head"><h3 class="fnt-skyblue home_heading">Granted Wishes</h3></div>

<div>    
    <section class="row">
        <?php foreach($models as $model){
            $class  =   '';
        echo '<div class="col-md-4 homeWishTpl"><div class="tplColor homeTplBlk">';
        echo '<div><a href="'.Url::to(['wish/view','id'=>$model->w_id]).'"><img src="'.\Yii::$app->homeUrl.'web/'.$model->primary_image.'?v='.strtotime('now').'" class="img-responsive wishImage" alt="Image" ></a></div>';
          if($model->likesCount > 0)
              $class    =   'likesView';
          echo  '<div class="smp-wish-desc">';
                echo  '<p><div class="list-icon">
                                <img src="'.$model->wisherPic.'" alt="" />
                                <a href="'.Url::to(['account/profile','id'=>$model->wished_by]).'"><span>'.$model->wisherName.'</span></a>
                        </div></p>
                <p class="desc">'.substr($model->wish_title,0,50).'..</p>
                <p><a class="fnt-green" href="'.Url::to(['wish/view','id'=>$model->w_id]).'">+Read More</a>
                &nbsp;<span data-id="'.$model->w_id.'" class="'.$class.'"> <span id="likecmt_'.$model->w_id.'"  >'.$model->likesCount.'</span> '.($model->likesCount > 1 ? "Loves" : "Love").' </span>
                <span><img class="pull-right" src="'.\Yii::$app->homeUrl.'web/images/tick.png" width="30"></span></p>';
                
          echo  '</div>';

          echo  '</div></div>';
        }?>
    </section>
</div>
<div class="simply-head pull-right">
    <a href="<?=\Yii::$app->homeUrl?>wish/granted"><button class="btn btn-smp-green smpl-brdr" type="button">SEE MORE GRANTED WISHES</button></a>
</div>

<div class="simply-head"><h3 class="fnt-skyblue home_heading">Forum</h3></div>

<div>    
    <section class="row">
        <?php foreach($forums as $forum){
            $profile = \app\models\UserProfile::find()->where(['user_id'=>$forum->created_by])->one();
            echo '<div class="col-md-4 homeWishTpl"><div class="tplColor homeTplBlk">';
        
            echo  '<div class="smp-wish-desc">';
            echo  '<h3><a href="'.\Yii::$app->homeUrl.'editorial/editorial-page?id='.$forum->e_id.'&class=article" style="color: #000;">'.$forum->e_title.'</a></h3>
                    <div class="list-icon">
                                <img src="'.\Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now').'" alt="" />
                                <a href="'.Url::to(['account/profile','id'=>$forum->created_by]).'"><span>'.$profile->Fullname.'</span></a>
                        </div>                
                    <div class="article_intro">'.substr($forum->e_text,0,200).'..</div>
                <a href="'.\Yii::$app->homeUrl.'editorial/editorial-page?id='.$forum->e_id.'&class=article"><h5>Read More</h5></a>';
                
          echo  '</div>';

          echo  '</div></div>';
        }?>
    </section>
</div>
<div class="simply-head pull-right" style='margin-bottom: 35px;'>
    <a href="<?=\Yii::$app->homeUrl?>editorial/editorial#forum"><button class="btn btn-smp-green smpl-brdr" type="button">SEE MORE ARTICLES & VIDEOS</button></a>
</div>

<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(".shareIcons").each(function(){
            var elem = $(this);
                    elem.jsSocials({
                    showLabel: false,
                    showCount: true,
                    shares: ["facebook","googleplus", "linkedin",
                    {
                            share: "twitter",           // name of share
                            via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                            hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
                    }],
                    url : elem.attr("data_url"),
                    text: elem.attr("data_text"),
            });
    });

    $(document).on('click', '.like-wish, .fav-wish', function(){ 
   //$(".like-wish, .fav-wish").on("click",function(){
           var wish_id = $(this).attr("data-w_id");
           var type = $(this).attr("data-a_type");
           var elem = $(this);
           $.ajax({
                   url : '<?=Url::to(['wish/like'])?>',
                   type: 'GET',
                   data: {w_id:wish_id,type:type},
                   success:function(data){
                           if(data == "added"){
                                   if(type=="fav"){
                                           elem.removeClass("txt-smp-orange");
                                           elem.addClass("txt-smp-blue");
                                   }
                                   if(type=="like"){
                                          elem.css('color', '#B23535');
                                           var likecmt = $("#likecmt_"+wish_id).text();
                                           likecmt = parseInt(likecmt) + parseInt(1);
                                           $("#likecmt_"+wish_id).text(likecmt);					
                                   }
                           }
                           if(data == "removed"){
                                   if(type=="fav"){
                                           elem.addClass("txt-smp-orange");
                                           elem.removeClass("txt-smp-blue");
                                   }
                                   if(type=="like"){
                                           elem.css('color', '#fff');
                                           var likecmt = $("#likecmt_"+wish_id).text();
                                           likecmt = parseInt(likecmt) - parseInt(1);
                                           $("#likecmt_"+wish_id).text(likecmt);

                                   }
                           }
                   }
           }); 		
   });

    $(document).ready(function(){	
        var isVisible = false;
        var clickedAway = false;
	$(function(){
		$('.listesinside').popover({   
			html: true,
			content: function () {
				var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
				return clone;
			}
		}).click(function(e) {
			//e.preventDefault();
			clickedAway = false;
			isVisible = true;
		});
	});
	/* $(document).on('click','.jssocials-shares',function(){
		 $('.listesinside').popover('hide');
	}); */
	
        $(document).click(function (e) {
            if (isVisible & clickedAway) {  
                $('.listesinside').popover('hide');
                isVisible = clickedAway = false;
            } else {
                clickedAway = true;
            }
        });

        $('body').on('hidden.bs.popover', function (e) {
            $(e.target).data("bs.popover").inState.click = false;
        });
var Wish    =   {
    ajax    :   false
};
        $(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                $.ajax({
                    url : '<?=Url::to(['wish/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
    });


/* 
$(document).on("click", ".report-img", function() {
	var wish_id = $(this).attr("data-id");
	 if($.trim(wish_id) !== "" )
	 {
		$.ajax({
			url : '<?=Url::to(['wish/report'])?>',
			type: 'GET',
			data: {w_id:wish_id},
			success:function(data){
				if($.trim(data) == "added")
				{
					alert(" Thanks For your Report. ");
				}
				console.log(data);
			}
		});
	 }
}); */


</script>
