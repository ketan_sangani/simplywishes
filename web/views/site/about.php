<?php 
use yii\helpers\Url;
use app\models\UserProfile;
use yii\helpers\Html;

$this->title = 'About Us';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about smp-mg-bottom">
    <h3 class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>
    <?php if(!Yii::$app->user->isGuest && \Yii::$app->user->identity->role == 'admin'){?>
    <p>
        <?= Html::a('Update', ['page/update', 'id' => $model->p_id], ['class' => 'btn btn-primary']) ?>
    </p>
    <?php } ?>
    <p>
        SimplyWishes.com was founded with the purpose of creating a community where people help each other to realize their dreams. Browse through our site and help someone’s wish become a reality or post a wish yourself and let us connect you with people that can help make your dream come true.
    </p>
    <div class="row">
        <div class="col-md-6" style="overflow:auto;"><img class="img-responsive aboutImage" src="<?= Yii::$app->homeUrl?>web/images/about.jpg" width="230"></div>
        <div class="col-md-6"><?= $model->content?></div>
    </div>
</div>

<!--------------- SLIDER CHECK Function ---------------------->
  <link rel="stylesheet" type="text/css" href="<?= Yii::$app->homeUrl?>web/src/slick/slick.css">
  <link rel="stylesheet" type="text/css" href="<?= Yii::$app->homeUrl?>web/src/slick/slick-theme.css"> 
  <script src="<?= Yii::$app->homeUrl?>web/src/slick/jquery-2.2.0.min.js" type="text/javascript"></script>
  <script src="<?= Yii::$app->homeUrl?>web/src/slick/slick.js" type="text/javascript" charset="utf-8"></script>
  
  
  
  <script type="text/javascript">
  var js = $.noConflict();
   js(document).on('ready', function() {
      js(".regular").slick({
         dots: false,
	 infinite: true,
	 speed: 300,
	 slidesToShow: 5,
	 slidesToScroll: 1,
	 responsive: [
		{
		 breakpoint: 1024,
		 settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
			infinite: true,
			dots: false
		 }
		},
		{
		 breakpoint: 600,
		 settings: {
			slidesToShow: 2,
			slidesToScroll: 1
		 }
		},
		{
		 breakpoint: 480,
		 settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		 }
		}
	 ]
      });
     
	 
    });
  </script>
  
 
  
  <!--------------- SLIDER CHECK Function END ---------------->
 
