<div style='margin: auto; width: 60%; padding-top: 5%'>
    <p>Thank you <b><?= $profile->firstname ?></b>. Your registration has been submitted.</p>
    <p>
        You will receive an e-mail with instructions on the next step in your Inbox. 
        If you do not receive the e-mail within the next 10 minutes (usually instantly),
        please also check your Spam or Junk box.
    </p>
    <p>The email has been sent to <b><?= $user->email ?></b> </p>
</div>


