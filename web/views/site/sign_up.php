<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>

<link rel="stylesheet" type="text/css" href="<?=Yii::$app->homeUrl?>web/css/croppie.css">
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>web/src/croppie.js"></script>
 
<div class="row page-header">
    <div class="container col-md-8">
        <?php $form = ActiveForm::begin(['id' => 'contact-form','options' => ['enctype'=>'multipart/form-data']]); ?>
        <h3 class="fnt-skyblue"  ><?= Html::encode($this->title) ?></h3>
        <?= $form->field($user, 'username')->textInput(['autofocus' => true])->label("Username <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($user, 'email')->label("Email Address <span class='valid-star-color' >*</span>") ?>

        <?= $form->field($profile, 'firstname')->label('First Name <span class="valid-star-color" >*</span>') ?>

        <?= $form->field($profile, 'lastname')->label('Last Name <span class="valid-star-color" >*</span>') ?>

        <?= $form->field($profile, 'about')->textarea(['rows' => 3])->label('About me')?>
	<div class="col-lg-4">
            <?= $form->field($profile, 'country')->widget(Select2::classname(), [
                'data' => $countries,
                'options' => ['placeholder' => '--Select Country--', 'onchange' => '$.post( "'.Yii::$app->urlManager->createUrl('site/get-states?country_id=').'"+$(this).val(), function( data ) 
                    {
                            $( "select#userprofile-state" ).html( data ).change();

                    });'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("Country <span class='valid-star-color' >*</span> "); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($profile, 'state')->widget(Select2::classname(), [
                'options' => ['placeholder' => '--Select State--', 'onchange' => '$.post( "'.Yii::$app->urlManager->createUrl('site/get-cities?state_id=').'"+$(this).val(), function( data ) 
                    {
                        $( "select#userprofile-city" ).html( data ).change();
                    });'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("State"); ?>
        </div>
        <div class="col-lg-4">
             <?= $form->field($profile, 'city')->widget(Select2::classname(), [
                'options' => ['placeholder' => '--Select City--'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("City"); ?>
        </div>
        <?= $form->field($user, 'password')->passwordInput()->label("Password <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($user, 'verify_password')->passwordInput()->label("Verify Password <span class='valid-star-color' >*</span> ") ?>
        
        <p><label>Profile Image</label></p>
        <div id="upload-img" class="hide"></div>
        <div>
            <img class="image_block hide" id="image" width="200" />
            <a href="#" class="removeImage hide">Remove</a>
        </div>
        <div class="actions">
            <a class="btn file-btn btn btn-primary">
                <span>Choose an Image from your Files</span>
                <input type="file" id="upload" value="Choose a file" accept="image/*" />
            </a>
            <img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loader" class="hide" width="30">
            <div class="upload-buttons hide">
                <button class="upload-result btn btn-primary">Upload</button>
                <button class="upload-rotate btn btn-primary" data-deg="-90">Rotate Left</button>
                <button class="upload-rotate btn btn-primary" data-deg="90">Rotate Right</button>
            </div>
        </div>
        <?= $form->field($profile, 'profile_image')->hiddenInput()->label(false) ?>
        <span>Or, Choose One from the Default Images below</span>         
        <div class="gravatar thumbnail">
            <a class="profilelogo" for="images/img1.jpg" ><img class="selected" src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img1.jpg"/></a>
            <a class="profilelogo" for="images/img2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img2.jpg"/></a>
            <a class="profilelogo" for="images/img3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img3.jpg"/></a>
            <a class="profilelogo" for="images/img4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img4.jpg"/></a>
            <a class="profilelogo" for="images/img6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img6.jpg"/></a>
            <a class="profilelogo" for="images/img7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img7.jpg"/></a>
            <a class="profilelogo" for="images/img8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img8.jpg"/></a>
            <a class="profilelogo" for="images/img9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img9.jpg"/></a>
            <a class="profilelogo" for="images/img10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img10.jpg"/></a>
            <a class="profilelogo" for="images/img11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img11.jpg"/></a>
            <a class="profilelogo" for="images/img12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img12.jpg"/></a>
            <a class="profilelogo" for="images/img13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img13.jpg"/></a>
            <a class="profilelogo" for="images/img14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img14.jpg"/></a>
            <a class="profilelogo" for="images/img15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img15.jpg"/></a>
            <a class="profilelogo" for="images/img16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img16.jpg"/></a>
            <a class="profilelogo" for="images/img17.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img17.jpg"/></a>
            <a class="profilelogo" for="images/img18.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img18.jpg"/></a>
            <a class="profilelogo" for="images/img19.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img19.jpg"/></a>
            <a class="profilelogo" for="images/img20.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img20.jpg"/></a>
            <a class="profilelogo" for="images/img21.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img21.jpg"/></a>
            <a class="profilelogo" for="images/img22.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img22.jpg"/></a>
            <a class="profilelogo" for="images/img23.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img23.jpg"/></a>
            <a class="profilelogo" for="images/img24.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img24.jpg"/></a>
            <a class="profilelogo" for="images/img25.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img25.jpg"/></a>
            <a class="profilelogo" for="images/img26.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img26.jpg"/></a>
            <a class="profilelogo" for="images/img27.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img27.jpg"/></a>
            <a class="profilelogo" for="images/img28.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img28.jpg"/></a>
            <a class="profilelogo" for="images/img29.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img29.jpg"/></a>
            <a class="profilelogo" for="images/img31.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img31.jpg"/></a>
            <a class="profilelogo" for="images/img32.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img32.jpg"/></a>
            <a class="profilelogo" for="images/img33.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img33.jpg"/></a>
            <a class="profilelogo" for="images/img34.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img34.jpg"/></a>
            <a class="profilelogo" for="images/img35.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img35.jpg"/></a>
            <a class="profilelogo" for="images/img36.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img36.jpg"/></a>
        </div>
	
        <?= $form->field($profile, 'dulpicate_image')->hiddenInput(['value'=>'images/img1.jpg'])->label(false) ?>
	<div class="checkbox">
            <label class="checkbox-inline"><input  type="checkbox" required class="terms" value="">I Agree to the 
                <a data-toggle="modal" data-target="#termsmodal" >Terms Of Use</a> , 
                <a data-toggle="modal" data-target="#communitymodal" >Community Guidlines</a> and 
                <a data-toggle="modal" data-target="#policymodal" >Privacy Policy</a>	
                <span class="valid-star-color" >*</span>
            </label>
	</div>
        </br>
	<!-- Terms modal Starts -->
	<div class="modal fade" id="termsmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Acceptance of Terms of Use</h4>
                    </div>
                    <div class="modal-body">
                        <p><?= $terms->content?></p>
                    </div>
                </div>
            </div>
	</div>
	<!-- Terms modal Ends -->
	
	<!-- Community modal Starts -->
	<div class="modal fade" id="communitymodal" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document" >
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Acceptance of Community Guidelines</h4>
		  </div>
		  <div class="modal-body">
			<p><?= $community_guidelines->content  ?></p>
		  </div>
		</div>
	  </div>
	</div>
	<!-- Community modal Ends -->
	
	<!-- Privacy Policy Modal Starts -->
	<div class="modal fade" id="policymodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Acceptance of Privacy</h4>
                      </div>
                      <div class="modal-body">
                          <p><?= $privacy_policy->content ?></p>
                      </div>
                  </div>
            </div>
	</div>
	<!-- Privacy Policy Modal Ends -->
	<div class="form-group">
            <?= Html::submitButton('Sign Up', ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>		
</div>

<script type="text/javascript"  >

$('.agree').click(function(){ 
 	var cat = $(this).data('cat');
	var id = $(this).data('id');
	 $('.'+cat).prop('checked', true);
	$(id).modal('hide'); 
}); 

$('.profilelogo').click(function(){
 $('.profilelogo').find( "img" ).removeClass('selected'); 
  var val = $(this).attr('for');
  $(this).find( "img" ).addClass('selected'); 
  $("#userprofile-dulpicate_image").val(val);
});
    
$(document).ready(function(){
    $uploadCrop = $('#upload-img').croppie({
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
        },
        boundary: {
            width: 300,
            height: 300
        },
        enableOrientation: true
    });

    $('#upload').on('change', function () { readFile(this); });

    function readFile(input) {
        $('#upload-img').removeClass('hide');
        $('#loader').removeClass('hide');
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.upload-img').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    $('#loader').addClass('hide');
                    $('.upload-buttons').removeClass('hide');
                });
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.upload-result').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (resp) {
            $('#userprofile-profile_image').val(resp);
            $('#image').attr('src', resp).removeClass('hide');
            $('.removeImage').removeClass('hide');
            $('html, body').animate({
                scrollTop: document.body.scrollHeight
            }, 'slow');
        });
    });
    
    $('.removeImage').on('click', function(e){
        e.preventDefault();
        $('#userprofile-profile_image').val('');
        $('#image').attr('src', '').addClass('hide');
        $(this).addClass('hide');
    });
    
    $('.upload-rotate').on('click', function(e){
        e.preventDefault();
        $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
    });
    
    $('#contact-form').on('afterValidate', function (event, messages) {
        if(typeof $('.has-error').first().offset() !== 'undefined') {
            $('html, body').animate({
                scrollTop: $('.has-error').first().offset().top - 200
            }, 1000);
        }
    });
    
    $('ul.nav li.dropdown').hover(function() { 
	$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() { 
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    }); 
});
	
</script>

