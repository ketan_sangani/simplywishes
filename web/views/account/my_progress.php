<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/masonry.js" type="text/javascript"></script>
<script src="<?= Yii::$app->request->baseUrl?>/web/src/imagesloaded.js" type="text/javascript"></script>	
	<?php echo $this->render('_profilenew',['user'=>$user,'profile'=>$profile])?>
	<!-- To replace tab as link remove data-toggle=tab and replace href with link-->
<div class="col-md-8" id="scroll-evt">
    <div class="marginPos">
	<ul class="nav nav-tabs smp-mg-bottom" role="tablist">
	  <li role="presentation">
		<a href="<?=\Yii::$app->homeUrl?>account/my-account" role="tab">My Active Wishes</a>
	  </li>
        <li class="dropdown active open">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">In Progress <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li class="active"><a href="#processwish" role="tab" data-toggle="tab">Wishes</a></li>
                <li><a href="#grantwish" role="tab" data-toggle="tab">Grants</a></li>                       
            </ul>
        </li>
        <li class="dropdown">
            <a href="<?=\Yii::$app->homeUrl?>account/my-fullfilled" role="tab">Granted <span class="caret"></span></a>
        </li>
	  <li role="presentation">
		<a href="<?=\Yii::$app->homeUrl?>account/my-saved" role="tab" >My Saved Wishes</a>
	  </li>
	 
	</ul>
	<div class="tab-content">
            <div role="tabpanel" class="tab-pane grid active" id="processwish">
                <?php

                    if( ! empty($dataProvider->models))
                    {
                        foreach($dataProvider->models as $wish){
                            echo $wish->htmlForProfile;
                        }
                    }
                    else
                        echo "No In-progress Wishes!";
                ?>
            </div>
            <div role="tabpanel" class="tab-pane grid" id="grantwish">
                <?php 
                    if( ! empty($dataProvider2->models))
                    {
                        foreach($dataProvider2->models as $wish){
                            echo $wish->htmlForProfile;
                        }
                    }
                    else
                        echo "No In-progress Grants!";
                ?>			
            </div>
        </div>
   </div>
</div>
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
	<script>
	$(".shareIcons").each(function(){
    var elem = $(this);
        elem.jsSocials({
        showLabel: false,
        showCount: true,
        shares: ["facebook","googleplus", "linkedin",
        {
                share: "twitter",           // name of share
                via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
        }],
        url : elem.attr("data_url"),
        text: elem.attr("data_text"),
    });
});
        $(document).on('load', function(){
            var $container = $('.grid');
            $container.imagesLoaded(function(){
                $container.animate({ opacity: 1 });
                $(".shareIcons").each(function(){
                        var elem = $(this);
                                elem.jsSocials({
                                showLabel: false,
                                showCount: true,
                                shares: ["facebook","googleplus", "linkedin",
                                {
                                        share: "twitter",           // name of share
                                        via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                                        hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
                                }],
                                url : elem.attr("data_url"),
                                text: elem.attr("data_text"),
                        });
                });
                $container.masonry();
            });
        });
	$(document).on('click', '.like-wish, .fav-wish', function(){ 
	//$(".like-wish, .fav-wish").on("click",function(){
		var wish_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['wish/like'])?>',
			type: 'GET',
			data: {w_id:wish_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+wish_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+wish_id).text(likecmt);
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+wish_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+wish_id).text(likecmt);
					}
				}

				console.log(data);
			}
		});
	});	
var isVisible = false;
var clickedAway = false;

$(document).on("click", ".listesinside", function() {
	$(function(){
		$('.listesinside').popover({   
			html: true,
			content: function () {
				var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
				return clone;
			}
		}).click(function(e) {
			e.preventDefault();
			clickedAway = false;
			isVisible = true;
		});
	});
 
});

$(document).click(function (e) {
    if (isVisible & clickedAway) {
        $('.listesinside').popover('hide');
        isVisible = clickedAway = false;
    } else {
        clickedAway = true;
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});
$(window).on('load', function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
});

var Wish    =   {
    ajax    :   false
};

$(document).on('click', '.likesView', function(){ 
    var val =   $(this).find('span').html();
    if(val <= 0)
        return false;

    var wish_id = $(this).data('id');
    if($.trim(wish_id) !== "" )
    {
        if(Wish.ajax)
            return false;

        Wish.ajax   =   true;
                
        $.ajax({
            url : '<?=Url::to(['wish/likes-view'])?>',
            type: 'GET',
            data: {w_id:wish_id},
            success:function(data){
                if(data)
                {
                    var tpl =   '';
                    var items   =   JSON.parse(data);
                    $.each(items, function(k,v){
                       tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                    });

                    $('.modal-body').append(tpl);
                    $('#likeModal').modal({backdrop: 'static', keyboard: false});
                    $('#likeModal').modal('show');

                    $('.closeModal').on('click', function(e){
                        e.preventDefault();
                        $('.modal-body').html('');
                        $('#likeModal').modal('hide');
                    });
                }
            },
            complete: function(){
                Wish.ajax   =   false;
            }
        });
    }
});
/* $(document).on('click','.jssocials-shares',function(){
		 $('.listesinside').popover('hide');
	}); */

/* $(document).on("click", ".report-img", function() {
	var wish_id = $(this).attr("data-id");
	 if($.trim(wish_id) !== "" )
	 {
		$.ajax({
			url : '<?=Url::to(['wish/report'])?>',
			type: 'GET',
			data: {w_id:wish_id},
			success:function(data){
				if($.trim(data) == "added")
				{
					alert(" Thanks For your Report. ");
				}
				console.log(data);
			}
		});
	 }
}); */

</script>