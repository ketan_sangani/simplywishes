<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\select2\Select2;

$this->title = 'My Account';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" type="text/css" href="<?=Yii::$app->homeUrl?>web/css/croppie.css">
<script type="text/javascript" src="<?=Yii::$app->homeUrl?>web/src/croppie.js"></script>
<?php echo $this->render('_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class="container site-contact col-md-8" id="scroll-evt">
    <h3 class="fnt-skyblue" ><?= Html::encode($this->title) ?></h3>
    <div>
        <?php if(Yii::$app->session->getFlash('success')!='') {?>
                <!--<div class="alert alert-success" role="alert">
                        <strong> <?= Yii::$app->session->getFlash('success'); ?>.</strong>
                </div>-->

        <?php }
        $form = ActiveForm::begin(['id' => 'contact-form','options' => ['enctype'=>'multipart/form-data']]); ?>

        <?= $form->field($user, 'username')->textInput(['autofocus' => true,'disabled' => true]) ?>

        <?= $form->field($user, 'email')->label("Email <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($profile, 'firstname')->label("First Name <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($profile, 'lastname')->label("Last Name <span class='valid-star-color' >*</span> ") ?>

        <?= $form->field($profile, 'about')->textarea(['rows' => 3])->label('About me') ?>
        <div class="col-lg-4">
            <?= $form->field($profile, 'country')->widget(Select2::classname(), [
                'data' => $countries,
                'options' => ['placeholder' => '--Select Country--', 'onchange' => '$.post( "'.Yii::$app->urlManager->createUrl('site/get-states?country_id=').'"+$(this).val(), function( data ) 
                    {
                            $( "select#userprofile-state" ).html( data ).change();

                    });'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("Country <span class='valid-star-color' >*</span> "); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($profile, 'state')->widget(Select2::classname(), [
                'data' => $states,
                'options' => ['placeholder' => '--Select State--', 'onchange' => '$.post( "'.Yii::$app->urlManager->createUrl('site/get-cities?state_id=').'"+$(this).val(), function( data ) 
                    {
                        $( "select#userprofile-city" ).html( data ).change();
                    });'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("State"); ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($profile, 'city')->widget(Select2::classname(), [
                'data' => $cities,
                'options' => ['placeholder' => '--Select City--'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label("City"); ?>
        </div>
        
        <p><label>Profile Image</label></p>
        <div id="upload-img" class="hide"></div>
        <div>
            <img class="image_block" id="image" src="<?= \Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now')?>" width="200" />
            <a href="#" class="removeImage">Remove</a>
        </div>
        <div class="actions">
            <a class="btn file-btn btn btn-primary">
                <span>Choose an Image from your Files</span>
                <input type="file" id="upload" value="Choose a file" accept="image/*" />
            </a>
            <img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif" id="loader" class="hide" width="30">
            <div class="upload-buttons hide">
                <button class="upload-result btn btn-primary">Upload</button>
                <button class="upload-rotate btn btn-primary" data-deg="-90">Rotate Left</button>
                <button class="upload-rotate btn btn-primary" data-deg="90">Rotate Right</button>
            </div>
        </div>
        <?= $form->field($profile, 'profile_image')->hiddenInput()->label(false) ?>
        <span>Or, Choose One from the Default Images below</span>       
        <div class="gravatar thumbnail">
            <a class="profilelogo" for="images/img1.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img1.jpg"/></a>
            <a class="profilelogo" for="images/img2.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img2.jpg"/></a>
            <a class="profilelogo" for="images/img3.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img3.jpg"/></a>
            <a class="profilelogo" for="images/img4.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img4.jpg"/></a>
            <a class="profilelogo" for="images/img6.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img6.jpg"/></a>
            <a class="profilelogo" for="images/img7.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img7.jpg"/></a>
            <a class="profilelogo" for="images/img8.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img8.jpg"/></a>
            <a class="profilelogo" for="images/img9.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img9.jpg"/></a>
            <a class="profilelogo" for="images/img10.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img10.jpg"/></a>
            <a class="profilelogo" for="images/img11.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img11.jpg"/></a>
            <a class="profilelogo" for="images/img12.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img12.jpg"/></a>
            <a class="profilelogo" for="images/img13.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img13.jpg"/></a>
            <a class="profilelogo" for="images/img14.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img14.jpg"/></a>
            <a class="profilelogo" for="images/img15.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img15.jpg"/></a>
            <a class="profilelogo" for="images/img16.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img16.jpg"/></a>
            <a class="profilelogo" for="images/img17.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img17.jpg"/></a>
            <a class="profilelogo" for="images/img18.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img18.jpg"/></a>
            <a class="profilelogo" for="images/img19.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img19.jpg"/></a>
            <a class="profilelogo" for="images/img20.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img20.jpg"/></a>
            <a class="profilelogo" for="images/img21.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img21.jpg"/></a>
            <a class="profilelogo" for="images/img22.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img22.jpg"/></a>
            <a class="profilelogo" for="images/img23.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img23.jpg"/></a>
            <a class="profilelogo" for="images/img24.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img24.jpg"/></a>
            <a class="profilelogo" for="images/img25.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img25.jpg"/></a>
            <a class="profilelogo" for="images/img26.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img26.jpg"/></a>
            <a class="profilelogo" for="images/img27.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img27.jpg"/></a>
            <a class="profilelogo" for="images/img28.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img28.jpg"/></a>
            <a class="profilelogo" for="images/img29.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img29.jpg"/></a>
            <a class="profilelogo" for="images/img31.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img31.jpg"/></a>
            <a class="profilelogo" for="images/img32.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img32.jpg"/></a>
            <a class="profilelogo" for="images/img33.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img33.jpg"/></a>
            <a class="profilelogo" for="images/img34.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img34.jpg"/></a>
            <a class="profilelogo" for="images/img35.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img35.jpg"/></a>
            <a class="profilelogo" for="images/img36.jpg" ><img src="<?=Yii::$app->homeUrl?>web/uploads/users/images/img36.jpg"/></a>
        </div>

        <?= $form->field($profile, 'dulpicate_image')->hiddenInput()->label(false) ?>	
        <h3  class="fnt-green" > Change Password </h3>
        <p> Change password if you want to, or leave it empty</p>

        <?= $form->field($user, 'password')->passwordInput() ?>

        <?= $form->field($user, 'verify_password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Update', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<script type="text/javascript" >
    
    $('.profilelogo').click(function(){
        $('.profilelogo').find( "img" ).removeClass('selected'); 
        var val = $(this).attr('for');
        $(this).find( "img" ).addClass('selected'); 
        $("#userprofile-dulpicate_image").val(val);
    });

    $(document).ready(function(){
        $uploadCrop = $('#upload-img').croppie({
            enableExif: true,
            viewport: {
                width: 200,
                height: 200
            },
            boundary: {
                width: 300,
                height: 300
            },
            enableOrientation: true
        });
        
        $('#upload').on('change', function () { readFile(this); });
        
        function readFile(input) {
            $('#upload-img').removeClass('hide');
            $('#loader').removeClass('hide');
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.upload-img').addClass('ready');
                    $uploadCrop.croppie('bind', {
                        url: e.target.result
                    }).then(function(){
                        $('#loader').addClass('hide');
                        $('.upload-buttons').removeClass('hide');
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
            else
                $('#loader').addClass('hide');
        }
        
        $('.upload-result').on('click', function(e){
            e.preventDefault();
            $uploadCrop.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            }).then(function (resp) {
                $('#userprofile-profile_image').val(resp);
                $('#image').attr('src', resp).removeClass('hide');
                $('.removeImage').removeClass('hide');
                $('html, body').animate({
                    scrollTop: $('.field-user-password').offset().top - 100
                }, 'slow');
            });
        });
        
        $('.upload-rotate').on('click', function(e){
            e.preventDefault();
            $uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
        });
        
        $('.removeImage').on('click', function(e){
            e.preventDefault();
            $('#userprofile-profile_image').val('');
            $('#image').attr('src', '').addClass('hide');
            $(this).addClass('hide');
        });
    });
    
    $(window).on('load', function() {
        $('html, body').animate({
            scrollTop: $('#scroll-evt').offset().top - 100
        }, 'slow');
    });
</script>


