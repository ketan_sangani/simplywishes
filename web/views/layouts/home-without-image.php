<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\models\UserProfile;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Message;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<title>SimplyWishes</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?= Html::csrfMetaTags() ?>
	<link rel="shortcut icon" type="image/png" href="<?=Yii::$app->homeUrl?>web/images/favicon.png?v=1"/>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!--***** Header Starts*****-->
<div class="navbar navbar-fixed-top smp-head">
    <div class="container nav-without-image-hmepge">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="margin: -5px 10px;" href="<?=Yii::$app->homeUrl?>"><img class="img-responsive" src="<?=Yii::$app->homeUrl?>web/images/logo.png" width="150"></a>
        </div>
        <?php $detect = new Mobile_Detect; ?>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right smp-pills">
                <?php
                if(!$detect->isMobile() && !$detect->isTablet()) { ?>
                    <li data-id="home"><a href="<?=Yii::$app->homeUrl?>" style="height: 30px;"><span style="margin-top: -10px;" class="glyphicon glyphicon-home"></span></a></li>
                <?php } ?>
                <li data-id="post_wish"><a href="<?=Yii::$app->homeUrl?>wish/create#post_wish">Make a Wish</a></li>
                <li data-id="search_wish"><a href="<?=Yii::$app->homeUrl?>wish/index#search_wish">Grant a Wish</a></li>
                <li data-id="forum"> 
                    <?php if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ ?>
                    <a href="<?=Yii::$app->homeUrl?>editorial/index#forum">Forum</a>
                    <?php } else { ?> 
                    <a href="<?=Yii::$app->homeUrl?>editorial/editorial#forum">Forum</a>
                    <?php } ?>			
                </li>
                <?php if(!\Yii::$app->user->isGuest){  
                    $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one(); ?>
                    <li data-id="edt_home" class="dropdown mainNav" class="active"><a href="<?=Yii::$app->homeUrl?>site/index-home#edt_home" style="display: inline-block; padding-right: 0px;" >Hi, <?php echo substr(\Yii::$app->user->identity->username,0,8)?>..!</a>
                        <img src="<?=\Yii::$app->homeUrl?>web/uploads/users/<?php echo $profile->profile_image ?>?v=<?= strtotime('now') ?>" class="profile-img" width='20px' height='20px'>
                        <?php
                        $detect = new Mobile_Detect;
                        $deleteduser = ",".\Yii::$app->user->id.",";	
                        $inbox_messages = Message::find()
                                ->select(['COUNT(*) AS cnt'])
                                ->where(['parent_id'=>0, 'read_text'=>0])
                                ->andwhere(['NOT LIKE','delete_status',$deleteduser ])
                                ->andwhere(['OR',['reply_recipient_id' => \Yii::$app->user->id],['and',['recipient_id'=>\Yii::$app->user->id],['reply_recipient_id' => 0 ]]])
                                ->all();
                         if($inbox_messages[0]['cnt'] > 0) { ?>
                            <span class="notification"><?php echo $inbox_messages[0]['cnt']; ?></span>
                         <?php }
                        if(!$detect->isMobile() && !$detect->isTablet()) { ?>
                        <ul class="dropdown-menu nav nav-stacked">
                            <li><a href="<?=Yii::$app->homeUrl?>account/inbox-message"><i class="fa fa-inbox fa-lg"></i> 
                                Inbox 
                                <?php if($inbox_messages[0]['cnt'] > 0) { ?>
                                    <span class="inboxNotification"><?php echo $inbox_messages[0]['cnt']; ?></span>
                                <?php } ?>
                            </a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>account/my-account"><i class="fa fa-heart"></i>My Wishes</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>wish/my-drafts"><i class="fa fa-window-restore"></i>My wish Drafts</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>donation/my-drafts"><i class="fa fa-window-restore"></i>My donation Drafts</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>account/my-friend"><i class="fa fa-users"></i>Friends</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>account/my-saved"><i class="fa fa-save fa-lg"></i>Saved Wishes</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>happy-stories/create"><i class="fa fa-commenting-o"></i>Tell Your Happy Story</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>happy-stories/my-story"><i class="fa fa-smile-o"></i>My Happy Stories</a></li>
                            <?php if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ ?>
                                <li><a href="<?=Yii::$app->homeUrl?>happy-stories/permission"><i class="fa fa-list-alt"></i>Stories Approval</a></li>
                                <li><a href="<?=Yii::$app->homeUrl?>mail-content/index"><i class="fa fa-clipboard"></i>Mail Content</a></li>
                            <?php } ?>
                            <li><a href="<?=Yii::$app->homeUrl?>account/edit-account"><i class="fa fa-user-circle-o"></i> Account Info</a></li>
                            <li>
                                <a href="#" >
                                    <?php  echo Html::beginForm(['/site/logout'], 'post')
                                    . Html::submitButton(
                                    '<i class="glyphicon glyphicon-log-out"></i>Logout',
                                    ['class' => 'a-button']
                                    )
                                    . Html::endForm();  ?>
                                </a>
                            </li>	
                        </ul>
                         <?php } ?>
                    </li>
                <?php } else { ?>	
                    <li data-id="signup"><a href="<?=Yii::$app->homeUrl?>site/sign-up#signup">Sign Up</a></li>
                    <li data-id="login"><a href="<?=Yii::$app->homeUrl?>site/login#login">Log In</a></li>
                    <!--<li data-id="edt_home" class="dropdown" class="active">
                        <div class="btn-group pull-right btngroup">
                            <a class="login" href="<?=Yii::$app->homeUrl?>site/login">
                                <button class="btn btn-smp-blue smpl-brdr-left" type="button">Login</button>
                            </a>
                            <a class="join" href="<?=Yii::$app->homeUrl?>site/sign-up">
                                <button class="btn btn-smp-green smpl-brdr-right" type="button">Join Today</button>
                            </a>
                        </div>
                    </li>-->
                <?php }  
                    if(!$detect->isMobile() && !$detect->isTablet()) { ?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span class="glyphicon glyphicon-menu-hamburger" style="margin-top: -13px; margin-left: 20px; font-size: 21px;"></span></a>
                        <ul class="dropdown-menu nav nav-stacked">
                            <li><a href="<?=Yii::$app->homeUrl?>site/index#home"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>site/about#abt"><i class="fa fa-user" aria-hidden="true"></i>About Us</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>wish/top-wishers#top_wishers"><i class="fa fa-gift" aria-hidden="true"></i>Wishers,Granters & Donors</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>happy-stories/index#i_wish"><i class="fa fa-smile-o" aria-hidden="true"></i>Happy Stories</a></li>
                            
                        </ul>
                    </li>
                <?php } else{ ?>
                    <li class="dropdown explore">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Explore
                        <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?=Yii::$app->homeUrl?>site/index#home" class="fnt-gold"><i class="fa fa-home" aria-hidden="true"></i>Home</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>site/about#abt" class="fnt-gold"><i class="fa fa-user" aria-hidden="true"></i>About Us</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>wish/top-wishers#top_wishers" class="fnt-gold"><i class="fa fa-gift" aria-hidden="true"></i>Wishers,Granters & Donors</a></li>
                            <li><a href="<?=Yii::$app->homeUrl?>happy-stories/index#i_wish" class="fnt-gold"><i class="fa fa-smile-o" aria-hidden="true"></i>Happy Stories</a></li>
                            
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<!--***** Header Ends*****-->
<div class="wrapper">
    <div class="container">
        <?=$content?>	
    </div>
</div>
<!--***** Footer Starts*****-->
<div class=" smp-foot">
    <footer class="container">
        <div class="row">
            <div class="col-md-4">
                <p> &copy; SimplyWishes 2017, All Rights Reserved.</p>
            </div>
            <div class="col-md-8">
                <div class="smp-footer-links">
                    <a href="<?=\Yii::$app->homeUrl?>page/view?id=1"><li>Privacy Policy</li></a>
                    <a href="<?=\Yii::$app->homeUrl?>page/view?id=2"><li>Terms Of Use</li></a>
                    <a href="<?=\Yii::$app->homeUrl?>page/view?id=3"><li>Community Guidelines</li></a>
                    <!--<a href="<?=\Yii::$app->homeUrl?>site/about"><li>About Us</li></a>-->
                    <a href="<?=\Yii::$app->homeUrl?>site/contact"><li>Contact Us</li></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 text-center">
                <a href='https://itunes.apple.com/us/app/simplywishes/id1321973376?ls=1&mt=8' target="_blank">
                    <img class="appIcon" alt='Get it on App store' src='<?=Yii::$app->homeUrl?>web/images/icon/appstore.png'/>
                </a>
                <a href='https://play.google.com/store/apps/details?id=com.simplywishes.simplywishes' target="_blank">
                    <img class="appIcon" alt='Get it on Google Play' src='<?=Yii::$app->homeUrl?>web/images/icon/google-play-badge.png'/>
                </a>
            </div>
            <div class="col-md-6">
                <div style="padding-bottom: 5px; text-align: center;">
                    <a style="text-decoration: none;" href="https://www.facebook.com/SimplyWishescom-1121671277927963/" target="_blank">
                            <img class="shareicon-home" style="width:33px" src="<?=Yii::$app->homeUrl?>web/images/icon/facebook.png" alt="Facebook" />
                    </a>	
                    <a style="text-decoration: none;"  href="https://plus.google.com/u/0/105910024848420550192" target="_blank">
                            <img class="shareicon-home" style="width:30px" src="<?=Yii::$app->homeUrl?>web/images/icon/Google-plus.png" alt="Google-plus" />
                    </a>
                    <a style="text-decoration: none;"  href="https://www.instagram.com/simplywishes777/" target="_blank">
                            <img class="shareicon-home" style="width:35px" src="<?=Yii::$app->homeUrl?>web/images/icon/instagram.png" alt="Instagram" />
                    </a>	
                    <a style="text-decoration: none;"  href="https://www.linkedin.com/company/simply-wishes/" target="_blank">
                            <img  class="shareicon-home" style="width:40px" src="<?=Yii::$app->homeUrl?>web/images/icon/Linkedin.png" alt="Linkedin" />
                    </a>
                    <a style="text-decoration: none;"  href="https://www.pinterest.com/simplywishe5244/" target="_blank">
                            <img  class="shareicon-home" style="width:30px" src="<?=Yii::$app->homeUrl?>web/images/icon/Pinterest.png" alt="Pinterest" />
                    </a>	
                    <a style="text-decoration: none;"  href="https://www.reddit.com/user/simplywishes/" target="_blank">
                            <img class="shareicon-home" style="width:30px" src="<?=Yii::$app->homeUrl?>web/images/icon/reddit.png" alt="Reddit" />
                    </a>
                    <a style="text-decoration: none;"  href="https://twitter.com/simply_wishes" target="_blank">
                            <img class="shareicon-home" style="width:40px" src="<?=Yii::$app->homeUrl?>web/images/icon/twitter.png" alt="Twitter" />
                    </a>	
                    <a style="text-decoration: none;"  href="https://www.youtube.com/channel/UC9oY1A49aO1ZQxjdGyJ3Z3Q" target="_blank">
                            <img class="shareicon-home" style="width:30px" src="<?=Yii::$app->homeUrl?>web/images/icon/youtube-icon.png" alt="Youtube" />
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>
<!--***** Footer Ends *****-->
<?php $this->endBody() ?>
</body>
<script>
jQuery(document).ready(function(){
    
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $('.smp-pills li ul li a').hover(function() {
            $(this).find('i').css('color', '#66cc66');
            $(this).css('color', '#66cc66');
        }, function() {
            $(this).find('i').css('color', '#006699');
            $(this).css('color', '#808080');
        });
        
    var hash = window.location.hash;
    hash = hash.replace('#', '');
    
    if($.trim(hash) != "")
    {
        $("li[data-id="+hash+"]").addClass("active");
    }		
});


</script>
</html>
<?php $this->endPage() ?>
