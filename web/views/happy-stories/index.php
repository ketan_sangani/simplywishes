<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Happy Stories';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="editorial-create smp-mg-bottom">
    <div class="row tell-ur-story">
        <div class="col-sm-6"><h3 class="fnt-skyblue">Happy Stories</h3></div>
        <div class="col-sm-6">
            <div class="pull-right newtest" style="margin-bottom : -30px;">
                <a class='btn btn-success' href="<?=Yii::$app->homeUrl?>happy-stories/create">Tell Your Happy Story</a> 
                <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>
    <h4 class='smp_about_slide'>Stories written by our user's whose wishes have been granted.</h4>
    <?php Pjax::begin() ?>
    <div id="gridheight">
	<?php
	
	if(!empty($dataProvider->models))
	{
            foreach($dataProvider->models as $story)
            {
                $class  =   '';	
                    
                if($story->likesCount > 0)
                    $class    =   'likesView';
		$profile = UserProfile::find()->where(['user_id'=>$story->user_id])->one();?>
                    <div class="happystory smp-mg-bottom">
                        <div class="media"> 
                            <div class="media-left"> 
                                <img alt="story" src="<?=Yii::$app->homeUrl?>web/<?= $story->story_image; ?>" class="media-object"   style="width: 200px;border: solid 2px #000;">
                                <span data-id="<?= $story->hs_id?>" class="<?php echo $class ?>"> <?=$story->likesCount?>  <?php echo $story->likesCount > 1 ? 'Loves' : 'Love'; ?></span>
                            </div> 
                            <div class="media-body"> 
                                <h4 class="media-heading"><?= $story->story_title; ?></h4>
                                <a href="<?= Url::to(["account/profile","id"=>$story->user_id]) ?>">Author: <?= $story->author->fullname; ?></a>
                                <p> <?=substr($story->story_text,0,450)?></p>
                                <a href="<?=Yii::$app->homeUrl?>happy-stories/story-details?id=<?= $story->hs_id; ?>" ><h5>Read More</h5></a>
                            </div> 
                        </div>
                    </div>
            <?php
            }
            } else {
                echo "sorry, no Happy Stories yet!";
            }?>
    </div>
    <div>
        <?php
        echo \yii\widgets\LinkPager::widget([
            'pagination'=>$dataProvider->pagination,
        ]);?> 
    </div>
    <?php Pjax::end() ?>
</div>	
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                
                $.ajax({
                    url : '<?=Url::to(['happy-stories/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
	
</script>