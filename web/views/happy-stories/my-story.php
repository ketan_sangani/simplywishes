<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\UserProfile;

/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$this->title = 'Happy Wish Stories';
$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php echo $this->render('@app/views/account/_profilenew',['user'=>$user,'profile'=>$profile])?>

<div class="col-md-8" id="scroll-evt">
        <div class="row tell-ur-story">
            <div class="col-sm-6"><h3 class="fnt-skyblue">My Happy Stories</h3></div>
            <div class="col-sm-6">
                <div class="pull-right">
                    <a class='btn btn-success' href="<?=Yii::$app->homeUrl?>happy-stories/create">Tell Your Happy Story</a>
                </div>
            </div>
        </div>
	
	<?php if (Yii::$app->session->hasFlash('success_happystory')): ?>
            <div class="alert alert-success" style="margin-top: 20px;  width: 80%;">
                    Your Story has been Updated Successfully.	
            </div>
	<?php endif; ?>	
	<?php if (Yii::$app->session->hasFlash('success_adminhappystory')): ?>
            <div class="alert alert-success" style="margin-top: 20px;  width: 80%;">
                    Your Story has been Created and is waiting for Site Approval.
            </div>
	<?php endif; ?>	
        <?php Pjax::begin() ?>
	<div class="grid" data-masonry='{ "itemSelector": ".grid-item" }'>
		
		<?php
		
        if(isset($dataProvider->models) && !empty($dataProvider->models))
        {		
            foreach($dataProvider->models as $story)
            {
                $class  =   '';
                $profile = UserProfile::find()->where(['user_id'=>$story->user_id])->one();	

                if($story->likesCount > 0)
                    $class    =   'likesView';
                ?>
				
                <div class="col-md-12 happystory smp-mg-bottom">
                    <div class="media"> 
                        <div class="media-left"> 
                                <img alt="story" src="<?=Yii::$app->homeUrl?>web/<?= $story->story_image; ?>" class="media-object"   style="width: 200px;border: solid 2px #000;">
                                <span data-id="<?= $story->hs_id?>" class="<?php echo $class ?>">  <?=$story->likesCount?>  <?php echo $story->likesCount > 1 ? 'Loves' : 'Love'; ?> </span>
                        </div> 
                        <div class="media-body">
                            <h4 class="media-heading"><?php echo $story->story_title; ?></h4>

                            <a href="<?= Url::to(["account/profile","id"=>$story->user_id]) ?>">Author: <?= $story->author->fullname; ?></a>
                            <p> <?=substr($story->story_text,0,450)?></p>
                            <a href="<?=Yii::$app->homeUrl?>happy-stories/story-details?id=<?= $story->hs_id; ?>" ><h5>Read More</h5></a>
                            <?php if(\Yii::$app->user->id == $story->user_id){ ?>
                                <div>
                                        <?= Html::a('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Update', ['update', 'id' => $story->hs_id], ['class' => 'btn btn-warning']) ?>


                                </div> 
                            <?php } ?>
                        </div> 
                    </div>
                </div>

            <?php
            }
        } else {
                echo "Write any interesting story about your wishes that has been granted.";
        }	
        ?>

    </div>
<div>
<?php
    echo \yii\widgets\LinkPager::widget([
        'pagination'=>$dataProvider->pagination,
    ]);?> 
</div>
<?php Pjax::end() ?>
</div>
<div style="display:none" align="center" id="loader_img" ><img src="<?= Yii::$app->homeUrl?>web/images/loaders/loading.gif"></div>
	
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
	$(document).on('click', '.deletecheck', function(){ 
		var checkmsg = confirm("Are Sure To Delete this Happy Story ?");	
		if(checkmsg == false)
		{
			return false;
		}	
		var id = $(this).attr('for');
		$.ajax({
			url : '<?=Url::to(['happy-stories/delete'])?>',
			type: 'POST',
			data: { id:id },
			success:function(data){
				location.reload();
			}
		});
	});
  $(window).on('load', function () {
    // Handler for .ready() called.
    $('html, body').animate({
        scrollTop: $('#scroll-evt').offset().top - 100
    }, 'slow');
});
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                
                $.ajax({
                    url : '<?=Url::to(['happy-stories/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
	
</script>	