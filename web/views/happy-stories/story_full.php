<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserProfile;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Editorial */

$profile = UserProfile::find()->where(['user_id'=>$model->user_id])->one();

if(\Yii::$app->user->id == $model->user_id)	
	$this->title = 'My Happy Story';
else	
	$this->title = ucfirst($profile->firstname)."'s".' Happy Story ';

$this->params['breadcrumbs'][] = ['label' => 'Editorials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

\Yii::$app->view->registerMetaTag([
            'name' => 'og:title',
            'property' => 'og:title',
            'content' =>$model->story_title
]);
\Yii::$app->view->registerMetaTag([
        'name' => 'og:description',
        'property' => 'og:description',
        'content' =>$model->story_text
]);
\Yii::$app->view->registerMetaTag([
        'name' => 'og:image',
        'property' => 'og:image',
        'content' =>Url::to(['web/'.$model->story_image],true)
]);

$class  =   '';	

if($model->likesCount > 0)
    $class    =   'likesView';
?>

<div class="wish-view">
    <div class="container my-profile">
	<div class="col-md-12 smp-mg-bottom">
            <?php if (Yii::$app->session->hasFlash('success_happystory')): ?>
            <div class="alert alert-success" style="margin-top: 20px;">
                Your Story has been Updated Successfully.	
            </div>
            <?php endif; ?>	
            <h3 class="smp-mg-bottom fnt-skyblue"><?=$this->title?></h3>
	
            <div class="col-md-3 happystory sharefull-list">
                <img src="<?=Yii::$app->homeUrl?>web/<?php echo $model->story_image; ?>"   class="img-responsive" alt="my-profile-Image"><br>
                <p><span data-id="<?= $model->hs_id?>" class="<?php echo $class ?>"> <span id="likecmt_<?= $model->hs_id ?>">  <?=$model->likesCount?> </span> <?php echo $model->likesCount > 1 ? 'Loves' : 'Love'; ?></span> &nbsp;
                 
                    <?php

                      if(!$model->isLiked(\Yii::$app->user->id))
                            echo  '<span title="Like it" data-w_id="'.$model->hs_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
                      else
                            echo  '<span title="You liked it" data-w_id="'.$model->hs_id.'" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';
                    ?>
                    <!--<i class="fa fa-save txt-smp-orange"></i> &nbsp;
                    <i class="fa fa-thumbs-o-up txt-smp-green"></i>--> 

                    <span title="Share it" data-placement="top"  data-popover-content=""><img data-placement="top" class="listesinside"  src="<?= Yii::$app->homeUrl ?>web/images/Share-Icon.png"  /></span>					
                    <div class="shareIcons hide" data_text="<?= $model->story_title ?>" data_url="<?= Url::to(['happy-stories/story-details','id'=>$model->hs_id],true)?>" ></div>

                </p>
                    <!--<div class="shareIcons" data_text="Happy Story" data_url="<?= Url::to(['happy-stories/story-details','id'=>$model->hs_id],true)?>" ></div>-->
            </div>
            <div class="col-md-8">
                <h4 class="media-heading"><?= $model->story_title; ?></h4>
                <p><?php echo Html::a('Author: '.$profile->fullname, ['account/profile', 'id' => $model->user_id])?> <?php echo $model->story_text; ?> </p>
                <?php if(\Yii::$app->user->id == $model->user_id){ ?>
                     <?= Html::a('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i> Update', ['update', 'id' => $model->hs_id], ['class' => 'btn btn-warning ']) ?>
                     <?= Html::a('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Delete', ['delete', 'id' => $model->hs_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                        ]
                    ]) ?>

                      <!--<button class="btn btn-danger deletecheck" for="<?= $model->hs_id ?>" ><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Delete </button>-->		 
                <?php } ?>
                <a class="btn btn-success" id="back">Back</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="likeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content" style="width: 80%; margin: auto;">
      <div class="modal-header">
        <a href="#" class="closeModal pull-right">&times;</a>
      </div>
      <div class="modal-body">
          
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" >

$(document).ready(function(){
        $('#back').on('click', function(){
            window.location.href =   document.referrer;
        });
	$(".shareIcons").each(function(){
		var elem = $(this);
		elem.jsSocials({
                    showLabel: false,
                    showCount: true,
                    shares: ["facebook","googleplus", "linkedin",
                    {
                            share: "twitter",           // name of share
                            via: "simply_wishes",       // custom twitter sharing param 'via' (optional)
                            hashtags: "simplywishes,dream_come_true"   // custom twitter sharing param 'hashtags' (optional)
                    }],
                    url : elem.attr("data_url"),
                    text: elem.attr("data_text")
		});
	});
var Wish    =   {
    ajax    :   false
};
$(document).on('click', '.likesView', function(){
            var val =   $(this).find('span').html();
            if(val <= 0)
                return false;
            
            var wish_id = $(this).data('id');
            if($.trim(wish_id) !== "" )
            {
                if(Wish.ajax)
                    return false;
                
                Wish.ajax   =   true;
                
                $.ajax({
                    url : '<?=Url::to(['happy-stories/likes-view'])?>',
                    type: 'GET',
                    data: {w_id:wish_id},
                    success:function(data){
                        if(data)
                        {
                            var tpl =   '';
                            var items   =   JSON.parse(data);
                            $.each(items, function(k,v){
                               tpl  +=   '<div class="likeBlock"><a href="<?=\Yii::$app->homeUrl?>account/profile?id='+v.user_id+'"><img src="<?=\Yii::$app->homeUrl?>web/uploads/users/'+v.image+'" width="50px"><span>'+v.name+'</span></a></div>';
                            });
                            
                            $('.modal-body').append(tpl);
                            $('#likeModal').modal({backdrop: 'static', keyboard: false});
                            $('#likeModal').modal('show');
                            
                            $('.closeModal').on('click', function(e){
                                e.preventDefault();
                                $('.modal-body').html('');
                                $('#likeModal').modal('hide');
                            });
                        }
                    },
                    complete: function(){
                        Wish.ajax   =   false;
                    }
                });
            }
        });
	
	$(document).on('click', '.like-wish', function(){ 
		var s_id = $(this).attr("data-w_id");
		var type = $(this).attr("data-a_type");
		var elem = $(this);
		$.ajax({
			url : '<?=Url::to(['happy-stories/like'])?>',
			type: 'GET',
			data: {s_id:s_id,type:type},
			success:function(data){
				if(data == "added"){
					if(type=="fav"){
						elem.removeClass("txt-smp-orange");
						elem.addClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#B23535');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) + parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
					}
				}
				if(data == "removed"){
					if(type=="fav"){
						elem.addClass("txt-smp-orange");
						elem.removeClass("txt-smp-blue");
					}
					if(type=="like"){
						elem.css('color', '#fff');
						var likecmt = $("#likecmt_"+s_id).text();
						likecmt = parseInt(likecmt) - parseInt(1);
						$("#likecmt_"+s_id).text(likecmt);
					}
				}
			}
		});
	});
	
	$(document).on('click', '.deletecheck', function(){ 
		var checkmsg = confirm("Are Sure To Delete this Happy Story ?");	
		if(checkmsg == false)
		{
			return false;
		}
		
		var id = $(this).attr('for');
		$.ajax({
			url : '<?=Url::to(['happy-stories/delete'])?>',
			type: 'POST',
			data: { id:id },
			success:function(data){
				window.location.href = "<?= Url::to('my-story')?>"
			}
		});
		
	}); 
	
	
});

 
	
	/*		
	$(document).on('click', '.deletecheck', function(){ 
		if(confirm("Are Sure To Delete this Happy Story ?"))	
		else
			return false;		
	});
	
	
	 */
	
</script>

<script>
var isVisible = false;
var clickedAway = false;

$(document).ready(function(){	
	$(function(){
		$('.listesinside').popover({   
			html: true,
			content: function () {
				var clone = $($(this).parents(".sharefull-list").find(".shareIcons")).clone(true).removeClass('hide');
				return clone;
			}
		}).click(function(e) {
			e.preventDefault();
			clickedAway = false;
			isVisible = true;
		});
	});
	
/* 	$(document).on('click','.jssocials-shares',function(){
		 $('.listesinside').popover('hide');
	}); */
	
$(document).click(function (e) {
    if (isVisible & clickedAway) {
        $('.listesinside').popover('hide');
        isVisible = clickedAway = false;
    } else {
        clickedAway = true;
    }
});

$('body').on('hidden.bs.popover', function (e) {
    $(e.target).data("bs.popover").inState.click = false;
});

});
</script>