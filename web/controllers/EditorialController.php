<?php

namespace app\controllers;

use Yii;
use app\models\Editorial;
use app\models\EditorialComments;
use app\models\search\SearchEditorial;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use Embed\Embed;
use app\models\User;
use app\models\UserProfile;
use app\models\EditorialActivity;
use app\models\EditorialCommentActivity;

/**
 * EditorialController implements the CRUD actions for Editorial model.
 */
class EditorialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Editorial models.
     * @return mixed
     */
    public function actionIndex()
    {
        if(Yii::$app->user->identity == NULL || Yii::$app->user->identity->role !== 'admin')
            return $this->goHome();
            
        $searchModel = new SearchEditorial();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch()
    {       
        $searchModel = new SearchEditorial();
        $dataProviderA = $searchModel->searchArticle(Yii::$app->request->queryParams);
        $dataProviderV = $searchModel->searchVideo(Yii::$app->request->queryParams);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        return $this->render('editorial_page', [
            'searchModel' => $searchModel,
            'modelArticles' => $dataProviderA->models,
            'modelVideos' => $dataProviderV->models,
            'paginationVideo' => $dataProviderV->pagination,
            'paginationArticle' => $dataProviderA->pagination,
            'user' => $user,
            'profile' => $profile
        ]);
    }

    /**
     * Displays a single Editorial model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Editorial model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Editorial();
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        if ($model->load(Yii::$app->request->post())) {
            
            if($model->featured_video_url)
            {
                try
                {
                    if ( ! filter_var($model->featured_video_url, FILTER_VALIDATE_URL))
                        throw new \Exception("Invalid Url");
                    
                    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $model->featured_video_url, $match);
                                    
                    if( ! isset($match[1]))
                        throw new \Exception("Invalid Url");
                    
                    $info = Embed::create($model->featured_video_url);				
                    //var_dump($info);exit;
                    //echo preg_match_all('/src="([\s\S]*?)"/', $info->code,$src[], PREG_SET_ORDER);die;
                    if($info && $info->code){			
                        $xpath = new \DOMXPath(@\DOMDocument::loadHTML($info->code));
                        $src = $xpath->evaluate("string(//iframe/@src)");
                        if(!$src)
                            throw new \Exception("Invalid Url");
                    }
                }
                catch (\Exception $e) {
                    return $this->render('create',[
                        'model' => $model, 'user'  =>  $user,
                        'profile' => $profile,
                        'error' => 1,
                        'msg' => $e->getMessage()
                    ]);
                }
            }
        
            $model->e_image = UploadedFile::getInstance($model, 'e_image');
            if(!empty($model->e_image)) {
                if(!$model->uploadImage())
                    return;
            }
            $model->created_by = \Yii::$app->user->id;
            
            if( ! $model->is_video_only)
                $class  =   'article';
            else
                $class  =   'video';
            
            if($model->save())
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            else 
                return $this->render('create', [ 'model' => $model, 'user'  =>  $user,
                    'profile' => $profile, 'error' => 0, 'msg' => 0]);
					
        } else {
            return $this->render('create', [
                'model' => $model,
                'user'  =>  $user,
                'profile' => $profile,
                'error' => 0, 'msg' => 0
            ]);
        }
    }

    /**
     * Updates an existing Editorial model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        if(Yii::$app->user->identity == NULL || Yii::$app->user->identity->role !== 'admin')
            return $this->goHome();
        
        $model = $this->findModel($id);
        $current_image = $model->e_image;
        $model->scenario = 'update_by_editorial_admin';
        
        if ($model->load(Yii::$app->request->post())){
            /**		Image Uploaded for Update function Line 
            **/		
            $model->e_image = UploadedFile::getInstance($model, 'e_image');										
            if(!empty($model->e_image)){ 
                if(!$model->uploadImage())
                    return;
            }else
                $model->e_image = $current_image;

            $model->updated_by = \Yii::$app->user->id;
            $model->updated_at = date('Y-m-d H:i:s');

            if($model->save())
            {				
                return $this->redirect(['view', 'id' => $model->e_id]);
            } else {
                return $this->render('update', ['model' => $model]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Editorial model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        if(Yii::$app->user->identity == NULL || Yii::$app->user->identity->role !== 'admin')
            return $this->goHome();
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Editorial model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Editorial the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Editorial::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionEditorial()
    {
        $searchModel = new SearchEditorial();
        $modelVideo     =   Editorial::find()->where(['is_video_only' => 1, 'status'=>0])->orderBy('e_id Desc');
        $modelArticle   =   Editorial::find()->where(['is_video_only' => 0, 'status'=>0])->orderBy('e_id Desc');
        $paginationVideo = new Pagination(['totalCount' => $modelVideo->count(), 'pageSize'=>10, 'pageParam' => 'video-page']);
        $paginationArticle = new Pagination(['totalCount' => $modelArticle->count(), 'pageSize'=>10,  'pageParam' => 'article-page']);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        $modelVideos = $modelVideo->offset($paginationVideo->offset)
            ->limit($paginationVideo->limit)
            ->all();
        $modelArticles = $modelArticle->offset($paginationArticle->offset)
            ->limit($paginationArticle->limit)
            ->all();
        
        return $this->render('editorial_page', [
            'modelArticles' => $modelArticles,
            'modelVideos' => $modelVideos,
            'searchModel' => $searchModel,
            'paginationVideo' => $paginationVideo,
            'user'  =>  $user,
            'profile' => $profile,
            'paginationArticle' => $paginationArticle
        ]);
    }
	
    public function actionEditorialPage($id, $class)
    {
        if( ! $id || ! $class || ! in_array($class, ['video', 'article']))
            return $this->goHome();
            
        $model = Editorial::findOne(['e_id' => $id, 'status' => 0]);
        if( ! $model)
            return $this->goHome();
        
        $listcomments = new EditorialComments();
        $comments = $listcomments->find()->where(['e_id'=>$id,'parent_id'=>0, 'status'=>0])->orderBy('e_comment_id Desc')->all();		 
        
        return $this->render('editorial_comments', [
            'model' =>$model,'comments'=>$comments,'listcomments'=>$listcomments, 'class' => $class
        ]); 
    }
	
    public function actionEditorialComments($class)
    {
        $model = new EditorialComments();
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }
        }
    }
    
    public function actionUpdateComment($id, $class)
    {
        $model = EditorialComments::findOne($id);
        
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
            }
        }
    }
    
    public function actionDeleteComment($id, $class)
    {
        $model  =   EditorialComments::findOne($id);
        $model->status  =   1;
        
        if($model->parent_id == 0)
        {
            $replycomments = EditorialComments::find()->where(['parent_id'=>$id])->all();
            
            foreach($replycomments as $reply)
            {
                $reply->status  =   1;
                $reply->save();
            }
        }
        
        $model->save();
        return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
    }
	
	public function actionCommentreply($class)
        {		
            $model = new EditorialComments();
            if($model->load(Yii::$app->request->post()))
            {				
                if(\Yii::$app->user->isGuest){			
                    Yii::$app->session->setFlash('login_to_comment');
                    return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
                }
                $model->user_id = \Yii::$app->user->id;
                if($model->save())
                {
                    return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
                }else{
                    Yii::$app->session->setFlash('error_comments');
                    return $this->redirect(['editorial/editorial-page?id='.$model->e_id.'&class='.$class]);
                } 
            }
        }
        
        public function actionLike($s_id,$type)
	{
		if(\Yii::$app->user->isGuest)
			return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
		$story = $this->findModel($s_id);
		$activity = EditorialActivity::find()->where(['e_id'=>$story->e_id,'activity'=>$type,'user_id'=>\Yii::$app->user->id])->one();
		if($activity != null){
			$activity->delete();
			return "removed";
		}
                $activity = new EditorialActivity();
		$activity->e_id = $story->e_id;
		$activity->activity = $type;
		$activity->user_id = \Yii::$app->user->id;
		if($activity->save())
			return "added";
		else return false;
	}
        
        public function actionLikesView($w_id)
        {
            $story = $this->findModel($w_id);
            $likedUser  =   [];
            if( ! empty($story->likes))
            {
                foreach($story->likes as $key => $like)
                {
                    $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();

                    $likedUser[$key]['user_id'] =   $profile->user_id;
                    $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                    $likedUser[$key]['image'] =   $profile->profile_image;
                }
            }

            return json_encode($likedUser);
        }
        
        public function actionCommentLikesView($w_id)
        {
            $comment = EditorialComments::find()->where(['e_comment_id'=>$w_id])->one();
            $likedUser  =   [];
            if( ! empty($comment->likes))
            {
                foreach($comment->likes as $key => $like)
                {
                    $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();

                    $likedUser[$key]['user_id'] =   $profile->user_id;
                    $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                    $likedUser[$key]['image'] =   $profile->profile_image;
                }
            }

            return json_encode($likedUser);
        }
	
        public function actionLikeComment($s_id)
	{
            if(\Yii::$app->user->isGuest)
                return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

            $comment = EditorialComments::find()->where(['e_comment_id'=>$s_id])->one();
            $activity = EditorialCommentActivity::find()->where(['e_comment_id'=>$comment->e_comment_id,'user_id'=>\Yii::$app->user->id])->one();
            if($activity != null){
                $activity->delete();
                return "removed";
            }
            $activity = new EditorialCommentActivity();
            $activity->e_comment_id = $comment->e_comment_id;
            $activity->user_id = \Yii::$app->user->id;

            if($activity->save())
                return "added";
            else return false;
	}
	
    public function actionEmbed($url)
    {
        try
        {
            if(Yii::$app->user->identity == NULL)
                return $this->goHome();

            //Load any url:
            $info = Embed::create($url);				
            //var_dump($info);exit;
            //echo preg_match_all('/src="([\s\S]*?)"/', $info->code,$src[], PREG_SET_ORDER);die;
            if($info && $info->code){			
                $xpath = new \DOMXPath(@\DOMDocument::loadHTML($info->code));
                $src = $xpath->evaluate("string(//iframe/@src)");	
                if(!$src)
                    return json_encode(['error' => 0, 'msg' => $url]);			
            }
            if (preg_match('/youtube.com/',$info->code))
                    return json_encode(['error' => 0, 'msg' => $src."&rel=0"]);
            else
                    return json_encode(['error' => 0, 'msg' => $src]);
        }
        catch (\Exception $e) {
            return json_encode(['error' => 1, 'msg' => $e->getMessage()]);
        }
    }
	
	
	public function actionUpload(){
            if(Yii::$app->user->identity == NULL)
                return $this->goHome();
            
		//$name = uniqid();
		$name = preg_replace( 
                     array("/\s+/", "/[^-\.\w]+/"), 
                     array("_", ""), 
                     trim($_FILES["media"]["name"])); 
		$dir = "web/uploads/media/";
		move_uploaded_file($_FILES["media"]["tmp_name"], $dir.$name);
		return $url = Yii::$app->urlManager->createAbsoluteUrl([$dir.$name]);
	}
	
}
