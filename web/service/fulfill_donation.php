<?php 

$status=1;
include "conn.php";
// include "function.php";
global $error_msgs;
if(isset($_REQUEST['uid']) && isset($_REQUEST['token']) && isset($_REQUEST['donation_id']))
{
	global $base_url_image;
	global $base_url;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	$donation_id=$db->real_escape_string($_REQUEST['donation_id']);
	
	if(validate_token($token,$uid)==0)
	// if(1==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		  
	}else{
			
		$updateSql = "update donations set granted_by='$uid',granted_date='".date('m-d-Y')."' where id='$donation_id' and process_status='1' and process_granted_by is not NULL and created_by='$uid'";
		// die($updateSql);
		$result = $db->query($updateSql);

		if($db->affected_rows < 1 ){
			$msg = "Error fulfilling the donationn";
			$status=0;
			return;	
		}

		$donation_result=$db->query("select * from donations where id='$donation_id' LIMIT 1");
		$row_donation=$donation_result->fetch_object();

		$sql_uid="select username,email from user where id='$uid' LIMIT 1";
		$sql_donation_user="select username,email from user where id='$row_donation->created_by' LIMIT 1";

		$result_uid=$db->query($sql_uid);
		$result_donation_user=$db->query($sql_donation_user);

		$row_uid=$result_uid->fetch_object();
		$row_donation_user=$result_donation_user->fetch_object();
	
		$result_email_content=$db->query("select * from mail_content where m_id='24' LIMIT 1");
		$row_email_content=$result_email_content->fetch_assoc();

		$loginlink= $base_url."/site/login";
		$donationlink= $base_url."/donation/view?id=".$donation_id;
		$happylink=$base_url."/happy-stories/my-story";
		$editmessage=$row_email_content['mail_message'];
		$editmessage = str_replace("##USERNAME##", nl2br($row_uid->username), $editmessage);		
		$editmessage = str_replace("##HAPPYSTORY##", "<a href='$happylink'>Click here</a>", $editmessage);	
		$editmessage = str_replace("##DONATIONLINK##", "<a href='$donationlink'>".nl2br($row_donation->title)."</a>", $editmessage);	
		
		$editmessage = str_replace("##LOGINLINK##", "<a href='$loginlink'>$loginlink</a>", $editmessage);
		$msg = "You fulfilled this donation successfully.";

		// send_email('alexcapitaneanu@gmail.com',$editmessage,$row_email_content['mail_subject']);
		send_email($row_uid->email,$editmessage,$row_email_content['mail_subject']); 
	}
					
}
?>
