<?php 

define("GOOGLE_API_KEY", "AIzaSyCsXaExP5FIix5kinj8RFNTHSBY-yHkUbc");
define("DEVELOPER_MODE", "false");

	function sendAndroidPushNotification($registatoin_ids, $message) 	
	{        
		//$url = 'https://android.googleapis.com/gcm/send';
		$url = 'https://fcm.googleapis.com/fcm/send';

		
		/*if(!isset($message['data']['badge_count']))
		{
			$message['data']['badge_count']=0;
		}*/

		$fields = array			
			(				
				'registration_ids' => $registatoin_ids,				
				'data' => $message,
				'priority' => "high"
			);
			
        $headers = array			
			(				
				'Authorization: key=' . GOOGLE_API_KEY,				
				'Content-Type: application/json'			
			);
			
        // Open connection        
		$ch = curl_init();        
		// Set the url, number of POST vars, POST data        
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);        
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);       
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly		
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);        
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
		// Execute post        
		$result = curl_exec($ch);
		
		//echo "result ---->".$result;		
		$result1 = json_decode($result);
		
		// Close connection        
		curl_close($ch);
		
		return $result;
	}

	function sendApplePushNotification($deviceToken, $message,$bodydata="")	
	{
		$ctx = stream_context_create();
		
		if(DEVELOPER_MODE=="true")
		{
			//echo "IF";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		else
		{
			//echo "ELSE";

			// stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates_live.pem');
			stream_context_set_option($ctx, 'ssl', 'local_cert', 'apnsCertificate.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', '1qazxsw2');
			$fp = stream_socket_client( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		
		if (!$fp)
			exit("Failed To Connect: $err - $errstr" . PHP_EOL);
		$badge=0;
		if(isset($bodydata['badge_count']))
		{
			$badge=$bodydata['badge_count'];
		}

		$body['aps'] = array('alert' => $message,'sound' => 'default','badge'=>$badge,'content-available'=>1);
			$body['data']=$bodydata;
		
		$payload = json_encode($body);//,JSON_UNESCAPED_UNICODE

		$payload = str_replace("\u", "u", $payload);
		
		//$payload = array_map('htmlentities', $body);
		//$payload = json_encode($body);
		//$payload = html_entity_decode($payload);
		//$payload = json_encode($body,JSON_UNESCAPED_UNICODE);		
		//$payload = str_replace("/\\u", "u", $payload);
		//$payload = preg_replace_callback('/\\\\u([0-9a-f]{4})/i', function($matches) {return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16');}, $payload);

		// echo $deviceToken;
		$packToken = pack('H*', str_replace(' ', '', sprintf('%u', CRC32($deviceToken))));
		// $packToken = pack('H*', $deviceToken);

		$msg = chr(0) . pack('n', 32) . $packToken . pack('n', strlen($payload)) . $payload;
		
		$result = fwrite($fp, $msg, strlen($msg));
		
		//echo $result."<br>";
		
		$res_val=0;			
		if (!$result)			
			$res_val=0;
		else			
			$res_val=1;	
			
		fclose($fp);		
		
		return $res_val;
	}

	function sendApplePushNotification_arr($deviceToken, $message)	
	{
		$ctx = stream_context_create();
		
		if(DEVELOPER_MODE=="true")
		{
			//echo "IF";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		else
		{
			//echo "ELSE";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'CertificatesLive.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		
		if (!$fp)
			exit("Failed To Connect: $err - $errstr" . PHP_EOL);
		
		foreach($deviceToken as $device_id) 
		{

			$body['aps'] = array('alert' => $message,'sound' => 'default','badge'=>1,'content-available'=>1);
			$body['data']=array();
			
			$payload = json_encode($body);		
			
			$msg = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;
			
			$result = fwrite($fp, $msg, strlen($msg));
		}
		
		$res_val=0;			
		if (!$result)			
			$res_val=0;
		else			
			$res_val=1;	
			
		fclose($fp);		
		
		return $res_val;	
	}



function get_access_token($id)
{
	global $db;
	$token=bin2hex(openssl_random_pseudo_bytes(64));
	$db->query("update user set login_token='$token' where id='$id' ");
	return $token;
}// get access token

function send_email($to,$msg,$subject)
{
	include 'smtp/class.phpmailer.php';
  $mail = new PHPMailer;
  $mail->IsSMTP(); // telling the class to use SMTP
  $mail->Host = "smtp.gmail.com"; // SMTP server
  //$mail->SMTPDebug = 2;
  $mail->SMTPAuth = true; // turn on SMTP authentication
  $mail->Username = "simplywishesn@gmail.com"; // SMTP username
  $mail->Password = "tguhwslzqrebziuk"; //
  $mail->IsHTML(true);
  $mail->SMTPSecure = 'tls';//tls
  $mail->SetFrom('simplywishesn@gmail.com', 'SimplyWishes');
  $mail->AddReplyTo("simplywishesn@gmail.com","SimplyWishes");
  $mail->Subject = $subject;
  $mail->MsgHTML($msg);
  //$mail->AddAddress('nilesh@smart-webtech.com');
  $mail->AddAddress($to);
 if (!$mail->Send()) {
    /* Error */
  //  echo 'Message not Sent! ';
  } else {
    /* Success */
 //  echo 'Sent Successfully! <b> Check your Mail</b>';
  }
}

function validate_token($token,$uid)
{
	global $db;
	$sql = "select id from user where id='$uid' and login_token='$token' LIMIT 1";
	// die($sql);
	$result=$db->query($sql);
	if($result->num_rows>0)
		return 1;
	else
		return 0;
}

function check_block_content($content_type,$id)
{
	global $db;
	$result=$db->query("select blocked from report_content where report_type='$content_type' AND content_id='$id' AND blocked='1' ");
	if($result->num_rows>0)
	{
		return 1;
	}else{
		return 0;
	}
} 

function check_reported_content($report_type, $content_id, $report_user, $reported_user)
{
	global $db;

	$queryReported = "select * from report_content where content_id='".$content_id."' and report_type='".$report_type."' and report_user = '".$report_user."' and reported_user='".$reported_user."' ";
// echo $queryReported;
	$result=$db->query($queryReported);
	if ($result){
		if($result->num_rows>0)
		{
			return 1;
		}else{
			return 0;
		}
	} else {
		return 0;
	}
}

function stripTags($field){
	$field = strip_tags($field);
	$field = str_ireplace("\r",'',$field);
	$field = str_ireplace("\n",'',$field);
	return $field;
	// die($field);
}

function get_unread_msg_total($uid)
{
	global $db;
	$result=$db->query("select m_id from messages where recipient_id='$uid' AND read_text='0' AND delete_status!='1'");
	return $result->num_rows;
}


function get_user_info($uid)
{
	global $db;

    $getProperties = "select max_no_wishes, max_no_donations from properties";
    $resultProperties=$db->query($getProperties);
    $rowProperties = $resultProperties->fetch_assoc();
    $maxAllowedWishes = $rowProperties['max_no_wishes'];
    $maxAllowedDonations = $rowProperties['max_no_donations'];
    $userTotalWishes = get_user_wishes_total($uid);
    $userTotalDonations = get_user_donations_total($uid);
    $canCreateDonations = false;
    $canCreateWishes = false;
    if ($userTotalWishes < $maxAllowedWishes) {
        $canCreateWishes = true;
	}
	if ($userTotalDonations < $maxAllowedDonations) {
        $canCreateDonations = true;
    }

    $data['max_allowed_wishes'] = $maxAllowedWishes;
    $data['user_total_wishes'] = $userTotalWishes;
	$data['can_create_wishes'] = $canCreateWishes;

	$data['max_allowed_donations'] = $maxAllowedDonations;
	$data['user_total_donations'] = $userTotalDonations;
	$data['can_create_donations'] = $canCreateDonations;

	$userInfoSql = "select city,coountry,state from user_profile where user_id = $uid";
	
	return $data;
}

function get_user_wishes_total($uid)
{
	global $db;
	$result=$db->query("select w_id from wishes where wished_by='$uid' and wish_status = 0 and granted_by is null ");
	return $result->num_rows;
}

function get_user_donations_total($uid)
{
	global $db;
	$result=$db->query("select id from donations where created_by='$uid' and status = 0 and granted_by is null ");
	return $result->num_rows;
}


function get_unread_friend_msg_total($uid,$friend_id)
{
	global $db;
	$result=$db->query("select m_id from messages where sender_id='$friend_id' AND recipient_id='$uid' AND read_text='0' AND delete_status!='1' ");
	return $result->num_rows;
}

function get_user_data($uid)
{
	global $db;
	global $base_url_image;
	global $base_url_storyimage;
	$data=array();
	$result_user=$db->query("select username,email,id from user where id='$uid'")or die($db->error);
	if($result_user->num_rows>0)
	{	
		$row_user=$result_user->fetch_assoc();
		$data['user']=$row_user;

		$result_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='$uid'")or die($db->error);
		$row_user_profile=$result_profile->fetch_assoc();
		$row_user_profile['profile_image_name']=$row_user_profile['profile_image'];
		$row_user_profile['profile_image']=$base_url_image.$row_user_profile['profile_image'];
		
		$data['user_profile']=$row_user_profile;

		$arr_story=array();
		$result_happy_story=$db->query("select hs_id,user_id,story_text,story_image,status,created_at from happy_stories where user_id='$uid'")or die($db->error);
		if($result_happy_story->num_rows>0)
		{
			while($row_happy_story=$result_happy_story->fetch_assoc())
			{
				$row_happy_story['story_image']=$base_url_storyimage.$row_happy_story['story_image'];
				$result_likes=$db->query("select count(a_id) as likes from story_activities where story_id='".$row_happy_story['hs_id']."'");
				$row_happy_story['likes']=0;
				if($result_likes->num_rows>0)
				{
					$row_likes=$result_likes->fetch_assoc();
					$row_happy_story['likes']=$row_likes['likes'];
				}
				array_push($arr_story, $row_happy_story);
			}
		}
		$data['my_happy_story']=$arr_story;

		$arr_followers=array();
		$result_follower=$db->query("select requested_by as uid from follow_request where requested_to='$uid' AND status='0'")or die($db->error);
		if($result_follower->num_rows>0)
		{

			while($row_follower=$result_follower->fetch_assoc())
			{
				$result_folower_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='".$row_follower['uid']."' LIMIT 1")or die($db->error);
				$row_folower_profile=$result_folower_profile->fetch_assoc();
				$row_folower_profile['profile_image']=$base_url_image.$row_folower_profile['profile_image'];
				array_push($arr_followers, $row_folower_profile);
			}
		}
		$data['follower']=$arr_followers;

		$arr_friends=array();
		$result_friends=$db->query("select requested_to as uid from follow_request where requested_by='$uid' AND status='0'")or die($db->error);
		if($result_friends->num_rows>0)
		{
				while ($row_friend=$result_friends->fetch_assoc()) {
						$result_friend_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='".$row_friend['uid']."' LIMIT 1")or die($db->error);
						$result_friend_profile=$result_friend_profile->fetch_assoc();
						$result_friend_profile['profile_image']=$base_url_image.$result_friend_profile['profile_image'];
						array_push($arr_friends, $result_friend_profile);		
				}
		}
		$data['friends']=$arr_friends;		



	}// if condition
	return $data;

}

function getDonationList($uid, $fq = 0, $search_text = "", $filter = "", $limit = 10, $start = 0, $type){
	
	global $base_url;
	global $paypal_url;
	global $base_url_image;
	global $error_msgs;
	global $select_reported;
	global $db;

	$user_id = $uid;
	$friend_query="";
	$sql="";
	$search_text_q="";
	$typeConditon = "";
	// $friend_query = " 1 = 1 ";

	switch($type) {
		case 'list':
			$typeConditon = " ";
		break;
		case 'my_list':
			$typeConditon = " AND created_by = '$user_id' ";
		break;
		case 'friends':
			$typeConditon = " AND d.created_by='$fq' ";
		break;
		default:
			$typeConditon = " ";
	break;
	}

	$uid=" AND user_id='$user_id'";
	$exclude_reported = " AND d.created_by not in ($select_reported $user_id) ";

	if ($type != "friends"){
		if($fq==1){
			$friend_query=" d.created_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) ";
		}
	}


	$search_text_q = $exclude_reported; 

	if($search_text != "")
	{
		$search_text_q.=" AND d.title LIKE '%$search_text%' ";
	}

	if($filter != "")
	{
		if ($filter != 'saved'){
			$search_text_q.= $typeConditon;
		}

		if($filter=="popular")
		{
			if($friend_query!=""){ $friend_query=" WHERE d.status='0' AND ".$friend_query; }else{ $friend_query=" WHERE d.status='0' "; }
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  $friend_query $search_text_q order by total_like DESC";
		}else if($filter=="financial")
		{
			if($friend_query!=""){ $friend_query=" AND  ".$friend_query; } 
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.status='0' $search_text_q ORDER BY d.id DESC ";
		}else if($filter=="non-financial")
		{
			if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.status='0' $friend_query $search_text_q ORDER BY d.id DESC ";
		}else if($filter=="granted")
		{
			if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
			$sql="SELECT *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like,str_to_date(d.granted_date, '%m-%d-%Y') as mydt from donations d  where d.granted_by IS NOT NULL AND d.status='0' $friend_query $search_text_q ORDER BY mydt DESC ";
		}else if($filter=="in_progress")
		{
			if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.process_status='1' AND d.status='0' $friend_query $search_text_q ORDER BY d.date_updated DESC";

		}else if($filter=="saved"){
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d where status='0' AND  d.id IN(select id from activities where user_id='".$user_id."' AND activity='fav' ) $search_text_q ORDER BY d.id DESC";
		}
		else if($filter=="friends" && $user_id!=""){
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d where d.created_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) AND d.status='0' $search_text_q ORDER BY d.id DESC";
		}else{
			if($friend_query!=""){ $friend_query=" WHERE d.status='0' AND d.granted_by IS NULL AND ".$friend_query; }else{
				$friend_query=" WHERE d.status='0' AND d.granted_by IS NULL ";
			}
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d $friend_query AND d.process_status!='1' $search_text_q ORDER BY d.id DESC";
		}
	}else{
			if($friend_query!=""){
				$friend_query=" WHERE ".$friend_query;  
			}else{ 
				$friend_query=" WHERE d.status='0'  "; 
			}
			// $sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d   $friend_query  $search_text_q  AND d.process_status!='1' AND d.granted_by IS NULL ORDER BY d.id DESC";
			$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d   $friend_query  $search_text_q  ORDER BY d.id DESC";
	}
	// print_r($sql);
	// exit; 

	$result1=$db->query($sql)or die($db->error);

	$total=$result1->num_rows;
	$data['total']=$total;
	$data['start']=$start;

	$data['limit']=$limit;
	if($start != 0)
	{
		$data['start']=$start;
		$sql=$sql." LIMIT $start,$limit";
	}
	

	$result=$db->query($sql)or die($db->error);
	$donations=array();
	
	while($row=$result->fetch_assoc())
	{
		// print_r($row);exit;
		$result_user=$db->query("select * from user_profile where user_id='".$row['created_by']."' LIMIT 1");
		$row_user=$result_user->fetch_assoc();

		$result_fav=$db->query("select a_id from activities where donation_id='".$row['id']."' $uid and activity='fav'");
		
		$result_my_like=$db->query("select * from activities where donation_id='".$row['id']."' AND activity='like'  $uid");

		$delivery = [];

			$deliveryTypes = explode(",", $row['delivery_type']);
		if ($deliveryTypes != FALSE){	
			foreach($deliveryTypes as $deliveryType){
				$delivery[]=intval($deliveryType);
			}
		} else {
			$delivery[] = intval($row['delivery_type']);
		}

		$single_donation=array();
		$single_donation['id']=$row['id'];
		$single_donation['title']=$row['title'];
		$single_donation['description']=stripTags($row['description']);
		$single_donation['image']=$row['image'];
		$single_donation['date']=$row['date_updated'];
		$single_donation['updated']=date('m-d-Y',strtotime($row['date_updated']));
		$single_donation['firstname']=$row_user['firstname'];
		$single_donation['lastname']=$row_user['lastname'];
		$single_donation['donated_by']=$row['created_by'];
		$single_donation['delivery_type']=$delivery;
		$single_donation['delivery_other']=$row['delivery_other'];
		$single_donation['process_granted_by']=$row['process_granted_by'];
		$single_donation['user_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
		$single_donation['likes']=$row['total_like'];
		if($uid!="")
			$single_donation['my_like']=$result_my_like->num_rows;
		else
			$single_donation['my_like']=0;

		$single_donation['in_progress']=$row['process_status'];
		if($uid!="")
			$single_donation['favorite']=$result_fav->num_rows;
		else
			$single_donation['favorite']=0;

		$single_donation['granted_by']=$row['granted_by'];
		// $single_donation['expected_date']=$row['expected_date'];
		// $single_donation['non_pay_option']=$row['non_pay_option'];


		$single_donation['share_url']=$base_url."/donation/view?id=".$single_donation['id'];

		$single_donation['is_reported_by_current_user']= check_reported_content('donation', $row['id'], $user_id, $row_user['user_id'] );

		array_push($donations, $single_donation);

	}		
	$status=1;
	$data['image_base_url']=$base_url."/web/";
	$data['donations']=$donations;

	return $data;
}

?>