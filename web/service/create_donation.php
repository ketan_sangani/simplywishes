<?php 
    global $error_msgs;

    function chk($str)
    {
        $check = (isset($str) && !empty($str));
        // if (!$check){
        //     echo "false $str";
        // }
        return $check;
    }

// Delivery type
    // 0 = “Delivery service (USPS, FedEx, UPS)”,
    // 1 = “Meet in public”,
    // 2 = “You can drop it off at a location”,
    // 3 = “They can pick it up at a location”,
    // 4 = “Other”
    $imageType = null;
    $deliveryOther = null;
    $deliveryType = null;
    $status = 1 ;
// print_r($_REQUEST);exit;
    if(!isset($_REQUEST['token']) 
        || !isset($_REQUEST['uid']) 
        || !isset($_REQUEST['title'])
        || !isset($_REQUEST['desc']) || !isset($_REQUEST['delivery_type']))
        {

            $status=0;
            $msg="Invalid arguments.";
            
        } else if (in_array('4', $_REQUEST['delivery_type'])) {
            if (!isset($_REQUEST['delivery_other'])){

                $status=0;
                $msg="Invalid arguments for delivery. Specify delivery details";  

            } else {
                
                $deliveryOther = $db->real_escape_string($_REQUEST['delivery_other']);
            }
        }

        if ($status == 0){
            return;
        }

        global $base_url;
    
        $token=$db->real_escape_string($_REQUEST['token']);
         $uid=$db->real_escape_string($_REQUEST['uid']);
         $deliveryType = implode(',', $_REQUEST['delivery_type']);
        //  die($deliveryType);

        // if(validate_token($token,$uid)==0)
        if(1==0)    
        {
            $status=0;
            $msg=$error_msgs['invalid_token'];
        }
        else
        {
            $userInfo = get_user_info($uid);


            if (!$userInfo['can_create_donations']) {
                $status=0;
                $msg=$error_msgs['max_donations_reached'] . " maximum is " . $userInfo['max_allowed_donations'];
            } else {
                if (!isset($_REQUEST['status'])){
                    $donation_status = 0;
                } else {
                    $donation_status=$db->real_escape_string($_REQUEST['status']);
                }
    
                $title=$db->real_escape_string($_REQUEST['title']);
                $description=$db->real_escape_string($_REQUEST['desc']);
                // $expected_date=$db->real_escape_string($_REQUEST['expected_date']);
        
                // $non_pay_option=$db->real_escape_string($_REQUEST['non_pay_option']);
                // $expected_cost=$db->real_escape_string($_REQUEST['expected_cost']);
                if (isset($_REQUEST['image_type'])  ){
                    $imageType=$db->real_escape_string($_REQUEST['image_type']);
                }

        
                $ins="insert into donations(title,description,status,created_by, delivery_type ";
                if (isset($deliveryOther)){
                    $ins.=", delivery_other)";
                } else {
                    $ins.=")";
                }
    
                $ins.=" values('$title','$description','$donation_status','$uid', '$deliveryType' ";
                if (isset($deliveryOther)){
                    $ins.=", '$deliveryOther' )";
                } else {
                    $ins.=")";
                }
                // echo $ins;exit;
            // print_r($_FILES);exit;
                $r=$db->query($ins);
    
                if ($r==true) {
                    $donationId=$db->insert_id;
                    $status=1;
                    $msg="Your donation created successfully.";
                    if ($imageType=="custom") {
                        if (isset($_FILES["image"]["tmp_name"])) {
                            $typ = $_FILES['image']['type'];
                            $image_info = getimagesize($_FILES["image"]["tmp_name"]);
                            $image_width = $image_info[0];
                            $image_height = $image_info[1];
                            $path_parts = pathinfo($_FILES["image"]["name"]);
                            $extension = $path_parts['extension'];
                            if ($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream") {
                                $uploaddir = "../uploads/donations/";
                                $name=$donationId.".".$extension;
                                $uploadimages = $uploaddir.basename($name);
                                if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadimages)) {
                                    $donation_image='uploads/donations/'.$name;
                                } else {
                                    $donation_image='images/wish_default/1.jpg';
                                    $msg="Not uploaded";
                                }
                            } else {
                                $donation_image='images/wish_default/1.jpg';
                                $msg="Invalid image type.".$typ;
                            }
                        } else {
                            $donation_image='images/wish_default/1.jpg';
                            $msg="Not set as file";
                        }
                    } else {
                        $donation_image=$db->real_escape_string($_REQUEST['image']);
                        $donation_image=$donation_image;
                    }
    
                    $db->query("update donations set image='$donation_image' where id='$donationId'");
                } else {
                    $status=0;
                    $msg="Error in adding donation".$db->error;
                }
            }
        }

    

?>