<?php 
global $base_url;
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['e_title']))
{

	if( !isset($_FILES["video_file"]["tmp_name"]) && !isset($_REQUEST['youtube_url'])) 
	{
		$msg="Please select video or youtube url";
		$status=0;
	}else{

	global $error_msgs;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$vid_msg=" ";
		$e_text="";	$msg="";
		$e_title=$db->real_escape_string($_REQUEST['e_title']);	
		$video_url="";
		if(isset($_FILES["video_file"]["tmp_name"]))
		{
			$name = preg_replace( 
                     array("/\s+/", "/[^-\.\w]+/"), 
                     array("_", ""), 
                     trim($_FILES["video_file"]["name"])); 
			$path_parts = pathinfo($_FILES["video_file"]["name"]);
			$extension = $path_parts['extension'];
			$name=rand()."_vid.".$extension;
			$dir = "../uploads/media/";
			if(move_uploaded_file($_FILES["video_file"]["tmp_name"], $dir.$name))
			{
				$video_url=$base_url."/web/uploads/media/".$name;	
			}else{
				//$video_url=$base_url."/web/uploads/media/".$name;	
				$vid_msg="Video not uplaoded";
			}
			
			
		}/// if video uploded

		if(isset($_REQUEST['youtube_url']))
		{
			$video_url=$_REQUEST['youtube_url'];
		}
		
		$video_url=$db->real_escape_string($video_url);

		$thumb_url="";

		if(isset($_FILES["thumb_image"]["tmp_name"]))
				{
					$typ = $_FILES['thumb_image']['type'];
					$image_info = getimagesize($_FILES["thumb_image"]["tmp_name"]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];
					$path_parts = pathinfo($_FILES["thumb_image"]["name"]);
					$extension = $path_parts['extension'];
				    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream")
				        {
				              $uploaddir = "../uploads/editorial/";
				              $name=rand().".".$extension;
				              $uploadimages = $uploaddir.basename($name);
				            if(move_uploaded_file($_FILES['thumb_image']['tmp_name'], $uploadimages))
				            {
				              $thumb_url='uploads/editorial/'.$name;
				            }else{
				            	$thumb_url='';
				            	$msg="Not uploaded";
				            }
				      }
				    else
	                {
	                  $profile_image='images/img1.jpg';
	                  $msg="Invalid image type.";
	                }
		}//if thumb file uploaded

		$thumb_url=$db->real_escape_string($thumb_url);
			
		$ins_first="insert into editorial(e_title,e_text,e_image,featured_video_url,status,is_video_only,created_by) ";
		$ins_first.="  values('$e_title','$e_text','$thumb_url','$video_url','0','1','$uid')";
		
		
		$r=$db->query($ins_first);
		if($r==TRUE)
		{
			$status=1;
			$msg="Added successfully.".$vid_msg;
		}else{
			$status=0;
			$msg="Error in adding.";
		}
	}	
 }
}else{
	$status=0;
	//$data['post']=$_REQUEST;
	//$data['imgdata']=$_FILES;
	$msg=$error_msgs['invalid_argument'];

}

