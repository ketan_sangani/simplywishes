<?php 
global $base_url;
global $base_url_image;
global $error_msgs;
$uid="";
$user_id="";


if(isset($_REQUEST['uid']) && isset($_REQUEST['token']) && isset($_REQUEST['w_id']))
{
		$user_id=$db->real_escape_string($_REQUEST['uid']); 
		$token=$db->real_escape_string($_REQUEST['token']);
		$wid=$db->real_escape_string($_REQUEST['w_id']); 
		if(validate_token($token,$user_id)==0)
		{
				$status=0;
				$msg=$error_msgs['invalid_token'];
			
		} else {

				$result=$db->query("select * from wishes where w_id='$wid' LIMIT 1");
				$row=$result->fetch_object();

				if($row->wished_by!=$user_id && $row->granted_by==NULL && $row->process_status!=1)
				{
					$uid=$user_id;

					$date=date('m-d-Y');
					$db->query("update wishes set i_agree_decide='1',process_status='1',process_granted_date='$date',process_granted_by='$uid' where w_id='$wid'");
					$status=1;
					$msg="Success";

					$wish_result=$db->query("select * from wishes where w_id='$wid' LIMIT 1");
					$row_wish=$wish_result->fetch_object();

					$sql_uid="select username,email from user where id='$uid' LIMIT 1";
					$sql_wish_user="select username,email from user where id='$row_wish->wished_by' LIMIT 1";

					$result_uid=$db->query($sql_uid);
					$result_wish_user=$db->query($sql_wish_user);

					$row_uid=$result_uid->fetch_object();
					$row_wish_user=$result_wish_user->fetch_object();

					$result_email_content=$db->query("select * from mail_content where m_id='12' LIMIT 1");
					$row_email_content=$result_email_content->fetch_assoc();
					
					$loginlink= $base_url."/site/login";
					$wishlink= $base_url."/wish/view?id=".$wid;
					$editmessage=$row_email_content['mail_message'];
					$editmessage = str_replace("##USERNAME##", nl2br($row_wish_user->username), $editmessage);		
					$editmessage = str_replace("##USERNAME2##", nl2br($row_uid->username), $editmessage);	
					$editmessage = str_replace("##WISHLINK##", "<a href='$wishlink'>".nl2br($row_wish->wish_title)."</a>", $editmessage);	
				//	$editmessage = str_replace("##USERNAME2##", nl2br($row_uid->username), $editmessage);
					$msg1="";
					if($row_wish->show_mail_status=="1")
					{
						$msg1.="Mail to this address :".$row_wish->show_mail;
					}

					if($row_wish->show_person_status=="1")
					{
						$msg1.="<br> In Person at this location : $row_wish->show_person_street $row_wish->show_person_city $row_wish->show_person_state $row_wish->show_person_zip $row_wish->show_person_country";
					}

					if($row_wish->show_other_status=="1")
					{
						$msg1.="<br> Other : ".$row_wish->show_other_specify;
					}

					$editmessage = str_replace("##MESSAGE##", nl2br($msg1), $editmessage);
					
					$editmessage = str_replace("##LOGINLINK##", "<a href='$loginlink'>$loginlink</a>", $editmessage);
					send_email($row_uid->email,$editmessage,$row_email_content['mail_subject']);


				}/// wish user and login user is same
				else
				{
					$status=0;
					$msg=" You are not allowed to grant this wish.";

				}

		} 
}
else
{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

