<?php 
global $error_msgs;
if (isset($_REQUEST['token']) && isset($_REQUEST['uid'])) {
    global $base_url;
	global $base_url_image;
	global $select_reported;
	global $exclude_reported;
	
	$token=$db->real_escape_string($_REQUEST['token']);
    $uid=$db->real_escape_string($_REQUEST['uid']);
    $msg=" ";
     if(validate_token($token,$uid)==0) 
    //  if(1==0)
     {
         $status=0;
         $msg.=$error_msgs['invalid_token'];
         
     } else { 
        // echo "1";
        $data  = get_user_info($uid);

		$status=1;
        $msg="";
        if (!$data['can_create_wishes']) {
            $msg.=$error_msgs['max_wishes_reached'] . ". The maximum allowed is " . $data['max_allowed_wishes'].". ";
        }

        if (!$data['can_create_donations']) {
            $msg.=$error_msgs['max_donations_reached'] . ". The maximum allowed is " . $data['max_allowed_donations'].". ";
        }

     }
}else{
    // echo "2";
	$status=0;
	$msg.=$error_msgs['invalid_argument'];
}
