<?php 
global $base_url;
global $paypal_url;
global $base_url_image;
global $error_msgs;
global $select_reported;
$uid="";
$user_id="";
$friend_query="";
$sql="";
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']))
{

	$token=$db->real_escape_string($_REQUEST['token']); 
	$uid=$db->real_escape_string($_REQUEST['uid']);
	$user_id = $uid;
	// follow this template to exclude content from reported user
	$exclude_reported = " AND d.created_by not in ($select_reported $uid) ";

	
 	// toaddreport
	// if(validate_token($token,$uid)==0)
	if(1==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$uid=""; 
		$uid=" AND user_id='".$_REQUEST['uid']."'";

		if(isset($_REQUEST['is_friend']))
		{
			$fq=$db->real_escape_string($_REQUEST['is_friend']);

			if($fq==1)
			{
				$friend_query=" d.created_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) ";
			}
		}
		$search_text_q="";

		$search_text_q = $exclude_reported;

		if(isset($_REQUEST['search_text']))
		{
			$search_text=$_REQUEST['search_text'];
			$search_text_q.=" AND d.title LIKE '%$search_text%' ";
		}

		if(isset($_REQUEST['filter'])) 
		{
			$filter=$db->real_escape_string($_REQUEST['filter']);
			if($filter=="popular")
			{
				if($friend_query!=""){ $friend_query=" WHERE d.status='0' AND ".$friend_query; }else{ $friend_query=" WHERE d.status='0' "; }
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  $friend_query $search_text_q order by total_like DESC";
			}else if($filter=="financial")
			{
				if($friend_query!=""){ $friend_query=" AND  ".$friend_query; } 
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.non_pay_option='0' $friend_query AND d.status='0' $search_text_q ORDER BY d.id DESC ";
			}else if($filter=="non-financial")
			{
				if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.non_pay_option='1' AND d.status='0' $friend_query $search_text_q ORDER BY d.id DESC ";
			}else if($filter=="granted")
			{
				if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
				$sql="SELECT *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like,str_to_date(d.granted_date, '%m-%d-%Y') as mydt from donations d  where d.granted_by IS NOT NULL AND d.status='0' $friend_query $search_text_q ORDER BY mydt DESC ";
			}else if($filter=="in_progress")
			{
				if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d  where d.process_status='1' AND d.status='0' $friend_query $search_text_q ORDER BY d.date_updated DESC";

			}else if($filter=="friends" && $user_id!=""){
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d where d.created_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) AND d.status='0' $search_text_q ORDER BY d.id DESC";
			}else{
				if($friend_query!=""){ $friend_query=" WHERE d.status='0' AND d.granted_by IS NULL AND ".$friend_query; }else{
					$friend_query=" WHERE d.status='0' AND d.granted_by IS NULL ";
				}
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d $friend_query AND d.process_status!='1' $search_text_q ORDER BY d.id DESC";
			}
		}else{
			
				if($friend_query!=""){
					$friend_query=" WHERE ".$friend_query;  
				}else{ 
					$friend_query=" WHERE d.status='0'  "; 
				}
				$sql="select *,(select COUNT(a_id) from activities where donation_id=d.id and activity='like' ) as total_like from donations d   $friend_query  $search_text_q  AND d.process_status!='1' AND d.granted_by IS NULL ORDER BY d.id DESC";
		}

		// print_r($sql);
		// exit;

		$result1=$db->query($sql)or die($db->error);
		$total=$result1->num_rows;
		$data['total']=$total;
		$data['start']=0;
		$limit=10;
		if(isset($_REQUEST['limit']))
		{
			$limit=$_REQUEST['limit'];

		}
		$data['limit']=$limit;
		if(isset($_REQUEST['start']))
		{
			$start=$_REQUEST['start'];
			$data['start']=$start;
			$sql=$sql." LIMIT $start,$limit";
		}
		

		$result=$db->query($sql)or die($db->error);
		$donations=array();
		
		while($row=$result->fetch_assoc())
		{
			// print_r($row);exit;
			$result_user=$db->query("select * from user_profile where user_id='".$row['created_by']."' LIMIT 1");
			$row_user=$result_user->fetch_assoc();

			$result_fav=$db->query("select a_id from activities where donation_id='".$row['id']."' $uid and activity='fav'");
			
			$result_my_like=$db->query("select * from activities where donation_id='".$row['id']."' AND activity='like'  $uid");

			$delivery = [];

				$deliveryTypes = explode(",", $row['delivery_type']);
			if ($deliveryTypes != FALSE){	
				foreach($deliveryTypes as $deliveryType){
					$delivery[]=intval($deliveryType);
				}
			} else {
				$delivery[] = intval($row['delivery_type']);
			}

			$single_donation=array();
			$single_donation['id']=$row['id'];
			$single_donation['title']=$row['title'];
			$single_donation['description']=$row['description'];
			$single_donation['image']=$row['image'];
			$single_donation['date']=$row['date_updated'];
			$single_donation['updated']=date('m-d-Y',strtotime($row['date_updated']));
			$single_donation['firstname']=$row_user['firstname'];
			$single_donation['lastname']=$row_user['lastname'];
			$single_donation['donated_by']=$row['created_by'];
			$single_donation['delivery_type']=$delivery;
			$single_donation['delivery_other']=$row['delivery_other'];

			$single_donation['user_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
			$single_donation['likes']=$row['total_like'];
			if($uid!="")
				$single_donation['my_like']=$result_my_like->num_rows;
			else
				$single_donation['my_like']=0;

			$single_donation['in_progress']=$row['process_status'];
			if($uid!="")
				$single_donation['favorite']=$result_fav->num_rows;
			else
				$single_donation['favorite']=0;

			$single_donation['granted_by']=$row['granted_by'];
			// $single_donation['expected_date']=$row['expected_date'];
			// $single_donation['non_pay_option']=$row['non_pay_option'];

			if($row['non_pay_option']==1)
			{
				$single_donation['show_mail_status']=$row['show_mail_status'];
				$single_donation['show_mail']=$row['show_mail'];
				$single_donation['show_person_status']=$row['show_person_status'];
				$single_donation['show_person_street']=$row['show_person_street'];
				$single_donation['show_person_city']=$row['show_person_city'];
				$single_donation['show_person_state']=$row['show_person_state'];
				$single_donation['show_person_zip']=$row['show_person_zip'];
				$single_donation['show_person_country']=$row['show_person_country'];
				$single_donation['show_other_status']=$row['show_other_status'];
				$single_donation['show_other_specify']=$row['show_other_specify'];
			}else{
				// $single_donation['paypal_url']=$paypal_url."/web/service/pay.php?w_id=".$single_donation['id']."&uid=".$user_id."&token=".md5(rand());
			}

			$single_donation['share_url']=$base_url."/donation/view?id=".$single_donation['id'];

			//added for is_reported_by_current_user 
			$single_donation['is_reported_by_current_user']= check_reported_content('donation', $row['id'], $user_id, $row_user['user_id'] );


			// $block=check_block_content("wish",$single_donation['w_id']);
			// if($block!=1)
				array_push($donations, $single_donation);

		}		
		$status=1;
		$data['image_base_url']=$base_url."/web/";
		$data['donations']=$donations;
		// $data['q']=$sql;
	}// if token is valid
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

