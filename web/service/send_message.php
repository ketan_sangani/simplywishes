<?php 
date_default_timezone_set('America/Los_Angeles');
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['text']) && isset($_REQUEST['recipient_id'])) 
{
	global $base_url_image;
	global $base_url;
	global $error_msgs;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	// if(validate_token($token,$uid)==0)
	if(1==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$parent_id=0;
		if(isset($_REQUEST['parent_id']))
		{
			if($_REQUEST['parent_id']!="")
				$parent_id=$db->real_escape_string($_REQUEST['parent_id']);	

		}else{
			$parent_id=0;
		}
		if(isset($_REQUEST['is_first']))
		{
			if($_REQUEST['is_first']!="")
				$is_first=$db->real_escape_string($_REQUEST['is_first']);	

		}else{
			$is_first=0;
		}

		
		$ins="";
		$recipient_id=$db->real_escape_string($_REQUEST['recipient_id']);
		$text=$db->real_escape_string($_REQUEST['text']);
		$created_at=date('Y-m-d H:i:s');
		if($is_first==0)
		{
			$sqlmid="select m_id,recipient_id,sender_id from messages where sender_id='$recipient_id' AND recipient_id='$uid' AND parent_id='0' AND delete_status='' ";	
			$resultmid=$db->query($sqlmid);
			if($resultmid->num_rows>0){
			$rowmid=$resultmid->fetch_object();
				$ins="insert into messages(sender_id,recipient_id,parent_id,reply_sender_id,reply_recipient_id,text,created_at) values('$uid','$recipient_id','$rowmid->m_id',$rowmid->recipient_id,$rowmid->sender_id,'$text','$created_at')";	
			}else{
				$ins="insert into messages(sender_id,recipient_id,parent_id,text,created_at) values('$uid','$recipient_id','$parent_id','$text','$created_at')";		
			}

		}else{
			$ins="insert into messages(sender_id,recipient_id,parent_id,text,created_at) values('$uid','$recipient_id','$parent_id','$text','$created_at')";	
		}
		
		
		$result=$db->query($ins);
		// $result = TRUE;
		if($result==TRUE)
		{
			$iid=$db->insert_id;
			$status=1;
			$msg="Success";
			$result_sender=$db->query("select * from user where id='$uid' LIMIT 1");
			$row_sender=$result_sender->fetch_object();

			$result_user=$db->query("select device_type,udid from user where id='$recipient_id' LIMIT 1");
			$row_u=$result_user->fetch_object();
			$msgdata=array();
			$msgdata['sender_id']=$uid;
			$msgdata['recipient_id']=$recipient_id;
			$msgdata['msg_id']=$iid;
			$msgdata['screen']="message";
			$msgdata['badge_count']=get_unread_msg_total($recipient_id);

			$messageandroid=array('title'=>'Message from '.$row_sender->username,'body'=>$text,"data"=>$msgdata);
			$messageios=array('title'=>'Message from '.$row_sender->username,'body'=>$_REQUEST['text']);

			if($row_u->device_type=="Iphone")
				$data['er']=sendApplePushNotification($row_u->udid, $messageios,$msgdata);
			else
				$data['er']=sendAndroidPushNotification(array($row_u->udid), $messageandroid);
		}else{
			$status=0;
			$msg="Error in sending message";
		}


	}/// token is valid
		
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

