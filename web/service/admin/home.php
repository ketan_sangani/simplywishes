<?php include "header.php"; ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			  <!-- Default panel contents -->
			  <div class="panel-heading">Report Content</div>
			  <div class="panel-body">
			    
			  </div>

			  <!-- Table -->
			  <table class="table">
			  	<thead>
			    	<tr>
			    		<th>Date</th>
			    		<th>Content Owner </th>
			    		<th>User Reporting</th>
			    		<th>Content Type</th>
			    		<th>Note</th>
			    		<th></th>
			    	</tr>
			    </thead>
			    <tbody id="result_filter" >
			    	
			    </tbody>
			    <tfoot>
			    	<tr id="loading"><th colspan="6"  >Loading...</th></tr>
			    	<tr><th colspan="6" id="pagination" ></th></tr>
			    </tfoot>
			  </table>
			</div><!-- panel-->
		</div><!-- col -->
	</div><!-- row-->
</div><!-- container-->
<?php include "footer.php"; ?>
<script type="text/javascript">
	function load_list(current_page)
	{
		$.ajax({
            'url':'get_list.php',
            'type':'post',
            'data': 'p='+current_page+"&eid=",
            success:function(data){

              $("#loading").css("display","none");
              $('#result_filter').css("visibility","visible");
              $('#pagination').css("visibility","visible");
                var data    =   $.parseJSON(data);
                $('#result_filter').html(data.html);
                $('#pagination').html(data.pagination);
                 $('[data-toggle="tooltip"]').tooltip();

            }
        });

              $("#loading").css("display","table-row");
              $('#result_filter').css("visibility","hidden");
              $('#pagination').css("visibility","hidden");
	}// load list
	load_list(0);

	$("body").on('click','.list_pagination li a',function(){
		  current_page    =   $(this).attr('href').split('#').join('');
		  load_list(current_page);
		});

	$(document).on("click",".block_content",function(){
		var id=$(this).data("id");
		var ele=$(this);
		var r=confirm("Are you sure want to block this content ?");
		if(r)
		{
			$.ajax({
				url:'ajax_block_content.php',
				data:{id:id},
				type:'POST',
				success:function(data){
					load_list(0);
				},
				error:function(data){
					alert("Error");
				}
			});// ajax call	
		}
		
	});/// block conent button click

	 $('[data-toggle="tooltip"]').tooltip();
</script>