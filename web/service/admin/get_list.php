<?php include "../conn.php";
session_start();

if(!isset($_SESSION['login_token']))
{
    echo "";
    die();
}

function get_user($id)
{
	global $db;
	$result=$db->query("select * from user where id='$id' LIMIT 1");
	$row=$result->fetch_object();
	return $row;
}

$sql="select * from report_content ";


   $limit = 5; 
    
    
    $result_b=$db->query($sql);
     $total_pages = $result_b->num_rows;
      //$total_pages = $total_pages[0];
      
      $stages = 3;
      $page = $db->real_escape_string($_POST['p']);
      if($page){
        $start = ($page - 1) * $limit; 
      }else{
        $start = 0; 
      }
      
      $query1 = "select * from report_content  LIMIT $start, $limit"; 
      
      
      $result = $db->query($query1)or die($db->error);
      
      // Initial page num setup
      if ($page == 0){$page = 1;}
      $prev = $page - 1;  
      $next = $page + 1;              
      $lastpage = ceil($total_pages/$limit);    
      $LastPagem1 = $lastpage - 1;  
      
        $paginate = '';
  if($lastpage > 1)
  { 
  

  
  
    $paginate .= "<ul class='pagination list_pagination pull-right'>";
    // Previous
    if ($page > 1){
      $paginate.= "<li><a href='#$prev'>Previous</a></li>";
    }else{
      $paginate.= "<li><span class='disabled'>Previous</span></li>"; }
      

    
    // Pages  
    if ($lastpage < 7 + ($stages * 2))  // Not enough pages to breaking it up
    { 
      for ($counter = 1; $counter <= $lastpage; $counter++)
      {
        if ($counter == $page){
          $paginate.= "<li><span class='current'>$counter</span></li>";
        }else{
          $paginate.= "<li><a href='#$counter'>$counter</a></li>";}          
      }
    }
    elseif($lastpage > 5 + ($stages * 2)) // Enough pages to hide a few?
    {
      // Beginning only hide later pages
      if($page < 1 + ($stages * 2))   
      {
        for ($counter = 1; $counter < 4 + ($stages * 2); $counter++)
        {
          if ($counter == $page){
            $paginate.= "<li><span class='current'>$counter</span></li>";
          }else{
            $paginate.= "<li><a href='#$counter'>$counter</a></li>";}          
        }
        $paginate.= "<li><span>...</span></li>";
        $paginate.= "<li><a href='#$LastPagem1'>$LastPagem1</a></li>";
        $paginate.= "<li><a href='#$lastpage'>$lastpage</a></li>";   
      }
      // Middle hide some front and some back
      elseif($lastpage - ($stages * 2) > $page && $page > ($stages * 2))
      {
        $paginate.= "<li><a href='#1'>1</a></li>";
        $paginate.= "<li><a href='#2'>2</a></li>";
        $paginate.= "<li><span>...</span></li>";
        for ($counter = $page - $stages; $counter <= $page + $stages; $counter++)
        {
          if ($counter == $page){
            $paginate.= "<li><span class='current'>$counter</span></li>";
          }else{
            $paginate.= "<li><a href='#$counter'>$counter</a></li>";}          
        }
        $paginate.= "<li><span>...</span></li>";
        $paginate.= "<li><a href='#$LastPagem1'>$LastPagem1</a></li>";
        $paginate.= "<li><a href='#$lastpage'>$lastpage</a></li>";   
      }
      // End only hide early pages
      else
      {
        $paginate.= "<li><a href='#1'>1</a></li>";
        $paginate.= "<li><a href='#2'>2</a></li>";
        $paginate.= "<li><span>...</span></li>";
        for ($counter = $lastpage - (2 + ($stages * 2)); $counter <= $lastpage; $counter++)
        {
          if ($counter == $page){
            $paginate.= "<li><span class='current'>$counter</span></li>";
          }else{
            $paginate.= "<li><a href='#$counter'>$counter</a></li>";}          
        }
      }
    }
          
        // Next
    if ($page < $counter - 1){ 
      $paginate.= "<li><a href='#$next'>Next</a></li>";
    }else{
      $paginate.= "<li><span class='disabled'>Next</span></li>";
      }
      
    $paginate.= "</ul>";   
  
  
} 



$html="";

if($result->num_rows==0)
{
  $html.="<tr><td colspan='6'><center>No record found </center></td></tr>";
}

$content_type_arr=array("article"=>"Article","article_comment"=>"Article Comment","wish"=>"Wish","wish_comment"=>"Wish Comment","message"=>"Message","user"=>"User");

 while($row=$result->fetch_object())
{
	$user_report=get_user($row->report_user);
	$user_reported=get_user($row->reported_user);
	$btn_block="";
	if($row->blocked==0)
		$btn_block="<button  data-toggle='tooltip' title='Block' class='btn btn-danger btn-xs block_content' data-id='$row->id' type='button' ><span class='glyphicon glyphicon-ban-circle'></span></button>";

	$btn_view="<a data-toggle='tooltip' title='View' class='btn btn-xs btn-primary' href='view.php?id=$row->id' ><span class='glyphicon glyphicon-eye-open'></span></a>";
	
	$html.="<tr>";
		$html.="<td>".date("m/d/Y h:i A",strtotime($row->date))."</td>";
		$html.="<td>$user_reported->username</td>";
		$html.="<td>$user_report->username</td>";
		$html.="<td>".$content_type_arr[$row->report_type]."</td>";
		$html.="<td>$row->comment</td>";
		$html.="<td> $btn_view $btn_block</td>";
	$html.="</tr>";
} 


  $data           =   array(
                            'html'          =>   $html,
                            'pagination'    =>   $paginate
                        );
    echo json_encode($data);

    ?>
