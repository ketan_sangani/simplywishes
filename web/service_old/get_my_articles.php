<?php 
global $base_url;
global $base_url_image;
global $error_msgs;
$uid="";
$user_id="";

if(isset($_REQUEST['uid']) && isset($_REQUEST['token']))
{
	$token=$db->real_escape_string($_REQUEST['token']);
	$uid=$db->real_escape_string($_REQUEST['uid']);

if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{

		if(isset($_REQUEST['uid']))
		{
			$user_id=$db->real_escape_string($_REQUEST['uid']);

			$uid=" and created_by='".$user_id."'";
		}
		if(isset($_REQUEST['filter']))
		{
			$filter=$db->real_escape_string($_REQUEST['filter']);
			if($filter=="video")
			{
				$sql="select e_id,e_title,e_text,e_image,featured_video_url,created_at,created_by from editorial where status='0' and is_video_only='1' $uid";
			}else if($filter=="article")
			{
				$sql="select e_id,e_title,e_text,e_image,featured_video_url,created_at,created_by from editorial where status='0' and is_video_only='0' $uid";
			}else
			{
				$sql="select e_id,e_title,e_text,e_image,featured_video_url,created_at,created_by from editorial where status='0' $uid";
			}
		}else{
				$sql="select e_id,e_title,e_text,e_image,featured_video_url,created_at,created_by from editorial where status='0' $uid ";
		}



		$result1=$db->query($sql)or die($db->error);
		$total=$result1->num_rows;
		$data['total']=$total;
		$data['start']=0;
		$limit=10;
		if(isset($_REQUEST['limit']))
		{
			$limit=$_REQUEST['limit'];

		}
		$data['limit']=$limit;
		if(isset($_REQUEST['start']))
		{
			$start=$_REQUEST['start'];
			$data['start']=$start;
			$sql=$sql." LIMIT $start,$limit";
		}

		//$data['sql']=$sql;
		$result=$db->query($sql)or die($db->error);
		$arr_article=array();
		while($row=$result->fetch_assoc())
		{

			$result_user=$db->query("select * from user_profile where user_id='".$row['created_by']."' LIMIT 1");
			$row_user=$result_user->fetch_assoc();

			$result_like=$db->query("select a_id from editorial_activities where e_id='".$row['e_id']."' and activity='like'");
			

			$single_article=array();
			$single_article['e_id']=$row['e_id'];
			$single_article['e_title']=$row['e_title'];
			$single_article['e_text']=$row['e_text'];
			$single_article['e_image']=$row['e_image'];
			$single_article['featured_video_url']=$row['featured_video_url'];
			$single_article['date']=$row['created_at'];
			$single_article['firstname']=$row_user['firstname'];
			$single_article['lastname']=$row_user['lastname'];


			$single_article['user_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
			$single_article['likes']=$result_like->num_rows;
			$y=strpos($row['featured_video_url'], "youtube.");
			if($y==false || $y==0)
			{
				$single_article['is_youtube']=0;
			}else{
				$single_article['is_youtube']=1;
			}
			$single_article['share_url']=$base_url."/editorial/editorial-page?id=".$row['e_id']."&class=article";
			
			array_push($arr_article, $single_article);
		}
		$status=1;
		$data['article_image_base_url']=$base_url."/web/";
		$data['article_list']=$arr_article;
		
		}// if token correct login

}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}


