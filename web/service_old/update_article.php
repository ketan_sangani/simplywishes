<?php 

if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['e_text']) && isset($_REQUEST['e_title']) && isset($_REQUEST['e_id']))
{

	global $base_url;
	global $error_msgs;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	$e_id=$db->real_escape_string($_REQUEST['e_id']);
 	
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$e_text=$db->real_escape_string($_REQUEST['e_text']);	
		$e_title=$db->real_escape_string($_REQUEST['e_title']);	

		$is_new_video=$db->real_escape_string($_REQUEST['is_new_video']);
		$is_new_image=$db->real_escape_string($_REQUEST['is_new_image']);

		$video_url="";
	if($is_new_video==1)
	{
		if(isset($_FILES["video_file"]["tmp_name"]))
		{
			$name = preg_replace( 
                     array("/\s+/", "/[^-\.\w]+/"), 
                     array("_", ""), 
                     trim($_FILES["video_file"]["name"])); 
			$dir = "../uploads/media/";
			$path_parts = pathinfo($_FILES["video_file"]["name"]);
			$extension = $path_parts['extension'];
			$name=rand()."_vid.".$extension;
			
			move_uploaded_file($_FILES["video_file"]["tmp_name"], $dir.$name);	
			$video_url=$base_url."/web/uploads/media/".$name;
		}/// if video uploded

		if(isset($_REQUEST['youtube_url']))
		{
			$video_url=$_REQUEST['youtube_url'];
		}
		
		$video_url=$db->real_escape_string($video_url);
	}// if its new video uploaded


		$thumb_url="";
if($is_new_image==1)
{
		if(isset($_FILES["thumb_image"]["tmp_name"]))
				{
					$typ = $_FILES['thumb_image']['type'];
					$image_info = getimagesize($_FILES["thumb_image"]["tmp_name"]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];
					$path_parts = pathinfo($_FILES["thumb_image"]["name"]);
					$extension = $path_parts['extension'];
				    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream" )
				    {
				              $uploaddir = "../uploads/editorial/";
				              $name=rand().".".$extension;
				              $uploadimages = $uploaddir.basename($name);
				            if(move_uploaded_file(strip_tags($_FILES['thumb_image']['tmp_name']), $uploadimages))
				            {
				              $thumb_url='uploads/editorial/'.$name;
				            }else{
				            	$thumb_url='';
				            	$msg="Not uploaded";
				            }
				    }
				    else
	                {
	                  $thumb_url='';
	                  $msg="Invalid image type.";
	                }
		}//if thumb file uploaded

		$thumb_url=$db->real_escape_string($thumb_url);
			
}/// if it is new uploaded image

$inskey="";
$insval="";
if($is_new_video==1)
{
	$inskey.=",featured_video_url='$video_url'";
	
}

if($thumb_url!="")
{
	$inskey.=",e_image='$thumb_url'";
	
}
$dt=date('Y-m-d H:i:s');
$ins_first="update editorial set updated_by='$uid',updated_at='$dt',e_title='$e_title',e_text='$e_text',status='0',is_video_only='0'".$inskey." WHERE e_id='$e_id'";
		
		
		$r=$db->query($ins_first);
		if($r==TRUE)
		{
			$status=1;
			$msg="updated successfully.".json_encode($_REQUEST);
		}else{
			$status=0;
			$msg="Error in updating.";
		}
	}	
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

