<?php 

if(isset($_REQUEST['token']) && isset($_REQUEST['uid']))
{

	global $base_url;
	global $error_msgs;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
		
	}else{

		
		
	 	$profile_image_type=$db->real_escape_string($_REQUEST['profile_image_type']);
	 	//$profile_image=$db->real_escape_string($_REQUEST['profile_image']);
	 	if($profile_image_type=="custom")
		{
	 		if(isset($_FILES["profile_image"]["tmp_name"]))
			{
				$typ = $_FILES['profile_image']['type'];
				$image_info = getimagesize($_FILES["profile_image"]["tmp_name"]);
				$image_width = $image_info[0];
				$image_height = $image_info[1];
				$path_parts = pathinfo($_FILES["profile_image"]["name"]);
				$extension = $path_parts['extension'];
			    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream" )
			        {
			              $uploaddir = "../uploads/users/";
			              $name=$uid.".".$extension;
			              $uploadimages = $uploaddir.basename($name);
			            if(move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadimages))
			            {
			              $profile_image=$name;
			            }else{
			            	$profile_image='images/img1.jpg';
			            	$msg="Not uploaded";
			            }
			      }
			      else
	                {
	                  $profile_image='images/img1.jpg';
	                  $msg="Invalid image type.";
	                }
			}/// if condition
		}/// profile image type
		else{
						$profile_image=$db->real_escape_string($_REQUEST['profile_image']);
				}
				$img_path=$base_url."/web/uploads/users/".$profile_image;
		$updated_at=date("Y-m-d H:i:s");
	 	
	 	$sql="update user_profile set profile_image='$profile_image' where user_id='$uid' ";
	 	$db->query($sql);
	 	$status=1;
	 	$msg="Profile image updated successfully.";
	 	$data['image_path']=$img_path;

	}///token is valid
}else{
	$status=0;
		$msg=$error_msgs['invalid_argument'];

}

