<?php 

global $base_url;
global $base_url_image;
global $paypal_url;
global $error_msgs;


$uid="";
$user_id="";
if(isset($_REQUEST['w_id']) && isset($_REQUEST['token']) && isset($_REQUEST['uid']))
{
	$user_id=$db->real_escape_string($_REQUEST['uid']);

	$uid=" and user_id='".$user_id."'";
$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{


	$w_id=$db->real_escape_string($_REQUEST['w_id']);
	$result=$db->query("select w_id,wished_by,granted_by,granted_date,wish_title,wish_description,primary_image,process_status as in_progress,expected_cost,expected_date,who_can,non_pay_option,date_updated,show_mail_status,show_mail,show_person_status,show_person_street,show_person_city,show_person_state,show_person_zip,show_person_country,show_other_status,show_other_specify,process_status,wish_status from wishes where w_id='$w_id' LIMIT 1");
	if($result->num_rows>0)
	{
		$uid=" AND user_id='".$db->real_escape_string($_REQUEST['uid'])."'";
		$status=1;
		$row=$result->fetch_assoc();
	

		$result_user=$db->query("select * from user_profile where user_id='".$row['wished_by']."' LIMIT 1");
		$row_user=$result_user->fetch_assoc();
		$row['wish_user_id']=$row['wished_by'];
		$row['wish_user_firstname']=$row_user['firstname'];
		$row['wish_user_lastname']=$row_user['lastname'];
		$row['wish_user_country']=$row_user['country'];
		$row['wish_user_state']=$row_user['state'];
		$row['wish_user_city']=$row_user['city'];
		$row['wish_user_about']=$row_user['about'];
		$row['profile_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
		$sql_friend="select * from follow_request where requested_by='$user_id' AND requested_to='".$row['wished_by']."' AND status='0' LIMIT 1";
		$result_friend=$db->query($sql_friend)or die($db->error);

		$row['is_friend']=$result_friend->num_rows;
		

		if($row['granted_by']!=NULL)
		{
			$result_granter=$db->query("select * from user_profile where user_id='".$row['granted_by']."' LIMIT 1");
			$row_granter=$result_granter->fetch_assoc();
			$row['granter_user_id']=$row['granted_by'];
			$row['granter_firstname']=$row_granter['firstname'];
			$row['granter_lastname']=$row_granter['lastname'];
			$row['granter_country']=$row_granter['country'];
			$row['granter_state']=$row_granter['state'];
			$row['granter_city']=$row_granter['city'];
			$row['granter_about']=$row_granter['about'];
			$row['granter_profile_image']=$base_url."/web/uploads/users/".$row_granter['profile_image'];
			
		}else{
			$row['granter_user_id']="";
			$row['granter_firstname']="";
			$row['granter_lastname']="";
			$row['granter_country']="";
			$row['granter_state']="";
			$row['granter_city']="";
			$row['granter_about']="";
			$row['granter_profile_image']="";
		}
		
		
		$arr_user_likes=array();

		$result_likes=$db->query("select a_id,user_id from activities where wish_id='$w_id' and activity='like'");
		$data['wish_total_likes']=$result_likes->num_rows;
		while($row_like=$result_likes->fetch_assoc())
		{
			$result_user_like=$db->query("select firstname,lastname,profile_image,user_id,about,country,city,state from user_profile where user_id='".$row_like['user_id']."' LIMIT 1");
			if($result_user_like->num_rows>0)
			{
				$row_user_like=$result_user_like->fetch_assoc();
				$row_user_like['a_id']=$row_like['a_id'];
				$row_user_like['profile_image']=$base_url."/web/uploads/users/".$row_user_like['profile_image'];
				array_push($arr_user_likes, $row_user_like);
			}

		}
		$data['my_like']=0;
		$data['favorite']=0;


		if($uid!=""){

			$result_fav=$db->query("select a_id from activities where wish_id='".$w_id."' $uid and activity='fav'");
			$result_my_like=$db->query("select * from activities where wish_id='".$w_id."' AND activity='like'  $uid");
			$data['my_like']=$result_my_like->num_rows;
			$data['favorite']=$result_fav->num_rows;
		}

		

			if($row['non_pay_option']==1)
			{
				$single_wish['show_mail_status']=$row['show_mail_status'];
				$single_wish['show_mail']=$row['show_mail'];
				
				$single_wish['show_person_status']=$row['show_person_status'];
				$single_wish['show_person_street']=$row['show_person_street'];
				$single_wish['show_person_city']=$row['show_person_city'];
				$single_wish['show_person_state']=$row['show_person_state'];
				$single_wish['show_person_zip']=$row['show_person_zip'];
				$single_wish['show_person_country']=$row['show_person_country'];

				$single_wish['show_other_status']=$row['show_other_status'];
				$single_wish['show_other_specify']=$row['show_other_specify'];
			}else{
				$row['paypal_url']=$base_url."/web/service/pay.php?w_id=".$row['w_id']."&uid=".$user_id."&token=".md5(rand());
			}

			$row['share_url']=$base_url."/wish/view?id=".$row['w_id'];

			$row['date']=$row['date_updated'];
			$row['date_updated']=date('m-d-Y',strtotime($row['date_updated']));
		$data['wish_likes']=$arr_user_likes;
		$data['wish_detail']=$row;
		$data['wish_image_base_url']=$base_url."/web/";

	}else{
		$status=0;
		$msg="Invalid wish id.";	
	}

	}//if token is valid
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];
}
?>