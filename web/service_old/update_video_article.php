<?php 
global $error_msgs;
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['e_title']))
{
	$e_id=$db->real_escape_string($_REQUEST['e_id']);
	$is_new_video=$db->real_escape_string($_REQUEST['is_new_video']);
	$is_new_image=$db->real_escape_string($_REQUEST['is_new_image']);
	$status=1;
	if($is_new_video==1)
	{
		if(!isset($_FILES["video_file"]["tmp_name"]) && !isset($_REQUEST['youtube_url']))
		{
			$status=0;
		}
	}// is new video

	if($status==0) 
	{
		$msg="Please select video or youtube url";
		$status=0;
	}else{

	global $base_url;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$vid_msg=" ";
		$e_text="";	$msg="";
		$e_title=$db->real_escape_string($_REQUEST['e_title']);	
		$video_url="";

if($is_new_video==1)
	{
		if(isset($_FILES["video_file"]["tmp_name"]))
		{
			$name = preg_replace( 
                     array("/\s+/", "/[^-\.\w]+/"), 
                     array("_", ""), 
                     trim($_FILES["video_file"]["name"])); 
			$path_parts = pathinfo($_FILES["video_file"]["name"]);
			$extension = $path_parts['extension'];
			$name=rand()."_vid.".$extension;
			$dir = "../uploads/media/";
			if(move_uploaded_file($_FILES["video_file"]["tmp_name"], $dir.$name))
			{
				$video_url=$base_url."/web/uploads/media/".$name;	
			}else{
				//$video_url=$base_url."/web/uploads/media/".$name;	
				$vid_msg="Video not uplaoded";
			}
			
			
		}/// if video uploded

		if(isset($_REQUEST['youtube_url']))
		{
			$video_url=$_REQUEST['youtube_url'];
		}
		
		$video_url=$db->real_escape_string($video_url);
}/// is new video


		$thumb_url="";

if($is_new_image==1)
{
		if(isset($_FILES["thumb_image"]["tmp_name"]))
				{
					$typ = $_FILES['thumb_image']['type'];
					$image_info = getimagesize($_FILES["thumb_image"]["tmp_name"]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];
					$path_parts = pathinfo($_FILES["thumb_image"]["name"]);
					$extension = $path_parts['extension'];
				    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream")
				        {
				              $uploaddir = "../uploads/editorial/";
				              $name=rand().".".$extension;
				              $uploadimages = $uploaddir.basename($name);
				            if(move_uploaded_file($_FILES['thumb_image']['tmp_name'], $uploadimages))
				            {
				              $thumb_url='uploads/editorial/'.$name;
				            }else{
				            	$thumb_url='';
				            	$msg="Not uploaded";
				            }
				      }
				    else
	                {
	                  $profile_image='images/img1.jpg';
	                  $msg="Invalid image type.";
	                }
		}//if thumb file uploaded

		$thumb_url=$db->real_escape_string($thumb_url);
}/// if image is new


$inskey="";
$insval="";
if($video_url!="")
{
	$inskey.=",featured_video_url='$video_url'";
	
}

if($thumb_url!="")
{
	$inskey.=",e_image='$thumb_url'";
	
}
$dt=date('Y-m-d H:i:s');
$ins_first="update editorial set updated_by='$uid',updated_at='$dt',e_title='$e_title',e_text='$e_text',status='0',is_video_only='1'".$inskey." WHERE e_id='$e_id'";

	/*	$ins_first="insert into editorial(e_title,e_text,e_image,featured_video_url,status,is_video_only,created_by) ";
		$ins_first.="  values('$e_title','$e_text','$thumb_url','$video_url','0','1','$uid')";*/
		
		
		$r=$db->query($ins_first);
		if($r==TRUE)
		{
			$status=1;
			$msg="update successfully.";
		}else{
			$status=0;
			$msg="Error in adding.";
		}
	}	
 }
}else{
	$status=0;
	//$data['post']=$_REQUEST;
	//$data['imgdata']=$_FILES;
	$msg=$error_msgs['invalid_argument'];

}

