<?php 

define("GOOGLE_API_KEY", "AIzaSyCsXaExP5FIix5kinj8RFNTHSBY-yHkUbc");
define("DEVELOPER_MODE", "false");

	function sendAndroidPushNotification($registatoin_ids, $message) 	
	{        
		//$url = 'https://android.googleapis.com/gcm/send';
		$url = 'https://fcm.googleapis.com/fcm/send';

		
		/*if(!isset($message['data']['badge_count']))
		{
			$message['data']['badge_count']=0;
		}*/

		$fields = array			
			(				
				'registration_ids' => $registatoin_ids,				
				'data' => $message,
				'priority' => "high"
			);
			
        $headers = array			
			(				
				'Authorization: key=' . GOOGLE_API_KEY,				
				'Content-Type: application/json'			
			);
			
        // Open connection        
		$ch = curl_init();        
		// Set the url, number of POST vars, POST data        
		curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);        
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);       
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly		
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);        
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
		// Execute post        
		$result = curl_exec($ch);
		
		//echo "result ---->".$result;		
		$result1 = json_decode($result);
		
		// Close connection        
		curl_close($ch);
		
		return $result;
	}

	function sendApplePushNotification($deviceToken, $message,$bodydata="")	
	{
		$ctx = stream_context_create();
		
		if(DEVELOPER_MODE=="true")
		{
			//echo "IF";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		else
		{
			//echo "ELSE";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates_live.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		
		if (!$fp)
			exit("Failed To Connect: $err - $errstr" . PHP_EOL);
		$badge=0;
		if(isset($bodydata['badge_count']))
		{
			$badge=$bodydata['badge_count'];
		}

		$body['aps'] = array('alert' => $message,'sound' => 'default','badge'=>$badge,'content-available'=>1);
			$body['data']=$bodydata;
		
		$payload = json_encode($body);//,JSON_UNESCAPED_UNICODE

		$payload = str_replace("\u", "u", $payload);
		
		//$payload = array_map('htmlentities', $body);
		//$payload = json_encode($body);
		//$payload = html_entity_decode($payload);
		//$payload = json_encode($body,JSON_UNESCAPED_UNICODE);		
		//$payload = str_replace("/\\u", "u", $payload);
		//$payload = preg_replace_callback('/\\\\u([0-9a-f]{4})/i', function($matches) {return mb_convert_encoding(pack('H*', $matches[1]), 'UTF-8', 'UTF-16');}, $payload);

		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
		
		$result = fwrite($fp, $msg, strlen($msg));
		
		//echo $result."<br>";
		
		$res_val=0;			
		if (!$result)			
			$res_val=0;
		else			
			$res_val=1;	
			
		fclose($fp);		
		
		return $res_val;
	}

	function sendApplePushNotification_arr($deviceToken, $message)	
	{
		$ctx = stream_context_create();
		
		if(DEVELOPER_MODE=="true")
		{
			//echo "IF";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'Certificates.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		else
		{
			//echo "ELSE";

			stream_context_set_option($ctx, 'ssl', 'local_cert', 'CertificatesLive.pem');
			stream_context_set_option($ctx, 'ssl', 'passphrase', 'hello_123');
			$fp = stream_socket_client( 'ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		}
		
		if (!$fp)
			exit("Failed To Connect: $err - $errstr" . PHP_EOL);
		
		foreach($deviceToken as $device_id) 
		{

			$body['aps'] = array('alert' => $message,'sound' => 'default','badge'=>1,'content-available'=>1);
			$body['data']=array();
			
			$payload = json_encode($body);		
			
			$msg = chr(0) . pack('n', 32) . pack('H*', $device_id) . pack('n', strlen($payload)) . $payload;
			
			$result = fwrite($fp, $msg, strlen($msg));
		}
		
		$res_val=0;			
		if (!$result)			
			$res_val=0;
		else			
			$res_val=1;	
			
		fclose($fp);		
		
		return $res_val;	
	}



function get_access_token($id)
{
	global $db;
	$token=bin2hex(openssl_random_pseudo_bytes(64));
	$db->query("update user set login_token='$token' where id='$id' ");
	return $token;
}// get access token

function send_email($to,$msg,$subject)
{
	include 'smtp/class.phpmailer.php';
  $mail = new PHPMailer;
  $mail->IsSMTP(); // telling the class to use SMTP
  $mail->Host = "smtp.gmail.com"; // SMTP server
  //$mail->SMTPDebug = 2;
  $mail->SMTPAuth = true; // turn on SMTP authentication
  $mail->Username = "simplywishesn@gmail.com"; // SMTP username
  $mail->Password = "tguhwslzqrebziuk"; //
  $mail->IsHTML(true);
  $mail->SMTPSecure = 'tls';//tls
  $mail->SetFrom('simplywishesn@gmail.com', 'SimplyWishes');
  $mail->AddReplyTo("simplywishesn@gmail.com","SimplyWishes");
  $mail->Subject = $subject;
  $mail->MsgHTML($msg);
  //$mail->AddAddress('nilesh@smart-webtech.com');
  $mail->AddAddress($to);
 if (!$mail->Send()) {
    /* Error */
  //  echo 'Message not Sent! ';
  } else {
    /* Success */
 //  echo 'Sent Successfully! <b> Check your Mail</b>';
  }
}

function validate_token($token,$uid)
{
	global $db;
	$result=$db->query("select id from user where id='$uid' and login_token='$token' LIMIT 1");
	if($result->num_rows>0)
		return 1;
	else
		return 0;
}

function check_block_content($content_type,$id)
{
	global $db;
	$result=$db->query("select blocked from report_content where report_type='$content_type' AND content_id='$id' AND blocked='1' ");
	if($result->num_rows>0)
	{
		return 1;
	}else{
		return 0;
	}
}

function get_unread_msg_total($uid)
{
	global $db;
	$result=$db->query("select m_id from messages where recipient_id='$uid' AND read_text='0' AND delete_status!='1'");
	return $result->num_rows;
}

function get_unread_friend_msg_total($uid,$friend_id)
{
	global $db;
	$result=$db->query("select m_id from messages where sender_id='$friend_id' AND recipient_id='$uid' AND read_text='0' AND delete_status!='1' ");
	return $result->num_rows;
}

function get_user_data($uid)
{
	global $db;
	global $base_url_image;
	global $base_url_storyimage;
	$data=array();
	$result_user=$db->query("select username,email,id from user where id='$uid'")or die($db->error);
	if($result_user->num_rows>0)
	{	
		$row_user=$result_user->fetch_assoc();
		$data['user']=$row_user;

		$result_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='$uid'")or die($db->error);
		$row_user_profile=$result_profile->fetch_assoc();
		$row_user_profile['profile_image_name']=$row_user_profile['profile_image'];
		$row_user_profile['profile_image']=$base_url_image.$row_user_profile['profile_image'];
		
		$data['user_profile']=$row_user_profile;

		$arr_story=array();
		$result_happy_story=$db->query("select hs_id,user_id,story_text,story_image,status,created_at from happy_stories where user_id='$uid'")or die($db->error);
		if($result_happy_story->num_rows>0)
		{
			while($row_happy_story=$result_happy_story->fetch_assoc())
			{
				$row_happy_story['story_image']=$base_url_storyimage.$row_happy_story['story_image'];
				$result_likes=$db->query("select count(a_id) as likes from story_activities where story_id='".$row_happy_story['hs_id']."'");
				$row_happy_story['likes']=0;
				if($result_likes->num_rows>0)
				{
					$row_likes=$result_likes->fetch_assoc();
					$row_happy_story['likes']=$row_likes['likes'];
				}
				array_push($arr_story, $row_happy_story);
			}
		}
		$data['my_happy_story']=$arr_story;

		$arr_followers=array();
		$result_follower=$db->query("select requested_by as uid from follow_request where requested_to='$uid' AND status='0'")or die($db->error);
		if($result_follower->num_rows>0)
		{

			while($row_follower=$result_follower->fetch_assoc())
			{
				$result_folower_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='".$row_follower['uid']."' LIMIT 1")or die($db->error);
				$row_folower_profile=$result_folower_profile->fetch_assoc();
				$row_folower_profile['profile_image']=$base_url_image.$row_folower_profile['profile_image'];
				array_push($arr_followers, $row_folower_profile);
			}
		}
		$data['follower']=$arr_followers;

		$arr_friends=array();
		$result_friends=$db->query("select requested_to as uid from follow_request where requested_by='$uid' AND status='0'")or die($db->error);
		if($result_friends->num_rows>0)
		{
				while ($row_friend=$result_friends->fetch_assoc()) {
						$result_friend_profile=$db->query("select profile_id,firstname,lastname,about,country,state,city,profile_image from user_profile where user_id='".$row_friend['uid']."' LIMIT 1")or die($db->error);
						$result_friend_profile=$result_friend_profile->fetch_assoc();
						$result_friend_profile['profile_image']=$base_url_image.$result_friend_profile['profile_image'];
						array_push($arr_friends, $result_friend_profile);		
				}
		}
		$data['friends']=$arr_friends;		



	}// if condition
	return $data;

}

?>