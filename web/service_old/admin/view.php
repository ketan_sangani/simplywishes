<?php include "header.php"; 

if(isset($_GET['id']))
{
	$id=$db->real_escape_string($_GET['id']);
	$result=$db->query("select * from report_content where id='$id' LIMIT 1");
	if($result->num_rows<=0)
	{
		echo "<script>window.location.href='home.php';</script>";
    	die();
	}
}else{
	echo "<script>window.location.href='home.php';</script>";
    die();
}

function get_user($id)
{
	global $db;
	$result=$db->query("select * from user where id='$id' LIMIT 1");
	$row=$result->fetch_object();
	return $row;
}

$row=$result->fetch_object();

$user_report=get_user($row->report_user);
	$user_reported=get_user($row->reported_user);


$content_type_arr=array("article"=>"Article","article_comment"=>"Article Comment","wish"=>"Wish","wish_comment"=>"Wish Comment","message"=>"Message","user"=>"User");
$content_type=$row->report_type;
$content_id=$row->content_id;

if($content_type=="article")
		{
			$sql="select * from editorial where e_id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->e_title;

			}else{
				$content_msg="";
			}
		}else if($content_type=="article_comment")
		{
			$sql="select * from editorial_comments where e_comment_id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->comments;
			}else{
				$content_msg="";
			}
		}else if($content_type=="wish")
		{
			$sql="select * from wishes where w_id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->wish_title;
			}else{
				$content_msg="";
			}
		}else if($content_type=="wish_comment")
		{
			$sql="select * from wish_comments where w_comment_id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->comments;
			}else{
				$content_msg="";
			}
		}else if($content_type=="message")
		{
			$sql="select * from messages where m_id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->text;
			}else{
				$status=0;
				$content_msg="";
			}
		}else if($content_type=="user")
		{
			$sql="select * from user where id='$content_id' LIMIT 1";
			$result=$db->query($sql);
			if($result->num_rows>0)
			{
				$row1=$result->fetch_object();
				$content_msg=$row1->username;
			}else{
				$status=0;
				$content_msg="";
			}
		}else{
			$status=0;
			$content_msg="";
		}

?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			  <!-- Default panel contents -->
			  <div class="panel-heading"> <a href="home.php" class='btn btn-primary' >Back</a> </div>
			  <div class="panel-body">
			  	  	<table class="table">
					    <tbody>
					    		<tr>
					    			<th>Date</th>
					    			<td><?php echo date("m/d/Y h:i A",strtotime($row->date)); ?></td>
					    		</tr>
					    		<tr>
					    			<th>Content Owner</th>
					    			<td><?php echo $user_reported->username; ?></td>
					    		</tr>
					    		<tr>
					    			<th>Reporting User</th>
					    			<td><?php echo $user_report->username; ?></td>
					    		</tr>
					    		<tr>
					    			<th>Content Type</th>
					    			<td><?php echo $content_type_arr[$row->report_type]; ?></td>
					    		</tr>
					    		<tr>
					    			<th>Comment</th>
					    			<td><?php echo $row->comment; ?></td>
					    		</tr>
					    		<tr>
					    			<th>Content</th>
					    			<td><?php echo $content_msg; ?></td>
					    		</tr>
					    		<tr>
					    			<th></th>
					    			<td></td>
					    		</tr>
					    		<tr>
					    			<th></th>
					    			<td></td>
					    		</tr>

					    </tbody>
					</table>
			  </div><!-- panel-body-->

			  <!-- Table -->
			  
			</div><!-- panel-->
		</div><!-- col -->
	</div><!-- row-->
</div><!-- container-->
<?php include "footer.php"; ?>
<script type="text/javascript">
	
	



	$(document).on("click",".block_content",function(){
		var id=$(this).data("id");
		var ele=$(this);
		var r=confirm("Are you sure want to block this content ?");
		if(r)
		{
			$.ajax({
				url:'ajax_block_content.php',
				data:{id:id},
				type:'POST',
				success:function(data){
					
				},
				error:function(data){
					alert("Error");
				}
			});// ajax call	
		}
		
	});/// block conent button click
</script>