<?php include "../conn.php";
session_start();


if(isset($_SESSION['login_token']))
{
    echo "<script>window.location.href='home.php';</script>";
    die();
}

$msg="";
if(isset($_POST['btn-login']))
{
        $username=$db->real_escape_string($_POST['username']);
        $password=md5($db->real_escape_string($_POST['password']));
        $sql="select * from admin where username='$username' AND password='$password' LIMIT 1";
        $result=$db->query($sql);
        if($result->num_rows>0)
        {
            $row=$result->fetch_object();
            $_SESSION['login_token']=md5($row->id);
            $_SESSION['uid']=$row->id;
            echo "<script>window.location.href='home.php';</script>";
            die();
        }else{
            $msg="<div class='alert alert-danger'>Please enter valid username/password.</div>";
        }
}
 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
                <div class="row justify-content-center" style="margin-top: 10%;">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post">
                            <?php echo $msg; ?>
                            <div class="form-group row">
                                <label for="username" class="col-md-4 control-label">Username</label>
                                <div class="col-md-6">
                                    <input type="text" id="username" class="form-control" name="username" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 control-label">Password</label>
                                <div class="col-md-6">
                                    <input type="password" id="password" class="form-control" name="password" required>
                                </div>
                            </div>

                          

                            <div class="col-md-6 offset-md-4">
                                <button type="submit" name="btn-login" value="1" class="btn btn-primary">
                                    Login
                                </button>
                               
                            </div>
                    
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
