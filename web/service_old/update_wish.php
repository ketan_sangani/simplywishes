<?php 

if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['wish_title']) && isset($_REQUEST['wish_desc']) && isset($_REQUEST['wish_expected_date']) && isset($_REQUEST['non_pay_option'])  && isset($_REQUEST['wish_show_mail_status']) && isset($_REQUEST['wish_show_mail']) && isset($_REQUEST['wish_show_person_status']) && isset($_REQUEST['wish_image_type']) && isset($_REQUEST['wish_show_person_street']) && isset($_REQUEST['wish_show_person_country']) && isset($_REQUEST['wish_show_person_city']) && isset($_REQUEST['wish_show_person_state']) && isset($_REQUEST['wish_show_person_zip']) && isset($_REQUEST['wish_show_other_status']) && isset($_REQUEST['wish_show_other_specify']) && isset($_REQUEST['wish_who_can']) && isset($_REQUEST['wish_status']) && isset($_REQUEST['w_id']) )
{
	global $base_url;
	global $error_msgs;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	$w_id=$db->real_escape_string($_REQUEST['w_id']);
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
	}else{

	
 	$wish_description=$db->real_escape_string($_REQUEST['wish_desc']);
 	$wish_status=$db->real_escape_string($_REQUEST['wish_status']);

 	$wish_title=$db->real_escape_string($_REQUEST['wish_title']);
 	$wish_desc=$db->real_escape_string($_REQUEST['wish_desc']);
 	$wish_expected_date=$db->real_escape_string($_REQUEST['wish_expected_date']);
 	
 	$non_pay_option=$db->real_escape_string($_REQUEST['non_pay_option']);
 	$wish_expected_cost=$db->real_escape_string($_REQUEST['wish_expected_cost']);
 	

 	$wish_show_mail_status=$db->real_escape_string($_REQUEST['wish_show_mail_status']);
 	$wish_show_mail=$db->real_escape_string($_REQUEST['wish_show_mail']);
 	$wish_show_person_status=$db->real_escape_string($_REQUEST['wish_show_person_status']);

 	$wish_image_type=$db->real_escape_string($_REQUEST['wish_image_type']);
 	//$wish_image=$db->real_escape_string($_REQUEST['wish_image']);

 	$wish_show_person_street=$db->real_escape_string($_REQUEST['wish_show_person_street']);
 	$wish_show_person_country=$db->real_escape_string($_REQUEST['wish_show_person_country']);
 	$wish_show_person_city=$db->real_escape_string($_REQUEST['wish_show_person_city']);
 	$wish_show_person_state=$db->real_escape_string($_REQUEST['wish_show_person_state']);
 	$wish_show_person_zip=$db->real_escape_string($_REQUEST['wish_show_person_zip']);

 	$wish_show_other_status=$db->real_escape_string($_REQUEST['wish_show_other_status']);
 	$wish_show_other_specify=$db->real_escape_string($_REQUEST['wish_show_other_specify']);
 	$wish_who_can=$db->real_escape_string($_REQUEST['wish_who_can']);

 	//$i_agree_decide=$db->real_escape_string($_REQUEST['i_agree_decide']);
 	//$i_agree_decide2=$db->real_escape_string($_REQUEST['i_agree_decide2']);
 	
 	$is_new_image=$_REQUEST['is_new_image'];
 		

 		$ins="update wishes set wish_title='$wish_title',wish_description='$wish_description',expected_cost='$wish_expected_cost',expected_date='$wish_expected_date',who_can='$wish_who_can',non_pay_option='$non_pay_option',wish_status='$wish_status',show_mail_status='$wish_show_mail_status',show_mail='$wish_show_mail',show_person_status='$wish_show_person_status',show_person_street='$wish_show_person_street',show_person_city='$wish_show_person_city',show_person_state='$wish_show_person_state',show_person_zip='$wish_show_person_zip',show_person_country='$wish_show_person_country',show_other_status='$wish_show_other_status',show_other_specify='$wish_show_other_specify' where w_id='$w_id'";
 		$r=$db->query($ins);
 		$debug="";
 		if($r==TRUE)
 		{
 			
 			$wid=$w_id;	
 			$status=1;
 			$msg="Your wish updated successfully.";

 			if($is_new_image==1)
 			{
 				$wish_image="";
 			if($wish_image_type=="custom")
		 	{

		 		if(isset($_FILES["wish_image"]["tmp_name"]))
				{
					$debug.=" is iset -";
					$typ = $_FILES['wish_image']['type'];
					$debug.=" tpye -".$typ;
					$image_info = getimagesize($_FILES["wish_image"]["tmp_name"]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];
					$path_parts = pathinfo($_FILES["wish_image"]["name"]);
					$extension = $path_parts['extension'];
				    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream")
				        {
				              $uploaddir = "../uploads/wishes/";
				              $name=$wid.".".$extension;
				              $uploadimages = $uploaddir.basename($name);
				            if(move_uploaded_file(strip_tags($_FILES['wish_image']['tmp_name']), $uploadimages))
				            {
				              $wish_image='uploads/wishes/'.$name;
				              $debug.=" uploaded -".$name;
				            }else{
				            	$wish_image='images/wish_default/1.jpg';
				            	$msg="Not uploaded";
				            	$debug.=" Not upload -".$msg;
				            }
				      }
				      else
	                {
	                  $profile_image='images/img1.jpg';
	                  $msg="Invalid image type.";
	                  $debug.=" invalid type -";
	                }
				}else{
					$wish_image='images/wish_default/1.jpg';
					$msg="Not set as file";
					$debug.=" image not selected for upload  wish_image-".$name;
				}

		 		/*$image_parts = explode(";base64,", $wish_image);
			    $image_type_aux = explode("image/", $image_parts[0]);
			    $image_type = $image_type_aux[1];
			    $image_base64 = base64_decode($image_parts[1]);
			    $file ='../uploads/wishes/'.$wid. '.' .'jpg';
			    file_put_contents($file, $image_base64);
			    
			    $wish_image='uploads/wishes/'.$wid. '.' .'jpg';*/

		       /* list($type, $data) = explode(';', $data);
		        list(, $data)      = explode(',', $data);
		        $data = base64_decode($data);
		        file_put_contents('../uploads/wishes/'.$wid. '.' .'jpg', $data);
		        $wish_image='uploads/wishes/'.$wid. '.' .'jpg';*/
		     }else{
		     	//$wish_image=$wish_image;
		     	$wish_image=$db->real_escape_string($_REQUEST['wish_image']);
		     }

		     $db->query("update wishes set primary_image='$wish_image' where w_id='$wid'");
		 }// is new image

 		}else{
 			$status=0;
 			$msg="Error in update wish".$db->error;
 		}
 		
 	}//if token not expired


}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

?>