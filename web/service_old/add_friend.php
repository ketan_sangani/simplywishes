<?php 
global $error_msgs;
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['friend_id']) && isset($_REQUEST['follow']))
{

	global $base_url;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$friend_id=$db->real_escape_string($_REQUEST['friend_id']);	
		$follow=$db->real_escape_string($_REQUEST['follow']);	

		if($follow==1)
			$ins_first="insert into follow_request(requested_by,requested_to,status) values('$uid','$friend_id','0')";
		else
			$ins_first="update follow_request set status='1' where requested_by='$uid' AND  requested_to='$friend_id' ";

		$f="Unfollowed";
		if($follow==1)
			$f="Followed";


		if($f=="Followed")		
		{
			$sql_uid="select username,email from user where id='$uid' LIMIT 1";
			$sql_friend="select username,email from user where id='$friend_id' LIMIT 1";

			$result_uid=$db->query($sql_uid);
			$result_friend=$db->query($sql_friend);

			$row_uid=$result_uid->fetch_object();
			$row_friend=$result_friend->fetch_object();

				$result_email_content=$db->query("select * from mail_content where m_id='17' LIMIT 1");
				$row_email_content=$result_email_content->fetch_assoc();
				$userlink = $base_url."/account/profile?id=".$uid;
				$loginlink= $base_url."/site/login";
				$editmessage=$row_email_content['mail_message'];
				$editmessage = str_replace("##USERNAME##", nl2br($row_friend->username), $editmessage);		
				$editmessage = str_replace("##USERNAME2##", nl2br($row_uid->username), $editmessage);		
				$editmessage = str_replace("##FRIENDSLINK##", "<a href='$userlink'>$userlink</a>", $editmessage);
				$editmessage = str_replace("##LOGINLINK##", "<a href='$loginlink'>$loginlink</a>", $editmessage);
				send_email($row_friend->email,$editmessage,$row_email_content['mail_subject']);

		}

		$r=$db->query($ins_first);
		if($r==TRUE)
		{
			$status=1;
			$msg="$f successfully.";
		}else{
			$status=0;
			$msg="Error in $f.";
		}
	}	
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

