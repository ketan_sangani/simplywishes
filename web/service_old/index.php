<?php 
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
//ini_set('upload_max_filesize', 100M);
//ini_set('post_max_size', 100M);
include "conn.php";
include "function.php";
include "Security.php";
$s=new Security();
//echo $s->generatePasswordHash("456789");


date_default_timezone_set('America/Los_Angeles');
$action=$_REQUEST['action'];
$type="";
if (isset($_REQUEST['type'])){
    $type = $_REQUEST['type'];
}
$status=0;
$msg="";
$select_reported = "select reported_user from report_content where report_user = ";
$exclude_reported = "";
$base_url="https://www.simplywishes.com";
$base_url_image="https://www.simplywishes.com/web/uploads/users/";
$base_url_storyimage="https://www.simplywishes.com/web/";
$paypal_url="https://www.simplywishes.com/web/content/web/service/";
$data=array();
date_default_timezone_set('America/Los_Angeles');

if ($type == 'test'){
    switch ($action) {
        case 'test':
            include "test/test.php";
            break;
        default:
        $msg="Please provide valid action parameter.";
    }
} else {

    switch ($action) {
        case "login":
            include "login.php";// Login service 
            break;
        case "register":
            include "register.php";
            break;
        case "updateProfile":
            include "update_profile.php";
            break;
        case "getProfileImages":
            include "get_profile_images.php";
            break;
        case "getMyFriends":
            include "get_my_friends.php";
            break;
        case "addFriend":
            include "add_friend.php";
            break;
        case "getMyMessages": //todo
            include "get_my_messages.php";
            break;
        case "getMyMessagesConveration":
            include "get_my_messages_list.php";
            break;
        case "sendMessage":
            include "send_message.php";
            break;
        case "getCountryList":
            include "getAddressLocation.php";
            break;
        case "getStateList":
            include "getAddressLocation.php";
            break;
        case "getCityList":
            include "getAddressLocation.php";
            break;
        case "getMyWishList":
            include "get_my_wishes.php";
            break;
        case "getWishList":
            include "get_wish_list.php";
            break;
        case "getSingleWish":
            include "get_single_wish.php";
            break;
        case "likeWish":
            include "like_wish.php";
            break;
        case "addToFavWish":
            include "add_to_fav_wish.php";
            break;
        case "createWish":
            include "create_wish1.php";
            break;
        case "RemoveMessage":
            include "remove_message.php";
            break;
        case "getWishImages":
            include "get_wish_images.php";
            break;
        case "commentOnWish":
            include "commenton_wish.php";
            break;
        case "updateCommentOnWish":
            include "comment_on_wish_update.php";
            break;
        case "commentOnArticle":
            include "comment_on_article.php";
            break;
        case "updateCommentOnArticle":
            include "comment_on_article_update.php";
            break;
        case "getWishCommentList":
            include "comment_list_wish.php";
            break;
        case "removeWishComment":
            include "remove_comment_on_wish.php";
            break;
        case "likeWishComment":
            include "like_comment_on_wish.php";
            break;
        case "addArticle":
            include 'add_article.php';
            break;
        case "grantWish":
            include 'grant_wish.php';
            break;      
        case "verifyGrantWish":
            include 'verify_grant_wish.php';
            break;      
        case "addArticleVideo":
            include 'add_article_video.php';
            break;      
        case "getArticleList":
            include 'get_articles.php';
            break;
        case "getMyArticleList":
            include 'get_my_articles.php';
            break;
        case "LikeArticle":
            include 'like_article.php';
            break;
        case "getSingleArticle":
            include 'get_single_articles.php';
            break;
        case "getArticleCommentList":
            include 'comment_list_article.php';
            break;
        case "getUserDetail":
            include 'get_user_detail.php';
            break;
        case "updateProfileImage":
            include 'update_profile_img.php';
            break;
        case "updateProfilePassword":
            include 'update_profile_pass.php';
            break;
        case "updateProfileDetail":
            include 'update_profile_detail.php';
            break;
        case "getFriendsWishList":
            include 'get_friends_wishes.php';
            break;
        case "searchUser":
            include 'search_user.php';
            break;
        case "verify-granted":
            include 'verify_granted.php';
            break;
        case "fullfilled":
            include 'fullfilled.php';
            break;
        case "updateWish":
            include 'update_wish.php';
            break;
        case "UpdateArticle":
            include 'update_article.php';
            break;
        case "UpdateArticleVideo":
            include 'update_video_article.php';
            break;
        case "removeWish":
            include 'remove_wish.php';
            break;
        case "ForgotPassword":
            include 'forgot_password.php';
            break;
        case "GetBadgeCount":
            include 'get_badge_count.php';
            break;
        case "ReportContent":
            include 'report_content.php';
            break;
        case "debug":
            include "debug.php";
            break;
        case "test":
            include "test.php";
            break;
        default:
            $msg="Please provide valid action parameter.";
    }
}

header('Content-Type: application/json');
$arr['status']=$status;
$arr['msg']=$msg;
$arr['data']=$data;

echo json_encode($arr);



?>