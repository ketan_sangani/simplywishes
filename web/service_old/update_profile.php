<?php 

if(isset($_REQUEST['token']) && isset($_REQUEST['uid']))
{

	global $base_url;
	global $error_msgs;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{

		
		$pass=$db->real_escape_string($_REQUEST['password']);
	 	$firstname=$db->real_escape_string($_REQUEST['firstname']);
	 	$lastname=$db->real_escape_string($_REQUEST['lastname']);
	 	$about=$db->real_escape_string($_REQUEST['about']);
	 	$country=$db->real_escape_string($_REQUEST['country']);
	 	$state=$db->real_escape_string($_REQUEST['state']);
	 	$city=$db->real_escape_string($_REQUEST['city']);
	 	if(isset($_REQUEST['email']))
		{
	 		$email=$db->real_escape_string($_REQUEST['email']);
		}else{
			$email="";
		}
	 	
	 	$profile_image_type=$db->real_escape_string($_REQUEST['profile_image_type']);
	 	$profile_old_image=$db->real_escape_string($_REQUEST['profile_old_image']);
	 	//$profile_image=$db->real_escape_string($_REQUEST['profile_image']);
	 	if($profile_old_image=="")
	 	{
		 	if($profile_image_type=="custom")
			{
		 		if(isset($_FILES["profile_image"]["tmp_name"]))
				{
					$typ = $_FILES['profile_image']['type'];
					$image_info = getimagesize($_FILES["profile_image"]["tmp_name"]);
					$image_width = $image_info[0];
					$image_height = $image_info[1];
					$path_parts = pathinfo($_FILES["profile_image"]["name"]);
					$extension = $path_parts['extension'];
				    if($typ == "image/jpeg" || $typ =="image/jpg" ||  $typ =="image/png" ||  $typ =="image/gif" || $typ == "application/octet-stream" )
				        {
				              $uploaddir = "../uploads/users/";
				              $name=$uid.".".$extension;
				              $uploadimages = $uploaddir.basename($name);
				            if(move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadimages))
				            {
				              $profile_image=$name;
				            }else{
				            	$profile_image='images/img1.jpg';
				            	$msg="Not uploaded";
				            }
				      }
				      else
		                {
		                  $profile_image='images/img1.jpg';
		                  $msg="Invalid image type.";
		                }
				}/// if condition
			}/// profile image type
			else{
							$profile_image=$db->real_escape_string($_REQUEST['profile_image']);
			}
		}else{
				$profile_image=$profile_old_image;
		}
		$data['pass_msg']="-".$profile_old_image;
		$updated_at=date("Y-m-d H:i:s");
		if($pass!="")
		{
			$pass=$s->generatePasswordHash($pass);
		 	$authkey=$s->generateRandomString();
		 	$sql_upd="update user set auth_key='$authkey',password_hash='$pass' where id='$uid'";
		 	$db->query($sql_upd);
		 	$data['pass_msg']="Password Updated";
		}
	 	
	 	$sql="update user_profile set firstname='$firstname',lastname='$lastname',about='$about',country='$country',state='$state',city='$city',profile_image='$profile_image' where user_id='$uid' ";
	 	$db->query($sql)or die($db->error);
	 	$status=1;
	 	$msg="Profile updated successfully. ";
	 	$img_path=$base_url."/web/uploads/users/".$profile_image;
	 	$data['image_path']=$img_path;
	 	$data['profile_image_name']=$profile_image;

	 	$sql_uid="select * from user where id='$uid' LIMIT 1";
	 	$result_uid=$db->query($sql_uid);
	 	$row_uid=$result_uid->fetch_object();

		$result_email_content=$db->query("select * from mail_content where m_id='6' LIMIT 1");
		$row_email_content=$result_email_content->fetch_assoc();
		
		$loginlink= $base_url."/site/login";
		
		$editmessage=$row_email_content['mail_message'];
		$editmessage = str_replace("##USERNAME##", nl2br($row_uid->username), $editmessage);		
		$editmessage = str_replace("##PROFILE##", "<a href='$loginlink'>Profile</a>", $editmessage);	
		
		send_email($row_uid->email,$editmessage,$row_email_content['mail_subject']);


if(isset($_REQUEST['email']))
{
	if($email!="")
	{
	 	$check_mail="select email from user where id!='$uid' AND email='$email' LIMIT 1";
	 	$result_check_mail=$db->query($check_mail);

	 	if($result_check_mail->num_rows>0)
	 	{
	 		$msg=" Cannot update email. Already exits";
	 	}else{
	 		$db->query("update user set email='$email' where id='$uid'");
	 	}
	 }
}

	}///token is valid
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];


}

