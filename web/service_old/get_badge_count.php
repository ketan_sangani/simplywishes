<?php 
date_default_timezone_set('America/Los_Angeles');
if(isset($_REQUEST['token']) && isset($_REQUEST['uid'])) 
{
	global $base_url_image;
	global $base_url;
	global $error_msgs;
	global $db;
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$data['badge_count']=get_unread_msg_total($uid);
		$status=1;
		$msg="";
	}
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}