<?php 
global $error_msgs;
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['comment']) && isset($_REQUEST['w_id']))
{

	global $base_url;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$comment=$db->real_escape_string($_REQUEST['comment']);	
		$w_id=$db->real_escape_string($_REQUEST['w_id']);	
		$created_at=date('Y-m-d H:i:s');
		$r=$db->query("insert into wish_comments(parent_id,w_id,user_id,comments,status,created_at) values('0','$w_id','$uid','$comment','0','$created_at')");
		if($r==TRUE)
		{

			$wish_result=$db->query("select * from wishes where w_id='$w_id' LIMIT 1");
			$row_wish=$wish_result->fetch_object();

			$sql_uid="select username,email from user where id='$uid' LIMIT 1";
			$sql_wish_user="select username,email from user where id='$row_wish->wished_by' LIMIT 1";

			$result_uid=$db->query($sql_uid);
			$result_wish_user=$db->query($sql_wish_user);

			$row_uid=$result_uid->fetch_object();
			$row_wish_user=$result_wish_user->fetch_object();

			$result_email_content=$db->query("select * from mail_content where m_id='16' LIMIT 1");
			$row_email_content=$result_email_content->fetch_assoc();
			$wishlink = $base_url."wish/view?id=".$w_id;
			$loginlink= $base_url."/site/login";
			$editmessage=$row_email_content['mail_message'];
			$editmessage = str_replace("##USERNAME##", nl2br($row_wish_user->username), $editmessage);		
			$editmessage = str_replace("##USERNAME2##", nl2br($row_uid->username), $editmessage);	
			$editmessage = str_replace("##WISHTITLE##", nl2br($row_wish->wish_title), $editmessage);	
			$editmessage = str_replace("##COMMENTTEXT##", "<a href='$wishlink'>".nl2br($comment)."</a>", $editmessage);	

			$editmessage = str_replace("##CONNECT##", "<a href='$loginlink'>$loginlink</a>", $editmessage);
			send_email($row_wish_user->email,$editmessage,$row_email_content['mail_subject']);

		
		$data['user_image_base_url']=$base_url_image;
		$result_main_cat=$db->query("select c.w_comment_id,c.comments,c.created_at,u.user_id,u.firstname,u.lastname,u.profile_image from wish_comments c,user_profile u where c.w_id='$w_id' and c.user_id=u.user_id and c.status='0' and c.parent_id='0' order by c.created_at DESC");	
		$main_comments=array();
		while($row_main_comment=$result_main_cat->fetch_assoc())
		{
			$comment_likes_result=$db->query("select c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments_activities as c,user_profile u where c.user_id=u.user_id and c.w_comment_id='".$row_main_comment['w_comment_id']."'");
			$row_main_comment['likes']=$comment_likes_result->num_rows;
			$like_users_arr=array();
			while($row_comment_likes=$comment_likes_result->fetch_assoc())
			{
				array_push($like_users_arr, $row_comment_likes);
			}// while loop
			$row_main_comment['like_users']=$like_users_arr;

			$result_sub_cat=$db->query("select c.w_comment_id,c.comments,c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments c,user_profile u where c.w_id='$w_id' and c.status='0' and c.parent_id='".$row_main_comment['w_comment_id']."' and c.user_id=u.user_id order by c.created_at DESC");	
			$sub_comments=array();
			while($row_sub_comment=$result_sub_cat->fetch_assoc())
			{
				$comment_sub_likes_result=$db->query("select c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments_activities as c,user_profile u where c.user_id=u.user_id and c.w_comment_id='".$row_sub_comment['w_comment_id']."'");
				$row_sub_comment['likes']=$comment_sub_likes_result->num_rows;
				$sub_like_users_arr=array();
				while($row_sub_comment_likes=$comment_sub_likes_result->fetch_assoc())
				{
					array_push($sub_like_users_arr, $row_sub_comment_likes);
				}// while loop
				$row_sub_comment['like_users']=$sub_like_users_arr;
				array_push($sub_comments, $row_sub_comment);
			}// while loop
			$row_main_comment['sub_comments']=$sub_comments;
			array_push($main_comments, $row_main_comment);
		}// while loop

		$data['comments']=$main_comments;

			$status=1;
			$msg="Comment added successfully.";
		}else{
			$status=0;
			$msg="Error in commenting.";
		}
	}	
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

