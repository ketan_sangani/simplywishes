<?php 
global $error_msgs;
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']) && isset($_REQUEST['w_id']))
{

	global $base_url;
	global $base_url_image;
	
	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$w_id=$db->real_escape_string($_REQUEST['w_id']);	
		
		$data['user_image_base_url']=$base_url_image;
		$result_main_cat=$db->query("select c.w_comment_id,c.comments,c.created_at,u.user_id,u.firstname,u.lastname,u.profile_image from wish_comments c,user_profile u where c.w_id='$w_id' and c.user_id=u.user_id and c.status='0' and c.parent_id='0' order by c.created_at DESC");	
		$main_comments=array();
		while($row_main_comment=$result_main_cat->fetch_assoc())
		{
			$comment_likes_result=$db->query("select c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments_activities as c,user_profile u where c.user_id=u.user_id and c.w_comment_id='".$row_main_comment['w_comment_id']."'");
			$row_main_comment['likes']=$comment_likes_result->num_rows;
			$like_users_arr=array();
			while($row_comment_likes=$comment_likes_result->fetch_assoc())
			{
				array_push($like_users_arr, $row_comment_likes);
			}// while loop
			$row_main_comment['like_users']=$like_users_arr;

			$result_sub_cat=$db->query("select c.w_comment_id,c.comments,c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments c,user_profile u where c.w_id='$w_id' and c.status='0' and c.parent_id='".$row_main_comment['w_comment_id']."' and c.user_id=u.user_id order by c.created_at DESC");	
			$sub_comments=array();
			while($row_sub_comment=$result_sub_cat->fetch_assoc())
			{
				$comment_sub_likes_result=$db->query("select c.created_at,u.firstname,u.lastname,u.profile_image from wish_comments_activities as c,user_profile u where c.user_id=u.user_id and c.w_comment_id='".$row_sub_comment['w_comment_id']."'");
				$row_sub_comment['likes']=$comment_sub_likes_result->num_rows;
				$sub_like_users_arr=array();
				while($row_sub_comment_likes=$comment_sub_likes_result->fetch_assoc())
				{
					array_push($sub_like_users_arr, $row_sub_comment_likes);
				}// while loop
				$row_sub_comment['like_users']=$sub_like_users_arr;
				$block=check_block_content("wish_comment",$row_sub_comment['w_comment_id']);
			
				if($block!=1)
					array_push($sub_comments, $row_sub_comment);
			}// while loop

			$row_main_comment['sub_comments']=$sub_comments;

			$block=check_block_content("wish_comment",$row_main_comment['w_comment_id']);
			
			if($block!=1)
				array_push($main_comments, $row_main_comment);
		}// while loop

		$data['comments']=$main_comments;		
		$status=1;
	}	
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];
}

