<?php
	require_once 'conn.php';
?>

<html>
	<head>
		<title>View Error Report</title>
	</head>
	<body>
		<table width="100%" border="1">
			<thead>
				<tr>
					<th>#</th>
					<th>Phone Type</th>
					<th>Device Info</th>
					<th>Screen Name</th>
					<th>Error Details</th>
					<th>Extra Details</th>
					<th>GMT DATE</th>
					<th>GMT TIME</th>
				</tr>
			</thead>
			<tbody>
				<?php
					$cnt=0;
					$res=$db->query("SELECT * FROM debug_app_data ORDER BY id desc");
					while($row=$res->fetch_object())
					{
						$cnt++;
				?>
						<tr>
							<td><?=$cnt?></td>
							<td><?=$row->app_type?></td>
							<td><?=$row->device_info?></td>
							<td><?=$row->screen_name?></td>
							<td><?=$row->error_details?></td>
							<td><?=$row->extra_data?></td>
							<td><?=$row->report_date?></td>
							<td><?=$row->report_time?></td>
						</tr>	
				<?php		
					}
				?>
			</tbody>
			
		</table>
	</body>
</html>	