<?php 
global $base_url;
global $paypal_url;
global $base_url_image;
global $error_msgs;
$uid="";
$user_id="";
$friend_query="";
$sql="";
if(isset($_REQUEST['token']) && isset($_REQUEST['uid']))
{

	$token=$db->real_escape_string($_REQUEST['token']);
 	$uid=$db->real_escape_string($_REQUEST['uid']);
 	if(isset($_REQUEST['user_id']))
 		$user_id=$db->real_escape_string($_REQUEST['uid']);
 	
	if(validate_token($token,$uid)==0)
	{
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$uid="";
		$uid=" AND user_id='".$_REQUEST['uid']."'";

			if(isset($_REQUEST['is_friend']))
			{
				$fq=$db->real_escape_string($_REQUEST['is_friend']);

				if($fq==1)
				{
					$friend_query=" w.wished_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) ";
				}
			}
			$search_text_q="";
			if(isset($_REQUEST['search_text']))
			{
				$search_text=$_REQUEST['search_text'];
				$search_text_q=" AND w.wish_title LIKE '%$search_text%' ";
			}

			if(isset($_REQUEST['filter']))
			{
				$filter=$db->real_escape_string($_REQUEST['filter']);
				if($filter=="popular")
				{
					if($friend_query!=""){ $friend_query=" WHERE w.wish_status='0' AND ".$friend_query; }else{ $friend_query=" WHERE w.wish_status='0' "; }
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  $friend_query $search_text_q order by total_like DESC";
				}else if($filter=="financial")
				{
					if($friend_query!=""){ $friend_query=" AND  ".$friend_query; }
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where w.non_pay_option='0' $friend_query AND w.wish_status='0' $search_text_q ORDER BY w.w_id DESC ";
				}else if($filter=="non-financial")
				{
					if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where w.non_pay_option='1' AND w.wish_status='0' $friend_query $search_text_q ORDER BY w.w_id DESC ";
				}else if($filter=="granted")
				{
					if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
					//$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where w.granted_by IS NOT NULL AND w.wish_status='0' $friend_query $search_text_q ORDER BY w.w_id DESC ";

					$sql="SELECT *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like,str_to_date(w.granted_date, '%m-%d-%Y') as mydt from wishes w  where w.granted_by IS NOT NULL AND w.wish_status='0' $friend_query $search_text_q ORDER BY mydt DESC ";
				}else if($filter=="in_progress")
				{
					if($friend_query!=""){ $friend_query=" AND ".$friend_query; }
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where w.process_status='1' AND w.wish_status='0' $friend_query $search_text_q ORDER BY w.date_updated DESC";

				}else if($filter=="friends" && $user_id!=""){
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w where w.wished_by IN(select f.requested_to from follow_request f where f.requested_by='".$user_id."' AND status='0' ) AND w.wish_status='0' $search_text_q ORDER BY w.w_id DESC";
				}else{
					if($friend_query!=""){ $friend_query=" WHERE w.wish_status='0' AND w.granted_by IS NULL AND ".$friend_query; }else{
						$friend_query=" WHERE w.wish_status='0' AND w.granted_by IS NULL ";
					}
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w $friend_query AND w.process_status!='1' $search_text_q ORDER BY w.w_id DESC";
				}
			}else{
					if($friend_query!=""){ $friend_query=" WHERE ".$friend_query; }else{ $friend_query=" WHERE w.wish_status='0'  "; }
					$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w   $friend_query  $search_text_q  AND w.process_status!='1' AND w.granted_by IS NULL ORDER BY w.w_id DESC";
			}



			$result1=$db->query($sql)or die($db->error);
			$total=$result1->num_rows;
			$data['total']=$total;
			$data['start']=0;
			$limit=10;
			if(isset($_REQUEST['limit']))
			{
				$limit=$_REQUEST['limit'];

			}
			$data['limit']=$limit;
			if(isset($_REQUEST['start']))
			{
				$start=$_REQUEST['start'];
				$data['start']=$start;
				$sql=$sql." LIMIT $start,$limit";
			}


			$result=$db->query($sql)or die($db->error);
			$arr_wish=array();
			while($row=$result->fetch_assoc())
			{

				$result_user=$db->query("select * from user_profile where user_id='".$row['wished_by']."' LIMIT 1");
				$row_user=$result_user->fetch_assoc();

				$result_fav=$db->query("select a_id from activities where wish_id='".$row['w_id']."' $uid and activity='fav'");
				
				$result_my_like=$db->query("select * from activities where wish_id='".$row['w_id']."' AND activity='like'  $uid");

				$single_wish=array();
				$single_wish['w_id']=$row['w_id'];
				$single_wish['wish_title']=$row['wish_title'];
				$single_wish['description']=$row['wish_description'];
				$single_wish['wish_image']=$row['primary_image'];
				$single_wish['date']=$row['date_updated'];
				$single_wish['date_updated']=date('m-d-Y',strtotime($row['date_updated']));
				$single_wish['firstname']=$row_user['firstname'];
				$single_wish['lastname']=$row_user['lastname'];
				$single_wish['wished_by']=$row['wished_by'];

				$single_wish['user_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
				$single_wish['likes']=$row['total_like'];
				if($uid!="")
					$single_wish['my_like']=$result_my_like->num_rows;
				else
					$single_wish['my_like']=0;

				$single_wish['in_progress']=$row['process_status'];
				if($uid!="")
					$single_wish['favorite']=$result_fav->num_rows;
				else
					$single_wish['favorite']=0;

				$single_wish['granted_by']=$row['granted_by'];
				$single_wish['expected_date']=$row['expected_date'];
				$single_wish['non_pay_option']=$row['non_pay_option'];

				if($row['non_pay_option']==1)
				{
					$single_wish['show_mail_status']=$row['show_mail_status'];
					$single_wish['show_mail']=$row['show_mail'];
					$single_wish['show_person_status']=$row['show_person_status'];
					$single_wish['show_person_street']=$row['show_person_street'];
					$single_wish['show_person_city']=$row['show_person_city'];
					$single_wish['show_person_state']=$row['show_person_state'];
					$single_wish['show_person_zip']=$row['show_person_zip'];
					$single_wish['show_person_country']=$row['show_person_country'];
					$single_wish['show_other_status']=$row['show_other_status'];
					$single_wish['show_other_specify']=$row['show_other_specify'];
				}else{
					$single_wish['paypal_url']=$paypal_url."/web/service/pay.php?w_id=".$single_wish['w_id']."&uid=".$user_id."&token=".md5(rand());
				}

				$single_wish['share_url']=$base_url."/wish/view?id=".$single_wish['w_id'];

				$block=check_block_content("wish",$single_wish['w_id']);
				if($block!=1)
					array_push($arr_wish, $single_wish);

			}		
			$status=1;
			$data['wish_image_base_url']=$base_url."/web/";
			$data['wish_list']=$arr_wish;
			$data['q']=$sql;
	}// if token is valid
}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

