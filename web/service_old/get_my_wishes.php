<?php 
global $base_url;
global $base_url_image;
global $error_msgs;
$uid="";
$user_id="";

$search_text_q="";

if(isset($_REQUEST['uid']) && isset($_REQUEST['token']))
{
	$user_id=$db->real_escape_string($_REQUEST['uid']);
	$token=$db->real_escape_string($_REQUEST['token']);

	// follow this template to exclude content from reported user
	$exclude_reported = " AND w.wished_by not in ($select_reported $user_id) ";

	// if(validate_token($token,$user_id)==0)
	if(1==0)
	{
		
		$status=0;
		$msg=$error_msgs['invalid_token'];
		
	}else{
		$uid=" and user_id='".$user_id."'";

		$search_text_q = $exclude_reported;

		if(isset($_REQUEST['search_text']))
		{
			$search_text=$_REQUEST['search_text'];
			$search_text_q.=" AND w.wish_title LIKE '%$search_text%' ";
		}


		if(isset($_REQUEST['filter']))
		{
			$filter=$db->real_escape_string($_REQUEST['filter']);
			if($filter=="popular")
			{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w where wish_status='0' AND  w.wished_by='$user_id' $search_text_q order by total_like DESC";
			}else if($filter=="financial")
			{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where wish_status='0' AND w.wished_by='$user_id' $search_text_q and w.non_pay_option='1' ORDER BY w.w_id DESC ";
			}else if($filter=="non-financial")
			{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where wish_status='0' AND  w.wished_by='$user_id' $search_text_q and w.non_pay_option='0' ORDER BY w.w_id DESC ";
			}else if($filter=="granted")
			{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where wish_status='0' AND  w.wished_by='$user_id' $search_text_q and w.granted_by IS NOT NULL";
			}else if($filter=="in_progress")
			{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w  where wish_status='0' AND w.wished_by='$user_id' $search_text_q and w.process_status='1'";
			}else if($filter=="saved"){
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w where wish_status='0' AND  w.w_id IN(select wish_id from activities where user_id='".$user_id."' AND activity='fav' ) $search_text_q ORDER BY w.w_id DESC";
			}else{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w where wish_status='0' AND w.wished_by='$user_id' $search_text_q ORDER BY w.w_id DESC";
			}
		}else{
				$sql="select *,(select COUNT(a_id) from activities where wish_id=w.w_id and activity='like' ) as total_like from wishes w where w.wished_by='$user_id' AND wish_status='0' $search_text_q ORDER BY w.w_id DESC";
		}

//echo $sql;

		$result1=$db->query($sql)or die($db->error);
		$total=$result1->num_rows;
		$data['total']=$total;
		$data['start']=0;
		$limit=10;
		if(isset($_REQUEST['limit']))
		{
			$limit=$_REQUEST['limit'];

		}
		$data['limit']=$limit;
		if(isset($_REQUEST['start']))
		{
			$start=$_REQUEST['start'];
			$data['start']=$start;
			$sql=$sql." LIMIT $start,$limit";
		}


		$result=$db->query($sql)or die($db->error);
		$arr_wish=array();
		while($row=$result->fetch_assoc())
		{

			$result_user=$db->query("select * from user_profile where user_id='".$row['wished_by']."' LIMIT 1");
			$row_user=$result_user->fetch_assoc();

			$result_fav=$db->query("select a_id from activities where wish_id='".$row['w_id']."' $uid and activity='fav'");
			$result_my_like=$db->query("select * from activities where wish_id='".$row['w_id']."' AND activity='like'  $uid");
			
			$single_wish=array();
			$single_wish['w_id']=$row['w_id'];
			$single_wish['wish_title']=$row['wish_title'];
			$single_wish['wish_status']=$row['wish_status'];
			$single_wish['description']=$row['wish_description'];
			$single_wish['wish_image']=$row['primary_image'];
			$single_wish['date']=$row['date_updated'];
			$single_wish['date_updated']=date('m-d-Y',strtotime($row['date_updated']));
			$single_wish['firstname']=$row_user['firstname'];
			$single_wish['lastname']=$row_user['lastname'];

			$single_wish['user_image']=$base_url."/web/uploads/users/".$row_user['profile_image'];
			$single_wish['likes']=$row['total_like'];
			if($uid!="")
				$single_wish['my_like']=$result_my_like->num_rows;
			else
				$single_wish['my_like']=0;

			$single_wish['in_progress']=$row['process_status'];
			if($uid!="")
				$single_wish['favorite']=$result_fav->num_rows;
			else
				$single_wish['favorite']=0;



			
			$single_wish['in_progress']=$row['process_status'];
			$single_wish['non_pay_option']=$row['non_pay_option'];
			$single_wish['granted_by']=$row['granted_by'];
			$single_wish['expected_date']=$row['expected_date'];
			
			
				if($row['non_pay_option']==1)
			{
				$single_wish['show_mail_status']=$row['show_mail_status'];
				$single_wish['show_mail']=$row['show_mail'];
				$single_wish['show_person_status']=$row['show_person_status'];
				$single_wish['show_person_street']=$row['show_person_street'];
				$single_wish['show_person_city']=$row['show_person_city'];
				$single_wish['show_person_state']=$row['show_person_state'];
				$single_wish['show_person_zip']=$row['show_person_zip'];
				$single_wish['show_person_country']=$row['show_person_country'];
				$single_wish['show_other_status']=$row['show_other_status'];
				$single_wish['show_other_specify']=$row['show_other_specify'];
			}else{
				$single_wish['paypal_url']=$paypal_url."/web/service/pay.php?w_id=".$single_wish['w_id']."&uid=".$user_id."&token=".md5(rand());
			}

			$single_wish['share_url']=$base_url."/wish/view?id=".$single_wish['w_id'];


			array_push($arr_wish, $single_wish);
		}		
		$status=1;
		$data['wish_image_base_url']=$base_url."/web/";
		$data['wish_list']=$arr_wish;
		$data['wish_status_note']="1=draft,0=published";
	}///if token not expired

}else{
	$status=0;
	$msg=$error_msgs['invalid_argument'];

}

