import SendBirdChat from "./sendbird-chat-sdk-javascript-main/index.js";
import {
  GroupChannelModule,
  MessageCollectionInitPolicy,
  MessageFilter,
} from "./sendbird-chat-sdk-javascript-main/groupChannel.js";
import { createPicker } from "./emoji_picker.js";

// const appId = "876F8D88-DC88-4CEF-A2BF-FD2652B7422F";`
const appId = "6198EF20-7666-4AB7-B59D-4CDCB14F91BA";

const sendbird = SendBirdChat.init({
  appId,
  modules: [new GroupChannelModule()],
});

const userId = window.userId;

const user = await sendbird.connect(userId);

let allChannel;

const msg_history_div = document.getElementById("msg_history");
const type_msg = document.getElementById("type_msg");

const usersListMessaging = document.querySelector("#usersList");

if (usersListMessaging) {
  usersListMessaging.addEventListener("click", async (e) => {
    const userElement = e.target.closest(".userListName");
    const userId = userElement.id;
    $("#userChatBox").show();
    $("#userProfileDiv").show();
    var userImg = $("#userImg-" + userId).attr("src");
    var userName = $("#userName-" + userId).text();
    var userProfileId = $("#userName-" + userId).data("id");
    $("#userProfileImg").attr("src", "");
    $("#userProfileName").text(userName);
    $("#userNameChatBox").text(userName);
    $("#userProfileImg").attr("src", userImg);
    $("#userProfileUrl").attr(
      "href",
      baseUrl + "account/profile?id=" + userProfileId
    );
    if (userId == "" || user.userId == "") {
      alert("Something went wrong.");
      return false;
    }
    const params = {
      invitedUserIds: [userId],
      operatorUserIds: [user.userId, userElement.id],
      isDistinct: true,
    };
    try {
      const channel = await sendbird.groupChannel.createChannel(params);
      window.channel = channel;
      getChannels();
      initialization(channel.url);
    } catch (error) {
      console.error("Error creating channel:", error);
    }
  });
}

function appendMessage(message) {
  const date = new Date(message?.createdAt);
  const displayDate = date.toString();

  const hours = date.getHours();
  const minutes = date.getMinutes();
  const ampm = hours >= 12 ? "PM" : "AM";
  const formattedHours = hours % 12 === 0 ? 12 : hours % 12;
  const formattedMinutes = minutes < 10 ? "0" + minutes : minutes;
  const formattedTime = formattedHours + ":" + formattedMinutes + " " + ampm;

  const displayDateArr = displayDate.split(" ");

  const messageObjStr = JSON.stringify(message);

  let messageHtml;
  if (message?.sender?.userId === userId) {
    messageHtml = `
    <div class="outgoing_msg" id="msg_id_${message?.messageId}">
      <div class="outgoing_msg_img">
        <img src="${senderUserImg}">
      </div>
      <div class="sent_msg">
        <div style="display:${
          message.parentMessageId > 0 ? "block" : "none"
        };background: #094757;">
                  <div id="${
                    message?.parentMessage?.messageId
                  }" style="display:flex;align-item:center;padding:0px">
                      <i class="fa fa-reply" style="padding: 8px 0px;"></i>
                      <p style="
                          margin-bottom:0px;
                          margin-left: 5px;
                          overflow: hidden;
                          text-overflow: ellipsis;
                          white-space: nowrap;
                          padding: 5px 0px;">${
                        message?.parentMessage?.message
                      }</p>
                    </div>
                    <span class="time_date" style="color: whitesmoke;opacity: 0.6;">${
                      message?.parentMessage?.sender?.nickname
                    } , 
                    ${displayDateArr[1]} ${displayDateArr[2]} | ${formattedTime}
                    </span>
                </div>
        <div id="${
          message?.messageId
        }" style="display:flex;justify-content:space-between;align-items:center;">
          <p style="margin-bottom:0px;">${message?.message}</p>
          <div class="dropend">
                <span type="button" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-chevron-down menu-icon"></i>
                </span>
                <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                  <div class="form-check" style="margin-left: 10px;">
                      <p style="cursor:pointer;" onclick="replyMsg(
                      ${message.messageId},
                      '${message.sender?.nickname}',
                      ${message.createdAt},
                      '${escapeMessage(message.message)}')
                      ">
                          Reply
                      </p>
                  </div>
                  <div class="form-check" style="margin-left: 10px;">
                      <p style="cursor:pointer;">
                          Forward
                      </p>
                  </div>
                  <div class="form-check" style="margin-left: 10px;">
                      <p style="cursor:pointer;" onclick="copyMsg('${
                        message?.message
                      }')">
                          Copy
                      </p>
                  </div>
              
                  <div class="form-check" style="margin-left: 10px;">
                      <p style="cursor:pointer;" onclick="deleteMsg(${
                        message?.messageId
                      })">
                          Delete
                      </p>
                  </div>
              </div>
        </div>
      </div>
        <span class="time_date">${displayDateArr[1]} ${
      displayDateArr[2]
    } | ${formattedTime}</span>
      </div>
    </div>
  `;
  } else {
    messageHtml = `
    <div class="incoming_msg" id="msg_id_${message?.messageId}">
      <div class="received_msg">
            <div class="incoming_msg_img">
              <img src="${window.userImg}">
            </div>
            <div class="received_withd_msg">
                <div style="display:${
                  message.parentMessageId > 0 ? "block" : "none"
                };background: #d3d3d3;">
                  <div id="${
                    message?.parentMessage?.messageId
                  }" style="display:flex;align-item:center;padding:0px">
                      <i class="fa fa-reply" style="padding: 8px 0px;"></i>
                      <p style="
                          margin-bottom:0px;
                          margin-left: 5px;
                          overflow: hidden;
                          text-overflow: ellipsis;
                          white-space: nowrap;
                          padding: 5px 0px;">${
                        message?.parentMessage?.message
                      }</p>
                    </div>
                    <span class="time_date">${
                      message?.parentMessage?.sender?.nickname
                    } , 
                    ${displayDateArr[1]} ${displayDateArr[2]} | ${formattedTime}
                    </span>
                </div>
            <div id="${
              message?.messageId
            }" style="display:flex;justify-content:space-between;align-items:center;">
                    <p style="margin-bottom:0px;">${message?.message}</p>
                    <div class="dropend">
                          <span type="button" class="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                              <i class="fa fa-chevron-down menu-icon"></i>
                          </span>
                          <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                            <div class="form-check" style="margin-left: 10px;">
                                <p style="cursor:pointer;" onclick="replyMsg(
                                  ${message.messageId},
                                  '${message.sender?.nickname}',
                                  ${message.createdAt},
                                  '${escapeMessage(message.message)}')
                                  ">
                                      Reply
                                </p>
                            </div>
                            <div class="form-check" style="margin-left: 10px;">
                                <p style="cursor:pointer;">
                                    Forward
                                </p>
                            </div>
                            <div class="form-check" style="margin-left: 10px;">
                                <p style="cursor:pointer;" onclick="copyMsg('${
                                  message?.message
                                }')">
                                    Copy
                                </p>
                            </div>
                            <div class="form-check" style="margin-left: 10px;">
                                <p style="cursor:pointer;" onclick="reportMsg('${
                                  message?.message
                                }', '${message.messageId}','${
      message?.channelUrl
    }','${message?.sender?.userId}')">
                                    Report
                                </p>
                            </div>
                        </div>
                  </div>
                </div>
                <span class="time_date">${displayDateArr[1]} ${
      displayDateArr[2]
    } | ${formattedTime}</span>
            </div>
        </div>
    </div>
  `;
  }
  msg_history_div.insertAdjacentHTML("beforeend", messageHtml);
}

const channelList = document.querySelector(".inbox_chat");

window.defaultChatFilter = 'all';
window.chatFilter = (e) => {
  const value = e.value;
  window.defaultChatFilter = value;
  getChannels();
}
getChannels();

async function getChannels() {
  const groupChannelCollection =
    sendbird.groupChannel.createMyGroupChannelListQuery();
  groupChannelCollection.includeEmpty = true;

  channelList.innerHTML = `
    <p style="text-align:center;">Loading...</p>
  `;

  const channels = await groupChannelCollection.next();
  const chatFilter = window.defaultChatFilter;

  let filteredChannels = [];

  if(chatFilter === 'read'){
    filteredChannels = channels.filter((channel) => channel.unreadMessageCount === 0 );
  }else if(chatFilter === 'unread'){
    filteredChannels = channels.filter((channel) => channel.unreadMessageCount > 0 );
  }else{
    filteredChannels = channels;
  }

  allChannel = JSON.stringify(channels);

  if (filteredChannels.length !== channelList.childNodes.length) {
    channelList.innerHTML = `
      <p style="text-align:center;">Loading...</p>
    `;
  }

  channelList.innerHTML = "";
  if(filteredChannels && filteredChannels.length){
    filteredChannels.forEach(async (channel) => {
      if(channel.members && channel.members.length > 1){
        appendChannel(channel);
      }
      checkAndUpdateChannelListDiv();
    });
  }else{
    checkAndUpdateChannelListDiv();
  }

  $("#userChatBox").show();
  $("#rightChatDiv").hide();
  $("#type_msg").hide();
  
  msg_history_div.innerHTML = `
    <div class="message-card">
        <h3>No chat selected</h3>
        <p>Please select chat to see the messages...</p>
      </div>
  `;
}

function checkAndUpdateChannelListDiv(){ 
  if (channelList.children.length === 0) {
    channelList.innerHTML = `
      <div class="message-card">
        <h3>You have no messages.</h3>
        <p>You can start sending messages now.</p>
      </div>
    `;
  }
}


function appendChannel(channel) {
  const member = channel.members.find((member) => member.userId !== userId);
  var userImg = $("#userImg-" + member.userId).attr("src") ? $("#userImg-" + member.userId).attr("src") : member.plainProfileUrl;
  var userName = $("#userName-" + member.userId).text() ?? member.nickname;
  const nickname = member.nickname ? member.nickname : userName;
  let profileUrl = member.profileUrl ?? member.plainProfileUrl ?? userImg;
  const date = new Date(channel.myLastRead);
  const dateArr = date.toString().split(" ");

  let localProfileUrl = $("#userImg-" + member.userId).attr("src");

  const channelHtml = `
    <div class="chat_list" id="${channel.url}" >
      <div class="chat_people">
        <div class="chat_img"> <img src="${localProfileUrl ?? profileUrl}" alt="${
    nickname ? nickname : "Unknown"
  }"> </div>
        <div class="chat_ib">
          <h5 id="${nickname}">
          ${nickname ? nickname : "Unknown"}
          <span id="unreadmsgcount_${channel.url}" style="background-color: #25d366;
          padding: 5px 10px;
          border-radius: 50%;display:${
            channel.unreadMessageCount === 0 ? "none" : "block"
          }">${channel.unreadMessageCount}</span>
          <span class="chat_date" style="padding-top: 6px;
          padding-right: 6px;">
          ${dateArr[1]} ${dateArr[2]}

          <div class="btn-group dropend">
            <div style="position: relative;">
                <span type="button" onclick="stopPropagationHandler(event)" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-ellipsis-h"></i>
                </span>
                <div class="dropdown-menu dropdown-menu-end" style="width: 50px; height: auto; padding-left: 8px">
                    <div class="form-check" style="margin-left: 10px;" onclick="deleteChatFromLeft(${channel.url},event)">
                        <label class="form-check-label" for="flexRadioDefault1">
                            Delete Chat
                        </label>
                    </div>
                </div>
            </div>
          </div>
          </span>
          </h5>
          <p>${channel.lastMessage ? channel.lastMessage.message : ""}</p>
        </div>
      </div>
    </div>
  `;

  channelList.insertAdjacentHTML("beforeend", channelHtml);
}

window.stopPropagationHandler = (event) => {
  event.stopPropagation();
}

if (channelList) {
  channelList.addEventListener("click", async (e) => {
    const channelElement = e.target.closest(".chat_list");
    if(channelElement){
      const channelId = channelElement.id;

      initialization(channelId);
    }
  });
}

async function initialization(channelId) {
  msg_history_div.innerHTML = `
    <p style="text-align:center;">Loading...</p>
  `;

  $("#userProfileDiv").hide();
  $("#userChatBox").hide();
  $("#rightChatDiv").show();
  $("#type_msg").show();
  $(`#unreadmsgcount_${channelId}`).hide();

  type_msg.innerHTML = `
      <div class="input_msg_write">
        <form autocomplete="off">
        <div class="reply_msg_container" id="reply_msg_container">
        <!-- Container for a single reply message -->
          <div>
            <div class="user_info">
              <span id="reply_user_name" class="user_name"></span>
              <span class="reply_close_icon" onclick="resetReplyMsg()">
                <i class="fa fa-times" aria-hidden="true"></i>
              </span>
            </div>
            <div class="msg_body reply_msg_body">
              <p id="reply_msg_txt"></p>
            </div>
          </div>
        </div>

        <div class="input-with-button">
          <input type="text" class="write_msg emoji-button" id="written_msg" placeholder="Type a message" autocomplete="off"/>
          
          <div class="dropend input-btn-container">
            <button class="emojiButton dropdown-toggle" id="emojiButton" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-smile" aria-hidden="true"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-end">
            <div class="pickerContainer"></div>
            </div>
            
            <button class="msg_send_btn" type="submit" id="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
        </div>
        </form>
      </div>
    `;

  $(".reply_close_icon").hide();
  const sendButton = document.getElementById("msg_send_btn");
  const inputMessage = document.getElementById("written_msg");

  const container = document.querySelector(".pickerContainer");
  const picker = createPicker({
    rootElement: container,
  });

  picker.addEventListener("emoji:select", (selection) => {
    inputMessage.value += selection.emoji;
  });

  const channel = await sendbird.groupChannel.getChannel(channelId);
  window.channel = channel;

  const member = window.channel.members.find(
    (member) => member.userId !== userId
  );
  window.channel.markAsRead().catch((err) => {
    console.log(err);
  });
  loadUserDetails(member);

  const filter = new MessageFilter();
  filter.replyType = "all";
  filter.messageType = "all";
  const limit = 100;

  const collection = window.channel.createMessageCollection({
    filter,
    limit,
  });
  collection
    .initialize(MessageCollectionInitPolicy.CACHE_AND_REPLACE_BY_API)
    .onCacheResult((err, messages) => {})
    .onApiResult((err, messages) => {
      msg_history_div.innerHTML = "";

      messages.reverse();

      messages.forEach((message) => {
        if (
          message.channelUrl === channelId &&
          !document.getElementById(message.messageId)
        ) {
          appendMessage(message);
        }
      });

      msg_history_div.scrollTop = msg_history_div.scrollHeight;
    });

  const handler = {
    onMessagesAdded: (context, channel, messages) => {
      let msgArrStr = JSON.stringify(messages);
      let msgArr = JSON.parse(msgArrStr);
      msgArr.reverse();

      msgArr.forEach((message) => {
        // if(!message.parentMessageId){
          if (
            message.channelUrl === channelId &&
            !document.getElementById(`'${message.messageId}'`) &&
            message.messageId > 0
          ) {
            appendMessage(message);
          }
        // }
      });
      msg_history_div.scrollTop = msg_history_div.scrollHeight;
    },
    onMessagesDeleted: (context, channel, messageIds) => {
      if (messageIds.length === 1) {
        let msgElem = document.getElementById(`msg_id_${messageIds[0]}`);
        msgElem.parentNode.removeChild(msgElem);
      }
      msg_history_div.innerHTML = "";
    },
    onChannelUpdated: (messageIds, channel) => {},
    onChannelDeleted: (context, channelUrl) => {
      resetAfterDelete();
    },
  };
  collection.setMessageCollectionHandler(handler);

  sendButton.addEventListener("click", async (e) => {
    e.preventDefault();
    const params = {
      message: inputMessage.value,
    };

    const replyMsgBody = document.getElementsByClassName("reply_msg_body");
    const parentMessageId = replyMsgBody[0].getAttribute("id");

    if (parentMessageId) {
      params.parentMessageId = Number(parentMessageId);
      params.isReplyToChannel = true;
    }

    await window.channel.sendUserMessage(params).onSucceeded((message) => {
      // if(message.parentMessageId){
      if(!document.getElementById(`'${message.messageId}'`) && message.messageId > 0){
        appendMessage(message);
      }
      // }
      $("#written_msg").blur();
      inputMessage.value = "";
      resetReplyMsg();
      msg_history_div.scrollTop = msg_history_div.scrollHeight;
    });
  });
}

window.replyMsg = async (...message) => {
  const msgTxt = message[3];
  const createdAt = formatDate(message[2]);
  const senderNickName = message[1];
  const parentMessageId = message[0];

  $("#written_msg").focus();
  
  $(".reply_close_icon").show();
  const reply_container = document.getElementById("reply_msg_container");
  const replyUsernameElem = document.getElementById("reply_user_name");
  const replyMsgElem = document.getElementById("reply_msg_txt");
  const replyMsgBody = document.getElementsByClassName("reply_msg_body");

  reply_container.children[0].classList.add("reply_msg");
  replyUsernameElem.innerText = senderNickName;
  replyMsgElem.innerText = msgTxt;
  replyMsgBody[0].setAttribute("id", parentMessageId);
};

window.resetReplyMsg = () => {
  $(".reply_close_icon").hide();
  const reply_container = document.getElementById("reply_msg_container");
  const replyUsernameElem = document.getElementById("reply_user_name");
  const replyMsgElem = document.getElementById("reply_msg_txt");
  const replyMsgBody = document.getElementsByClassName("reply_msg_body");

  reply_container.children[0].classList.remove("reply_msg");
  replyUsernameElem.innerText = null;
  replyMsgElem.innerText = null;
  replyMsgBody[0].removeAttribute("id");
} 

async function loadUserDetails(member) {
  const formData = new FormData();
  formData.append("sendbird_user_id", member.userId);
  window.openedMember = member;

  const response = await fetch(`${window.location.origin}/api/user-details`, {
      method: "POST",
      body: formData
  });

  const resData = await response.json();
  const profileData = resData.data.profile;

  if ($("#userProfileDiv").is(":hidden")) {
    $("#userChatBox").show();
    $("#userProfileDiv").show();
    var userImg = $("#userImg-" + member.userId).attr("src") ? $("#userImg-" + member.userId).attr("src") : member.plainProfileUrl;
    window.userImg = userImg;
    var userName = $("#userName-" + member.userId).text() ? $("#userName-" + member.userId).text() : member.nickname;
    var userProfileId = profileData ? profileData.id : $("#userName-" + member.userId).data("id") ? $("#userName-" + member.userId).data("id") : member.userId;
    $("#userProfileImg").attr("src", "");
    $("#userProfileName").text(userName);
    $("#userNameChatBox").text(userName);
    $("#userProfileImg").attr("src", userImg);
    $("#userProfileUrl").attr(
      "href",
      baseUrl + "account/profile?id=" + userProfileId
    );
  }
}

window.copyMsg = (message) => {
  let textArea = document.createElement("textarea");
  textArea.value = message;
  document.body.appendChild(textArea);
  textArea.select();
  document.execCommand("copy");
  textArea.remove();
  alert("Message copied to clipboard!");
};

window.deleteMsg = (messageId) => {
  const deleteMsgUrl = `https://api-${appId}.sendbird.com/v3/group_channels/${channel.url}/messages/${messageId}`;

  $.ajax({
    url: deleteMsgUrl,
    type: "DELETE",
    headers: {
      "Api-Token": "cbe0fefbda0ee834d59074dd117cb9141c495ed0",
    },
    success: function (data) {
      alert("Message deleted successfully");
      initialization(channel.url);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      console.log(XMLHttpRequest);
      alert(XMLHttpRequest.responseJSON.message);
    },
  });
};

window.clearChat = async () => {
  await channel.resetMyHistory();
};

window.deleteChat = async () => {
  try {
    await channel.delete();
    alert("Chat deleted successfully!");
  } catch (error) {
    alert("Error deleting chat");
  }
};

window.deleteChatFromLeft = async (channel, event) => {
  const channelUrl = channel.id;
  event.stopPropagation();

  const selectedChannel = await sendbird.groupChannel.getChannel(channelUrl);

  try {
    await selectedChannel.delete();
    resetAfterDelete();
    alert("Chat deleted successfully!");
  } catch (error) {
    alert("Error deleting chat");
  }
}

window.resetAfterDelete = () => {
  getChannels();
  msg_history_div.innerHTML = "";
  $("#userChatBox").hide();
  $("#userProfileDiv").hide();
}

window.formatDate = (timestamp) => {
  const date = new Date(timestamp);
  const displayDate = date.toString();

  const hours = date.getHours();
  const minutes = date.getMinutes();
  const ampm = hours >= 12 ? "PM" : "AM";
  const formattedHours = hours % 12 === 0 ? 12 : hours % 12;
  const formattedMinutes = minutes < 10 ? "0" + minutes : minutes;
  const formattedTime = formattedHours + ":" + formattedMinutes + " " + ampm;

  const displayDateArr = displayDate.split(" ");

  return `${displayDateArr[1]} ${displayDateArr[2]} | ${formattedTime}`;
};

window.escapeMessage = (message) => {
  if (message === null || message === undefined) return "";
  return message
    .replace(/\\/g, "\\\\") // Escape backslashes
    .replace(/'/g, "\\'") // Escape single quotes
    .replace(/"/g, '\\"') // Escape double quotes
    .replace(/\n/g, "\\n") // Escape newlines
    .replace(/\r/g, "\\r"); // Escape carriage returns
};

window.reportMsg = (message, messageId, channelUrl, reportedUser) => {
  $.ajax({
    url: reportMessageUrl,
    type: "POST",
    data: {
      message: message,
      messageId: messageId,
      channelUrl: channelUrl,
      reportedUser: reportedUser,
    },
    success: function (data) {
      data = $.parseJSON(data);
      if (data.status == "1") {
        alert("You have reported message");
      } else if (data.status == "2") {
        alert("You have already reported this message");
      }
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      console.log("Status: " + textStatus);
      console.log("Error: " + errorThrown);
    },
  });
};

window.reportUser = () => {
  console.log("report user worked");

  if(window.openedMember){
    const reportMsgUrl = `https://api-${appId}.sendbird.com/v3/report/users/${window.openedMember.userId}`;
    const payload = {
      channel_type: "group_channels",
      channel_url: window.channel.url,
      report_category: "spam"
    };

    $.ajax({
      url: reportMsgUrl,
      type: "POST",
      data: JSON.stringify(payload),
      headers: {
        "Api-Token": "cbe0fefbda0ee834d59074dd117cb9141c495ed0",
      },
      success: function (data) {
        alert("User reported successfully");
      },
      error: function (XMLHttpRequest, textStatus, errorThrown) {
        console.log(XMLHttpRequest);
        alert(XMLHttpRequest.responseJSON.message);
      },
    });
  }
}

const searchMessageElem = document.querySelector(".search-message");

if (searchMessageElem) {
  searchMessageElem.addEventListener("keyup", (event) => {
    const searchTerm = event.target.value.toLowerCase();
    const chatLists = document.querySelectorAll('.chat_list');

    chatLists.forEach(chat => {
      const chatName = chat.querySelector('h5').textContent.trim().toLowerCase();
      if (chatName.includes(searchTerm)) {
        chat.style.display = '';
      } else {
        chat.style.display = 'none';
      }
    });
  });
} else {
  console.error('Element with ID "search-input" not found.');
}

window.addEventListener('beforeunload', async function (e) {
  console.log("Log at index.js 641 --> ",e);
  await sendbird.disconnect();
});