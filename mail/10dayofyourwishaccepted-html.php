<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $wish->w_id]);
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $user2->id]);
$createwish = Yii::$app->urlManager->createAbsoluteUrl('wish/create');
$grantedwish = Yii::$app->urlManager->createAbsoluteUrl('wish/granted#search_wish');
?>
<div class="password-reset">

    <?php
    $username1 = \app\console\models\UserProfile::findOne([
        'user_id' => $user->id,
    ]);
    $username2 = \app\console\models\UserProfile::findOne([
        'user_id' => $user2->id,
    ]);
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
    $editmessage = str_replace("##USERACCEPTEDNAME##", Html::a(Html::encode($username2->firstname), $resetLink2), $editmessage);
    $editmessage = str_replace("##WISHLINK##", Html::a(Html::encode($wish->wish_title), $resetLink1), $editmessage);
    $editmessage = str_replace("##CREATEWISH##", Html::a(Html::encode('post it again'), $createwish), $editmessage);
    $editmessage = str_replace("##GRANTEDWISHLINK##", Html::a(Html::encode('Granted Wishes'), $grantedwish), $editmessage);

    echo $editmessage;
    ?>
</div>
