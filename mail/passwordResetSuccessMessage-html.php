<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */
$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
$username1 = \app\models\UserProfile::findOne([
	'user_id' => $user->id,
]);
?>
<div class="password-reset">


<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
		$editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $loginLink), $editmessage);		
		echo $editmessage;
	?>

<!--	
Hello <?php // $user->username ?>,
<br>Your Password is Reset Successfully	
<br>
Thanks, 
<br>
<?php // $loginLink ?>
-->

</div>
