<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\models\UserProfile;
use yii\data\ActiveDataProvider;
use app\models\Wish;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = "home";
        $query = Wish::find()->where(['not', ['granted_by' => null]])->orderBy('w_id DESC');	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize'=>3
            ] 
        ]);
        
        $curr_query = Wish::find()->where(['granted_by' => null,'wish_status'=>0])->orderBy('w_id DESC');	
        $curr_dataProvider = new ActiveDataProvider([
            'query' => $curr_query,
             'pagination' => [
                'pageSize'=>3
            ] 
        ]);
        
        $forum = \app\models\Editorial::find()->where(['is_video_only' => 0,'status'=>0])->orderBy('created_at DESC');	
        $forum_dataProvider = new ActiveDataProvider([
            'query' => $forum,
             'pagination' => [
                'pageSize'=>3
            ] 
        ]);
        
        return $this->render('index',['models'=>$dataProvider->models, 'current'=>$curr_dataProvider->models, 'forums'=>$forum_dataProvider->models]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin($red_url=null)
    {	
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            if($red_url)
		return $this->redirect($red_url);
            
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

			$model->contact();
			$model->admincontact();
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
	 
    public function actionAbout()
    {
        $model = \app\models\Page::find()->where(['p_id'=>4])->one();

        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	         'pagination' => [
                'pageSize'=>10
            ] 
        ]);
        return $this->render('about',[
			'dataProvider' => $dataProvider,
			'model' => $model,
		]);
    }

    public function actionFbSignUp($res)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $result =   json_decode($res);
        
        if( ! empty($result) && ! empty($result->email))
        {
            $user = new User();
            
            $user_fb    =   $user->findByFbId($result->id);
            
            if( ! empty($user_fb) && ! empty($user_fb->username))
            {
                $userName   =   User::findByUsername($user_fb->username);
                
                if(Yii::$app->user->login($userName, TRUE ? 3600*24*30 : 0))
                    return $this->redirect(['/']);
                else
                    return $this->redirect(['login']);
            }
            
            if( ! empty(User::findByEmail($result->email)))
            {
                return 'Email already Exists! Try loging in with username and Password.';
            }
            
            //$user->scenario = 'sign-up';
            $profile = new UserProfile();
            
            $user->fb_id    =   $result->id;
            $user->username    =   $result->email;
            $user->generateAuthKey();
            $user->email    =   $result->email;
            $user->status = 10;
            $user->created_at = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");
            
            if($user->save()){
                $profile->user_id = $user->id;
                $profile->save_id       =   1;
                $profile->firstname =   $result->first_name;
                $profile->lastname  =   $result->last_name;
                
                if( ! empty($result->picture) && ! empty($result->picture->data->url))
                {
                    $imgdata   =   file_get_contents($result->picture->data->url, false);
                    
                    if( ! empty($imgdata))
                    {
                        file_put_contents('web/uploads/users/'.$profile->user_id.'.jpg', $imgdata);
                        
                        $profile->profile_image =   $profile->user_id.'.jpg';
                        $profile->save_id       =   0;
                    }
                }
                else
                {
                    $profile->profile_image =   'images/img1.jpg';
                }
                
                $profile->save();
                
                $userName   =   User::findByUsername($user->username);
                
                if(Yii::$app->user->login($userName, TRUE ? 3600*24*30 : 0))
                    return $this->redirect(['/']);
            }
        }
    }
    
    public function actionSignUp()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = new User();
        $user->scenario = 'sign-up';
        $profile = new UserProfile();
        $countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');	

        $privacy_policy = \app\models\Page::find()->where(['p_id'=>1])->one();		
        $terms = \app\models\Page::find()->where(['p_id'=>2])->one();		
        $community_guidelines = \app\models\Page::find()->where(['p_id'=>3])->one();		

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())){			
            $user->setPassword($user->password);
            $user->generateAuthKey();
            $user->status = 13;
            $user->created_at = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");

            if($user->save()){
                $profile->user_id = $user->id;
                $profile->save_id       =   1;
                
                if(empty(Yii::$app->request->post()['UserProfile']['profile_image']))
                {
                    $profile->profile_image = $profile->dulpicate_image;
                }
                else
                {
                    $data   =   Yii::$app->request->post()['UserProfile']['profile_image'];
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    file_put_contents('web/uploads/users/'.$profile->user_id.'.jpg', $data);

                    $profile->profile_image =   $profile->user_id.'.jpg';
                    $profile->save_id       =   0;
                }

                $profile->save();
                $profile->sendVAlidationEmail($user->email);
                Yii::$app->session->setFlash('RegisterFormSubmitted');
                //return $this->redirect(['success-signup', 'profile' => $profile]);
                return $this->render('success_signup', [
                    'user' => $user,
                    'profile'=>$profile
                ]);
            } else 
            { 
                return $this->render('sign_up', [
                'user' => $user,
                'profile' => $profile,
                'countries' => $countries,
                'privacy_policy' => $privacy_policy,
                'terms' => $terms,
                'community_guidelines' => $community_guidelines,
                ]);
            }
        }
        else return $this->render('sign_up', [
            'user' => $user,
            'profile' => $profile,
            'countries' => $countries,
            'privacy_policy' => $privacy_policy,
            'terms' => $terms,
            'community_guidelines' => $community_guidelines,
        ]);
    }
	public function actionGetStates($country_id){
		$states = \app\models\State::find()->where(['country_id'=>$country_id])->all();
		if(count($states)>0){
			echo "<option value=''>--Select State--</option>";
			foreach($states as $state){
				echo "<option value='".$state->id."'>".$state->name."</option>";
			}			
		}
		else{
			echo "<option value=''>-</option>";
		}
	}
	public function actionGetCities($state_id){
		$cities = \app\models\City::find()->where(['state_id'=>$state_id])->all();
		if(count($cities)>0){
			echo "<option value=''>--Select City--</option>";
			foreach($cities as $city){
				echo "<option value='".$city->id."'>".$city->name."</option>";
			}			
		}
		else{
			echo "<option value=''>-</option>";
		}
	}
	
	public function actionEditorial(){
		$model = \app\models\Editorial::find()->where(['status'=>0])->orderBy()->all();		
		return $this->render('', [
            'model' => $model,	
			]);
	}
	
    public function actionRequestPasswordReset()
    {		
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->redirect(['login']);
            } else { 
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
				
            }			   
        } 
        return $this->render('requestPasswordResetToken', ['model' => $model,]);
    }
	
    public function actionResetPassword($token)
    {
        try {         
            $user = User::findByPasswordResetToken($token);

            if(!$user)
            {
                    return $this->goHome();
            }
             $model = new ResetPasswordForm($token);
			
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
			
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');			
            $model->sendEmailResetSuccess();			
             return $this->redirect(['login']);
			
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    } 

	/**
	 * Send test mail
	 * Only for debugging purposes
	 */
	public function actionTestMail(){
		Yii::$app->mailer->compose()
			->setTo('dency@abacies.com')
			->setFrom(['dency@abacies.com' => 'Dency G B'])
			->setSubject('Test mail from simplywishes')
			->setTextBody('Regards')
			->send();		
	}

	
	/**
     * Lists all Wish models.
     * @return mixed
     */
    public function actionIndexHome()
    {
	  $user = User::findOne(\Yii::$app->user->id);
	  $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
	  return $this->render('index_home',['user' => $user,
			'profile' => $profile ]);
    }
	
	
	public function actionTest(){
		
		/// Cron JOb Function mail
		$oldApp = \Yii::$app;		
		$config  = require(__DIR__ . '/../config/console.php');
		$console = new \yii\console\Application($config);
		\Yii::$app->runAction('onemonth/index');
		\Yii::$app = $oldApp;
	}
	
	
	public function actionUserValidation($auth_key)
    {
		
		if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
		
        try { 
        
			$user = User::findByAuthKeyValidation($auth_key);			
			if($user)
			{
				
				 $model = User::findOne($user->id);
				 $model->status = 10;
				 if($model->save())
				 {
					 $profile = UserProfile::find()->where(['user_id'=>$user->id])->one();
					 $profile->sendEmail($user->email);
					 Yii::$app->session->setFlash('activeCheckmail');
				 }	 
			} 	 
				return $this->redirect(['login']);
			 							
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
			              
    }

	
}
