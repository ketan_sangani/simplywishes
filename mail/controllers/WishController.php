<?php

namespace app\controllers;

use Yii;
use app\models\Wish;
use app\models\WishComments;
use app\models\Activity;
use app\models\ReportWishes;
use app\models\search\SearchWish;
use app\models\search\SearchReportWishes;
use app\models\User;
use app\models\UserProfile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\MailContent;

use app\models\Payment;
/**
 * WishController implements the CRUD actions for Wish model.
 */
class WishController extends Controller
{
	public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionSearch(){
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        $searchModel = new SearchWish();
        //$cat_id = null;
        //$searchModel->wish_title = Yii::$app->request->queryParams['match'];
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);
        
        return $this->render('searched_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'profile' => $profile
        ]);		
        
    }
    
    public function actionSearchScroll($page){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);

        $dataProvider->pagination->page = $page;
        $str = '';
		
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }
        return $str;
        
    }
    
    public function actionSearchUser(){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustomUser(Yii::$app->request->queryParams);

        return $this->render('searched_user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);		
        
    }
    /**
     * Lists all Wish models.
     * @return mixed
     */
    public function actionIndex($cat_id=null)
    {
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
        
        return $this->render('current_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cat_id' => $cat_id,
            'user' => $user,
            'profile' => $profile
        ]);
    }
    /**
     * Lists all Wish models when scrolls.
     * @return mixed
     */
    public function actionScroll($page,$cat_id=null)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
		$dataProvider->pagination->page = $page;
        $str = '';
		//if ($dataProvider->totalCount > 0) {
        foreach($dataProvider->models as $wish){
				$str .= $wish->wishAsCard;
        }
		//}
        return $str;
    }
    /**
     * Lists all Wish models according to the number of likes.
     * @return mixed
     */
    public function actionPopular()
    {
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
        
        return $this->render('popular_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'profile' => $profile
        ]);
		
    }
    public function actionScrollPopular($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
		$dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }
        return $str;
    }
    public function actionGranted(){
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        
        return $this->render('fullfilled_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'profile' => $profile
        ]);		
    }
    public function actionScrollGranted($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
			$str .= $wish->wishAsCard;
        }
        return $str;
    }
    /**
     * Displays a single Wish model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $wish = $this->findModel($id);
        
        $listcomments = new WishComments();
        $comments = $listcomments->find()->where(['w_id'=>$id,'parent_id'=>0,'status'=>0])->orderBy('w_comment_id Desc')->all();		 
        
        return $this->render('view', [
            'model' => $wish, 'comments'=>$comments,'listcomments'=>$listcomments
        ]);
    }
    
    public function actionWishComments()
    {
        $model = new WishComments();
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                $wish = $this->findModel($model->w_id);
                 
                if($wish->wished_by != \Yii::$app->user->id)
                    $this->sendCommentEmail($wish, $model->comments);
                
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
        }
    }
    
    public function actionCommentreply()
    {		
        $model = new WishComments();
        if($model->load(Yii::$app->request->post()))
        {				
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            $model->user_id = \Yii::$app->user->id;
            if($model->save())
            {
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            } 
        }
    }
    
    public function actionUpdateComment($id)
    {
        $model = WishComments::findOne($id);
        
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
        }
    }
    
    public function actionDeleteComment($id)
    {
        $model  =   WishComments::findOne($id);
        $model->status  =   1;
        
        if($model->parent_id == 0)
        {
            $replycomments = WishComments::find()->where(['parent_id'=>$id])->all();
            
            foreach($replycomments as $reply)
            {
                $reply->status  =   1;
                $reply->save();
            }
        }
        
        $model->save();
        return $this->redirect(['wish/view?id='.$model->w_id]);
    }

    /**
     * Creates a new Wish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Wish();
        $model->scenario = 'create';	

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        if ($model->load(Yii::$app->request->post()))
        {
            $model->wished_by = \Yii::$app->user->id;
            $model->wish_status = 0;
			
            if(!empty($model->auto_id))
            {
                $model1 = Wish::findOne($model->auto_id);
                $model1->w_id = $model->auto_id;
                $model1->wished_by = \Yii::$app->user->id;
                $model1->wish_title = $model->wish_title;
                $model1->wish_description = $model->wish_description;
                $model1->primary_image = $model->primary_image;
                $model1->expected_cost = $model->expected_cost;
                $model1->expected_date = $model->expected_date;
                $model1->in_return = $model->in_return;
                $model1->who_can = $model->who_can;
                $model1->non_pay_option = $model->non_pay_option;
                $model1->wish_status = $model->wish_status;				
                $model1->show_mail_status = $model->show_mail_status;
                $model1->show_mail = $model->show_mail;
                $model1->show_person_status = $model->show_person_status;
                $model1->show_person_street = $model->show_person_street;
                $model1->show_person_city = $model->show_person_city;
                $model1->show_person_state = $model->show_person_state;
                $model1->show_person_zip = $model->show_person_zip;
                $model1->show_person_country = $model->show_person_country;
                $model1->show_other_status = $model->show_other_status;
                $model1->show_other_specify = $model->show_other_specify;
                $model1->i_agree_decide = $model->i_agree_decide;
                $model1->i_agree_decide2 = $model->i_agree_decide2;
                $model1->process_status = $model->process_status;
                $model1->process_granted_by = $model->process_granted_by;
                $model1->process_granted_date= $model->process_granted_date;			

                $model1->update(false);
            }
            else
            { 	
                $model->save();
            }
            
            if(empty(Yii::$app->request->post()['Wish']['primary_image']))
            {
                if(empty($model->primary_image_name))
                {
                    $model->primary_image =   'images/wish_default/1.jpg';
                }
                else {
                        $model->primary_image = $model->primary_image_name;
                }
            }
            else
            {
                $data   =   Yii::$app->request->post()['Wish']['primary_image'];

                list($type, $data) = explode(';', $data);
                list(, $data)      = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents('web/uploads/wishes/'.$model->w_id. '.' .'jpg', $data);

                $model->primary_image =   'uploads/wishes/'.$model->w_id.'.jpg';
            }

            $model->save();
            $model->sendCreateSuccessEmail(\Yii::$app->user->id);
            return $this->redirect(['account/my-account']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Updates an existing Wish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->primary_image;

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post())) {
            //check for a new image
            $model->wished_by = \Yii::$app->user->id;
            $model->save();
            
            if(empty(Yii::$app->request->post()['Wish']['primary_image']))
            {
                if(empty($model->primary_image_name))
                {
                    $model->primary_image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->primary_image_name) && ($model->primary_image_name != $current_image ))
                {
                        $model->primary_image = $model->primary_image_name;
                } else {
                        $model->primary_image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Wish']['primary_image'])
                {
                    $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                    
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    
                    file_put_contents('web/uploads/wishes/'.$model->w_id. '.' .'jpg', $data);
                    
                    $model->primary_image =   'uploads/wishes/'.$model->w_id.'.jpg';
                }
                else
                {
                    if(! empty($model->primary_image_name))
                        $model->primary_image = $model->primary_image_name;
                    else
                        $model->primary_image = $current_image;
                }
            }
            
            //save model
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['view', 'id' => $model->w_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user ,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Deletes an existing Wish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         $this->findModel($id)->delete();
        return $this->redirect(['index']); 
    }

	
	public function actionAjaxDelete()
    {
		$id = Yii::$app->request->post('id');
		$this->findModel($id)->delete();
    }
	
	
	
    /**
     * Like a wish
     * User has to be logged in to like a wish
     * Param: wish id
     * @return boolean
     */
    public function actionLike($w_id,$type)
    {
        if(\Yii::$app->user->isGuest)
            return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
        $wish = $this->findModel($w_id);
        $activity = Activity::find()->where(['wish_id'=>$wish->w_id,'activity'=>$type,'user_id'=>\Yii::$app->user->id])->one();
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        $activity = new Activity();
        $activity->wish_id = $wish->w_id;
        $activity->activity = $type;
        $activity->user_id = \Yii::$app->user->id;
        if($activity->save())
        {
            $this->sendLikedEmail($wish);
            return "added";
        }
        else 
            return false;
    }

    public function actionFullfilled($w_id){
        $wish = $this->findModel($w_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.

        $wish->granted_by = \Yii::$app->user->id;
        $wish->granted_date = date('m-d-Y');

        if($wish->save(false))
        {			
            $this->sendEmail($wish);		
            return $this->redirect(['wish/view','id'=>$w_id]);
        }
    }
    /**
     * Finds the Wish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Wish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Wish::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionTopWishers(){
        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
        
        return $this->render('iWish', [
            'dataProvider' => $dataProvider,
        ]);		
    }
	
	public function actionTopGranters(){
		$query = Wish::find()->select(['wishes.granted_by,count(w_id) as total_wishes'])->where(['not', ['granted_by' => null]])->orderBy('total_wishes DESC');
		$query->groupBy('granted_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
		return $this->render('iGrant', [
			'dataProvider' => $dataProvider,
		]);		
	}
	
	/**
	 * IPN listener for paypal
	 * Ref: http://stackoverflow.com/questions/14015144/sample-php-code-to-integrate-paypal-ipn
	 * Change the status back to not paid if not veified
	 */
	public function actionVerifyGranted(){
		// STEP 1: Read POST data

		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$raw_post_data = file_get_contents('php://input');
			$fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $raw_post_data);
			  fclose($fh);
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
				$value = urlencode(stripslashes($value)); 
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}


		// STEP 2: Post IPN data back to paypal to validate

		//https://www.sandbox.paypal.com/cgi-bin/webscr
		//$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
		$ch = curl_init('https://ipnpb.paypal.com/cgi-bin/webscr');

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if( !($res = curl_exec($ch)) ) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);


		// STEP 3: Inspect IPN validation result and act accordingly

		if (strcmp ($res, "VERIFIED") == 0) {
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
			$payment = new Payment();
			// assign posted variables to local variables
			$payment->item_name = $_POST['item_name'];
			$payment->item_number = $_POST['item_number'];
			$payment->payment_status = $_POST['payment_status'];
			$payment->payment_amount = $_POST['mc_gross'];
			$payment->payment_currency = $_POST['mc_currency'];
			$payment->txn_id = $_POST['txn_id'];
			$payment->receiver_email = $_POST['receiver_email'];
			$payment->payer_email = $_POST['payer_email'];
			$payment->payment_date = $_POST['payment_date'];
			$payment->save();
			//check if success
			$wish = $this->findModel($_POST['item_number']);
			//if not fully paid or if not successful, revert the granted status
			if($payment->payment_status != "Completed"){
				$wish->granted_by = NULL;
				$wish->save();
			}

		} else if (strcmp ($res, "INVALID") == 0) {
			  // Save the output (to append or create file)
			  $fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $res);
			  fclose($fh);
			// log for manual investigation
		}		
	}
	
	public function actionRemoveWish($wish_id)
	{
		if(\Yii::$app->user->isGuest)
			return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
		$wish = $this->findModel($wish_id);
		$activity = Activity::find()->where(['wish_id'=>$wish->w_id,'activity'=>'fav','user_id'=>\Yii::$app->user->id])->one();
		if($activity != null){
			$activity->delete();
		}
		
		 return $this->redirect(['account/my-saved']);
	}
	
    public function sendEmail($wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>5])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
		
        if (!$user) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'grantedSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendLikedEmail($wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>15])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
        
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
		
        if (!$user || !$likedUser) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishLike-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish, 'likedUser' =>  $likedUser]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendCommentEmail($wish, $comment)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>16])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
        
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
        
        if (!$user || !$likedUser) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishComment-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish, 'likedUser' =>  $likedUser, 'comment' => $comment]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendEmailToGranter($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>14])->one();
        
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
		
        if (!$user) {
            return false;
        }
        
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
        
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        
        return $message->send();
    }
	
    public function actionLikesView($w_id)
    {
        $wish = $this->findModel($w_id);
        $likedUser  =   [];
        if( ! empty($wish->likes))
        {
            foreach($wish->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();
                
                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }
        
        return json_encode($likedUser);
    }
    
    public function actionReport($w_id)
    {
            if(\Yii::$app->user->isGuest)
                    return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

            $wish = $this->findModel($w_id);
            $activity = ReportWishes::find()->where(['w_id'=>$wish->w_id])->one();
            if($activity){
                    $activity->count = $activity->count + 1;
                    $activity->save();
                    return "added";			
            } else {
            $activity = new ReportWishes();
            $activity->w_id = $wish->w_id;
            $activity->count = 1;	
            $activity->save();
                    return "added";
            }
    }	
	
	public function actionReportAction()
	{
	 if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
		$searchModel = new SearchReportWishes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('report_view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
	 } else {
		 return $this->redirect(['site/index']);
	 }
	}
	
	
	public function actionReportActionView($id)
    {
		if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$wish = Wish::find()->where(['w_id'=>$id])->one();		
			if($wish)
			{			
				return $this->render('report_full_view', ['model' => $this->findModel($id) ]);
			} else {
					return $this->redirect('report-action');
			}	
		} else {
				 return $this->redirect(['site/index']);
			 }		
    }

	
	public function actionReportDelete()
    {
	  if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$id = Yii::$app->request->post('id');
			$this->findModel($id)->delete();
			$model = ReportWishes::find()->where(['w_id'=>$id])->one();
			$model->delete();
		 } else {
		 return $this->redirect(['site/index']);
	  }
    }
	
    public function actionUploadFile(){
        if( ! empty(Yii::$app->request->post()['image']) && ! empty(Yii::$app->request->post()['id']))
        {
            $data   =   Yii::$app->request->post()['image'];

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            file_put_contents('web/uploads/wishes/'.Yii::$app->request->post()['id']. '.' .'jpg', $data);

            return 'uploads/wishes/'.Yii::$app->request->post()['id'].'.jpg';
        }
   }
	
    public function actionWishAutosave()
    {	
        if(Yii::$app->request->post())
        {
            $models2 = Yii::$app->request->post();
            
            if( ! $models2['Wish']['w_id'])
            {
                $models = new Wish();
                $models->wished_by = \Yii::$app->user->id;
                $models->wish_status = 1;
                $models->save(false);
                $models2['Wish']['w_id'] =   $models->w_id;
            }
            
            $models =  Wish::find()->where(['w_id'=>$models2['Wish']['w_id']])->one();
            
            //$models->category = $models2['Wish']['category'];
            $models->wish_title = $models2['Wish']['wish_title'];		
            $models->wish_description = $models2['Wish']['wish_description'];
            
            if(empty($models2['Wish']['primary_image']) && empty($models2['Wish']['primary_image_name']))
                $models->primary_image = 'images/wish_default/1.jpg';
            else if( ! empty($models2['Wish']['primary_image_name']))
                $models->primary_image = $models2['Wish']['primary_image_name'];
            else
                $models->primary_image = $models2['Wish']['primary_image'];
            
            $models->expected_cost = $models2['Wish']['expected_cost'];
            $models->expected_date = $models2['Wish']['expected_date'];
            $models->in_return = $models2['Wish']['in_return'];
            $models->who_can = $models2['Wish']['who_can'];
            $models->non_pay_option = $models2['Wish']['non_pay_option'];

            $models->show_mail_status = $models2['Wish']['show_mail_status'];
            $models->show_mail = $models2['Wish']['show_mail'];
            $models->show_person_status = $models2['Wish']['show_person_status'];
            $models->show_person_street = $models2['Wish']['show_person_street'];
            $models->show_person_city = $models2['Wish']['show_person_city'];
            $models->show_person_state = $models2['Wish']['show_person_state'];
            $models->show_person_zip = $models2['Wish']['show_person_zip'];
            $models->show_person_country = $models2['Wish']['show_person_country'];
            $models->show_other_status = $models2['Wish']['show_other_status'];
            $models->show_other_specify = $models2['Wish']['show_other_specify'];
            $models->wish_status = 1;
            
            if($models->save(false))
            {
                return json_encode((int)$models2['Wish']['w_id']);
            }
            else
            {				
                echo "failed2";
            }

        }	
    }
	
	
		/**
     * Lists all Editorial models.
     * @return mixed
     */
    public function actionMyDrafts()
    {
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchDrafts(Yii::$app->request->queryParams);

        return $this->render('my_drafts', [
            'user' => $user,
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
    public function actionDeleteDraft($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['my-drafts']); 
    }
    
    public function actionDeleteWish($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['account/my-account']); 
    }
	
    public function actionViewDraft($id)
    {
        return $this->render('view_draft', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
    public function actionUpdateDraft($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');	
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->primary_image;

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post())) {
            //check for a new image
            $model->wished_by = \Yii::$app->user->id;
            $model->save();
            
            if(empty(Yii::$app->request->post()['Wish']['primary_image']))
            {
                if(empty($model->primary_image_name))
                {
                    $model->primary_image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->primary_image_name) && ($model->primary_image_name != $current_image ))
                {
                        $model->primary_image = $model->primary_image_name;
                } else {
                        $model->primary_image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Wish']['primary_image'])
                {
                    $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                    
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    
                    file_put_contents('web/uploads/wishes/'.$model->w_id. '.' .'jpg', $data);
                    
                    $model->primary_image =   'uploads/wishes/'.$model->w_id.'.jpg';
                }
                else
                {
                    if(! empty($model->primary_image_name))
                        $model->primary_image = $model->primary_image_name;
                    else
                        $model->primary_image = $current_image;
                }
            }
            
            $model->wish_status = 0;
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['my-drafts']);
        } else {
            return $this->render('update_draft', [
                'model' => $model,
                'user' => $user ,
                'profile' => $profile
            ]);
        }
		
    }
    
    public function actionProcessWish()
    {
        $w_id = \Yii::$app->request->post()['wish_id'];
        $processstatus = \Yii::$app->request->post()['processstatus'];
        $send_message = \Yii::$app->request->post()['send_message'];

        $wish = $this->findModel($w_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.		
        $wish->process_granted_by = \Yii::$app->user->id;
        $wish->process_granted_date = date('m-d-Y');
        $wish->process_status = $processstatus;

        if($wish->save(false))
        {
            if($send_message == 1)			
                $result = $wish->sendGrantWishNonFinancialEmail($wish->process_granted_by,$wish);	
            
            $res    =   $this->sendEmail($wish);
        }	
    }
		
	 public function actionResubmitProcessWish()
    {
		
        $w_id = \Yii::$app->request->post()['wish_id'];
		$userid = \Yii::$app->request->post()['userid'];
		
	if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin'))		
		$wish =  Wish::find()->where(['w_id'=>$w_id])->one();
	else
		$wish =  Wish::find()->where(['w_id'=>$w_id,'wished_by'=>$userid])->one();

		if($wish)
		{
			$wish->process_granted_by = "";
			$wish->process_granted_date = "00-00-0000";
			$wish->process_status = 0;
			$wish->email_status = 0;

			if($wish->save(false))
			{		
				echo "Success";
			}
		}
		
    }
	
    public function actionGrantProcessWish()
    {	
        $w_id = \Yii::$app->request->post()['wish_id'];
        $userid = \Yii::$app->request->post()['userid'];
        
        if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin'))		
            $wish =  Wish::find()->where(['w_id'=>$w_id])->one();
        else
            $wish =  Wish::find()->where(['w_id'=>$w_id,'wished_by'=>$userid])->one();

        if($wish)
        {
            $wish->granted_by = $wish->process_granted_by;
            $wish->granted_date = date('m-d-Y');
            $wish->process_granted_by = "";
            $wish->process_granted_date = "00-00-0000";
            $wish->process_status = 0;
            
            if($wish->save(false))
            {
                $this->sendEmailToGranter($wish->granted_by, $wish);		
                //return $this->redirect(['wish/view','id'=>$w_id]);
            }
        }	
    }	
	
	 public function actionMultiDeleteWishes()
	 {    

			$w_id = Yii::$app->request->post()['w_id'];	
			if($w_id)
			{
				 foreach($w_id as $tmp)
				 {										
					$this->findModel($tmp)->delete();
				 }
			}  			
	}
	
}
