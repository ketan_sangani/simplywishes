<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $donation->id]);
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $user2->id]);
$userCreatedLink = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $user->id]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$createdonation = Yii::$app->urlManager->createAbsoluteUrl('donation/create');
$username1 = \app\console\models\UserProfile::findOne([
	'user_id' => $user->id,
]);
$username2 = \app\console\models\UserProfile::findOne([
	'user_id' => $user2->id,
]);
?>
<div class="password-reset">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
        $editmessage = str_replace("##USERACCEPTEDNAME##", Html::a(Html::encode($username2->firstname), $resetLink2), $editmessage);
	    $editmessage = str_replace("##USERACCEPTEDNAMEONLY##", Html::encode($username2->firstname), $editmessage);
        $editmessage = str_replace("##USERACCEPTEDLINK##", Html::a(Html::encode($username2->firstname), $userCreatedLink), $editmessage);
        $editmessage = str_replace("##DONATIONLINK##", Html::a(Html::encode($donation->title), $resetLink1), $editmessage);
	    $editmessage = str_replace("##USERACCEPTEDLINKMESSAGE##", Html::a(Html::encode('sending a message'), $resetLink2), $editmessage);
	    $editmessage = str_replace("##COMPLETELINK##", Html::a(Html::encode('complete'), $resetLink1), $editmessage);
		$editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);		
		$editmessage = str_replace("##CREATEDONATION##", Html::a(Html::encode('post it again'), $createdonation), $editmessage);
	echo $editmessage;

	?>
	
    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->
	
</div>
