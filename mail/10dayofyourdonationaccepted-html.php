<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $wish->id]);
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $user2->id]);
$createwish = Yii::$app->urlManager->createAbsoluteUrl('donation/create');
$grantedwish = Yii::$app->urlManager->createAbsoluteUrl('wish/granted#search_wish');

?>
<div class="password-reset">

    <?php
    $username1 = \app\console\models\UserProfile::findOne([
        'user_id' => $user->id,
    ]);
    $username2 = \app\console\models\UserProfile::findOne([
        'user_id' => $user2->id,
    ]);
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
    $editmessage = str_replace("##USERACCEPTEDNAME##", Html::a(Html::encode($username2->firstname), $resetLink2), $editmessage);
    $editmessage = str_replace("##DONATIONLINK##", Html::a(Html::encode($wish->title), $resetLink1), $editmessage);
    $editmessage = str_replace("##CREATEDONATION##", Html::a(Html::encode('post your donation'), $createwish), $editmessage);
    $editmessage = str_replace("##GRANTEDDONATIONLINK##", Html::a(Html::encode('Granted Donations'), $grantedwish), $editmessage);

    echo $editmessage;
    ?>
</div>
