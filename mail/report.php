<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */


?>
<div class="template">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($profile)), $editmessage);
		echo $editmessage;
	?>
 
</div>
