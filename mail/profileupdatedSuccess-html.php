<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl('site/index-home#edt_home');
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$username1 = \app\models\UserProfile::findOne([
	'user_id' => $user->id,
]);
?>
<div class="password-reset">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
                $editmessage = str_replace("##PROFILE##", Html::a(Html::encode('Profile'), $resetLink1), $editmessage);
		$editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);		
		echo $editmessage;
	?>
	
    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->
	
</div>
