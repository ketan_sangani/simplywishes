<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $wish_id]);
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$username1 = \app\console\models\UserProfile::findOne([
    'user_id' => $user->id,
]);
?>
<div class="password-reset">
    <?php 			
        $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
        $editmessage = str_replace("##WISHNAME##", Html::a(Html::encode($wish_title), $resetLink), $editmessage);		
        $editmessage = str_replace("##WISHLINK##", Html::a(Html::encode('here'), $resetLink), $editmessage);
        $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink1), $editmessage);		
        echo $editmessage;
    ?>
</div>
