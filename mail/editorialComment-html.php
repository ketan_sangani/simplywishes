<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl('/');
$type = ($wish->is_video_only==1)?'video':'article';
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['editorial/editorial-page', ['id' => $wish->e_id,'class'=>$type]]);
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$username1 = \app\models\UserProfile::findOne([
    'user_id' => $user->id,
]);
$username2 = \app\models\UserProfile::findOne([
    'user_id' => $likedUser->id,
]);
?>
<div class="password-reset">

    <?php
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
    $editmessage = str_replace("##USERNAME2##", nl2br(Html::encode($username2->firstname)), $editmessage);
    $editmessage = str_replace("##COMMENTTEXT##", nl2br(Html::encode($comment)), $editmessage);
    $editmessage = str_replace("##WISHTITLE##", Html::a(Html::encode($wish->e_title), $resetLink1), $editmessage);
    $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('Sign-in'), $resetLink2), $editmessage);
    $editmessage = str_replace("##CONNECT##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);
    echo $editmessage;
    ?>

    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->

</div>
