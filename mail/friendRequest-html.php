<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['account/friend-requested']);
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl('account/inbox-message');
$otheruserprofile = Yii::$app->urlManager->createAbsoluteUrl(['account/profile','id'=>$fromuser->user_id]);
$confirmurl = Yii::$app->urlManager->createAbsoluteUrl(['friend/request-acceptedemail','id'=>$f_id]);
$rejecturl = Yii::$app->urlManager->createAbsoluteUrl(['friend/request-rejectedemail','id'=>$f_id]);
$confirmlink = 'Confirm';//Html::a('Confirm', $confirmurl);
$rejectlink = 'Ignore';//Html::a('Ignore', $rejecturl);
?>
<div class="password-reset">
    <p>Dear <?= Html::encode(ucfirst($user->firstname).' '.ucfirst($user->lastname)) ?>,</p>

    <p> <?= Html::a(Html::encode(ucfirst($fromuser->firstname).' '.ucfirst($fromuser->lastname)), $otheruserprofile); ?> would like to add you as a Friend!<br>
        Please <?php echo $confirmlink;?> or <?php echo $rejectlink;?> this request by going to your <?php echo Html::a(Html::encode('SimplyWishes Inbox'), $resetLink1);?> messages.<br>
        By confirming their request, each of you will be added to your each other’s Friends Page.

    </p>
<br>
    <p>All the best,<br>
       SimplyWishes Team
    </p>
</div>
