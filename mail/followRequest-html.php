<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $from->id]);
$username1 = \app\models\UserProfile::findOne([
	'user_id' => $to->id,
]);
$username2 = \app\models\UserProfile::findOne([
	'user_id' => $from->id,
]);
?>
<div class="password-reset">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
                $editmessage = str_replace("##USERNAME2##", Html::a(Html::encode($username2->firstname), $resetLink1), $editmessage);
		$editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);
		echo $editmessage;
	?>
	
    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->
	
</div>
