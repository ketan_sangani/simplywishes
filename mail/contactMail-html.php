<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
?>
<div class="password-reset">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username)), $editmessage);
                $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);		
		
		echo $editmessage;
	?>
	
	
	
   <!-- <p>Hello  <?php // Html::encode($username) ?>,</p>
	<p>Thank you for contacting us. </p>
	<p>We will review your message and get back to you!</p> -->
	
</div>
