<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $wish->w_id]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl('happy-stories/index');
$resetLink3 = Yii::$app->urlManager->createAbsoluteUrl('editorial/editorial');
$username1 = \app\console\models\UserProfile::findOne([
    'user_id' => $user->id,
]);
?>
<div class="password-reset">

    <?php
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
    $editmessage = str_replace("##WISHLINK##", Html::a(Html::encode($wish->wish_title), $resetLink1), $editmessage);
    $editmessage = str_replace("##HAPPYSTORY##", Html::a(Html::encode('Happy Stories'), $resetLink2), $editmessage);
    $editmessage = str_replace("##FORUM##", Html::a(Html::encode('Forum'), $resetLink3), $editmessage);
    $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);
    echo $editmessage;
    ?>

    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->

</div>
