<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $model->id]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$userCreatedLink = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $userDonation->id]);
$username1 = \app\console\models\UserProfile::findOne([
    'user_id' => $user->id,
]);
$username2 = \app\console\models\UserProfile::findOne([
    'user_id' => $userDonation->id,
]);
?>
<div class="password-reset">

    <?php
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
    $editmessage = str_replace("##USERACCEPTEDNAME##", Html::a(nl2br(Html::encode($username2->firstname)),$userCreatedLink), $editmessage);

    $details ='Please check your email';

    $editmessage = str_replace("##MESSAGE##", nl2br($details), $editmessage);
    $editmessage = str_replace("##DONATIONLINK##", Html::a(Html::encode($model->title), $resetLink1), $editmessage);
    $editmessage = str_replace("##USERACCEPTEDLINKMESSAGE##", Html::a(Html::encode('sending a message'), $resetLink1), $editmessage);
    $editmessage = str_replace("##USERACCEPTEDLINK##", Html::a(Html::encode($username2->firstname), $userCreatedLink), $editmessage);
    $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);

    $editmessage = str_replace("##DNATIONGIVEDATE##", nl2br($model->expected_date), $editmessage);
    $editmessage = str_replace("##DONATIONMETHOD##", nl2br($model->financial_assistance), $editmessage);
    $editmessage = str_replace("##DONATIONAMOUNT##", nl2br($model->expected_cost), $editmessage);
    echo $editmessage;
    ?>


</div>
