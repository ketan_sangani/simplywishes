<?php
use yii\helpers\Html;
use \app\console\models\UserProfile;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$inbox_link = Yii::$app->urlManager->createAbsoluteUrl('account/inbox-message');
$otheruserprofile = Yii::$app->urlManager->createAbsoluteUrl(['account/profile','id'=>$user->id]);

$fromuserprofile = UserProfile::findOne([
    'user_id'=> $user->id]);
$touserprofile = UserProfile::findOne([
    'user_id'=> $user2->id]);

?>
<div class="password-reset">

    <?php
    $editmessage = (isset($touserprofile->firstname))?str_replace("##USERNAME##", nl2br(Html::encode($touserprofile->firstname)), $editmessage):str_replace("##USERNAME##", nl2br(Html::encode("Simplywishes")), $editmessage);
    $editmessage = str_replace("##USERNAME2##", Html::a(Html::encode($fromuserprofile->firstname), $otheruserprofile), $editmessage);
    $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('Login'), $resetLink), $editmessage);
    $editmessage = str_replace("##MESSAGE##", Html::a(Html::encode($message->text)), $editmessage);
    if(isset($message) && $message->subject!=''){
        if($message->w_id!=''){
            $wishlink =  Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $message->w_id]);
            $editmessage = str_replace("##SUBJECT##", Html::a(Html::encode($message->subject), $wishlink), $editmessage);
        }else if($message->donation_id!=''){
            $wishlink =  Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $message->donation_id]);
            $editmessage = str_replace("##SUBJECT##", Html::a(Html::encode($message->subject), $wishlink), $editmessage);
        }else {
            $editmessage = str_replace("##SUBJECT##", $message->subject, $editmessage);
        }
    }
    echo $editmessage;
    ?>






    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> Your Registration has been Successfully Completed.  </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->

</div>
