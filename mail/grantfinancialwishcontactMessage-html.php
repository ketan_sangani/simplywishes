<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $model->w_id]);
$resetLink2 = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $user2->id]);
$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
?>
<div class="password-reset">

    <?php
    $sql = "SELECT * FROM user_profile WHERE user_id = ".$user->id;
    $username1 = \Yii::$app->getDb()->createCommand($sql)->queryOne();

    
    $sql2 = "SELECT * FROM user_profile WHERE user_id = ".$user2->id;
    $username2 = \Yii::$app->getDb()->createCommand($sql2)->queryOne();
    
    $editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1['firstname'])), $editmessage);

    $details ='';
    if($model->show_mail_status == 1){
        $details .='<p>Mail to this address : <b>'.$model->show_mail.'</b></p>';
    }
    if(($model->show_mail_status == 1) && ($model->show_person_status == 1)){
        $details .='<p>or</p>';
    }
    if($model->show_person_status == 1){
        $details .='<p>In Person at this location :  <b>'.$model->show_person_street.' '.$model->show_person_city.' '.$model->show_person_state.' '.$model->show_person_zip.' '.$model->show_person_country.'</b></p>';
    }
    if((($model->show_mail_status == 1) || ($model->show_person_status == 1))&& ($model->show_other_status == 1)){
        $details .='<p>or</p>';
    }
    if($model->show_other_status == 1){
        $details .='<p>Other : <b>'.$model->show_other_specify.'</b></p>';
    }

    $editmessage = str_replace("##MESSAGE##", nl2br($details), $editmessage);
    $editmessage = str_replace("##WISHLINK##", Html::a(Html::encode($model->wish_title), $resetLink1), $editmessage);
    $editmessage = str_replace("##USERNAME1##", Html::a(Html::encode($username2['firstname']), $resetLink2), $editmessage);
    $editmessage = str_replace("##USERNAME1WITHOUTLINK##", Html::encode($username2['firstname']), $editmessage);
    $editmessage = str_replace("##SENDINGMESSAGE##", Html::a(Html::encode('sending a message'), $resetLink2), $editmessage);

    $editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);
    $editmessage = str_replace("##GRANTEDWISH##", nl2br($model->expected_date), $editmessage);
    $editmessage = str_replace("##RECEIVINGMETHOD##", nl2br($model->financial_assistance), $editmessage);
    $editmessage = str_replace("##ASSOCIATEDWITHACCOUNT##", nl2br($user2->email), $editmessage);
    $editmessage = str_replace("##REQUESTEDAMOUNT##", nl2br($model->expected_cost), $editmessage);
    echo $editmessage;
    ?>


</div>
