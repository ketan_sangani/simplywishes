<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl('site/login');
$resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $donation->id]);
$username1 = \app\models\UserProfile::findOne([
	'user_id' => $user->id,
]);
?>
<div class="password-reset">

	<?php 			
		$editmessage = str_replace("##USERNAME##", nl2br(Html::encode($username1->firstname)), $editmessage);
        $editmessage = str_replace("##DONATIONTITLE##", Html::a(Html::encode($donation->title), $resetLink1), $editmessage);		
		$editmessage = str_replace("##LOGINLINK##", Html::a(Html::encode('SimplyWishes.com'), $resetLink), $editmessage);		
		echo $editmessage;
	?>
	
    <!--<p>Hello <?php // Html::encode($user->username) ?>,</p>

    <p>Welcome to SimplyWishes, <br> You Got Grant For Your Wish </p>

    <p><?php // Html::a(Html::encode($resetLink), $resetLink) ?></p>-->
	
</div>
