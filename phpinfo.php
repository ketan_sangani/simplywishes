
namespace App\Repositories\TourBooking;

use App\Constants\Constants;
use App\Events\BookingNew;
use App\Models\Admin;
use App\Models\Customer;
use App\Models\Staff;
use App\Models\Tax;
use App\Models\TourBooking;
use App\Models\TourBookingHistory;
use App\Models\TourOperator;
use App\Models\TourPackage;
use App\Models\Transaction;
use App\Models\TourPackageRate;
use App\Models\BookingTransact;
use App\Notifications\TourBooking\PeopleRemovedFromBooking;
use App\Notifications\TourBooking\BookingRescheduled;
use App\Notifications\TourBooking\TourBookingAcknowledgement;
use App\Notifications\TourBooking\TourBookingCancelled;
use App\Notifications\TourBooking\TourBookingRefund;
use App\Repositories\Repository;
use Carbon\Carbon;
use App\Exceptions\Core\GeneralCoreException;
use App\Jobs\CancelDontRefund;
use App\Jobs\CashBooking;
use App\Jobs\InvoicedBooking;
use App\Jobs\DeletePersonDontRefund;
use App\Models\InvoiceBooking;
use App\Notifications\TourBooking\TipForCustomer;
use App\Notifications\TourBooking\TipForGuide;
use App\Repositories\TourSlot\TourSlotRepository;
use App\Services\Cancel100RefundService;
use App\Services\CancelFullRefundService;
use App\Services\CancelPartialRefundService;
use App\Services\CardBookingService;
use App\Services\DeletePerson100RefundService;
use App\Services\DeletePersonFullRefundService;
use App\Services\DeletePersonPartialRefundService;
use App\Services\PermitFeeRefundBookingService;
use App\Services\RefundBookingService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Stripe\Exception\CardException;
use Stripe\PaymentIntent;
use Stripe\PaymentMethod;
use Stripe\Stripe;
use Stripe\StripeClient;
use Illuminate\Support\Facades\Route;
use Stripe\Customer as StripeCustomer;

class TourBookingRepository extends Repository
{
    /**
     * @var string
     */
    public $customer = Customer::class;

    public $tourOperator = TourOperator::class;

    public $admin = Admin::class;

    public $staff = Staff::class;

    public $tourBooking = TourBooking::class;

    public $transaction = Transaction::class;

    public $tourPackage = TourPackage::class;

    public $tourBookingHistory = TourBookingHistory::class;

    public $tourPackageRate = TourPackageRate::class;

    public $bookingTransact = BookingTransact::class;

    public $invoiceBooking = InvoiceBooking::class;

    protected $tourSlotRepository;

    /**
     * @param  TourSlotRepository  $tourSlotRepository
     */
    public function __construct(TourSlotRepository $tourSlotRepository)
    {
        $this->tourSlotRepository = $tourSlotRepository;
    }

    /**
     * Initialize the booking process
     * TODO: Need to optimize this method in future
     * @return array
     */
    public function initBooking(): array
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $vars = [
                'page_title' => 'Book Tour Package',
                'slotTimes' => [],
                'rateGroups' => [],
                'tourOperatorId' => 0,
                'tourPackageId' => 0,
                'date' => date('Y-m-d'),
                'uType' => $userType,
                'touristsGroups' => [],
                'timeSlotId' => 0,
                'serviceCommissionPercentage' => 0,
                'affiliateProcessingPercentage' => 0
            ];

            $tourOperators = $this->tourOperator::where('status', 'Active')
                ->get(['id', 'name', 'service_commission_percentage']);

            if ($userType == 'Admin') {
                if (Cookie::has('tourOpId')) {
                    $tourOperatorId = Cookie::get('tourOpId');
                } elseif (count($tourOperators)) {
                    $tourOperatorId = $tourOperators[0]->id;
                }
            }else{
                $tourOperatorId = auth()->user()->tour_operator_id ?? $tourOperators[0]->id;
            }


            if (request('customer_id')) {
                $customerData = $this->customer::find(request('customer_id'));
                $tourOperatorId = $customerData->tour_operator_id;
            }

            $vars['paymentacknowledgement'] = '';

            if (isset($_GET) && isset($_GET['payment_intent_client_secret']) && $_GET['payment_intent_client_secret'] != '' && isset($_GET['payment_intent']) && $_GET['payment_intent'] != '') {
                $bookingdetails = $this->tourBooking::where('stripe_paymentintent_id', $_GET['payment_intent_client_secret'])->where('status', 'Booked')->first();

                if ($bookingdetails) {
                    $transactionsexist = $this->transaction::where('tour_booking_id', $bookingdetails->id)->where('status', 'Completed')->first();
                    if (!$transactionsexist) {
                        try {
                            Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));
                            $paymentIntentDetails = PaymentIntent::retrieve(['id' => $_GET['payment_intent']]);
                            if (!empty($paymentIntentDetails)) {
                                if ($paymentIntentDetails->status == 'succeeded') {
                                    $splitpayment = (isset($paymentIntentDetails->charges->data[0]->metadata->split_payment))?$paymentIntentDetails->charges->data[0]->metadata->split_payment:0;
                                    $splitamount =  (isset($paymentIntentDetails->charges->data[0]->amount))?$paymentIntentDetails->charges->data[0]->amount/100:0;
                                    $feesamount =  (isset($paymentIntentDetails->charges->data[0]->application_fee_amount))?$paymentIntentDetails->charges->data[0]->application_fee_amount/100:0;
                                    $transactionId = DB::table('transactions')
                                        ->insertGetId([
                                            'tour_booking_id' => $bookingdetails->id,
                                            'amount' => $splitamount,
                                            'fee_amount' => $feesamount,
                                            'transaction_number' => !empty($_GET['payment_intent']) ? $_GET['payment_intent'] : 0,
                                            'application_fee_id' => !empty($paymentIntentDetails->charges->data[0]->application_fee) ? $paymentIntentDetails->charges->data[0]->application_fee : 0,
                                            'via' => 'Card',
                                            'response_init' => '',
                                            'response_end' => '',
                                            'response_end_datetime' => '0000-00-00 00:00:00',
                                            'payment_type' => ($splitpayment==1)? 1 : 0,
                                            'status' => 'Completed',
                                            'visitor' => request()->getClientIp(),
                                            'request_from' => request()->is('api/*') ? Constants::API : Constants::BACKEND,
                                            'created_by' => auth()->id(),
                                            'created_by_user_type' => $userType,
                                            'created_at' => Carbon::now(),
                                        ]);

                                    $crd = !empty($paymentIntentDetails->charges->data[0]->payment_method_details->card) ? $paymentIntentDetails->charges->data[0]->payment_method_details->card->last4 : '';
                                    $cardtype = !empty($paymentIntentDetails->charges->data[0]->payment_method_details->card->brand) ? $paymentIntentDetails->charges->data[0]->payment_method_details->card->brand : '';
                                    if ($userType == "Admin") {
                                        $user = $this->admin::find(auth()->id());
                                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                                    } elseif ($userType == "Tour Operator") {
                                        $user = $this->staff::find(auth()->id());
                                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                                    }

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $bookingdetails->id,
                                            'tourists' => $bookingdetails->tourists,
                                            'operation' => 'Payment',
                                            'details' => json_encode(['amount' => $splitamount, 'payment_type' => 'Card', 'card_number' => $crd,"card_type"=>$cardtype,'payment_method' => ($splitpayment==1) ? 'Split Payment' : 'Full Payment', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(),
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);

                                    if (!empty($data['note'])) {
                                        DB::table('tour_booking_history')
                                            ->insert([
                                                'tour_booking_id' => $bookingdetails->id,
                                                'operation' => 'Note',
                                                'details' => $bookingdetails->note,
                                                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                                'operation_by' => auth()->id(),
                                                'operation_by_user_type' => $userType,
                                                'updated_at' => Carbon::now(),
                                                'created_at' => Carbon::now(),
                                            ]);
                                    }

                                    $info = DB::table('tour_slot_times AS tst')
                                        ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                                        ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                                        ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                                        ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                                        ->join('states AS st', 'st.id', '=', 'ct.state_id')
                                        ->where('tst.id', $bookingdetails->tour_slot_id)
                                        ->select([
                                            'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                                            'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                                            't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                                            't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                                            'tp.stripe_destination_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                                            DB::raw("(SELECT SUM(tbm.tourists)
                                                FROM tour_bookings_master AS tbm
                                                WHERE tbm.tour_slot_id = tst.id
                                                    AND tbm.status != 'Cancelled'
                                                GROUP BY tbm.tour_slot_id) AS booked_seats")
                                        ]);

                                    if ($userType == 'Tour Operator') {
                                        $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                                    }

                                    $info = $info->first();

                                    // Below line is for to update tour booking history table
                                    $isSplitOrFull = ($splitpayment == 1) ? 'Split Payment' : 'Full Payment';
                                    $bookingdetails->booking_id = $bookingdetails->id;
                                    $bookingdetails->tour_operator_id = $info->tour_operator_id;

                                    CardBookingService::run((array) $bookingdetails, $transactionId, $historyId, $isSplitOrFull, request()->all());
                                    // dispatch(new CardBooking((array) $bookingdetails, $transactionId, $historyId, $isSplitOrFull, request()->all()));

                                    $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                                        ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                                        ->where('tbm.tour_booking_id', '=', $bookingdetails->id)
                                        ->select('tpr.rate_for', 'tbm.tourists')
                                        ->get();

                                    $totalTourists = 0;
                                    foreach ($tourListwithSeat as $value) {
                                        $totalTourists = $totalTourists + $value->tourists;
                                    }

                                    $customerdetails = $this->customer::find($bookingdetails->customer_id);
                                    $tour_d_t = $info->date . ' ' . $info->time_from;

                                    $arEmailData = [
                                        'bookingId' => $bookingdetails->id,
                                        'tourOperatorId' => $info->tour_operator_id,
                                        'tourOperator' => $info->tour_operator,
                                        'tourOpPhone' => $info->tour_op_phone,
                                        'tourOpEmail' => $info->tour_op_email,
                                        'tourOpWebsite' => $info->tour_op_website,
                                        'tourPackage' => $info->package_name,
                                        'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($tour_d_t)),
                                        'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($bookingdetails->id)]),
                                        'custName' => $customerdetails->name,
                                        'custEmail' => $customerdetails->email,
                                        'custPhone' => $customerdetails->phone_number,
                                        'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($bookingdetails->id)]),
                                        'importantNotes' => $info->important_notes,
                                        'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                                        'cancellationPolicy' => $info->cancellation_policy,
                                        'numberoftourist' => $totalTourists,
                                        'tourListwithSeat' => $tourListwithSeat,
                                        'packageEmail' => $info->packageEmail,
                                        'packageFromEmail' => $info->packageFromEmail,
                                        'packageApiKey' => $info->packageApiKey,
                                    ];

                                    if (!empty($info->tour_op_logo)) {
                                        if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                                            $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                                        } else {
                                            $tourOperatorLogo = asset('images/no-photo.png');
                                        }
                                        $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                                    }

                                    $tourPkgPhoto = DB::table('tour_package_photos')
                                        ->where('tour_package_id', $info->tour_package_id)
                                        ->where('placement', 2)
                                        ->where('status', 'Active')
                                        ->where('deleted_by', 0)
                                        ->latest()
                                        ->first(['path']);

                                    if ($tourPkgPhoto) {
                                        if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                                            $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                                        } else {
                                            $tourPackageLogo = asset('images/no-photo.png');
                                        }
                                        $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                                    }

                                    if(auth()->id() != 130) {
                                        $bookingdetails->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                                    }

                                    $vars['paymentacknowledgement'] = 'success';
                                } else {
                                    if ($userType == "Admin") {
                                        $user = $this->admin::find(auth()->id());
                                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                                    } elseif ($userType == "Tour Operator") {
                                        $user = $this->staff::find(auth()->id());
                                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                                    }
                                    $bookingdetails->status = 'Cancelled';
                                    $bookingdetails->updated_by = auth()->id();
                                    $bookingdetails->updated_by = $userType;
                                    $bookingdetails->save();
                                    $vars['paymentacknowledgement'] = 'failed';
                                }
                            } else {
                                $vars['paymentacknowledgement'] = 'wrongdetails';
                            }
                        } catch (CardException $ex) {
                            Log::error($ex);

                            DB::table('tour_booking_history')
                                ->insert([
                                    'tour_booking_id' => $bookingdetails->id,
                                    'operation' => 'Error',
                                    'details' => $ex->getMessage(),
                                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                    'operation_by' => auth()->id(),
                                    'operation_by_user_type' => $userType,
                                    'updated_at' => Carbon::now(),
                                    'created_at' => Carbon::now(),
                                ]);

                            $vars['paymentacknowledgement'] = 'failed';
                        }
                    }
                }
            }

            $date = date('Y-m-d');
            if ($userType == 'Tour Operator') {
                $touroperatordetails = $this->tourOperator::find(auth()->user()->tour_operator_id);
                $timezone = $touroperatordetails->timezone;
                $vars['currenttime'] = Carbon::now($timezone)->format('Y-m-d H:i:s');
                $converted_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now(), 'US/Arizona')
                    ->setTimezone($timezone)->format('Y-m-d');
                if ($converted_date != '') {
                    $date = $converted_date;
                }
            } else {
                $touroperatordetails = $this->tourOperator::find($tourOperatorId);
                $timezone = $touroperatordetails->timezone;
                $vars['currenttime'] = Carbon::now($timezone)->format('Y-m-d H:i:s');
                $converted_date = Carbon::createFromFormat('Y-m-d H:i:s', Carbon::now(), 'US/Arizona')
                    ->setTimezone($timezone)->format('Y-m-d');
                if ($converted_date != '') {
                    $date = $converted_date;
                }
            }

            $updateTourStatus = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_op', 't_op.id', '=', 'tp.tour_operator_id')
                ->where('tst.deleted_by', '=', '0')
                ->where('tsd.date', $date)
                ->where('t_op.id', $tourOperatorId)
                ->select(['tst.id', 'tp.duration', 'tp.day_hours', 'tsd.date', 'tst.time_from', 'tst.bookable_status', 't_op.timezone'])
                ->get()
                ->toArray();

            for ($a = 0; $a < count($updateTourStatus); $a++) {
                $currentTime = ($updateTourStatus[$a]->timezone != '') ? Carbon::now($updateTourStatus[$a]->timezone)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');
                $currentTime = Carbon::parse($currentTime);
                $dataTime = Carbon::parse($updateTourStatus[$a]->date . ' ' . $updateTourStatus[$a]->time_from);
                if ($dataTime >= $currentTime) {
                    $status = "Open";
                    if ($updateTourStatus[$a]->day_hours !== null && $updateTourStatus[$a]->duration !== 0) {
                        if ($updateTourStatus[$a]->day_hours == 1 || $updateTourStatus[$a]->day_hours == 2) {
                            if ($updateTourStatus[$a]->duration !== 0) {
                                $generatedTime = ($updateTourStatus[$a]->timezone != '') ? Carbon::now($updateTourStatus[$a]->timezone)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');
                                $now = Carbon::parse($updateTourStatus[$a]->date . ' ' . $updateTourStatus[$a]->time_from);
                                if ($updateTourStatus[$a]->day_hours == 1) {
                                    $hour = $updateTourStatus[$a]->duration * 24;
                                    $v = -1;
                                } else {
                                    $hour = $updateTourStatus[$a]->duration - 1;
                                    $v = -1;
                                }
                                $generatedTime = Carbon::parse($generatedTime);
                                $generatedTime = $generatedTime->modify('+' . $hour . 'hours');
                                $difference = $now->diffInHours($generatedTime, false);
                                if ($difference > $v) {
                                    $status = "Closed";
                                } else {
                                    $status = "Open";
                                }
                            } else {
                                $generatedTime = ($updateTourStatus[$a]->timezone != '') ? Carbon::now($updateTourStatus[$a]->timezone) : Carbon::now();
                                $now = Carbon::parse($updateTourStatus[$a]->date . ' ' . $updateTourStatus[$a]->time_from);
                                $generatedTime = Carbon::parse($generatedTime);
                                $difference = $now->diffInHours($generatedTime, false);
                                if ($difference > 0) {
                                    $status = "Closed";
                                } else {
                                    $status = "Open";
                                }
                            }
                        }
                    } else {
                        $generatedTime = ($updateTourStatus[$a]->timezone != '') ? Carbon::now($updateTourStatus[$a]->timezone) : Carbon::now();
                        $now = Carbon::parse($updateTourStatus[$a]->date . ' ' . $updateTourStatus[$a]->time_from);
                        $difference = $generatedTime->diffInMinutes($now);

                        $convertToDecimal = $difference / 60;
                        if ($convertToDecimal <= 0) {
                            $status = "Closed";
                        } else {
                            $status = "Open";
                        }
                    }
                    // Update status in database
                    if ($status == "Closed") {
                        DB::table('tour_slot_times')
                            ->where('id', '=', $updateTourStatus[$a]->id)
                            ->update([
                                'bookable_status' => $status
                            ]);
                    } elseif ($status == "Open") {
                    }
                } else {
                    $generatedTime = ($updateTourStatus[$a]->timezone != '') ? Carbon::now($updateTourStatus[$a]->timezone)->format('Y-m-d H:i:s') : Carbon::now()->format('Y-m-d H:i:s');
                    $now = Carbon::parse($updateTourStatus[$a]->date . ' ' . $updateTourStatus[$a]->time_from);

                    $generatedTime = Carbon::parse($generatedTime);
                    $difference = $now->diffInHours($generatedTime, false);
                    if ($difference > 0) {
                        DB::table('tour_slot_times')
                            ->where('id', '=', $updateTourStatus[$a]->id)
                            ->update([
                                'bookable_status' => 'Closed'
                            ]);
                    }
                }
            }

            if (request('time_slot_id')) {

                $tourInfo = DB::table('tour_slot_times AS tst')
                    ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                    ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                    ->where('tst.id', request('time_slot_id'))
                    ->first(['tp.tour_operator_id', 'tsd.tour_package_id', 'tsd.date']);

                $request = new Request;

                $request->replace([
                    'date' => str_replace('-', '', $tourInfo->date),
                    'tourPackageId' => $tourInfo->tour_package_id,
                    'getRateGroups' => 1
                ]);

                $tourSlotTime = $this->tourSlotRepository->getTourSlotTimesByDate((object) $request);

                $response = json_decode(json_encode($tourSlotTime));

                $vars = [
                        'slotTimes' => $response->slotTimes,
                        'rateGroups' => $response->rateGroups,
                        'date' => $tourInfo->date,
                        'tourPackageId' => $tourInfo->tour_package_id,
                        'tourOperatorId' => $tourInfo->tour_operator_id,
                        'timeSlotId' => request('time_slot_id')
                    ] + $vars;
            }

            if ($userType == 'Admin') {
                $tourPackages = $this->tourPackage::where('status', 'Active')
                    ->where('tour_operator_id', $tourOperatorId)
                    ->orderBy('Order_by')
                    ->get(['id', 'name', 'stripe_destination_id']);
                $vars['tourPackages'] = $tourPackages;
                $vars['tourOperators'] = $tourOperators;
                $vars['tourOperatorId'] = $tourOperatorId;
            } else {
                $tourPackages = $this->tourPackage::where('status', 'Active')
                    ->where('tour_operator_id', $tourOperatorId)
                    ->orderBy('Order_by')
                    ->get(['id', 'name', 'stripe_destination_id']);
                $vars['tourPackages'] = $tourPackages;
                $vars['tourOperatorId'] = $tourOperatorId;
                $vars['serviceCommissionPercentage'] = DB::table('tour_operators')
                    ->where('id', $vars['tourOperatorId'])
                    ->first(['service_commission_percentage'])
                    ->service_commission_percentage;
                $vars['affiliateProcessingPercentage'] = DB::table('tour_operators')
                    ->where('id', $vars['tourOperatorId'])
                    ->first(['affiliate_processing_percentage'])
                    ->affiliate_processing_percentage;
            }

            // book new tour for the customer
            $fields1 = ['c.id', 'c.name', 'c.email', 'c.phone_number'];
            $vars['tourOperator'] = $this->tourOperator::select('is_affiliate')->where('id', $tourOperatorId)->first();
            $customer = DB::table('customers AS c')
                ->where('is_affiliate', 'Yes')
                ->where('deleted_by', 0)
                ->where('status', 'Active')
                ->where('tour_operator_id', $tourOperatorId)
                ->get($fields1);

            $vars['affiliate'] = $customer;

            if (request('customer_id') && request('booking_id')) {
                $customer = DB::table('customers AS c')
                    ->join('tour_bookings_master AS tbm', 'tbm.customer_id', '=', 'c.id')
                    ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                    ->where('c.id', request('customer_id'))
                    ->where('tbm.id', request('booking_id'))
                    ->where('tp.tour_operator_id', $tourOperatorId);

                $fields = ['tbm.name', 'tbm.email', 'tbm.phone_number', 'tp.tour_operator_id', 'tbm.tour_package_id'];
                $customer = $customer->first($fields);

                $vars['customer'] = $customer;

                if (request('rebook')) {
                    $vars['tourOperatorId'] = $customer->tour_operator_id;
                    $vars['tourPackageId'] = $customer->tour_package_id;

                    $vars['touristsGroups'] = DB::table('tour_bookings_transact AS tbt')
                        ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                        ->where('tbt.tour_booking_id', request('booking_id'))
                        ->orderByDesc('tpr.age_from')
                        ->orderByDesc('tpr.age_to')
                        ->get(['tpr.rate_for', 'tbt.tour_package_rate_id', 'tbt.tourists']);

                    $bookData = DB::table('tour_bookings_master AS tbm')
                        ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                        ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                        ->where('tbm.id', request('booking_id'))
                        ->first(['tst.id as timeSlotId', 'tsd.date']);

                    $request = new Request;

                    $request->replace([
                        'date' => $bookData->date,
                        'tourPackageId' => $vars['tourPackageId'],
                        'getRateGroups' => 1
                    ]);

                    $tourSlotTime = $this->tourSlotRepository->getTourSlotTimesByDate((object) $request);

                    $response = json_decode(json_encode($tourSlotTime));

                    $vars = [
                            'date' => $bookData->date,
                            'slotTimes' => $response->slotTimes,
                            'rateGroups' => $response->rateGroups,
                            'timeSlotId' => $bookData->timeSlotId
                        ] + $vars;
                }
            }

            $data = $this->getTourOperatorData($tourOperatorId);

            if ($data === false) {
                throw new Exception('Unable to found data to create booking.');
            }

            $vars = $data + $vars;

            return $vars;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Get the tour booking operator data
     *
     * @return array
     */
    public function getTourOperatorData(int $id)
    {
        try {
            $tourPackages = $this->tourPackage::where('status', 'Active')
                ->where('tour_operator_id', $id)
                ->orderBy('order_by')
                ->get(['id', 'name', 'stripe_destination_id']);

            $staff = $this->staff::where('status', 'Active')
                ->where('tour_operator_id', $id)
                ->orderBy('first_name')
                ->orderBy('last_name')
                ->whereNull('deleted_at')
                ->get(['id', 'first_name', 'last_name']);

            $taxes = DB::table('taxes AS t')
                ->join('states AS s', 's.id', '=', 't.state_id')
                ->join('cities AS ct', 'ct.state_id', '=', 's.id')
                ->join('tour_operators AS t_o', 't_o.city_id', '=', 'ct.id')
                ->where('t.status', 'Active')
                ->where('t_o.id', $id)
                ->orderBy('t.name')
                ->get(['t.name', 't.value', 't.value_type']);

            return [
                'tourPackages' => $tourPackages,
                'staff' => $staff,
                'taxes' => $taxes,
            ];
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Create new booking
     *
     * @param object $request
     * @return array
     */
    public function createBooking($request)
    {

        DB::beginTransaction();
        try {
            if (isset($request->api) && $request->api == 1) {
                if(isset($request->permit_fee[0])) {
                    $request->permit_fee = explode(",", $request->permit_fee[0]);
                }
                if(isset($request->rate_group[0])) {
                    $request->rate_group = explode(",", $request->rate_group[0]);
                }
                if(isset($request->tourists[0])) {
                    $request->tourists = explode(",", $request->tourists[0]);
                }
            }

            $totalTourists = 0;

            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                $totalTourists += $request->tourists[$a];
            }

            if (!$totalTourists) {
                throw new Exception('Please select at least one tourist.');
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $request->slot_time)
                ->select([
                    'tst.time_from', 'tst.time_to', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'tp.stripe_destination_id', 't_o.id as tour_operator_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            if (!$info) {
                throw new Exception('Unable to book the tour package.');
            }

            if ($request->has('api') && $request->api == '1') {
                $paymentIntentId = ($request->has('payment_intent_id') && $request->payment_intent_id != '') ? $request->payment_intent_id : '';
                if ($info->date . ' ' . $info->time_from <= mysqlDT() && $paymentIntentId == '') {
                    throw new Exception("The tour's date-time has passed; booking isn't possible in the past.");
                }
            } else {
                if ($info->date . ' ' . $info->time_from <= mysqlDT()) {
                    throw new Exception("The tour's date-time has passed; booking isn't possible in the past.");
                }
            }

            $rateGrpIds = [];
            $permitfeeAr = [];

            for ($a = 0, $c = count($request->tourists); $a < $c; $a++) {
                if ($request->tourists[$a]) {
                    $x = $a;
                    $rateGrpIds[] = $request->rate_group[$a];
                    $permitfeeAr[] = $request->permit_fee[++$x];
                }
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->whereIn('tpr.id', $rateGrpIds)
                ->where('tpr.deleted_by', 0)
                //->orderByDesc('tpr.age_from')
                ->orderBy('tpr.orderby', 'asc')
                ->get([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage',
                    'tpr.processing_charge_percentage', 'tpr.additional_charge'
                ]);

            $customer = $this->customer::where('email', $request->customer_email)
                ->where('tour_operator_id', $info->tour_operator_id)
                ->first();

            if (!$customer && $request->affiliatecust == 0) {
                $pwdChars = '23456789~!@#$%^*()-=+_[]{};:?.,>abcdefghijkpqstuxyzABCDEFGHJKLMNPQRSTUXYZ';
                $pwd = '';

                for ($i = 0, $cl = strlen($pwdChars) - 1; $i < 10; $i++) {
                    $pwd .= $pwdChars[rand(0, $cl)];
                }

                $customer = $this->customer::create([
                    'tour_operator_id' => $info->tour_operator_id,
                    'name' => $request->customer_name,
                    'email' => $request->customer_email,
                    'phone_number' => str_replace(['(', ')', '+', '-', '.', ' '], '', $request->customer_phone_number),
                    'password' => Hash::make(env('APP_DEBUG') ? '123456' : $pwd),
                ]);
                DB::commit();

                $newCustomer = true;
            } else {
                $newCustomer = false;
            }

            $natFeesPercentage = 0;
            $stripeFeesPercentage = 0;
            if ($customer->is_affiliate == 'Yes') {
                $natFeesPercentage = $info->affiliate_processing_percentage;
                $stripeFeesPercentage = $info->stripe_affiliate_percentage;
            } else {
                $natFeesPercentage = $info->service_commission_percentage;
                $stripeFeesPercentage = $info->stripe_commission_percentage;
            }

            $total = $discountAmt = $serviceCommissionTotal = 0;
            $tbTransact = $touristGroups = [];

            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                if (!$request->tourists[$a])
                    continue;

                $tourists = $request->tourists[$a];
                $groupTotal = 0;

                for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                    if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                        $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                        break;
                    }
                }

                if ($b >= $bc) { // rate group not found
                    DB::rollBack();
                    throw new Exception('Unable to book the tour package.');
                }

                // rate may be zero for one or more age/rate group and
                // if rate is non-zero then only add other amounts
                // if ($rateGroups[$b]->rate != 0) {
                $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                        $rateGroups[$b]->additional_charge +
                        $permitfeeAr[$b]
                    ));

                if ($request->payment_method === 'Card') {
                    if ($request->has('discount_type') == false) {
                        $serviceCommissionPax = roundout((($rateGroups[$b]->rate + $rateGroups[$b]->additional_charge + $permitfeeAr[$b]) * $natFeesPercentage / 100), 2);
                        $serviceCommissionPax = $tourists * $serviceCommissionPax;
                        $serviceCommissionTotal += $serviceCommissionPax;
                    }
                    $groupTotal += ($groupTotal * (float)$rateGroups[$b]->processing_charge_percentage) / 100;
                }
                // }

                if ($request->subtt == null) {
                    $request->subtt = 0.00;
                }

                if ($request->fee == null) {
                    $request->fee = 0.00;
                }

                $tt = $request->subtt + $request->fee;

                $tbTransact[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'processing_charge_percentage' => (($request->payment_method !== 'Card') ? 0 : $rateGroups[$b]->processing_charge_percentage),
                    'nat_fees_percentage' => $natFeesPercentage,
                    'stripe_fees_percentage' => $stripeFeesPercentage,
                    'additional_tax_percentage' => ($rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => ($rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => $groupTotal,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];

                /* START - Added this to place the history of person added */
                $tbTransaction[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'rate_for' => $rateGroups[$b]->rate_for,
                    'processing_charge_percentage' => 0,
                    'nat_fees_percentage' => $natFeesPercentage,
                    'stripe_fees_percentage' => $stripeFeesPercentage,
                    'additional_tax_percentage' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => $groupTotal,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];
                /* END - Added this to place the history of person added */

                $total += $groupTotal;
            }

            if ($request->has('discount_type')) {
                if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                    $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                    $discountAmt = round($request->discount_fixed_money, 2);
                }
            }

            $total -= $discountAmt;
            if ($request->has('discount_type')) {
                if ($request->affiliatecust == 0) {
                    $serviceCommission = roundout(($total * $info->service_commission_percentage / 100), 2);
                } else {
                    $serviceCommission = roundout(($total * $info->affiliate_processing_percentage / 100), 2);
                }
            } else {
                $serviceCommission = $serviceCommissionTotal;
            }

            $total += $serviceCommission;
            $total = round($total, 2);

            $tourBooking = $this->tourBooking::create([
                'tour_package_id' => $info->tour_package_id,
                'tour_slot_id' => $request->slot_time,
                'customer_id' => $request->affiliatecust == 0 ? $customer->id : $request->affiliatecust,
                'tourists' => $totalTourists,
                'name' => $request->customer_name,
                'email' => $request->customer_email,
                'phone_number' => $request->customer_phone_number,
                'comments' => $request->customer_comment,
                'note' => ($request->has('tour_operator_note'))?$request->tour_operator_note:'',
                'note_datetime' => empty($request->tour_operator_note) ? '0000-00-00 00:00:00' : mysqlDT(),
                'mask_required' => 'Yes',
                'discount2_value' => $discountAmt,
                'discount2_percentage' => empty($request->discount_percentage) ? 0 : $request->discount_percentage,
                'service_commission' => ($request->payment_method == "Card") ? $request->fee : 0,
                'total' => ($request->payment_method == "Card") ? $tt : $request->subtt,
                'booking_total' => ($request->payment_method == "Card") ? $tt : $request->subtt,
                'status' => 'Booked',
                'created_by' => empty($request->booked_by_staff) ? auth()->id() : $request->booked_by_staff,
                'created_by_user_type' => empty($request->booked_by_staff) ? $userType : 'Tour Operator',
                'updated_at' => '0000-00-00 00:00:00',
            ]);
            DB::commit();

            if (!empty($request->customer_comment)) {
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $tourBooking->id,
                        'operation' => 'Comments',
                        'details' => $request->customer_comment,
                        'operation_at' => mysqlDT(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                DB::commit();
            }

            for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
            }

            DB::table('tour_bookings_transact')->insert($tbTransact);
            DB::commit();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            /* START - Added this to place the history of person added */
            $addedPeoplesText = array();
            if (!empty($tbTransaction)) {
                foreach ($tbTransaction as $tblTransaction) {
                    $addedPeoplesText[] = $tblTransaction['rate_for'] . " person " . $tblTransaction['tourists'];
                }
            }

            $operationTouristsAdded = [
                'tour_booking_id' => $tourBooking->id,
                'tourists' => $tourBooking->tourists,
                'operation' => 'Added',
                'details' => json_encode([
                    'sub_operation' => 'Tourists Added',
                    'discount_percentage' => $request->discount_percentage?:0,
                    'discount2_value' => $discountAmt,
                    'added' => $addedPeoplesText,
                    'newTourists' => $totalTourists,
                    'created_by_with_name' => $created_by_with_name
                ], JSON_NUMERIC_CHECK),
                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                'operation_by' => auth()->id(),
                'operation_by_user_type' => $userType,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ];

            DB::table('tour_booking_history')->insert($operationTouristsAdded);
            /* END - Added this to place the history of person added */

            if ($request->payment_method == 'Cash') {
                $cashPaidAmount = ($request->has('cash_paid_amount'))?$request->cash_paid_amount:'';
                $paymentAmountCash = !empty($request->split_value) ? $request->split_value : $request->subtt;
                $changeAmount = 0;
                if($cashPaidAmount!='' && $cashPaidAmount > 0 && $cashPaidAmount>$paymentAmountCash){
                    $changeAmount = $cashPaidAmount - $paymentAmountCash;
                    $changeAmount = round($changeAmount, 2);
                }
                $transactionId = DB::table('transactions')
                    ->insertGetId([
                        'tour_booking_id' => $tourBooking->id,
                        'amount' => !empty($request->split_value) ? $request->split_value : $request->subtt,
                        'transaction_number' => '',
                        'via' => 'Cash',
                        'created_at' => mysqlDT(),
                        'response_init' => '',
                        'response_end' => '',
                        'response_end_datetime' => '0000-00-00 00:00:00',
                        'payment_type' => !empty($request->split_value) ? 1 : 0,
                        'status' => 'Completed',
                        'visitor' => $request->getClientIp(),
                        'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();

                $historyId = DB::table('tour_booking_history')
                    ->insertGetId([
                        'tour_booking_id' => $tourBooking->id,
                        'tourists' => $tourBooking->tourists,
                        'change_amount'=> $changeAmount,
                        'operation' => 'Payment',
                        'details' => json_encode(['amount' => !empty($request->split_value) ? $request->split_value : $request->subtt, 'payment_type' => 'Cash', 'created_by_with_name' => $created_by_with_name, 'payment_method' => !empty($request->split_value) ? 'Split Payment' : 'Full Payment' ], JSON_NUMERIC_CHECK),
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();

                if (!empty($request->tour_operator_note)) {
                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $tourBooking->id,
                            'operation' => 'Note',
                            'details' => $request->tour_operator_note,
                            'operation_at' => mysqlDT(),
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();
                }

                // Below line is for to update tour booking history table
                $isSplitOrFull = !empty($request->split_value) ? 'Split Payment' : 'Full Payment';
                dispatch(new CashBooking($tourBooking, $transactionId, $historyId, $isSplitOrFull, $request->all()));
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }

        $errors = [];

        $paymentIntentInfo = [
            'clientSecret' => '',
            'transactionId' => '',
        ];

        try {
            $slotInfo = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->where('tst.id', $request->slot_time)
                ->when($userType == 'Tour Operator', function ($query) {
                    $query->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                })
                ->select([
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ])
                ->first();

            $booking = [
                'id' => $tourBooking->id,
                'cust_name' => $tourBooking->name,
                'pkg_id' => $info->tour_package_id,
                'pkg_name' => $info->package_name,
                'paid' => $request->payment_method,
                'total' => ($request->payment_method == "Card") ? $tt : $request->subtt,
                'tour_slot_id' => $request->slot_time,
                'tour_d_t' => $info->date . ' ' . $info->time_from,
                'booking_d_t' => $tourBooking->created_at,
                't_groups' => implode(' | ', $touristGroups),
                'tour_operator_id' => $info->tour_operator_id,
                'tourists' => $totalTourists,
                'is_affiliate' => 'No',
                'notif_for' => 'admin',
                'date' => $info->date,
                'time_from' => $info->time_from,
                'time_to' => $info->time_to,
                'seats' => $info->seats,
                'booked_seats' => $slotInfo->booked_seats,
                'broadcaster_user_id' => auth()->id(),
                'env' => config('app.env')
            ];

            BookingNew::dispatch($booking);
            $booking['notif_for'] = 'Tour Operator';
            BookingNew::dispatch($booking);

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $tourBooking->id)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $arEmailData = [
                'bookingId' => $booking['id'],
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($booking['id'])]),
                'custName' => $tourBooking->name,
                'custEmail' => $tourBooking->email,
                'custPhone' => $tourBooking->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($booking['id'])]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()
                ->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            if ($newCustomer) {
                $arEmailData['password'] = $pwd;
            }

            if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later') {
                // TODO
                if (auth()->id()!=130) {
                    $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            $errors[] = 'email';
        }
        $splitCount = 0;
        if($request->dividedBy != "Please select option"){
            $splitCount = $request->dividedBy;
        }
        if ($request->payment_method == 'Pay Later') {
            if (!empty($request->tour_operator_note)) {
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $tourBooking->id,
                        'operation' => 'Note',
                        'details' => $request->tour_operator_note,
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
            }

            return [
                'isAffiliate' => $customer->is_affiliate,
                'bookingId' => $tourBooking->id,
                'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                'stripeId' => $paymentIntentInfo['clientSecret'],
                'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                'errors' => $errors,
                'splitCount' => $splitCount,
            ];
        }

        $month = substr($request->expiration, 0, 2);
        $yearSet = substr($request->expiration, 3, 2);
        $y = 20;
        $year = $y . '' . $yearSet;
        $cardnumber = $request->cardnumber;
        $cvc = $request->cvv;
        $master = $this->tourBooking::where('id', $tourBooking->id)->first();

        if ($request->payment_method == 'Card') {
            $data = [
                'total' => $tt,
                'discount2_value' => $discountAmt,
                'customer_id' => $master->customer_id,
                'customer_name' => $master->name,
                'customer_email' => $master->email,
                'customer_phone' => $master->phone_number,
                'tourists' => $totalTourists,
                'booking_id' => $tourBooking->id,
                'tour_package' => $info->package_name,
                'date' => $info->date,
                'time_from' => $info->time_from,
                'subtotal' => $request->subtt,
                'serviceCommission' => $request->fee,
                'month' => $month,
                'year' => $year,
                'cardnumber' => $cardnumber,
                'cvc' => $cvc,
                'destination' => $info->stripe_destination_id,
                'slot_time' => $request->slot_time,
                'tour_operator_id' => $info->tour_operator_id,
                'note' => $request->tour_operator_note,
                'booked_by_staff' => $request->booked_by_staff,
                'split_value' => $request->split_value,
                'api' => $request->api,
                'splitCount' => $request->dividedBy,
            ];

            if ($request->has('api') && $request->api == '1') {
                $paymentIntentId= ($request->has('payment_intent_id') && $request->payment_intent_id!='')?$request->payment_intent_id:'';
                if ($paymentIntentId == '') {
                    throw new Exception('You must provide Stripe Payment Intent Id');
                }

                $data['payment_intent_id'] = $paymentIntentId;
                $paymentIntentInfo = $this->createBookingCard($data);
            } else {
                $paymentIntentInfo = $this->createPaymentIntent($data);
            }

            if (isset($paymentIntentInfo['nextAction'])) {
                $master->stripe_paymentintent_id = $paymentIntentInfo['clientSecret'];
                $master->save();
                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'require3ds' => 1,
                    'nextAction' => $paymentIntentInfo['nextAction'],
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if ($paymentIntentInfo['clientSecret'] == '') {
                $errors[] = 'payment';
            }

            if (!empty($paymentIntentInfo['ex'])) {
                $errors_message = $paymentIntentInfo['ex']->getError()->message;
            }
        }
        $splitCount = 0;
        if($request->dividedBy != "Please select option"){
            $splitCount = $request->dividedBy;
        }

        return [
            'isAffiliate' => $customer->is_affiliate,
            'bookingId' => $tourBooking->id,
            'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
            'stripeId' => $paymentIntentInfo['clientSecret'],
            'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
            'errors' => $errors,
            'errors_message' => !empty($errors_message) ? $errors_message : '',
            'splitCount' => $splitCount,
        ];
    }

    /**
     * Create payment intent for the new booking
     *
     * @param object $data
     * @return array
     */
    public function createPaymentIntent($data)
    {

        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));

            $tour_operator_name = '';
            if ($data['tour_operator_id'] == 1) {
                $tour_operator_name = "Dixies Lower Antelope";
            }

            if ($data['tour_operator_id'] == 2) {
                $tour_operator_name = "DD MTA";
            }

            if ($data['tour_operator_id'] == 3) {
                $tour_operator_name = "John Rambo Adventures";
            }

            if ($data['tour_operator_id'] == 4) {
                $tour_operator_name = "L.A.C.T. Shuttle LLC";
            }

            if ($data['split_value']) {
                $data['total'] = $data['split_value'];
            }

            $oldBookingID = config('constants.old_bookings.booking_id');
            $data['total'] = !empty($data['total']) ? $data['total'] : 0;
            $customer = $this->customer::find($data['customer_id']);
            if($data['booking_id'] < $oldBookingID){
                if ($customer->is_affiliate === 'Yes') {
                    $destination_amount = ($data['total'] - ($data['total'] * 2.9 / 100));
                } else {
                    $destination_amount = ($data['total'] - ($data['total'] * 8.9 / 100));
                }
                $data['destination'] = 'acct_1KiPPfF2s9WqF2Qr';
            } else {
                $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $data['booking_id'])->first();
                $destination_amount = ($data['total'] - ($data['total'] * $tourBookingTransaction->stripe_fees_percentage / 100));
            }

            $destination_amount = $data['total'] - $destination_amount;
            $destination_amount = round($destination_amount, 2);
            $data['serviceCommission'] = $destination_amount;

            if ($data['total'] != 0) {
                if ($customer->stripe_id == null) {
                    $stripeCustomer = StripeCustomer::create([
                        'name' => $customer->name,
                        'email' => $customer->email,
                        'phone' => $customer->phone_number
                    ]);

                    $customer->stripe_id = $stripeCustomer->id;
                    $customer->save();
                }

                $method = PaymentMethod::create([
                    'type' => 'card',
                    'card' => [
                        'number' => $data['cardnumber'],
                        'exp_month' => $data['month'],
                        'exp_year' => $data['year'],
                        'cvc' => $data['cvc'],
                    ],
                ]);
                DB::commit();

                $paymentIntent = PaymentIntent::create([
                    'payment_method_types' => ['card'],
                    'payment_method' => $method->id,
                    'currency' => 'usd',
                    'amount' => $data['total'] * 100, // stripe needs amount in cents
                    'description' => 'Booking Id: ' . $data['booking_id'] .
                        "\nTour Package: " . $data['tour_package'] .
                        "\nTour Date: " . $data['date'] .
                        "\nTour Time: " . $data['time_from'] .
                        "\nTourists: " . $data['tourists'] .
                        "\nEmail: " . $data['customer_email'],
                    'metadata' => [
                        'Booking Id' => $data['booking_id'],
                        'Tour Package' => $data['tour_package'],
                        'Tour Date' => $data['date'],
                        'Tour Time' => $data['time_from'],
                        'Tourists' => $data['tourists'],
                        'Customer Name' => $data['customer_name'],
                        'Customer Phone' => $data['customer_phone'],
                        'Email' => $data['customer_email'],
                        'split_payment'=>($data['split_value'])?1:0
                    ],
                    'statement_descriptor' => $tour_operator_name,
                    'confirm' => true,
                    'application_fee_amount' => $destination_amount * 100,
                    'transfer_data' => [
                        'destination' => $data['destination'],
                    ],
                    'customer' => $customer->stripe_id,
                ], ['idempotency_key' => $data['booking_id'] . carbon::now()]);

                if (isset($paymentIntent) && $paymentIntent->status == 'requires_action') {
                    $stripe = new StripeClient(
                        env('STRIPE_API_SECRET_KEY')
                    );
                    $paymentIntentConfirm = $stripe->paymentIntents->confirm(
                        $paymentIntent->id,
                        [
                            'return_url' => route(prefixedRouteName('tour-operator.booking.create')),
                            'payment_method_options' => [
                                'card' => [
                                    'request_three_d_secure' => 'any'
                                ]
                            ]
                        ]
                    );

                    return [
                        'clientSecret' => !empty($paymentIntentConfirm->client_secret) ? $paymentIntentConfirm->client_secret : 123,
                        'intentId' => !empty($paymentIntentConfirm->id) ? $paymentIntentConfirm->id : '',
                        'nextAction' => $paymentIntentConfirm->next_action
                    ];
                }
            }

            $crd = substr($data['cardnumber'], -4); //substr($data['cardnumber'], 12, 16);

            $transactionId = DB::table('transactions')
                ->insertGetId([
                    'tour_booking_id' => $data['booking_id'],
                    'amount' => $data['total'],
                    'fee_amount' => $data['serviceCommission'],
                    'transaction_number' => !empty($paymentIntent->id) ? $paymentIntent->id : '',
                    'application_fee_id' => !empty($paymentIntent->charges->data[0]->application_fee) ? $paymentIntent->charges->data[0]->application_fee : '',
                    'via' => 'Card',
                    'response_init' => '',
                    'response_end' => '',
                    'response_end_datetime' => '0000-00-00 00:00:00',
                    'payment_type' => !empty($data['split_value']) ? 1 : 0,
                    'status' => 'Completed',
                    'visitor' => request()->getClientIp(),
                    'request_from' => request()->is('api/*') ? Constants::API : Constants::BACKEND,
                    'created_by' => empty($data['booked_by_staff']) ? auth()->id() : $data['booked_by_staff'],
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ]);
            DB::commit();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }
            $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
            $tourBookingDetails = $this->tourBooking::find($data['booking_id']);
            $historyId = DB::table('tour_booking_history')
                ->insertGetId([
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' => $tourBookingDetails->tourists,
                    'operation' => 'Payment',
                    'details' => json_encode(['amount' => $data['total'], 'payment_type' => 'Card', 'card_number' => $crd,'card_type'=>$cardtype,'payment_method' => !empty($data['split_value']) ? 'Split Payment' : 'Full Payment', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);
            DB::commit();
            if(isset($data['add_people']) && $data['add_people']==1){

                $operationTouristsAdded = [
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' => $tourBookingDetails->tourists,
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Added', 'discount_percentage' => $data['discount_percentage']?:0,'discount2_value' => $data['discount2_value']?:0,'added'=>(isset($data['addedpeoplestexts']))?$data['addedpeoplestexts']:'','newTourists' => $data['newTourists'], 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];
                DB::table('tour_booking_history')
                    ->insert($operationTouristsAdded);
                DB::commit();
            }
            if(isset($data['change_people']) && $data['change_people']==1) {
                $operationTouristsChanged = [
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' => $data['tourists'],
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Changed', 'discount_percentage' => $data['discount_percentage'] ?: 0, 'discount2_value' => $data['discount2_value'], 'added' => (isset($data['addedpeoplesetexts']))?$data['addedpeoplesetexts']:'', 'deleted' => (isset($data['deletedpeoplestexts']))?$data['deletedpeoplestexts']:'', 'addedTourists' => (isset($data['addedtourists']))?$data['addedtourists']:'', 'deletedTourits' => (isset($data['deletedtourits']))?$data['deletedtourits']:'', 'paymenttotal' => (isset($data['paymenttotal']))?$data['paymenttotal']:'', 'refundtotal' => (isset($data['refundtotal']))?$data['refundtotal']:'', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];
                DB::table('tour_booking_history')
                    ->insert($operationTouristsChanged);
                DB::commit();
            }
            if (!empty($data['note'])) {
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $data['booking_id'],
                        'operation' => 'Note',
                        'details' => $data['note'],
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();
            }

            // Below line is for to update tour booking history table
            $isSplitOrFull = !empty($data['split_value']) ? 'Split Payment' : 'Full Payment';
            CardBookingService::run($data, $transactionId, $historyId, $isSplitOrFull, request()->all());
            // dispatch(new CardBooking($data, $transactionId, $historyId, $isSplitOrFull, request()->all()));

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $data['slot_time'])
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy', 't_o.service_commission_percentage', 'tp.stripe_destination_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $data['booking_id'])
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }

            $tour_d_t = $info->date . ' ' . $info->time_from;

            $arEmailData = [
                'bookingId' => $data['booking_id'],
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($tour_d_t)),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($data['booking_id'])]),
                'custName' => $data['customer_name'],
                'custEmail' => $data['customer_email'],
                'custPhone' => $data['customer_phone'],
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($data['booking_id'])]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()
                ->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }
            if(auth()->id()!=130) {
                // TODO
                $tourBooking = $this->tourBooking::find($data['booking_id']);
                $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
            }
            return [
                'clientSecret' => !empty($paymentIntent->client_secret) ? $paymentIntent->client_secret : 123,
                'transactionId' => $transactionId,
            ];
        } catch (CardException $exception) {
            Log::error($exception);
            DB::rollBack();

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $data['booking_id'],
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            return [
                'clientSecret' => '',
                'transactionId' => '',
                'ex' => $exception,
            ];
        }
    }

    /**
     * Create payment using card for the new booking from mobile app
     *
     * @param object $data
     * @return array
     */
    public function createBookingCard($data)
    {
        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $stripe = new StripeClient(
                env('STRIPE_API_SECRET_KEY')
            );

            if($data['split_value']){
                $data['total'] = $data['split_value'];
            }

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $data['booking_id'])->first();
            $oldBookingID = config('constants.old_bookings.booking_id');
            if($data['booking_id'] < $oldBookingID){
                $customer = $this->customer::find($data['customer_id']);
                if ($customer->is_affiliate === 'Yes') {
                    $destination_amount = ($data['total'] - ($data['total'] * 2.9 / 100));
                } else {
                    $destination_amount = ($data['total'] - ($data['total'] * 8.9 / 100));
                }
            }else {
                $destination_amount = ($data['total'] - ($data['total'] * $tourBookingTransaction->stripe_fees_percentage / 100));
            }

            $destination_amount = $data['total'] - $destination_amount;
            $destination_amount = round($destination_amount, 2);
            $data['serviceCommission'] = $destination_amount;

            $paymentIntentId = $data['payment_intent_id'];

            $paymentIntent = $stripe->paymentIntents->retrieve(
                $paymentIntentId,
                []
            );

            // This condition added to prevent the duplicate bookings
            if (!isset($data['after_capture'])) {
                $stripe->paymentIntents->update(
                    $paymentIntentId,
                    [
                        'description' => 'Booking Id: ' . $data['booking_id'] .
                            "\nTour Package: " . $data['tour_package'] .
                            "\nTour Date: " . $data['date'] .
                            "\nTour Time: " . $data['time_from'] .
                            "\nTourists: " . $data['tourists'] .
                            "\nEmail: " . $data['customer_email'],
                        'metadata' => [
                            'Booking Id' => $data['booking_id'],
                            'Tour Package' => $data['tour_package'],
                            'Tour Date' => $data['date'],
                            'Tour Time' => $data['time_from'],
                            'Tourists' => $data['tourists'],
                            'Customer Name' => $data['customer_name'],
                            'Customer Phone' => $data['customer_phone'],
                            'Email' => $data['customer_email'],
                        ]
                    ], ['idempotency_key' => $data['booking_id'] . carbon::now()]
                );
            }

            $crd = '';
            $cardtype = '';
            if (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'])) {
                $crd = (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'])) ? $paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'] : '';
                $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card_present->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card_present->brand : '';
            } else if(isset($paymentIntent->charges->data[0]->payment_method_details['card']['last4'])){
                $crd = (isset($paymentIntent->charges->data[0]->payment_method_details['card']['last4'])) ? $paymentIntent->charges->data[0]->payment_method_details['card']['last4'] : '';
                $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
            }

            $transactionId = DB::table('transactions')
                ->insertGetId([
                    'tour_booking_id' => $data['booking_id'],
                    'amount' => $data['total'],
                    'fee_amount' => $data['serviceCommission'],
                    'transaction_number' => !empty($paymentIntent->id) ? $paymentIntent->id : '',
                    'application_fee_id' => !empty($paymentIntent->charges->data[0]->application_fee) ? $paymentIntent->charges->data[0]->application_fee : '',
                    'via' => 'Card',
                    'response_init' => '',
                    'response_end' => '',
                    'response_end_datetime' => '0000-00-00 00:00:00',
                    'status' => 'Completed',
                    'payment_type' => !empty($data['split_value']) ? 1 : 0,
                    'visitor' => request()->getClientIp(),
                    'request_from' => request()->is('api/*') ? Constants::API : Constants::BACKEND,
                    'created_by' => empty($data['booked_by_staff']) ? auth()->id() : $data['booked_by_staff'],
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ]);
            DB::commit();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $tourBookingDetails = $this->tourBooking::find($data['booking_id']);

            $historyId = DB::table('tour_booking_history')
                ->insertGetId([
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' =>$tourBookingDetails->tourists,
                    'operation' => 'Payment',
                    'details' => json_encode(['amount' => $data['total'], 'payment_type' => 'Card', 'card_number' => $crd,'card_type'=>$cardtype,'payment_method' => !empty($data['split_value']) ? 'Split Payment' : 'Full Payment', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);
            DB::commit();

            if(isset($data['add_people']) && $data['add_people']==1){
                $tourBooking = $this->tourBooking::find($data['booking_id']);
                $operationTouristsAdded = [
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' => $tourBookingDetails->tourists,
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Added', 'discount_percentage' => $data['discount_percentage']?:0,'discount2_value' => $data['discount2_value']?:0,'added'=>(isset($data['addedpeoplestexts']))?$data['addedpeoplestexts']:'','newTourists' => $data['newTourists'], 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];
                DB::table('tour_booking_history')
                    ->insert($operationTouristsAdded);
                DB::commit();
            }

            if(isset($data['change_people']) && $data['change_people']==1) {
                $operationTouristsChanged = [
                    'tour_booking_id' => $data['booking_id'],
                    'tourists' => $data['tourists'],
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Changed', 'discount_percentage' => $data['discount_percentage'] ?: 0, 'discount2_value' => $data['discount2_value'], 'added' => (isset($data['addedpeoplesetexts']))?$data['addedpeoplesetexts']:'', 'deleted' => (isset($data['deletedpeoplestexts']))?$data['deletedpeoplestexts']:'', 'addedTourists' => (isset($data['addedtourists']))?$data['addedtourists']:'', 'deletedTourits' => (isset($data['deletedtourits']))?$data['deletedtourits']:'', 'paymenttotal' => (isset($data['paymenttotal']))?$data['paymenttotal']:'', 'refundtotal' => (isset($data['refundtotal']))?$data['refundtotal']:'', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];
                DB::table('tour_booking_history')
                    ->insert($operationTouristsChanged);
                DB::commit();
            }

            if (!empty($data['note'])) {
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $data['booking_id'],
                        'operation' => 'Note',
                        'details' => $data['note'],
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();
            }

            // Below line is for to update tour booking history table
            $isSplitOrFull = !empty($data['split_value']) ? 'Split Payment' : 'Full Payment';
            CardBookingService::run($data, $transactionId, $historyId, $isSplitOrFull, request()->all());
            // dispatch(new CardBooking($data, $transactionId, $historyId, $isSplitOrFull, request()->all()));

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $data['slot_time'])
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.service_commission_percentage',
                    'tp.stripe_destination_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $data['booking_id'])
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }

            $tour_d_t = $info->date . ' ' . $info->time_from;

            $arEmailData = [
                'bookingId' => $data['booking_id'],
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($tour_d_t)),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($data['booking_id'])]),
                'custName' => $data['customer_name'],
                'custEmail' => $data['customer_email'],
                'custPhone' => $data['customer_phone'],
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($data['booking_id'])]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()
                ->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            // TODO
            $tourBooking = $this->tourBooking::find($data['booking_id']);
            if(auth()->id()!=130) {
                $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
            }
            return [
                'clientSecret' => !empty($paymentIntent->client_secret) ? $paymentIntent->client_secret : 123,
                'transactionId' => $transactionId,
            ];
        } catch (CardException $exception) {
            Log::error($exception);
            DB::rollBack();

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $data['booking_id'],
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            return [
                'clientSecret' => '',
                'transactionId' => '',
                'ex' => $exception,
            ];
        }
    }

    /**
     * Get booking information
     *
     * @param int $id
     * @param bool $forPrinting
     * @return array
     */
    public function booking(int $id, $forPrinting)
    {
        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';
            $paymentacknowledgement = '';
            if (isset($_GET) && isset($_GET['payment_intent_client_secret']) && $_GET['payment_intent_client_secret'] != '' && isset($_GET['payment_intent']) && $_GET['payment_intent'] != '') {
                $bookingdetails = $this->tourBooking::where('stripe_paymentintent_id', $_GET['payment_intent_client_secret'])->where('status', 'Booked')->first();
                if ($bookingdetails) {
                    $transactionsexist = $this->transaction::where('tour_booking_id', $bookingdetails->id)->where('status', 'Completed')->first();
                    if ((!$transactionsexist) || (!empty($transactionsexist) && $transactionsexist->payment_type=='1')) {
                        Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));
                        $paymentIntentDetails = PaymentIntent::retrieve(['id' => $_GET['payment_intent']]);
                        if (!empty($paymentIntentDetails)) {
                            if ($paymentIntentDetails->status == 'succeeded') {
                                $splitpayment = (isset($paymentIntentDetails->charges->data[0]->metadata->split_payment))?$paymentIntentDetails->charges->data[0]->metadata->split_payment:0;
                                $splitamount =  (isset($paymentIntentDetails->charges->data[0]->amount))?$paymentIntentDetails->charges->data[0]->amount/100:0;
                                $feesamount =  (isset($paymentIntentDetails->charges->data[0]->application_fee_amount))?$paymentIntentDetails->charges->data[0]->application_fee_amount/100:0;
                                DB::table('transactions')
                                    ->insertGetId([
                                        'tour_booking_id' => $bookingdetails->id,
                                        'amount' => $splitamount,
                                        'fee_amount' => $feesamount,
                                        'transaction_number' => !empty($_GET['payment_intent']) ? $_GET['payment_intent'] : 0,
                                        'application_fee_id' => !empty($paymentIntentDetails->charges->data[0]->application_fee) ? $paymentIntentDetails->charges->data[0]->application_fee : 0,
                                        'via' => 'Card',
                                        'response_init' => '',
                                        'response_end' => '',
                                        'response_end_datetime' => '0000-00-00 00:00:00',
                                        'payment_type' => ($splitpayment==1) ? 1 : 0,
                                        'status' => 'Completed',
                                        'visitor' => request()->getClientIp(),
                                        'request_from' => request()->is('api/*') ? Constants::API : Constants::BACKEND,
                                        'created_by' => auth()->id(),
                                        'created_by_user_type' => $userType,
                                        'created_at' => Carbon::now(),
                                    ]);
                                DB::commit();

                                $crd = !empty($paymentIntentDetails->charges->data[0]->payment_method_details->card) ? $paymentIntentDetails->charges->data[0]->payment_method_details->card->last4 : '';
                                $cardtype = !empty($paymentIntentDetails->charges->data[0]->payment_method_details->card->brand) ? $paymentIntentDetails->charges->data[0]->payment_method_details->card->brand : '';
                                if ($userType == "Admin") {
                                    $user = $this->admin::find(Auth::id());
                                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                                } elseif ($userType == "Tour Operator") {
                                    $user = $this->staff::find(Auth::id());
                                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                                }

                                DB::table('tour_booking_history')
                                    ->insert([
                                        'tour_booking_id' => $bookingdetails->id,
                                        'tourists' => $bookingdetails->tourists,
                                        'operation' => 'Payment',
                                        'details' => json_encode(['amount' => $splitamount, 'payment_type' => 'Card', 'card_number' => $crd,'card_type'=>$cardtype,'payment_method' => ($splitpayment==1) ? 'Split Payment' : 'Full Payment', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                        'operation_by' => auth()->id(),
                                        'operation_by_user_type' => $userType,
                                        'updated_at' => Carbon::now(),
                                        'created_at' => Carbon::now(),
                                    ]);
                                DB::commit();

                                if (!empty($data['note'])) {
                                    DB::table('tour_booking_history')
                                        ->insert([
                                            'tour_booking_id' => $bookingdetails->id,
                                            'operation' => 'Note',
                                            'details' => $bookingdetails->note,
                                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                }

                                $info = DB::table('tour_slot_times AS tst')
                                    ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                                    ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                                    ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                                    ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                                    ->join('states AS st', 'st.id', '=', 'ct.state_id')
                                    ->where('tst.id', $bookingdetails->tour_slot_id)
                                    ->select([
                                        'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                                        'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                                        't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy', 't_o.service_commission_percentage', 'tp.stripe_destination_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                                        DB::raw("(SELECT SUM(tbm.tourists)
                                            FROM tour_bookings_master AS tbm
                                            WHERE tbm.tour_slot_id = tst.id
                                                AND tbm.status != 'Cancelled'
                                            GROUP BY tbm.tour_slot_id) AS booked_seats")
                                    ]);

                                if ($userType == 'Tour Operator') {
                                    $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                                }

                                $info = $info->first();

                                $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                                    ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                                    ->where('tbm.tour_booking_id', '=', $bookingdetails->id)
                                    ->select('tpr.rate_for', 'tbm.tourists')
                                    ->get();

                                $totalTourists = 0;
                                foreach ($tourListwithSeat as $value) {
                                    $totalTourists = $totalTourists + $value->tourists;
                                }

                                $customerdetails = $this->customer::find($bookingdetails->customer_id);
                                $tour_d_t = $info->date . ' ' . $info->time_from;

                                $arEmailData = [
                                    'bookingId' => $bookingdetails->id,
                                    'tourOperatorId' => $info->tour_operator_id,
                                    'tourOperator' => $info->tour_operator,
                                    'tourOpPhone' => $info->tour_op_phone,
                                    'tourOpEmail' => $info->tour_op_email,
                                    'tourOpWebsite' => $info->tour_op_website,
                                    'tourPackage' => $info->package_name,
                                    'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($tour_d_t)),
                                    'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($bookingdetails->id)]),
                                    'custName' => $customerdetails->name,
                                    'custEmail' => $customerdetails->email,
                                    'custPhone' => $customerdetails->phone_number,
                                    'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($bookingdetails->id)]),
                                    'importantNotes' => $info->important_notes,
                                    'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                                    'cancellationPolicy' => $info->cancellation_policy,
                                    'numberoftourist' => $totalTourists,
                                    'tourListwithSeat' => $tourListwithSeat,
                                    'packageEmail' => $info->packageEmail,
                                    'packageFromEmail' => $info->packageFromEmail,
                                    'packageApiKey' => $info->packageApiKey,
                                ];

                                if (!empty($info->tour_op_logo)) {
                                    if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                                        $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                                    } else {
                                        $tourOperatorLogo = asset('images/no-photo.png');
                                    }
                                    $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                                }

                                $tourPkgPhoto = DB::table('tour_package_photos')
                                    ->where('tour_package_id', $info->tour_package_id)
                                    ->where('placement', 2)
                                    ->where('status', 'Active')
                                    ->where('deleted_by', 0)
                                    ->latest()
                                    ->first(['path']);

                                if ($tourPkgPhoto) {
                                    if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                                        $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                                    } else {
                                        $tourPackageLogo = asset('images/no-photo.png');
                                    }
                                    $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                                }
                                if(auth()->id()!=130){
                                    $bookingdetails->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                                }
                                $paymentacknowledgement = 'success';
                            } else {
                                if ($userType == "Admin") {
                                    $user = $this->admin::find(Auth::id());
                                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                                } elseif ($userType == "Tour Operator") {
                                    $user = $this->staff::find(Auth::id());
                                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                                }

                                $bookingdetails->status = 'Cancelled';
                                $bookingdetails->updated_by = auth()->id();
                                $bookingdetails->updated_by = $userType;
                                $bookingdetails->save();
                                DB::commit();

                                $paymentacknowledgement = 'failed';
                            }
                        } else {
                            $paymentacknowledgement = 'wrongdetails';
                        }
                    }
                }
            }
            if (request()->is('api/*')) {
                $selectFields = [
                    'tbm.id', 'tbm.tour_package_id', 'tbm.tourists', 'tbm.comments', 'tbm.status as statusm', 'tbm.discount', 'tbm.discount2_value', 'tbm.discount2_percentage', 'tbm.service_commission', 'tbm.total', 'tbm.note', 'tbm.created_at', 'tbm.created_by', 'tbm.created_by_user_type', 'tbm.liability_waiver_signing',
                    'tp.tour_operator_id', 'tp.name AS tour_package', 'tp.stripe_destination_id', 't.status', 't.via',
                    'tsd.date', 'tst.id as time_id', 'tst.time_from', 'tst.time_to',
                    'tbm.customer_id', 'tbm.name AS customer', 'tbm.email AS cust_email', 'tbm.phone_number AS cust_phone', 'c.is_affiliate', 'tbm.created_by_user_type', 'tbm.booking_total',
                    'tsd.seats AS slot_seats', 'tbm.reference_booking'
                ];
            } else {
                $selectFields = [
                    'tbm.id', 'tbm.tour_package_id', 'tbm.tourists', 'tbm.comments', 'tbm.status as statusm', 'tbm.discount', 'tbm.discount2_value', 'tbm.discount2_percentage', 'tbm.service_commission', 'tbm.total', 'tbm.note', 'tbm.created_at', 'tbm.created_by', 'tbm.created_by_user_type', 'tbm.liability_waiver_signing',
                    'tp.tour_operator_id', 'tp.name AS tour_package', 'tp.stripe_destination_id', 'promo.discount_value', 'promo.discount_value_type', 't.status', 't.via',
                    'tsd.date', 'tst.id as time_id', 'tst.time_from', 'tst.time_to','tst.id AS tour_slot_time_id','tsd.seats AS slot_seats','tst.bookable_status AS slot_bookable_status',
                    'tbm.reference_booking',
                    DB::raw("IFNULL((SELECT SUM(tbm2.tourists)
						FROM tour_bookings_master AS tbm2
						WHERE tbm2.tour_slot_id = tst.id
							AND tbm2.status != 'Cancelled'
						GROUP BY tbm2.tour_slot_id), 0) AS slot_all_tourists_count"),
                    'tbm.customer_id', 'tbm.name AS customer', 'tbm.email AS cust_email', 'tbm.phone_number AS cust_phone', 'c.is_affiliate', 'c.is_price_show', 'tbm.created_by_user_type', 'tbm.booking_total'
                ];
            }

            $qry = DB::table('tour_bookings_master AS tbm')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                ->leftJoin('transactions As t', 't.tour_booking_id', '=', 'tbm.id');

            if (!request()->is('api/*')) {
                $qry->leftJoin('tour_promotions AS promo', 'promo.id', '=', 'tbm.tour_promotion_id');
            }

            $qry->where('tbm.id', $id);

            if ($userType=='Admin' || $forPrinting || request()->is('api/*')) {
                $qry->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id');
                $selectFields[] = 't_o.name AS tour_operator';
                $selectFields[] = 't_o.timezone AS timezone';
                $selectFields[] = 't_o.logo AS tour_op_logo';
                $selectFields[] = 't_o.logo_print AS tour_op_logo_print';
                $selectFields[] = 't_o.email AS tour_op_email';
                $selectFields[] = 't_o.phone_number AS tour_op_phone';
                $selectFields[] = 't_o.website AS tour_op_website';
            }else{
                $qry->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id');
                $selectFields[] = 't_o.name AS tour_operator';
            }

            if ($userType == 'Tour Operator') {
                $qry = $qry->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $bookingM = $qry->first($selectFields);

            if (!$bookingM) {
                throw new Exception("Tour booking not found.");
            }

            $touristsCount = 0;
            if (request()->is('api/*')) {
                $tourists = DB::select("SELECT SUM(tbm2.tourists) as tourists_count FROM tour_bookings_master AS tbm2 WHERE tbm2.tour_slot_id = '".$bookingM->time_id."' AND tbm2.status != 'Cancelled' GROUP BY tbm2.tour_slot_id");
                $touristsCount = (isset($tourists[0]->tourists_count)) ? $tourists[0]->tourists_count : 0;
            }

            if ($userType == 'Admin') {
                $created_at = $bookingM->created_at;
                $timezone = $bookingM->timezone;
                $converted_date = Carbon::createFromFormat('Y-m-d H:i:s', $created_at, 'US/Arizona')
                    ->setTimezone($bookingM->timezone)->format('Y-m-d H:i:s');
                if ($converted_date != '') {
                    $bookingM->created_at = $converted_date;
                }
            } else if ($userType == 'Tour Operator') {
                $touroperatordetails = $this->tourOperator::find(auth()->user()->tour_operator_id);
                $timezone = $touroperatordetails->timezone;
                $created_at = $bookingM->created_at;
                $converted_date = Carbon::createFromFormat('Y-m-d H:i:s', $created_at, 'US/Arizona')
                    ->setTimezone($timezone)->format('Y-m-d H:i:s');
                if ($converted_date != '') {
                    $bookingM->created_at = $converted_date;
                }
            }

            $bookingM->tour_date_time = date('l, F jS Y @ g:i a - ', strtotime($bookingM->date . ' ' . $bookingM->time_from)) .
                date('g:i a', strtotime($bookingM->date . ' ' . $bookingM->time_to));

            if (request()->is('api/*')) {
                $selectFieldsBookingT = [
                    'tbt.id', 'tbt.tour_package_rate_id', 'tbt.check_in_status', 'tbt.refund_permitfee_status',
                    'tbt.tourists', 'tbt.processing_charge_percentage', 'tbt.additional_tax_percentage',
                    'tbt.permit_fee', 'tbt.additional_charge', 'tbt.state_tax', 'tbt.rate', 'tbt.total',
                    'tpr.processing_charge_percentage as original_processing_charge_percentage',
                    'tpr.rate_for', 'to.service_commission_percentage','to.affiliate_processing_percentage',
                    'tbt.nat_fees_percentage', 'tbt.stripe_fees_percentage'
                ];
            } else {
                $selectFieldsBookingT = [
                    'tbt.id', 'tbt.tour_package_rate_id', 'tbt.check_in_status', 'tbt.refund_permitfee_status',
                    'tbt.tourists', 'tbt.processing_charge_percentage', 'tbt.additional_tax_percentage',
                    'tbt.permit_fee', 'tbt.additional_charge', 'tbt.state_tax', 'tbt.rate', 'tbt.total',
                    'tpr.processing_charge_percentage as original_processing_charge_percentage',
                    'tpr.rate_for', 'tpr.age_from', 'tpr.age_to','tpr.permit_fee as tpr_permit_fee',
                    'to.service_commission_percentage', 'tp.id As tour_packages_id', 'to.id as tour_operator_id',
                    'to.affiliate_processing_percentage', 'tbt.nat_fees_percentage', 'tbt.stripe_fees_percentage'
                ];
            }

            $bookingT = DB::table('tour_bookings_transact AS tbt')
                ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->join('tour_operators as to', 'to.id', '=', 'tp.tour_operator_id')
                ->where('tbt.tour_booking_id', $id)
                //->orderByDesc('tpr.age_from')
                //->orderByDesc('tpr.age_to')
                ->orderBy('tpr.orderby', 'asc')
                ->get($selectFieldsBookingT);

            if (!$bookingT) {
                throw new Exception("Transaction details not found.");
            }

            $rate = 0;
            $permit_fee = 0;
            $additional_charge = 0;
            $tourists = 0;
            $makePaymentValueForCardFailed = 0;
            $payment_type = 0;
            $rateGroupWiseTourists = array();
            $rateGroupWiseTotal = array();
            $rateGroupWiseOldServiceCommission = array();
            $rateGroupWisePermitFeesRefunded = array();
            foreach ($bookingT as $value) {
                $rate = $value->rate;
                $permit_fee = $value->permit_fee;
                $additional_charge = $value->additional_charge;
                $tourists = $value->tourists;
                $permitFeesRefunded = 0;
                if ($value->refund_permitfee_status != '') {
                    $refundPermitFeeInfo = json_decode($value->refund_permitfee_status);
                    if(!empty($refundPermitFeeInfo)){
                        foreach ($refundPermitFeeInfo as $key=>$info){
                            if(!isset($info->is_pax_added)) {
                                if ($info->status == 'Y') {
                                    if (isset($info->amount) && $info->amount > 0) {
                                        $permitFeesRefunded += $info->amount;
                                        if (isset($info->processing_fees) && $info->processing_fees > 0) {
                                            $permitFeesRefunded += $info->processing_fees;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $rateGroupWiseTotal[$value->tour_package_rate_id] = $value->total;
                $rateGroupWiseTourists[$value->tour_package_rate_id] = $value->tourists;
                $rateGroupWiseOldServiceCommission[$value->tour_package_rate_id] = $value->processing_charge_percentage;
                $rateGroupWisePermitFeesRefunded[$value->tour_package_rate_id] = $permitFeesRefunded;
                $makePaymentValueForCardFailed += ($rate + $permit_fee + $additional_charge) * $tourists;
            }

            // the following isn't working in .blade view file when an element is dynamically created if it doesn't exist
            // laravel is throwing error of undefined offset even after element is created by assigning a new class to it
            for ($a = 0, $ac = count($bookingT); $a < $ac; $a++) {
                if ($bookingT[$a]->check_in_status == '') {
                    $statusInfo = [];
                } else {
                    $statusInfo = json_decode($bookingT[$a]->check_in_status);
                }

                for ($b = 0; $b < $bookingT[$a]->tourists; $b++) {
                    if (!isset($statusInfo[$b])) {
                        $statusInfo[$b] = new \stdClass;
                        $statusInfo[$b]->status = 'N';
                    }
                    $statusInfo[$b]->ageGroupIndex = $b;
                }

                $bookingT[$a]->check_in_status = $statusInfo;
                $bookingT[$a]->permit_fee_history = isset($bookingT[$a]->refund_permitfee_status) ? json_decode($bookingT[$a]->refund_permitfee_status) : [];
            }

            $payments = DB::table('transactions')
                ->where('tour_booking_id', $id)
                ->where('status', 'Completed')
                ->get(['amount', 'created_at', 'via']);

            if ($bookingM->created_by_user_type == 'Tour Operator') {

                $staffUsers = DB::table('tour_operator_staff')
                    ->where('tour_operator_id', $bookingM->tour_operator_id)
                    ->where(function ($qry) use ($bookingM) {
                        $qry->where(function ($qry2) {
                            $qry2->where('status', 'Active')
                                ->where('deleted_by', 0);
                        })
                            ->orWhere('id', $bookingM->created_by);
                    })
                    ->get(['id', 'first_name', 'last_name']);
            } else {
                $staffUsers = DB::table('tour_operator_staff')
                    ->where('tour_operator_id', $bookingM->tour_operator_id)
                    ->where('status', 'Active')
                    ->where('deleted_by', 0)
                    ->get(['id', 'first_name', 'last_name']);
            }

            $BookingUser = $this->tourBooking::where('id', $id)->withTrashed()->first();

            $user = [];
            if ($BookingUser->created_by_user_type == "Admin") {
                $admin = $this->admin::where('id', $BookingUser->created_by)->first();
                $user['name'] = $admin->name;
                $user['type'] = "Admin";
            } elseif ($BookingUser->created_by_user_type == "Tour Operator") {
                $operation = $this->staff::where('id', $BookingUser->created_by)->withTrashed()->first();
                $user['name'] = $operation->first_name . ' ' . $operation->last_name;
                $user['type'] = "Tour Operator";
            } else {
                $user['name'] = 'Online';
                $user['type'] = "";
            }

            $trans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'DESC')->first();
            $transP = $this->transaction::select('id','payment_type','via','transaction_number')->where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'ASC')->first();
            $HistoryPaymentTime = $this->tourBookingHistory::select('id','operation_at','details')->where('tour_booking_id', $id)->where('operation', 'Payment')->first();
            $transF = $this->transaction::where('tour_booking_id', $id)->where('status', 'Failed')->orderBy('id', 'DESC')->first();

            $refund_amount = 0;
            $payment_total = 0;
            $booking_total = 0;
            if (!empty($HistoryPaymentTime)) {
                $TourBookingHistory = $this->tourBookingHistory::where('tour_booking_id', $id)->where('operation_at', '>', $HistoryPaymentTime->operation_at)
                    ->where(function ($query) {
                        return $query->where('operation', '=', 'Refund')
                            ->orWhere('operation', '=', 'Cancelled');
                    })->get(['id','details']);
                if (count($TourBookingHistory) > 0) {
                    foreach ($TourBookingHistory as $key => $bookingHistory) {
                        $details = json_decode($bookingHistory->details);
                        if(isset($details->refund_type) && $details->refund_type != "Don't Refund" && $details->refund_type != "don't refund"){
                            $refund_amount = $refund_amount + (float)$details->amount;
                        }
                    }
                }
            }

            $booking_total = $booking_total + $bookingM->total + $refund_amount;
            if ($trans != null) {
                $payment_total = $payment_total + $booking_total - $refund_amount;
            }

            $paymentOnly = $this->tourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->get(['id','details']);
            $paymentTrans = $this->transaction::with('bookingHistory')->where('tour_booking_id', $id)->where('status', 'Completed')->get(['id','tour_booking_id','amount','fee_amount','transaction_number','application_fee_id','via','payment_type','status']);

            $paymentTransCount = count($paymentTrans);
            $refundTrans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Refund')->get(['id','tour_booking_id','transaction_number']);
            foreach ($paymentTrans as $key => $value) {
                foreach ($refundTrans as $ky => $val) {
                    if($value->transaction_number == $val->transaction_number){
                        unset($paymentTrans[$key]);
                    }
                }
            }

            if (request()->is('api/*')) {
                $paymentTrans = $paymentTrans->values();
            }

            if (request()->is('api/*')) {
                if ($bookingM->created_by_user_type == 'Tour Operator') {
                    $finduser = $this->staff::where('id', $bookingM->created_by)->first();
                    $name = $finduser->first_name . ' ' . $finduser->last_name;
                } else {
                    $finduser = $this->admin::where('id', $bookingM->created_by)->first();
                    $name = $finduser->name ?? '';
                }
                $bookingM->username = $name;
            }
            $paymentTransactions = $this->transaction::with('bookingHistory')->where('tour_booking_id', $id)->where('status', 'Completed')->get(['id','tour_booking_id','amount','fee_amount','transaction_number','application_fee_id','via','payment_type','status']);
            foreach ($paymentTransactions as $value) {
                $value->refund_amount = 0;
            }
            if (request()->is('api/*')) {
                foreach ($paymentTrans as $value) {
                    $value->card_last_4_digit = $value->bookingHistory->card_last_4_digit ?? '';
                }
            }

            $refundTransactions = $this->transaction::where('tour_booking_id', $id)->where('status', 'Refund')->get(['id','tour_booking_id','amount','fee_amount','transaction_number','application_fee_id','via','payment_type','status']);
            if ($refundTransactions->count()) {
                foreach ($paymentTransactions as $key => $payment) {
                    foreach ($refundTransactions as $refund) {
                        if ($refund->transaction_number == $payment->transaction_number) {
                            $payment->refund_amount += $refund->amount;
                        }
                    }
                }
            }

            $paymentOnlyCheck = 0;
            foreach ($paymentOnly as $value) {
                $details = json_decode($value->details);
                $paymentOnlyCheck += (!empty($details->amount) && $details->amount!='NaN') ? $details->amount : 0;
            }

            $historyDet = DB::table('tour_booking_history')
                ->orderByDesc('operation_at')
                ->where('tour_booking_id', $id)
                ->get();

            foreach ($historyDet as $historyData) {
                $converted_date = '';
                if (($historyData->operation == "Note") || ($historyData->operation == "Comments") || ($historyData->operation == "Email") || ($historyData->operation == "Error") || ($historyData->operation == "Reference Booking")) {
                    $historyData->details = $historyData->details;
                    $historyData->username = '';
                    if ($historyData->operation == "Note" || $historyData->operation == "Email" || $historyData->operation == "Comments" || $historyData->operation == "Reference Booking") {
                        if ($historyData->operation_by_user_type == 'Tour Operator') {
                            $finduser = $this->staff::where('id', $historyData->operation_by)->first();
                            $name = $finduser->first_name . ' ' . $finduser->last_name;
                        } else {
                            $finduser = $this->admin::where('id', $historyData->operation_by)->first();
                            $name = $finduser->name ?? '';
                        }
                        $historyData->username = $name;
                    } else if ($historyData->operation == "Customer Info Update") {
                        $name = '';
                        $email = '';
                        $phone_number = '';
                        if ($historyData->operation_by_user_type == 'Tour Operator') {
                            $finduser = $this->staff::where('id', $historyData->operation_by)->first();
                        } else {
                            $finduser = $this->admin::where('id', $historyData->operation_by)->first();
                        }
                        if (isset($details) && !empty($details->name)) {
                            $name = 'Name' . ': (' . $details->name . ')';
                        }
                        if (isset($details->email) && !empty($details->email)) {
                            $email = 'Email' . ': (' . $details->email . ')';
                        }
                        if (isset($details->phone_number) && !empty($details->phone_number)) {
                            $phone_number = 'Phone number' . ': (' . $details->phone_number . ')';
                        }
                        $historyData->username = $name . ' ' . $email . ' ' . $phone_number;
                    } else if ($historyData->operation == "Error") {
                        $historyData->details = $historyData->details;
                    }
                } else {
                    $historyData->details = json_decode($historyData->details);
                }

                $operation_at = $historyData->operation_at;

                if ($operation_at != '' && $timezone != '') {
                    $converted_date = Carbon::createFromFormat('Y-m-d H:i:s', $operation_at, 'US/Arizona')
                        ->setTimezone($timezone)->format('Y-m-d H:i:s');
                    if ($converted_date != '') {
                        $historyData->operation_at = $converted_date;
                    }
                }
            }

            if ($bookingM->created_by_user_type == 'Tour Operator') {
                $staffUsers = DB::table('tour_operator_staff')
                    ->where('tour_operator_id', $bookingM->tour_operator_id)
                    ->where(function ($qry) use ($bookingM) {
                        $qry->where(function ($qry2) {
                            $qry2->where('status', 'Active')
                                ->where('deleted_by', 0);
                        })
                            ->orWhere('id', $bookingM->created_by);
                    })
                    ->get(['id', 'first_name', 'last_name']);
            } else {
                $staffUsers = DB::table('tour_operator_staff')
                    ->where('tour_operator_id', $bookingM->tour_operator_id)
                    ->where('status', 'Active')
                    ->where('deleted_by', 0)
                    ->get(['id', 'first_name', 'last_name']);
            }
            if(isset($bookingM->tour_op_logo_print) && $bookingM->tour_op_logo_print != ''){
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $bookingM->tour_op_logo_print)) {
                    $printLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $bookingM->tour_op_logo_print);
                } elseif (Storage::disk('s3')->exists('images/tour-operator/logo/' . $bookingM->tour_op_logo_print)) {
                    $printLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $bookingM->tour_op_logo_print);
                } else {
                    $printLogo = asset('images/no-photo.png');
                }
                if (isset($bookingM->tour_op_logo) && $bookingM->tour_op_logo != '') {
                    if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $bookingM->tour_op_logo)) {
                        $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $bookingM->tour_op_logo);
                    } else {
                        $tourOperatorLogo = asset('images/no-photo.png');
                    }
                }

            }else if (isset($bookingM->tour_op_logo) && $bookingM->tour_op_logo != '') {
                if (Storage::disk('s3')->exists('images/tour-operator/resized-logo/' . $bookingM->tour_op_logo)) {
                    $printLogo = Storage::disk('s3')->url('images/tour-operator/resized-logo/' . $bookingM->tour_op_logo);
                } elseif (Storage::disk('s3')->exists('images/tour-operator/logo/' . $bookingM->tour_op_logo)) {
                    $printLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $bookingM->tour_op_logo);
                } else {
                    $printLogo = asset('images/no-photo.png');
                }

                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $bookingM->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $bookingM->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
            } else {
                $printLogo = $tourOperatorLogo = asset('images/no-photo.png');
            }

            if (request()->is('api/*')) {
                $printInfo = [
                    'tour_operator' => isset($bookingM->tour_operator) ? $bookingM->tour_operator : "",
                    'tour_op_logo' => $tourOperatorLogo,
                    'tour_op_email' => isset($bookingM->tour_op_email) ? $bookingM->tour_op_email : "",
                    'tour_op_phone' => isset($bookingM->tour_op_phone) ? $bookingM->tour_op_phone : "",
                    'tour_op_website' => isset($bookingM->tour_op_website) ? $bookingM->tour_op_website : "",
                    'logo' => $printLogo,
                    'payment_via' => $bookingM->via,
                ];
            } else {
                $printInfo = [
                    'booking_id' => $bookingM->id,
                    'tour_package' => $bookingM->tour_package,
                    'tour_operator_id' => $bookingM->tour_operator_id,
                    'tour_operator' => isset($bookingM->tour_operator) ? $bookingM->tour_operator : "",
                    'tour_op_logo' => $tourOperatorLogo,
                    'tour_op_email' => isset($bookingM->tour_op_email) ? $bookingM->tour_op_email : "",
                    'tour_op_phone' => isset($bookingM->tour_op_phone) ? $bookingM->tour_op_phone : "",
                    'tour_op_website' => isset($bookingM->tour_op_website) ? $bookingM->tour_op_website : "",
                    'tour_date_time' => $bookingM->tour_date_time,
                    'tour_package_id' => $bookingM->tour_package_id,
                    'customer_id' => $bookingM->customer_id,
                    'customer_name' => $bookingM->customer,
                    'cust_email' => $bookingM->cust_email,
                    'cust_phone' => $bookingM->cust_phone,
                    'is_affiliate' => $bookingM->is_affiliate,
                    'date' => $bookingM->date,
                    'time_from' => $bookingM->time_from,
                    'time_to' => $bookingM->time_to,
                    'tourists' => $bookingM->tourists,
                    'comments' => $bookingM->comments,
                    'service_commission' => $bookingM->service_commission,
                    'subtotal' => $booking_total,
                    'payment_total' => $payment_total,
                    'payment_date' => $bookingM->created_at,
                    'payment_via' => $bookingM->via,
                    'logo' => $printLogo
                ];
            }

            $invoiceDetails = [];
            if ($bookingM->via == 'Invoiced') {
                $invoiceDetails = $this->invoiceBooking::join('invoices AS iv', 'iv.id', '=', 'invoice_bookings.invoice_id')->where('tour_booking_id', $id)
                    ->select('iv.id', 'iv.status', 'invoice_bookings.invoice_id')->orderBy('invoice_bookings.id', 'DESC')->first();
            }

            if (request()->is('api/*')) {
                if (count($historyDet) > 0) {
                    foreach ($historyDet as $key => $hist) {
                        $operationDesc = '';
                        $opByUType = $hist->operation_by_user_type;

                        if ($hist->operation == 'Note') {
                            if ($opByUType == 'Tour Operator') {
                                $opByUType = 'A tour operator staff';
                            } else {
                                $opByUType = 'An admin';
                            }
                            $descAdd = "<span style='color:green'>"."$opByUType user ($hist->username) added/updated note:<br></span>";
                            $operationDesc = $descAdd."<textarea rows='3' disabled style='border: none;background: transparent;overflow: hidden;width: 100%;color: #212529;font-size: 16px;'>" . e($hist->details)."</textarea>";
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Error') {
                            $operationDesc = 'Booking error - ' . e($hist->details);
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Reference Booking') {
                            $operationDesc = 'Reference booking number added/updated. ' . $opByUType.'('.$hist->username.'): <b>' . e($hist->details) . '</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Comments') {
                            $operationDesc = 'Comment from the customer added/updated. '.$opByUType.'('.$hist->username.'):<br>' . e($hist->details);
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Restore') {
                            $operationDesc = 'Booking restored by <b>' . e($hist->details->created_by_with_name) . '</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Payment Type Update') {
                            $operationDesc = 'History ID - <b>' . $hist->details->history_id . '</b>, Payment type changed from <b>' . $hist->details->from . '</b> to <b>' . $hist->details->to . '</b> by <b>' . e($hist->details->created_by_with_name) . '</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Payment' || $hist->operation == 'Refund') {
                            $details = $hist->details;
                            $details->amount = sprintf('%0.2f', $details->amount);
                            $details->payment_type = sprintf('%s', $details->payment_type ?? '');
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                            if (isset($details->deleted) && !empty($details->deleted)) {
                                if (is_array($details->deleted)) {
                                    $deletedImplode = implode('<br>', $details->deleted);
                                    $details->deleted = sprintf('%s', $deletedImplode);
                                } else {
                                    $details->deleted = sprintf('%s', $details->deleted ?? '');
                                }
                            } else {
                                $details->deleted = '';
                            }

                            $details->refund_type = sprintf('%s', $details->refund_type ?? '');
                            if ($hist->operation == 'Payment') {
                                $paymentMethod = '';
                                if (!empty($details) && !empty($details->payment_method)) {
                                    $paymentMethod = $details->payment_method;
                                }
                                $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost;
                                if ((isset($hist->change_amount) && $hist->change_amount > 0) && isset($details->amount)) {
                                    $paidAmountCash = (float)$hist->change_amount + (float)$details->amount;
                                    $detailCostDesc .= '<br /> Paid: $'.$paidAmountCash;
                                    $detailCostDesc .= '<br /> Change: $'.$hist->change_amount;
                                }
                                if ((isset($hist->discount) && $hist->discount > 0) && (isset($paymentMethod) && $paymentMethod != 'Split Payment')) {
                                    $detailCostDesc .= '<br/> Discount: $'.$hist->discount;
                                }
                                $operationDesc = 'Received payment of $' . $details->amount .' '.$details->payment_type .' '.$paymentMethod .' '. $details->created_by_with_name.'<br/>'.$detailCostDesc;
                                $historyDet[$key]->history_api_data = $operationDesc;
                                if ($details->payment_type == "Card") {
                                    $detailCostDesc = '';
                                    $cardNumber = (isset($details->card_number)) ? $details->card_number : '';
                                    $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost.'<br/> Processing Fees: $'.$hist->processing_fees.'<br/> Platform Fees: $'.$hist->platform_fees.'';
                                    if ((isset($hist->discount) && $hist->discount > 0) && (isset($paymentMethod) && $paymentMethod != 'Split Payment')) {
                                        $detailCostDesc .= '<br/> Discount: $'.$hist->discount;
                                    }
                                    $operationDesc = 'Received payment of $' . $details->amount .' '.$details->payment_type .' '.$paymentMethod .' ('. $cardNumber .') ' . $details->created_by_with_name.'<br/>'.$detailCostDesc;
                                    $historyDet[$key]->history_api_data = $operationDesc;
                                }
                            } else {
                                $detailCostDesc = '';
                                $descDeleted = ($details->refund_type != 'Permitfees Refund') ? '<b>Deleted</b>: <br/>'.$details->deleted.'<br />' : '';
                                if ($details->payment_type == 'Card' || $hist->payment_type == 'Card') {
                                    $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost.'<br/> Processing Fees: $'.$hist->processing_fees.'<br/> Platform Fees: $'.$hist->platform_fees.'';
                                } else {
                                    $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost;
                                }
                                $note = (isset($details->note)) ? $details->note : '';
                                if ($details->refund_type == 'Full Refund minus Fees') {
                                    $details->refund_type = 'Ticket Cost';
                                }
                                if ($note != '') {
                                    $operationDesc = 'Refunded amount of $' . $details->amount .' '.$details->payment_type.' '.$details->deleted.' '. $details->refund_type.' '.'(Note: '. $note.') '.$details->created_by_with_name.'<br/>'.$descDeleted.$detailCostDesc;
                                } else {
                                    $operationDesc = 'Refunded amount of $' . $details->amount .' '.$details->payment_type.' '.$details->deleted.' '. $details->refund_type.' '.$details->created_by_with_name.'<br/>'.$descDeleted.$detailCostDesc;
                                }
                                $historyDet[$key]->history_api_data = $operationDesc;
                            }
                            if ($hist->tourists) {
                                $historyDet[$key]->history_api_data .= '<br /> <b>New Pax Count</b>: ' . $hist->tourists;
                            }
                        } elseif ($hist->operation == 'Update') {
                            $details = $hist->details;
                            if (isset($details->sub_operation) && $details->sub_operation && ($details->sub_operation === 'Tourists Added' || $details->sub_operation === 'Tourists Changed')) {
                                $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');

                                $descAdd = '';
                                if (isset($details->added) && !empty($details->added)) {
                                    if (is_array($details->added)) {
                                        $addedImplode = implode('<br>', $details->added);
                                        $descAdd = sprintf('%s', $addedImplode);
                                    } else {
                                        $descAdd = sprintf('%s', $details->added ?? '');
                                    }
                                }

                                $descDelete = '';
                                if (isset($details->deleted) && !empty($details->deleted)) {
                                    if (is_array($details->deleted)) {
                                        $deletedImplode = implode('<br>', $details->deleted);
                                        $descDelete = sprintf('%s', $deletedImplode);
                                    } else {
                                        $descDelete = sprintf('%s', $details->deleted ?? '');
                                    }
                                }

                                if ($details->sub_operation === 'Tourists Added') {
                                    $operationDesc = 'Edit booking - ' . $details->newTourists . ' tourist(s) added. ' . $details->created_by_with_name."<br>".$descAdd;
                                } else {
                                    $operationDesc = 'Edit booking - ' . $details->addedTourists . ' tourist(s) added. '.$details->deletedTourits . ' tourist(s) deleted. '.$details->created_by_with_name.'<br>';
                                    if ($descAdd != "") {
                                        $operationDesc .= '<b>Added</b>: <br/>'.$descAdd.'<br/>';
                                    }
                                    if ($descDelete != "") {
                                        $operationDesc .= '<b>Deleted</b>: <br/>'.$descDelete.'<br/>';
                                    }
                                }
                                $hist->operation = 'Update Pax';
                                if ($hist->tourists) {
                                    $operationDesc .= '<b>New Pax Count</b>: ' . $hist->tourists;
                                }
                                $historyDet[$key]->history_api_data = $operationDesc;
                            } else {
                                $details->amount = sprintf('%0.2f', $details->amount);
                                $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                                $hist->operation = 'Permit';
                                if (isset($details->permit_fees)) {
                                    $operationDesc = 'Edit permit fee updated amount $' . $details->amount . '(Permit Fees Refunded : $' . $details->permit_fees . ' ) ' . $details->created_by_with_name;
                                    $historyDet[$key]->history_api_data = $operationDesc;
                                } else {
                                    $operationDesc = 'Edit permit fee updated amount $' . $details->amount . ' ' . $details->created_by_with_name;
                                    $historyDet[$key]->history_api_data = $operationDesc;
                                }
                            }
                        } elseif ($hist->operation == 'Added') {
                            $details = $hist->details;
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');

                            $descAdd = '';
                            if (isset($details->added) && !empty($details->added)) {
                                if (is_array($details->added)) {
                                    $addedImplode = implode('<br>', $details->added);
                                    $descAdd = sprintf('%s', $addedImplode);
                                } else {
                                    $descAdd = sprintf('%s', $details->added ?? '');
                                }
                            }

                            $operationDesc = 'Added booking - ' . $details->newTourists . ' tourist(s) added. ' . $details->created_by_with_name . "<br>" . $descAdd;
                            $hist->operation = 'Original';
                            if ($hist->tourists) {
                                $operationDesc .= '<br /> <b>New Pax Count</b>: ' . $hist->tourists;
                            }
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Cancelled') {
                            $details = $hist->details;

                            $descDelete = '';
                            if (isset($details->deleted) && !empty($details->deleted)) {
                                if (is_array($details->deleted)) {
                                    $deletedImplode = implode('<br>', $details->deleted);
                                    $descDelete = sprintf('%s', $deletedImplode);
                                } else {
                                    $descDelete = sprintf('%s', $details->deleted ?? '');
                                }
                            }

                            $details->amount = sprintf('%0.2f', $details->amount);
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                            $details->payment_type = sprintf('%s', $details->payment_type ?? '');
                            $details->refund_type = sprintf('%s', $details->refund_type ?? '');
                            $details->note = sprintf('%s', $details->note ?? '');
                            $detailCostDesc = '';
                            if ($details->payment_type=='Card' || $hist->payment_type=='Card'){
                                $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost.'<br/> Processing Fees: $'.$hist->processing_fees.'<br/> Platform Fees: $'.$hist->platform_fees.'';
                            } else {
                                $detailCostDesc = 'Ticket Cost: $'.$hist->ticket_cost;
                            }
                            if ($details->refund_type == 'Full Refund') {
                                $details->refund_type = 'Ticket Cost';
                            }
                            $operationDesc = 'Booking cancelled' .' ' .$descDelete .' '.' $' . $details->amount .' '.$details->payment_type .' '. $details->refund_type .' '.'(Note: '. $details->note.') '. $details->created_by_with_name.'<br/>'.$detailCostDesc;
                            if ($hist->tourists) {
                                $operationDesc .= '<br /> <b>New Pax Count</b>: ' . $hist->tourists;
                            }
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Email') {
                            $operationDesc =   e($hist->details) . ' by '  . $hist->username;
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Check-in') {
                            $details = $hist->details;
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                            $operationDesc = 'Set check-in status to <b>' . $details->status . '</b> for <b>' . e($details->who) . '</b>'.' '. $details->created_by_with_name;
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Booked By') {
                            $details = $hist->details;
                            $operationDesc = 'Changed booked by person from <b>' . e($details->from) . '</b> to <b>' . e($details->to) . '</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Rescheduled') {
                            $details = $hist->details;
                            $dateTimeFrom = formatDateTime($details->dateFrom . ' ' . $details->timeFrom);
                            $dateTimeTo = formatDateTime($details->dateTo . ' ' . $details->timeTo);
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                            $operationDesc = "Rescheduled from <b>$dateTimeFrom</b> to <b>$dateTimeTo</b>".' '. $details->created_by_with_name;
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Waiver') {
                            $details = $hist->details;
                            $data = ($details && isset($details->whoSignIn) && $details->whoSignIn) ? $details->whoSignIn : '';
                            $string = ($data) ? implode(" , ",$data) : '';
                            $operationDesc = 'Waivers sign By <b>'.$string.'</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Customer Info Update') {
                            $details = $hist->details;
                            $name = '';
                            $email = '';
                            $phone_number = '';
                            if ($opByUType == 'Tour Operator') {
                                $opByUType = 'A tour operator staff';
                                $finduser = \App\Models\Staff::where('id', $hist->operation_by)->first();
                                $staffname = 'updated by a tour operator staff'.': '. $finduser->first_name .' '. $finduser->last_name;
                            } else {
                                $opByUType = 'An admin';
                                $finduser = \App\Models\Admin::where('id', $hist->operation_by)->first();
                                if ($finduser == null) {
                                    $finduser = \App\Models\Staff::where('id', $hist->operation_by)->first();
                                    $staffname = 'updated by a tour operator staff'.': '. $finduser->first_name .' '. $finduser->last_name;
                                } else {
                                    $staffname = 'updated by an admin'.': '. $finduser->name;
                                }
                            }
                            if (isset($details->name) && !empty($details->name)) {
                                $name = 'Name' . ': (' . $details->name . ')';
                            }
                            if (isset($details->email) && !empty($details->email)) {
                                $email = 'Email' . ': (' . $details->email. ')';
                            }
                            if (isset($details->phone_number) && !empty($details->phone_number)) {
                                $phone_number = 'Phone number' . ': (' . $details->phone_number . ')';
                            }
                            $operationDesc = 'Customer Information update <b>'. '' .$name. ' ' . $email . ' ' . $phone_number . ' ' . $staffname.'</b>';
                            $historyDet[$key]->history_api_data = $operationDesc;
                        } elseif ($hist->operation == 'Tip') {
                            $details = $hist->details;
                            $details->amount = sprintf('%0.2f', $details->amount);
                            $details->payment_type = sprintf('%s', $details->payment_type ?? '');
                            $details->created_by_with_name = sprintf('%s', $details->created_by_with_name ?? '');
                            $operationDesc = 'Received Tip of $' . $details->amount .' '.$details->payment_type .' '. $details->created_by_with_name;
                            $historyDet[$key]->history_api_data = $operationDesc;
                            if ($details->payment_type == "Card" && isset($details->card_number)) {
                                $operationDesc = 'Received Tip of $' . $details->amount .' '.$details->payment_type .' ('. $details->card_number .') ' . $details->created_by_with_name;
                                $historyDet[$key]->history_api_data = $operationDesc;
                            }
                        }

                        // unset($hist->id);
                        unset($hist->tour_booking_id);
                        // unset($hist->operation);
                        unset($hist->details);
                        unset($hist->operation_by);
                        // unset($hist->opedddration_by_user_type);
                        unset($hist->updated_at);
                        unset($hist->created_at);
                    }
                }

                if($bookingM->liability_waiver_signing != null) {
                    $waiverData = json_decode($bookingM->liability_waiver_signing, true);
                    $modifiedWaiverData = array();

                    foreach ($waiverData as $key => $waiver) {
                        if ($waiver[1] == "-1" || ($waiver[0]=='' && $waiver[1]=='')) {
                            $modifiedWaiverData[$key]['liability_waiver_signing'] = '';
                            $modifiedWaiverData[$key]['name'] = '';
                            $modifiedWaiverData[$key]['age'] = '';
                        } else {
                            $modifiedWaiverData[$key]['liability_waiver_signing'] = isset($waiver[2])?$waiver[2]:'';
                            $modifiedWaiverData[$key]['name'] = (isset($waiver[0]))?$waiver[0]:'';
                            $modifiedWaiverData[$key]['age'] = (isset($waiver[1]) && $waiver[1]>0)?(string)$waiver[1]:'';
                        }
                    }

                    $bookingM->liability_waiver_signing = $modifiedWaiverData;
                } else {
                    $bookingM->liability_waiver_signing = array();
                }

                for($a = 0, $ac = count($payments), $paid = 0; $a < $ac; $a++)
                    $paid = $payments[$a]->amount;

                $amount = 0;
                $paidF = 0;
                if (!empty($trans)) {
                    $paidF = $trans->amount;
                }

                if (!empty($TourBookingHistory)) {
                    foreach($TourBookingHistory as $BookingHistory){
                        $details = json_decode($BookingHistory->details);
                        if ($details->refund_type != "Don't Refund" && $details->refund_type != "don't refund") {
                            $amount += $details->amount;
                        }
                    }
                    $amount = number_format((float)$amount, 2, '.', '');
                    $amount = (float)($amount);
                    $paidF = $paymentOnlyCheck - $amount;
                }

                $paidAmount = $paidF;
                if( ($paid) == 0) {
                    $unPaidAmount = $bookingM->total - $paid;
                } elseif (!empty($trans) && $trans->payment_type == 1 && $bookingM->total > $paymentOnlyCheck) {
                    $unPaidAmount = number_format((float)$bookingM->total - $paymentOnlyCheck, 2, '.', '');
                } else {
                    $unPaidAmount = $amount;
                }

                if ($trans != null && $trans->status == "Completed") {
                    if ($trans->via=="Invoiced" && (isset($invoiceDetails) && !empty($invoiceDetails) && $invoiceDetails->status=='paid')) {
                        $paymentStatus = "Invoiced Paid";
                    } elseif($trans->via=="Invoiced") {
                        $paymentStatus = "Invoiced";
                    } else {
                        if ($bookingM->statusm == 'Cancelled' && $refund_amount > 0) {
                            $paymentStatus = "Refunded";
                        } elseif (!empty($trans) && $trans->payment_type == 1 && $bookingM->total > $paymentOnlyCheck) {
                            $paymentStatus = "Split";
                        } else {
                            $paymentStatus = "Paid in Full";
                        }
                    }
                } else {
                    $paymentStatus = 'UnPaid';
                }

                $totalBeforeServiceCommission = $bookingM->total - $bookingM->service_commission;

                $forCardProcChargePercentage = 0;
                $subTotal = $totalBeforeServiceCommission + $bookingM->discount2_value;
                if ($bookingM->discount2_percentage != 0) {
                    $discountPercentage = $bookingM->discount2_percentage;
                }
                $discountAmount = $bookingM->discount2_value;
                $feesAmount = $bookingM->service_commission;
                $trans = $this->transaction::where('tour_booking_id', $bookingM->id)->where('status', 'Completed')->orderBy('id', 'DESC')->first(['payment_type','via']);
                if (!empty($trans)) {
                    $payment_type = $trans->payment_type;
                } else {
                    $trans = [];
                }

                if (!empty($trans) && $trans->payment_type == 1) {
                    $totalAmount = $bookingM->total - $paymentOnlyCheck;
                    $totalV =  number_format((float)$bookingM->total - $paymentOnlyCheck, 2, '.', '');
                } else {
                    $totalAmount = $bookingM->total;
                }
                $bookingTotal = ($bookingM->booking_total != "0.00") ? $bookingM->booking_total : $bookingM->total;
                $totalRefundAmount = $amount;
                $paymentTotal = $paidF;

                if ($bookingM->status != 'Completed') {
                    $forCardProcChargePercentage = 0;
                    $totalSubtotal = 0;

                    foreach ($bookingT as $value) {
                        $rate = 0;
                        $per = 0;
                        $touristSubtotal=0;
                        $cardProccharge = 0;
                        $rate = $value->rate + $value->additional_charge + $value->permit_fee;
                        $per = $rate * ($value->additional_tax_percentage + $value->processing_charge_percentage) / 100;
                        if ($value->rate > 0) {
                            $touristSubtotal = ($rate + $per)*$value->tourists;
                            $totalSubtotal += (($rate + $per)*$value->tourists);
                            $cardProccharge = number_format((float) ($touristSubtotal) * $value->original_processing_charge_percentage / 100, 2, '.', '');
                            $forCardProcChargePercentage += $cardProccharge;
                        }
                    }

                    if ($bookingM->is_affiliate == 'Yes') {
                        if ($bookingM->discount2_value > 0) {
                            if (isset($bookingT[0]->processing_charge_percentage) && $bookingT[0]->processing_charge_percentage != "0.00") {
                                $subtotal = $bookingM->total;
                            } else {
                                $oldDiscountAmt = number_format((($bookingM->total) * $bookingT[0]->affiliate_processing_percentage)/100, 2, '.', '');
                                $subtotal = $bookingM->total + $oldDiscountAmt;
                            }
                            $totalV = number_format((float)$subtotal, 2, '.', '');
                        } else {
                            if (isset($bookingT[0]->processing_charge_percentage) && $bookingT[0]->processing_charge_percentage != "0.00") {
                                if ($bookingM->id < config('constants.old_bookings.booking_id')) {
                                    $processing_charge = $bookingM->total * 2.9 /100;
                                    $subtotal = $bookingM->total + $processing_charge;
                                    $totalV = number_format((float)$subtotal, 2, '.', '');
                                } else {
                                    $totalV = number_format((float)$bookingM->total, 2, '.', '');
                                }
                            } else {
                                if (isset($bookingT[0]->affiliate_processing_percentage) && $bookingT[0]->affiliate_processing_percentage != "0.00"){
                                    $bookingFees = 0;
                                    if ($bookingM->is_affiliate == "Yes" && $bookingM->id < config('constants.old_bookings.booking_id')){
                                        $processing_charge = $bookingM->total * 2.9 /100;
                                        $subtotal = $bookingM->total + $processing_charge;
                                    } else {
                                        foreach($bookingT as $key => $booking_t) {
                                            $rate = $booking_t->rate + $booking_t->additional_charge + $booking_t->permit_fee;
                                            $per = roundout(($rate * ($booking_t->affiliate_processing_percentage) / 100), 2);
                                            $bookingFees = $bookingFees + $booking_t->tourists*$per;
                                        }
                                        $subtotal = $bookingM->total + $bookingFees;
                                    }
                                }else {
                                    $subtotal = $bookingM->total + $forCardProcChargePercentage;
                                }
                                $totalV = number_format((float)$subtotal, 2, '.', '');
                            }
                        }
                    } else {
                        $forCardProcChargePercentageForOld = 0;
                        $serviceCommissionTotal = 0;
                        if ($bookingM->id < config('constants.old_bookings.booking_id')) {
                            foreach ($bookingT as $bt) {
                                $rate = $bt->rate + $bt->additional_charge + $bt->permit_fee;
                                $per = $rate * ($bt->additional_tax_percentage) / 100;
                                $subtotalOld = $rate + $per;
                                $touristSubtotalOld = $subtotalOld * $bt->tourists;
                                $cardProcchargeOld = number_format((float) ($touristSubtotalOld) * 2.9 / 100, 2, '.', '');
                                $forCardProcChargePercentageForOld += $cardProcchargeOld;
                            }
                        } else {
                            foreach ($bookingT as $bt) {
                                $rate = $bt->rate + $bt->additional_charge + $bt->permit_fee;
                                $per = roundout(($rate * ($bt->nat_fees_percentage) / 100), 2);
                                $serviceCommissionTotal += $bt->tourists * $per;
                            }
                        }
                        if($bookingM->discount2_percentage > 0) {
                            if ($bookingM->id < config('constants.old_bookings.booking_id')) {
                                $oldDiscountAmt = number_format((($totalSubtotal + $forCardProcChargePercentageForOld) * $bookingM->discount2_percentage) / 100, 2, '.', '');
                                if ($bookingM->is_affiliate == "No") {
                                    $percentage = roundout(((($totalSubtotal + $forCardProcChargePercentageForOld - $oldDiscountAmt) * 6) / 100), 2);
                                } else {
                                    $percentage = 0;
                                }
                                if ($bookingM->service_commission == null || $bookingM->service_commission == "0.00") {
                                    $totalV = ($totalSubtotal + $forCardProcChargePercentageForOld - $oldDiscountAmt) + $percentage;
                                } else {
                                    $totalV = $bookingM->total;
                                }
                            } else {
                                $oldDiscountAmt = number_format((($totalSubtotal + $forCardProcChargePercentage) * $bookingM->discount2_percentage) / 100, 2, '.', '');
                                $percentage = roundout(((($totalSubtotal + $forCardProcChargePercentage - $oldDiscountAmt) * $bookingT[0]->service_commission_percentage) / 100), 2);
                                if ($bookingM->service_commission == null || $bookingM->service_commission == "0.00") {
                                    $totalV = ($totalSubtotal + $forCardProcChargePercentage - $oldDiscountAmt) + $percentage;
                                } else {
                                    $totalV = $bookingM->total;
                                }
                            }
                        } else {
                            if ($bookingM->id < config('constants.old_bookings.booking_id')) {
                                if ($bookingM->is_affiliate == "No") {
                                    $percentage = roundout(((($bookingM->total + $forCardProcChargePercentageForOld) * 6) / 100), 2);
                                } else {
                                    $percentage = 0;
                                }
                                if ($bookingM->service_commission == null || $bookingM->service_commission == "0.00") {
                                    $totalV = $bookingM->total + $forCardProcChargePercentageForOld + $percentage;
                                } else {
                                    $totalV = $bookingM->total;
                                }
                            } else {
                                if($bookingM->discount2_value>0 && isset($bookingT[0]->nat_fees_percentage)){
                                    $serviceCommissionTotal = roundout(($bookingM->total * ($bookingT[0]->nat_fees_percentage) / 100), 2);
                                    $totalV = $bookingM->total + $serviceCommissionTotal;
                                }else {
                                    if ($serviceCommissionTotal > 0 && $bookingM->via == null) {
                                        $totalV = $bookingM->total + $serviceCommissionTotal;
                                    } else {
                                        $totalV = $bookingM->total;
                                    }
                                }
                            }
                        }
                        $totalV = (string)$totalV;
                    }
                }
            }

            $tourPackageRate = $this->tourPackageRate::where('affiliate_id', $bookingM->customer_id)
                ->where('scheduling_year', date('Y', strtotime($bookingM->date)))
                ->first();

            $isAffiliate = $bookingM->is_affiliate;
            if ($tourPackageRate ==  null) {
                $isAffiliate = 'No';
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where([
                    'tpr.tour_package_id' => $bookingM->tour_package_id,
                    //'tpr.status' => 'Active',
                    'tpr.deleted_by' => 0
                ])
                ->when($isAffiliate == 'Yes', function ($q) use ($bookingM) {
                    return $q->where('tpr.affiliate_id', $bookingM->customer_id);
                })
                ->when($isAffiliate == 'No', function ($q) {
                    return $q->whereNull('tpr.affiliate_id');
                })
                ->where('tpr.scheduling_year', date('Y', strtotime($bookingM->date)))
                // ->orderByDesc('tpr.age_from')
                ->orderBy('tpr.orderby', 'asc')
                ->select([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.description',
                    'tpr.additional_tax_percentage', 'tpr.processing_charge_percentage', 'tpr.additional_charge',
                    'tpr.permit_fee',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage',
                    't_o.stripe_commission_percentage',
                ]);

            $rateGroups = $rateGroups->where('t_o.id', $bookingM->tour_operator_id)->get();
            if ($rateGroups) {
                foreach ($rateGroups as $key=>$rateGroup) {
                    if(request()->is('api/*') && $bookingM->id < config('constants.old_bookings.booking_id') && $rateGroup->rate>0){
                        $rateGroups[$key]->processing_charge_percentage = (string)2.9;
                    }
                    if (!empty($rateGroupWiseTourists) && isset($rateGroupWiseTourists[$rateGroup->id])) {
                        $rateGroups[$key]->tourists = $rateGroupWiseTourists[$rateGroup->id];
                    } else {
                        $rateGroups[$key]->tourists = 0;
                    }
                    if (!empty($rateGroupWiseTotal) && isset($rateGroupWiseTotal[$rateGroup->id])) {
                        $rateGroups[$key]->total = (float)$rateGroupWiseTotal[$rateGroup->id];
                    } else {
                        $rateGroups[$key]->total = 0;
                    }
                    if (!empty($rateGroupWiseOldServiceCommission) && isset($rateGroupWiseOldServiceCommission[$rateGroup->id]) && $rateGroupWiseOldServiceCommission[$rateGroup->id]>0 && $rateGroup->rate>0) {
                        $rateGroups[$key]->old_processing_charge_percentage = (float)$rateGroupWiseOldServiceCommission[$rateGroup->id];
                    } else {
                        $rateGroups[$key]->old_processing_charge_percentage = 0;
                    }

                    if (!empty($rateGroupWisePermitFeesRefunded) && isset($rateGroupWisePermitFeesRefunded[$rateGroup->id])) {
                        $rateGroups[$key]->permitfees_refunded = (float)$rateGroupWisePermitFeesRefunded[$rateGroup->id];
                    } else {
                        $rateGroups[$key]->permitfees_refunded = 0;
                    }
                }
            }

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $bookingM->id)->first();
            $nat_fees_percentage = $tourBookingTransaction->nat_fees_percentage;
            if(request()->is('api/*') && $bookingM->id < config('constants.old_bookings.booking_id')){
                if($bookingM->is_affiliate=='No'){
                    $nat_fees_percentage = "6.0";
                }else{
                    $nat_fees_percentage = "0.0";
                }
            }

            if (request()->is('api/*')) {
                $vars = [
                    'page_title' => 'Tour Booking Info',
                    'bookingM' => $bookingM,
                    'bookingT' => $bookingT,
                    'history' => $historyDet,
                    'payments' => $payments,
                    'staffUsers' => $staffUsers,
                    'trans' => $trans,
                    'transF' => $transF,
                    'timezone' => $timezone,
                    'invoiceDetails' => ($bookingM->via == 'Invoiced') ? $invoiceDetails : '',
                    'paymentacknowledgement' => $paymentacknowledgement,
                    'makePaymentValueForCardFailed' => $makePaymentValueForCardFailed,
                    'paymentTotalForCard' => $totalV ?? 0,
                    'Refund_Amount' => $refund_amount,
                    'Booking_Total' => $booking_total,
                    'Payment_Total' => $payment_total,
                    'paidAmount' => (isset($paidAmount)) ? number_format((float)$paidAmount, 2, '.', '') : '',
                    'unPaidAmount' => (isset($unPaidAmount)) ? number_format((float)$unPaidAmount, 2, '.', '') : '',
                    'paymentStatus' => $paymentStatus ?? '',
                    'subTotal' => (isset($subTotal)) ? number_format((float)$subTotal, 2, '.', '') : '',
                    'discountPercentage' => $discountPercentage ?? '',
                    'discountAmount' => $discountAmount ?? '',
                    'feesAmount' => (isset($feesAmount)) ? number_format((float)$feesAmount, 2, '.', '') : '',
                    'totalAmount' => (isset($totalAmount)) ? number_format((float)$totalAmount, 2, '.', '') : '',
                    'bookingTotal' => (isset($bookingTotal)) ? number_format((float)$bookingTotal, 2, '.', '') : '',
                    'totalRefundAmount' => (isset($totalRefundAmount)) ? number_format((float)$totalRefundAmount, 2, '.', '') : '',
                    'paymentTotal' => (isset($paymentTotal)) ? number_format((float)$paymentTotal, 2, '.', '') : '',
                    'printInfo' => $printInfo,
                    'paymentOnlyCheck' => $paymentOnlyCheck,
                    'transP' => $transP,
                    'paymentTrans' => $paymentTrans,
                    'paymentCheck' => $payment_type,
                    'rateGroups'=>$rateGroups,
                    'paymentTransCount'=>$paymentTransCount,
                    'paymentTransactions'=>$paymentTransactions,
                    'vacantSeats' => $bookingM->slot_seats-$touristsCount,
                    'nat_fees_percentage' => (isset($nat_fees_percentage))?$nat_fees_percentage:$tourBookingTransaction->nat_fees_percentage,
                    'stripe_fees_percentage' => $tourBookingTransaction->stripe_fees_percentage
                ];
            } else {
                $vars = [
                    'page_title' => 'Tour Booking Info',
                    'bookingM' => $bookingM,
                    'bookingT' => $bookingT,
                    'history' => $historyDet,
                    'payments' => $payments,
                    'staffUsers' => $staffUsers,
                    'user' => $user,
                    'trans' => $trans,
                    'TourBookingHistory' => $TourBookingHistory ?? '',
                    'HistoryPaymentTime' => $HistoryPaymentTime ?? '',
                    'transF' => $transF,
                    'timezone' => $timezone,
                    'invoiceDetails' => ($bookingM->via == 'Invoiced') ? $invoiceDetails : '',
                    'paymentacknowledgement' => $paymentacknowledgement,
                    'makePaymentValueForCardFailed' => $makePaymentValueForCardFailed,
                    'paymentTotalForCard' => $totalV ?? 0,
                    'Refund_Amount' => $refund_amount,
                    'Booking_Total' => $booking_total,
                    'Payment_Total' => $payment_total,
                    'paidAmount' => (isset($paidAmount)) ? number_format((float)$paidAmount, 2, '.', '') : '',
                    'unPaidAmount' => (isset($unPaidAmount)) ? number_format((float)$unPaidAmount, 2, '.', '') : '',
                    'paymentStatus' => $paymentStatus ?? '',
                    'subTotal' => (isset($subTotal)) ? number_format((float)$subTotal, 2, '.', '') : '',
                    'discountPercentage' => $discountPercentage ?? '',
                    'discountAmount' => $discountAmount ?? '',
                    'feesAmount' => (isset($feesAmount)) ? number_format((float)$feesAmount, 2, '.', '') : '',
                    'totalAmount' => (isset($totalAmount)) ? number_format((float)$totalAmount, 2, '.', '') : '',
                    'bookingTotal' => (isset($bookingTotal)) ? number_format((float)$bookingTotal, 2, '.', '') : '',
                    'totalRefundAmount' => (isset($totalRefundAmount)) ? number_format((float)$totalRefundAmount, 2, '.', '') : '',
                    'paymentTotal' => (isset($paymentTotal)) ? number_format((float)$paymentTotal, 2, '.', '') : '',
                    'printInfo' => $printInfo,
                    'paymentOnlyCheck' => $paymentOnlyCheck,
                    'transP' => $transP,
                    'paymentTrans' => $paymentTrans,
                    'refundTrans' => $refundTrans,
                    'paymentCheck' => $payment_type,
                    'rateGroups'=>$rateGroups,
                    'paymentTransCount'=>$paymentTransCount,
                    'paymentTransactions'=>$paymentTransactions,
                    'nat_fees_percentage' => $tourBookingTransaction->nat_fees_percentage,
                    'stripe_fees_percentage' => $tourBookingTransaction->stripe_fees_percentage
                ];
            }

            return $vars;
        } catch (CardException $exception) {
            Log::error($exception);
            DB::rollBack();

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $id,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            $paymentacknowledgement = 'failed';
            throw new GeneralCoreException($exception);
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $id,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Resend the booking information via email
     *
     * @param object $request
     * @param int $id
     * @return array
     */
    public function resendEmail($request, int $id)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_bookings_master as bm', 'bm.tour_slot_id', '=', 'tst.id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('bm.id', $id)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy', 't_o.service_commission_percentage', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
						FROM tour_bookings_master AS tbm
						WHERE tbm.tour_slot_id = tst.id
							AND tbm.status != 'Cancelled'
						GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            $info = $info->first();

            $booking = [
                'tour_d_t' => $info->date . ' ' . $info->time_from,
            ];

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $id)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }

            $TourBookingHistory = new TourBookingHistory;
            $TourBookingHistory->tour_booking_id = $id;
            $TourBookingHistory->operation = 'Email';
            $TourBookingHistory->details = 'Resend Booking';
            $TourBookingHistory->operation_at = Carbon::now();
            $TourBookingHistory->operation_by = auth()->id();
            $TourBookingHistory->operation_by_user_type = $userType;
            $TourBookingHistory->save();

            $tourBooking = $this->tourBooking::where('id', $id)->first();

            if ($request->email) {
                $tourBooking->email = $request->email;
            }

            $arEmailData = [
                'bookingId' => $id,
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($id)]),
                'custName' => $tourBooking->name,
                'custEmail' => $tourBooking->email,
                'custPhone' => $tourBooking->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($id)]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'cardNumber' => '', // when needed then we will add it here
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()
                ->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            if ($request->mail_type == 2) {
                // TODO
                $tourBooking->notify(new TourBookingCancelled($arEmailData, 'tour_booking_cancelled'));
            } else {
                // TODO
                $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
            }

            return [];
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Reschedule the booking which is already created
     *
     * @param object $request
     * @param int $id
     * @return array
     */
    public function reschedule(object $request, int $id)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $booking = DB::table('tour_bookings_master AS tbm')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('customers as cus', 'cus.id', '=', 'tbm.customer_id')
                ->where('tbm.id', $id);

            if ($userType == 'Tour Operator') {
                $booking = $booking->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                    ->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $booking = $booking->first(['tbm.tourists', 'tsd.date', 'tst.time_from', 'tbm.name', 'tbm.email', 'tbm.phone_number']);
            if (!$booking) {
                throw new Exception("Tour booking not found.");
            }

            if (!isset($request->api)) {
                if ($userType == 'Tour Operator') {
                    $routePrefix = '';
                } else
                    $routePrefix = 'admin.';

                $routeParams = ['booking' => $id];
                if ($request->has('modal')) {
                    $routeParams['modal'] = 1;
                }
            }

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->where('tst.id', $request->tour_slot_time)
                ->first([
                    'tst.time_from', 'tst.time_to', 'tsd.date', 'tsd.seats', 'tp.name AS package_name', 'tp.id as tour_package_id', 'tp.things_to_bring', 'tp.important_notes',
                    't_o.cancellation_policy', 't_o.id as tour_operator_id', 't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT COUNT(tbm.tourists)
					FROM tour_bookings_master AS tbm
					WHERE tbm.tour_slot_id = tst.id
						AND tbm.status != 'Cancelled'
					GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if (!$info) {
                throw new Exception("Tour or date-time slot not found.");
            }

            DB::table('tour_bookings_master')
                ->where('id', $id)
                ->update([
                    'tour_slot_id' => $request->tour_slot_time,
                    'updated_at' => mysqlDT(),
                    'updated_by' => auth()->id(),
                    'updated_by_user_type' => $userType,
                ]);

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $id,
                    'operation' => 'Rescheduled',
                    'details' => json_encode([
                        'dateFrom' => $booking->date,
                        'timeFrom' => $booking->time_from,
                        'dateTo' => $info->date,
                        'timeTo' => $info->time_from,
                        'created_by_with_name' => $created_by_with_name
                    ]),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $id)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $arEmailData = [
                'bookingId' => $id,
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking->date . " " . $booking->time_from)),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($id)]),
                'custName' => $booking->name,
                'custEmail' => $booking->email,
                'custPhone' => $booking->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($id)]),
                'importantNotes' => $info->important_notes,
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $booking->tourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
                'rescheduledDateTime' => date('l, F j, Y @ g:i a', strtotime($info->date . " " . $info->time_from)),
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            $tourBooking = $this->tourBooking::find($id);
            if ($request->has('send_rb_mail')) {
                if ($request->send_rb_mail != null && ($request->send_rb_mail == "Yes" || $request->send_rb_mail == true)) {
                    // TODO
                    $tourBooking->notify(new BookingRescheduled($arEmailData, 'booking_rescheduled'));

                    $tourBookingHistory = new TourBookingHistory;
                    $tourBookingHistory->tour_booking_id = $id;
                    $tourBookingHistory->operation = 'Email';
                    $tourBookingHistory->details = 'Rescheduled Booking';
                    $tourBookingHistory->operation_at = Carbon::now();
                    $tourBookingHistory->operation_by = auth()->id();
                    $tourBookingHistory->operation_by_user_type = $userType;
                    $tourBookingHistory->save();
                }
            }

            if (isset($request->api) && $request->api == 1) {
                $tourBooking['dateFrom'] = $booking->date;
                $tourBooking['timeFrom'] = $booking->time_from;
                $tourBooking['dateTo'] = $info->date;
                $tourBooking['timeTo'] = $info->time_from;
                $tourBooking['created_by_with_name'] = $created_by_with_name;

                return $tourBooking;
            } else {
                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                    ->with('message', 'Booking has been rescheduled.');
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Cancel the booking which is already created
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function cancel(object $request, int $id)
    {
        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $qry = DB::table('tour_bookings_master AS tbm')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('customers as cus', 'cus.id', '=', 'tbm.customer_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->where('tbm.id', $id);

            if ($userType == 'Tour Operator') {
                $routePrefix = '';
                $qry = $qry->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            } else {
                $routePrefix = 'admin.';
            }

            $booking = $qry->first([
                'tbm.tour_package_id AS pkg_id', 'tbm.tourists', 'tbm.status',
                'tst.id AS tour_slot_id', 'tp.tour_operator_id', 'tbm.email', 'tbm.name', 'tbm.phone_number', 'cus.is_affiliate','t_o.service_commission_percentage',
                DB::raw("CONCAT(tsd.date, ' ', tst.time_from) AS tour_d_t"),
            ]);

            if (!$booking) {
                throw new Exception("Booking not found.");
            }
            $customer_is_affiliate = $booking->is_affiliate;

            $info = DB::table('tour_bookings_master AS tbm')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->where('tbm.id', $id)
                ->first([
                    'tst.time_from', 'tst.time_to', 'tsd.date', 'tsd.seats', 'tp.name AS package_name', 'tp.id as tour_package_id', 'tp.things_to_bring', 'tp.important_notes',
                    't_o.cancellation_policy', 't_o.id as tour_operator_id', 't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT COUNT(tbm.tourists)
					FROM tour_bookings_master AS tbm
					WHERE tbm.tour_slot_id = tst.id
						AND tbm.status != 'Cancelled'
					GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $id)->first();

            if (!isset($request->api)) {
                $routeParams = ['booking' => $id];
                if ($request->has('modal')) {
                    $routeParams['modal'] = 1;
                }
            }

            $transaction = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->first();
            if (!empty($transaction)) {
                $net_amount = $transaction->amount - $transaction->fee_amount;
            }

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $tourBooking = $this->tourBooking::where('id', $id)->first();
            if ($request->has('cancel_booking') && $booking->status != 'Cancelled') {

                if($request->refund_type == "partial_refund"){

                    if($request->partial_refund_value > $net_amount){
                        if (!isset($request->api)) {
                            $currentURL = Route::currentRouteName();
                            if ($currentURL == "affiliate.booking.cancel") {
                                return redirect()
                                    ->route('affiliate.booking.show', $routeParams)
                                    ->with('message', 'Unable to cancel booking. Please enter amount less than total.');
                            }
                            return redirect()
                                ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                ->withErrors(['errors' => 'Unable to cancel booking. Please enter amount less than total.'])
                                ->withInput();
                        } else {
                            throw new Exception('Unable to cancel booking. Please enter amount less than total.');
                        }
                    }


                    if (!empty($transaction) && ($transaction->via == "Cash" || $transaction->via == "Invoiced" || $transaction->via=='Wire Transfer')) {

                        if ($request->partial_refund_value > $tourBooking->total) {
                            if (!isset($request->api)) {
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.cancel") {
                                    return redirect()
                                        ->route('affiliate.booking.show', $routeParams)
                                        ->withErrors('errors', 'Unable to cancel booking. Please enter amount less than total.');
                                }
                                return redirect()
                                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                    ->withErrors(['errors' => 'Unable to cancel booking. Please enter amount less than total.'])
                                    ->withInput();
                            } else {
                                throw new Exception('Unable to cancel booking. Please enter amount less than total.');
                            }
                        }
                    }

                }

                $update = $qry->update([
                    'tbm.status' => 'Cancelled',
                    'tbm.updated_by' => auth()->id(),
                    'tbm.updated_by_user_type' => $userType,
                ]);
                if ($update) {
                    if (!empty($transaction)) {

                        if ($request->fareharbor == "fareharbor") {
                            if (isset($request->is_cash) && $request->is_cash!='') {
                                if($request->is_cash=='Cash') {
                                    $refundVia = 'Cash';
                                }elseif($request->is_cash=='Wire Transfer'){
                                    $refundVia = 'Check/Wire';
                                }
                            }else{
                                $refundVia = 'Cash';
                            }
                            $trans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'asc')->first();
                            $PaymentHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->get();
                            $RefundHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Refund')->get();
                            $ref = 0;
                            $net_amount_split = 0;
                            foreach ($RefundHistory as $key => $value) {
                                $details = json_decode($value->details);
                                $ref += $details->amount;
                            }
                            foreach ($PaymentHistory as $value) {
                                $details = json_decode($value->details);
                                $net_amount_split += $details->amount;
                            }

                            if ($request->refund_type == 'dont_refund') {
                                $refund_keep_us = 0;
                                if($trans->payment_type == 1){
                                    $net_amount = $net_amount_split - $ref;
                                }else{
                                    $net_amount = $tourBooking->total;
                                }
                                //$net_amount = $tourBooking->total;
                                $refund_type = "fareharbor : don't refund";
                            }

                            if ($request->refund_type == 'partial_refund') {
                                $net_amount = $request->partial_refund_value;
                                $refund_keep_us = $tourBooking->total - $request->partial_refund_value;
                                $refund_type = "fareharbor : Partial Refund";
                            }

                            if ($request->refund_type == 'full_refund') {
                                $net_amount = $tourBooking->total;
                                $refund_keep_us = 0;
                                $refund_type = "fareharbor : Full Refund";
                            }

                            if ($request->refund_type == '100_refund') {
                                if($trans->payment_type == 1){
                                    $net_amount = $net_amount_split - $ref;
                                }else {
                                    $net_amount = $tourBooking->total;
                                }
                                $refund_keep_us = 0;
                                $refund_type = "fareharbor : 100 % Refund";
                            }

                            $Transaction = new Transaction;
                            $Transaction->tour_booking_id = $id;
                            if ($request->refund_type == 'dont_refund') {
                                $Transaction->amount = 0.00;
                            }else {
                                $Transaction->amount = $net_amount;
                            }
                            $Transaction->via = $refundVia;
                            $Transaction->response_init = '';
                            $Transaction->response_end = '';
                            $Transaction->response_end_datetime = Carbon::now();
                            $Transaction->status = 'Refund';
                            $Transaction->visitor = $request->getClientIp();
                            $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                            $Transaction->save();

                            $historyId = DB::table('tour_booking_history')
                                ->insertGetId([
                                    'tour_booking_id' => $id,
                                    'tourists' => $tourBooking->tourists,
                                    'operation' => 'Cancelled',
                                    'details' => json_encode(['amount' => $net_amount, 'refund_keep_us' => $refund_keep_us, 'payment_type' => $refundVia, 'created_by_with_name' => $created_by_with_name, 'refund_type' => $refund_type, 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                    'operation_at' => Carbon::now(),
                                    'operation_by' => auth()->id(),
                                    'operation_by_user_type' => $userType,
                                    'updated_at' => Carbon::now(),
                                    'created_at' => Carbon::now(),
                                ]);
                            DB::commit();

                            // Below line is for to update tour booking history table
                            $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $refundVia, $Transaction->id);
                        }

                        if (isset($request->is_cash) && $request->is_cash!='') {
                            if($request->is_cash=='Cash') {
                                $transaction->via = "Cash";
                            }else if($request->is_cash=='Wire Transfer'){
                                $transaction->via = "Wire Transfer";
                            }
                        }

                        if (($transaction->via) == "Cash" || $transaction->via == "Invoiced" || $transaction->via=='Wire Transfer') {
                            $booking = (array)$booking;
                            unset($booking['status']);
                            $booking['notif_for'] = 'admin';
                            $booking['env'] = config('app.env');
                            // BookingCancelled::dispatch($booking);
                            $booking['notif_for'] = 'Tour Operator';
                            // BookingCancelled::dispatch($booking);
                            $trans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'asc')->first();
                            $PaymentHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->get();
                            $RefundHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Refund')->get();
                            $ref = 0;
                            $net_amount_split = 0;
                            foreach ($RefundHistory as $key => $value) {
                                $details = json_decode($value->details);
                                $ref += $details->amount;
                            }

                            foreach ($PaymentHistory as $value) {
                                $details = json_decode($value->details);
                                $net_amount_split += $details->amount;
                            }

                            if (isset($request->is_cash) && $request->is_cash!='') {
                                if($request->is_cash=='Cash') {
                                    $refundVia = 'Cash';
                                }elseif($request->is_cash=='Wire Transfer'){
                                    $refundVia = 'Check/Wire';
                                }
                            }else{
                                $refundVia = $transaction->via;
                            }
                            if ($request->fareharbor !== "fareharbor") {
                                $Transaction = new Transaction;
                                $Transaction->tour_booking_id = $id;
                                if ($request->refund_type == 'dont_refund') {
                                    $Transaction->amount = 0.00;
                                }else{
                                    $Transaction->amount = $net_amount_split - $ref;
                                }
                                $Transaction->transaction_number = '';
                                $Transaction->via = $refundVia;
                                $Transaction->response_init = '';
                                $Transaction->response_end = '';
                                $Transaction->response_end_datetime = Carbon::now();
                                $Transaction->status = 'Refund';
                                $Transaction->visitor = $request->getClientIp();
                                $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                $Transaction->save();
                                DB::commit();

                                if ($request->refund_type == 'dont_refund') {
                                    if($trans->payment_type == 1){
                                        $net_amount = $net_amount_split - $ref;
                                    }else{
                                        $net_amount = $tourBooking->total;
                                    }
                                    $refund_keep_us = 0;
                                    $refund_type = "don't refund";
                                }


                                if ($request->refund_type == 'partial_refund') {
                                    $net_amount = $request->partial_refund_value;
                                    $refund_keep_us = $tourBooking->total - $request->partial_refund_value;
                                    $refund_type = "Partial Refund";
                                }

                                if ($request->refund_type == 'full_refund') {
                                    $net_amount = $net_amount_split - $ref;
                                    $refund_keep_us = 0;
                                    $refund_type = "Full Refund";
                                }

                                if ($request->refund_type == '100_refund') {
                                    if($trans->payment_type == 1){
                                        $net_amount = $net_amount_split - $ref;
                                    }else{
                                        $net_amount = $tourBooking->total;
                                    }
                                    $refund_keep_us = 0;
                                    $refund_type = "100 % Refund";
                                }

                                $historyId = DB::table('tour_booking_history')
                                    ->insertGetId([
                                        'tour_booking_id' => $id,
                                        'tourists' => $tourBooking->tourists,
                                        'operation' => 'Cancelled',
                                        'details' => json_encode(['amount' => $net_amount, 'refund_keep_us' => $refund_keep_us, 'payment_type' => $refundVia, 'created_by_with_name' => $created_by_with_name, 'note' => $request->comment, 'refund_type' => $refund_type], JSON_NUMERIC_CHECK),
                                        'operation_at' => mysqlDT(),
                                        'operation_by' => auth()->id(),
                                        'operation_by_user_type' => $userType,
                                        'updated_at' => Carbon::now(),
                                        'created_at' => Carbon::now(),
                                    ]);
                                DB::commit();

                                // Below line is for to update tour booking history table
                                $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $refundVia, $Transaction->id);
                            }
                        } elseif ($transaction->via == "Card" && $request->fareharbor != "fareharbor") {
                            $booking = (array)$booking;
                            unset($booking['status']);
                            $booking['notif_for'] = 'admin';
                            $booking['env'] = config('app.env');
                            // BookingCancelled::dispatch($booking);
                            $booking['notif_for'] = 'Tour Operator';
                            // BookingCancelled::dispatch($booking);

                            $firstransaction = $trans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'asc')->first();
                            $paymentTransCount = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->count();
                            $net_amount_split = 0;
                            if(($trans->payment_type == 1 || $paymentTransCount > 1) && ($request->refund_type == "100_refund" || $request->refund_type == "dont_refund")){
                                $trans = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->get();
                                foreach ($trans as $value) {
                                    $refundtrans = $this->transaction::where('tour_booking_id', $id)->where('transaction_number', $value->transaction_number)->where('status', 'Refund')->count();
                                    if ($request->refund_type == "100_refund") {
                                        if ($refundtrans == 0) {
                                            $net_amount = $value->amount;

                                            if($value->transaction_number!='' && $net_amount > 0) {
                                                $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                                $stripe->refunds->create([
                                                    'payment_intent' => $value->transaction_number,
                                                    'amount' => $net_amount * 100,
                                                    'reverse_transfer' => true,
                                                ]);
                                            }
                                            $Transaction = new Transaction;
                                            $Transaction->tour_booking_id = $id;
                                            $Transaction->amount = $net_amount;
                                            $Transaction->fee_amount = $value->fee_amount;
                                            $Transaction->application_fee_id = $value->application_fee_id;
                                            $Transaction->transaction_number = $value->transaction_number;
                                            $Transaction->via = 'Card';
                                            $Transaction->response_init = '';
                                            $Transaction->response_end = '';
                                            $Transaction->response_end_datetime = Carbon::now();
                                            $Transaction->status = 'Refund';
                                            $Transaction->visitor = $request->getClientIp();
                                            $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                            $Transaction->save();
                                        } else {
                                            // This is for transactions for added people to booking
                                            if($paymentTransCount > 1 && $firstransaction->payment_type != 1){
                                                $refundedTransForTransaction = $this->transaction::where('tour_booking_id', $id)->where('transaction_number', $value->transaction_number)->where('status', 'Refund')->get(['id','amount']);
                                                $totalrefundedamountfortransaction = 0;
                                                foreach ($refundedTransForTransaction as $ky => $val) {
                                                    $totalrefundedamountfortransaction += $val->amount;
                                                }
                                                $net_amount = $value->amount-$totalrefundedamountfortransaction;
                                                if ($net_amount >= 0) {
                                                    $net_amount = number_format((float)$net_amount, 2, '.', '');
                                                    if($value->transaction_number!='' && $net_amount > 0) {
                                                        $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                                        $stripe->refunds->create([
                                                            'payment_intent' => $value->transaction_number,
                                                            'amount' => $net_amount * 100,
                                                            'reverse_transfer' => true,
                                                        ]);
                                                    }

                                                    $Transaction = new Transaction;
                                                    $Transaction->tour_booking_id = $id;
                                                    $Transaction->amount = $net_amount;
                                                    $Transaction->fee_amount = round($net_amount * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                                                    $Transaction->application_fee_id = $value->application_fee_id;
                                                    $Transaction->transaction_number = $value->transaction_number;
                                                    $Transaction->via = 'Card';
                                                    $Transaction->response_init = '';
                                                    $Transaction->response_end = '';
                                                    $Transaction->response_end_datetime = Carbon::now();
                                                    $Transaction->status = 'Refund';
                                                    $Transaction->visitor = $request->getClientIp();
                                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                                    $Transaction->save();
                                                }

                                                $net_amount_split += $net_amount;
                                            }else if($paymentTransCount >= 1 && $firstransaction->payment_type == 1){
                                                $refundedTransForTransaction = $this->transaction::where('tour_booking_id', $id)->where('transaction_number', $value->transaction_number)->where('status', 'Refund')->get(['id','amount']);
                                                $totalrefundedamountfortransaction = 0;
                                                foreach ($refundedTransForTransaction as $ky => $val) {
                                                    $totalrefundedamountfortransaction += $val->amount;
                                                }
                                                $net_amount = $value->amount-$totalrefundedamountfortransaction;
                                                if ($net_amount >= 0) {
                                                    $net_amount = number_format((float)$net_amount, 2, '.', '');
                                                    if($value->transaction_number!='' && $net_amount > 0) {
                                                        $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                                        $stripe->refunds->create([
                                                            'payment_intent' => $value->transaction_number,
                                                            'amount' => $net_amount * 100,
                                                            'reverse_transfer' => true,
                                                        ]);
                                                    }

                                                    $Transaction = new Transaction;
                                                    $Transaction->tour_booking_id = $id;
                                                    $Transaction->amount = $net_amount;
                                                    $Transaction->fee_amount = round($net_amount * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                                                    $Transaction->application_fee_id = $value->application_fee_id;
                                                    $Transaction->transaction_number = $value->transaction_number;
                                                    $Transaction->via = 'Card';
                                                    $Transaction->response_init = '';
                                                    $Transaction->response_end = '';
                                                    $Transaction->response_end_datetime = Carbon::now();
                                                    $Transaction->status = 'Refund';
                                                    $Transaction->visitor = $request->getClientIp();
                                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                                    $Transaction->save();
                                                }
                                                $net_amount_split += $net_amount;
                                            }
                                        }
                                        $refund_keep_us = 0;
                                    }

                                    if ($request->refund_type == "100_refund") {
                                        if ($refundtrans == 0) {
                                            $net_amount_split += $value->amount;
                                        }
                                    } elseif ($request->refund_type == "dont_refund") {
                                        $net_amount_split += $value->amount;
                                    }
                                }
                                if ($request->refund_type == "100_refund") {

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode([
                                                'amount' => $net_amount_split,
                                                'refund_keep_us' => $refund_keep_us,
                                                'payment_type' => 'Card',
                                                'created_by_with_name' => $created_by_with_name,
                                                'refund_type' => '100 % Refund',
                                                'note' => $request->comment
                                            ], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(),
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    if(isset($Transaction->id)) {
                                        // Below line is for to update tour booking history table
                                        $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), 'Card', $Transaction->id);
                                    }
                                }
                                if ($request->refund_type == "dont_refund") {
                                    $RefundHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Refund')->get();
                                    $ref = 0;
                                    foreach ($RefundHistory as $key => $value) {
                                        $details = json_decode($value->details);
                                        $ref += $details->amount;
                                    }
                                    $net_amount_split = $net_amount_split - $ref;
                                    $Transaction = new Transaction;
                                    $Transaction->tour_booking_id = $id;
                                    $Transaction->amount = 0.00;//$net_amount_split;
                                    $Transaction->fee_amount = 0;
                                    $Transaction->transaction_number = $transaction->transaction_number;
                                    $Transaction->via = 'Card';
                                    $Transaction->response_init = '';
                                    $Transaction->response_end = '';
                                    $Transaction->response_end_datetime = Carbon::now();
                                    $Transaction->status = 'Refund';
                                    $Transaction->visitor = $request->getClientIp();
                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                    $Transaction->save();
                                    $refund_keep_us = $transaction->amount;

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode(['amount' => $net_amount_split, 'refund_keep_us' => $refund_keep_us, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name, 'refund_type' => "Don't Refund", 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    // Below line is for to update tour booking history table
                                    $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $Transaction->via, $Transaction->id);
                                }
                            } else {
                                if ($request->refund_type == "dont_refund") {
                                    $Transaction = new Transaction;
                                    $Transaction->tour_booking_id = $id;
                                    $Transaction->amount = 0.00;//$tourBooking->total;
                                    $Transaction->fee_amount = 0.00;//$tourBooking->fee_amount;
                                    $Transaction->transaction_number = $transaction->transaction_number;
                                    $Transaction->via = 'Card';
                                    $Transaction->response_init = '';
                                    $Transaction->response_end = '';
                                    $Transaction->response_end_datetime = Carbon::now();
                                    $Transaction->status = 'Refund';
                                    $Transaction->visitor = $request->getClientIp();
                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                    $Transaction->save();
                                    $refund_keep_us = $transaction->amount;

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode(['amount' => $tourBooking->total, 'refund_keep_us' => $refund_keep_us, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name, 'refund_type' => "Don't Refund", 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    // Below line is for to update tour booking history table
                                    $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $Transaction->via, $Transaction->id);
                                }

                                if ($request->refund_type == "partial_refund") {
                                    $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                    $refund = $stripe->refunds->create([
                                        'payment_intent' => $transaction->transaction_number,
                                        'amount' => $request->partial_refund_value * 100,
                                        'reverse_transfer' => true,
                                    ]);
                                    $Transaction = new Transaction;
                                    $Transaction->tour_booking_id = $id;
                                    $Transaction->amount = $request->partial_refund_value;
                                    $Transaction->transaction_number = $transaction->transaction_number;
                                    $Transaction->via = 'Card';
                                    $Transaction->response_init = '';
                                    $Transaction->response_end = '';
                                    $Transaction->response_end_datetime = Carbon::now();
                                    $Transaction->status = 'Refund';
                                    $Transaction->visitor = $request->getClientIp();
                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                    $Transaction->save();
                                    $refund_keep_us = $transaction->amount - $request->partial_refund_value;

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode(['amount' => $request->partial_refund_value, 'refund_keep_us' => $refund_keep_us, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Partial Refund', 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    // Below line is for to update tour booking history table
                                    $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $Transaction->via, $Transaction->id);
                                }

                                if ($request->refund_type == "full_refund") {
                                    $RefundHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Refund')->get();
                                    $oldRefundKeepUs = 0;
                                    foreach ($RefundHistory as $key => $value) {
                                        $details = json_decode($value->details);
                                        if(isset($details->refund_type) && $details->refund_type=='Full Refund minus Fees'){
                                            if(isset($details->refund_keep_us) && $details->refund_keep_us>0) {
                                                $oldRefundKeepUs += $details->refund_keep_us;
                                            }
                                        }
                                    }
                                    $net_amount = $tourBooking->total + $oldRefundKeepUs - $tourBooking->service_commission;
                                    $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                    $refund = $stripe->refunds->create([
                                        'payment_intent' => $transaction->transaction_number,
                                        'amount' => $net_amount * 100,
                                        'reverse_transfer' => true,
                                    ]);

                                    $Transaction = new Transaction;
                                    $Transaction->tour_booking_id = $id;
                                    $Transaction->amount = $net_amount;
                                    $Transaction->transaction_number = $transaction->transaction_number;
                                    $Transaction->via = 'Card';
                                    $Transaction->response_init = '';
                                    $Transaction->response_end = '';
                                    $Transaction->response_end_datetime = Carbon::now();
                                    $Transaction->status = 'Refund';
                                    $Transaction->visitor = $request->getClientIp();
                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                    $Transaction->save();
                                    $refund_keep_us = $tourBooking->service_commission - $oldRefundKeepUs;

                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode(['amount' => $net_amount, 'refund_keep_us' => $refund_keep_us, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Full Refund', 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(),
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    // Below line is for to update tour booking history table
                                    $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $Transaction->via, $Transaction->id);
                                }

                                if ($request->refund_type == "100_refund") {
                                    $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                    $refund = $stripe->refunds->create([
                                        'payment_intent' => $transaction->transaction_number,
                                        'amount' => $tourBooking->total * 100,
                                        'reverse_transfer' => true,
                                    ]);
                                    $Transaction = new Transaction;
                                    $Transaction->tour_booking_id = $id;
                                    $Transaction->amount = $tourBooking->total;
                                    $Transaction->fee_amount = round($tourBooking->total * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                                    $Transaction->transaction_number = $transaction->transaction_number;
                                    $Transaction->application_fee_id = $transaction->application_fee_id;
                                    $Transaction->via = 'Card';
                                    $Transaction->response_init = '';
                                    $Transaction->response_end = '';
                                    $Transaction->response_end_datetime = Carbon::now();
                                    $Transaction->status = 'Refund';
                                    $Transaction->visitor = $request->getClientIp();
                                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                    $Transaction->save();

                                    $refund_keep_us = 0;
                                    $historyId = DB::table('tour_booking_history')
                                        ->insertGetId([
                                            'tour_booking_id' => $id,
                                            'tourists' => $tourBooking->tourists,
                                            'operation' => 'Cancelled',
                                            'details' => json_encode(['amount' => $tourBooking->total, 'refund_keep_us' => $refund_keep_us, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name, 'refund_type' => '100 % Refund', 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                            'operation_at' => mysqlDT(),
                                            'operation_by' => auth()->id(),
                                            'operation_by_user_type' => $userType,
                                            'updated_at' => Carbon::now(),
                                            'created_at' => Carbon::now(),
                                        ]);
                                    DB::commit();

                                    // Below line is for to update tour booking history table
                                    $this->handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request->all(), $Transaction->via, $Transaction->id);
                                }
                            }
                        }
                    } else {
                        $booking = (array)$booking;
                        unset($booking['status']);
                        $booking['notif_for'] = 'admin';
                        $booking['env'] = config('app.env');
                        // BookingCancelled::dispatch($booking);
                        $booking['notif_for'] = 'Tour Operator';
                        // BookingCancelled::dispatch($booking);
                        $historyId = DB::table('tour_booking_history')
                            ->insertGetId([
                                'tour_booking_id' => $id,
                                'tourists' => $tourBooking->tourists,
                                'operation' => 'Cancelled',
                                'details' => json_encode(['amount' => 0, 'payment_type' => '', 'created_by_with_name' => $created_by_with_name, 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                'operation_by' => auth()->id(),
                                'operation_by_user_type' => $userType,
                                'updated_at' => Carbon::now(),
                                'created_at' => Carbon::now(),
                            ]);
                        DB::commit();
                    }
                } else {
                    DB::rollBack();
                    if (!isset($request->api)) {
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.cancel") {
                            return redirect()
                                ->route('affiliate.booking.show', $routeParams)
                                ->with('message', 'Unable to cancel booking');
                        }
                        return redirect()
                            ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                            ->withErrors(['errors' => 'Unable to cancel booking.'])
                            ->withInput();
                    } else {
                        throw new Exception('Unable to cancel booking.');
                    }
                }
            }

            if ($request->amount != 0) {
                $this->transaction::create([
                    'tour_booking_id' => $id,
                    'amount' => -$request->amount, // negative amount for refund
                    'transaction_number' => $request->transaction_number,
                    'via' => '',
                    'response_init' => '',
                    'response_end' => '',
                    'response_end_datetime' => '0000-00-00 00:00:00',
                    'status' => 'Completed',
                    'visitor' => $request->getClientIp(),
                    'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND
                ]);
                DB::commit();

                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $id,
                        'tourists' => $tourBooking->tourists,
                        'operation' => 'Cancelled',
                        'details' => json_encode(['amount' => $request->amount], JSON_NUMERIC_CHECK),
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();
            }
            DB::commit();

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $id)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }

            $booking = is_array($booking) ? $booking : (array)$booking;
            $TourBookingHistory = $this->tourBookingHistory::where('tour_booking_id', $id)->first();

            if (!empty($transaction)) {
                if (isset($request->is_cash)) {
                    $transaction->via = "Cash";
                }
                if ($transaction->via == "Card") {
                    $details = json_decode($TourBookingHistory->details);
                }
            }

            $tourBooking = $this->tourBooking::find($id);
            if ($booking['email']) {
                $arEmailData = [
                    'bookingId' => $id ?? '',
                    'tourOperatorId' => $info->tour_operator_id,
                    'tourOperator' => $info->tour_operator ?? '',
                    'tourOpPhone' => $info->tour_op_phone ?? '',
                    'tourOpEmail' => $info->tour_op_email ?? '',
                    'tourOpWebsite' => $info->tour_op_website ?? '',
                    'tourPackage' => $info->package_name ?? '',
                    'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])) ?? '',
                    'custName' => $booking['name'] ?? '',
                    'custEmail' => $booking['email'] ?? '',
                    'custPhone' => $booking['phone_number'] ?? '',
                    'numberoftourist' => $totalTourists,
                    'tourListwithSeat' => $tourListwithSeat,
                    'cardNumber' => $details->card_number ?? '',
                    'packageEmail' => $info->packageEmail,
                    'packageFromEmail' => $info->packageFromEmail,
                    'packageApiKey' => $info->packageApiKey
                ];

                if (!empty($info->tour_op_logo)) {
                    if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                        $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                    } else {
                        $tourOperatorLogo = asset('images/no-photo.png');
                    }
                    $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                }

                $tourPkgPhoto = DB::table('tour_package_photos')
                    ->where('tour_package_id', $info->tour_package_id)
                    ->where('placement', 2)
                    ->where('status', 'Active')
                    ->where('deleted_by', 0)
                    ->latest()
                    ->first(['path']);

                if ($tourPkgPhoto) {
                    if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                        $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                    } else {
                        $tourPackageLogo = asset('images/no-photo.png');
                    }
                    $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                }
                // TODO
                $tourBooking->notify(new TourBookingCancelled($arEmailData, 'tour_booking_cancelled'));
            }

            if (isset($request->api) && $request->api == 1) {
                return $tourBooking;
            } else {
                $currentURL = Route::currentRouteName();
                if ($currentURL == "affiliate.booking.cancel") {
                    return redirect()
                        ->route('affiliate.booking.show', $routeParams)
                        ->with('message', 'Booking has been canceled and the refund is being issued');
                }
                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                    ->with('message', 'Booking has been canceled and the refund is being issued.');
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            if (isset($request->api) && $request->api == 1) {
                throw new GeneralCoreException($exception);
            } else {
                $currentURL = Route::currentRouteName();
                if ($currentURL == "affiliate.booking.cancel") {
                    return redirect()
                        ->route('affiliate.booking.show', $routeParams)
                        ->withErrors(['errors' => 'Unable to cancel booking.' . ' ' . $exception->getMessage()]);
                }
                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                    ->withErrors(['errors' => 'Unable to cancel booking.' . ' ' . $exception->getMessage()]);
            }
        }
    }

    /**
     * Restore the booking which is cancelled
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function restoreBooking(object $request, int $id)
    {
        DB::beginTransaction();
        try {
            // Fetch
            $booking = $this->tourBooking::find($id);
            $history = $this->tourBookingHistory::where(['tour_booking_id' => $id, 'operation' => 'Cancelled'])->first();

            $transaction = null;
            if (isset($history->transaction_id)) {
                $transaction = $this->transaction::find($history->transaction_id);
            }

            // Actions
            if ($transaction)
                $transaction->delete();
            if ($history)
                $history->delete();
            if ($booking)
                $booking->status = 'Booked';
            $booking->save();

            // Manipulate
            if (auth()->user()->tour_operator_id) {
                $userType = "Tour Operator";
                $created_by_with_name = auth()->user()->name . ': (' . $userType . ')';
            } else {
                $userType = "Admin";
                $created_by_with_name = auth()->user()->name . ': (' . $userType . ')';
            }

            // Add
            $tourBookingHistory = new $this->tourBookingHistory();
            $tourBookingHistory->tour_booking_id = $id;
            $tourBookingHistory->operation = 'Restore';
            $tourBookingHistory->details = json_encode(['created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK);
            $tourBookingHistory->operation_at = Carbon::now();
            $tourBookingHistory->operation_by = auth()->id();
            $tourBookingHistory->operation_by_user_type = $userType;
            $tourBookingHistory->save();

            DB::commit();

            return true;
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            if (isset($request->api) && $request->api == 1) {
                throw new GeneralCoreException($exception);
            } else {
                if (auth()->user()->tour_operator_id) {
                    $routePrefix = '';
                } else {
                    $routePrefix = 'admin.';
                }

                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', ['booking' => $id])
                    ->withErrors(['errors' => 'Unable to restore booking.' . ' ' . $exception->getMessage()]);
            }
        }
    }

    /**
     * Change the payment type of booking
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function changeHistoryPaymentType(object $request, int $id)
    {
        DB::beginTransaction();
        try {
            // Fetch
            $history = $this->tourBookingHistory::find($id);
            $transaction = $this->transaction::find($history->transaction_id);

            // Manipulate
            $originalPaymentType = $history->payment_type;
            $paymentType = $originalPaymentType == 'Cash' ? 'Invoiced' : 'Cash';
            $jsonData = json_decode($history->details, true);
            $jsonData['payment_type'] = $paymentType;

            if (auth()->user()->tour_operator_id) {
                $userType = "Tour Operator";
                $created_by_with_name = auth()->user()->name . ': (' . $userType . ')';
            } else {
                $userType = "Admin";
                $created_by_with_name = auth()->user()->name . ': (' . $userType . ')';
            }

            // Actions
            $history->payment_type = $paymentType;
            $history->details = json_encode($jsonData, JSON_NUMERIC_CHECK);
            $history->save();

            $transaction->via = $paymentType;
            $transaction->save();

            // Add
            $tourBookingHistory = new $this->tourBookingHistory();
            $tourBookingHistory->tour_booking_id = $history->tour_booking_id;
            $tourBookingHistory->operation = 'Payment Type Update';
            $tourBookingHistory->details = json_encode(['history_id' => $id,'from' => $originalPaymentType,'to' => $paymentType,'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK);
            $tourBookingHistory->operation_at = Carbon::now();
            $tourBookingHistory->operation_by = auth()->id();
            $tourBookingHistory->operation_by_user_type = $userType;
            $tourBookingHistory->save();

            DB::commit();

            return true;
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            if (isset($request->api) && $request->api == 1) {
                throw new GeneralCoreException($exception);
            } else {
                if (auth()->user()->tour_operator_id) {
                    $routePrefix = '';
                } else {
                    $routePrefix = 'admin.';
                }

                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', ['booking' => $id])
                    ->withErrors(['errors' => 'Unable to change payment type.' . ' ' . $exception->getMessage()]);
            }
        }
    }

    /**
     * Handle the queue jobs to perform the history data updates when booking cancelled
     *
     * @param  object  $tourBooking
     * @param  int     $historyId
     * @param  array   $request
     * @param  string  $refundVia
     * @param  int     $transactionId
     *
     * @return void
     */
    public function handleCancelRefundHistoryUpdateJobs($tourBooking, $historyId, $request, $refundVia, $transactionId): void
    {
        // Perform the jobs based on categories
        if ($request['refund_type'] == 'dont_refund') {
            dispatch(new CancelDontRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_type'] == 'partial_refund') {
            CancelPartialRefundService::run($historyId, $refundVia, $transactionId);
            // dispatch(new CancelPartialRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_type'] == 'full_refund') {
            CancelFullRefundService::run($tourBooking, $historyId, $refundVia, $transactionId);
            // dispatch(new CancelFullRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_type'] == '100_refund') {
            Cancel100RefundService::run($tourBooking, $historyId, $refundVia, $transactionId);
            // dispatch(new Cancel100Refund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        }
    }

    /**
     * Refund booking which is already created
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function refund(object $request, int $id)
    {

        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $qry = DB::table('tour_bookings_master AS tbm')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('customers as cus', 'cus.id', '=', 'tbm.customer_id')
                ->where('tbm.id', $id);

            if (auth()->user()->tour_operator_id) {
                $routePrefix = '';
                $qry = $qry->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            } else {
                $routePrefix = 'admin.';
            }

            $booking = $qry->first([
                'tbm.tour_package_id AS pkg_id', 'tbm.tourists', 'tbm.status',
                'tst.id AS tour_slot_id', 'tp.tour_operator_id', 'tbm.email', 'tbm.name', 'tbm.phone_number', 'cus.is_affiliate', 'tbm.total',
                DB::raw("CONCAT(tsd.date, ' ', tst.time_from) AS tour_d_t"),
            ]);

            if (!$booking) {
                throw new Exception("Booking not found.");
            }

            if (!isset($request->api)) {
                $routeParams = ['booking' => $id];
                if ($request->has('modal'))
                    $routeParams['modal'] = 1;
            }

            $transaction = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->first();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            } else {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $paymentOnly = $this->tourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->get();
            $refundOnly = $this->tourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Refund')->get();
            $paymentOnlyCheck = 0;
            $refundOnlyCheck = 0;
            foreach ($paymentOnly as $value) {
                $details = json_decode($value->details);
                $paymentOnlyCheck += $details->amount;
            }
            foreach ($refundOnly as $key => $value) {
                $details = json_decode($value->details);
                $refundOnlyCheck += $details->amount;
            }
            if ($request->has('refund_booking')) {
                $update = $this->tourBooking::where('id', $id)->update(['updated_by' => auth()->id(), 'updated_by_user_type' => $userType]);

                if ($update) {
                    if (!empty($transaction)) {
                        if ($request->partial_refund_value > ($paymentOnlyCheck - $refundOnlyCheck)) {
                            if (isset($request->api) && $request->api == 1) {
                                throw new Exception('Unable to refund booking. Please enter amount less than total.');
                            } else {
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.refund") {
                                    return redirect()
                                        ->route('affiliate.booking.show', $routeParams)
                                        ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                        ->withInput();
                                }

                                return redirect()
                                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                    ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                    ->withInput();
                            }
                        }
                        $splitTotal = 0;
                        if($request->refund_booking == 2){
                            $check = $request->input('check', []);
                            $partial_refund = $request->input('partial_refund', []);
                            $refund_type = "Partial Refund";
                            foreach ($check as $key => $value) {
                                foreach ($partial_refund as $kp => $partial) {
                                    if($key == $kp && !empty($partial) && !empty($value)){

                                        $trans = $this->transaction::where('transaction_number', $value)->first();
                                        if(!isset($request->is_cash)){
                                            if($partial > $trans->amount){
                                                if (isset($request->api) && $request->api == 1) {
                                                    throw new Exception('Unable to refund booking. Please enter amount less than total.');
                                                } else {
                                                    $currentURL = Route::currentRouteName();
                                                    if ($currentURL == "affiliate.booking.refund") {
                                                        return redirect()
                                                            ->route('affiliate.booking.show', $routeParams)
                                                            ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                                            ->withInput();
                                                    }

                                                    return redirect()
                                                        ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                                        ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                                        ->withInput();
                                                }

                                            }
                                            $stripe = new \Stripe\StripeClient(env('STRIPE_API_SECRET_KEY'));
                                            $refund = $stripe->refunds->create([
                                                'payment_intent' => $trans->transaction_number,
                                                'amount' => $partial * 100,
                                                'reverse_transfer' => true,
                                            ]);
                                        }

                                        $Transaction = new Transaction;
                                        $Transaction->tour_booking_id = $id;
                                        $Transaction->amount = $partial;
                                        $Transaction->transaction_number = $trans->transaction_number;
                                        $Transaction->application_fee_id = $trans->application_fee_id;
                                        $Transaction->fee_amount = '';//$trans->fee_amount;
                                        $Transaction->via = !empty($request->is_cash) ? $request->is_cash : 'Card';
                                        $Transaction->response_init = '';
                                        $Transaction->response_end = '';
                                        $Transaction->response_end_datetime = '';
                                        $Transaction->status = 'Refund';
                                        $Transaction->visitor = $request->getClientIp();
                                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                        $Transaction->created_by = auth()->id();
                                        $Transaction->created_by_user_type = $userType;
                                        $Transaction->save();

                                        $splitTotal += $partial;
                                    }
                                    if($key == $kp && empty($partial) && !empty($value)){

                                        $trans = $this->transaction::where('transaction_number', $value)->first();
                                        if(!isset($request->is_cash)){
                                            $stripe = new \Stripe\StripeClient(env('STRIPE_API_SECRET_KEY'));
                                            $refund = $stripe->refunds->create([
                                                'payment_intent' => $trans->transaction_number,
                                                'amount' => $trans->amount * 100,
                                                'reverse_transfer' => true,
                                            ]);
                                        }

                                        $Transaction = new Transaction;
                                        $Transaction->tour_booking_id = $id;
                                        $Transaction->amount = $trans->amount;
                                        $Transaction->transaction_number = $trans->transaction_number;
                                        $Transaction->application_fee_id = $trans->application_fee_id;
                                        $Transaction->fee_amount = '';//$trans->fee_amount;
                                        $Transaction->via = !empty($request->is_cash) ? $request->is_cash : 'Card';
                                        $Transaction->response_init = '';
                                        $Transaction->response_end = '';
                                        $Transaction->response_end_datetime = '';
                                        $Transaction->status = 'Refund';
                                        $Transaction->visitor = $request->getClientIp();
                                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                        $Transaction->created_by = auth()->id();
                                        $Transaction->created_by_user_type = $userType;
                                        $Transaction->save();

                                        $splitTotal += $trans->amount;
                                    }


                                }
                            }
                            if(isset($request->is_cash)){
                                if($request->is_cash == "Cash"){
                                    $trans->via = 'Cash';
                                }else{
                                    $trans->via = "Cheque/Wire";
                                }
                            }
                            if(empty($trans)){
                                if (isset($request->api) && $request->api == 1) {
                                    throw new Exception(' Please select checkbox.');
                                } else {
                                    $currentURL = Route::currentRouteName();
                                    if ($currentURL == "affiliate.booking.refund") {
                                        return redirect()
                                            ->route('affiliate.booking.show', $routeParams)
                                            ->withErrors(['errors' => 'Please select checkbox .'])
                                            ->withInput();
                                    }

                                    return redirect()
                                        ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                        ->withErrors(['errors' => 'Please select checkbox .'])
                                        ->withInput();
                                }

                            }
                            $details = json_encode([
                                'amount' => $splitTotal,
                                'refund_keep_us' => 0,
                                'payment_type' => $trans->via,
                                'created_by_with_name' => $created_by_with_name,
                                'refund_type' => $refund_type,
                                'note' => $request->comment
                            ], JSON_NUMERIC_CHECK);

                            $historyId = DB::table('tour_booking_history')
                                ->insertGetId([
                                    'tour_booking_id' => $id,
                                    'tourists' => $booking->tourists,
                                    'operation' => 'Refund',
                                    'details' => $details,
                                    'operation_at' => mysqlDT(),
                                    'operation_by' => auth()->id(),
                                    'operation_by_user_type' => $userType,
                                    'updated_at' => Carbon::now(),
                                    'created_at' => Carbon::now(),
                                ]);
                            DB::commit();

                            // Below line is for to update tour booking history table
                            RefundBookingService::run($Transaction->id ?? null, $historyId, $trans->via);
                            // dispatch(new RefundBooking($booking, $Transaction->id ?? null, $historyId, $request->all(), $trans->via));
                        }else{

                            $this->tourBooking::where('id', $id)->update(['total'=>number_format((float)$booking->total-$request->partial_refund_value, 2, '.', '')]);
                            $refund_keep_us = $transaction->amount - (float)$request->partial_refund_value;
                            $refund_keep_us = number_format((float)$refund_keep_us, 2, '.', '');
                            $total = number_format((float)$request->partial_refund_value, 2, '.', '');
                            $refund_type = "Partial Refund";
                            if (isset($request->is_cash) && $request->is_cash!='') {
                                if($request->is_cash=='Cash') {
                                    $transaction->via = 'Cash';
                                }elseif($request->is_cash=='Wire Transfer'){
                                    $transaction->via = 'Check/Wire';
                                }
                            }

                            if ($transaction->via == 'Card') {
                                $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                $refund = $stripe->refunds->create([
                                    'payment_intent' => $transaction->transaction_number,
                                    'amount' => $request->partial_refund_value * 100,
                                    'reverse_transfer' => true,
                                ]);
                            }

                            $details = json_encode([
                                'amount' => $total,
                                'refund_keep_us' => $refund_keep_us,
                                'payment_type' => $transaction->via,
                                'created_by_with_name' => $created_by_with_name,
                                'refund_type' => $refund_type,
                                'note' => $request->comment
                            ], JSON_NUMERIC_CHECK);

                            $historyId = DB::table('tour_booking_history')
                                ->insertGetId([
                                    'tour_booking_id' => $id,
                                    'tourists' => $booking->tourists,
                                    'operation' => 'Refund',
                                    'details' => $details,
                                    'operation_at' => mysqlDT(),
                                    'operation_by' => auth()->id(),
                                    'operation_by_user_type' => $userType,
                                    'updated_at' => Carbon::now(),
                                    'created_at' => Carbon::now(),
                                ]);
                            DB::commit();

                            $TS = $this->transaction::where('tour_booking_id', $id)->where('status', 'Completed')->first();

                            if (!empty($transaction) && $transaction->via == 'Card') {
                                $Transaction = new Transaction;
                                $Transaction->tour_booking_id = $id;
                                $Transaction->amount = $total;
                                $Transaction->application_fee_id = $TS->application_fee_id;
                                $Transaction->transaction_number = $TS->transaction_number;
                                $Transaction->fee_amount = '';
                                $Transaction->via = 'Card';
                                $Transaction->response_init = '';
                                $Transaction->response_end = '';
                                $Transaction->response_end_datetime = '';
                                $Transaction->status = 'Refund';
                                $Transaction->visitor = $request->getClientIp();
                                $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                $Transaction->created_by = auth()->id();
                                $Transaction->created_by_user_type = $userType;
                                $Transaction->save();
                            } elseif (!empty($transaction) && ($transaction->via == 'Cash' || $transaction->via=='Check/Wire')) {
                                $Transaction = new Transaction;
                                $Transaction->tour_booking_id = $id;
                                $Transaction->amount = $total;
                                $Transaction->transaction_number = '';
                                $Transaction->fee_amount = '';
                                $Transaction->via = $transaction->via;
                                $Transaction->response_init = '';
                                $Transaction->response_end = '';
                                $Transaction->response_end_datetime = '';
                                $Transaction->status = 'Refund';
                                $Transaction->visitor = $request->getClientIp();
                                $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                                $Transaction->created_by = auth()->id();
                                $Transaction->created_by_user_type = $userType;
                                $Transaction->save();
                            }
                            DB::commit();

                            // Below line is for to update tour booking history table
                            RefundBookingService::run($Transaction->id ?? null, $historyId, $transaction->via);
                            // dispatch(new RefundBooking($booking, $Transaction->id ?? null, $historyId, $request->all(), $transaction->via));
                        }


                    } else {
                        if ($request->partial_refund_value > $booking->total) {
                            if (isset($request->api) && $request->api == 1) {
                                throw new Exception('Unable to refund booking.Please enter amount less than total.');
                            } else {
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.refund") {
                                    return redirect()
                                        ->route('affiliate.booking.show', $routeParams)
                                        ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                        ->withInput();
                                }
                                return redirect()
                                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                                    ->withErrors(['errors' => 'Unable to refund booking.Please enter amount less than total .'])
                                    ->withInput();
                            }
                        }
                        /* $this->tourBooking::where('id', $id)->update(['total' => ($booking->total - (float)$request->partial_refund_value)]);*/
                        DB::table('tour_booking_history')
                            ->insert([
                                'tour_booking_id' => $id,
                                'tourists' => $booking->tourists,
                                'operation' => 'Refund',
                                'details' => json_encode(['amount' => 0, 'payment_type' => '', 'created_by_with_name' => $created_by_with_name, 'note' => $request->comment], JSON_NUMERIC_CHECK),
                                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                'operation_by' => auth()->id(),
                                'operation_by_user_type' => $userType,
                                'updated_at' => Carbon::now(),
                                'created_at' => Carbon::now(),
                            ]);
                        DB::commit();

                    }
                } else {
                    if (isset($request->api) && $request->api == 1) {
                        throw new Exception('Unable to refund booking.');
                    } else {
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.cancel") {
                            return redirect()
                                ->route('affiliate.booking.show', $routeParams)
                                ->with('message', 'Unable to refund booking');
                        }
                        return redirect()
                            ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                            ->withErrors(['errors' => 'Unable to refund booking.'])
                            ->withInput();
                    }
                }
            }
            DB::commit();
            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_bookings_master as bm', 'bm.tour_slot_id', '=', 'tst.id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('bm.id', $id)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'bm.name As custname', 'bm.phone_number As phone_number', 'bm.email As custemail', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            $info = $info->first();

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $id)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();
            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }
            $booking = is_array($booking) ? $booking : (array)$booking;
            $TourBookingHistory = $this->tourBookingHistory::where('tour_booking_id', $id)->first();

            if (!empty($transaction)) {
                if (isset($request->is_cash)) {
                    $transaction->via = 'Cash';
                }
                if ($transaction->via == "Card") {
                    $details = json_decode($TourBookingHistory->details);
                }
            }

            if ($booking['email']) {
                $arEmailData = [
                    'bookingId' => $id,
                    'tourOperatorId' => $info->tour_operator_id,
                    'tourOperator' => $info->tour_operator,
                    'tourOpPhone' => $info->tour_op_phone,
                    'tourOpEmail' => $info->tour_op_email,
                    'tourOpWebsite' => $info->tour_op_website,
                    'tourPackage' => $info->package_name,
                    'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                    'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($id)]),
                    'custName' => $info->custname,
                    'custEmail' => $info->custemail,
                    'custPhone' => $info->phone_number ?? '',
                    'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($id)]),
                    'importantNotes' => $info->important_notes,
                    'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                    'cancellationPolicy' => $info->cancellation_policy,
                    'numberoftourist' => $totalTourists,
                    'tourListwithSeat' => $tourListwithSeat,
                    'packageEmail' => $info->packageEmail,
                    'packageFromEmail' => $info->packageFromEmail,
                    'packageApiKey' => $info->packageApiKey,
                    'refundedAmount' => $request->partial_refund_value,
                ];

                if (!empty($info->tour_op_logo)) {
                    if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                        $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                    } else {
                        $tourOperatorLogo = asset('images/no-photo.png');
                    }
                    $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                }

                $tourPkgPhoto = DB::table('tour_package_photos')
                    ->where('tour_package_id', $info->tour_package_id)
                    ->where('placement', 2)
                    ->where('status', 'Active')
                    ->where('deleted_by', 0)
                    ->latest()
                    ->first(['path']);

                if ($tourPkgPhoto) {
                    if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                        $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                    } else {
                        $tourPackageLogo = asset('images/no-photo.png');
                    }
                    $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                }
                // TODO
                $tourBooking = $this->tourBooking::find($id);
                $tourBooking->notify(new TourBookingRefund($arEmailData, 'tour_booking_refund'));
            }
            if (isset($request->api) && $request->api == 1) {
                return $tourBooking;
            } else {
                $currentURL = Route::currentRouteName();
                if ($currentURL == "affiliate.booking.refund") {
                    return redirect()
                        ->route('affiliate.booking.show', $routeParams)
                        ->with('message', 'Booking refund is being issued.');
                }

                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                    ->with('message', 'Booking refund is being issued.');
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            if (isset($request->api) && $request->api == 1) {
                throw new GeneralCoreException($exception);
            } else {
                $currentURL = Route::currentRouteName();
                if ($currentURL == "affiliate.booking.refund") {
                    return redirect()
                        ->route('affiliate.booking.show', $routeParams)
                        ->withErrors(['errors' => 'Unable to refund booking.' . ' ' . $exception->getMessage()]);
                }
                return redirect()
                    ->route($routePrefix . 'tour-operator.booking.show', $routeParams)
                    ->withErrors(['errors' => 'Unable to refund booking.' . ' ' . $exception->getMessage()]);
            }
        }
    }

    /**
     * Search the booking which is already created
     *
     * @param object $request
     * @return object
     */
    public function search(object $request)
    {
        try {
            $search   = $request->search;

            $bookings = DB::table('tour_bookings_master AS tbm')
                ->join('tour_bookings_transact AS tbt', 'tbt.tour_booking_id', '=', 'tbm.id')
                ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->groupBy('tbt.tour_booking_id')
                ->orderByDesc('tbm.created_at')
                ->take(50);

            $bookings = $bookings->where('tp.tour_operator_id', $request->tour_operator_id);

            $bookings = $bookings->where(function ($qry) use ($search) {
                $qry->where('tbm.name', 'LIKE', "%{$search}%")->orWhere('tbm.email', '=', "$search")->orWhere('tbm.phone_number', '=', "$search")->orWhere('tbm.note', 'LIKE', "%{$search}%")->orWhere('tbm.comments', '=', "$search")
                    ->orWhere('tbm.id', $search);
            });

            $response = $bookings->get([
                'tbm.id', 'tbm.name AS cust_name', 'c.is_affiliate', 'tp.name AS pkg_name',
                'tbm.email AS cust_email','tbm.total', 'tbm.created_at AS booking_d_t',
                DB::raw(
                    "(SELECT SUM(t.amount)
                    FROM transactions AS t
                    WHERE t.tour_booking_id = tbm.id
                            AND t.status = 'Completed'
                    GROUP BY t.tour_booking_id
                    ) AS paid"
                ),
                DB::raw("CONCAT(tsd.date, ' ', tst.time_from) AS tour_d_t"),
                DB::raw(
                    "GROUP_CONCAT(
                    CONCAT(LEFT(tpr.rate_for, 1), ':', tbt.tourists)
                    ORDER BY age_from DESC, age_to DESC
                    SEPARATOR ' | '
                    ) AS t_groups"
                )
            ]);
            //$response = DB::select(DB::raw($bookingsSql));

            if (!empty($response) && !empty($response)) {
                $result = array(
                    'search'            => $request->search,
                    'tour_operator_id'  => $request->tour_operator_id,
                    'bookings'          => $response,
                );
                return $result;
            }
            throw new Exception('Search details not available.');
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Update checkin status of booking which is created
     *
     * @param object $request
     * @return object
     */
    public function updateCheckInStatus(object $request)
    {
        try {
            DB::beginTransaction();

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $qry = DB::table('tour_bookings_transact AS tbt')
                ->join('tour_bookings_master AS tbm', 'tbm.id', '=', 'tbt.tour_booking_id')
                ->where('tbt.tour_booking_id', $request->booking_id);

            if (auth()->user()->tour_operator_id) {
                $qry = $qry->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                    ->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            if ($request->has('tpr_id')) // set status for a particular age/rate groups
                $qry = $qry->where('tbt.tour_package_rate_id', $request->tpr_id);

            $bookingT = $qry->get(['tbm.status', 'tbt.id', 'tbt.tourists', 'tbt.check_in_status','tbt.tour_package_rate_id']);
            $updated = 0;
            $mysqlDT = mysqlDT();

            if (count($bookingT) && $bookingT[0]->status == 'Cancelled') {
                throw new Exception('The booking has been cancelled, check-in status can not be updated.');
            }
            if($request->has('all_same_checked_in') && $request->input('all_same_checked_in')=='0'){
                $oldCheckInStatus = $request->old_check_in_status;
                if ($userType == "Admin") {
                    $user = $this->admin::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = $this->staff::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                }
                $historyArray = array();
                $updatedCheckInCount = 0;
                for ($a = 0, $ac = count($bookingT); $a < $ac; $a++) {
                    if ($bookingT[$a]->check_in_status == '')
                        $statusInfo = [];
                    else
                        $statusInfo = json_decode($bookingT[$a]->check_in_status);

                    $rateGrp = DB::table('tour_package_rates AS tpr')
                        ->join('tour_bookings_transact AS tst', 'tst.tour_package_rate_id', '=', 'tpr.id')
                        ->where('tst.tour_booking_id', $request->booking_id)
                        ->where('tpr.id', $bookingT[$a]->tour_package_rate_id)
                        ->first(['tpr.rate_for', 'tst.tourists']);
                    if (!$rateGrp) {
                        DB::rollBack();
                        throw new Exception('Unable to set check-in status.');
                    }

                    for ($b = 0; $b < $bookingT[$a]->tourists; $b++) {
                        if (!isset($statusInfo[$b])) {
                            $statusInfo[$b] = new \stdClass;
                            $statusInfo[$b]->status = 'N';
                        }

                        if ($request->check_in_status=='1') {
                            if ($statusInfo[$b]->status == $oldCheckInStatus || $statusInfo[$b]->status == '' || !isset($statusInfo[$b])) {
                                $statusInfo[$b]->status = '1';
                                $statusInfo[$b]->dateTime = $mysqlDT;
                                $historyArray[$a][$b]['status'] = "1st Check In";
                                $historyArray[$a][$b]['who'] = "{$rateGrp->rate_for} #" . ($b + 1);
                                $historyArray[$a][$b]['created_by_with_name'] = $created_by_with_name;
                                $updatedCheckInCount++;
                            }
                        } else if($request->check_in_status=='2') {
                            if ($statusInfo[$b]->status == $oldCheckInStatus) {
                                $statusInfo[$b]->status = '2';
                                $statusInfo[$b]->dateTime = $mysqlDT;
                                $historyArray[$a][$b]['status'] = "2nd Check In";
                                $historyArray[$a][$b]['who'] = "{$rateGrp->rate_for} #" . ($b + 1);
                                $historyArray[$a][$b]['created_by_with_name'] = $created_by_with_name;
                                $updatedCheckInCount++;
                            }
                        }else if($request->check_in_status=='N'){
                            if ($statusInfo[$b]->status == $oldCheckInStatus) {
                                $statusInfo[$b]->status = 'N';
                                $statusInfo[$b]->dateTime = $mysqlDT;
                                $historyArray[$a][$b]['status'] = "Not Checked In";
                                $historyArray[$a][$b]['who'] = "{$rateGrp->rate_for} #" . ($b + 1);
                                $historyArray[$a][$b]['created_by_with_name'] = $created_by_with_name;
                                $updatedCheckInCount++;
                            }
                        }else if($request->check_in_status=='A'){
                            if ($statusInfo[$b]->status == $oldCheckInStatus) {
                                $statusInfo[$b]->status = 'A';
                                $statusInfo[$b]->dateTime = $mysqlDT;
                                $historyArray[$a][$b]['status'] = "Absent";
                                $historyArray[$a][$b]['who'] = "{$rateGrp->rate_for} #" . ($b + 1);
                                $historyArray[$a][$b]['created_by_with_name'] = $created_by_with_name;
                                $updatedCheckInCount++;
                            }
                        }

                    }

                    $updated = $updated + DB::table('tour_bookings_transact')
                            ->where('id', $bookingT[$a]->id)
                            ->update(['check_in_status' => json_encode($statusInfo)]);

                }
                if($updated){
                    if(!empty($historyArray)){

                        foreach ($historyArray as $key=>$historArr){
                            foreach ($historArr as $k=>$v){
                                $dateTime = mysqlDT();
                                DB::table('tour_booking_history')
                                    ->insert([
                                        'tour_booking_id' => $request->booking_id,
                                        'operation' => 'Check-in',
                                        'details' => json_encode($v),
                                        'operation_at' => $dateTime,
                                        'operation_by' => auth()->id(),
                                        'operation_by_user_type' => $userType,
                                        'updated_at' => Carbon::now(),
                                        'created_at' => Carbon::now(),
                                    ]);
                            }
                        }
                    }
                }
                DB::commit();
                $masterCheck = DB::table('tour_bookings_master AS tbm')
                    ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                    ->where('tbm.id', $request->booking_id)
                    ->first(['tbm.status', 'c.is_affiliate']);

                $transaction = Transaction::where('tour_booking_id', $request->booking_id)->first();

                return [
                    'whoCheckedIn' => $updatedCheckInCount,
                    'checkInDateTime' => $dateTime,
                    'check_in_status' => '',
                    'status' => $transaction,
                    'is_affiliate' => $masterCheck->is_affiliate,
                ];
            }else {

                for ($a = 0, $ac = count($bookingT); $a < $ac; $a++) {
                    if ($bookingT[$a]->check_in_status == '')
                        $statusInfo = [];
                    else
                        $statusInfo = json_decode($bookingT[$a]->check_in_status);

                    for ($b = 0; $b < $bookingT[$a]->tourists; $b++) {
                        if (!isset($statusInfo[$b])) {
                            $statusInfo[$b] = new \stdClass;
                            $statusInfo[$b]->status = 'N';
                        }

                        if ($request->has('index')) {
                            if ($request->index == $b) {
                                $statusInfo[$b]->status = $request->check_in_status;
                                $statusInfo[$b]->dateTime = $mysqlDT;
                                break;
                            }
                        } else {
                            $statusInfo[$b]->status = $request->check_in_status;
                            $statusInfo[$b]->dateTime = $mysqlDT;
                        }
                    }

                    $updated = $updated + DB::table('tour_bookings_transact')
                            ->where('id', $bookingT[$a]->id)
                            ->update(['check_in_status' => json_encode($statusInfo)]);
                }

                if ($updated) {
                    $checkInStatuses = ['N' => 'Not Checked In', '1' => '1st Check In', '2' => '2nd Check In', 'A' => 'Absent'];

                    if ($request->has('tpr_id')) {
                        $rateGrp = DB::table('tour_package_rates AS tpr')
                            ->join('tour_bookings_transact AS tst', 'tst.tour_package_rate_id', '=', 'tpr.id')
                            ->where('tst.tour_booking_id', $request->booking_id)
                            ->where('tpr.id', $request->tpr_id)
                            ->first(['tpr.rate_for', 'tst.tourists']);

                        if (!$rateGrp) {
                            DB::rollBack();
                            throw new Exception('Unable to set check-in status.');
                        }

                        $whoCheckedIn = "{$rateGrp->rate_for} #" . ($request->index + 1);
                    } else
                        $whoCheckedIn = 'All';

                    $dateTime = mysqlDT();
                    if ($userType == "Admin") {
                        $user = $this->admin::find(auth()->id());
                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                    } elseif ($userType == "Tour Operator") {
                        $user = $this->staff::find(auth()->id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    }

                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $request->booking_id,
                            'operation' => 'Check-in',
                            'details' => json_encode([
                                'status' => $checkInStatuses[$request->check_in_status],
                                'who' => $whoCheckedIn,
                                'created_by_with_name' => $created_by_with_name
                            ]),
                            'operation_at' => $dateTime,
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();

                    $masterCheck = DB::table('tour_bookings_master AS tbm')
                        ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                        ->where('tbm.id', $request->booking_id)
                        ->first(['tbm.status', 'c.is_affiliate']);

                    $transaction = Transaction::select('id','tour_booking_id','amount','fee_amount','transaction_number','via','payment_type','status')->where	('tour_booking_id', $request->booking_id)->first();

                    return [
                        'whoCheckedIn' => $whoCheckedIn,
                        'checkInDateTime' => $dateTime,
                        'check_in_status' => $checkInStatuses[$request->check_in_status],
                        'status' => $transaction,
                        'is_affiliate' => $masterCheck->is_affiliate,
                    ];
                } else {
                    throw new Exception('Nothing has been updated or selected check-in status may already be set.');
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Tip payment of booking which is created
     *
     * @param object $request
     * @return object
     */
    public function tipPayment(object $request)
    {
        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $tourBooking = DB::table('tour_bookings_master AS tbm')
                ->join('customers as c', 'c.id', '=', 'tbm.customer_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('tour_operator_staff as tos', 'tos.tour_operator_id', '=', 't_o.id')
                ->join('tour_slot_times as st', 'st.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates as sd', 'sd.id', '=', 'st.tour_slot_id')
                ->where('tbm.id', '=', $request->bookingID)
                ->select('tbm.id', 'tbm.name as tbmName', 'tbm.email as tbmEmail', 'tbm.phone_number as tbmPhoneNumber',
                    'tp.id as tour_package_id', 'tp.name as package_name', 'tp.important_notes', 'tos.id as staff_id',
                    'c.name as Name', 'sd.date as Date', 'st.time_from as StartTime', 'c.phone_number as ContactNumber',
                    'c.email as Email', 'tbm.tourists', 't_o.id as tour_operator_id', 't_o.name as tour_operator_name',
                    't_o.phone_number AS tour_op_phone', 't_o.email AS tour_op_email', 't_o.logo AS tour_op_logo',
                    't_o.cancellation_policy', 't_o.website AS tour_op_website', 'tp.stripe_destination_id',
                    'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey')
                ->first();

            if($request->has('api') && $request->api=='1'){
                $paymentIntentId= ($request->has('paymentintentid') && $request->paymentintentid!='')?$request->paymentintentid:'';

                $stripe = new StripeClient(
                    env('STRIPE_API_SECRET_KEY')
                );

                $paymentIntent =  $stripe->paymentIntents->retrieve(
                    $paymentIntentId,
                    []
                );

                $stripe->paymentIntents->update(
                    $paymentIntentId,
                    [
                        'description' => 'Booking Id: ' . $tourBooking->id .
                            "\nTour Package: " . $tourBooking->package_name .
                            "\nTour Date: " . $tourBooking->Date .
                            "\nTour Time: " . $tourBooking->StartTime .
                            "\nTourists: " . $tourBooking->tourists .
                            "\nEmail: " . $tourBooking->Email,
                        'metadata' => [
                            'Booking Id' => $tourBooking->id,
                            'Tour Package' => $tourBooking->package_name,
                            'Tour Date' => $tourBooking->Date,
                            'Tour Time' => $tourBooking->StartTime,
                            'Tourists' => $tourBooking->tourists,
                            'Customer Name' => $tourBooking->Name,
                            'Customer Phone' => $tourBooking->ContactNumber,
                            'Email' => $tourBooking->Email,
                        ]
                    ], ['idempotency_key' => $tourBooking->id . carbon::now()]
                );

                $crd = '';
                if (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'])) {
                    $crd = $paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'];
                } elseif (isset($paymentIntent->charges->data[0]->payment_method_details->card->last4)) {
                    $crd = $paymentIntent->charges->data[0]->payment_method_details->card->last4;
                }

                $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card_present->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card_present->brand : '';

                if($crd == '') {
                    throw new Exception('Something went wrong with Stripe payment.Please try again.');
                }

                $data = [
                    'amount' => $request->amount
                ];
            } else {
                $month = substr($request->expiration, 0, 2);
                $yearSet = substr($request->expiration, 3, 2);
                $y = 20;
                $year = $y . '' . $yearSet;
                $data = [
                    'amount' => $request->amount,
                    'notes' => $request->notes,
                    'booking_id' => $tourBooking->id,
                    'tour_operator_staff_id' => $request->staffname,
                    'tour_package' => $tourBooking->package_name,
                    'month' => $month,
                    'year' => $year,
                    'cardnumber' => $request->cardnumber,
                    'cvc' => $request->cvc
                ];

                Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));

                $method = PaymentMethod::create([
                    'type' => 'card',
                    'card' => [
                        'number' => $data['cardnumber'],
                        'exp_month' => $data['month'],
                        'exp_year' => $data['year'],
                        'cvc' => $data['cvc'],
                    ],
                ]);

                $destination_amount = $data['amount'];
                $destination_amount = round($destination_amount, 2);

                $paymentIntent = PaymentIntent::create([
                    'payment_method_types' => ['card'],
                    'payment_method' => $method->id,
                    'currency' => 'usd',
                    'amount' => $data['amount'] * 100,
                    'description' => 'Booking Id: ' . $request->bookingID .
                        "\nTour Package: " . $tourBooking->tour_operator_name .
                        "\nTour Date: " . $tourBooking->Date .
                        "\nTour Time: " . $tourBooking->StartTime .
                        "\nTourists: " . $tourBooking->tourists .
                        "\nEmail: " . $tourBooking->Email,
                    'metadata' => [
                        'Booking Id' => $request->bookingID,
                        'Tour Package' => $tourBooking->tour_operator_name,
                        'Tour Date' => $tourBooking->Date,
                        'Tour Time' => $tourBooking->StartTime,
                        'Tourists' => $tourBooking->tourists,
                        'Customer Name' => $tourBooking->Name,
                        'Customer Phone' => $tourBooking->ContactNumber,
                        'Email' => $tourBooking->Email,
                    ],
                    'confirm' => true,
                    'transfer_data' => [
                        'amount' => $destination_amount * 100,
                        'destination' => $tourBooking->stripe_destination_id,
                    ],
                ]);

                $paymentIntentId = $paymentIntent->id;
                $crd = substr($data['cardnumber'], -4); //substr($data['cardnumber'], 12, 16);
                $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
            }
            $date = Carbon::now();
            DB::table('tour_booking_tips')->insert([
                'tour_booking_id' => $tourBooking->id,
                'tour_operator_staff_id' => $request->staffname,
                'amount' => $request->amount,
                'notes' => $request->notes,
                'date' => $date,
                'transaction_number' => $paymentIntentId,
                'status' => 'Completed',
                'created_by' => auth()->id(),
                'created_by_user_type' => $userType,
            ]);

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            } else {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            DB::table('tour_booking_history')->insert([
                'tour_booking_id' => $tourBooking->id,
                'operation' => 'Tip',
                'details' => json_encode(['amount' => $data['amount'], 'payment_type' => 'Card', 'card_number' => $crd,'card_type'=>$cardtype, 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                'operation_by' => auth()->id(),
                'operation_by_user_type' => $userType,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ]);

            $bookingId = $tourBooking->id;

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $bookingId)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $touros = DB::table('tour_operator_staff as tos')
                ->where('tos.id', '=', $request->staffname)
                ->select('tos.id', 'tos.Email', 'tos.phone_number', DB::raw("CONCAT(tos.first_name,' ',tos.last_name) as tosName"))
                ->first();

            $arEmailData = [
                'bookingId' => $bookingId,
                'tourOperatorId' => $tourBooking->tour_operator_id,
                'tourOperator' => $tourBooking->tour_operator_name,
                'tourOpPhone' => $tourBooking->tour_op_phone,
                'tourOpEmail' => $tourBooking->tour_op_email,
                'tourOpWebsite' => $tourBooking->tour_op_website,
                'tourPackage' => $tourBooking->package_name,
                'tourDateTime' => $tourBooking->Date,
                'tourtime' => $tourBooking->StartTime,
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($bookingId)]),
                'custName' => $tourBooking->tbmName,
                'custEmail' => $tourBooking->tbmEmail,
                'custPhone' => $tourBooking->tbmPhoneNumber,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($bookingId)]),
                'importantNotes' => $tourBooking->important_notes,
                'cancellationPolicy' => $tourBooking->cancellation_policy,
                'numberoftourist' => $tourBooking->tourists,
                'tourListwithSeat' => $tourListwithSeat,/**/
                'guideName' => $touros->tosName,
                'custHead' => "Customer",
                'guideEmail' => $touros->Email,
                'custPayAmount' => $request->amount,
                'date' => Carbon::now()->format('Y-m-d'),
                'packageEmail' => $tourBooking->packageEmail,
                'packageFromEmail' => $tourBooking->packageFromEmail,
                'packageApiKey' => $tourBooking->packageApiKey,
            ];

            if (!empty($tourBooking->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $tourBooking->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $tourBooking->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $tourBooking->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $tourBooking->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $tourBooking->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            if ($request->has('send_tip_mail')) {
                if ($request->send_tip_mail != null && $request->send_tip_mail == "Yes") {
                    // TODO
                    $booking = $this->tourBooking::find($bookingId);
                    $booking->notify(new TipForCustomer($arEmailData, 'tip_for_customer'));
                    $booking->notify(new TipForGuide($arEmailData, 'tip_for_guide'));
                }
            }

            DB::commit();

            if (isset($request->api) && $request->api == 1) {
                return response()->json([
                    'bookingId' => $tourBooking->id,
                ]);
            } else {
                return response()->json([
                    'bookingId' => $tourBooking->id,
                ]);
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Save note for booking
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function saveNote(object $request, int $id)
    {
        try {
            if (empty($request->all())) {
                throw new Exception("Atleast select any staff user or add note for this booking.");
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            if ($userType == 'Admin')
                $booking = TourBooking::findOrFail($id);
            else {
                $booking = TourBooking::join('tour_packages AS tp', 'tp.id', '=', 'tour_bookings_master.tour_package_id')
                    ->where('tp.tour_operator_id', auth()->user()->tour_operator_id)
                    ->select('tour_bookings_master.*')
                    ->findOrFail($id);
            }

            $bookerInfo = [];

            if ($request->booked_by_staff == '' && $booking->created_by_user_type == 'Tour Operator') {
                if ($userType == 'Tour Operator') {
                    throw new Exception("Please select a booked by person's name or leave it as it was.");
                }
                $bookerInfo = [
                    'newId' => auth()->id(),
                    'newUserType' => 'Admin',
                ];
            }

            if ($request->booked_by_staff != '' && (
                    ($request->booked_by_staff != $booking->created_by && $booking->created_by_user_type == 'Tour Operator') ||
                    $booking->created_by_user_type == 'Admin'
                )) {
                $bookerInfo = [
                    'newId' => $request->booked_by_staff,
                    'newUserType' => 'Tour Operator',
                ];
            }

            if ($request->note !== $booking->note || !empty($bookerInfo)) {
                DB::beginTransaction();

                if ($request->note !== $booking->note) {
                    $booking->note = $request->note;
                    $booking->note_datetime = mysqlDT();
                    $updatingNote = true;
                } else {
                    $updatingNote = false;
                }

                if (!empty($bookerInfo)) {
                    $bookerInfo['oldName'] = $bookerInfo['newName'] = '';
                    $bookerInfo['oldId'] = $booking->created_by;
                    $bookerInfo['oldUserType'] = $booking->created_by_user_type;
                }

                $booking->save();

                if ($updatingNote) {
                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $id,
                            'operation' => 'Note',
                            'details' => $request->note,
                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                }

                if (!empty($bookerInfo)) {
                    if ($bookerInfo['newUserType'] == 'Admin') {
                        $admin = DB::table('admins')
                            ->where('id', $bookerInfo['newId'])
                            ->first(['name']);

                        if ($admin)
                            $bookerInfo['newName'] = $admin->name;
                    } else {
                        $staffUser = DB::table('tour_operator_staff')
                            ->where('id', $bookerInfo['newId'])
                            ->first(['first_name', 'last_name']);

                        if ($staffUser)
                            $bookerInfo['newName'] = trim($staffUser->first_name . ' ' . $staffUser->last_name);
                    }

                    if ($bookerInfo['oldUserType'] == 'Admin') {
                        $admin = DB::table('admins')
                            ->where('id', $bookerInfo['oldId'])
                            ->first(['name']);

                        if ($admin)
                            $bookerInfo['oldName'] = $admin->name;
                    } else {
                        $staffUser = DB::table('tour_operator_staff')
                            ->where('id', $bookerInfo['oldId'])
                            ->first(['first_name', 'last_name']);

                        if ($staffUser)
                            $bookerInfo['oldName'] = trim($staffUser->first_name . ' ' . $staffUser->last_name);
                    }

                    if ($bookerInfo['newUserType'] == 'Tour Operator')
                        $bookerInfo['newUserType'] = $bookerInfo['newUserType'] . ' Staff';

                    if ($bookerInfo['oldUserType'] == 'Tour Operator')
                        $bookerInfo['oldUserType'] = $bookerInfo['oldUserType'] . ' Staff';

                    if ($userType == "Admin") {
                        $user = Admin::find(auth()->id());
                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                    } elseif ($userType == "Tour Operator") {
                        $user = Staff::find(auth()->id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    }

                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $id,
                            'operation' => 'Booked By',
                            'details' => json_encode([
                                'from' => $bookerInfo['oldName'] . ' (Id: ' . $bookerInfo['oldId'] . ', ' . $bookerInfo['oldUserType'] . ')',
                                'to' => $bookerInfo['newName'] . ' (Id: ' . $bookerInfo['newId'] . ', ' . $bookerInfo['newUserType'] . ')', 'created_by_with_name' => $created_by_with_name
                            ]),
                            'operation_at' => mysqlDT(),
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                }

                DB::commit();

                if (isset($request->api) && $request->api == 1) {
                    $noteArr = [
                        'bookingId' => $id,
                        'booked_by_staff' => $request->booked_by_staff,
                        'notes' => $request->note,
                        'operation' => 'Note',
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType
                    ];
                    return $noteArr;
                } else {
                    return response()->json(['updated' => 1]);
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Save comments for booking
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function saveComments(object $request, int $id)
    {
        try {
            if (empty($request->all())) {
                throw new Exception("Atleast add small comment for this booking.");
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            if ($userType == 'Admin')
                $booking = $this->tourBooking::findOrFail($id);
            else {
                $booking = $this->tourBooking::join('tour_packages AS tp', 'tp.id', '=', 'tour_bookings_master.tour_package_id')
                    ->where('tp.tour_operator_id', auth()->user()->tour_operator_id)
                    ->select('tour_bookings_master.*')
                    ->findOrFail($id);
            }

            if ($request->comments != $booking->comments) {
                DB::beginTransaction();

                $booking->comments = $request->comments;
                $booking->save();
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $id,
                        'operation' => 'Comments',
                        'details' => $request->comments,
                        'operation_at' => mysqlDT(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                DB::commit();
                $name = '';
                if ($userType == 'Tour Operator') {
                    $finduser = $this->staff::where('id', auth()->id())->first();
                    $name = $finduser->first_name . ' ' . $finduser->last_name;
                } else {
                    $finduser = $this->admin::where('id', auth()->id())->first();
                    $name = $finduser->name ?? '';
                }
                if (isset($request->api) && $request->api == 1) {
                    return [
                        'booking_id' => $id,
                        'comments' => $request->comments,
                        'operation_at' => mysqlDT(),
                    ];
                } else {
                    return response()->json(['operation_at' => mysqlDT(),'name'=>$name,'user_type'=>$userType]);
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Save reference of booking
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function saveReferenceBooking(object $request, int $id)
    {
        try {
            if (empty($request->all())) {
                throw new Exception("Add reference number for this booking.");
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            if ($userType == 'Admin') {
                $booking = $this->tourBooking::findOrFail($id);
            } else {
                $booking = $this->tourBooking::join('tour_packages AS tp', 'tp.id', '=', 'tour_bookings_master.tour_package_id')
                    ->where('tp.tour_operator_id', auth()->user()->tour_operator_id)
                    ->select('tour_bookings_master.*')
                    ->findOrFail($id);
            }

            if ($request->reference_booking != $booking->reference_booking) {
                DB::beginTransaction();

                $booking->reference_booking = $request->reference_booking;
                $booking->save();

                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $id,
                        'operation' => 'Reference Booking',
                        'details' => $request->reference_booking,
                        'operation_at' => mysqlDT(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                DB::commit();

                $name = '';
                if ($userType == 'Tour Operator') {
                    $finduser = $this->staff::where('id', auth()->id())->first();
                    $name = $finduser->first_name . ' ' . $finduser->last_name;
                } else {
                    $finduser = $this->admin::where('id', auth()->id())->first();
                    $name = $finduser->name ?? '';
                }

                if (isset($request->api) && $request->api == 1) {
                    return [
                        'booking_id' => $id,
                        'reference_booking' => $request->reference_booking,
                        'operation_at' => mysqlDT(),
                    ];
                } else {
                    return response()->json([
                        'operation_at' => mysqlDT(),
                        'name' => $name,
                        'user_type' => $userType
                    ]);
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Send email for booking
     *
     * @param object $request
     * @param int $id
     * @return object
     */
    public function sendEmail(object $request, int $bookingId)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $qry = DB::table('tour_bookings_master AS tbm')
                ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'c.tour_operator_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->join('countries AS cn', 'cn.id', '=', 'st.country_id')
                ->where('tbm.id', $bookingId)
                ->select([
                    't_o.name AS tour_operator', 't_o.email AS t_o_email', 't_o.phone_number AS t_o_phone',
                    't_o.address AS t_o_address', 't_o.postal_code AS t_o_postal_code', 't_o.logo AS t_o_logo',
                    'ct.name AS t_o_city', 'st.name AS t_o_state', 'cn.name AS t_o_country',
                    'tbm.name AS customer', 'tbm.email AS cust_email', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                ]);

            if ($userType == 'Tour Operator')
                $qry = $qry->where('c.tour_operator_id', auth()->user()->tour_operator_id);

            $info = $qry->first();

            if (!$info) {
                throw new Exception("Unable to get the data of tour booking or customer.");
            }

            $arEmailData = [
                'emailContent' => $request->email_content,
                'tourOperator' => $info->tour_operator,
                'tourOpEmail' => $info->t_o_email,
                'tourOpPhone' => $info->t_o_phone,
                'tourOpAddress' => $info->t_o_address,
                'tourOpPostalCode' => $info->t_o_postal_code,
                'tourOpCity' => $info->t_o_city,
                'tourOpState' => $info->t_o_state,
                'tourOpCountry' => $info->t_o_country,
                'tourOpLogo' => $info->t_o_logo,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if ($info->t_o_email) {
                $arEmailData['replyTo'] = [
                    'name' => $info->tour_operator,
                    'email' => $info->t_o_email,
                ];
            }

            sendEmail(
                $request->email_subject,
                [
                    'name' => $info->customer,
                    'email' => $info->cust_email,
                ],
                'customer-booking-custom',
                $arEmailData
            );

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $bookingId,
                    'operation' => 'Email',
                    'details' => $request->email_subject,
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            if (isset($request->api) && $request->api == 1) {
                return [];
            } else {
                return response()->json([]);
            }
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Initialize the Calendar
     *
     * @param object $request
     * @return array
     */
    public function calendar(object $request): array
    {
        try {
            $vars = [
                'page_title' => 'Tour Bookings Calendar',
                'pageId' => 'calendar',
                'tourOperatorId' => 0,
                'currentdate'=>date('Y-m-d H:i:s')
            ];

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            if ($userType == 'Admin') {
                $tourOperators = $this->tourOperator::where('status', 'Active')
                    ->orderBy('id')
                    ->get(['id', 'name']);

                $vars['tourOperators'] = $tourOperators;

                if (Cookie::has('tourOpId'))
                    $vars['tourOperatorId'] = Cookie::get('tourOpId');
                elseif (count($tourOperators))
                    $vars['tourOperatorId'] = $tourOperators[0]->id;
            } else
                $vars['tourOperatorId'] = isset($request->tourOperatorId) ? $request->tourOperatorId : auth()->user()->tour_operator_id;

            $yearMonth = $request->year . $request->month;

            if (isset($request->api) && $request->api == 1) {
                $date = substr($yearMonth, 0, 4); // . '-' . substr($yearMonth, 4, 2) . -1;
                $mon = substr($yearMonth, 4, 2);
                if (strlen($mon) == 1) {
                    $month = '0' . $mon;
                } else {
                    $month = substr($yearMonth, 4, 2);
                }
                $date = $date . '-' . $month . '-01';
            } else {
                $date = date('Y-m-01');
            }

            $arYM = explode('-', $date);

            $tourOpId = $vars['tourOperatorId'];

            $dateSlotsSql = "select `tsd`.`id`, `tsd`.`date`, `tp`.`id` as `pkg_id`, `tp`.`name` as `pkg_name`, IFNULL((
				SELECT SUM(tbm.tourists)
				FROM tour_bookings_master AS tbm
				JOIN tour_slot_times AS tst on tst.id = tbm.tour_slot_id
				WHERE tst.tour_slot_id = tsd.id
					AND tbm.status != 'Cancelled'
				GROUP BY tst.tour_slot_id,
				tbm.tour_package_id
				), 0) AS tourists,
				SUM(tsd.seats) AS seats from `tour_slot_dates` as `tsd`
				inner join `tour_packages` as `tp` on `tp`.`id` = `tsd`.`tour_package_id`
				inner join `tour_slot_times` as `tst` on `tst`.`tour_slot_id` = `tsd`.`id`
				where tsd.date BETWEEN CAST('".$date."' AS DATE) and LAST_DAY(CAST('".$date."' AS DATE))
				and `tp`.`tour_operator_id` =  ".$tourOpId." and `tsd`.`status` = 'Active'
				and `tsd`.`deleted_by` = 0 and `tst`.`deleted_by` = 0
				group by `tst`.`tour_slot_id`, `tsd`.`tour_package_id`
				order by `tsd`.`date` asc, `tp`.`Order_by` asc";

            $dateSlots =  DB::select(DB::raw($dateSlotsSql));

            $result = $dateSlots;
            $vars['year'] = $arYM[0];
            $vars['month'] = $arYM[1];
            $vars['tourSlots'] = $result;
            $vars['date'] = $date;

            // Overbooked bookings total
            foreach ($vars['tourSlots'] as $key => $dateSlot) {
                $timeSlots = $this->getTimeSlotsByDate($dateSlot->date, $dateSlot->pkg_id);
                foreach ($timeSlots as $keys => $timeSlot) {
                    if ($timeSlot['date'] == $dateSlot->date) {
                        $dateSlot->over_booking = $timeSlot['over_booking'];
                    }
                }
            }

            $calData = array();
            if (!empty($vars['tourSlots'])) {
                foreach ($vars['tourSlots'] as $key => $resultVal) {
                    if (!array_key_exists('over_booking', (array) $resultVal)) {
                        $calData[$key]['id']            = $resultVal->id;
                        $calData[$key]['date']          = $resultVal->date;
                        $calData[$key]['pkg_id']        = $resultVal->pkg_id;
                        $calData[$key]['pkg_name']      = $resultVal->pkg_name;
                        $calData[$key]['tourists']      = $resultVal->tourists;
                        $calData[$key]['seats']         = $resultVal->seats;
                        $calData[$key]['over_booking']  = 0;
                        $calData[$key]['available_seats'] = $resultVal->seats - $resultVal->tourists;
                    } else {
                        $calData[$key]['id']            = $resultVal->id;
                        $calData[$key]['date']          = $resultVal->date;
                        $calData[$key]['pkg_id']        = $resultVal->pkg_id;
                        $calData[$key]['pkg_name']      = $resultVal->pkg_name;
                        $calData[$key]['tourists']      = $resultVal->tourists;
                        $calData[$key]['seats']         = $resultVal->seats;
                        $calData[$key]['over_booking']  = $resultVal->over_booking;
                        $calData[$key]['available_seats'] = ($resultVal->seats - $resultVal->tourists) + $resultVal->over_booking;
                    }
                }
            }

            $vars['tourSlots'] = $calData;

            if(!$request->is('api/*')) {
                $vars['affiliates'] = Customer::where('tour_operator_id', $vars['tourOperatorId'])
                    ->where('status', 'Active')
                    ->where('is_affiliate', true)
                    ->orderBy('name')
                    ->get(['id', 'name']);
            }

            $BookingTotal = 0;
            $RefundAmount = 0;
            $Cancelled = 0;


            $tourOpId = $vars['tourOperatorId'];

            //We need to change it to raw query and separated subqueries as it was very slow
            $bookings = DB::select(DB::raw("select `tbm`.`id`, `tbm`.`name` as `cust_name`, `c`.`is_affiliate`, `tp`.`name` as `pkg_name`, `tbm`.`id`, `tbm`.`email` as `cust_email`, `c`.`is_affiliate`, `tp`.`name` as `pkg_name`, `tbm`.`total`, `tbm`.`created_at` as `booking_d_t`, `t_op`.`timezone`, `tbm`.`status`,
      	 		CONCAT(tsd.date, ' ', tst.time_from) AS tour_d_t,
				(SELECT SUM(t.amount) FROM transactions AS t WHERE t.tour_booking_id = tbm.id AND t.status = 'Completed' GROUP BY t.tour_booking_id ) AS paid,
				(SELECT SUM(t.amount) FROM transactions AS t WHERE tour_booking_id = tbm.id AND t.status = 'Completed' AND payment_type='1') AS paid_split,
				(SELECT t.via FROM transactions AS t WHERE t.tour_booking_id = tbm.id AND t.status = 'Completed' GROUP BY t.tour_booking_id) AS payment_method
				from `tour_bookings_master` as `tbm`
				inner join `customers` as `c` on `c`.`id` = `tbm`.`customer_id`
				inner join `tour_packages` as `tp` on `tp`.`id` = `tbm`.`tour_package_id`
				inner join `tour_operators` as `t_op` on `t_op`.`id` = `tp`.`tour_operator_id`
				inner join `tour_slot_times` as `tst` on `tst`.`id` = `tbm`.`tour_slot_id`
				inner join `tour_slot_dates` as `tsd` on `tsd`.`id` = `tst`.`tour_slot_id` where `tp`.`tour_operator_id` = ".$tourOpId."
				order by `tbm`.`created_at` desc limit 50"));
            foreach ($bookings as $key => $bookingData) {
                $history = TourBookingHistory::where('tour_booking_id', $bookingData->id)->get(['id', 'tour_booking_id', 'details', 'operation']);

                $tGroups = DB::select(DB::raw(
                    "(SELECT GROUP_CONCAT( CONCAT(LEFT(tpr.rate_for, 1), ':', tbt.tourists,'^',tpr.rate_for) ORDER BY age_from DESC, age_to DESC SEPARATOR ' | ' ) AS t_groups FROM tour_bookings_transact as tbt inner join tour_package_rates as tpr on tpr.id=tbt.tour_package_rate_id WHERE tbt.tour_booking_id=  ".$bookingData->id.")"
                ));

                $tGroupsChanged = '';
                $rateGroupWithTourists = array();
                $groupsArray = array();
                if(isset($tGroups[0]->t_groups) && !empty($tGroups[0]->t_groups)){
                    $groupsArray = explode('|',$tGroups[0]->t_groups);
                    $cnt = count($groupsArray);
                    if(!empty($groupsArray)){
                        foreach ($groupsArray as $key1=>$value){

                            $rateGroupWithTourists = explode('^',$value);
                            if(!empty($rateGroupWithTourists)){
                                if(Str::contains($value, "Adults - Military Teachers") || Str::contains($value, "Children (Ages 8-12) - Military/Teachers") || Str::contains($value, "Children (Ages 4-7) - Military/Teachers")){
                                    if(Str::contains($value, "Adults - Military Teachers")){
                                        $tGroupsChanged .= "AM".substr($rateGroupWithTourists[0],2);
                                    }else if(Str::contains($value, "Children (Ages 8-12) - Military/Teachers") || Str::contains($value, "Children (Ages 4-7) - Military/Teachers")){
                                        $tGroupsChanged .= "CM".substr($rateGroupWithTourists[0],2);
                                    }
                                    if($key1!=$cnt-1){
                                        $tGroupsChanged .= " | ";
                                    }
                                }else{
                                    $tGroupsChanged .= $rateGroupWithTourists[0];
                                    if($key1!=$cnt-1){
                                        $tGroupsChanged .= " | ";
                                    }
                                }
                            }
                        }
                    }
                }
                $bookings[$key]->t_groups = (isset($tGroupsChanged))?$tGroupsChanged:'';
                $bookings[$key]->BookingTotal = 0;
                $bookings[$key]->RefundAmount = 0;
                $bookings[$key]->PaymentTotal = 0;
                $bookings[$key]->Cancelled = 0;
                if (count($history) == 0) {
                    $bookings[$key]->BookingTotal = $bookingData->total;
                }

                foreach ($history as $his => $historyData) {

                    $details = json_decode($historyData->details);
                    if ($historyData->operation == 'Update') {
                        $bookings[$key]->BookingTotal = $bookingData->total;
                    }

                    if ($historyData->operation == 'Payment') {
                        $BookingTotal = $details->amount;
                        $bookings[$key]->BookingTotal = round((float) $BookingTotal, 2);
                    }

                    if ($historyData->operation == 'Refund') {
                        if ($bookingData->id == $historyData->tour_booking_id) {
                            $RefundAmount += (float)$details->amount;
                            $bookings[$key]->RefundAmount = round($RefundAmount, 2);
                        }
                    }

                    if ($historyData->operation == 'Cancelled' && isset($details->refund_type) && $details->refund_type != "Don't Refund") {
                        $Cancelled += $details->amount;
                        $bookings[$key]->Cancelled = round($Cancelled, 2);
                    }
                }

                $bookings[$key]->RefundAmount = $bookings[$key]->RefundAmount + $bookings[$key]->Cancelled;
                if (count($history) == 0) {
                    $bookings[$key]->PaymentTotal = 0;
                } elseif ($BookingTotal == 0) {
                    $bookings[$key]->PaymentTotal = 0;
                } else {
                    $PaymentTotal = $bookings[$key]->BookingTotal - $bookings[$key]->RefundAmount;
                    $bookings[$key]->PaymentTotal = round($PaymentTotal, 2);
                }

                $RefundAmount = 0;
                $BookingTotal = 0;
                $Cancelled = 0;
                unset($bookings[$key]->Cancelled);
            }
            $vars['tourBookings'] = $bookings;
            if($request->is('api/*')) {
                $settings =  DB::table('settings')
                    ->select(['key','value'])->get();

                if (!empty($settings)) {
                    foreach($settings as $setting) {
                        if ($setting->key == 'API_VERSION') {
                            $vars['apiVersion'] = (string) $setting->value;
                        }

                        if ($setting->key == 'IS_NO_HIDE') {
                            $vars['isNoHide'] = ($setting->value == '1') ? true : false;
                        }
                    }
                }
            }

            if (count($vars['tourBookings']) != 0 || count($vars['affiliates']) != 0 || count($vars) != 0) {
                return $vars;
            }

            throw new Exception("Calendar details not available.");
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    public function getTimeSlotsByDate($date, $pkg_id)
    {
        try {
            $overbooked_seats = 0;
            $overbooked_seats_arr = array();
            $timeSlots = DB::select(DB::raw("select `tst`.`id`, `tst`.`time_from`, `tst`.`time_to`, `tsd`.`seats`, `tst`.`bookable_status`, IFNULL((SELECT SUM(tbm.tourists)
				FROM tour_bookings_master AS tbm
				WHERE tbm.tour_slot_id = tst.id
						AND tbm.status != 'Cancelled'
						AND tbm.tour_package_id = ".$pkg_id."
				GROUP BY tbm.tour_slot_id, tbm.tour_package_id), 0) AS tourists, IFNULL((SELECT SUM(tbm.tourists)
				FROM tour_bookings_master AS tbm
				JOIN customers AS c ON c.id = tbm.customer_id
				WHERE tbm.tour_slot_id = tst.id
						AND c.is_affiliate = 'Yes'
						AND tbm.status != 'Cancelled'
						AND tbm.tour_package_id = ".$pkg_id."
				GROUP BY tbm.tour_slot_id, tbm.tour_package_id), 0) AS afl_tourists from `tour_slot_dates` as `tsd`
				inner join `tour_slot_times` as `tst` on `tst`.`tour_slot_id` = `tsd`.`id` where `tsd`.`date` = '".$date."'
				and `tsd`.`status` ='Active' and `tst`.`deleted_by` = '0' and `tsd`.`tour_package_id` = ".$pkg_id.""));

            foreach ($timeSlots as $key => $timeSlot) {
                if ((int)$timeSlot->tourists > $timeSlot->seats) {
                    $overbooked_seats_arr[$key]['date'] = $date;
                    $overbooked_seats += ((int)$timeSlot->tourists - $timeSlot->seats);
                    $overbooked_seats_arr[$key]['over_booking'] = $overbooked_seats;
                }
            }
            $overbooked_seats_arr = array_values($overbooked_seats_arr);

            return $overbooked_seats_arr;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Edit booking (Delete and Edit)
     *
     * @param object $request
     * @return object
     */
    public function editBooking(object $request)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $check = $request->input('check', []);

            if (count($check) == 0) {
                if (isset($request->api) && $request->api == 1) {
                    throw new Exception('Your are not selected any check box in action option.');
                } else {
                    $path = env('APP_URL');
                    $currentURL = Route::currentRouteName();
                    if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                        return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'Your are not selected any check box in action option.']);
                    }

                    $currentURL = Route::currentRouteName();

                    if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                        return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'Your are not selected any check box in action option.']);
                    } else {
                        return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'Your are not selected any check box in action option.']);
                    }
                }
            }
            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->first();

            $array = array_count_values($check);
            $checkPermitfee = $request->input('permitF', []);
            if ($request->action_type == "update") {
                $paymentTransCountForBooking = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->count();
                if($request->payment_type=='Card' && $paymentTransCountForBooking>1 && $request->checked_transactions==''){
                    if (isset($request->api) && $request->api == 1) {
                        throw new Exception("You must need to select payment transaction to update.");
                    } else {
                        $path = env('APP_URL');
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                            return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to update permit fees.']);
                        }
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                            return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to update permit fees.']);
                        } else {
                            return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to update permit fees.']);
                        }
                    }
                }
                $refundpermitarray = array();
                $oldpermitfeesarray = array();
                $oldprocessingfees = array();
                $newprocessingfees = array();
                $tourbooking = $this->tourBooking::where('id', $request->bookingID)->first();
                $totalTouristtriedtoincreasepermitfeesarray = array();
                $totalTouristswithoutpermitfeesarray = array();

                foreach ($check as $key => $value) {
                    $btset = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $value)->first();
                    $customerCheck = $this->tourBooking::where('id', $request->bookingID)->first();
                    $customer = $this->customer::where('id', $customerCheck->customer_id)->first();
                    $oldpermitfeesarray[$value] = $btset->permit_fee;
                    if ($customer->is_affiliate === 'Yes' && $btset->additional_tax_percentage) {
                        $oldprocessingfees[$value] = ($btset->rate + $btset->additional_charge + $btset->permit_fee) * ($btset->additional_tax_percentage) / 100;
                    } else {
                        $oldprocessingfees[$value] = ($btset->rate + $btset->additional_charge + $btset->permit_fee) * ($btset->additional_tax_percentage + $btset->processing_charge_percentage) / 100;
                    }
                    $tourpackagerate = $this->tourPackageRate::withTrashed()->find($value);
                    if ($btset->refund_permitfee_status == '') {
                        $statusInfo = [];
                    } else {
                        $statusInfo = json_decode($btset->refund_permitfee_status);
                    }

                    $oldpermitfees = $btset->permit_fee * $btset->tourists;
                    $totalTouristswithoutpermitfees = 0;
                    $totalTouristtriedtoincreasepermitfees = 0;
                    for ($b = 0; $b < $btset->tourists; $b++) {
                        $totalold = $btset->total;
                        if (!isset($statusInfo[$b]) || $checkPermitfee[$tourpackagerate->rate_for . "." . $b] > 0) {
                            $statusInfo[$b] = new \stdClass;
                            $statusInfo[$b]->status = 'N';
                        }
                        if ($oldpermitfees > 0 && $checkPermitfee[$tourpackagerate->rate_for . "." . $b] == 0.00 && (((isset($statusInfo[$b]->status) && $statusInfo[$b]->status == 'N') || $btset->refund_permitfee_status == ''))) {
                            $totalTouristswithoutpermitfees++;
                            if ($customer->is_affiliate === 'Yes' && $btset->additional_tax_percentage) {
                                $oldrt = ($btset->rate + $btset->additional_charge + $btset->permit_fee) * ($btset->additional_tax_percentage) / 100;
                                $rt = ($btset->rate + $btset->additional_charge) * ($btset->additional_tax_percentage) / 100;
                            } else {
                                $oldrt = ($btset->rate + $btset->additional_charge + $btset->permit_fee) * ($btset->additional_tax_percentage + $btset->processing_charge_percentage) / 100;
                                $rt = ($btset->rate + $btset->additional_charge) * ($btset->additional_tax_percentage + $btset->processing_charge_percentage) / 100;
                            }
                            $refundpermitarray[$value] = $totalTouristswithoutpermitfees;
                            $newprocessingfees[$value] = number_format((float)($oldrt - $rt), 2);
                            $statusInfo[$b]->amount = $btset->permit_fee;
                            $statusInfo[$b]->processing_fees = ($request->bookingID<config('constants.old_bookings.booking_id'))?number_format((float)($oldrt - $rt), 2):0; // number_format((float)($oldrt - $rt), 2);
                            $statusInfo[$b]->status = 'Y';
                            $statusInfo[$b]->dateTime = mysqlDT();
                            if(isset($statusInfo[$b]->is_pax_added) && $statusInfo[$b]->is_pax_added==true){
                                unset($statusInfo[$b]->is_pax_added);
                            }
                        }
                        if ($oldpermitfees == 0 && $checkPermitfee[$tourpackagerate->rate_for . "." . $b] > 0) {
                            $totalTouristtriedtoincreasepermitfees++;
                        }
                    }

                    $totalTouristtriedtoincreasepermitfeesarray[$value] = $totalTouristtriedtoincreasepermitfees;
                    $totalTouristswithoutpermitfeesarray[$value] = $totalTouristswithoutpermitfees;

                    $subtotal = $total = $totalold;
                    $total = $subtotal;
                    $touroperator = $this->tourOperator::where('id', $request->tour_operator_id)->first();
                    if ($request->payment_type == 'Card' && $request->bookingID > 14000 && $customer->is_affiliate == 'No') {
                        $transaction = $this->transaction::where('tour_booking_id', $request->bookingID)->first();
                        $fee = ($touroperator->service_commission_percentage * $subtotal) / 100;
                        $total = $fee + $total;
                    }
                    $bt = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $value)->first();

                    if ($bt->permit_fee != $checkPermitfee[$key]) {
                        $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $value)
                            ->update([
                                'refund_permitfee_status' => json_encode($statusInfo)
                            ]);
                    }
                }


                if (array_sum($totalTouristtriedtoincreasepermitfeesarray) > 0 && array_sum($totalTouristswithoutpermitfeesarray) == 0) {
                    if (isset($request->api) && $request->api == 1) {
                        throw new Exception("Your are not allowed to increase permit fees.");
                    } else {
                        $path = env('APP_URL');
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                            return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Your are not allowed to increase permit fees.']);
                        }
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                            return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Your are not allowed to increase permit fees.']);
                        } else {
                            return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Your are not allowed to increase permit fees.']);
                        }
                    }
                }

                if (!empty($refundpermitarray)) {
                    $amountTotal = 0;
                    $amountTotalrefundpermitfees = 0;
                    $amountTotalrefundprocessingfees = 0;
                    $textfordeleted = '';
                    $i = 0;
                    foreach ($refundpermitarray as $key => $value) {
                        $btset = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)->first();
                        $tourpackagerate = $this->tourPackageRate::withTrashed()->find($key);

                        $textfordeleted .= ($i == 0) ? $tourpackagerate->rate_for . " person " . $value : "  , " . $tourpackagerate->rate_for . " person " . $value;
                        $totalrefundpermitfeesamount = 0;
                        $totalrefundprocessingfeesamount = 0;
                        $totalrefundpermitfeesamount = (isset($oldpermitfeesarray[$key])) ? $oldpermitfeesarray[$key] * $value : 0;
                        if(($request->bookingID<config('constants.old_bookings.booking_id'))) {
                            $totalrefundprocessingfeesamount = (isset($newprocessingfees[$key])) ? $newprocessingfees[$key] * $value : 0;
                        }
                        $amountTotalrefundprocessingfees += $totalrefundprocessingfeesamount;
                        $amountTotalrefundpermitfees += $oldpermitfeesarray[$key] * $value;
                        $bookingtotal = $btset->total - $totalrefundpermitfeesamount - $totalrefundprocessingfeesamount;

                        $permitfee = ($btset->tourists == $value) ? 0.00 : $btset->permit_fee;
                        $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)
                            ->update(
                                [
                                    'total' => number_format((float)$bookingtotal, 2),
                                    'permit_fee' => $permitfee,
                                ]
                            );
                        $i++;
                    }

                    $amountTotal = $tourbooking->total - $amountTotalrefundpermitfees - $amountTotalrefundprocessingfees;
                    $TS = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->first();

                    $permitFee = 0;
                    if (!empty($TS) && $TS->via == "Card") {
                        $permitFee = $tourbooking->service_commission;
                    }

                    $tourBooking = $this->tourBooking::where('id', $request->bookingID)->update(['total' => $amountTotal, 'service_commission' => $permitFee]);
                    $details = json_encode(['amount' => $amountTotal,  'permit_fees' => $amountTotalrefundpermitfees + $amountTotalrefundprocessingfees, 'payment_type' => $request->payment_type, 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK);

                    $TourBookingHistory = new TourBookingHistory;
                    $TourBookingHistory->tour_booking_id = $request->bookingID;
                    // $TourBookingHistory->tourists = $tourBooking->tourists;
                    $TourBookingHistory->operation = "Update";
                    $TourBookingHistory->details = $details;
                    $TourBookingHistory->operation_at = Carbon::now();
                    $TourBookingHistory->operation_by = auth()->id();
                    $TourBookingHistory->operation_by_user_type = $userType;
                    $TourBookingHistory->save();
                    if ($request->payment_type == 'Card' && $request->bookingID > 14000 && $customer->is_affiliate == 'No') {
                        if($paymentTransCountForBooking==1) {
                            $this->transaction::where('tour_booking_id', $request->bookingID)->first()->update(['amount' => $amountTotal, 'fee_amount' => $permitFee]);
                        }
                    } else {
                        $bookingDet = $this->transaction::where('tour_booking_id', $request->bookingID)->first();
                        if ($bookingDet) {
                            if($paymentTransCountForBooking==1) {
                                $this->transaction::where('tour_booking_id', $request->bookingID)->first()->update(['amount' => $amountTotal]);
                            }
                        }
                    }
                    if (!empty($TS)) {
                        $details = json_encode(['amount' => $amountTotalrefundpermitfees + $amountTotalrefundprocessingfees, 'refund_keep_us' => 0, 'payment_type' => $request->payment_type, 'deleted' => $textfordeleted, 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Permitfees Refund'], JSON_NUMERIC_CHECK);
                    } else {
                        $details = json_encode(['amount' => 0, 'refund_keep_us' => 0, 'payment_type' => '', 'deleted' => $textfordeleted, 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Permitfees Refund'], JSON_NUMERIC_CHECK);
                    }

                    $bookingInfo = $this->tourBooking::find($request->bookingID);

                    $TourBookingHistory = new TourBookingHistory;
                    $TourBookingHistory->tour_booking_id = $request->bookingID;
                    $TourBookingHistory->tourists = $bookingInfo->tourists;
                    $TourBookingHistory->operation = "Refund";
                    $TourBookingHistory->details = $details;
                    $TourBookingHistory->operation_at = Carbon::now();
                    $TourBookingHistory->operation_by = auth()->id();
                    $TourBookingHistory->operation_by_user_type = $userType;
                    $TourBookingHistory->save();

                    if (!empty($TS) && $request->payment_type == 'Card') {
                        if($request->has('checked_transactions') && $request->checked_transactions!=''){
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $request->checked_transactions,
                                'amount' => ($amountTotalrefundpermitfees + $amountTotalrefundprocessingfees) * 100,
                                'reverse_transfer' => true,
                            ]);
                            $TSDetails = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number',$request->checked_transactions)->where('status', 'Completed')->first(['id','amount']);

                            $TS_number = $request->checked_transactions;
                            $application_fee_id = $TSDetails->application_fee_id;
                        }else {
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $TS->transaction_number,
                                'amount' => ($amountTotalrefundpermitfees + $amountTotalrefundprocessingfees) * 100,
                                'reverse_transfer' => true,
                            ]);
                            $TS_number = $TS->transaction_number;
                            $application_fee_id = $TS->application_fee_id;
                        }
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $request->bookingID;
                        $Transaction->amount = $amountTotalrefundpermitfees + $amountTotalrefundprocessingfees;
                        $Transaction->application_fee_id = $application_fee_id;
                        $Transaction->transaction_number = $TS_number;
                        //$Transaction->fee_amount = $TS->fee_amount;
                        $Transaction->via = 'Card';
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $request->getClientIp();
                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    } elseif (!empty($TS) && $request->payment_type == 'Cash') {
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $request->bookingID;
                        $Transaction->amount = $amountTotalrefundpermitfees + $amountTotalrefundprocessingfees;
                        $Transaction->transaction_number = '';
                        $Transaction->fee_amount = '';
                        $Transaction->via = 'Cash';
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $request->getClientIp();
                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    }

                    // Permit fee refund queue job dispatch
                    PermitFeeRefundBookingService::run($tourbooking, $Transaction->id ?? null, $TourBookingHistory->id, $request->all(), $request->payment_type);
                    // dispatch(new PermitFeeRefundBooking($tourbooking, $Transaction->id ?? null, $TourBookingHistory->id, $request->all(), $request->payment_type));
                }

                if (!isset($request->api)) {
                    $path = env('APP_URL');
                    $currentURL = Route::currentRouteName();
                    if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                        return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                            ->with('message', 'Permit fee updated successfully.');
                    }

                    $currentURL = Route::currentRouteName();

                    if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                        return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->with('message', 'Permit fee updated successfully.');
                    } else {
                        return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->with('message', 'Permit fee updated successfully.');
                    }
                } else {
                    return [
                        'booking_id' => $request->bookingID,
                        'history' => isset($TourBookingHistory) ? $TourBookingHistory : null,
                        'message' => 'Permit fee updated successfully.'
                    ];
                }
            }

            $total = 0;
            $checkTotal_tourist = count($check);
            if ($request->total_tourist == $checkTotal_tourist) {
                if (isset($request->api) && $request->api == 1) {
                    throw new Exception("You can not delete all records. Please use cancel booking option.");
                } else {
                    $path = env('APP_URL');
                    $currentURL = Route::currentRouteName();
                    if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                        return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'You can not delete all records. Please use cancel booking option.']);
                    }
                    $currentURL = Route::currentRouteName();
                    if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                        return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'You can not delete all records. Please use cancel booking option.']);
                    } else {
                        return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                            ->withErrors(['errors' => 'You can not delete all records. Please use cancel booking option.']);
                    }
                }
            }
            $oldBookingID = config('constants.old_bookings.booking_id');
            if ($request->action_type == "delete") {

                $paymentTransCountForBooking = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->count();
                if($request->payment_type=='Card' && $request->refund_Type != "fareharbor" && $request->is_cash=='' && $paymentTransCountForBooking>1 && ($request->has('checked_transactions') && $request->checked_transactions=='' && $request->refund_Type != 'dont_refund')){
                    if (isset($request->api) && $request->api == 1) {
                        throw new Exception("You must need to select payment transaction to delete person.");
                    } else {
                        $path = env('APP_URL');
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                            return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to delete person.']);
                        }
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                            return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to delete person.']);
                        } else {
                            return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'You must need to select payment transaction to delete person.']);
                        }
                    }
                }

                $refundedpermitarray = array();

                $checkarrays = array();
                $checkinstatusarray = array();
                $refundedpermitfeearray = array();

                foreach ($check as $key => $value) {
                    $btset = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $value)->first();

                    $explode = explode('.',$key);

                    $checkarrays[$value][] = $explode[1];
                    $checkinstatusarray[$value] = $btset->check_in_status;
                    $refundedpermitfeearray[$value] = $btset->refund_permitfee_status;
                }
                $updatedCheckIns = array();
                $updatedRefundedPermitfees = array();

                if(!empty($checkarrays)){
                    foreach ($checkarrays as $k=>$checkarray){
                        if (isset($checkinstatusarray[$k]) && $checkinstatusarray[$k]!='')
                            $checkinstatusInfo = json_decode($checkinstatusarray[$k]);
                        else
                            $checkinstatusInfo = [];

                        if (isset($refundedpermitfeearray[$k]) && $refundedpermitfeearray[$k]!='')
                            $refundpermitfeesstatusInfo = json_decode($refundedpermitfeearray[$k]);
                        else
                            $refundpermitfeesstatusInfo = [];

                        foreach ($checkarray as $l=>$m){
                            if(isset($checkinstatusInfo[$m]) && !empty($checkinstatusInfo[$m])){
                                unset($checkinstatusInfo[$m]);
                            }
                            if(isset($refundpermitfeesstatusInfo[$m]) && !empty($refundpermitfeesstatusInfo[$m])){

                                if(isset($refundpermitfeesstatusInfo[$m]->status) && $refundpermitfeesstatusInfo[$m]->status=='Y' && isset($refundpermitfeesstatusInfo[$m]->amount) && $refundpermitfeesstatusInfo[$m]->amount > 0){
                                    $refundedpermitarray[$k][] = $m;
                                }

                                unset($refundpermitfeesstatusInfo[$m]);
                            }
                        }
                        $updatedCheckIns[$k] = $checkinstatusInfo;
                        $updatedRefundedPermitfees[$k] = $refundpermitfeesstatusInfo;
                    }
                }
                //Check validations with calculations for multiple payments using card
                if($request->payment_type=='Card' && $request->refund_Type != "fareharbor" && $request->is_cash=='' && $paymentTransCountForBooking>1 &&($request->has('checked_transactions') && $request->checked_transactions!='')) {
                    $refundTrans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number', $request->checked_transactions)->where('status', 'Refund')->get(['id', 'amount']);
                    $paymentTrans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number', $request->checked_transactions)->where('status', 'Completed')->first(['id', 'amount']);
                    $totalrefundedamount = 0;
                    foreach ($refundTrans as $ky => $val) {
                        $totalrefundedamount += $val->amount;
                    }
                    $amountCapturedStripe = 0;
                    $amountRefundedStripe = 0;
                    $stripe = new StripeClient(
                        env('STRIPE_API_SECRET_KEY')
                    );

                    $paymentIntent = $stripe->paymentIntents->retrieve(
                        $request->checked_transactions,
                        []
                    );
                    if(!empty($paymentIntent) && isset($paymentIntent->charges) && isset($paymentIntent->charges->data[0]) && !empty($paymentIntent->charges->data[0])){
                        $amountCapturedStripe = $paymentIntent->charges->data[0]->amount_captured;
                        $amountRefundedStripe =  ($paymentIntent->charges->data[0]->amount_refunded!='')?$paymentIntent->charges->data[0]->amount_refunded:0;
                    }

                    //Validation check if payment transactions fully refunded or not
                    if($paymentTrans->amount-$totalrefundedamount <= 0){
                        if (isset($request->api) && $request->api == 1) {
                            throw new Exception('Unable to refund booking. Selected transaction already fully refunded.Please select different one.');
                        } else {
                            $path = env('APP_URL');
                            $currentURL = Route::currentRouteName();
                            if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                                return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                    ->withErrors(['errors' => 'Unable to refund booking. Selected transaction already fully refunded.Please select different one.']);
                            }
                            $currentURL = Route::currentRouteName();
                            if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                                return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                    ->withErrors(['errors' => 'Unable to refund booking. Selected transaction already fully refunded.Please select different one.']);
                            } else {
                                return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                    ->withErrors(['errors' => 'Unable to refund booking. Selected transaction already fully refunded.Please select different one.']);
                            }
                        }
                    }
                    $totalamountforvalidation = 0;
                    foreach ($array as $key => $value) {
                        $TourOperator = $this->tourOperator::where('id', $request->tour_operator_id)->first();
                        $tourBooking = $this->tourBooking::where('id', $request->bookingID)->where('status', 'Booked')->first();
                        $customer = $this->customer::where('id', $tourBooking->customer_id)->first();

                        $rateCheck = $this->bookingTransact::where('tour_package_rate_id', $key)->where('tour_booking_id', $request->bookingID)->first();
                        if ($customer->is_affiliate == 'Yes') {
                            $service_commission_percentage = $TourOperator->affiliate_processing_percentage;
                        } else {
                            $service_commission_percentage = $TourOperator->service_commission_percentage;
                        }
                        if ($request->action_type == "delete" && $rateCheck->refund_permitfee_status != '' && isset($refundedpermitarray) && !empty($refundedpermitarray) && isset($refundedpermitarray[$key])) {
                            $touristrefundedpermitfeesalready = count($refundedpermitarray[$key]);
                            $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;

                            if ($request->payment_type === 'Card') {
                                $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                            } else {
                                $per = $rate * $rateCheck->additional_tax_percentage / 100;
                            }
                            $subtotal = ($rate + $per);
                            if ($request->payment_type === 'Card' && $rateCheck->processing_charge_percentage > 0) {
                                $subtotal = number_format((float)(($subtotal * $value) - (($touristrefundedpermitfeesalready * $rateCheck->permit_fee) + ($touristrefundedpermitfeesalready * (($rateCheck->permit_fee * $rateCheck->processing_charge_percentage) / 100)))), 2, '.', '');
                            } else {
                                $subtotal = number_format((float)(($subtotal * $value) - ($touristrefundedpermitfeesalready * $rateCheck->permit_fee)), 2, '.', '');
                            }
                        } else {
                            $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;
                            if ($request->payment_type === 'Card') {
                                $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                            } else {
                                $per = $rate * $rateCheck->additional_tax_percentage / 100;
                            }
                            $subtotal = ($rate + $per);
                            $subtotal = number_format((float)$subtotal * $value, 2, '.', '');
                        }
                        $totalamountforvalidation += $subtotal;
                        if($request->refund_Type == "partial_refund"){
                            if ($request->partial_Refund_value > $totalamountforvalidation) {
                                if (isset($request->api) && $request->api == 1) {
                                    throw new Exception('Unable to refund booking. Please enter amount less than total.');
                                } else {
                                    $path = env('APP_URL');
                                    $currentURL = Route::currentRouteName();
                                    if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                                        return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                            ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                            ->withInput();
                                    }
                                    if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                                        return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                            ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                            ->withInput();
                                    } else {
                                        return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                            ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                            ->withInput();
                                    }
                                }
                            }
                        }

                    }
                    if($request->refund_Type != "partial_refund" && $request->refund_Type != "dont_refund"  && $amountCapturedStripe>0){
                        $remainingAmountStripe = $amountCapturedStripe/100 - $amountRefundedStripe/100;
                        if($totalamountforvalidation > $remainingAmountStripe){
                            if (isset($request->api) && $request->api == 1) {
                                throw new Exception('Unable to refund booking. Refund amount is greator than unrefunded amount for selected transaction. Please select different transaction.');
                            } else {
                                $path = env('APP_URL');
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                                    return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Refund amount is greator than unrefunded amount for selected transaction. Please select different transaction.'])
                                        ->withInput();
                                }
                                if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                                    return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Refund amount is greator than unrefunded amount for selected transaction. Please select different transaction.'])
                                        ->withInput();
                                } else {
                                    return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Refund amount is greator than unrefunded amount for selected transaction. Please select different transaction.'])
                                        ->withInput();
                                }
                            }
                        }
                    }

                    //Validation check for 100% refund when deleting persons
                    if ($request->refund_Type == "100_refund" && ($totalamountforvalidation > 0)) {
                        $set_fee_amount = 0;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $set_fee_amount = ($totalamountforvalidation * 6) / 100;
                            }
                        } else {
                            $set_fee_amount = ($totalamountforvalidation * $service_commission_percentage) / 100;
                        }
                        $Refund100 = $totalamountforvalidation + $set_fee_amount;
                        $Refund100 = round($Refund100, 2);

                        $rounded = round($paymentTrans->amount - $totalrefundedamount - $Refund100,2);
                        if ($rounded < -0.05) {
                            if (isset($request->api) && $request->api == 1) {
                                throw new Exception('Unable to refund booking. Please select valid transaction or person to refund.');
                            } else {
                                $path = env('APP_URL');
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                                    return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                }
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                                    return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                } else {
                                    return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                }
                            }
                        }
                    }

                    //Validation check for full refund minus fees when deleting persons
                    if ($request->refund_Type == "full_refund" && ($totalamountforvalidation > 0)) {

                        $Refund100MinusFees = round($totalamountforvalidation, 2);

                        if (($paymentTrans->amount - $totalrefundedamount - $Refund100MinusFees) < -0.05) {
                            if (isset($request->api) && $request->api == 1) {
                                throw new Exception('Unable to refund booking. Please select valid transaction or person to refund.');
                            } else {
                                $path = env('APP_URL');
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                                    return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                }
                                $currentURL = Route::currentRouteName();
                                if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                                    return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                } else {
                                    return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                        ->withErrors(['errors' => 'Unable to refund booking. Please select valid transaction or person to refund.']);
                                }
                            }
                        }
                    }
                }

                $tourBooking = $this->tourBooking::where('id', $request->bookingID)->where('status', 'Booked')->first();
                $liability_waiver_signing = $tourBooking->liability_waiver_signing;
                $touristsNumbers = ($request->has('tourits_numbers'))?$request->input('tourits_numbers'):'';
                if($liability_waiver_signing!='' && $touristsNumbers!=''){
                    $liability_waiver_signing = json_decode($liability_waiver_signing);
                    if($touristsNumbers!=''){
                        $touristNumberArray = explode(",",$touristsNumbers);
                        if(!empty($touristNumberArray)){
                            foreach ($touristNumberArray as $k=>$value){
                                if(isset($liability_waiver_signing[$value])){
                                    unset($liability_waiver_signing[$value]);
                                }
                            }
                        }
                        $this->tourBooking::where('id', $request->bookingID)
                            ->update([
                                'liability_waiver_signing' => (!empty($liability_waiver_signing)) ? json_encode((array_values($liability_waiver_signing))) : ''
                            ]);
                    }
                }


                if(!empty($updatedCheckIns)){
                    foreach ($updatedCheckIns as $key=>$value){
                        if(!empty($value)){
                            $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)
                                ->update([
                                    'check_in_status' => (!empty($value)) ? json_encode((array_values($value))) : ''
                                ]);
                        }
                    }
                }
                if(!empty($updatedRefundedPermitfees)){
                    foreach ($updatedRefundedPermitfees as $key=>$value){
                        if(!empty($value)){
                            $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)
                                ->update([
                                    'refund_permitfee_status' => (!empty($value)) ? json_encode(array_values($value)) : '',
                                ]);
                        }
                    }
                }


            }
            $total = 0;
            foreach ($array as $keyl => $value) {
                $TourOperator = $this->tourOperator::where('id', $request->tour_operator_id)->first();
                $transaction = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->first();
                $tourBooking = $this->tourBooking::where('id', $request->bookingID)->where('status', 'Booked')->first();
                $customer = $this->customer::where('id', $tourBooking->customer_id)->first();
                $rateCheckname = $this->tourPackageRate::where('id', $keyl)->withTrashed()->first();
                $rateCheck = $this->bookingTransact::where('tour_package_rate_id', $keyl)->where('tour_booking_id', $request->bookingID)->first();
                if ($customer->is_affiliate == 'Yes') {
                    $service_commission_percentage = $TourOperator->affiliate_processing_percentage;
                } else {
                    $service_commission_percentage = $TourOperator->service_commission_percentage;
                }
                if ($request->action_type == "delete" && $rateCheck->refund_permitfee_status != '' && isset($refundedpermitarray) && !empty($refundedpermitarray) && isset($refundedpermitarray[$keyl])) {
                    $touristrefundedpermitfeesalready = count($refundedpermitarray[$keyl]);
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;

                    if ($request->payment_type === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    if ($request->payment_type === 'Card' && $rateCheck->processing_charge_percentage > 0) {
                        $subtotal = number_format((float)(($subtotal * $value) - (($touristrefundedpermitfeesalready * $rateCheck->permit_fee) + ($touristrefundedpermitfeesalready * (($rateCheck->permit_fee * $rateCheck->processing_charge_percentage) / 100)))), 2, '.', '');
                    } else {
                        $subtotal = number_format((float)(($subtotal * $value) - ($touristrefundedpermitfeesalready * $rateCheck->permit_fee)), 2, '.', '');
                    }
                } else {
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;
                    if ($request->payment_type === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    $subtotal = number_format((float)$subtotal * $value, 2, '.', '');
                }
                $total += $subtotal;
            }
            if ($request->refund_Type == "partial_refund") {
                if ($request->partial_Refund_value > $total) {
                    if (isset($request->api) && $request->api == 1) {
                        throw new Exception('Unable to refund booking. Please enter amount less than total.');
                    } else {
                        $path = env('APP_URL');
                        $currentURL = Route::currentRouteName();
                        if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                            return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                ->withInput();
                        }
                        if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                            return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                ->withInput();
                        } else {
                            return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                                ->withErrors(['errors' => 'Unable to refund booking. Please enter amount less than total.'])
                                ->withInput();
                        }
                    }
                }
            }
            $rateChecknameForPartialRefund = [];
            $tourBookingTotalForPayLater = 0;
            foreach ($array as $key => $value) {
                $TourOperator = $this->tourOperator::where('id', $request->tour_operator_id)->first();
                $transaction = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->first();
                $tourBooking = $this->tourBooking::where('id', $request->bookingID)->where('status', 'Booked')->first();
                $customer = $this->customer::where('id', $tourBooking->customer_id)->first();
                $rateCheckname = $this->tourPackageRate::where('id', $key)->withTrashed()->first();

                $rateChecknameForPartialRefund[] = $rateCheckname->rate_for . ' person ' . $value;

                $rateCheck = $this->bookingTransact::where('tour_package_rate_id', $key)->where('tour_booking_id', $request->bookingID)->first();
                if ($customer->is_affiliate == 'Yes') {
                    $service_commission_percentage = $TourOperator->affiliate_processing_percentage;
                } else {
                    $service_commission_percentage = $TourOperator->service_commission_percentage;
                }
                if ($request->action_type == "delete" && $rateCheck->refund_permitfee_status != '' && isset($refundedpermitarray) && !empty($refundedpermitarray) && isset($refundedpermitarray[$key])) {
                    $touristrefundedpermitfeesalready = count($refundedpermitarray[$key]);
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;

                    if ($request->payment_type === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    if ($request->payment_type === 'Card' && $rateCheck->processing_charge_percentage > 0) {
                        $subtotal = number_format((float)(($subtotal * $value) - (($touristrefundedpermitfeesalready * $rateCheck->permit_fee) + ($touristrefundedpermitfeesalready * (($rateCheck->permit_fee * $rateCheck->processing_charge_percentage) / 100)))), 2, '.', '');
                    } else {
                        $subtotal = number_format((float)(($subtotal * $value) - ($touristrefundedpermitfeesalready * $rateCheck->permit_fee)), 2, '.', '');
                    }
                } else {
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;
                    if ($request->payment_type === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    $subtotal = number_format((float)$subtotal * $value, 2, '.', '');
                }
                $total = $subtotal;

                $bt = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)->first();

                $set_total = $bt->total - $total;
                $set_tourist = $bt->tourists - $value;

                $total_fee_amount = 0;
                if (!empty($transaction) && $transaction->via == "Card") {
                    if ($request->bookingID < $oldBookingID) {
                        if ($customer->is_affiliate === "No") {
                            $total_fee_amount = ($set_total * 6) / 100;
                        }
                    } else {
                        $total_fee_amount = ($set_total * $service_commission_percentage) / 100;
                    }
                }
                $total_fee_amount = number_format((float)$total_fee_amount, 2, '.', '');

                $set_fee_amount = 0;
                if ($transaction) {
                    $set_fee_amount = $tourBooking->service_commission - (float)$total_fee_amount;
                    $set_fee_amount = number_format((float)$set_fee_amount, 2, '.', '');
                }
                if ($set_tourist == 0) {
                    $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)
                        ->update([
                            'total' => $set_total,
                            'tourists' => $set_tourist,
                            'check_in_status'=>'',
                            'refund_permitfee_status'=>''
                        ]);
                } else {
                    $this->bookingTransact::where('tour_booking_id', $request->bookingID)->where('tour_package_rate_id', $key)
                        ->update([
                            'total' => $set_total,
                            'tourists' => $set_tourist
                        ]);
                }


                if (!empty($transaction)) {
                    $tourBooking_set_total = $tourBooking->total - $total;
                    $tourBookingset_tourist = $tourBooking->tourists - $value;
                    if ($transaction->via == "Card" && $request->refund_Type != "fareharbor") {
                        $set_fee_amount = 0.00;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $set_fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $set_fee_amount = ($total * $service_commission_percentage) / 100;
                        }

                        $set_fee_amount = number_format((float)$set_fee_amount, 2, '.', '');
                        $set_fee_amount = (float)$set_fee_amount;
                        if ($request->refund_Type == "dont_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission;
                        }
                        if ($request->refund_Type == "100_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission - $set_fee_amount;
                        }
                        if ($request->refund_Type == "full_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission;
                        }
                        if ($request->refund_Type == "partial_refund") {
                            $service_com = $tourBooking->service_commission;
                        }
                        $tourBooking_set_total = number_format((float)$tourBooking_set_total, 2, '.', '');
                        if ($request->refund_Type != "partial_refund") {
                            $this->tourBooking::where('id', $request->bookingID)->update([
                                'total' => $tourBooking_set_total,
                                'tourists' => $tourBookingset_tourist,
                                'service_commission' => $service_com
                            ]);
                        }

                    } else {
                        if ($request->refund_Type != "partial_refund") {
                            $this->tourBooking::where('id', $request->bookingID)->update([
                                'total' => $tourBooking_set_total,
                                'tourists' => $tourBookingset_tourist
                            ]);
                        }
                    }
                } else {
                    $tourBooking = $this->tourBooking::where('id', $request->bookingID)->first();
                    $tourBooking_set_total = $tourBooking->total - $total;
                    $tourBookingTotalForPayLater += $total;
                    $tourBookingset_tourist = $tourBooking->tourists - $value;
                    if ($request->refund_Type != "partial_refund") {
                        $this->tourBooking::where('id', $request->bookingID)->update([
                            'total' => $tourBooking_set_total,
                            'booking_total'=>$tourBooking_set_total,
                            'tourists' => $tourBookingset_tourist
                        ]);
                    }
                }

                if ($request->payment_type == 'Card' && $request->refund_Type != "fareharbor" && ($request->is_cash=='')) {
                    if ($request->refund_Type == "full_refund" && ($total > 0)) {
                        if($request->has('checked_transactions') && $request->checked_transactions!=''){
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $request->checked_transactions,
                                'amount' => $total * 100,
                                'reverse_transfer' => true,
                            ]);
                        }else {
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $transaction->transaction_number,
                                'amount' => $total * 100,
                                'reverse_transfer' => true,
                            ]);
                        }
                    }
                    if ($request->refund_Type == "100_refund" && ($total > 0)) {
                        $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                        $set_fee_amount = 0.00;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $set_fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $set_fee_amount = ($total * $service_commission_percentage) / 100;
                        }
                        $Refund100 = $total + $set_fee_amount;
                        $Refund100 = round($Refund100, 2);
                        if($request->has('checked_transactions') && $request->checked_transactions!=''){
                            $refundTrans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number',$request->checked_transactions)->where('status', 'Refund')->get(['id','amount']);
                            $paymentTrans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number',$request->checked_transactions)->where('status', 'Completed')->first(['id','amount']);
                            $totalrefundedamount = 0;
                            foreach ($refundTrans as $ky => $val) {
                                $totalrefundedamount += $val->amount;
                            }

                            $rounded = round($paymentTrans->amount - $totalrefundedamount - $Refund100,2);

                            if ($rounded <= -0.01) {
                                $stripe->refunds->create([
                                    'payment_intent' => $request->checked_transactions,
                                    'amount' => ($Refund100 + $rounded) * 100,
                                    'reverse_transfer' => true,
                                ]);
                            } else {
                                $stripe->refunds->create([
                                    'payment_intent' => $request->checked_transactions,
                                    'amount' => $Refund100 * 100,
                                    'reverse_transfer' => true,
                                ]);
                            }
                        } else {
                            $stripe->refunds->create([
                                'payment_intent' => $transaction->transaction_number,
                                'amount' => $Refund100 * 100,
                                'reverse_transfer' => true,
                            ]);
                        }
                    }
                }

                $deletedPeople = $value;
                if ($request->refund_Type == "dont_refund") {
                    if ($request->payment_type == 'Card' && $request->refund_Type != "fareharbor") {
                        $fee_amount = 0;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $fee_amount = ($total * $service_commission_percentage) / 100;
                        }

                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = $total;
                    } else {
                        $refund_keep_us = $total;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $fee_amount = 0;
                    $refund_type = "Don't Refund";
                }
                if ($request->refund_Type == "partial_refund") {
                    if ($request->payment_type == 'Card' && $request->refund_Type != "fareharbor") {
                        $fee_amount = 0;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $fee_amount = ($total * $service_commission_percentage) / 100;
                        }

                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = $total - (float)$request->partial_Refund_value;
                        $refund_keep_us = number_format((float)$refund_keep_us, 2, '.', '');
                        $fee_amount = 0;
                    } else {
                        $refund_keep_us = $total - (float)$request->partial_Refund_value;
                        $refund_keep_us = number_format((float)$refund_keep_us, 2, '.', '');
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    if ($rateCheck->rate == 0) {
                        $request->partial_Refund_value = 0;
                    }
                    $total = number_format((float)$request->partial_Refund_value, 2, '.', '');
                    $refund_type = "Partial Refund";
                }
                if ($request->refund_Type == "full_refund") {
                    if ($request->payment_type == 'Card' && $request->refund_Type != "fareharbor") {
                        $fee_amount = 0;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $fee_amount = ($total * $service_commission_percentage) / 100;
                        }

                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $refund_keep_us = $fee_amount;
                        $fee_amount = 0;
                    } else {
                        $refund_keep_us = 0;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $refund_type = "Full Refund minus Fees";
                }
                if ($request->refund_Type == "fareharbor") {
                    $refund_keep_us = $total;
                    $refund_type = "Fareharbor";
                }
                if ($request->refund_Type == "100_refund") {
                    if ($request->payment_type == 'Card' && $request->refund_Type != "fareharbor") {
                        $fee_amount = 0;
                        if ($request->bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            $fee_amount = ($total * $service_commission_percentage) / 100;
                        }

                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = 0;
                    } else {
                        $total = $total;
                        $refund_keep_us = 0;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $refund_type = "100 % Refund";
                }
                if (isset($request->is_cash) && $request->is_cash!='') {
                    if ($request->is_cash=='Cash') {
                        $refundVia = 'Cash';
                    } elseif ($request->is_cash=='Wire Transfer') {
                        $refundVia = 'Check/Wire';
                    }
                } else {
                    $refundVia = $request->payment_type;
                }

                $details = json_encode([
                    'amount' => $total,
                    'refund_keep_us' => $refund_keep_us,
                    'payment_type' => $refundVia,
                    'deleted' => $rateCheckname->rate_for . ' person ' . $value,
                    'created_by_with_name' => $created_by_with_name,
                    'refund_type' => $refund_type
                ], JSON_NUMERIC_CHECK);

                $bookingInfo = $this->tourBooking::find($request->bookingID);
                if ($request->refund_Type != "partial_refund") {
                    $TourBookingHistory = new TourBookingHistory;
                    $TourBookingHistory->tour_booking_id = $request->bookingID;
                    $TourBookingHistory->tourists = $bookingInfo->tourists;
                    $TourBookingHistory->operation = "Refund";
                    $TourBookingHistory->details = $details;
                    $TourBookingHistory->operation_at = Carbon::now();
                    $TourBookingHistory->operation_by = auth()->id();
                    $TourBookingHistory->operation_by_user_type = $userType;
                    $TourBookingHistory->save();
                }

                if (!empty($transaction) && $request->payment_type == 'Card' && $request->refund_Type != "fareharbor" && $request->refund_Type != "partial_refund") {
                    if (isset($request->is_cash) && $request->is_cash!='') {
                        if($request->is_cash=='Cash') {
                            $refundVia = 'Cash';
                        }elseif($request->is_cash=='Wire Transfer'){
                            $refundVia = 'Check/Wire';
                        }
                    } else {
                        $refundVia = $request->payment_type;
                    }
                    $TS = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->first();
                    if ($request->has('checked_transactions') && $request->checked_transactions!=''){
                        $TS1 = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number', $request->checked_transactions)->where('status', 'Completed')->first();
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $request->bookingID;
                        if ($request->refund_Type == "dont_refund") {
                            $Transaction->amount = 0.00;
                        } else {
                            $Transaction->amount = $total;
                        }
                        $Transaction->application_fee_id = $TS1->application_fee_id;
                        $Transaction->transaction_number = $TS1->transaction_number;
                        if ($request->refund_Type == "dont_refund") {
                            $Transaction->fee_amount = 0.00;
                        } else {
                            $Transaction->fee_amount = round($total * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                        }
                        $Transaction->via = $refundVia;
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $request->getClientIp();
                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    } else {
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $request->bookingID;
                        if ($request->refund_Type == "dont_refund") {
                            $Transaction->amount = 0.00;
                        }else {
                            $Transaction->amount = $total;
                        }
                        $Transaction->application_fee_id = $TS->application_fee_id;
                        $Transaction->transaction_number = $TS->transaction_number;
                        if ($request->refund_Type == "dont_refund") {
                            $Transaction->fee_amount = 0.00;
                        } else {
                            $Transaction->fee_amount = round($total * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                        }
                        $Transaction->via = $refundVia;
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $request->getClientIp();
                        $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    }
                } elseif (!empty($transaction) && $request->payment_type == 'Cash') {
                    if (isset($request->is_cash) && $request->is_cash!='') {
                        if($request->is_cash=='Cash') {
                            $refundVia = 'Cash';
                        }elseif($request->is_cash=='Wire Transfer'){
                            $refundVia = 'Check/Wire';
                        }
                    }else{
                        $refundVia = $request->payment_type;
                    }
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $request->bookingID;
                    if ($request->refund_Type == "dont_refund") {
                        $Transaction->amount = 0.00;
                    }else {
                        $Transaction->amount = $total;
                    }
                    $Transaction->transaction_number = '';
                    $Transaction->fee_amount = '';
                    $Transaction->via = $refundVia;
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = '';
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $request->getClientIp();
                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                    $Transaction->created_by = auth()->id();
                    $Transaction->created_by_user_type = $userType;
                    $Transaction->save();
                } elseif (!empty($transaction) && $request->refund_Type == "fareharbor") {
                    if (isset($request->is_cash) && $request->is_cash!='') {
                        if($request->is_cash=='Cash') {
                            $refundVia = 'Cash';
                        }elseif($request->is_cash=='Wire Transfer'){
                            $refundVia = 'Check/Wire';
                        }
                    }else{
                        $refundVia = $request->payment_type;
                    }
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $request->bookingID;
                    if ($request->refund_Type == "dont_refund") {
                        $Transaction->amount = 0.00;
                    }else {
                        $Transaction->amount = $total;
                    }
                    $Transaction->transaction_number = '';
                    $Transaction->fee_amount = '';
                    $Transaction->via = $refundVia;
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = '';
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $request->getClientIp();
                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                    $Transaction->created_by = auth()->id();
                    $Transaction->created_by_user_type = $userType;
                    $Transaction->save();
                }

                // Below line is for to update tour booking history table
                if ($request->refund_Type != "partial_refund"){
                    $this->handleDeletePersonHistoryUpdateJobs($tourBooking, $TourBookingHistory->id, $request->all(), $refundVia, $Transaction->id ?? null);
                }
            }
            // Partial Refund edit booking

            if($request->refund_Type == "partial_refund") {

                $totalc = $this->tourBooking::where('id', $request->bookingID)->first();
                DB::beginTransaction();
                if ($request->has('checked_transactions') && $request->checked_transactions!=''){
                    $transac = $this->transaction::where('tour_booking_id', $request->bookingID)->where('transaction_number', $request->checked_transactions)->where('status', 'Completed')->first();
                }else {
                    $transac = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->first();
                }
                $transactionFirst = $this->transaction::where('tour_booking_id', $request->bookingID)->first();
                if($refundVia == "Card"){
                    $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                    $refund = $stripe->refunds->create([
                        'payment_intent' => $transac->transaction_number,
                        'amount' => $request->partial_Refund_value * 100,
                        'reverse_transfer' => true,
                    ]);
                }
                $btp = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->get();
                $tcount = 0;
                foreach ($btp as $value) {
                    $tcount += $value->tourists;
                }
                if(!empty($transactionFirst)){
                    $ptotal = $totalc->total - $request->partial_Refund_value;
                    $this->tourBooking::where('id', $request->bookingID)->update([
                        'total' => $ptotal,
                        'tourists' => $tcount,
                    ]);
                }else{
                    $this->tourBooking::where('id', $request->bookingID)->update([
                        'total' => $totalc->total - $tourBookingTotalForPayLater,
                        'booking_total'=> $totalc->booking_total - $tourBookingTotalForPayLater,
                        'tourists' => $tcount
                    ]);
                }
                if(!empty($transactionFirst)){
                    $partialValue = $request->partial_Refund_value;
                }else{
                    $partialValue = 0;
                    $refund_keep_us = 0;
                }
                $details = json_encode([
                    'amount' => $partialValue,
                    'refund_keep_us' => $refund_keep_us,
                    'payment_type' => $refundVia,
                    'deleted' => $rateChecknameForPartialRefund,
                    'created_by_with_name' => $created_by_with_name,
                    'refund_type' => $refund_type
                ], JSON_NUMERIC_CHECK);

                $bookingInfo = $this->tourBooking::find($request->bookingID);
                $TourBookingHistory = new TourBookingHistory;
                $TourBookingHistory->tour_booking_id = $request->bookingID;
                $TourBookingHistory->tourists = $bookingInfo->tourists;
                $TourBookingHistory->operation = "Refund";
                $TourBookingHistory->details = $details;
                $TourBookingHistory->operation_at = Carbon::now();
                $TourBookingHistory->operation_by = auth()->id();
                $TourBookingHistory->operation_by_user_type = $userType;
                $TourBookingHistory->save();
                if(!empty($transactionFirst)) {
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $request->bookingID;
                    $Transaction->amount = $request->partial_Refund_value;
                    $Transaction->transaction_number = ($refundVia == "Card") ? $transac->transaction_number : '';
                    $Transaction->fee_amount = '';
                    $Transaction->via = $refundVia;
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = '';
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $request->getClientIp();
                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                    $Transaction->created_by = auth()->id();
                    $Transaction->created_by_user_type = $userType;
                    $Transaction->save();
                    $this->handleDeletePersonHistoryUpdateJobs($tourBooking, $TourBookingHistory->id, $request->all(), $refundVia, $Transaction->id ?? null);
                }
                DB::commit();
            }


            // $array = array_count_values($check);
            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_bookings_master as bm', 'bm.tour_slot_id', '=', 'tst.id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('bm.id', $request->bookingID)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'bm.name As custname', 'bm.email As custemail',
                    'bm.phone_number As phone_number', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                            FROM tour_bookings_master AS tbm
                            WHERE tbm.tour_slot_id = tst.id
                                AND tbm.status != 'Cancelled'
                            GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            $info = $info->first();

            $booking = [
                'tour_d_t' => $info->date . ' ' . $info->time_from,
            ];

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $request->bookingID)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $totalTourists = 0;
            foreach ($tourListwithSeat as $value) {
                $totalTourists = $totalTourists + $value->tourists;
            }
            $deletedPeople = $this->tourBookingHistory::where('tour_booking_id', $request->bookingID)->where('operation', 'Refund')->get();
            $arEmailData = [
                'bookingId' => $request->bookingID,
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'custName' => $info->custname,
                'custEmail' => $info->custemail,
                'custPhone' => $info->phone_number ?? '',
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'deletedPeople' => $deletedPeople,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            // TODO
            $tourBooking->notify(new PeopleRemovedFromBooking($arEmailData, 'people_remove_from_booking'));

            if (isset($request->api) && $request->api == 1) {
                return [
                    'booking_id' => $request->bookingID,
                    'message' => 'Booking is updated and refund will be get soon.'
                ];
            } else {
                $path = env('APP_URL');
                $currentURL = Route::currentRouteName();
                if ($currentURL == "affiliate.booking.refund_and_booking_edit") {
                    return redirect()->to($path . '/affiliate/booking/' . $request->bookingID . '?modal=1')
                        ->with('message', 'Booking is updated and refund will be get soon.');
                }

                $currentURL = Route::currentRouteName();

                if ($currentURL == 'admin.tour-operator.booking.refund_and_booking_edit') {
                    return redirect()->to($path . '/admin/tour-operator/booking/' . $request->bookingID . '?modal=1')
                        ->with('message', 'Booking is updated and refund will be get soon.');
                } else {
                    return redirect()->to($path . '/tour-operator/booking/' . $request->bookingID . '?modal=1')
                        ->with('message', 'Booking is updated and refund will be get soon.');
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);

            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Add people in the existing booking
     *
     * @param object $request
     * @return array
     */
    public function addPeopleBooking($request)
    {
        DB::beginTransaction();
        try {
            $isValidationCheckForBookingAddPeople = $request->get('validation_check') === 'true';
            if (isset($request->api) && $request->api == 1) {
                if (isset($request->permit_fee[0])) {
                    $request->permit_fee = explode(",", $request->permit_fee[0]);
                }

                if (isset($request->rate_group[0])) {
                    $request->rate_group = explode(",", $request->rate_group[0]);
                }

                if (isset($request->tourists[0])) {
                    $request->tourists = explode(",", $request->tourists[0]);
                }
            }

            $totalTourists = 0;
            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                $totalTourists += $request->tourists[$a];
            }

            if ($totalTourists == 0) {
                throw new Exception('Please select at least one tourist.');
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $request->slot_time)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'tp.stripe_destination_id', 't_o.id as tour_operator_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            if (!$info) {
                throw new Exception('Unable to add people to tour package');
            }

            if ($info->date . ' ' . $info->time_from <= mysqlDT()) {
                throw new Exception("The tour's date-time has passed; add people isn't possible in the past.");
            }

            $rateGrpIds = [];
            $permitfeeAr = [];

            for ($a = 0, $c = count($request->tourists); $a < $c; $a++) {
                if ($request->tourists[$a]) {
                    $x = $a;
                    $rateGrpIds[] = $request->rate_group[$a];
                    $permitfeeAr[] = $request->permit_fee[$a];
                }
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->whereIn('tpr.id', $rateGrpIds)
                ->where('tpr.deleted_by', 0)
                ->orderBy('tpr.orderby', 'asc')
                // ->orderByDesc('tpr.age_from')
                ->get([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage',
                    'tpr.processing_charge_percentage', 'tpr.additional_charge'
                ]);
            $trans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->orderBy('id', 'DESC')->first();
            $subttamount = $request->subtt;
            // This is for Pay Later bookings
            if ($trans == null || $subttamount==0) {
                $total = $discountAmt = 0;
                $tbTransact = $touristGroups = [];
                for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                    if (!$request->tourists[$a])
                        continue;

                    $tourists = $request->tourists[$a];
                    $groupTotal = 0;

                    for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                        if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                            $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                            break;
                        }
                    }

                    if ($b >= $bc) { // rate group not found
                        DB::rollBack();
                        throw new Exception('Unable to book the tour package.');
                    }

                    // rate may be zero for one or more age/rate group and
                    // if rate is non-zero then only add other amounts
                    $permitfeeAr[$b] = $permitfeeAr[$b];
                    // if ($rateGroups[$b]->rate != 0) {
                    $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                            $rateGroups[$b]->additional_charge +
                            $permitfeeAr[$b]
                        ));
                    // }

                    $tt = 0;
                    $tbTransact[] = [
                        'tour_package_rate_id' => $rateGroups[$b]->id,
                        'tourists' => $tourists,
                        'rate_for' =>$rateGroups[$b]->rate_for,
                        'processing_charge_percentage' => ((/*$rateGroups[$b]->rate == 0 || */$request->payment_method !== 'Card') ? 0 : $rateGroups[$b]->processing_charge_percentage),
                        'additional_tax_percentage' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_tax_percentage),
                        'additional_charge' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_charge),
                        'permit_fee' => $permitfeeAr[$b],
                        'rate' => $rateGroups[$b]->rate,
                        'total' => $groupTotal,
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ];

                    $total += $groupTotal;
                }

                $subtt = $total;
                if ($request->has('discount_type')) {
                    if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                        $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                    } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                        $discountAmt = round($request->discount_fixed_money, 2);
                    }
                }

                $total -= $discountAmt;
                $tt = $total = round($total, 2);
                $tourBooking = $this->tourBooking::find($request->bookingID);
                $tourBookingUpdate = false;
                if ($tourBooking) {
                    $updates = [
                        'tourists' => DB::raw('tourists+' . $totalTourists),
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'total' => DB::raw('total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'booking_total' => DB::raw('booking_total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'updated_at' => mysqlDT(),
                        'updated_by'=> auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];

                    if (!empty($request->discount_percentage) || (!empty($tourBooking->discount2_percentage) && $tourBooking->discount2_percentage>0)) {
                        $oldDiscountedTotalWithoutServiceCharge = $tourBooking->total;
                        $originalTotalOld = $oldDiscountedTotalWithoutServiceCharge + $tourBooking->discount2_value;

                        $newDiscountedTotalWithoutServiceCharge = $total;
                        $originalTotalNew = $newDiscountedTotalWithoutServiceCharge + $discountAmt;

                        $newDiscountPercent = round(100 - (
                                ($oldDiscountedTotalWithoutServiceCharge + $newDiscountedTotalWithoutServiceCharge) * 100 /
                                ($originalTotalOld + $originalTotalNew)
                            ), 2);

                        $updates['discount2_percentage'] = $newDiscountPercent;
                    }

                    $tourBookingUpdate = $this->tourBooking::where('id', $request->bookingID)->update($updates);
                }

                if (!$tourBookingUpdate) {
                    DB::rollBack();
                    return sendJsonErrMsg('Unable to add people to tour package.');
                }
                DB::commit();

                for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                    $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
                }

                $addedpeoplestexts = array();
                if (!empty($tbTransact)) {
                    foreach ($tbTransact as $tbltrans) {
                        $addedpeoplestexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                        unset($tbltrans['rate_for']);
                        $tbTransactexist = $this->bookingTransact::where('tour_package_rate_id', $tbltrans['tour_package_rate_id'])->where('tour_booking_id', $request->bookingID)->first();
                        $tourPackageRateGroup = $this->tourPackageRate::find($tbltrans['tour_package_rate_id']);
                        if (!empty($tbTransactexist) && isset($tbTransactexist->id)) {

                            $statusInfo = [];
                            $updatesTransact = [
                                'tourists' => DB::raw('tourists+' . $tbltrans['tourists']),
                                'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                                'total' => DB::raw('total+' . $tbltrans['total']),
                                'updated_by'=>auth()->id(),
                                'updated_by_user_type' => $userType,
                                'updated_at' => mysqlDT()
                            ];

                            if ($tbTransactexist->check_in_status != '') {
                                $statusInfo = json_decode($tbTransactexist->check_in_status);
                                for($b = $tbTransactexist->tourists; $b < ($tbltrans['tourists']+$tbTransactexist->tourists); $b++) {
                                    if (!isset($statusInfo[$b])) {
                                        $statusInfo[$b] = new \stdClass;
                                        $statusInfo[$b]->status = 'N';
                                    }
                                }

                                $updatesTransact['check_in_status'] = $statusInfo;
                            }

                            if ($request->payment_method == "Card") {
                                $updatesTransact = $this->saveCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans, $tourPackageRateGroup);
                            } else {
                                $updatesTransact = $this->saveNonCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans);
                            }

                            $this->bookingTransact::where('id', $tbTransactexist->id)->update($updatesTransact);
                        } else {
                            DB::table('tour_bookings_transact')->insert($tbltrans);
                        }
                    }
                }
                DB::commit();

                if ($userType == "Admin") {
                    $user = $this->admin::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = $this->staff::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                }

                $operationTouristsAdded = [
                    'tour_booking_id' => $tourBooking->id,
                    'tourists' => $tourBooking->tourists+$totalTourists,
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Added', 'discount_percentage' => $request->discount_percentage?:0, 'discount2_value' => $discountAmt,'added' => $addedpeoplestexts, 'newTourists' => $totalTourists, 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];

                DB::table('tour_booking_history')->insert($operationTouristsAdded);
                DB::commit();

                $errors = [];
                $paymentIntentInfo = [
                    'clientSecret' => '',
                    'transactionId' => '',
                ];
                try {
                    $slotInfo = DB::table('tour_slot_times AS tst')
                        ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                        ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                        ->where('tst.id', $request->slot_time)
                        ->when($userType == 'Tour Operator', function ($query) {
                            $query->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                        })
                        ->select([
                            DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                        ])
                        ->first();

                    $booking = [
                        'tour_d_t' => $info->date . ' ' . $info->time_from,
                    ];


                    $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                        ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                        ->where('tbm.tour_booking_id', '=', $request->bookingID)
                        ->select('tpr.rate_for', 'tbm.tourists')
                        ->get();

                    $arEmailData = [
                        'bookingId' => $request->bookingID,
                        'tourOperatorId' => $info->tour_operator_id,
                        'tourOperator' => $info->tour_operator,
                        'tourOpPhone' => $info->tour_op_phone,
                        'tourOpEmail' => $info->tour_op_email,
                        'tourOpWebsite' => $info->tour_op_website,
                        'tourPackage' => $info->package_name,
                        'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                        'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                        'custName' => $tourBooking->name,
                        'custEmail' => $tourBooking->email,
                        'custPhone' => $tourBooking->phone_number,
                        'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                        'importantNotes' => $info->important_notes,
                        'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                        'cancellationPolicy' => $info->cancellation_policy,
                        'numberoftourist' => $tourBooking->tourists,
                        'tourListwithSeat' => $tourListwithSeat,
                        'packageEmail' => $info->packageEmail,
                        'packageFromEmail' => $info->packageFromEmail,
                        'packageApiKey' => $info->packageApiKey,
                    ];

                    if (!empty($info->tour_op_logo)) {
                        if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                            $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                        } else {
                            $tourOperatorLogo = asset('images/no-photo.png');
                        }
                        $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                    }

                    $tourPkgPhoto = DB::table('tour_package_photos')
                        ->where('tour_package_id', $info->tour_package_id)
                        ->where('placement', 2)
                        ->where('status', 'Active')
                        ->latest()->first(['path']);

                    if ($tourPkgPhoto) {
                        if (Storage::disk('s3')->exists('images/tour-operator/package/' . $tourPkgPhoto->path)) {
                            $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $tourPkgPhoto->path);
                        } else {
                            $tourPackageLogo = asset('images/no-photo.png');
                        }
                        $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                    }



                    // TODO
                    if(auth()->id()!=130) {
                        $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                    }

                } catch (Exception $exception) {
                    Log::error($exception);
                    $errors[] = 'email';
                }

                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if(!$request->is('api/*')) {
                if ($isValidationCheckForBookingAddPeople) {
                    return response()->json([
                        'message' => 'All validation checks were successful!'
                    ]);
                }
            }


            $total = $discountAmt = 0;
            $tbTransact = $touristGroups = [];
            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                if (!$request->tourists[$a])
                    continue;

                $tourists = $request->tourists[$a];
                $groupTotal = 0;

                for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                    if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                        $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                        break;
                    }
                }

                if ($b >= $bc) { // rate group not found
                    DB::rollBack();
                    throw new Exception('Unable to add people to tour package.');
                }

                // rate may be zero for one or more age/rate group and
                // if rate is non-zero then only add other amounts
                // if ($rateGroups[$b]->rate != 0) {
                $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                        $rateGroups[$b]->additional_charge +
                        $permitfeeAr[$b]
                    ));

                if ($request->payment_method === 'Card') {
                    $groupTotal += ($groupTotal * (float)$rateGroups[$b]->processing_charge_percentage) / 100;
                }
                // }

                $tt = 0;
                $tbTransact[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'rate_for' =>$rateGroups[$b]->rate_for,
                    'processing_charge_percentage' => ((/*$rateGroups[$b]->rate == 0 || */$request->payment_method !== 'Card') ? 0 : $rateGroups[$b]->processing_charge_percentage),
                    'additional_tax_percentage' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => (/*$rateGroups[$b]->rate == 0 ? 0 : */$rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => $groupTotal,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];

                $total += $groupTotal;
            }

            $subtt = $total;
            if ($request->has('discount_type')) {
                if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                    $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                    $discountAmt = round($request->discount_fixed_money, 2);
                }
            }

            $tourBooking = $this->tourBooking::find($request->bookingID);
            $total -= $discountAmt;

            if ($request->payment_method != "Card") {
                $subtt -= $discountAmt;
            }

            $serviceCommission = 0;
            $checkaffiliate = null;
            if ($tourBooking->customer_id != 0) {
                $checkaffiliate = $this->customer::where('id', $tourBooking->customer_id)->first();
            }

            if ($request->payment_method == "Card" && $checkaffiliate->is_affiliate=='No') {
                $serviceCommission = $total * $info->service_commission_percentage / 100;
                $total += $serviceCommission;
            }

            $tt = $total = round($total, 2);
            $serviceCommission = round($serviceCommission, 2);

            $tourBookingUpdate = false;
            if ($tourBooking) {
                if($tourBooking->booking_total=='' || $tourBooking->booking_total==0){
                    $bookingT = $this->bookingTransact::with('packageRate')
                        ->where('tour_booking_id', $request->bookingID)
                        ->get();

                    $bookingTotal = 0;

                    // Calculate the booking amount because old bookings data have booking_total in tour_booking_master table is 0
                    foreach ($bookingT as $tbTransaction) {
                        $rate = $tbTransaction->rate;
                        $permitFee = $tbTransaction->permit_fee;
                        $procFee = $request->payment_method == 'Card' ? $tbTransaction->packageRate->processing_charge_percentage : 0;
                        $additionalCharge = $tbTransaction->additional_charge;
                        $tourists = $tbTransaction->tourists;
                        $refund_permitfee_status = ($tbTransaction->refund_permitfee_status != '') ? json_decode($tbTransaction->refund_permitfee_status) : '';
                        $refundedPermitFeesTotal = 0;

                        if(!empty($refund_permitfee_status) && $permitFee > 0) {
                            foreach ($refund_permitfee_status as $val) {
                                if($val->status=='Y') {
                                    $refundedPermitFeesTotal += (isset($val->amount) && $val->amount > 0) ? $val->amount : 0;
                                }
                            }
                            $subTotal = (($rate + $permitFee + $additionalCharge) * $tourists) - $refundedPermitFeesTotal;
                        } else {
                            $subTotal = ($rate + $permitFee + $additionalCharge) * $tourists;
                        }

                        $bookingTotal += $procFee ? $subTotal + ($subTotal * ($procFee / 100)) : $subTotal;
                    }

                    $serviceCommissionOld = 0;
                    if($request->payment_method == "Card" && $checkaffiliate->is_affiliate=='No') {
                        $serviceCommissionOld = $bookingTotal * $info->service_commission_percentage / 100;
                        $bookingTotal += $serviceCommissionOld;
                    }

                    if ($request->payment_method == "Card") {
                        $bookingTotal += $total;
                    } else {
                        $bookingTotal += $subtt;
                    }

                    $updates = [
                        'tourists' => DB::raw('tourists+' . $totalTourists),
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'service_commission' => DB::raw('service_commission+' . ($request->payment_method == "Card" ? $serviceCommission : 0)),
                        'total' => DB::raw('total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'booking_total' => DB::raw('booking_total+' . ($bookingTotal)),
                        'updated_at' => mysqlDT(),
                        'updated_by' => auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];
                } else {
                    $updates = [
                        'tourists' => DB::raw('tourists+' . $totalTourists),
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'service_commission' => DB::raw('service_commission+' . ($request->payment_method == "Card" ? $serviceCommission : 0)),
                        'total' => DB::raw('total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'booking_total' => DB::raw('booking_total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'updated_at' => mysqlDT(),
                        'updated_by' => auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];
                }

                if (!empty($request->discount_percentage) || (!empty($tourBooking->discount2_percentage) && $tourBooking->discount2_percentage>0)) {
                    $oldDiscountedTotalWithoutServiceCharge = $tourBooking->total - $tourBooking->service_commission;
                    $originalTotalOld = $oldDiscountedTotalWithoutServiceCharge + $tourBooking->discount2_value;

                    $newDiscountedTotalWithoutServiceCharge = $total - $serviceCommission;
                    $originalTotalNew = $newDiscountedTotalWithoutServiceCharge + $discountAmt;

                    $newDiscountPercent = round(100 - (
                            ($oldDiscountedTotalWithoutServiceCharge + $newDiscountedTotalWithoutServiceCharge) * 100 /
                            ($originalTotalOld + $originalTotalNew)
                        ), 2);

                    $updates['discount2_percentage'] = $newDiscountPercent;
                }

                $tourBookingUpdate = $this->tourBooking::where('id', $request->bookingID)->update($updates);
            }

            if (!$tourBookingUpdate) {
                DB::rollBack();
                if (request()->is('api/*')) {
                    throw new Exception('Unable to add people to tour package.');
                } else {
                    return sendJsonErrMsg('Unable to add people to tour package.');
                }
            }
            DB::commit();

            for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
            }

            $addedpeoplestexts = array();
            if (!empty($tbTransact)) {
                foreach ($tbTransact as $tbltrans) {
                    $tbTransactexist = $this->bookingTransact::where('tour_package_rate_id', $tbltrans['tour_package_rate_id'])->where('tour_booking_id', $request->bookingID)->first();
                    $tourPackageRateGroup = $this->tourPackageRate::find($tbltrans['tour_package_rate_id']);
                    $addedpeoplestexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                    unset($tbltrans['rate_for']);
                    if (!empty($tbTransactexist) && isset($tbTransactexist->id)) {

                        $statusInfo = [];
                        $updatesTransact = [
                            'tourists' => DB::raw('tourists+' . $tbltrans['tourists']),
                            'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                            'total' => DB::raw('total+' . $tbltrans['total']),
                            'updated_by'=>auth()->id(),
                            'updated_by_user_type' => $userType,
                            'updated_at' => mysqlDT()
                        ];

                        if ($tbTransactexist->check_in_status != '') {
                            $statusInfo = json_decode($tbTransactexist->check_in_status);
                            for($b = 0; $b < ($tbltrans['tourists']+$tbTransactexist->tourists); $b++) {
                                if (!isset($statusInfo[$b])) {
                                    $statusInfo[$b] = new \stdClass;
                                    $statusInfo[$b]->status = 'N';
                                }
                            }

                            $updatesTransact['check_in_status'] = (!empty($statusInfo))?json_encode((array_values($statusInfo))):'';
                        }

                        if ($request->payment_method == "Card") {
                            $updatesTransact = $this->saveCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans, $tourPackageRateGroup);
                        } else {
                            $updatesTransact = $this->saveNonCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans);
                        }

                        $this->bookingTransact::where('id', $tbTransactexist->id)->update($updatesTransact);
                    } else {
                        DB::table('tour_bookings_transact')->insert($tbltrans);
                    }
                }
            }
            DB::commit();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $operationTouristsAdded = [
                'tour_booking_id' => $tourBooking->id,
                'tourists' => $tourBooking->tourists+$totalTourists,
                'operation' => 'Update',
                'details' => json_encode(['sub_operation' => 'Tourists Added', 'discount_percentage' => $request->discount_percentage?:0, 'discount2_value' => $discountAmt,'added' => $addedpeoplestexts, 'newTourists' => $totalTourists, 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                'operation_by' => auth()->id(),
                'operation_by_user_type' => $userType,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ];

            if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later' || $request->payment_method == 'Invoiced') {
                $paymentMethod = ($request->payment_method == 'Pay Later' || $request->payment_method == 'Invoiced') ? 'Invoiced' : 'Cash';
                $transactionId = DB::table('transactions')
                    ->insertGetId([
                        'tour_booking_id' => $tourBooking->id,
                        'amount' => !empty($request->split_value) ? $request->split_value :$subtt,
                        'transaction_number' => '',
                        'via' => $paymentMethod,
                        'created_at' => mysqlDT(),
                        'response_init' => '',
                        'response_end' => '',
                        'response_end_datetime' => '0000-00-00 00:00:00',
                        'payment_type' => !empty($request->split_value) ? 1 : 0,
                        'status' => 'Completed',
                        'visitor' => $request->getClientIp(),
                        'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();

                $historyId = DB::table('tour_booking_history')
                    ->insertGetId([
                        'tour_booking_id' => $tourBooking->id,
                        'tourists' => $tourBooking->tourists,
                        'operation' => 'Payment',
                        'details' => json_encode(['amount' => !empty($request->split_value) ? $request->split_value : $subtt, 'payment_type' => $paymentMethod, 'created_by_with_name' => $created_by_with_name, 'payment_method' => !empty($request->split_value) ? 'Split Payment' :'Full Payment' ], JSON_NUMERIC_CHECK),
                        'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();

                // Below line is for to update tour booking history table
                $isSplitOrFull = !empty($request->split_value) ? 'Split Payment' : 'Full Payment';
                if ($paymentMethod === 'Cash') {
                    dispatch(new CashBooking($tourBooking, $transactionId, $historyId, $isSplitOrFull, $request->all() + ['is_pax_added' => true]));
                } elseif ($paymentMethod === 'Invoiced') {
                    dispatch(new InvoicedBooking($tourBooking, $transactionId, $historyId, $isSplitOrFull, $request->all() + ['is_pax_added' => true]));
                }

                DB::table('tour_booking_history')->insert($operationTouristsAdded);
            }

            DB::commit();
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }

        $errors = [];

        $paymentIntentInfo = [
            'clientSecret' => '',
            'transactionId' => '',
        ];

        try {
            $slotInfo = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->where('tst.id', $request->slot_time)
                ->when($userType == 'Tour Operator', function ($query) {
                    $query->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                })
                ->select([
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ])
                ->first();
            $booking = [
                'tour_d_t' => $info->date . ' ' . $info->time_from,
            ];


            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $request->bookingID)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $arEmailData = [
                'bookingId' => $request->bookingID,
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'custName' => $tourBooking->name,
                'custEmail' => $tourBooking->email,
                'custPhone' => $tourBooking->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }



            if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later' || $paymentMethod=='Cash' || $paymentMethod=='Invoiced') {
                // TODO
                if(auth()->id()!=130) {
                    $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            $errors[] = 'email';
        }
        if ($request->payment_method == 'Pay Later') {
            return [
                'bookingId' => $tourBooking->id,
                'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                'stripeId' => $paymentIntentInfo['clientSecret'],
                'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                'errors' => $errors,
            ];
        }

        $checkaffiliate = null;
        if ($request->affiliatecust != 0) {
            $checkaffiliate = $this->customer::where('id', $request->affiliatecust)->first();
        }

        if (!empty($checkaffiliate)) {
            $tt = $request->subtt;
            $request->fee = 0;
        }

        $month = substr($request->expiration, 0, 2);
        $yearSet = substr($request->expiration, 3, 2);
        $y = 20;
        $year = $y . '' . $yearSet;
        $cardnumber = $request->cardnumber;
        $cvc = $request->cvv;
        $master = $this->tourBooking::where('id', $tourBooking->id)->first();

        if ($request->payment_method == 'Card') {
            $data = [
                'total' => $tt,
                'customer_id' => $master->customer_id,
                'customer_name' => $master->name,
                'customer_email' => $master->email,
                'customer_phone' => $master->phone_number,
                'tourists' => $totalTourists,
                'booking_id' => $tourBooking->id,
                'tour_package' => $info->package_name,
                'date' => $info->date,
                'time_from' => $info->time_from,
                'subtotal' => $subtt,
                'serviceCommission' => $serviceCommission,
                'month' => $month,
                'year' => $year,
                'cardnumber' => $cardnumber,
                'cvc' => $cvc,
                'destination' => $info->stripe_destination_id,
                'slot_time' => $request->slot_time,
                'tour_operator_id' => $info->tour_operator_id,
                'note' => $request->tour_operator_note,
                'booked_by_staff' => $request->booked_by_staff,
                'split_value' => $request->split_value,
                'api' => $request->api,
                'add_people'=>1,
                'discount_percentage' => $request->discount_percentage?$request->discount_percentage:0,
                'discount2_value' => $discountAmt,
                'newTourists' => $totalTourists,
                'addedpeoplestexts'=>$addedpeoplestexts,
                'is_pax_added' => true,
            ];

            if ($request->has('api') && $request->api=='1') {
                $paymentIntentId= ($request->has('payment_intent_id') && $request->payment_intent_id!='')?$request->payment_intent_id:'';
                if ($paymentIntentId == '') {
                    throw new Exception('You must provide Stripe Payment Intent Id');
                }
                $data['payment_intent_id'] = $paymentIntentId;
                $paymentIntentInfo = $this->createBookingCard($data);
            } else {
                $paymentIntentInfo = $this->createPaymentIntent($data);
            }

            if (isset($paymentIntentInfo['nextAction'])) {
                $master->stripe_paymentintent_id = $paymentIntentInfo['clientSecret'];
                $master->save();
                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'require3ds' => 1,
                    'nextAction' => $paymentIntentInfo['nextAction'],
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if ($paymentIntentInfo['clientSecret'] == '') {
                $errors[] = 'payment';
            }

            if (!empty($paymentIntentInfo['ex'])) {
                $errors_message = $paymentIntentInfo['ex']->getError()->message;
            }
        }

        return [
            'bookingId' => $tourBooking->id,
            'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
            'stripeId' => $paymentIntentInfo['clientSecret'],
            'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
            'errors' => $errors,
            'errors_message' => !empty($errors_message) ? $errors_message : '',
        ];
    }
    /**
     * Change people in the existing booking
     *
     * @param object $request
     * @return array
     */
    public function changePeopleBooking($request)
    {
        DB::beginTransaction();
        try {
            $isValidationCheckForBookingAddPeople = $request->get('validation_check') === 'true';
            if (isset($request->api) && $request->api == 1) {
                if (isset($request->permit_fee[0])) {
                    $request->permit_fee = explode(",", $request->permit_fee[0]);
                }

                if (isset($request->rate_group[0])) {
                    $request->rate_group = explode(",", $request->rate_group[0]);
                }

                if (isset($request->tourists[0])) {
                    $request->tourists = explode(",", $request->tourists[0]);
                }
            }
            $oldBookingID = config('constants.old_bookings.booking_id');
            $bookingT = DB::table('tour_bookings_transact AS tbt')
                ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                ->where('tbt.tour_booking_id', $request->bookingID)
                //->orderByDesc('tpr.age_from')
                //->orderByDesc('tpr.age_to')
                ->orderBy('tpr.orderby', 'asc')
                ->get(['tpr.rate_for', 'tbt.tour_package_rate_id', 'tbt.tourists','tbt.total']);

            $rateGroupWiseTourists = array();
            $rateGroupWiseTotal = array();
            foreach ($bookingT as $value) {
                $rateGroupWiseTourists[$value->tour_package_rate_id] = $value->tourists;
                $rateGroupWiseTotal[$value->tour_package_rate_id] = $value->total;
            }

            if (empty($rateGroupWiseTourists)) {
                throw new Exception('Please select at least one tourist.');
            }

            //Validations to restrict user to perform only delete peoples
            $addedPeopleCount = 0;
            $deletedPeopleCount = 0;
            $rateGroupWiseaddedPeople = array();
            $rateGroupWisedeletedPeople = array();
            $rateGroupWiseaddedPeopleArray = array();
            $rateGroupWisedeletedPeopleArray = array();
            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                $rateGroupWiseaddedPeople[$a] = 0;
                $rateGroupWisedeletedPeople[$a] = 0;
                if (isset($request->rate_group[$a]) && isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $request->tourists[$a] > $rateGroupWiseTourists[$request->rate_group[$a]]) {
                    $addedPeopleCount += $request->tourists[$a] - $rateGroupWiseTourists[$request->rate_group[$a]];
                    $rateGroupWiseaddedPeople[$a] = (int)$request->tourists[$a] - $rateGroupWiseTourists[$request->rate_group[$a]];
                    $rateGroupWiseaddedPeopleArray[$request->rate_group[$a]] = $rateGroupWiseaddedPeople[$a];
                }
                if (isset($request->rate_group[$a]) && isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $request->tourists[$a] < $rateGroupWiseTourists[$request->rate_group[$a]]) {
                    $deletedPeopleCount += $rateGroupWiseTourists[$request->rate_group[$a]] - $request->tourists[$a];
                    $rateGroupWisedeletedPeople[$a] = (int)$rateGroupWiseTourists[$request->rate_group[$a]] - $request->tourists[$a];
                    $rateGroupWisedeletedPeopleArray[$request->rate_group[$a]] = $rateGroupWisedeletedPeople[$a];
                }
                if (isset($request->rate_group[$a]) && !isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $request->tourists[$a] > 0) {
                    $addedPeopleCount += $request->tourists[$a];
                    $rateGroupWiseaddedPeople[$a] = (int)$request->tourists[$a];
                    $rateGroupWiseaddedPeopleArray[$request->rate_group[$a]] = (int)$rateGroupWiseaddedPeople[$a];
                }
            }

            if ($addedPeopleCount == 0 && $deletedPeopleCount == 0) {
                throw new Exception('You must need to select tourists to change.');
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $request->slot_time)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'tp.stripe_destination_id', 't_o.id as tour_operator_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            if (!$info) {
                throw new Exception('Unable to change people to tour package');
            }

            $tourBooking = $this->tourBooking::find($request->bookingID);
            $customer = $this->customer::where('id', $tourBooking->customer_id)->first();

            $natFeesPercentage = 0;
            $stripeFeesPercentage = 0;
            if ($request->bookingID > $oldBookingID) {
                if ($customer->is_affiliate == 'Yes') {
                    $natFeesPercentage = $info->affiliate_processing_percentage;
                    $stripeFeesPercentage = $info->stripe_affiliate_percentage;
                } else {
                    $natFeesPercentage = $info->service_commission_percentage;
                    $stripeFeesPercentage = $info->stripe_commission_percentage;
                }
            }

            $rateGrpIds = [];
            $permitfeeAr = [];

            for ($a = 0, $c = count($request->tourists); $a < $c; $a++) {
                if ($request->tourists[$a]) {
                    $rateGrpIds[] = $request->rate_group[$a];
                    $permitfeeAr[] = $request->permit_fee[$a];
                } else if ($request->tourists[$a] == 0 && isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $rateGroupWiseTourists[$request->rate_group[$a]] > 0){
                    $rateGrpIds[] = $request->rate_group[$a];
                    $permitfeeAr[] = $request->permit_fee[$a];
                }
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->whereIn('tpr.id', $rateGrpIds)
                ->where('tpr.deleted_by', 0)
                // ->orderByDesc('tpr.age_from')
                ->orderBy('tpr.orderby', 'asc')
                ->get([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage',
                    'tpr.processing_charge_percentage', 'tpr.additional_charge'
                ]);

            $trans = $this->transaction::where('tour_booking_id', $request->bookingID)->where('status', 'Completed')->orderBy('id', 'DESC')->first();
            $payAmount = $request->payment_amount;
            $refundAmount = $request->refund_amount;
            $infantsorchilds = $request->infantorchilds;

            // This is for Pay Later bookings and infants and payment and refund amounts are same
            if ($trans == null || ($payAmount==0 && $refundAmount==0 && $infantsorchilds>0) || ($payAmount==$refundAmount || round($refundAmount - $payAmount,2)==-0.01 || round($payAmount - $refundAmount,2)==0.01)) {
                $total = $discountAmt = 0;
                $tbTransact = $touristGroups = [];

                $newlyAddedTotal = 0;
                $deletedTotal = 0;
                $paymentTotal = 0;
                $refundTotal = 0;

                for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                    if (!$request->tourists[$a]) {
                        if(!isset($rateGroupWiseTourists[$request->rate_group[$a]]) || (isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $rateGroupWiseTourists[$request->rate_group[$a]] == 0)) {
                            continue;
                        }
                    }

                    $tourists = 0;
                    $added = false;
                    $deleted = false;

                    if(isset($rateGroupWiseaddedPeople[$a]) && $rateGroupWiseaddedPeople[$a]>0){
                        $tourists = $rateGroupWiseaddedPeople[$a];
                        $added = true;
                    }

                    if(isset($rateGroupWisedeletedPeople[$a]) && $rateGroupWisedeletedPeople[$a]>0){
                        $tourists = $rateGroupWisedeletedPeople[$a];
                        $deleted = true;
                    }

                    $groupTotal = 0;

                    for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                        if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                            $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                            break;
                        }
                    }

                    if ($b >= $bc) { // rate group not found
                        DB::rollBack();
                        throw new Exception('Unable to change people in tour package.');
                    }

                    // rate may be zero for one or more age/rate group and
                    // if rate is non-zero then only add other amounts
                    $permitfeeAr[$b] = $permitfeeAr[$b];
                    if ($rateGroups[$b]->rate != 0) {
                        $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                                $rateGroups[$b]->additional_charge +
                                $permitfeeAr[$b]
                            ));
                    }

                    $tt = 0;
                    $tbTransact[] = [
                        'tour_package_rate_id' => $rateGroups[$b]->id,
                        'tourists' => $tourists,
                        'added' => $added,
                        'deleted' => $deleted,
                        'rate_for' =>$rateGroups[$b]->rate_for,
                        'processing_charge_percentage' => 0,
                        'nat_fees_percentage' => $natFeesPercentage,
                        'stripe_fees_percentage' => $stripeFeesPercentage,
                        'additional_tax_percentage' => $rateGroups[$b]->additional_tax_percentage,
                        'additional_charge' => $rateGroups[$b]->additional_charge,
                        'permit_fee' => $permitfeeAr[$b],
                        'rate' => $rateGroups[$b]->rate,
                        'total' => ($rateGroups[$b]->rate == 0 ? 0 : $groupTotal),
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ];

                    if (isset($rateGroupWiseaddedPeople[$a]) && $rateGroupWiseaddedPeople[$a] > 0) {
                        $total += $groupTotal;
                        $paymentTotal+=$groupTotal;
                    }

                    if (isset($rateGroupWisedeletedPeople[$a]) && $rateGroupWisedeletedPeople[$a] > 0) {
                        if (isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $rateGroupWiseTourists[$request->rate_group[$a]] > 0) {
                            $total -= $groupTotal;
                            $refundTotal += $groupTotal;
                            if ($request->tourists[$a] == 0) {
                                $deletedTotal += $groupTotal;
                            }
                        }
                    }

                    $bookingTExists = $this->bookingTransact::where('tour_booking_id', $request->bookingID)
                        ->where('tour_package_rate_id',$request->rate_group[$a])
                        ->count();

                    if ($bookingTExists == 0) {
                        $newlyAddedTotal+=$groupTotal;
                    }
                }

                $subtt = $total;

                //This is for refund process
                $refundProcess = false;
                if ($total < 0) {
                    $refundProcess = true;
                }

                if (!$refundProcess) {
                    if ($request->has('discount_type')) {
                        if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                            $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                        } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                            $discountAmt = round($request->discount_fixed_money, 2);
                        }
                    }
                }

                $tt = $total = round($total, 2);
                $tourBooking = $this->tourBooking::find($request->bookingID);
                $totalTourists = $tourBooking->tourists + $addedPeopleCount - $deletedPeopleCount;

                $tourBookingUpdate = false;
                if ($tourBooking) {
                    $updates = [
                        'tourists' => $totalTourists,
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'total' => DB::raw('total+' . $subtt),
                        'booking_total' => DB::raw('booking_total+' . $subtt),
                        'updated_at' => mysqlDT(),
                        'updated_by'=> auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];

                    if (!empty($request->discount_percentage) || (!empty($tourBooking->discount2_percentage) && $tourBooking->discount2_percentage>0)) {
                        $oldDiscountedTotalWithoutServiceCharge = $tourBooking->total;
                        $originalTotalOld = $oldDiscountedTotalWithoutServiceCharge + $tourBooking->discount2_value;

                        $newDiscountedTotalWithoutServiceCharge = $total;
                        $originalTotalNew = $newDiscountedTotalWithoutServiceCharge + $discountAmt;

                        $newDiscountPercent = round(100 - (
                                ($oldDiscountedTotalWithoutServiceCharge + $newDiscountedTotalWithoutServiceCharge) * 100 /
                                ($originalTotalOld + $originalTotalNew)
                            ), 2);

                        $updates['discount2_percentage'] = $newDiscountPercent;
                    }

                    $tourBookingUpdate = $this->tourBooking::where('id', $request->bookingID)->update($updates);
                }

                if (!$tourBookingUpdate) {
                    DB::rollBack();
                    return sendJsonErrMsg('Unable to change people to tour package.');
                }

                for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                    $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
                }

                $addedPeoplesTexts = array();
                $deletedPeoplesTexts= array();

                if (!empty($tbTransact)) {
                    foreach ($tbTransact as $tbltrans) {

                        $tbTransactexist = $this->bookingTransact::where('tour_package_rate_id', $tbltrans['tour_package_rate_id'])->where('tour_booking_id', $request->bookingID)->first();
                        $tourPackageRateGroup = $this->tourPackageRate::find($tbltrans['tour_package_rate_id']);
                        if (!empty($tbTransactexist) && isset($tbTransactexist->id)) {

                            $statusInfo = [];
                            $updatesTransact = [];
                            if($tbltrans['added']==true){
                                $addedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                                $updatesTransact = [
                                    'tourists' => DB::raw('tourists+' . $tbltrans['tourists']),
                                    'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                                    'total' => DB::raw('total+' . $tbltrans['total']),
                                    'updated_by'=>auth()->id(),
                                    'updated_by_user_type' => $userType,
                                    'updated_at' => mysqlDT()
                                ];
                            }else if($tbltrans['deleted']==true){
                                $deletedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                                $updatesTransact = [
                                    'tourists' => DB::raw('tourists-' . $tbltrans['tourists']),
                                    'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                                    'total' => DB::raw('total-' . $tbltrans['total']),
                                    'updated_by'=>auth()->id(),
                                    'updated_by_user_type' => $userType,
                                    'updated_at' => mysqlDT()
                                ];
                            }
                            unset($tbltrans['rate_for']);

                            if ($tbTransactexist->check_in_status != '') {
                                $statusInfo = json_decode($tbTransactexist->check_in_status);
                                for($b = $tbTransactexist->tourists; $b < ($tbltrans['tourists']+$tbTransactexist->tourists); $b++) {
                                    if (!isset($statusInfo[$b])) {
                                        $statusInfo[$b] = new \stdClass;
                                        $statusInfo[$b]->status = 'N';
                                    }
                                }

                                $updatesTransact['check_in_status'] = $statusInfo;
                            }
                            if(isset($updatesTransact) && !empty($updatesTransact)) {
                                if ($request->payment_method == "Card") {
                                    $updatesTransact = $this->saveCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans, $tourPackageRateGroup);
                                } else {
                                    $updatesTransact = $this->saveNonCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans);
                                }
                            }
                            if(isset($updatesTransact) && !empty($updatesTransact)) {
                                $this->bookingTransact::where('id', $tbTransactexist->id)->update($updatesTransact);
                            }
                        } else {
                            $addedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                            unset($tbltrans['rate_for']);
                            unset($tbltrans['added']);
                            unset($tbltrans['deleted']);
                            DB::table('tour_bookings_transact')->insert($tbltrans);
                        }
                    }
                }
                DB::commit();

                if ($userType == "Admin") {
                    $user = $this->admin::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = $this->staff::find(auth()->id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                }

                $operationTouristsChanged = [
                    'tour_booking_id' => $tourBooking->id,
                    'tourists' => $totalTourists,
                    'operation' => 'Update',
                    'details' => json_encode(['sub_operation' => 'Tourists Changed', 'discount_percentage' => $request->discount_percentage?:0, 'discount2_value' => $discountAmt,'added' => $addedPeoplesTexts,'deleted' =>$deletedPeoplesTexts,'addedTourists' => $addedPeopleCount,'deletedTourits'=>$deletedPeopleCount, 'paymenttotal'=>$paymentTotal,'refundtotal'=>$refundTotal, 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ];

                DB::table('tour_booking_history')->insert($operationTouristsChanged);
                DB::commit();

                $errors = [];
                $paymentIntentInfo = [
                    'clientSecret' => '',
                    'transactionId' => '',
                ];
                try {
                    $booking = [
                        'tour_d_t' => $info->date . ' ' . $info->time_from,
                    ];

                    $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                        ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                        ->where('tbm.tour_booking_id', '=', $request->bookingID)
                        ->select('tpr.rate_for', 'tbm.tourists')
                        ->get();

                    $arEmailData = [
                        'bookingId' => $request->bookingID,
                        'tourOperatorId' => $info->tour_operator_id,
                        'tourOperator' => $info->tour_operator,
                        'tourOpPhone' => $info->tour_op_phone,
                        'tourOpEmail' => $info->tour_op_email,
                        'tourOpWebsite' => $info->tour_op_website,
                        'tourPackage' => $info->package_name,
                        'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                        'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                        'custName' => $tourBooking->name,
                        'custEmail' => $tourBooking->email,
                        'custPhone' => $tourBooking->phone_number,
                        'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                        'importantNotes' => $info->important_notes,
                        'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                        'cancellationPolicy' => $info->cancellation_policy,
                        'numberoftourist' => $totalTourists,
                        'tourListwithSeat' => $tourListwithSeat,
                        'packageEmail' => $info->packageEmail,
                        'packageFromEmail' => $info->packageFromEmail,
                        'packageApiKey' => $info->packageApiKey,
                    ];

                    if (!empty($info->tour_op_logo)) {
                        if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                            $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                        } else {
                            $tourOperatorLogo = asset('images/no-photo.png');
                        }
                        $arEmailData['tourOpLogo'] = $tourOperatorLogo;
                    }

                    $tourPkgPhoto = DB::table('tour_package_photos')
                        ->where('tour_package_id', $info->tour_package_id)
                        ->where('placement', 2)
                        ->where('status', 'Active')
                        ->latest()->first(['path']);

                    if ($tourPkgPhoto) {
                        if (Storage::disk('s3')->exists('images/tour-operator/package/' . $tourPkgPhoto->path)) {
                            $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $tourPkgPhoto->path);
                        } else {
                            $tourPackageLogo = asset('images/no-photo.png');
                        }
                        $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
                    }

                    // TODO
                    if (auth()->id() != 130) {
                        $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                    }
                } catch (Exception $exception) {
                    Log::error($exception);
                    $errors[] = 'email';
                }

                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if (!$request->is('api/*')) {
                if ($isValidationCheckForBookingAddPeople) {
                    return response()->json([
                        'message' => 'All validation checks were successful!'
                    ]);
                }
            }

            if($addedPeopleCount == 0 && $deletedPeopleCount > 0) {
                $formData = $request->all();
                $formData['clientIp'] =  $request->getClientIp();
                $formData['requestFrom'] = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                $res = $this->deletePeopleChangePeople($formData);
                if (!empty($res) && isset($res['success']) && $res['success'] == true) {
                    DB::commit();
                    return [
                        'bookingId' => $request->bookingID,
                        'bookingToken' => Crypt::encryptString($request->bookingID), // for security purpose
                    ];
                } else {
                    DB::rollBack();
                    return sendJsonErrMsg('Something went wrong.Unable to change people to tour package.');
                }
            }

            $transactionFirst = $this->transaction::where('status', 'Completed')->where('tour_booking_id', $request->bookingID)->first();
            $paymentMethod = (!empty($transactionFirst))?$transactionFirst->via:'';

            $total = $discountAmt = 0;
            $tbTransact = $touristGroups = [];

            $newlyAddedTotal = 0;
            $paymentTotal = 0;
            $refundTotal = 0;
            $serviceCommission = 0;
            $serviceCommissionTotal = 0;
            $serviceCommissionNewlyAddedTotal = 0;
            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                if (!$request->tourists[$a]) {
                    if(!isset($rateGroupWiseTourists[$request->rate_group[$a]]) || (isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $rateGroupWiseTourists[$request->rate_group[$a]] == 0)) {
                        continue;
                    }
                }

                $tourists = 0;
                $added = false;
                $deleted = false;
                if(isset($rateGroupWiseaddedPeople[$a]) && $rateGroupWiseaddedPeople[$a]>0){
                    $tourists = $rateGroupWiseaddedPeople[$a];
                    $added = true;
                }

                if(isset($rateGroupWisedeletedPeople[$a]) && $rateGroupWisedeletedPeople[$a]>0){
                    $tourists = $rateGroupWisedeletedPeople[$a];
                    $deleted = true;
                }

                $groupTotal = 0;

                for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                    if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                        $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                        break;
                    }
                }

                if ($b >= $bc) { // rate group not found
                    DB::rollBack();
                    throw new Exception('Unable to change people to tour package.');
                }

                // rate may be zero for one or more age/rate group and
                // if rate is non-zero then only add other amounts
                if ($rateGroups[$b]->rate != 0) {
                    $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                            $rateGroups[$b]->additional_charge +
                            $permitfeeAr[$b]
                        ));

                    if ($request->payment_method == 'Card' || $paymentMethod == 'Card') {
                        if ($request->bookingID < $oldBookingID) {
                            $groupTotal += ($groupTotal * 2.9) / 100;
                        } else {
                            if ($request->has('discount_type') == false) {
                                $serviceCommissionPax = roundout((($rateGroups[$b]->rate + $rateGroups[$b]->additional_charge + $permitfeeAr[$b]) * $natFeesPercentage / 100), 2);
                                $serviceCommissionPax = $tourists * $serviceCommissionPax;
                                $serviceCommissionTotal += $serviceCommissionPax;
                            }
                            $groupTotal += ($groupTotal * (float)$rateGroups[$b]->processing_charge_percentage) / 100;
                        }
                    }
                }

                $tt = 0;
                $processing_charge_percentage = 0;
                if ($request->payment_method == 'Card' || $paymentMethod=='Card') {
                    if ($request->bookingID < $oldBookingID) {
                        $processing_charge_percentage = 2.9;
                    } else {
                        $processing_charge_percentage = $rateGroups[$b]->processing_charge_percentage;
                    }

                    if ($rateGroups[$b]->rate == 0) {
                        $processing_charge_percentage = 0;
                    }
                }

                $tbTransact[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'added' => $added,
                    'deleted' => $deleted,
                    'rate_for' =>$rateGroups[$b]->rate_for,
                    'processing_charge_percentage' => $processing_charge_percentage,
                    'nat_fees_percentage' => $natFeesPercentage,
                    'stripe_fees_percentage' => $stripeFeesPercentage,
                    'additional_tax_percentage' => ($rateGroups[$b]->rate == 0 ? 0 : $rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => ($rateGroups[$b]->rate == 0 ? 0 : $rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => ($rateGroups[$b]->rate == 0 ? 0 : $groupTotal),
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];

                if (isset($rateGroupWiseaddedPeople[$a]) && $rateGroupWiseaddedPeople[$a] > 0) {
                    $total += $groupTotal;
                    $paymentTotal+=$groupTotal;
                }

                if (isset($rateGroupWisedeletedPeople[$a]) && $rateGroupWisedeletedPeople[$a] > 0){
                    if(isset($rateGroupWiseTourists[$request->rate_group[$a]]) && $rateGroupWiseTourists[$request->rate_group[$a]] > 0){
                        $total -= $groupTotal;
                        $refundTotal += $groupTotal;
                    }
                }

                $bookingTExists = $this->bookingTransact::where('tour_booking_id', $request->bookingID)
                    ->where('tour_package_rate_id',$request->rate_group[$a])
                    ->count();

                if ($bookingTExists == 0) {
                    if ($request->has('discount_type') == false) {
                        $serviceCommissionNewlyAddedPax = roundout((($rateGroups[$b]->rate + $rateGroups[$b]->additional_charge + $permitfeeAr[$b]) * $natFeesPercentage / 100), 2);
                        $serviceCommissionNewlyAddedPax = $tourists * $serviceCommissionNewlyAddedPax;
                        $serviceCommissionNewlyAddedTotal += $serviceCommissionNewlyAddedPax;
                    }
                    $newlyAddedTotal += $groupTotal;
                }
            }


            if ($total > 0) {
                if($request->payment_method == 'Card' && !$request->is('api/*')) {
                    try {
                        $month = substr($request->expiration, 0, 2);
                        $yearSet = substr($request->expiration, 3, 2);
                        $y = 20;
                        $year = $y . '' . $yearSet;
                        //validation for card
                        Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));
                        $method = PaymentMethod::create([
                            'type' => 'card',
                            'card' => [
                                'number' => $request->cardnumber,
                                'exp_month' => $month,
                                'exp_year' => $year,
                                'cvc' => $request->cvc,
                            ],
                        ]);
                    } catch (CardException $exception) {
                        Log::error($exception->getMessage());

                        DB::rollBack();

                        DB::table('tour_booking_history')
                            ->insert([
                                'tour_booking_id' => $request->bookingID,
                                'operation' => 'Error',
                                'details' => $exception->getMessage(),
                                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                'operation_by' => auth()->id(),
                                'operation_by_user_type' => $userType,
                                'updated_at' => Carbon::now(),
                                'created_at' => Carbon::now(),
                            ]);

                        throw new Exception($exception->getMessage());
                    }
                }
            }

            //This is for refund process
            $refundProcess = false;
            if ($total < 0) {
                $refundProcess = true;
            }

            $tourBooking = $this->tourBooking::find($request->bookingID);
            $totalTourists = $tourBooking->tourists + $addedPeopleCount - $deletedPeopleCount;

            $subtt = $total;
            if(!$refundProcess) {
                if ($request->has('discount_type')) {
                    if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                        $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                    } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                        $discountAmt = round($request->discount_fixed_money, 2);
                    }
                    $total -= $discountAmt;
                    $subtt -= $discountAmt;
                }
            }

            $checkaffiliate = null;
            if ($tourBooking->customer_id != 0) {
                $checkaffiliate = $this->customer::where('id', $tourBooking->customer_id)->first();
            }
            if (!$refundProcess) {
                if ($request->payment_method == "Card" && $checkaffiliate->is_affiliate == 'No') {
                    //This is for old bookings
                    if ($request->bookingID < $oldBookingID) {
                        $serviceCommission = $total * 6 / 100;
                        $total += $serviceCommission;
                        if ($newlyAddedTotal > 0) {
                            $newlyAddedTotal += $newlyAddedTotal * 6 / 100;
                        }
                    } else {
                        if ($serviceCommissionTotal > 0 && $deletedPeopleCount==0) { //This is the calculations if people only added
                            $serviceCommission = $serviceCommissionTotal;
                        } else {
                            $serviceCommission = roundout(($total * $natFeesPercentage / 100), 2); //This is the calculations if people only added and deleted both
                        }
                        $total += $serviceCommission;
                        if ($newlyAddedTotal > 0) {
                            if ($serviceCommissionNewlyAddedTotal > 0 && $deletedPeopleCount==0) { //This is the calculations if people only added
                                $newlyAddedTotal += $serviceCommissionNewlyAddedTotal;
                            } else {
                                $newlyAddedTotal += roundout(($newlyAddedTotal * $natFeesPercentage / 100), 2); //This is the calculations if people only added and deleted both
                            }
                        }
                    }
                }

                if ($request->payment_method == "Card" && $checkaffiliate->is_affiliate == 'Yes') {
                    if($request->bookingID > $oldBookingID) {
                        if ($serviceCommissionTotal > 0 && $deletedPeopleCount==0) {
                            $serviceCommission = $serviceCommissionTotal;
                        } else {
                            $serviceCommission = roundout(($total * $natFeesPercentage / 100), 2);
                        }
                        $total += $serviceCommission;
                        if ($newlyAddedTotal > 0) {
                            if ($serviceCommissionNewlyAddedTotal > 0 && $deletedPeopleCount==0) {
                                $newlyAddedTotal += $serviceCommissionNewlyAddedTotal;
                            } else {
                                $newlyAddedTotal += roundout(($newlyAddedTotal * $natFeesPercentage / 100), 2);
                            }
                        }
                    }
                }
            }

            $tt = $total = round($total, 2);
            $serviceCommission = round($serviceCommission, 2);
            $tourBookingUpdate = false;
            if ($tourBooking) {
                $partialRefunds = $this->tourBookingHistory::where('tour_booking_id',$request->bookingID)->where('operation','Refund')->whereIn('payment_method',['Partial Refund','100 % Refund','Full Refund minus Fees','Permitfees Refund',"Don't Refund"])->get();

                $bookingT = $this->bookingTransact::with('packageRate')
                    ->where('tour_booking_id', $request->bookingID)
                    ->get();

                $bookingTotal = 0;
                $refundedPermitFeesTotalAmount = 0;
                $refundedProcessingFeesTotal = 0;
                $serviceCommissionOldTotal = 0;
                // Calculate the booking amount because old bookings data have booking_total in tour_booking_master table is 0
                foreach ($bookingT as $tbTransaction) {
                    $rate = $tbTransaction->rate;
                    $permitFee = $tbTransaction->permit_fee;
                    if ($request->payment_method == 'Card' ||  $paymentMethod == 'Card') {
                        if ($request->bookingID < $oldBookingID) {
                            $procFee = 2.9;
                        } else {
                            $procFee = $tbTransaction->packageRate->processing_charge_percentage;
                        }
                    } else {
                        $procFee = 0;
                    }
                    $additionalCharge = $tbTransaction->additional_charge;
                    $tourists = $tbTransaction->tourists;
                    //Adding and Subtracting peoples count from booking_transact and calculations changed
                    if (isset($rateGroupWiseaddedPeopleArray[$tbTransaction->tour_package_rate_id]) && $rateGroupWiseaddedPeopleArray[$tbTransaction->tour_package_rate_id] > 0) {
                        $tourists = $tourists + $rateGroupWiseaddedPeopleArray[$tbTransaction->tour_package_rate_id];
                    }
                    if (isset($rateGroupWisedeletedPeopleArray[$tbTransaction->tour_package_rate_id]) && $rateGroupWisedeletedPeopleArray[$tbTransaction->tour_package_rate_id] > 0 && $tourists >= $rateGroupWisedeletedPeopleArray[$tbTransaction->tour_package_rate_id]) {
                        $tourists = $tourists - $rateGroupWisedeletedPeopleArray[$tbTransaction->tour_package_rate_id];
                    }
                    $refund_permitfee_status = ($tbTransaction->refund_permitfee_status != '') ? json_decode($tbTransaction->refund_permitfee_status) : '';
                    $refundedPermitFeesTotal = 0;

                    if(!empty($refund_permitfee_status) && $permitFee > 0) {
                        foreach ($refund_permitfee_status as $val) {
                            if(!isset($val->is_pax_added)){
                                if ($val->status == 'Y') {
                                    $refundedPermitFeesTotal += (isset($val->amount) && $val->amount > 0) ? $val->amount : 0;
                                    if ($request->bookingID < $oldBookingID) {
                                        $refundedProcessingFeesTotal += (isset($val->processing_fees) && $val->processing_fees > 0) ? $val->processing_fees : 0;
                                    }
                                }
                            }
                        }
                        $refundedPermitFeesTotalAmount += $refundedPermitFeesTotal;
                        $subTotal = (($rate + $permitFee + $additionalCharge) * $tourists) - $refundedPermitFeesTotal;
                    } else {
                        $subTotal = ($rate + $permitFee + $additionalCharge) * $tourists;
                    }

                    if ($request->bookingID > $oldBookingID) {
                        $serviceCommissionOldBt = roundout(($subTotal * $natFeesPercentage / 100), 2);
                        $serviceCommissionOldTotal += $serviceCommissionOldBt;
                    }
                    $bookingTotal += $procFee ? $subTotal + ($subTotal * ($procFee / 100)) : $subTotal;
                }

                if ($tourBooking->discount2_value>0 && $bookingTotal>0) {
                    $bookingTotal -= $tourBooking->discount2_value;
                }

                $serviceCommissionOld = 0;
                if ($request->payment_method == "Card"  && $checkaffiliate->is_affiliate=='No') {
                    if ($request->bookingID < $oldBookingID) {
                        $serviceCommissionOld = ($bookingTotal + $refundedPermitFeesTotalAmount + $refundedProcessingFeesTotal) * 6 / 100;
                        $bookingTotal += $serviceCommissionOld;
                    } else {
                        if ($serviceCommissionOldTotal > 0) {
                            $serviceCommissionOld = $serviceCommissionOldTotal;
                        } else {
                            $serviceCommissionOld = round((($bookingTotal + $refundedPermitFeesTotalAmount + $refundedProcessingFeesTotal) * $natFeesPercentage / 100), 2);
                        }
                        $bookingTotal += $serviceCommissionOld;
                    }
                }

                if ($request->payment_method == "Card"  && $checkaffiliate->is_affiliate == 'Yes' && $natFeesPercentage > 0 && $request->bookingID > $oldBookingID) {
                    if ($serviceCommissionOldTotal > 0) {
                        $serviceCommissionOld = $serviceCommissionOldTotal;
                    } else {
                        $serviceCommissionOld = round((($bookingTotal + $refundedPermitFeesTotalAmount + $refundedProcessingFeesTotal) * $natFeesPercentage / 100), 2);
                    }
                    $bookingTotal += $serviceCommissionOld;
                }

                //This is for adjust calculation of booking total on booking info page if partial refunds done
                if ($refundProcess) {
                    $bookingTotal += abs($total);
                }

                if (!empty($partialRefunds)) {
                    $partialRefundAmount = 0;
                    foreach($partialRefunds as $partialRefund){
                        $details = json_decode($partialRefund->details);
                        //This conditions is for not consider refunded transactions using refund button in booking info page
                        if ($partialRefund->payment_method == 'Partial Refund'){
                            if ((isset($details->deleted) && $details->deleted != '') || (isset($details->paymenttotal) && isset($details->refundtotal))) {
                                $partialRefundAmount += $details->amount;
                            }
                        } else {
                            $partialRefundAmount += $details->amount;
                        }
                        //Adjust amount of booking total with partial refunds when people deleted
                        if (($partialRefund->payment_method == 'Partial Refund' || $partialRefund->payment_method == 'Full Refund minus Fees') && isset($details->deleted) && $details->deleted != '' && isset($details->refund_keep_us) && $details->refund_keep_us > 0) {
                            $partialRefundAmount += $details->refund_keep_us;
                        }
                    }

                    $bookingTotal += $partialRefundAmount;
                }

                $bookingTotal = round($bookingTotal, 2);
                $transactionFirst = $this->transaction::where('status', 'Completed')->where('tour_booking_id', $tourBooking->id)->first();
                $paymentMethod = $transactionFirst->via;
                if ($refundProcess && $paymentMethod == 'Card') {
                    $updates = [
                        'tourists' => $totalTourists,
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'service_commission' => DB::raw('service_commission+' . ($request->payment_method == "Card" ? $serviceCommission : 0)),
                        'total' => DB::raw('total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'updated_at' => mysqlDT(),
                        'updated_by' => auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];
                } else {
                    $updates = [
                        'tourists' => $totalTourists,
                        'discount2_value' => DB::raw('discount2_value+' . $discountAmt),
                        'service_commission' => DB::raw('service_commission+' . ($request->payment_method == "Card" ? $serviceCommission : 0)),
                        'total' => DB::raw('total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'booking_total' => DB::raw('booking_total+' . (($request->payment_method == "Card") ? $total : $subtt)),
                        'updated_at' => mysqlDT(),
                        'updated_by' => auth()->id(),
                        'updated_by_user_type' => $userType,
                    ];
                }

                if (!empty($request->discount_percentage) || (!empty($tourBooking->discount2_percentage) && $tourBooking->discount2_percentage>0)) {
                    $oldDiscountedTotalWithoutServiceCharge = $tourBooking->total - $tourBooking->service_commission;
                    $originalTotalOld = $oldDiscountedTotalWithoutServiceCharge + $tourBooking->discount2_value;

                    $newDiscountedTotalWithoutServiceCharge = $total - $serviceCommission;
                    $originalTotalNew = $newDiscountedTotalWithoutServiceCharge + $discountAmt;

                    $newDiscountPercent = round(100 - (
                            ($oldDiscountedTotalWithoutServiceCharge + $newDiscountedTotalWithoutServiceCharge) * 100 /
                            ($originalTotalOld + $originalTotalNew)
                        ), 2);

                    $updates['discount2_percentage'] = $newDiscountPercent;
                }

                $tourBookingUpdate = $this->tourBooking::where('id', $request->bookingID)->update($updates);
            }

            for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
            }


            $addedPeoplesTexts = array();
            $deletedPeoplesTexts= array();
            if (!empty($tbTransact)) {
                foreach ($tbTransact as $tbltrans) {
                    $tbTransactexist = $this->bookingTransact::where('tour_package_rate_id', $tbltrans['tour_package_rate_id'])->where('tour_booking_id', $request->bookingID)->first();
                    $tourPackageRateGroup = $this->tourPackageRate::find($tbltrans['tour_package_rate_id']);

                    if (!empty($tbTransactexist) && isset($tbTransactexist->id)) {
                        if(($tbltrans['added']==true || $tbltrans['deleted']==true)) {
                            $statusInfo = [];
                            $updatesTransact = [];
                            if ($tbltrans['added'] == true) {
                                $addedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                                $updatesTransact = [
                                    'tourists' => DB::raw('tourists+' . $tbltrans['tourists']),
                                    'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                                    'total' => DB::raw('total+' . $tbltrans['total']),
                                    'updated_by' => auth()->id(),
                                    'updated_by_user_type' => $userType,
                                    'updated_at' => mysqlDT()
                                ];
                            } else if ($tbltrans['deleted'] == true) {
                                $deletedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                                $updatesTransact = [
                                    'tourists' => DB::raw('tourists-' . $tbltrans['tourists']),
                                    'permit_fee' => $tbltrans['permit_fee'] != 0 ? $tbltrans['permit_fee'] : $tbTransactexist->permit_fee,
                                    'total' => DB::raw('total-' . $tbltrans['total']),
                                    'updated_by' => auth()->id(),
                                    'updated_by_user_type' => $userType,
                                    'updated_at' => mysqlDT()
                                ];
                            }

                            if ($tbTransactexist->check_in_status != '') {
                                $statusInfo = json_decode($tbTransactexist->check_in_status);
                                for ($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                                    if (!isset($statusInfo[$b])) {
                                        $statusInfo[$b] = new \stdClass;
                                        $statusInfo[$b]->status = 'N';
                                    }
                                }
                                $updatesTransact['check_in_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';
                            }

                            if (isset($updatesTransact) && !empty($updatesTransact)) {
                                if ($request->payment_method == "Card") {
                                    $updatesTransact = $this->saveCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans, $tourPackageRateGroup);
                                } else {
                                    $updatesTransact = $this->saveNonCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans);
                                }
                                $this->bookingTransact::where('id', $tbTransactexist->id)->update($updatesTransact);
                            }
                        }
                    } else {
                        $addedPeoplesTexts[] = $tbltrans['rate_for'] . " person " . $tbltrans['tourists'];
                        unset($tbltrans['rate_for']);
                        unset($tbltrans['added']);
                        unset($tbltrans['deleted']);
                        DB::table('tour_bookings_transact')->insert($tbltrans);
                    }
                }
            }
            if($refundProcess || $request->payment_method != "Card") {
                DB::commit();
            }

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $paymentTotal = round($paymentTotal, 2);
            $refundTotal = round($refundTotal, 2);

            $operationTouristsChanged = [
                'tour_booking_id' => $tourBooking->id,
                'tourists' => $totalTourists,
                'operation' => 'Update',
                'details' => json_encode(['sub_operation' => 'Tourists Changed', 'discount_percentage' => $request->discount_percentage?:0, 'discount2_value' => $discountAmt,'added' => $addedPeoplesTexts,'deleted' =>$deletedPeoplesTexts,'addedTourists' => $addedPeopleCount,'deletedTourits'=>$deletedPeopleCount,'paymenttotal'=>$paymentTotal,'refundtotal'=>$refundTotal, 'created_by_with_name' => $created_by_with_name ], JSON_NUMERIC_CHECK),
                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                'operation_by' => auth()->id(),
                'operation_by_user_type' => $userType,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ];

            //This is for refund process
            if ($refundProcess) {
                $transactionFirst = $this->transaction::where('status', 'Completed')->where('tour_booking_id', $tourBooking->id)->first();
                $paymentMethod = $transactionFirst->via;
                if($paymentMethod=='Cash' || $paymentMethod=='Invoiced'){
                    $total = abs($total);
                    $transactionId = DB::table('transactions')
                        ->insertGetId([
                            'tour_booking_id' => $tourBooking->id,
                            'amount' => $total,
                            'transaction_number' => '',
                            'via' => $paymentMethod,
                            'created_at' => mysqlDT(),
                            'response_init' => '',
                            'response_end' => '',
                            'response_end_datetime' => '0000-00-00 00:00:00',
                            'status' => 'Refund',
                            'visitor' => $request->getClientIp(),
                            'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                            'created_by' => auth()->id(),
                            'created_by_user_type' => $userType,
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();
                    $historyId = DB::table('tour_booking_history')
                        ->insertGetId([
                            'tour_booking_id' => $tourBooking->id,
                            'tourists' => $totalTourists,
                            'operation' => 'Refund',
                            'details' => json_encode(['amount' => $total, 'payment_type' => $paymentMethod, 'paymenttotal' => $paymentTotal, 'refundtotal' => $refundTotal, 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Partial Refund'], JSON_NUMERIC_CHECK),
                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();
                    DB::table('tour_booking_history')->insert($operationTouristsChanged);
                    DB::commit();
                } else if($paymentMethod == 'Card') {
                    $total = abs($total);
                    if ($total > 0) {
                        if ($request->has('checked_transactions') && $request->checked_transactions != '') {
                            $transID = $request->checked_transactions;
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $stripe->refunds->create([
                                'payment_intent' => $request->checked_transactions,
                                'amount' => $total * 100,
                                'reverse_transfer' => true,
                            ]);
                        } else {
                            if ($transactionFirst->transaction_number != '') {
                                $transID = $transactionFirst->transaction_number;
                                $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                $stripe->refunds->create([
                                    'payment_intent' => $transactionFirst->transaction_number,
                                    'amount' => $total * 100,
                                    'reverse_transfer' => true,
                                ]);
                            }
                        }
                        $transactionId = DB::table('transactions')
                            ->insertGetId([
                                'tour_booking_id' => $tourBooking->id,
                                'amount' => $total,
                                'transaction_number' => $transID,
                                'via' => $paymentMethod,
                                'created_at' => mysqlDT(),
                                'response_init' => '',
                                'response_end' => '',
                                'response_end_datetime' => '0000-00-00 00:00:00',
                                'status' => 'Refund',
                                'visitor' => $request->getClientIp(),
                                'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                                'created_by' => auth()->id(),
                                'created_by_user_type' => $userType,
                                'created_at' => Carbon::now(),
                            ]);
                        DB::commit();
                        $historyId = DB::table('tour_booking_history')
                            ->insertGetId([
                                'tour_booking_id' => $tourBooking->id,
                                'tourists' => $totalTourists,
                                'operation' => 'Refund',
                                'details' => json_encode(['amount' => $total, 'payment_type' => $paymentMethod, 'paymenttotal' => $paymentTotal, 'refundtotal' => $refundTotal, 'created_by_with_name' => $created_by_with_name, 'refund_type' => 'Partial Refund'], JSON_NUMERIC_CHECK),
                                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                                'operation_by' => auth()->id(),
                                'operation_by_user_type' => $userType,
                                'updated_at' => Carbon::now(),
                                'created_at' => Carbon::now(),
                            ]);
                        DB::commit();
                        DB::table('tour_booking_history')->insert($operationTouristsChanged);
                        DB::commit();
                    }
                }

                RefundBookingService::run($transactionId ?? null, $historyId, $paymentMethod);
                // dispatch(new RefundBooking($tourBooking, $transactionId ?? null, $historyId, $request->all(), $paymentMethod));
                DB::commit();

                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                ];
                exit;
            } else {
                if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later' || $request->payment_method == 'Invoiced') {
                    $paymentMethod = ($request->payment_method == 'Pay Later' || $request->payment_method == 'Invoiced') ? 'Invoiced' : 'Cash';
                    $transactionId = DB::table('transactions')
                        ->insertGetId([
                            'tour_booking_id' => $tourBooking->id,
                            'amount' => !empty($request->split_value) ? $request->split_value : $subtt,
                            'transaction_number' => '',
                            'via' => $paymentMethod,
                            'created_at' => mysqlDT(),
                            'response_init' => '',
                            'response_end' => '',
                            'response_end_datetime' => '0000-00-00 00:00:00',
                            'payment_type' => !empty($request->split_value) ? 1 : 0,
                            'status' => 'Completed',
                            'visitor' => $request->getClientIp(),
                            'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                            'created_by' => auth()->id(),
                            'created_by_user_type' => $userType,
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();

                    $historyId = DB::table('tour_booking_history')
                        ->insertGetId([
                            'tour_booking_id' => $tourBooking->id,
                            'tourists' => $totalTourists,
                            'operation' => 'Payment',
                            'details' => json_encode(['amount' => !empty($request->split_value) ? $request->split_value : $subtt, 'payment_type' => $paymentMethod, 'paymenttotal' => $paymentTotal, 'refundtotal' => $refundTotal, 'created_by_with_name' => $created_by_with_name, 'payment_method' => !empty($request->split_value) ? 'Split Payment' : 'Full Payment'], JSON_NUMERIC_CHECK),
                            'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);
                    DB::commit();

                    // Below line is for to update tour booking history table
                    $isSplitOrFull = !empty($request->split_value) ? 'Split Payment' : 'Full Payment';
                    if ($paymentMethod === 'Cash') {
                        dispatch(new CashBooking($tourBooking, $transactionId, $historyId, $isSplitOrFull, $request->all() + ['is_pax_added' => true]));
                    } elseif ($paymentMethod === 'Invoiced') {
                        dispatch(new InvoicedBooking($tourBooking, $transactionId, $historyId, $isSplitOrFull, $request->all() + ['is_pax_added' => true]));
                    }

                    DB::table('tour_booking_history')->insert($operationTouristsChanged);
                    DB::commit();
                }


            }
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }

        $errors = [];

        $paymentIntentInfo = [
            'clientSecret' => '',
            'transactionId' => '',
        ];

        try {
            $booking = [
                'tour_d_t' => $info->date . ' ' . $info->time_from,
            ];

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $request->bookingID)
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $arEmailData = [
                'bookingId' => $request->bookingID,
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'custName' => $tourBooking->name,
                'custEmail' => $tourBooking->email,
                'custPhone' => $tourBooking->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($request->bookingID)]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $totalTourists,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later' || $paymentMethod=='Cash' || $paymentMethod=='Invoiced') {
                // TODO
                if(auth()->id()!=130) {
                    $tourBooking->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
                }
            }
        } catch (Exception $exception) {
            Log::error($exception);
            $errors[] = 'email';
        }

        if ($request->payment_method == 'Pay Later') {
            return [
                'bookingId' => $tourBooking->id,
                'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                'stripeId' => $paymentIntentInfo['clientSecret'],
                'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                'errors' => $errors,
            ];
        }

        $month = substr($request->expiration, 0, 2);
        $yearSet = substr($request->expiration, 3, 2);
        $y = 20;
        $year = $y . '' . $yearSet;
        $cardnumber = $request->cardnumber;
        $cvc = $request->cvv;
        $master = $this->tourBooking::where('id', $tourBooking->id)->first();

        if ($request->payment_method == 'Card') {
            $data = [
                'total' => $tt,
                'customer_id' => $master->customer_id,
                'customer_name' => $master->name,
                'customer_email' => $master->email,
                'customer_phone' => $master->phone_number,
                'tourists' => $totalTourists,
                'booking_id' => $tourBooking->id,
                'tour_package' => $info->package_name,
                'date' => $info->date,
                'time_from' => $info->time_from,
                'subtotal' => $subtt,
                'serviceCommission' => $serviceCommission,
                'month' => $month,
                'year' => $year,
                'cardnumber' => $cardnumber,
                'cvc' => $cvc,
                'destination' => $info->stripe_destination_id,
                'slot_time' => $request->slot_time,
                'tour_operator_id' => $info->tour_operator_id,
                'note' => $request->tour_operator_note,
                'booked_by_staff' => $request->booked_by_staff,
                'split_value' => $request->split_value,
                'api' => $request->api,
                'change_people'=>1,
                'discount_percentage' => $request->discount_percentage?$request->discount_percentage:0,
                'discount2_value' => $discountAmt,
                'addedpeoplesetexts' => $addedPeoplesTexts,
                'deletedpeoplestexts' =>$deletedPeoplesTexts,
                'addedtourists' => $addedPeopleCount,
                'deletedtourits'=>$deletedPeopleCount,
                'paymenttotal'=>$paymentTotal,
                'refundtotal'=>$refundTotal,
                'is_pax_added' => true
            ];

            if ($request->has('api') && $request->api=='1') {
                $paymentIntentId = ($request->has('payment_intent_id') && $request->payment_intent_id != '') ? $request->payment_intent_id : '';
                if ($paymentIntentId == '') {
                    throw new Exception('You must provide Stripe Payment Intent Id');
                }
                $data['payment_intent_id'] = $paymentIntentId;
                $paymentIntentInfo = $this->createBookingCard($data);
            } else {
                $paymentIntentInfo = $this->createPaymentIntent($data);
            }

            if (isset($paymentIntentInfo['nextAction'])) {
                $master->stripe_paymentintent_id = $paymentIntentInfo['clientSecret'];
                $master->save();
                return [
                    'bookingId' => $tourBooking->id,
                    'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'require3ds' => 1,
                    'nextAction' => $paymentIntentInfo['nextAction'],
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if ($paymentIntentInfo['clientSecret'] == '') {
                $errors[] = 'payment';
            }

            if (!empty($paymentIntentInfo['ex'])) {
                $errors_message = $paymentIntentInfo['ex']->getError()->message;
            }

        }

        return [
            'bookingId' => $tourBooking->id,
            'bookingToken' => Crypt::encryptString($tourBooking->id), // for security purpose
            'stripeId' => $paymentIntentInfo['clientSecret'],
            'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
            'errors' => $errors,
            'errors_message' => !empty($errors_message) ? $errors_message : '',
        ];
    }

    private function deletePeopleChangePeople($data)
    {
        try {
            if (isset($data['api']) && $data['api'] == 1) {
                if (isset($data['permit_fee'][0])) {
                    $data['permit_fee'] = explode(",", $data['permit_fee'][0]);
                }

                if (isset($data['rate_group'][0])) {
                    $data['rate_group'] = explode(",", $data['rate_group'][0]);
                }

                if (isset($data['tourists'][0])) {
                    $data['tourists']= explode(",", $data['tourists'][0]);
                }
            }

            $bookingID = $data['bookingID'];
            $oldBookingID = config('constants.old_bookings.booking_id');

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $bookingID)->first();

            $clientIp = $data['clientIp'];
            $requestFrom = $data['requestFrom'];
            $bookingT = DB::table('tour_bookings_transact AS tbt')
                ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                ->where('tbt.tour_booking_id', $data['bookingID'])
                ->orderByDesc('tpr.age_from')
                ->orderByDesc('tpr.age_to')
                ->get(['tpr.rate_for', 'tbt.tour_package_rate_id', 'tbt.tourists','tbt.total']);

            $rateGroupWiseTourists = array();
            $rateGroupWiseTotal = array();
            foreach ($bookingT as $value) {
                $rateGroupWiseTourists[$value->tour_package_rate_id] = $value->tourists;
                $rateGroupWiseTotal[$value->tour_package_rate_id] = $value->total;
            }

            $deletedPeopleCount = 0;
            $rateGroupWiseaddedPeople = array();
            $rateGroupWisedeletedPeople = array();
            $rateGroupWisedeletedPeopleArray = array();
            for ($a = 0, $ac = count($data['tourists']); $a < $ac; $a++) {
                $rateGroupWiseaddedPeople[$a] = 0;
                $rateGroupWisedeletedPeople[$a] = 0;

                if(isset($data['rate_group'][$a]) && isset($rateGroupWiseTourists[$data['rate_group'][$a]]) && $data['tourists'][$a] < $rateGroupWiseTourists[$data['rate_group'][$a]]){
                    $deletedPeopleCount += $rateGroupWiseTourists[$data['rate_group'][$a]] - $data['tourists'][$a];
                    $rateGroupWisedeletedPeople[$a] = (int)$rateGroupWiseTourists[$data['rate_group'][$a]] - $data['tourists'][$a];
                    $rateGroupWisedeletedPeopleArray[$data['rate_group'][$a]] = $rateGroupWisedeletedPeople[$a];
                }
            }

            //Validations
            if ($data['total_tourist'] == $deletedPeopleCount) {
                throw new Exception("You can not delete all records. Please use cancel booking option.");
            }

            $paymentTransCountForBooking = $this->transaction::where('tour_booking_id', $data['bookingID'])->where('status', 'Completed')->count();

            if($data['payment_type']=='Card' && $data['refund_Type'] != "fareharbor" && $data['is_cash'] == '' && $paymentTransCountForBooking > 1 && (isset($data['checked_transactions']) && $data['checked_transactions'] == '') && $data['refund_Type'] != 'dont_refund'){
                throw new Exception("You must need to select payment transaction to delete person.");
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';
            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            $refundedpermitarray = array();

            $checkarrays = array();
            $checkinstatusarray = array();
            $refundedpermitfeearray = array();

            foreach ($rateGroupWisedeletedPeopleArray as $key => $value) {
                $btset = $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)->first();
                $touristsCount = $btset->tourists-$value;
                //This is for removing indexes from the end
                for ($a = $touristsCount, $ac = $btset->tourists; $a < $ac; $a++) {
                    $checkarrays[$key][] = $a;
                }
                $checkinstatusarray[$key] = $btset->check_in_status;
                $refundedpermitfeearray[$key] = $btset->refund_permitfee_status;
            }

            $updatedCheckIns = array();
            $updatedRefundedPermitfees = array();

            if(!empty($checkarrays)){
                foreach ($checkarrays as $k=>$checkarray){
                    if (isset($checkinstatusarray[$k]) && $checkinstatusarray[$k]!='')
                        $checkinstatusInfo = json_decode($checkinstatusarray[$k]);
                    else
                        $checkinstatusInfo = [];

                    if (isset($refundedpermitfeearray[$k]) && $refundedpermitfeearray[$k]!='')
                        $refundpermitfeesstatusInfo = json_decode($refundedpermitfeearray[$k]);
                    else
                        $refundpermitfeesstatusInfo = [];

                    foreach ($checkarray as $l=>$m){
                        if(isset($checkinstatusInfo[$m]) && !empty($checkinstatusInfo[$m])){
                            unset($checkinstatusInfo[$m]);
                        }
                        if(isset($refundpermitfeesstatusInfo[$m]) && !empty($refundpermitfeesstatusInfo[$m])){

                            if(isset($refundpermitfeesstatusInfo[$m]->status) && $refundpermitfeesstatusInfo[$m]->status=='Y' && isset($refundpermitfeesstatusInfo[$m]->amount) && $refundpermitfeesstatusInfo[$m]->amount > 0){
                                $refundedpermitarray[$k][] = $m;
                            }

                            unset($refundpermitfeesstatusInfo[$m]);
                        }
                    }
                    $updatedCheckIns[$k] = $checkinstatusInfo;
                    $updatedRefundedPermitfees[$k] = $refundpermitfeesstatusInfo;
                }
            }

            //Check validations with calculations for multiple payments using card
            if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor" && $data['is_cash'] == '' && $paymentTransCountForBooking > 1 && (isset($data['checked_transactions']) && $data['checked_transactions'] != '')) {
                $refundTrans = $this->transaction::where('tour_booking_id', $bookingID)->where('transaction_number', $data['checked_transactions'])->where('status', 'Refund')->get(['id', 'amount']);
                $paymentTrans = $this->transaction::where('tour_booking_id', $bookingID)->where('transaction_number', $data['checked_transactions'])->where('status', 'Completed')->first(['id', 'amount']);
                $totalrefundedamount = 0;
                foreach ($refundTrans as $val) {
                    $totalrefundedamount += $val->amount;
                }
                //Validation check if payment transactions fully refunded or not
                if ($paymentTrans->amount - $totalrefundedamount <= 0) {
                    throw new Exception('Unable to refund booking. Selected transaction already fully refunded.Please select different one.');
                }
                $amountCapturedStripe = 0;
                $amountRefundedStripe = 0;
                $stripe = new StripeClient(
                    env('STRIPE_API_SECRET_KEY')
                );

                $paymentIntent = $stripe->paymentIntents->retrieve(
                    $data['checked_transactions'],
                    []
                );
                if(!empty($paymentIntent) && isset($paymentIntent->charges) && isset($paymentIntent->charges->data[0]) && !empty($paymentIntent->charges->data[0])){
                    $amountCapturedStripe = $paymentIntent->charges->data[0]->amount_captured;
                    $amountRefundedStripe =  ($paymentIntent->charges->data[0]->amount_refunded!='')?$paymentIntent->charges->data[0]->amount_refunded:0;
                }
                $totalamountforvalidation = 0;
                foreach ($rateGroupWisedeletedPeopleArray as $key => $value) {
                    $TourOperator = $this->tourOperator::where('id', $data['tour_operator_id'])->first();
                    $tourBooking = $this->tourBooking::where('id', $bookingID)->where('status', 'Booked')->first();
                    $customer = $this->customer::where('id', $tourBooking->customer_id)->first();

                    $rateCheck = $this->bookingTransact::where('tour_package_rate_id', $key)->where('tour_booking_id', $bookingID)->first();
                    if ($rateCheck->refund_permitfee_status != '' && isset($refundedpermitarray) && !empty($refundedpermitarray) && isset($refundedpermitarray[$key])) {
                        $touristrefundedpermitfeesalready = count($refundedpermitarray[$key]);
                        $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;

                        if ($data['payment_type'] === 'Card') {
                            $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                        } else {
                            $per = $rate * $rateCheck->additional_tax_percentage / 100;
                        }
                        $subtotal = ($rate + $per);
                        if ($data['payment_type'] === 'Card' && $rateCheck->processing_charge_percentage > 0) {
                            $subtotal = number_format((float)(($subtotal * $value) - (($touristrefundedpermitfeesalready * $rateCheck->permit_fee) + ($touristrefundedpermitfeesalready * (($rateCheck->permit_fee * $rateCheck->processing_charge_percentage) / 100)))), 2, '.', '');
                        } else {
                            $subtotal = number_format((float)(($subtotal * $value) - ($touristrefundedpermitfeesalready * $rateCheck->permit_fee)), 2, '.', '');
                        }
                    } else {
                        $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;
                        if ($data['payment_type'] === 'Card') {
                            $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                        } else {
                            $per = $rate * $rateCheck->additional_tax_percentage / 100;
                        }
                        $subtotal = ($rate + $per);
                        $subtotal = number_format((float)$subtotal * $value, 2, '.', '');
                    }
                    $totalamountforvalidation += $subtotal;

                    if($data['refund_Type'] == "partial_refund"){
                        if ($data['partial_Refund_value'] > $totalamountforvalidation) {
                            throw new Exception('Unable to refund booking. Please enter amount less than total.');

                        }
                    }
                }
                if($data['refund_Type'] != "partial_refund" && $data['refund_Type'] != "dont_refund"  && $amountCapturedStripe>0){
                    $remainingAmountStripe = $amountCapturedStripe/100 - $amountRefundedStripe/100;
                    if($totalamountforvalidation > $remainingAmountStripe){
                        throw new Exception('Unable to refund booking. Refund amount is greator than unrefunded amount for selected transaction. Please select different transaction.');
                    }
                }

                //Validation check for 100% refund when deleting persons
                if ($data['refund_Type']  == "100_refund" && ($totalamountforvalidation > 0)) {
                    $set_fee_amount = 0;
                    if ($bookingID < $oldBookingID) {
                        if ($customer->is_affiliate === "No") {
                            $set_fee_amount = ($totalamountforvalidation * 6) / 100;
                        }
                    } else {
                        if ($customer->is_affiliate === 'No') {
                            $set_fee_amount = ($totalamountforvalidation * $TourOperator->service_commission_percentage) / 100;
                        } else {
                            $set_fee_amount = ($totalamountforvalidation * $TourOperator->affiliate_processing_percentage) / 100;
                        }
                    }
                    $Refund100 = $totalamountforvalidation + $set_fee_amount;
                    $Refund100 = round($Refund100, 2);
                    $rounded = round($paymentTrans->amount - $totalrefundedamount - $Refund100,2);
                    if ($rounded < -0.05) {
                        throw new Exception('Unable to refund booking. Please select valid transaction or person to refund.');
                    }
                }

                //Validation check for full refund minus fees when deleting persons
                if ($data['refund_Type'] == "full_refund" && ($totalamountforvalidation > 0)) {
                    $Refund100MinusFees = round($totalamountforvalidation, 2);
                    if (($paymentTrans->amount - $totalrefundedamount - $Refund100MinusFees) < -0.05) {
                        throw new Exception('Unable to refund booking. Please select valid transaction or person to refund.');
                    }
                }
            }

            //Updating checkins json
            if (!empty($updatedCheckIns)) {
                foreach ($updatedCheckIns as $key=>$value){
                    if(!empty($value)){
                        $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)
                            ->update([
                                'check_in_status' => (!empty($value)) ? json_encode((array_values($value))) : ''
                            ]);
                    }
                }
            }

            //Updating refunded permit fee json
            if (!empty($updatedRefundedPermitfees)) {
                foreach ($updatedRefundedPermitfees as $key=>$value){
                    if(!empty($value)){
                        $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)
                            ->update([
                                'refund_permitfee_status' => (!empty($value)) ? json_encode(array_values($value)) : '',
                            ]);
                    }
                }
            }

            foreach ($rateGroupWisedeletedPeopleArray as $key => $value) {
                $TourOperator = $this->tourOperator::where('id', $data['tour_operator_id'])->first();
                $transaction = $this->transaction::where('tour_booking_id', $bookingID)->where('status', 'Completed')->first();
                $tourBooking = $this->tourBooking::where('id', $bookingID)->where('status', 'Booked')->first();
                $customer = $this->customer::where('id', $tourBooking->customer_id)->first();

                $rateCheckname = $this->tourPackageRate::where('id', $key)->first();
                $rateCheck = $this->bookingTransact::where('tour_package_rate_id', $key)->where('tour_booking_id', $bookingID)->first();
                if ($rateCheck->refund_permitfee_status != '' && isset($refundedpermitarray) && !empty($refundedpermitarray) && isset($refundedpermitarray[$key])) {
                    $touristrefundedpermitfeesalready = count($refundedpermitarray[$key]);
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;

                    if ($data['payment_type'] === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    if ($data['payment_type'] === 'Card' && $rateCheck->processing_charge_percentage > 0) {
                        $subtotal = number_format((float)(($subtotal * $value) - (($touristrefundedpermitfeesalready * $rateCheck->permit_fee) + ($touristrefundedpermitfeesalready * (($rateCheck->permit_fee * $rateCheck->processing_charge_percentage) / 100)))), 2, '.', '');
                    } else {
                        $subtotal = number_format((float)(($subtotal * $value) - ($touristrefundedpermitfeesalready * $rateCheck->permit_fee)), 2, '.', '');
                    }
                } else {
                    $rate = $rateCheck->rate + $rateCheck->additional_charge + $rateCheck->permit_fee;
                    if ($data['payment_type'] === 'Card') {
                        $per = $rate * ($rateCheck->additional_tax_percentage + $rateCheck->processing_charge_percentage) / 100;
                    } else {
                        $per = $rate * $rateCheck->additional_tax_percentage / 100;
                    }
                    $subtotal = ($rate + $per);
                    $subtotal = number_format((float)$subtotal * $value, 2, '.', '');
                }
                $total = $subtotal;

                $bt = $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)->first();

                $set_total = $bt->total - $total;
                $set_tourist = $bt->tourists - $value;

                $total_fee_amount = 0;
                if (!empty($transaction) && $transaction->via == "Card") {
                    if ($bookingID < $oldBookingID) {
                        if ($customer->is_affiliate === "No") {
                            $total_fee_amount = ($set_total * 6) / 100;
                        }
                    } else {
                        if ($customer->is_affiliate === 'No') {
                            $total_fee_amount = ($set_total * $TourOperator->service_commission_percentage) / 100;
                        } else {
                            $total_fee_amount = ($set_total * $TourOperator->affiliate_processing_percentage) / 100;
                        }
                    }
                }
                $total_fee_amount = number_format((float)$total_fee_amount, 2, '.', '');

                $set_fee_amount = 0;
                if ($transaction) {
                    $set_fee_amount = $tourBooking->service_commission - (float)$total_fee_amount;
                    $set_fee_amount = number_format((float)$set_fee_amount, 2, '.', '');
                }

                if ($data['refund_Type'] == "partial_refund"){
                    if ($total > 0 && $data['partial_Refund_value'] > $total) {
                        throw new Exception('Unable to refund booking. Please enter amount less than total.');
                    }
                }

                if ($set_tourist == 0) {
                    $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)
                        ->update([
                            'total' => $set_total,
                            'tourists' => $set_tourist,
                            'check_in_status'=>'',
                            'refund_permitfee_status'=>''
                        ]);
                } else {
                    $this->bookingTransact::where('tour_booking_id', $bookingID)->where('tour_package_rate_id', $key)
                        ->update([
                            'total' => $set_total,
                            'tourists' => $set_tourist
                        ]);
                }

                if (!empty($transaction)) {
                    $tourBooking_set_total = $tourBooking->total - $total;
                    $tourBookingset_tourist = $tourBooking->tourists - $value;
                    if ($transaction->via == "Card" && $data['refund_Type'] != "fareharbor") {
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $set_fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $set_fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $set_fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $set_fee_amount = number_format((float)$set_fee_amount, 2, '.', '');
                        $set_fee_amount = (float)$set_fee_amount;
                        if ($data['refund_Type'] == "dont_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission;
                        }
                        if ($data['refund_Type'] == "100_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission - $set_fee_amount;
                        }
                        if ($data['refund_Type'] == "full_refund") {
                            $tourBooking_set_total = $tourBooking->total - ($total + $set_fee_amount);
                            $service_com = $tourBooking->service_commission;
                        }
                        if ($data['refund_Type'] == "partial_refund") {
                            $service_com = $tourBooking->service_commission;
                        }
                        $tourBooking_set_total = number_format((float)$tourBooking_set_total, 2, '.', '');
                        $this->tourBooking::where('id', $bookingID)->update([
                            'total' => $tourBooking_set_total,
                            'tourists' => $tourBookingset_tourist,
                            'service_commission' => $service_com
                        ]);
                    } else {
                        $this->tourBooking::where('id', $bookingID)->update([
                            'total' => $tourBooking_set_total,
                            'tourists' => $tourBookingset_tourist
                        ]);
                    }
                } else {
                    $tourBooking = $this->tourBooking::where('id', $bookingID)->first();
                    $tourBooking_set_total = $tourBooking->total - $total;
                    $tourBookingset_tourist = $tourBooking->tourists - $value;
                    $this->tourBooking::where('id', $bookingID)->update([
                        'total' => $tourBooking_set_total,
                        'booking_total'=>$tourBooking_set_total,
                        'tourists' => $tourBookingset_tourist
                    ]);
                }

                if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor" && ($data['is_cash']=='')) {
                    if ($data['refund_Type'] == "partial_refund") {
                        $Partial_refund = (float)$data['partial_Refund_value'];
                        if ($rateCheck->rate > 0) {
                            if(isset($data['checked_transactions']) && $data['checked_transactions']!=''){
                                $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                $refund = $stripe->refunds->create([
                                    'payment_intent' => $data['checked_transactions'],
                                    'amount' => $Partial_refund * 100,
                                    'reverse_transfer' => true,
                                ]);
                            } else {
                                $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                                $refund = $stripe->refunds->create([
                                    'payment_intent' => $transaction->transaction_number,
                                    'amount' => $Partial_refund * 100,
                                    'reverse_transfer' => true,
                                ]);
                            }
                        }
                    }
                    if ($data['refund_Type'] == "full_refund" && ($total > 0)) {
                        if(isset($data['checked_transactions']) && $data['checked_transactions']!=''){
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $data['checked_transactions'],
                                'amount' => $total * 100,
                                'reverse_transfer' => true,
                            ]);
                        }else {
                            $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                            $refund = $stripe->refunds->create([
                                'payment_intent' => $transaction->transaction_number,
                                'amount' => $total * 100,
                                'reverse_transfer' => true,
                            ]);
                        }
                    }
                    if ($data['refund_Type'] == "100_refund" && ($total > 0)) {
                        $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $set_fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $set_fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $set_fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $Refund100 = $total + $set_fee_amount;
                        $Refund100 = round($Refund100, 2);
                        if(isset($data['checked_transactions']) && $data['checked_transactions']!=''){
                            $refundTrans = $this->transaction::where('tour_booking_id', $bookingID)->where('transaction_number',$data['checked_transactions'])->where('status', 'Refund')->get(['id','amount']);
                            $paymentTrans = $this->transaction::where('tour_booking_id', $bookingID)->where('transaction_number',$data['checked_transactions'])->where('status', 'Completed')->first(['id','amount']);
                            $totalrefundedamount = 0;
                            foreach ($refundTrans as $ky => $val) {
                                $totalrefundedamount += $val->amount;
                            }

                            $rounded = round($paymentTrans->amount - $totalrefundedamount - $Refund100,2);

                            if ($rounded <= -0.01) {
                                $stripe->refunds->create([
                                    'payment_intent' => $data['checked_transactions'],
                                    'amount' => ($Refund100 + $rounded) * 100,
                                    'reverse_transfer' => true,
                                ]);
                            } else {
                                $stripe->refunds->create([
                                    'payment_intent' => $data['checked_transactions'],
                                    'amount' => $Refund100 * 100,
                                    'reverse_transfer' => true,
                                ]);
                            }
                        } else {
                            $stripe->refunds->create([
                                'payment_intent' => $transaction->transaction_number,
                                'amount' => $Refund100 * 100,
                                'reverse_transfer' => true,
                            ]);
                        }
                    }
                }

                if ($data['refund_Type'] == "dont_refund") {
                    if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor") {
                        $fee_amount = 0;
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = $total;
                    } else {
                        $refund_keep_us = $total;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $fee_amount = 0;
                    $refund_type = "Don't Refund";
                }

                if ($data['refund_Type'] == "partial_refund") {
                    if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor") {
                        $fee_amount = 0;
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = $total - (float)$data['partial_Refund_value'];
                        $refund_keep_us = number_format((float)$refund_keep_us, 2, '.', '');
                        $fee_amount = 0;
                    } else {
                        $refund_keep_us = $total - (float)$data['partial_Refund_value'];
                        $refund_keep_us = number_format((float)$refund_keep_us, 2, '.', '');
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    if ($rateCheck->rate == 0) {
                        $data['partial_Refund_value'] = 0;
                    }
                    $total = number_format((float)$data['partial_Refund_value'], 2, '.', '');
                    $refund_type = "Partial Refund";
                }

                if ($data['refund_Type'] == "full_refund") {
                    if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor") {
                        $fee_amount = 0;
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $refund_keep_us = $fee_amount;
                        $fee_amount = 0;
                    } else {
                        $refund_keep_us = 0;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $refund_type = "Full Refund minus Fees";
                }

                if ($data['refund_Type'] == "fareharbor") {
                    $refund_keep_us = $total;
                    $refund_type = "Fareharbor";
                }

                if ($data['refund_Type'] == "100_refund") {
                    if ($data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor") {
                        $fee_amount = 0;
                        if ($bookingID < $oldBookingID) {
                            if ($customer->is_affiliate === "No") {
                                $fee_amount = ($total * 6) / 100;
                            }
                        } else {
                            if ($customer->is_affiliate === 'No') {
                                $fee_amount = ($total * $TourOperator->service_commission_percentage) / 100;
                            } else {
                                $fee_amount = ($total * $TourOperator->affiliate_processing_percentage) / 100;
                            }
                        }
                        $fee_amount = number_format((float)$fee_amount, 2, '.', '');
                        $total = $total + $fee_amount;
                        $refund_keep_us = 0;
                    } else {
                        $total = $total;
                        $refund_keep_us = 0;
                    }
                    if (empty($transaction)) {
                        $total = 0;
                    }
                    $refund_type = "100 % Refund";
                }

                if (isset($data['is_cash']) && $data['is_cash']!='') {
                    if ($data['is_cash']=='Cash') {
                        $refundVia = 'Cash';
                    } elseif ($data['is_cash']=='Wire Transfer') {
                        $refundVia = 'Check/Wire';
                    }
                } else {
                    $refundVia = $data['payment_type'];
                }

                $details = json_encode([
                    'amount' => $total,
                    'refund_keep_us' => $refund_keep_us,
                    'payment_type' => $refundVia,
                    'deleted' => $rateCheckname->rate_for . ' person ' . $value,
                    'created_by_with_name' => $created_by_with_name,
                    'refund_type' => $refund_type
                ], JSON_NUMERIC_CHECK);

                $bookingInfo = $this->tourBooking::find($bookingID);

                $TourBookingHistory = new TourBookingHistory;
                $TourBookingHistory->tour_booking_id = $bookingID;
                $TourBookingHistory->tourists = $bookingInfo->tourists;
                $TourBookingHistory->operation = "Refund";
                $TourBookingHistory->details = $details;
                $TourBookingHistory->operation_at = Carbon::now();
                $TourBookingHistory->operation_by = auth()->id();
                $TourBookingHistory->operation_by_user_type = $userType;
                $TourBookingHistory->save();

                if (!empty($transaction) && $data['payment_type'] == 'Card' && $data['refund_Type'] != "fareharbor") {
                    if (isset($data['is_cash']) && $data['is_cash']!='') {
                        if($data['is_cash']=='Cash') {
                            $refundVia = 'Cash';
                        }elseif($data['is_cash']=='Wire Transfer'){
                            $refundVia = 'Check/Wire';
                        }
                    } else {
                        $refundVia = $data['payment_type'];
                    }
                    $TS = $this->transaction::where('tour_booking_id', $bookingID)->where('status', 'Completed')->first();
                    if (isset($data['checked_transactions']) && $data['checked_transactions']!=''){
                        $TS1 = $this->transaction::where('tour_booking_id', $bookingID)->where('transaction_number', $data['checked_transactions'])->where('status', 'Completed')->first();
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $bookingID;
                        if ($data['refund_Type'] == "dont_refund") {
                            $Transaction->amount = 0.00;
                        }else {
                            $Transaction->amount = $total;
                        }
                        $Transaction->application_fee_id = $TS1->application_fee_id;
                        $Transaction->transaction_number = $TS1->transaction_number;
                        if ($data['refund_Type'] == "dont_refund") {
                            $Transaction->fee_amount = 0.00;
                        }else{
                            $Transaction->fee_amount = round($total * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                        }
                        $Transaction->via = $refundVia;
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $clientIp;
                        $Transaction->request_from = $requestFrom;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    } else {
                        $Transaction = new Transaction;
                        $Transaction->tour_booking_id = $bookingID;
                        if ($data['refund_Type'] == "dont_refund") {
                            $Transaction->amount = 0.00;
                        }else {
                            $Transaction->amount = $total;
                        }
                        $Transaction->application_fee_id = $TS->application_fee_id;
                        $Transaction->transaction_number = $TS->transaction_number;
                        if ($data['refund_Type'] == "dont_refund") {
                            $Transaction->fee_amount = 0.00;
                        }else {
                            $Transaction->fee_amount = round($total * $tourBookingTransaction->stripe_fees_percentage / 100, 2);
                        }
                        $Transaction->via = $refundVia;
                        $Transaction->response_init = '';
                        $Transaction->response_end = '';
                        $Transaction->response_end_datetime = '';
                        $Transaction->status = 'Refund';
                        $Transaction->visitor = $clientIp;
                        $Transaction->request_from = $requestFrom;
                        $Transaction->created_by = auth()->id();
                        $Transaction->created_by_user_type = $userType;
                        $Transaction->save();
                    }
                } elseif (!empty($transaction) && $data['payment_type'] == 'Cash') {
                    if (isset($data['is_cash']) && $data['is_cash'] != '') {
                        if ($data['is_cash'] == 'Cash') {
                            $refundVia = 'Cash';
                        } elseif ($data['is_cash'] == 'Wire Transfer') {
                            $refundVia = 'Check/Wire';
                        }
                    } else {
                        $refundVia = $data['payment_type'];
                    }
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $bookingID;
                    if ($data['refund_Type'] == "dont_refund") {
                        $Transaction->amount = 0.00;
                    } else {
                        $Transaction->amount = $total;
                    }
                    $Transaction->transaction_number = '';
                    $Transaction->fee_amount = '';
                    $Transaction->via = $refundVia;
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = '';
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $clientIp;
                    $Transaction->request_from = $requestFrom;
                    $Transaction->created_by = auth()->id();
                    $Transaction->created_by_user_type = $userType;
                    $Transaction->save();
                } elseif (!empty($transaction) && $data['refund_Type'] == "fareharbor") {
                    if (isset($data['is_cash']) && $data['is_cash']!='') {
                        if ($data['is_cash']=='Cash') {
                            $refundVia = 'Cash';
                        } elseif ($data['is_cash']=='Wire Transfer'){
                            $refundVia = 'Check/Wire';
                        }
                    } else {
                        $refundVia = $data['payment_type'];
                    }
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $bookingID;
                    if ($data['refund_Type'] == "dont_refund") {
                        $Transaction->amount = 0.00;
                    } else {
                        $Transaction->amount = $total;
                    }
                    $Transaction->transaction_number = '';
                    $Transaction->fee_amount = '';
                    $Transaction->via = $refundVia;
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = '';
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $clientIp;
                    $Transaction->request_from = $requestFrom;
                    $Transaction->created_by = auth()->id();
                    $Transaction->created_by_user_type = $userType;
                    $Transaction->save();
                }

                // Below line is for to update tour booking history table
                $this->handleDeletePersonHistoryUpdateJobs($tourBooking, $TourBookingHistory->id, $data, $refundVia, $Transaction->id ?? null);
            }

            return [
                'booking_id' => $bookingID,
                'success' => true,
                'message' => 'Booking is updated and refund will be get soon.'
            ];
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    public function saveCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans, $tourPackageRateGroup)
    {
        if ($tbTransactexist->refund_permitfee_status != '') {
            $statusInfo = json_decode($tbTransactexist->refund_permitfee_status);
            for($b = 0; $b < $tbTransactexist->tourists; $b++) {
                $statusInfo[$b] = $statusInfo[$b];
            }
            for($b = $tbTransactexist->tourists; $b < ($tbTransactexist->tourists + $tbltrans['tourists']); $b++) {
                if ($tbltrans['permit_fee']) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $statusInfo[$b]->is_pax_added = true;
                } elseif ($tbltrans['permit_fee'] == 0 && $tourPackageRateGroup->permit_fee == 0) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = '0.00';
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                } else {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = 8 * $tbltrans['processing_charge_percentage'] / 100;
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        } elseif ($tbTransactexist->permit_fee == 0) {
            for($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                $overrideStatusInfo = 0;
                if (!isset($statusInfo[$b])) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $statusInfo[$b]->is_pax_added = true;
                    $overrideStatusInfo = 1;
                }
                if ($overrideStatusInfo && $tbltrans['permit_fee'] == 0 && $tbTransactexist->tourists > $b) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = $tbTransactexist->tourists > $b ? 0 : (8 * $tbltrans['processing_charge_percentage'] / 100);
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
                if ($overrideStatusInfo && $tbltrans['permit_fee'] != 0 && $tbTransactexist->tourists > $b) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = 8 * $tbltrans['processing_charge_percentage'] / 100;
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        } elseif ($tbTransactexist->permit_fee != 0) {
            for($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                $overrideStatusInfo = 0;
                if (!isset($statusInfo[$b])) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $statusInfo[$b]->is_pax_added = true;
                    $overrideStatusInfo = 1;
                }
                if ($overrideStatusInfo && $tbltrans['permit_fee'] == 0 && $tbTransactexist->tourists <= $b) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = 8 * $tbltrans['processing_charge_percentage'] / 100;
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        }
    }

    public function saveNonCardPermitFeeJson($updatesTransact, $tbTransactexist, $tbltrans)
    {
        if ($tbTransactexist->refund_permitfee_status != '') {
            $statusInfo = json_decode($tbTransactexist->refund_permitfee_status);
            for($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                $overrideStatusInfo = 0;
                if (!isset($statusInfo[$b])) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $overrideStatusInfo = 1;
                }
                if ($overrideStatusInfo && $tbltrans['permit_fee'] == 0) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = '0.00';
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        } elseif ($tbTransactexist->permit_fee == 0) {
            for($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                $overrideStatusInfo = 0;
                if (!isset($statusInfo[$b])) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $overrideStatusInfo = 1;
                }
                if ($overrideStatusInfo && ($tbltrans['permit_fee'] == 0 || $tbTransactexist->tourists > $b)) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = '0.00';
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        } elseif ($tbTransactexist->permit_fee != 0) {
            for($b = 0; $b < ($tbltrans['tourists'] + $tbTransactexist->tourists); $b++) {
                $overrideStatusInfo = 0;
                if (!isset($statusInfo[$b])) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'N';
                    $overrideStatusInfo = 1;
                }
                if ($overrideStatusInfo && $tbltrans['permit_fee'] == 0 && $tbTransactexist->tourists <= $b) {
                    $statusInfo[$b] = new \stdClass;
                    $statusInfo[$b]->status = 'Y';
                    $statusInfo[$b]->amount = '8.00';
                    $statusInfo[$b]->processing_fees = '0.00';
                    $statusInfo[$b]->is_pax_added = true;
                    $statusInfo[$b]->dateTime = mysqlDT();
                }
            }

            $updatesTransact['refund_permitfee_status'] = (!empty($statusInfo)) ? json_encode((array_values($statusInfo))) : '';

            return $updatesTransact;
        }
    }

    /**
     * Handle the queue jobs to perform the history data updates when delete person from edit booking section
     *
     * @param  object  $tourBooking
     * @param  int     $historyId
     * @param  array   $request
     * @param  string  $refundVia
     * @param  int     $transactionId
     *
     * @return void
     */
    public function handleDeletePersonHistoryUpdateJobs($tourBooking, $historyId, $request, $refundVia, $transactionId): void
    {
        // Perform the jobs based on categories
        if ($request['refund_Type'] == "dont_refund") {
            dispatch(new DeletePersonDontRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_Type'] == "partial_refund" || $request['refund_Type'] == "fareharbor") {
            DeletePersonPartialRefundService::run($historyId, $request, $refundVia, $transactionId);
            // dispatch(new DeletePersonPartialRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_Type'] == "full_refund") {
            DeletePersonFullRefundService::run($historyId, $request, $refundVia, $transactionId);
            // dispatch(new DeletePersonFullRefund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        } elseif ($request['refund_Type'] == "100_refund") {
            DeletePerson100RefundService::run($tourBooking, $historyId, $request, $refundVia, $transactionId);
            // dispatch(new DeletePerson100Refund($tourBooking, $historyId, $request, $refundVia, $transactionId));
        }
    }

    /**
     * Get the group of people for new booking
     *
     * @param object $request
     * @return object
     */
    public function getGroupOfPeopleNewBook(object $request)
    {
        try {
            $fields = [
                'tst.id', 'tst.tour_slot_id', 'tst.time_from', 'tst.time_to', 'tsd.seats', 'tst.bookable_status',
                DB::raw("IFNULL((SELECT SUM(tbm.tourists)
                FROM tour_bookings_master AS tbm
                WHERE tbm.tour_slot_id = tst.id
                        AND tbm.status != 'Cancelled'
                GROUP BY tbm.tour_slot_id), 0) AS tourists"),
                DB::raw("IFNULL((SELECT SUM(tbm.tourists)
                FROM tour_bookings_master AS tbm
                JOIN customers AS c ON c.id = tbm.customer_id
                WHERE tbm.tour_slot_id = tst.id
                        AND c.is_affiliate = 'Yes'
                        AND tbm.status != 'Cancelled'
                GROUP BY tbm.tour_slot_id), 0) AS afl_tourists"),
            ];

            $slotTimes = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->where('tsd.date', $request->date) // separator is not necessary in date
                ->where('tsd.tour_package_id', $request->tour_package_id)
                ->where('tsd.status', 'Active')
                ->where('tst.deleted_by', '=', '0')
                ->orderBy('tst.time_from');

            $slotTimes = $slotTimes->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->where('tp.tour_operator_id', $request->tour_operator_id);

            $response = $slotTimes->get($fields)->toArray();

            if (!empty($response)) {
                $finalslotTimes = array();
                $touristsInfoData = array();
                foreach ($response as $key => $responseVal) {
                    $touristsInfo = DB::table('tour_package_rates AS tpr')
                        ->leftJoin('tour_bookings_transact AS tbt', 'tbt.tour_package_rate_id', '=', 'tpr.id')
                        ->join('tour_bookings_master AS tbm', 'tbm.id', '=', 'tbt.tour_booking_id')
                        ->where('tbm.tour_slot_id', $responseVal->id)
                        ->whereIn('tbm.status', ['Booked', 'Completed'])
                        ->groupBy('tbt.tour_package_rate_id')
                        ->orderByDesc('tpr.age_from')
                        ->select(['tpr.rate_for', DB::raw('SUM(tbt.tourists) AS tourists')]);
                    $touristsInfo = $touristsInfo->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                        ->where('tp.tour_operator_id', $request->tour_package_id);
                    $touristsInfoData = $touristsInfo->get();

                    if ($responseVal->tourists > $responseVal->seats) {
                        $answer = abs($responseVal->tourists - $responseVal->seats);
                    } else {
                        $answer = 0;
                    }

                    $finalslotTimes[$key]['id']              = $responseVal->id;
                    $finalslotTimes[$key]['tour_slot_id']    = $responseVal->tour_slot_id;
                    $finalslotTimes[$key]['time_from']       = $responseVal->time_from;
                    $finalslotTimes[$key]['time_to']         = $responseVal->time_to;
                    $finalslotTimes[$key]['seats']           = $responseVal->seats;
                    $finalslotTimes[$key]['tourists']        = $responseVal->tourists;
                    $finalslotTimes[$key]['overbooked ']     = $answer;
                    $finalslotTimes[$key]['bookable_status'] = $responseVal->bookable_status;
                    $finalslotTimes[$key]['afl_tourists']    = $responseVal->afl_tourists;
                    $finalslotTimes[$key]['touristsInfo']    = $touristsInfoData;
                }

                $rateGroups = DB::table('tour_package_rates AS tpr')
                    ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                    ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                    ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                    ->join('states AS st', 'st.id', '=', 'ct.state_id')
                    ->where([
                        'tpr.tour_package_id' => $request->tour_package_id,
                        'tpr.deleted_by' => 0
                    ])
                    ->when($request->affiliateId, function ($q) use ($request) {
                        return $q->where('tpr.affiliate_id', $request->affiliateId);
                    })
                    ->when($request->affiliateId == 0, function ($q) use ($request) {
                        return $q->whereNull('tpr.affiliate_id');
                    })
                    ->orderBy('tpr.id')
                    ->select([
                        'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.description',
                        'tpr.additional_tax_percentage', 'tpr.processing_charge_percentage', 'tpr.additional_charge',
                        'tpr.permit_fee',
                        't_o.service_commission_percentage'
                    ]);

                $rateGroups = $rateGroups->where('t_o.id', $request->tour_operator_id);
                $rateGroups = $rateGroups->get();


                if (!empty($finalslotTimes)) {
                    return ['slotTimes' => $finalslotTimes, 'rateGroups' => $rateGroups];
                } else {
                    return ['slotTimes' => $slotTimes->get($fields), 'rateGroups' => $rateGroups];
                }
            }
            throw new Exception('Group of people details not available.');
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Booking agenda with/without day time slots selection
     *
     * @param int $date
     * @param int $tourOperatorId
     * @param string $tourPackagesId
     * @param int $PaymentStatus
     * @param string $slotId
     * @param int $api
     * @return array
     */
    public function dayTimeSlots(int $date = 0, int $tourOperatorId = 0, string $tourPackagesId = '', int $PaymentStatus = 0, $slotId = '',int $api = 0)
    {

        try {
            if ($PaymentStatus == 2)
                $PaymentStatus = "Completed";

            if ($PaymentStatus == 3)
                $PaymentStatus = "Failed";

            $vars = [
                'page_title' => 'Tour Booking Schedules',
                'pageId' => 'date bookings',
                'tourOperatorId' => $tourOperatorId,
                'tourPackagesId' => (int) $tourPackagesId,
                'PaymentStatus' => $PaymentStatus,
                'slotId' => $slotId
            ];

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            if ($userType == 'Admin') {
                $tourOperators = $this->tourOperator::where('status', 'Active')
                    ->orderBy('name')
                    ->get(['id', 'name']);

                $vars['tourOperators'] = $tourOperators;

                $tourPackages = $this->tourPackage::where('status', 'Active')
                    ->where('tour_operator_id', $tourOperatorId)
                    ->orderBy('name')
                    ->get(['id', 'name','stripe_destination_id']);

                $vars['tourPackages'] = $tourPackages;

                if ($vars['tourOperatorId'] == 0) {
                    if (Cookie::has('tourOpId')) {
                        $vars['tourOperatorId'] = Cookie::get('tourOpId');
                    } elseif (count($tourOperators)) {
                        $vars['tourOperatorId'] = $tourOperators[0]->id;
                    }
                }
            } else {
                $vars['tourOperatorId'] = auth()->user()->tour_operator_id;
            }

            $vars['tourOperatorId'] = (int)$vars['tourOperatorId'];

            if ($date == 0) {
                $date = date('Y-m-d');
            } else {
                $date = substr($date, 0, 4) . '-' . substr($date, 4, 2) . '-' . substr($date, 6, 2);
            }

            $vars['date'] = $date;

            $tourPackages = $this->tourPackage::where('status', 'Active')
                ->where('tour_operator_id', $vars['tourOperatorId'])
                ->orderBy('name')
                ->get(['id', 'name','stripe_destination_id']);

            $vars['tourPackages'] = $tourPackages;

            $vars['dayBookings'] = DB::table('tour_bookings_master AS tbm')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                ->leftJoin('transactions as t', 't.tour_booking_id', '=', 'tbm.id')
                ->where('tsd.date', $date)
                ->where('tbm.status', 'Booked');

            $paymentSql = 'select `tbh`.`details`, `tbh`.`operation_at`, `tbh`.`tour_booking_id` from `tour_bookings_master` as `tbm`
				inner join `tour_slot_times` as `tst` on `tst`.`id` = `tbm`.`tour_slot_id`
				inner join `tour_slot_dates` as `tsd` on `tsd`.`id` = `tst`.`tour_slot_id`
				inner join `tour_packages` as `tp` on `tp`.`id` = `tsd`.`tour_package_id`
				inner join `tour_booking_history` as `tbh` on `tbh`.`tour_booking_id` = `tbm`.`id`
				where `tsd`.`date` = "'.$date.'" and `tbm`.`status` = "Booked" and `tbh`.`operation` = "Payment"';

            $vars['Payment'] = DB::select(DB::raw($paymentSql));

            $paymentBookingIDs = array();
            if(!empty($vars['Payment'])) {
                foreach ($vars['Payment'] as $value) {
                    $paymentBookingIDs[] = $value->tour_booking_id;
                }
            }

            if(!empty($paymentBookingIDs)) {
                $refundCancelSql = 'select `tbh`.`tour_booking_id`,
					ifnull(sum(json_extract(`tbh`.`details`, "$.amount")), 0) as TotalRefund
					from `tour_booking_history` as `tbh`
					inner join `tour_bookings_master` as `tbm` on `tbm`.`id` = `tbh`.`tour_booking_id`
					where `tbm`.`status` = "Booked"
					and `tbh`.`tour_booking_id` in (' . implode(",", $paymentBookingIDs) . ')
					and (`tbh`.`operation` = "Refund" or `tbh`.`operation`= "Cancelled")
					and (
						not JSON_CONTAINS(
							`tbh`.`details`, json_quote("Don\'t Refund"), "$.refund_type"
						)
						and not JSON_CONTAINS(
							`tbh`.`details`, json_quote("don\'t refund"), "$.refund_type"
						)
						and not JSON_CONTAINS(
							`tbh`.`details`, json_quote("fareharbor : don\'t refund"), "$.refund_type"
						)
					)
					group by `tbh`.`tour_booking_id`';

                $vars['Refund_Cancel1'] = DB::select(DB::raw($refundCancelSql));
            } else {
                $vars['Refund_Cancel1']= [];
            }

            if ($PaymentStatus) {
                if ($PaymentStatus == 'Failed') {
                    $vars['dayBookings'] = DB::table('tour_bookings_master AS tbm')
                        ->leftJoin('tour_booking_history AS tbh', 'tbh.tour_booking_id', '=', 'tbm.id')
                        ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                        ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                        ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                        ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                        ->leftJoin('transactions as t', 't.tour_booking_id', '=', 'tbm.id')
                        ->where('tsd.date', $date)
                        ->whereNotIn('tbm.id', static function ($q) use ($date) {
                            return $q->select('tour_booking_id')->from('transactions')
                                ->join('tour_bookings_master AS tbm', 'tbm.id', '=', 'transactions.tour_booking_id')
                                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                                ->where('tsd.date', $date)
                                ->groupBy('tour_booking_id')
                                ->having(DB::raw('count(`transactions`.`id`)'), '>', 0);
                        })
                        ->where('tbm.status', 'Booked');
                } else {
                    $vars['dayBookings'] = $vars['dayBookings']->where('t.status', $vars['PaymentStatus']);
                }
            }

            $vars['dayBookings'] = $vars['dayBookings']->where('tp.tour_operator_id', $vars['tourOperatorId']);

            if ($tourPackagesId && $tourPackagesId !== '0') {
                $vars['dayBookings'] = $vars['dayBookings']->where('tp.id', $vars['tourPackagesId']);
            }

            if ($slotId) {
                $vars['dayBookings'] = $vars['dayBookings']->where('tst.id', $vars['slotId']);
            }

            $vars['dayBookings'] = $vars['dayBookings']->groupBy('tbm.id')->orderBy('tst.time_from')
                ->orderBy('tp.name')
                ->orderBy('c.name', 'asc')
                ->get([
                    'tbm.id AS booking_id', 'tbm.tour_slot_id', 'tbm.tourists', 'tbm.comments',
                    'tbm.note', 'tbm.total', 'tbm.liability_waiver_signing', 'tbm.created_by_user_type',
                    'tsd.seats', 'tst.time_from', 'tst.time_to',
                    'tp.id AS tour_package_id', 'tp.name AS tour_package_name',
                    'tbm.name AS customer_name', 'tbm.email', 'tbm.phone_number', 'c.is_affiliate','tbm.booking_total',
                    DB::raw("IFNULL((SELECT SUM(amount)
						FROM transactions AS t
						WHERE t.tour_booking_id = tbm.id
						AND t.status = 'Completed'
						GROUP BY t.tour_booking_id), 0) AS paids"
                    ),
                    DB::raw('IFNULL((SELECT SUM(json_extract(`tbh`.`details`, "$.amount"))
						FROM tour_booking_history AS tbh
						WHERE tbh.tour_booking_id = tbm.id
						AND
						(
							`tbh`.`operation` = "Refund"
							or `tbh`.`operation` = "Cancelled"
						)
						and (
							not JSON_CONTAINS(
								`tbh`.`details`, json_quote("Don\'t Refund"), "$.refund_type"
							)
							and not JSON_CONTAINS(
								`tbh`.`details`, json_quote("don\'t refund"), "$.refund_type"
							)
							and not JSON_CONTAINS(
								`tbh`.`details`, json_quote("fareharbor : don\'t refund"), "$.refund_type"
							)
						)
						GROUP BY tbh.tour_booking_id), 0) AS refund'
                    ),
                ]);

            foreach ($vars['dayBookings'] as $dayBookingValues) {
                $counter = 0;
                if ($dayBookingValues->liability_waiver_signing != "") {
                    $convertArray = json_decode($dayBookingValues->liability_waiver_signing, true);
                    $i = 0;
                    foreach ($convertArray as $convertArrayValues) {
                        if ($convertArray[$i][0] != "") {
                            $counter += 1;
                        }
                        $i++;
                    }
                }

                $dayBookingValues->counter = $counter;
            }

            $vars['dayBookingsInfo'] = [];

            for ($a = 0, $ac = count($vars['dayBookings']); $a < $ac; $a++) {

                if ($api != '' && $api == 1) {
                    $checkinsSql = 'select `tbt`.`tourists`, `tbt`.`check_in_status`, `tpr`.`rate_for` from `tour_bookings_transact` as `tbt`
						inner join `tour_package_rates` as `tpr` on `tpr`.`id` = `tbt`.`tour_package_rate_id`
						where `tbt`.`tour_booking_id` = "'.$vars['dayBookings'][$a]->booking_id.'"';

                    $checkins = DB::select(DB::raw($checkinsSql));

                    if ($checkins) {
                        foreach ($checkins as $k=>$checkin) {
                            $decoded = '';
                            $decoded = json_decode($checkin->check_in_status,true);
                            $checkins[$k]->check_in_status = $decoded;
                        }
                    }

                    $vars['dayBookingsInfo']['b-' . $vars['dayBookings'][$a]->booking_id] = $checkins;
                } else {
                    $vars['dayBookingsInfo']['b-' . $vars['dayBookings'][$a]->booking_id] = DB::table('tour_bookings_transact AS tbt')
                        ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                        ->where('tbt.tour_booking_id', $vars['dayBookings'][$a]->booking_id)
                        ->get(['tbt.tourists', 'tbt.check_in_status', 'tpr.rate_for']);
                }
            }

            $cancelledBookingSql = 'select ifnull(sum(tbm.total), 0) as amount from `tour_bookings_master` as `tbm`
				inner join `tour_slot_times` as `tst` on `tst`.`id` = `tbm`.`tour_slot_id`
				inner join `tour_slot_dates` as `tsd` on `tsd`.`id` = `tst`.`tour_slot_id`
				inner join `tour_packages` as `tp` on `tp`.`id` = `tbm`.`tour_package_id`
				left join `transactions` as `t` on `t`.`tour_booking_id` = `tbm`.`id`
                where `tbm`.`status` = "Cancelled" and `t`.`status` = "Completed" and `tsd`.`date` = "'.$date.'"';

            if ($tourOperatorId == null) {
                $tourOperatorId = $vars['tourOperatorId'];
            }

            if ($tourPackagesId && $tourPackagesId !== 0) {
                $cancelledBookingSql .= ' and `tbm`.`tour_package_id` = "'.$tourPackagesId.'" ';
            }

            if ($tourOperatorId && $tourOperatorId !== 0) {
                $cancelledBookingSql .= ' and `tp`.`tour_operator_id` = "'.$tourOperatorId.'" ';
            }

            $cancelledBookingResult = DB::select(DB::raw($cancelledBookingSql));

            $vars['cancelledBookingAmount'] = (isset($cancelledBookingResult[0]->amount)) ? (float)$cancelledBookingResult[0]->amount : 0;

            if ($api != '' && $api == 1) {
                $dayBookings = $vars['dayBookings'];

                $allBookedTotal = 0;
                $allRefundTotal = 0;
                $allPaymentTotal =0;
                $totalTourits = 0;
                $tourPackageId = 0;
                $slotId = 0;
                $seatsInfo = [];
                $sortedData = array();
                if (!empty($dayBookings)) {
                    $Booking_Total = [];
                    $Booking_Total1= [];
                    foreach($vars['Payment']  as $Payment_Total) {
                        $Booking_Total[] = json_decode($Payment_Total->details);
                        $Booking_Total1[] = array(
                            "detail"=>json_decode($Payment_Total->details),
                            "tour_booking_id"=>$Payment_Total->tour_booking_id
                        );
                    }

                    $Refund_Total = [];
                    if (!empty($vars['Refund_Cancel1'])) {
                        foreach($vars['Refund_Cancel1'] as $Refund_T) {
                            $Refund_Total[] = $Refund_T;
                        }
                    }

                    foreach ($dayBookings as $key=>$dayBooking) {
                        $sortedData[$dayBooking->tour_slot_id]['slotTime'] = $dayBooking->time_from." - ".$dayBooking->time_to;
                        if (isset($vars['dayBookingsInfo']['b-'.$dayBooking->booking_id])) {
                            $dayBookings[$key]->dayBookings = $vars['dayBookingsInfo']['b-'.$dayBooking->booking_id];
                        } else {
                            $dayBookings[$key]->dayBookings = array();
                        }
                        $dayBookings[$key]->bookingtotal = 0;
                        $dayBookings[$key]->totalrefund = 0;
                        $dayBookings[$key]->booking_Total = 0;

                        if (!empty($Booking_Total1)) {
                            foreach ($Booking_Total1 as $bt1) {
                                if (isset($bt1['tour_booking_id']) && $bt1['tour_booking_id'] == $dayBooking->booking_id) {
                                    if (!empty($bt1['detail']->amount)) {
                                        $dayBookings[$key]->bookingtotal += (float)$bt1['detail']->amount;
                                    }
                                }
                            }
                        }

                        foreach($Refund_Total as $rc1) {
                            if (isset($rc1->tour_booking_id) && $rc1->tour_booking_id==$dayBooking->booking_id){
                                $dayBookings[$key]->totalrefund = $rc1->TotalRefund;
                            }
                        }

                        $sortedData[$dayBooking->tour_slot_id]['slotArray'][] = $dayBooking;
                        foreach ($sortedData[$dayBooking->tour_slot_id]['slotArray'] as $key => $value) {
                            if ($value->booking_total != "0.00") {
                                $value->total = $value->booking_total;
                            }
                            $value->paid = 0;
                            $value->paid = (string)($value->bookingtotal - $value->refund);
                            $value->refund = (string)$value->refund;
                        }
                    }

                    for($i = 0, $j = -1, $ic = count($dayBookings); $i < $ic; $i++) {
                        $dayBookings[$i]->bookingtotal = 0;
                        $dayBookings[$i]->totalrefund =0;

                        foreach($Refund_Total as $rc1) {
                            if (isset($rc1->tour_booking_id) && $rc1->tour_booking_id==$dayBookings[$i]->booking_id){
                                $dayBookings[$i]->totalrefund = $rc1->TotalRefund;
                            }
                        }

                        foreach($Booking_Total1 as $bt1) {
                            if (isset($bt1['tour_booking_id']) && $bt1['tour_booking_id']==$dayBookings[$i]->booking_id){
                                if (!empty($bt1['detail']->amount)){
                                    $dayBookings[$i]->bookingtotal += (float)$bt1['detail']->amount;
                                }
                            }
                        }

                        if($dayBookings[$i]->tour_package_id != $tourPackageId || $dayBookings[$i]->tour_slot_id != $slotId) {
                            $tourPackageId = $dayBookings[$i]->tour_package_id;
                            $slotId = $dayBookings[$i]->tour_slot_id;
                            $seatsInfo[++$j] = [
                                'slotId' => $slotId,
                                'seatsCount' => $dayBookings[$i]->seats,
                                'bookingsCount' => 0,
                                'touristsCount' => 0,
                                'amtUnpaid' => 0,
                                'amtPaid' => 0,
                                'amtTotal' => 0,
                                'bookingTotal' => 0,
                                'refundTotal' => 0,
                                'paymentTotal' => 0,
                                'Refund_Total' => 0,
                                'booking_total' => 0,
                            ];
                        }

                        $seatsInfo[$j]['bookingsCount']++;
                        $seatsInfo[$j]['touristsCount'] += $dayBookings[$i]->tourists;
                        $seatsInfo[$j]['amtUnpaid'] += $dayBookings[$i]->total - $dayBookings[$i]->paid ;
                        $seatsInfo[$j]['amtPaid'] += $dayBookings[$i]->paid;
                        $seatsInfo[$j]['amtTotal'] = $dayBookings[$i]->total;
                        $seatsInfo[$j]['booking_total'] += $dayBookings[$i]->booking_total;

                        if ($dayBookings[$i]->booking_total != 0) {
                            $seatsInfo[$j]['bookingTotal'] += $dayBookings[$i]->booking_total;
                        } elseif ($dayBookings[$i]->bookingtotal == 0) {
                            $seatsInfo[$j]['bookingTotal'] += $dayBookings[$i]->total;
                        } else {
                            $seatsInfo[$j]['bookingTotal'] += $dayBookings[$i]->bookingtotal;
                        }

                        $seatsInfo[$j]['refundTotal'] += $dayBookings[$i]->refund;
                        $seatsInfo[$j]['paymentTotal'] += $dayBookings[$i]->bookingtotal - $dayBookings[$i]->totalrefund;
                    }

                    if (!empty($seatsInfo)) {
                        foreach ($seatsInfo as $seatInfo) {
                            $allBookedTotal+=$seatInfo['bookingTotal'];
                            $allRefundTotal+=$seatInfo['refundTotal'];
                            $allPaymentTotal+=$seatInfo['paymentTotal'];
                            $totalTourits+=$seatInfo['touristsCount'];
                        }
                    }
                }

                $vars['allBookedTotal'] = number_format((float)$allBookedTotal, 2, '.', '');
                $vars['allRefundTotal'] = number_format((float)$allRefundTotal, 2, '.', '');
                $vars['allPaymentTotal'] = number_format((float)$allPaymentTotal, 2, '.', '');
                $vars['totalTourits'] = $totalTourits;

                $newSortedData = array();
                if (!empty($sortedData)) {
                    $i = 0;
                    foreach ($sortedData as $key => $sortdata) {
                        $sortedData[$key]['availableSeats'] = (isset($seatsInfo[$i]) && isset($seatsInfo[$i]['seatsCount']) && isset($seatsInfo[$i]['touristsCount']))?(string)($seatsInfo[$i]['seatsCount']-$seatsInfo[$i]['touristsCount']):"0";
                        $sortedData[$key]['seats'] = (isset($seatsInfo[$i]) && isset($seatsInfo[$i]['seatsCount']))?(string)($seatsInfo[$i]['seatsCount']):"0";
                        $newSortedData[$i] = $sortedData[$key];
                        $i++;
                    }
                }

                unset($vars['dayBookings']);
                unset($vars['dayBookingsInfo']);
                unset($vars['Payment']);
                unset($vars['Refund_Cancel']);
                unset($vars['Refund_Cancel1']);

                $vars['data'] = $newSortedData;
            }

            return $vars;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Generate stripe token for payment
     * @return array
     */

    public function stripeGenerateToken()
    {
        try {
            // Set your secret key. Remember to switch to your live secret key in production.
            // See your keys here: https://dashboard.stripe.com/apikeys
            $stripe = new StripeClient(
                env('STRIPE_API_SECRET_KEY')
            );

            // In a new endpoint on your server, create a ConnectionToken and return the
            // `secret` to your app. The SDK needs the `secret` to connect to a reader.
            $connectionToken = $stripe->terminal->connectionTokens->create([]);

            $tokenData = [
                'secret' => $connectionToken->secret
            ];

            return $tokenData;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Create stripe payment intent for booking payment
     *
     * @param object $request
     * @return array
     */
    public function stripeCreatePaymentIntent($request)
    {
        try {
            Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));

            $tourOperator = $this->tourOperator::find($request->tour_operator_id);

            $tourBookingTransaction = $this->bookingTransact::where('tour_booking_id', $request->booking_id)->first();

            $convertedAmount = $request->amount / 100;
            $destinationAmount = ($convertedAmount - ($convertedAmount * $tourBookingTransaction->stripe_fees_percentage / 100));

            $destinationAmount = $convertedAmount - $destinationAmount;
            $destinationAmount = round($destinationAmount, 2);
            $tourOperatorName = '';
            if ($request->tour_operator_id == 1) {
                $tourOperatorName = "Dixies Lower Antelope";
            }

            if ($request->tour_operator_id == 2) {
                $tourOperatorName = "DD MTA";
            }

            if ($request->tour_operator_id == 3) {
                $tourOperatorName = "John Rambo Adventures";
            }

            if ($request->tour_operator_id == 4) {
                $tourOperatorName = "L.A.C.T. Shuttle LLC";
            }

            if ($request->booking_id < config('constants.old_bookings.booking_id')) {
                $tourOperator->stripe_destination_id = 'acct_1KiPPfF2s9WqF2Qr';
            }

            if ($request->booking_id && $request->card_reader) {
                $customer = $this->customer::where('email', $request->customer_email)->first();
                if ($customer->stripe_id == null) {
                    $stripeCustomer = StripeCustomer::create([
                        'name' => $customer->name,
                        'email' => $customer->email,
                        'phone' => $customer->phone_number
                    ]);

                    $customer->stripe_id = $stripeCustomer->id;
                    $customer->save();
                }

                $paymentIntent = PaymentIntent::create([
                    'payment_method_types' => ['card_present'],
                    'capture_method' => 'manual',
                    'currency' => $request->currency,
                    'amount' => $convertedAmount * 100,
                    'description' => 'Booking Id: ' . $request->booking_id .
                        "\nTour Package: " . $request->tour_package .
                        "\nTour Date: " . $request->date .
                        "\nTour Time: " . $request->time_from .
                        "\nTourists: " . $request->tourists .
                        "\nEmail: " . $request->customer_email,
                    'metadata' => [
                        'Booking Id' => $request->booking_id,
                        'Tour Package' => $request->tour_package,
                        'Tour Date' => $request->date,
                        'Tour Time' => $request->time_from,
                        'Tourists' => $request->tourists,
                        'Customer Name' => $request->customer_name,
                        'Customer Phone' => $request->customer_phone,
                        'Email' => $request->customer_email,
                    ],
                    'statement_descriptor' => $tourOperatorName,
                    'application_fee_amount' => $destinationAmount * 100,
                    'transfer_data' => [
                        'destination' => $tourOperator->stripe_destination_id,
                    ],
                    'customer' => $customer->stripe_id,
                ], ['idempotency_key' => $request->booking_id . Carbon::now()]);
            } else {
                $paymentIntent = PaymentIntent::create([
                    'payment_method_types' => ['card_present'],
                    'capture_method' => 'manual',
                    'currency' => $request->currency,
                    'amount' => $convertedAmount * 100,
                    'statement_descriptor' => $tourOperatorName,
                    'application_fee_amount' => $destinationAmount * 100,
                    'transfer_data' => [
                        'destination' => $tourOperator->stripe_destination_id,
                    ],
                ]);
            }

            $paymentIntentToken = [
                'client_secret' => $paymentIntent->client_secret,
                'id' => $paymentIntent->id
            ];

            return $paymentIntentToken;
        } catch (CardException $exception) {
            Log::error($exception);

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $request->booking_id,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            throw new GeneralCoreException($exception);
        } catch (Exception $exception) {
            Log::error($exception);

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $request->booking_id,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Capture stripe payment using payment intent id
     *
     * @param object $request
     * @return array
     */
    public function stripeCapturePayment($request)
    {
        try {
            $stripe = new StripeClient(
                env('STRIPE_API_SECRET_KEY')
            );

            $paymentCapture = $stripe->paymentIntents->capture(
                $request->payment_intent_id,
                []
            );

            $paymentCaptureStatus = [
                'status' => $paymentCapture->status,
                'payment_intent_id' => $request->payment_intent_id
            ];

            return $paymentCaptureStatus;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Returns a list of Reader objects from Stripe for physical device
     *
     * @param object $request
     * @return object
     */
    public function listAllReaders($request)
    {
        try {
            $stripe = new StripeClient(
                env('STRIPE_API_SECRET_KEY')
            );

            $data = $stripe->terminal->readers->all(['limit' => 3]);

            return $data;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Make payment for pay later bookings
     *
     * @param object $request
     * @return array
     */
    public function paymentNewBooking($request)
    {
        DB::beginTransaction();
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('tour_bookings_master as tbm', 'tbm.tour_package_id', '=', 'tp.id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->join('customers as cus', 'cus.id', '=', 'tbm.customer_id')
                ->where('tbm.id', $request->bookingID)
                ->select([
                    'tbm.id', 'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone',
                    't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'cus.name as customername', 'cus.email as customeremail',
                    'cus.phone_number as customerphone', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail',
                    'tp.api_key as packageApiKey', 'tbm.tourists',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ])->first();

            $customer = $this->customer::where('email', $info->customeremail)
                ->where('tour_operator_id', $info->tour_operator_id)
                ->first();

            if (!$customer) {
                $pwdChars = '23456789~!@#$%^*()-=+_[]{};:?.,>abcdefghijkpqstuxyzABCDEFGHJKLMNPQRSTUXYZ';
                $pwd = '';

                for ($i = 0, $cl = strlen($pwdChars) - 1; $i < 10; $i++) {
                    $pwd .= $pwdChars[rand(0, $cl)];
                }

                $customer = $this->customer::create([
                    'tour_operator_id' => $info->tour_operator_id,
                    'name' => $info->customername,
                    'email' => $info->customeremail,
                    'phone_number' => str_replace(['(', ')', '+', '-', '.', ' '], '', $info->customerphone),
                    'password' => Hash::make(env('APP_DEBUG') ? '123456' : $pwd),
                ]);
            }

            $tourBooking = DB::table('tour_bookings_master AS tbm')
                ->join('customers as c', 'c.id', '=', 'tbm.customer_id')
                ->join('tour_bookings_transact as tbt', 'tbt.tour_booking_id', '=', 'tbm.id')
                ->join('tour_packages as pkg', 'pkg.id', '=', 'tbm.tour_package_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'pkg.tour_operator_id')
                ->join('tour_slot_times as st', 'st.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates as sd', 'sd.id', '=', 'st.tour_slot_id')
                ->where('tbm.id', '=', $request->bookingID)
                ->select(
                    'tbm.id',
                    'tbm.id as booking_id',
                    'tbm.total',
                    'tbm.discount2_value',
                    'tbm.discount2_percentage',
                    'c.name as Name',
                    'sd.date as Date',
                    'st.time_from as StartTime',
                    'c.phone_number as ContactNumber',
                    'c.email as Email',
                    'tbm.tourists',
                    'c.id as customer_id',
                    't_o.id as tour_operator_id',
                    't_o.name as tour_operator_name',
                    'tbm.service_commission',
                    'tp.stripe_destination_id',
                    'tp.name as package_name',
                    't_o.service_commission_percentage as tourOperatorServiceCommission',
                    't_o.affiliate_processing_percentage as tourOperatorAffiliateServiceCommission'
                )
                ->get();

            $natFeesPercentage = 0;
            $stripeFeesPercentage = 0;
            if ($request->bookingID > config('constants.old_bookings.booking_id')) {
                if ($customer->is_affiliate == 'Yes') {
                    $natFeesPercentage = $info->affiliate_processing_percentage;
                    $stripeFeesPercentage = $info->stripe_affiliate_percentage;
                } else {
                    $natFeesPercentage = $info->service_commission_percentage;
                    $stripeFeesPercentage = $info->stripe_commission_percentage;
                }
            }

            $bookingT = $this->bookingTransact::with('packageRate')
                ->where('tour_booking_id', $request->bookingID)
                ->get();

            $rate = 0;
            $permitFee = 0;
            $procFee = 0;
            $additionalCharge = 0;
            $tourists = 0;
            $subTotal = 0;
            $total = 0;
            $serviceCommission = 0;
            $fees = 0;
            $discountAmt= 0;
            $oldDiscountAmt = 0;
            $serviceCommissionTotal = 0;

            // Calculate the booking amount
            foreach ($bookingT as $tbTransaction) {
                $rate = $tbTransaction->rate;
                $permitFee = $tbTransaction->permit_fee;
                $additionalCharge = $tbTransaction->additional_charge;
                $tourists = $tbTransaction->tourists;

                if ($request->payment_method == 'Card') {
                    if ($request->bookingID < config('constants.old_bookings.booking_id')) {
                        $procFee = 2.9;
                    } else {
                        $procFee = $tbTransaction->packageRate->processing_charge_percentage;
                        if ($request->has('discount_type') == false) {
                            $serviceCommissionPax = roundout((($rate + $additionalCharge + $permitFee) * $natFeesPercentage / 100), 2);
                            $serviceCommissionPax = $tourists * $serviceCommissionPax;
                            $serviceCommissionTotal += $serviceCommissionPax;
                        }
                    }
                } else {
                    $procFee = 0;
                }
                $refund_permitfee_status = ($tbTransaction->refund_permitfee_status != '') ? json_decode($tbTransaction->refund_permitfee_status) : '';
                $refundedPermitFeesTotal = 0;
                if (!empty($refund_permitfee_status) && $permitFee > 0) {
                    foreach ($refund_permitfee_status as $val) {
                        if ($val->status == 'Y' && !isset($val->is_pax_added)) {
                            $refundedPermitFeesTotal += (isset($val->amount) && $val->amount > 0) ? $val->amount : 0;
                        }
                    }
                    $subTotal = (($rate + $permitFee + $additionalCharge) * $tourists) - $refundedPermitFeesTotal;
                } else {
                    $subTotal = ($rate + $permitFee + $additionalCharge) * $tourists;
                }
                $total += ($procFee && $request->payment_method == 'Card') ? $subTotal + ($subTotal * ($procFee / 100)) : $subTotal;
                $tbTransaction->processing_charge_percentage = $procFee;
                $tbTransaction->nat_fees_percentage = $natFeesPercentage;
                $tbTransaction->stripe_fees_percentage = $stripeFeesPercentage;
                $tbTransaction->total = $procFee ? $subTotal + ($subTotal * ($procFee / 100)) : $subTotal;
                $tbTransaction->save();
            }

            $trans = $this->transaction::where('tour_booking_id', $request->bookingID)->first(['payment_type']);
            if (!empty($trans) && $trans->payment_type == 1) {
                if ($request->split_value) {
                    $total = $request->split_value;
                }
            }

            if (!empty($trans) && ($trans->payment_type == '' || $trans->payment_type == 0)) {
                if (isset($request->api) && $request->api == 1) {
                    throw new Exception('Tour booking already paid.');
                } else {
                    return response()->json([
                        'tourBookingId' => $request->bookingID,
                        'paid' => 1
                    ]);
                }
            }

            if ($request->has('discount_type')) {
                if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage))
                    $discountAmt = round(($total * $request->discount_percentage) / 100, 2);//round($request->pr_amount, 2);
                elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money))
                    $discountAmt = round($request->discount_fixed_money, 2);
            }

            if (empty($request->split_value)) {
                $total -= $discountAmt;
            }

            //Check for old discount applied when booking is done using Pay Later
            if (isset($tourBooking[0]->discount2_value) && $tourBooking[0]->discount2_value > 0 && $tourBooking[0]->discount2_percentage == 0) {
                $oldDiscountAmt = $tourBooking[0]->discount2_value;
                if (empty($request->split_value)) {
                    $total = $total - $tourBooking[0]->discount2_value;
                }
            } else if ($tourBooking[0]->discount2_percentage > 0) {
                $oldDiscountAmt = number_format(($total * $tourBooking[0]->discount2_percentage) / 100, 2, '.', '');
                if (empty($request->split_value)) {
                    $total = $total - $oldDiscountAmt;
                }
            }

            // Add the service commission in the total amount
            if ($request->payment_method == "Card" && $customer->is_affiliate == 'No') {
                if ($request->bookingID >= config('constants.old_bookings.booking_id')) { // Set booking id 95440
                    if ($discountAmt > 0 || $oldDiscountAmt > 0 || !empty($request->split_value)) {
                        $serviceCommission = roundout(($total * $tourBooking[0]->tourOperatorServiceCommission / 100), 2);
                    } else {
                        $serviceCommission = $serviceCommissionTotal;
                    }
                } else {
                    $serviceCommission = number_format((float)$total * 6 / 100, 2, '.', '');
                }
                $total = $total + $serviceCommission;
            } elseif ($request->payment_method == "Card" && $customer->is_affiliate == 'Yes') {
                if ($request->bookingID >= config('constants.old_bookings.booking_id')) { // Set booking id 95440
                    if ($discountAmt > 0 || $oldDiscountAmt > 0 || !empty($request->split_value)) {
                        $serviceCommission = roundout(($total * $tourBooking[0]->tourOperatorAffiliateServiceCommission / 100), 2);
                    } else {
                        $serviceCommission = $serviceCommissionTotal;
                    }
                    $total = $total + $serviceCommission;
                }
            }

            // Set user of updated booking
            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $createdByWithName = $user->name . ': (' . "Admin" . ')';
                $routePrefix = 'admin.';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $createdByWithName = $user->name . ': (' . "Tour Operator" . ')';
                $routePrefix = '';
            } else {
                $user = $this->staff::find(auth()->id());
                $createdByWithName = $user->name . ': (' . "Tour Operator" . ')';
                $routePrefix = '';
            }

            // Set payment type while do payment of booking
            if ($request->payment_method == "Pay Later" || $request->payment_method == "Invoiced") {
                $paymentType = "Invoiced";
            } else if ($request->payment_method == "Cash") {
                $paymentType = "Cash";
            } else {
                $paymentType = "Card";
            }

            $trans = $this->transaction::where('tour_booking_id', $request->bookingID)->first(['payment_type']);
            if (!empty($trans) && $trans->payment_type == 1) {
                $paymentOnly = $this->tourBookingHistory::where('tour_booking_id', $request->bookingID)->where('operation', 'Payment')->get();
                $paymentOnlyCheck = 0;
                foreach ($paymentOnly as $value) {
                    $details = json_decode($value->details);
                    $paymentOnlyCheck += $details->amount;
                }
            }

            if ($request->payment_method != 'Card') {
                $total = number_format((float)$total, 2, '.', '');
                if (!empty($trans) && $trans->payment_type == 1) {
                    if (empty($request->split_value)) {
                        $total = $total - $paymentOnlyCheck;
                    }
                }
            } else {
                $total = round($total, 2);
                $subTotal = round($total - $serviceCommission, 2);
                if ($request->has('api') && $request->api == '1') {
                    if (!empty($trans) && $trans->payment_type == 1) {
                        if (empty($request->split_value)) {
                            $total = $total - $paymentOnlyCheck;
                        }
                    }

                    $paymentIntentId = ($request->has('payment_intent_id') && $request->payment_intent_id != '') ? $request->payment_intent_id : '';
                    if ($paymentIntentId == '') {
                        throw new Exception('You must provide Stripe Payment Intent Id');
                    }

                    $stripe = new StripeClient(
                        env('STRIPE_API_SECRET_KEY')
                    );

                    $paymentIntent = $stripe->paymentIntents->retrieve(
                        $paymentIntentId,
                        []
                    );

                    $stripe->paymentIntents->update(
                        $paymentIntentId,
                        [
                            'description' => 'Booking Id: ' . $info->id .
                                "\nTour Package: " . $info->package_name .
                                "\nTour Date: " . $info->date .
                                "\nTour Time: " . $info->time_from .
                                "\nTourists: " . $info->tourists .
                                "\nEmail: " . $info->customeremail,
                            'metadata' => [
                                'Booking Id' => $info->id,
                                'Tour Package' => $info->package_name,
                                'Tour Date' => $info->date,
                                'Tour Time' => $info->time_from,
                                'Tourists' => $info->tourists,
                                'Customer Name' => $info->customername,
                                'Customer Phone' => $info->customerphone,
                                'Email' => $info->customeremail,
                            ]
                        ], ['idempotency_key' => $info->id . carbon::now()]
                    );

                    $crd = '';
                    $cardtype = '';
                    if (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'])) {
                        $crd = (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'])) ? $paymentIntent->charges->data[0]->payment_method_details['card_present']['last4'] : '';
                        $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card_present->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card_present->brand : '';
                    } else if (isset($paymentIntent->charges->data[0]->payment_method_details['card']['last4'])) {
                        $crd = (isset($paymentIntent->charges->data[0]->payment_method_details['card']['last4'])) ? $paymentIntent->charges->data[0]->payment_method_details['card']['last4'] : '';
                        $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
                    }

                    $fees = (isset($paymentIntent->application_fee_amount) && $paymentIntent->application_fee_amount > 0) ? $paymentIntent->application_fee_amount/100 : 0;
                } else {
                    if (!empty($trans) && $trans->payment_type == 1) {
                        if (empty($request->split_value)) {
                            $total = $total - $paymentOnlyCheck;
                        }
                        $serviceCommission = ($total * $tourBooking[0]->tourOperatorServiceCommission) / 100;
                    }

                    $tourBookingTransactionDetails = $this->bookingTransact::where('tour_booking_id', $request->bookingID)->first();

                    $tour_operator_name = '';
                    if ($info->tour_operator_id == 1) {
                        $tour_operator_name = "Dixies Lower Antelope";
                    }

                    if ($info->tour_operator_id == 2) {
                        $tour_operator_name = "DD MTA";
                    }

                    if ($info->tour_operator_id == 3) {
                        $tour_operator_name = "John Rambo Adventures";
                    }

                    if ($info->tour_operator_id == 4) {
                        $tour_operator_name = "L.A.C.T. Shuttle LLC";
                    }

                    $month = substr($request->expiration, 0, 2);
                    $yearSet = substr($request->expiration, 3, 2);
                    $y = 20;
                    $year = $y . '' . $yearSet;
                    $cardNumber = $request->cardnumber;
                    $cvc = $request->cvv;

                    Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));

                    $method = PaymentMethod::create([
                        'type' => 'card',
                        'card' => [
                            'number' => $cardNumber,
                            'exp_month' => $month,
                            'exp_year' => $year,
                            'cvc' => $cvc,
                        ],
                    ]);

                    if (!empty($request->split_value)) {
                        $destination_amount = ($request->split_value - ($request->split_value * $tourBookingTransactionDetails->stripe_fees_percentage / 100));
                        $destination_amount = $request->split_value - $destination_amount;
                        $destination_amount = round($destination_amount, 2);
                        $fees = $destination_amount;
                    } else {
                        $destination_amount = ($total - ($total * $tourBookingTransactionDetails->stripe_fees_percentage / 100));
                        $destination_amount = $total - $destination_amount;
                        $destination_amount = round($destination_amount, 2);
                        $fees = $destination_amount;
                    }

                    if ($customer->stripe_id == null) {
                        $stripeCustomer = StripeCustomer::create([
                            'name' => $customer->name,
                            'email' => $customer->email,
                            'phone' => $customer->phone_number
                        ]);

                        $customer->stripe_id = $stripeCustomer->id;
                        $customer->save();
                    }

                    if ($request->bookingID < config('constants.old_bookings.booking_id')) {
                        $tourBooking[0]->stripe_destination_id = 'acct_1KiPPfF2s9WqF2Qr';
                    }
                    $totalSp = 0;
                    if (!empty($request->split_value)) {
                        $totalSp = $request->split_value;
                    }else{
                        $totalSp = $total;
                    }
                    $paymentIntent = PaymentIntent::create([
                        'payment_method_types' => ['card'],
                        'payment_method' => $method->id,
                        'currency' => 'usd',
                        'amount' => $totalSp * 100, // stripe needs amount in cents
                        'description' => 'Booking Id: ' . $request->bookingID .
                            "\nTour Package: " . $tourBooking[0]->package_name .
                            "\nTour Date: " . $tourBooking[0]->Date .
                            "\nTour Time: " . $tourBooking[0]->StartTime .
                            "\nTourists: " . $tourBooking[0]->tourists .
                            "\nEmail: " . $tourBooking[0]->Email,
                        'metadata' => [
                            'Booking Id' => $request->bookingID,
                            'Tour Package' => $tourBooking[0]->package_name,
                            'Tour Date' => $tourBooking[0]->Date,
                            'Tour Time' => $tourBooking[0]->StartTime,
                            'Tourists' => $tourBooking[0]->tourists,
                            'Customer Name' => $tourBooking[0]->Name,
                            'Customer Phone' => $tourBooking[0]->ContactNumber,
                            'Email' => $tourBooking[0]->Email,
                            'split_payment' => (!empty($request->split_value)) ? 1 : 0
                        ],
                        'statement_descriptor' => $tour_operator_name,
                        'confirm' => true,
                        'application_fee_amount' => $destination_amount * 100,
                        'transfer_data' => [
                            'destination' => $tourBooking[0]->stripe_destination_id,
                        ],
                        'customer' => $customer->stripe_id,
                    ], ['idempotency_key' => $request->bookingID . Carbon::now()]);

                    $paymentIntentId = $paymentIntent->id;
                    if (isset($paymentIntent) && $paymentIntent->status == 'requires_action') {
                        $stripe = new StripeClient(
                            env('STRIPE_API_SECRET_KEY')
                        );

                        $paymentIntentConfirm = $stripe->paymentIntents->confirm(
                            $paymentIntent->id,
                            [
                                'return_url' => route($routePrefix . 'tour-operator.booking.show', ['booking' => $request->bookingID]),
                                'payment_method_options' => [
                                    'card' => [
                                        'request_three_d_secure' => 'any'
                                    ]
                                ]
                            ]
                        );

                        if (isset($paymentIntentConfirm->next_action) && !empty($paymentIntentConfirm->next_action) && isset($paymentIntentConfirm->next_action->redirect_to_url->url) && !empty($paymentIntentConfirm->next_action->redirect_to_url->url)) {
                            DB::table('tour_bookings_master')
                                ->where('id', $request->bookingID)
                                ->update([
                                    'stripe_paymentintent_id' => $paymentIntent->client_secret,
                                    'updated_at' => mysqlDT(),
                                    'updated_by' => auth()->id(),
                                    'updated_by_user_type' => session('userType'),
                                ]);

                            DB::commit();

                            if (isset($request->api) && $request->api == 1) {
                                return [
                                    'tourBookingId' => $request->bookingID,
                                    'require3ds' => 1,
                                    'nextAction' => $paymentIntentConfirm->next_action->redirect_to_url->url
                                ];
                            } else {
                                return response()->json([
                                    'tourBookingId' => $request->bookingID,
                                    'require3ds' => 1,
                                    'nextAction' => $paymentIntentConfirm->next_action->redirect_to_url->url
                                ]);
                            }
                        }
                    }

                    $crd = substr($cardNumber, -4); //substr($data['cardnumber'], 12, 16);
                    $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
                }
            }

            $transactionId = DB::table('transactions')
                ->insertGetId([
                    'tour_booking_id' => $request->bookingID,
                    'amount' => !empty($request->split_value) ? $request->split_value : $total,
                    'fee_amount' => $fees,
                    'transaction_number' => $paymentIntent->id ?? '',
                    'application_fee_id' => $paymentIntent->charges->data[0]->application_fee ?? '',
                    'via' => $paymentType,
                    'payment_type' => !empty($request->split_value) ? 1 : 0 ,
                    'response_init' => '',
                    'response_end' => '',
                    'response_end_datetime' => '0000-00-00 00:00:00',
                    'status' => 'Completed',
                    'visitor' => $request->getClientIp(),
                    'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ]);

            $historyId = DB::table('tour_booking_history')
                ->insertGetId([
                    'tour_booking_id' => $tourBooking[0]->booking_id,
                    'tourists' => $tourBooking[0]->tourists,
                    'operation' => 'Payment',
                    'details' => json_encode([
                        'amount' => !empty($request->split_value) ? $request->split_value : $total,
                        'payment_type' => $paymentType,
                        'card_number' => $crd ?? null,
                        'created_by_with_name' => $createdByWithName,
                        'payment_method' => !empty($request->split_value) ? 'Split Payment' : 'Full Payment',
                        'card_type'=> $cardtype ?? null,

                    ], JSON_NUMERIC_CHECK),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => $userType,
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            if (empty($trans)) {
                $amountUpdate = TourBooking::findOrFail($request->bookingID);
                $amountUpdate->total = $total;
                $amountUpdate->booking_total = $total;
                $amountUpdate->service_commission = $serviceCommission;
                $amountUpdate->save();
            }

            if (empty($trans)) {
                if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later' || $request->payment_method == 'Invoiced') {
                    $amountupdate = $this->tourBooking::findOrFail($request->bookingID);
                    if ($amountupdate) {
                        if (!empty($request->split_value) && $request->has('discount_type') && $discountAmt > 0) {
                            $total -= $discountAmt;
                        }

                        if (!empty($request->split_value) && $oldDiscountAmt > 0) {
                            $total -= $oldDiscountAmt;
                        }


                        $amountupdate->total = $total;
                        $amountupdate->booking_total = $total;
                        if ($request->has('discount_type')) {
                            if ($request->discount_type == 'Percentage') {
                                $amountupdate->discount2_percentage = $request->discount_percentage;
                                $amountupdate->discount2_value =  ($amountupdate->discount2_value>0)?$amountupdate->discount2_value+$discountAmt:$discountAmt;
                            } elseif ($request->discount_type == 'Fixed Money') {
                                $amountupdate->discount2_value =  ($amountupdate->discount2_value>0)?$amountupdate->discount2_value+$discountAmt:$discountAmt;
                            }
                        }

                        $amountupdate->service_commission = 0;
                        $amountupdate->save();
                    }
                } else {
                    if (!empty($request->split_value) && $request->has('discount_type') && $discountAmt > 0) {
                        $total -= $discountAmt;
                    }

                    if (!empty($request->split_value) && $oldDiscountAmt > 0) {
                        $total -= $oldDiscountAmt;
                    }

                    $amountupdate = $this->tourBooking::findOrFail($request->bookingID);
                    $amountupdate->total = $total;
                    $amountupdate->booking_total = $total;
                    if ($request->has('discount_type')) {
                        if ($request->discount_type == 'Percentage') {
                            $amountupdate->discount2_percentage = $request->discount_percentage;
                            $amountupdate->discount2_value = ($amountupdate->discount2_value>0) ? $amountupdate->discount2_value + $discountAmt : $discountAmt;
                        } elseif ($request->discount_type == 'Fixed Money') {
                            $amountupdate->discount2_value = ($amountupdate->discount2_value>0) ? $amountupdate->discount2_value + $discountAmt : $discountAmt;
                        }
                    }

                    $amountupdate->save();
                }
            } else if (!empty($trans)) {
                if ($request->has('discount_type') && $discountAmt > 0) {
                    $amountupdate = $this->tourBooking::findOrFail($request->bookingID);
                    if ($amountupdate) {
                        $amountupdate->total = $amountupdate->total - $discountAmt;
                        $amountupdate->booking_total = $amountupdate->booking_total - $discountAmt;
                        if ($request->has('discount_type')) {
                            if ($request->discount_type == 'Percentage') {
                                $amountupdate->discount2_percentage = $request->discount_percentage;
                                $amountupdate->discount2_value = ($amountupdate->discount2_value>0)?$amountupdate->discount2_value+$discountAmt:$discountAmt;
                            } elseif ($request->discount_type == 'Fixed Money') {
                                $amountupdate->discount2_value = ($amountupdate->discount2_value>0)?$amountupdate->discount2_value+$discountAmt:$discountAmt;
                            }
                        }

                        $amountupdate->save();
                    }
                }
            }

            // Below line is for to update tour booking history table
            $this->handlePaymentHistoryUpdateJobs($tourBooking, $transactionId, $historyId, $request, $paymentType, $info);

            DB::commit();

            if (isset($request->api) && $request->api == 1) {
                return [
                    'tourBookingId' => $request->bookingID,
                    'totalBookedSeats' => $info->seats
                ];
            } else {
                return response()->json([
                    'tourBookingId' => $request->bookingID,
                    'totalBookedSeats' => $info->seats,
                    'splitCount' => $request->dividedBy,
                ]);
            }
        } catch (CardException $exception) {
            Log::error($exception);

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $request->bookingID,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            throw new GeneralCoreException($exception);
        } catch (Exception $exception) {
            Log::error($exception);

            DB::table('tour_booking_history')
                ->insert([
                    'tour_booking_id' => $request->bookingID,
                    'operation' => 'Error',
                    'details' => $exception->getMessage(),
                    'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                    'operation_by' => auth()->id(),
                    'operation_by_user_type' => auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin',
                    'updated_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                ]);

            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Handle the queue jobs to perform the history data updates
     *
     * @param  object  $tourBooking
     * @param  int     $transactionId
     * @param  int     $historyId
     * @param  object  $request
     * @param  string  $paymentType
     *
     * @return void
     */
    public function handlePaymentHistoryUpdateJobs($tourBooking, $transactionId, $historyId, $request, $paymentType, $info): void
    {
        // Check the split payment or full payment
        $isSplitOrFull = !empty($request->split_value) ? 'Split Payment' : 'Full Payment';
        $tourBooking[0]->tour_operator_id = $info->tour_operator_id;

        // Perform the jobs based on categories
        if ($paymentType === 'Card') {
            CardBookingService::run((array) $tourBooking[0], $transactionId, $historyId, $isSplitOrFull, $request->all());
            // dispatch(new CardBooking((array) $tourBooking[0], $transactionId, $historyId, $isSplitOrFull, $request->all()));
        } elseif ($paymentType === 'Cash') {
            dispatch(new CashBooking($tourBooking[0], $transactionId, $historyId, $isSplitOrFull, $request->all()));
        } elseif ($paymentType === 'Invoiced') {
            dispatch(new InvoicedBooking($tourBooking[0], $transactionId, $historyId, $isSplitOrFull, $request->all()));
        }
    }

    /**
     * Make payment for affiliate bookings
     *
     * @param object $request
     * @return array
     */
    public function paymentAffiliateBooking($request)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $transactionExist = $this->transaction::where('tour_booking_id',$request->bookingID)->where('status','=','Completed')->count();

            if($transactionExist > 0) {
                throw new Exception('You already made payment for this booking');
            }

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('tour_bookings_master as tbm', 'tbm.tour_package_id', '=', 'tp.id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->join('customers as cus', 'cus.id', '=', 'tbm.customer_id')
                ->where('tbm.id', $request->bookingID)
                ->select([
                    'tst.time_from', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    'cus.name as customername', 'cus.email as customeremail', 'cus.phone_number as customerphone',
                    DB::raw("(SELECT SUM(tbm.tourists)
						FROM tour_bookings_master AS tbm
						WHERE tbm.tour_slot_id = tst.id
							AND tbm.status != 'Cancelled'
						GROUP BY tbm.tour_slot_id) AS booked_seats")
                ])->first();

            $rateGrpIds = [];

            for ($a = 0, $c = ($request->tourists); $a < $c; $a++) {
                if ($request->tourists[$a])
                    $rateGrpIds[] = $request->rate_group[$a];
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->whereIn('tpr.id', $rateGrpIds)
                ->where('tpr.deleted_by', 0)
                ->orderByDesc('tpr.age_from')
                ->get([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage',
                    'tpr.processing_charge_percentage', 'tpr.additional_charge'
                ]);

            $taxes = Tax::where([
                'state_id' => $info->state_id,
                'status' => 'Active',
                'deleted_by' => 0
            ])
                ->get(['value', 'value_type']);

            $customer = Customer::where('email', $info->customeremail)
                ->where('tour_operator_id', $info->tour_operator_id)
                ->first();

            DB::beginTransaction();

            if (!$customer) {
                $pwdChars = '23456789~!@#$%^*()-=+_[]{};:?.,>abcdefghijkpqstuxyzABCDEFGHJKLMNPQRSTUXYZ';
                $pwd = '';

                for ($i = 0, $cl = strlen($pwdChars) - 1; $i < 10; $i++)
                    $pwd .= $pwdChars[rand(0, $cl)];

                $customer = Customer::create([
                    'tour_operator_id' => $info->tour_operator_id,
                    'name' => $info->customername,
                    'email' => $info->customeremail,
                    'phone_number' => str_replace(['(', ')', '+', '-', '.', ' '], '', $info->customerphone),
                    'password' => Hash::make(env('APP_DEBUG') ? '123456' : $pwd),
                ]);
            }

            $total = $discountAmt = 0;
            $touristGroups = [];

            for ($a = 0, $ac = ($request->tourists); $a < $ac; $a++) {
                if (!$request->tourists[$a])
                    continue;

                $tourists = $request->tourists[$a];
                $groupTotal = $stateTax = 0;

                for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                    if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                        $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                        break;
                    }
                }

                if ($b >= $bc) { // rate group not found
                    DB::rollBack();
                    throw new Exception('Unable to book the tour package.');
                }

                // rate may be zero for one or more age/rate group and
                // if rate is non-zero then only add other amounts
                // if ($rateGroups[$b]->rate != 0) {
                for ($c = 0, $cc = count($taxes); $c < $cc; $c++) {
                    $stateTax += $tourists * ($taxes[$c]->value_type == 'Percent'
                            ? $rateGroups[$b]->rate * $taxes[$c]->value / 100
                            : $taxes[$c]->value
                        );
                }

                $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                            $rateGroups[$b]->additional_charge +
                            $info->permit_fee
                        )) + $stateTax;

                $groupTotal += $groupTotal * $rateGroups[$b]->processing_charge_percentage / 100;
                // }

                $total += $groupTotal;
            }

            if ($request->has('discount_type')) {
                if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage))
                    $discountAmt = round($request->pr_amount, 2);
                elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money))
                    $discountAmt = round($request->discount_fixed_money, 2);
            }

            $total -= $discountAmt;
            $serviceCommission = $total * $info->service_commission_percentage / 100;
            $total += $serviceCommission;
            $total = round($total, 2);

            $tourBooking = DB::table('tour_bookings_master AS tbm')
                ->join('customers as c', 'c.id', '=', 'tbm.customer_id')
                ->join('tour_packages as pkg', 'pkg.id', '=', 'tbm.tour_package_id')
                ->join('tour_bookings_transact as tbt', 'tbt.tour_booking_id', '=', 'tbm.id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'pkg.tour_operator_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_slot_times as st', 'st.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates as sd', 'sd.id', '=', 'st.tour_slot_id')
                ->where('c.is_affiliate', 'Yes')
                ->where('tbm.id', '=', $request->bookingID)
                ->select('tbm.id', 'tbt.total', 'tbm.service_commission', 'tbm.total as Mtotal', 'c.name as Name', 'sd.date as Date', 'st.time_from as StartTime', 'c.phone_number as ContactNumber', 'c.email as Email', 'tbm.tourists', 'c.id as customer_id', 't_o.name as tour_operator_name', 'tp.stripe_destination_id')
                ->get();

            if ($request->payment_method == 'Cash' || $request->payment_method == 'Pay Later') {
                $str = ltrim($request->tt_cost, '$');
                $total = round($str, 2);
                if ($request->payment_method == 'Cash') {
                    $payment_type = 'Cash';
                } else {
                    $payment_type = 'Invoiced';
                }

                DB::table('transactions')
                    ->insert([
                        'tour_booking_id' => $tourBooking[0]->id,
                        'amount' => $total,
                        'transaction_number' => '',
                        'via' => $payment_type,
                        'response_init' => '',
                        'response_end' => '',
                        'response_end_datetime' => '0000-00-00 00:00:00',
                        'status' => 'Completed',
                        'visitor' => $request->getClientIp(),
                        'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ]);

                if ($userType == "Admin") {
                    $user = Admin::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = Staff::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                } else {
                    $user = Customer::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Affiliate" . ')';
                }

                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $tourBooking[0]->id,
                        'tourists' => $tourBooking[0]->tourists,
                        'operation' => 'Payment',
                        'details' => json_encode(['amount' => $total, 'payment_type' => $payment_type, 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                        'operation_at' => Carbon::now(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                $amountupdate = TourBooking::findOrFail($request->bookingID);
                if ($amountupdate) {
                    $amountupdate->total = $total;
                    if ($request->has('discount_type')) {
                        if ($request->discount_type == 'Percentage') {
                            $amountupdate->discount2_percentage = $request->discount_percentage;
                            $amountupdate->discount2_value = $discountAmt;
                        } elseif ($request->discount_type == 'Fixed Money') {
                            $amountupdate->discount2_value = $discountAmt;
                        }
                    }
                    $amountupdate->service_commission = 0;
                    $amountupdate->save();
                }
            } else {
                $str = ltrim($request->tt_cost, '$');
                $total = round($str, 2);
                if($request->has('api') && $request->api=='1'){
                    $paymentIntentId= ($request->has('paymentintentid') && $request->paymentintentid!='')?$request->paymentintentid:'';
                    if($paymentIntentId == '') {
                        throw new Exception('You must provide Stripe Payment Intent Id');
                    }
                    $stripe = new StripeClient(
                        env('STRIPE_API_SECRET_KEY')
                    );
                    $paymentIntent =  $stripe->paymentIntents->retrieve(
                        $paymentIntentId,
                        []
                    );
                    $crd = (isset($paymentIntent->charges->data[0]->payment_method_details['card_present']['last4']))?$paymentIntent->charges->data[0]->payment_method_details['card_present']['last4']:'';
                    $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card_present->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card_present->brand : '';

                    if($crd==''){
                        throw new Exception('Something went wrong with Stripe payment.Please try again.');
                    }
                    $data = [
                        'amount' => $request->amount
                    ];

                } else {
                    $month = substr($request->expiration, 0, 2);
                    $yearSet = substr($request->expiration, 3, 2);
                    $y = 20;
                    $year = $y . '' . $yearSet;
                    $cardnumber = $request->cardnumber;
                    $cvc = $request->cvv;
                    $data = [
                        'total' => $total,
                        'booking_id' => $request->bookingID,
                        'month' => $month,
                        'year' => $year,
                        'cardnumber' => $cardnumber,
                        'cvc' => $cvc
                    ];

                    if ($request->bookingID < config('constants.old_bookings.booking_id')) {
                        $tourBooking[0]->stripe_destination_id = 'acct_1KiPPfF2s9WqF2Qr';
                    }

                    Stripe::setApiKey(env('STRIPE_API_SECRET_KEY'));

                    $method = PaymentMethod::create([
                        'type' => 'card',
                        'card' => [
                            'number' => $data['cardnumber'],
                            'exp_month' => $data['month'],
                            'exp_year' => $data['year'],
                            'cvc' => $data['cvc'],
                        ],
                    ]);

                    $destination_amount = ($data['total'] - ($data['total'] * $info->stripe_commission_percentage / 100));

                    $destination_amount = round($destination_amount, 2);
                    $paymentIntent = PaymentIntent::create([
                        'payment_method_types' => ['card'],
                        'payment_method' => $method->id,
                        'currency' => 'usd',
                        'amount' => $data['total'] * 100, // stripe needs amount in cents
                        'description' => 'Booking Id: ' . $request->bookingID .
                            "\nTour Package: " . $tourBooking[0]->tour_operator_name .
                            "\nTour Date: " . $tourBooking[0]->Date .
                            "\nTour Time: " . $tourBooking[0]->StartTime .
                            "\nTourists: " . $tourBooking[0]->tourists .
                            "\nEmail: " . $tourBooking[0]->Email,
                        'metadata' => [
                            'Booking Id' => $request->bookingID,
                            'Tour Package' => $tourBooking[0]->tour_operator_name,
                            'Tour Date' => $tourBooking[0]->Date,
                            'Tour Time' => $tourBooking[0]->StartTime,
                            'Tourists' => $tourBooking[0]->tourists,
                            'Customer Name' => $tourBooking[0]->Name,
                            'Customer Phone' => $tourBooking[0]->ContactNumber,
                            'Email' => $tourBooking[0]->Email,
                        ],
                        'confirm' => true,
                        'transfer_data' => [
                            'amount' => $destination_amount * 100,
                            'destination' => $tourBooking[0]->stripe_destination_id,
                        ],
                    ], ['idempotency_key' => $request->bookingID . Carbon::now()]);
                    $paymentIntentId = $paymentIntent->id;
                    $crd = substr($data['cardnumber'], -4); //substr($data['cardnumber'], 12, 16);
                    $cardtype = !empty($paymentIntent->charges->data[0]->payment_method_details->card->brand) ? $paymentIntent->charges->data[0]->payment_method_details->card->brand : '';
                }

                DB::table('transactions')
                    ->insertGetId([
                        'tour_booking_id' => $request->bookingID,
                        'amount' => $data['total'],
                        'transaction_number' => $paymentIntentId,
                        'application_fee_id' => $paymentIntent->charges->data[0]->application_fee??0,
                        'via' => 'Card',
                        'response_init' => '',
                        'response_end' => '',
                        'response_end_datetime' => '0000-00-00 00:00:00',
                        'status' => 'Completed',
                        'visitor' => $request->getClientIp(),
                        'request_from' => $request->is('api/*') ? Constants::API : Constants::BACKEND,
                        'created_by' => auth()->id(),
                        'created_by_user_type' => $userType,
                        'created_at' => Carbon::now(),
                    ]);

                if ($userType == "Admin") {
                    $user = Admin::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = Staff::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                } else {
                    $user = Customer::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Affiliate" . ')';
                }

                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $data['booking_id'],
                        'tourists' => $data['tourists'],
                        'operation' => 'Payment',
                        'details' => json_encode(['amount' => $data['total'], 'payment_type' => 'Card', 'card_number' => $crd,'card_type'=>$cardtype, 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                        'operation_at' => Carbon::now(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                $amountupdate = TourBooking::findOrFail($request->bookingID);
                if ($amountupdate) {
                    $amountupdate->total = $data['total'];
                    if ($request->has('discount_type')) {
                        if ($request->discount_type == 'Percentage') {
                            $amountupdate->discount2_percentage = $request->discount_percentage;
                            $amountupdate->discount2_value = $discountAmt;
                        } elseif ($request->discount_type == 'Fixed Money') {
                            $amountupdate->discount2_value = $discountAmt;
                        }
                    }
                    $amountupdate->save();
                }

                $bookingT = DB::table('tour_bookings_transact AS tbt')
                    ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                    ->where('tbt.tour_booking_id', $request->bookingID)
                    ->get([
                        'tbt.id',
                        'tpr.processing_charge_percentage as original_processing_charge_percentage',
                    ]);

                foreach ($bookingT as $tbTransaction) {
                    $tourBookingTransaction = BookingTransact::find($tbTransaction->id);
                    $tourBookingTransaction->processing_charge_percentage = $tbTransaction->original_processing_charge_percentage;
                    $tourBookingTransaction->total = $tourBookingTransaction->total + ($tourBookingTransaction->total * $tbTransaction->original_processing_charge_percentage / 100);
                    $tourBookingTransaction->save();
                }
            }

            DB::commit();
            if(isset($request->api) && $request->api == 1) {
                return [
                    'tourBookingId' => $request->bookingID,
                    'totalBookedSeats' => $info->seats
                ];
            } else {
                return response()->json([
                    'tourBookingId' => $request->bookingID,
                    'totalBookedSeats' => $info->seats
                ]);
            }

        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Save affiliate bookings from calendar
     *
     * @param object $request
     * @return array
     */
    public function saveAffiliateBooking($request)
    {
        try{
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';
            $affiliate = DB::table('customers')
                ->where('id', $request->affiliate_id)
                ->where('status', 'Active')
                ->where('is_affiliate', 'Yes');

            if ($userType == 'Tour Operator')
                $affiliate = $affiliate->where('tour_operator_id', auth()->user()->tour_operator_id);

            $affiliate = $affiliate->first();
            if (!$affiliate) {
                throw new Exception('Unable to book/reserve seats or affiliate not found.');
            }

            $booking = DB::table('tour_bookings_master')
                ->where('tour_slot_id', $request->tour_slot_id)
                ->where('customer_id', $request->affiliate_id)
                ->where('status', '!=', 'Cancelled')
                ->orderBy('id', 'DESC')
                ->first(['id', 'tourists', 'created_at', 'status', 'total']);

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $request->tour_slot_id)
                ->select([
                    'tsd.seats', 'tsd.date', 'tsd.tour_package_id', 'tp.tour_operator_id', 'tp.name AS tour_package', 'ct.state_id', 'st.permit_fee',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    't_o.name AS tour_operator', 't_o.phone_number AS tour_op_phone', 't_o.email AS tour_op_email', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy', 'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring',
                    'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("CONCAT(tsd.date, ' ', tst.time_from) AS tour_d_t"),
                    DB::raw("(SELECT SUM(tbm.tourists)
					FROM tour_bookings_master AS tbm
					WHERE tbm.tour_slot_id = tst.id
						AND tbm.status != 'Cancelled'
					GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator')
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);

            $info = $info->first();

            if (!$info)
                throw new Exception('Unable to book/reserve seats.');

            $trans = null;
            if ($booking) {
                $trans = Transaction::where('tour_booking_id', $booking->id)->orderBy('id', 'DESC')->first();
            }

            if ($request->seats == 0 && $trans == null) {
                TourBooking::where('id', $booking->id)
                    ->update([
                        'status' => 'Cancelled',
                        'updated_by' => auth()->id(),
                        'updated_by_user_type' => $userType
                    ]);

                if ($userType == "Admin") {
                    $user = Admin::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                } elseif ($userType == "Tour Operator") {
                    $user = Staff::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                } else {
                    $user = Customer::find(Auth::id());
                    $created_by_with_name = $user->name . ': (' . "Affiliate" . ')';
                }

                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $booking->id,
                        'tourists' => $booking->tourists,
                        'operation' => 'Cancelled',
                        'details' => json_encode(['amount' => $booking->total, 'payment_type' => '', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                        'operation_at' => Carbon::now(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);

                if (isset($request->api) && $request->api == 1) {
                    return [
                        'tourBookingId' => $booking->id,
                        'totalBookedSeats' => $info->seats,
                    ];
                } else {
                    return response()->json([
                        'tourBookingId' => $booking->id,
                        'totalBookedSeats' => $info->seats,
                    ]);
                }
            } elseif ($request->seats == 0 && $trans != null) {
                if ($trans->via == "Cash" || $trans->via == "Invoiced") {
                    TourBooking::where('id', $booking->id)
                        ->update([
                            'status' => 'Cancelled',
                            'updated_by' => auth()->id(),
                            'updated_by_user_type' => session('userType')
                        ]);

                    if ($userType == "Admin") {
                        $user = Admin::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                    } elseif ($userType == "Tour Operator") {
                        $user = Staff::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    } else {
                        $user = Staff::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    }
                    if ($trans->via == "Cash") {
                        $payment_type = "Cash";
                    } else {
                        $payment_type = "Invoiced";
                    }
                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $booking->id,
                            'tourists' => $booking->tourists,
                            'operation' => 'Cancelled',
                            'details' => json_encode(['amount' => $booking->total, 'payment_type' => $payment_type, 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                            'operation_at' => Carbon::now(),
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);

                    if (isset($request->api) && $request->api == 1) {
                        return [
                            'tourBookingId' => $booking->id,
                            'totalBookedSeats' => $info->seats,
                        ];
                    } else {
                        return response()->json([
                            'tourBookingId' => $booking->id,
                            'totalBookedSeats' => $info->seats,
                        ]);
                    }
                } else {
                    TourBooking::where('id', $booking->id)
                        ->update([
                            'status' => 'Cancelled',
                            'updated_by' => auth()->id(),
                            'updated_by_user_type' => $userType
                        ]);

                    if ($userType == "Admin") {
                        $user = Admin::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Admin" . ')';
                    } elseif ($userType == "Tour Operator") {
                        $user = Staff::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    } else {
                        $user = Staff::find(Auth::id());
                        $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
                    }
                    $net_amount = $trans->amount - $trans->fee_amount;
                    $stripe = new StripeClient(env('STRIPE_API_SECRET_KEY'));
                    $refund = $stripe->refunds->create([
                        'payment_intent' => $trans->transaction_number,
                        'amount' => $net_amount * 100,
                        'reverse_transfer' => true,
                    ]);
                    $Transaction = new Transaction;
                    $Transaction->tour_booking_id = $booking->id;
                    $Transaction->amount = $net_amount;
                    $Transaction->transaction_number = $trans->transaction_number;
                    $Transaction->via = 'Card';
                    $Transaction->response_init = '';
                    $Transaction->response_end = '';
                    $Transaction->response_end_datetime = Carbon::now();
                    $Transaction->status = 'Refund';
                    $Transaction->visitor = $request->getClientIp();
                    $Transaction->request_from = $request->is('api/*') ? Constants::API : Constants::BACKEND;
                    $Transaction->save();

                    DB::table('tour_booking_history')
                        ->insert([
                            'tour_booking_id' => $booking->id,
                            'tourists' => $booking->tourists,
                            'operation' => 'Cancelled',
                            'details' => json_encode(['amount' => $booking->total, 'payment_type' => 'Card', 'created_by_with_name' => $created_by_with_name], JSON_NUMERIC_CHECK),
                            'operation_at' => Carbon::now(),
                            'operation_by' => auth()->id(),
                            'operation_by_user_type' => $userType,
                            'updated_at' => Carbon::now(),
                            'created_at' => Carbon::now(),
                        ]);

                    if (isset($request->api) && $request->api == 1) {
                        return [
                            'tourBookingId' => $booking->id,
                            'totalBookedSeats' => $info->seats,
                        ];
                    } else {
                        return response()->json([
                            'tourBookingId' => $booking->id,
                            'totalBookedSeats' => $info->seats,
                        ]);
                    }
                }
            }

            $seatsToBook = $request->seats;

            $numOfEmptySeats = $info->seats - $info->booked_seats;

            if ($booking) { // blocks are already booked for the affiliate
                $seatsToBook = $seatsToBook - $booking->tourists;
            }

            DB::beginTransaction();

            if ($booking != null && $trans == null) { // if already booked then update

                $qryM = DB::table('tour_bookings_master AS tbm')
                    ->where('tbm.id', $booking->id);
                $qryT = DB::table('tour_bookings_transact AS tbt')
                    ->join('tour_bookings_master AS tbm', 'tbm.id', '=', 'tbt.tour_booking_id')
                    ->where('tbt.tour_booking_id', $booking->id);

                if ($userType == 'Tour Operator') {
                    $qryM = $qryM->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                        ->where('tp.tour_operator_id', auth()->user()->tour_operator_id);

                    $qryT = $qryT->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                        ->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                }

                $rateGroup = $qryT->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                    ->first(['tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage', 'tpr.processing_charge_percentage', 'tpr.additional_charge']);

                $updateTotal = $stateTax = 0;
                $updateTotal = $discountAmt = 0;

                $updateTotal = (
                    ($rateGroup->rate) + ($rateGroup->additional_charge)
                    + $info->permit_fee
                );

                if ($updateTotal > 0) {
                    $updateTotal += (float)($updateTotal) *  ((float)$rateGroup->additional_tax_percentage) / 100;

                    $updateTotal = $updateTotal * $request->seats;
                    $updateTotal = number_format((float)$updateTotal, 2, '.', '');
                }

                if (!$rateGroup) {
                    throw new Exception('Unable to update affiliate booking data.');
                }
                $updateDT = mysqlDT();

                $uFieldValueM = [
                    'tbm.updated_at' => $updateDT,
                    'tbm.updated_by' => auth()->id(),
                    'tbm.updated_by_user_type' => $userType,
                ];

                $uFieldValueT = [
                    'tbt.updated_at' => $updateDT,
                    'tbt.updated_by' => auth()->id(),
                    'tbt.updated_by_user_type' => $userType,
                ];

                if ($request->seats) {
                    $qryM->update([
                            'tbm.tourists' => $request->seats,
                            'tbm.total' => $updateTotal,
                            'tbm.booking_total' => $updateTotal,
                            'tbm.status' => DB::raw("IF(tbm.status = 'Cancelled', 'Booked', tbm.status)"),
                        ] + $uFieldValueM);

                    $qryT->update(['tbt.tourists' => $request->seats, 'tbt.total' => $updateTotal] + $uFieldValueT);

                    $seatsDiff = $request->seats - $booking->tourists;
                } else { // zero seats then cancel the booking
                    $qryM->update([
                            'tbm.status' => 'Cancelled',
                        ] + $uFieldValueM);

                    $qryT->update(['tbt.tourists' => 0,] + $uFieldValueT);
                    $seatsDiff = -$booking->tourists;
                }

                DB::commit();
            } else { // add new booking
                if ($request->seats == 0)
                    return sendJsonErrMsg('No seat was booked for the affiliate so no seat is added/updated.');

                $rateGroup = $this->tourPackageRate::where('tour_package_id', $info->tour_package_id)
                    ->where('status', 'Active')
                    ->when($request->affiliate_id, function ($q) use ($request) {
                        return $q->where('affiliate_id', $request->affiliate_id);
                    })
                    ->where('scheduling_year', date('Y', strtotime($info->date)))
                    ->first();

                if (!$rateGroup) {
                    $rateGroup = $this->tourPackageRate::where('tour_package_id', $info->tour_package_id)
                        ->where('scheduling_year', date('Y', strtotime($info->date)))
                        ->where('status', 'Active')
                        ->first();
                }

                $rateGroup = $rateGroup->toArray();

                $groupTotal = $stateTax = 0;
                $groupTotal = $discountAmt = 0;

                $taxes = Tax::where([
                    'state_id' => $info->state_id,
                    'status' => 'Active',
                    'deleted_by' => 0
                ])
                    ->get(['value', 'value_type']);

                if ($rateGroup['rate'] != 0) {
                    for ($c = 0, $cc = count($taxes); $c < $cc; $c++) {
                        $stateTax = ($taxes[$c]->value_type == 'Percent'
                            ? $rateGroup['rate'] * $taxes[$c]->value / 100
                            : $taxes[$c]->value
                        );
                    }

                    $groupTotal = ($request->seats * ($rateGroup['rate'] +
                                $rateGroup['additional_charge'] +
                                $info->permit_fee
                            )) + $stateTax;
                }

                if ($request->has('discount_type')) {
                    if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage))
                        $discountAmt = round(($groupTotal * $request->discount_percentage) / 100, 2);
                    elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money))
                        $discountAmt = round($request->discount_fixed_money, 2);
                }

                $groupTotal -= $discountAmt;
                $groupTotal = round($groupTotal, 2);
                $booking = $this->tourBooking::create([
                    'tour_package_id' => $info->tour_package_id,
                    'tour_slot_id' => $request->tour_slot_id,
                    'customer_id' => $request->affiliate_id,
                    'name' => $affiliate->name,
                    'email' => $affiliate->email,
                    'phone_number' => $affiliate->phone_number,
                    'tourists' => $request->seats,
                    'discount2_value' => $discountAmt,
                    'discount2_percentage' => empty($request->discount_percentage) ? 0 : $request->discount_percentage,
                    'total' => $groupTotal,
                    'booking_total' => $groupTotal,
                    'status' => 'Booked',
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'updated_at' => '0000-00-00 00:00:00',
                ]);

                $this->bookingTransact::create([
                    'tour_booking_id' => $booking->id,
                    'tour_package_rate_id' => $rateGroup['id'],
                    'tourists' => $request->seats,
                    'processing_charge_percentage' => 0,
                    'nat_fees_percentage' => $info->affiliate_processing_percentage,
                    'stripe_fees_percentage' => $info->stripe_affiliate_percentage,
                    'additional_tax_percentage' => $rateGroup['additional_tax_percentage'],
                    'state_tax' => $stateTax,
                    'total' => $groupTotal,
                    'additional_charge' => $rateGroup['additional_charge'],
                    'permit_fee' => ($rateGroup['rate'] == 0 ? 0 : $info->permit_fee),
                    'rate' => $rateGroup['rate'],
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                ]);

                $seatsDiff = $request->seats;
                DB::commit();
            }

            $booking = [
                'id' => $booking->id,
                'cust_name' => $affiliate->name,
                'pkg_id' => $info->tour_package_id,
                'pkg_name' => $info->tour_package,
                'tour_slot_id' => $request->tour_slot_id,
                'tour_d_t' => $info->tour_d_t,
                'tourists' => $request->seats,
                'seats_diff' => $seatsDiff,
                't_groups' => isset($rateGroup->rate_for) ? $rateGroup->rate_for : "" . ':' . $seatsDiff,
                'tour_operator_id' => $info->tour_operator_id,
                'booking_d_t' => $booking->created_at,
                'paid' => ($request->payment_method == 'Cash' ? ((isset($groupTotal) && $groupTotal) ? $groupTotal : $updateTotal) : 0),
                'total' => (isset($groupTotal) && $groupTotal) ? $groupTotal : $updateTotal,
                'notif_for' => 'admin',
                'is_affiliate' => 'Yes',
            ];

            $tourListwithSeat = DB::table('tour_bookings_transact as tbm')
                ->join('tour_package_rates as tpr', 'tpr.id', '=', 'tbm.tour_package_rate_id')
                ->where('tbm.tour_booking_id', '=', $booking['id'])
                ->select('tpr.rate_for', 'tbm.tourists')
                ->get();

            $arEmailData = [
                'bookingId' => $booking['id'],
                'tourOperatorId' => $info->tour_operator_id,
                'tourOperator' => $info->tour_operator,
                'tourOpPhone' => $info->tour_op_phone,
                'tourOpEmail' => $info->tour_op_email,
                'tourOpWebsite' => $info->tour_op_website,
                'tourPackage' => $info->package_name,
                'tourDateTime' => date('l, F j, Y @ g:i a', strtotime($booking['tour_d_t'])),
                'urlBookingInfo' => route('customer.tour-booking-info', ['bookingId' => Crypt::encryptString($booking['id'])]),
                'custName' => $affiliate->name,
                'custEmail' => $affiliate->email,
                'custPhone' => $affiliate->phone_number,
                'urlLiabilityWaiver' => route('customer.liability-waiver-form', ['bookingId' => Crypt::encryptString($booking['id'])]),
                'importantNotes' => $info->important_notes,
                'thingsToBring' => $info->things_to_bring ? json_decode($info->things_to_bring) : [],
                'cancellationPolicy' => $info->cancellation_policy,
                'numberoftourist' => $request->seats,
                'tourListwithSeat' => $tourListwithSeat,
                'packageEmail' => $info->packageEmail,
                'packageFromEmail' => $info->packageFromEmail,
                'packageApiKey' => $info->packageApiKey,
            ];

            if (!empty($info->tour_op_logo)) {
                if (Storage::disk('s3')->exists('images/tour-operator/logo/' . $info->tour_op_logo)) {
                    $tourOperatorLogo = Storage::disk('s3')->url('images/tour-operator/logo/' . $info->tour_op_logo);
                } else {
                    $tourOperatorLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourOpLogo'] = $tourOperatorLogo;
            }

            $tourPkgPhoto = DB::table('tour_package_photos')
                ->where('tour_package_id', $info->tour_package_id)
                ->where('placement', 2)
                ->where('status', 'Active')
                ->where('deleted_by', 0)
                ->latest()->first(['path']);

            if ($tourPkgPhoto) {
                if (Storage::disk('s3')->exists('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path)) {
                    $tourPackageLogo = Storage::disk('s3')->url('images/tour-operator/package/' . $info->tour_operator_id . '/' . $tourPkgPhoto->path);
                } else {
                    $tourPackageLogo = asset('images/no-photo.png');
                }
                $arEmailData['tourPkgPhoto'] = $tourPackageLogo;
            }

            $trans = null;
            if (count($booking) > 0) {
                $trans = $this->transaction::where('tour_booking_id', $booking['id'])->orderBy('id', 'DESC')->first();
            }

            // TODO
            $affiliate = $this->customer::find($affiliate->id);
            if(auth()->id()!=130) {
                $affiliate->notify(new TourBookingAcknowledgement($arEmailData, 'tour_booking_acknoledgement'));
            }
            $booking['env'] = config('app.env');
            \App\Events\BookingAffiliate::dispatch($booking);
            $booking['notif_for'] = 'Tour Operator';
            \App\Events\BookingAffiliate::dispatch($booking);
            if(isset($request->api) && $request->api == 1){
                return [
                    'tourBookingId' => $booking['id'],
                    'totalBookedSeats' => $info->seats - $numOfEmptySeats + $seatsToBook
                ];
            } else {
                return response()->json([
                    'tourBookingId' => $booking['id'],
                    'totalBookedSeats' => $info->seats - $numOfEmptySeats + $seatsToBook
                ]);
            }
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Check the overbooking for the specific slot user trying to book
     *
     * @param object $request
     * @return array
     */
    public function checkOverbooking($request)
    {
        try {
            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->where('tst.id', $request->slot_time)
                ->when($userType == 'Tour Operator', function ($query) {
                    $query->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
                })
                ->select([
                    'tsd.seats',DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ])
                ->first();

            return $info;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * View and print the invoice of booking
     *
     * @param int $id
     */
    public function bookingInvoicePrint(int $id)
    {
        try {
            $bookingM = DB::table('tour_bookings_master AS tbm')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tbm.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('tour_slot_times AS tst', 'tst.id', '=', 'tbm.tour_slot_id')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('customers AS c', 'c.id', '=', 'tbm.customer_id')
                ->leftJoin('tour_promotions AS promo', 'promo.id', '=', 'tbm.tour_promotion_id')
                ->where('tbm.id', $id)
                ->first([
                    'tbm.id', 'tbm.tourists', 'tbm.status', 'tbm.total', 'tbm.note', 'tbm.discount', 'tbm.discount2_value', 'tbm.discount2_percentage', 'tbm.service_commission', 'tbm.created_at',
                    'tp.name AS tour_package', 't_o.name AS tour_operator',
                    't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.logo',
                    'tsd.date', 'tst.time_from', 'tst.time_to',
                    'tbm.name AS customer', 'tbm.email AS cust_email', 'tbm.phone_number AS cust_phone',
                    'promo.discount_value', 'promo.discount_value_type','tbm.booking_total'
                ]);

            if (!$bookingM) {
                throw new Exception("Tour booking not found.");
            }
            $bookingT = DB::table('tour_bookings_transact AS tbt')
                ->join('tour_package_rates AS tpr', 'tpr.id', '=', 'tbt.tour_package_rate_id')
                ->where('tbt.tour_booking_id', $id)
                //->orderByDesc('tpr.age_from')
                //->orderByDesc('tpr.age_to')
                ->orderBy('tpr.orderby', 'asc')
                ->get([
                    'tbt.id', 'tbt.tourists', 'tbt.processing_charge_percentage', 'tbt.additional_tax_percentage',
                    'tbt.permit_fee', 'tbt.additional_charge', 'tbt.state_tax', 'tbt.rate', 'tbt.total',
                    'tpr.rate_for'
                ]);

            if (!$bookingT) {
                abort(500, "Unable to get booking's information.");
            }

            $history = DB::table('tour_booking_history')
                ->orderByDesc('operation_at')
                ->where('tour_booking_id', $id)
                ->get();

            $transaction = Transaction::where('tour_booking_id', $id)->first();
            $trans = Transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'DESC')->first();
            $HistoryPaymentTime = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->first();

            if (!empty($HistoryPaymentTime)) {
                $TourBookingHistory = TourBookingHistory::where('tour_booking_id', $id)->where('operation_at', '>', $HistoryPaymentTime->operation_at)
                    ->where(function ($query) {
                        return $query->where('operation', '=', 'Refund')
                            ->orWhere('operation', '=', 'Cancelled');
                    })
                    ->get();
            }

            $paymentOnly = TourBookingHistory::where('tour_booking_id', $id)->where('operation', 'Payment')->get();
            $paymentTrans = Transaction::where('tour_booking_id', $id)->where('status', 'Completed')->get(['id','tour_booking_id','amount','fee_amount','transaction_number','application_fee_id','via','payment_type','status']);

            $paymentTransCount = count($paymentTrans);
            $paymentOnlyCheck = 0;
            foreach ($paymentOnly as $value) {
                $details = json_decode($value->details);
                $paymentOnlyCheck += $details->amount;
            }

            $transP = Transaction::where('tour_booking_id', $id)->where('status', 'Completed')->orderBy('id', 'ASC')->first();

            $vars = [
                'page_title' => 'Tour Booking Info',
                'bookingM' => $bookingM,
                'bookingT' => $bookingT,
                'history' => $history,
                'transaction' => $transaction,
                'TourBookingHistory' => $TourBookingHistory ?? '',
                'HistoryPaymentTime' => $HistoryPaymentTime ?? '',
                'trans' => $trans,
                'paymentOnlyCheck' => $paymentOnlyCheck,
                'transP' => $transP,
                'paymentTransCount'=>$paymentTransCount
            ];

            return $vars;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Build this method to store the booking before capture payment
     *
     * @param object $request
     */
    public function cardReaderBeforeCapture($request)
    {
        DB::beginTransaction();
        try {
            if(isset($request->permit_fee[0])) {
                $request->permit_fee = explode(",", $request->permit_fee[0]);
            }
            if(isset($request->rate_group[0])) {
                $request->rate_group = explode(",", $request->rate_group[0]);
            }
            if(isset($request->tourists[0])) {
                $request->tourists = explode(",", $request->tourists[0]);
            }

            $totalTourists = 0;

            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                $totalTourists += $request->tourists[$a];
            }

            if (!$totalTourists) {
                throw new Exception('Please select at least one tourist.');
            }

            $userType = auth()->user()->tour_operator_id ? 'Tour Operator' : 'Admin';

            $info = DB::table('tour_slot_times AS tst')
                ->join('tour_slot_dates AS tsd', 'tsd.id', '=', 'tst.tour_slot_id')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tsd.tour_package_id')
                ->join('tour_operators AS t_o', 't_o.id', '=', 'tp.tour_operator_id')
                ->join('cities AS ct', 'ct.id', '=', 't_o.city_id')
                ->join('states AS st', 'st.id', '=', 'ct.state_id')
                ->where('tst.id', $request->slot_time)
                ->select([
                    'tst.time_from', 'tst.time_to', 'tsd.date', 'tsd.seats', 'tp.tour_operator_id', 'tsd.tour_package_id',
                    'tp.name AS package_name', 'tp.important_notes', 'tp.things_to_bring', 'ct.state_id', 'st.permit_fee',
                    't_o.name AS tour_operator', 't_o.email AS tour_op_email', 't_o.phone_number AS tour_op_phone', 't_o.website AS tour_op_website', 't_o.logo AS tour_op_logo', 't_o.cancellation_policy',
                    't_o.service_commission_percentage', 't_o.affiliate_processing_percentage', 't_o.stripe_affiliate_percentage', 't_o.stripe_commission_percentage',
                    'tp.stripe_destination_id', 't_o.id as tour_operator_id', 'tp.email as packageEmail', 'tp.from_email as packageFromEmail', 'tp.api_key as packageApiKey',
                    DB::raw("(SELECT SUM(tbm.tourists)
                        FROM tour_bookings_master AS tbm
                        WHERE tbm.tour_slot_id = tst.id
                            AND tbm.status != 'Cancelled'
                        GROUP BY tbm.tour_slot_id) AS booked_seats")
                ]);

            if ($userType == 'Tour Operator') {
                $info = $info->where('tp.tour_operator_id', auth()->user()->tour_operator_id);
            }

            $info = $info->first();

            if (!$info) {
                throw new Exception('Unable to book the tour package.');
            }

            if ($info->date . ' ' . $info->time_from <= mysqlDT()) {
                throw new Exception("The tour's date-time has passed; booking isn't possible in the past.");
            }

            $rateGrpIds = [];
            $permitfeeAr = [];

            for ($a = 0, $c = count($request->tourists); $a < $c; $a++) {
                if ($request->tourists[$a]) {
                    $x = $a;
                    $rateGrpIds[] = $request->rate_group[$a];
                    $permitfeeAr[] = $request->permit_fee[++$x];
                }
            }

            $rateGroups = DB::table('tour_package_rates AS tpr')
                ->join('tour_packages AS tp', 'tp.id', '=', 'tpr.tour_package_id')
                ->whereIn('tpr.id', $rateGrpIds)
                ->where('tpr.deleted_by', 0)
                ->orderBy('tpr.orderby', 'asc')
                ->get([
                    'tpr.id', 'tpr.rate_for', 'tpr.rate', 'tpr.additional_tax_percentage',
                    'tpr.processing_charge_percentage', 'tpr.additional_charge'
                ]);

            $customer = $this->customer::where('email', $request->customer_email)
                ->where('tour_operator_id', $info->tour_operator_id)
                ->first();

            if (!$customer && $request->affiliatecust == 0) {
                $pwdChars = '23456789~!@#$%^*()-=+_[]{};:?.,>abcdefghijkpqstuxyzABCDEFGHJKLMNPQRSTUXYZ';
                $pwd = '';

                for ($i = 0, $cl = strlen($pwdChars) - 1; $i < 10; $i++) {
                    $pwd .= $pwdChars[rand(0, $cl)];
                }

                $customer = $this->customer::create([
                    'tour_operator_id' => $info->tour_operator_id,
                    'name' => $request->customer_name,
                    'email' => $request->customer_email,
                    'phone_number' => str_replace(['(', ')', '+', '-', '.', ' '], '', $request->customer_phone_number),
                    'password' => Hash::make(env('APP_DEBUG') ? '123456' : $pwd),
                ]);
                DB::commit();
            }

            $total = $discountAmt = 0;
            $tbTransact = $touristGroups = [];

            for ($a = 0, $ac = count($request->tourists); $a < $ac; $a++) {
                if (!$request->tourists[$a])
                    continue;

                $tourists = $request->tourists[$a];
                $groupTotal = 0;

                for ($b = 0, $bc = count($rateGroups); $b < $bc; $b++) {
                    if ($request->rate_group[$a] == $rateGroups[$b]->id) {
                        $touristGroups[] = $rateGroups[$b]->rate_for[0] . ':' . $tourists;
                        break;
                    }
                }

                if ($b >= $bc) { // rate group not found
                    DB::rollBack();
                    throw new Exception('Unable to book the tour package.');
                }

                // rate may be zero for one or more age/rate group and
                // if rate is non-zero then only add other amounts
                $groupTotal = ($tourists * ($rateGroups[$b]->rate +
                        $rateGroups[$b]->additional_charge +
                        $permitfeeAr[$b]
                    ));

                if ($request->payment_method === 'Card') {
                    $groupTotal += ($groupTotal * (float)$rateGroups[$b]->processing_charge_percentage) / 100;
                }

                if ($request->subtt == null) {
                    $request->subtt = 0.00;
                }

                if ($request->fee == null) {
                    $request->fee = 0.00;
                }

                $tt = $request->subtt + $request->fee;

                $natFeesPercentage = 0;
                $stripeFeesPercentage = 0;
                if ($customer->is_affiliate == 'Yes') {
                    $natFeesPercentage = $info->affiliate_processing_percentage;
                    $stripeFeesPercentage = $info->stripe_affiliate_percentage;
                } else {
                    $natFeesPercentage = $info->service_commission_percentage;
                    $stripeFeesPercentage = $info->stripe_commission_percentage;
                }

                $tbTransact[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'processing_charge_percentage' => (($request->payment_method !== 'Card') ? 0 : $rateGroups[$b]->processing_charge_percentage),
                    'nat_fees_percentage' => $natFeesPercentage,
                    'stripe_fees_percentage' => $stripeFeesPercentage,
                    'additional_tax_percentage' => ($rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => ($rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => $groupTotal,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];

                /* START - Added this to place the history of person added */
                $tbTransaction[] = [
                    'tour_package_rate_id' => $rateGroups[$b]->id,
                    'tourists' => $tourists,
                    'rate_for' => $rateGroups[$b]->rate_for,
                    'processing_charge_percentage' => (($request->payment_method !== 'Card') ? 0 : $rateGroups[$b]->processing_charge_percentage),
                    'nat_fees_percentage' => $natFeesPercentage,
                    'stripe_fees_percentage' => $stripeFeesPercentage,
                    'additional_tax_percentage' => ($rateGroups[$b]->additional_tax_percentage),
                    'additional_charge' => ($rateGroups[$b]->additional_charge),
                    'permit_fee' => $permitfeeAr[$b],
                    'rate' => $rateGroups[$b]->rate,
                    'total' => $groupTotal,
                    'created_by' => auth()->id(),
                    'created_by_user_type' => $userType,
                    'created_at' => Carbon::now(),
                ];
                /* END - Added this to place the history of person added */

                $total += $groupTotal;
            }

            if ($request->has('discount_type')) {
                if ($request->discount_type == 'Percentage' && !empty($request->discount_percentage)) {
                    $discountAmt = round(($total * $request->discount_percentage) / 100, 2);
                } elseif ($request->discount_type == 'Fixed Money' && !empty($request->discount_fixed_money)) {
                    $discountAmt = round($request->discount_fixed_money, 2);
                }
            }

            $total -= $discountAmt;
            $serviceCommission = $total * $info->service_commission_percentage / 100;

            $total += $serviceCommission;
            $total = round($total, 2);

            $tourBooking = $this->tourBooking::create([
                'tour_package_id' => $info->tour_package_id,
                'tour_slot_id' => $request->slot_time,
                'customer_id' => $request->affiliatecust == 0 ? $customer->id : $request->affiliatecust,
                'tourists' => $totalTourists,
                'name' => $request->customer_name,
                'email' => $request->customer_email,
                'phone_number' => $request->customer_phone_number,
                'comments' => $request->customer_comment,
                'note' => ($request->has('tour_operator_note'))?$request->tour_operator_note:'',
                'note_datetime' => empty($request->tour_operator_note) ? '0000-00-00 00:00:00' : mysqlDT(),
                'mask_required' => 'Yes',
                'discount2_value' => $discountAmt,
                'discount2_percentage' => empty($request->discount_percentage) ? 0 : $request->discount_percentage,
                'service_commission' => ($request->payment_method == "Card") ? $request->fee : 0,
                'total' => ($request->payment_method == "Card") ? $tt : $request->subtt,
                'booking_total' => ($request->payment_method == "Card") ? $tt : $request->subtt,
                'status' => 'Cancelled',
                'created_by' => empty($request->booked_by_staff) ? auth()->id() : $request->booked_by_staff,
                'created_by_user_type' => empty($request->booked_by_staff) ? $userType : 'Tour Operator',
                'updated_at' => '0000-00-00 00:00:00',
            ]);
            DB::commit();

            if (!empty($request->customer_comment)) {
                DB::table('tour_booking_history')
                    ->insert([
                        'tour_booking_id' => $tourBooking->id,
                        'operation' => 'Comments',
                        'details' => $request->customer_comment,
                        'operation_at' => mysqlDT(),
                        'operation_by' => auth()->id(),
                        'operation_by_user_type' => $userType,
                        'updated_at' => Carbon::now(),
                        'created_at' => Carbon::now(),
                    ]);
                DB::commit();
            }

            for ($a = 0, $ac = count($tbTransact); $a < $ac; $a++) {
                $tbTransact[$a]['tour_booking_id'] = $tourBooking->id;
            }

            DB::table('tour_bookings_transact')->insert($tbTransact);
            DB::commit();

            if ($userType == "Admin") {
                $user = $this->admin::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Admin" . ')';
            } elseif ($userType == "Tour Operator") {
                $user = $this->staff::find(auth()->id());
                $created_by_with_name = $user->name . ': (' . "Tour Operator" . ')';
            }

            /* START - Added this to place the history of person added */
            $addedPeoplesText = array();
            if (!empty($tbTransaction)) {
                foreach ($tbTransaction as $tblTransaction) {
                    $addedPeoplesText[] = $tblTransaction['rate_for'] . " person " . $tblTransaction['tourists'];
                }
            }

            $operationTouristsAdded = [
                'tour_booking_id' => $tourBooking->id,
                'tourists' => $tourBooking->tourists,
                'operation' => 'Added',
                'details' => json_encode([
                    'sub_operation' => 'Tourists Added',
                    'discount_percentage' => $request->discount_percentage?:0,
                    'discount2_value' => $discountAmt,
                    'added' => $addedPeoplesText,
                    'newTourists' => $totalTourists,
                    'created_by_with_name' => $created_by_with_name
                ], JSON_NUMERIC_CHECK),
                'operation_at' => mysqlDT(), // laravel doesn't seems to honouring timezone given in config > app.php!
                'operation_by' => auth()->id(),
                'operation_by_user_type' => $userType,
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ];

            DB::table('tour_booking_history')->insert($operationTouristsAdded);
            /* END - Added this to place the history of person added */

            $master = $this->tourBooking::find($tourBooking->id);

            $vars = [
                'booking_id' => $tourBooking->id,
                'total' => $tt,
                'discount2_value' => $discountAmt,
                'customer_id' => $master->customer_id,
                'customer_name' => $master->name,
                'customer_email' => $master->email,
                'customer_phone' => $master->phone_number,
                'tourists' => $totalTourists,
                'tour_package' => $info->package_name,
                'date' => $info->date,
                'time_from' => $info->time_from,
                'subtotal' => $request->subtt,
                'serviceCommission' => $request->fee,
                'destination' => $info->stripe_destination_id,
                'slot_time' => $request->slot_time,
                'tour_operator_id' => $info->tour_operator_id,
                'note' => $request->tour_operator_note,
                'booked_by_staff' => $request->booked_by_staff,
                'split_value' => $request->split_value
            ];

            return $vars;
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }

    /**
     * Capture stripe payment using payment intent id
     *
     * @param object $request
     * @return array
     */
    public function cardReaderAfterCapture($request)
    {
        try {
            $stripe = new StripeClient(
                env('STRIPE_API_SECRET_KEY')
            );

            $paymentCapture = $stripe->paymentIntents->capture(
                $request->payment_intent_id,
                []
            );

            $master = $this->tourBooking::find($request->booking_id);

            $data = [
                'booking_id' => $request->booking_id,
                'total' => $request->total,
                'discount2_value' => $request->discount2_value,
                'customer_id' => $request->customer_id,
                'customer_name' => $request->customer_name,
                'customer_email' => $request->customer_email,
                'customer_phone' => $request->customer_phone,
                'tourists' => $request->tourists,
                'tour_package' => $request->tour_package,
                'date' => $request->date,
                'time_from' => $request->time_from,
                'subtotal' => $request->subtotal,
                'serviceCommission' => $request->serviceCommission,
                'destination' => $request->stripe_destination_id,
                'slot_time' => $request->slot_time,
                'tour_operator_id' => $request->tour_operator_id,
                'note' => $request->tour_operator_note,
                'booked_by_staff' => $request->booked_by_staff,
                'split_value' => $request->split_value,
                'api' => $request->api
            ];

            $paymentIntentId= ($request->has('payment_intent_id') && $request->payment_intent_id!='')?$request->payment_intent_id:'';
            if ($paymentIntentId == '') {
                throw new Exception('You must provide Stripe Payment Intent Id');
            }

            $data['payment_intent_id'] = $paymentIntentId;
            $data['after_capture'] = 1;
            $paymentIntentInfo = $this->createBookingCard($data);

            $errors = [];
            if (isset($paymentIntentInfo['nextAction'])) {
                $master->stripe_paymentintent_id = $paymentIntentInfo['clientSecret'];
                $master->save();

                return [
                    'status' => $paymentCapture->status,
                    'payment_intent_id' => $request->payment_intent_id,
                    'bookingId' => $request->booking_id,
                    'bookingToken' => Crypt::encryptString($request->booking_id), // for security purpose
                    'stripeId' => $paymentIntentInfo['clientSecret'],
                    'require3ds' => 1,
                    'nextAction' => $paymentIntentInfo['nextAction'],
                    'errors' => $errors,
                    'errors_message' => !empty($errors_message) ? $errors_message : '',
                ];
            }

            if ($paymentIntentInfo['clientSecret'] == '') {
                $errors[] = 'payment';
            }

            if (!empty($paymentIntentInfo['ex'])) {
                $errors_message = $paymentIntentInfo['ex']->getError()->message;
            }

            if ($paymentCapture->status == 'succeeded') {
                $master->status = 'Booked';
                $master->save();
            }

            return [
                'status' => $paymentCapture->status,
                'payment_intent_id' => $request->payment_intent_id,
                'bookingId' => $request->booking_id,
                'bookingToken' => Crypt::encryptString($request->booking_id), // for security purpose
                'stripeId' => $paymentIntentInfo['clientSecret'],
                'transactionId' => Crypt::encryptString($paymentIntentInfo['transactionId']),
                'errors' => $errors,
                'errors_message' => !empty($errors_message) ? $errors_message : '',
            ];
        } catch (Exception $exception) {
            Log::error($exception);
            throw new GeneralCoreException($exception);
        }
    }
}
