<?php

namespace app\models;



use Yii;
use app\models\ReportContent;

/**
 * This is the model class for table "donation_comments".
 *
 * @property integer $d_com_id
 * @property integer $d_id
 * @property integer $user_id
 * @property string $comments
 * @property string $created_at
 * @property integer $status
 */
class DonationComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donation_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_id', 'user_id', 'comments'], 'required'],
            [['d_id', 'user_id', 'status','parent_id'], 'integer'],
            [['comments'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'd_comment_id' => 'd Com ID',
            'parent_id' => 'Parent ID',
            'd_id' => 'donation ID',
            'user_id' => 'User ID',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    public function getLikes()
    {
        return $this->hasMany(DonationCommentsActivities::className(), ['d_comment_id' => 'd_comment_id']);
    }
    
    public function getLikesCount()
    {
        return DonationCommentsActivities::find()->where(['d_comment_id'=>$this->d_comment_id])->count();
    }
    public function getCommenterModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }
    public function getCommentReply()
    {
        return $this->hasMany(DonationComments::className(), ['parent_id' => 'd_comment_id']);
    }
    public function isLiked($byUser){
            if(DonationCommentsActivities::find()->where(['d_comment_id'=>$this->d_comment_id,'user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }
}
