<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "wish_comments".
 *
 * @property integer $w_com_id
 * @property integer $w_id
 * @property integer $user_id
 * @property string $comments
 * @property string $created_at
 * @property integer $status
 */
class WishCommentsActivities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wish_comments_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['w_comment_id', 'user_id'], 'required'],
            [['w_comment_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'a_id' => 'A ID',
            'w_comment_id' => 'w Com ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
