<?php

namespace app\models;

use app\models\custom\CustomModel;
use Yii;
use yii\helpers\Url;

class BaseModel extends \yii\db\ActiveRecord
{
    public static function find()
    {
        return new CustomModel(get_called_class());
    }
}