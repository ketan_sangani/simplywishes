<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "editorial".
 *
 * @property integer $e_id
 * @property string $e_title
 * @property string $e_text
 * @property string $e_image
 * @property string $is_video_only
 * @property integer $status
 * @property string $created_at
 */
class Editorial extends \yii\db\ActiveRecord
{
    public $featured_video_upload;
		
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'editorial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_title'], 'required'],
            //[['e_image'], 'required','except' => 'update_by_editorial_admin'], 
            [['e_text'], 'string'],
            [['featured_video_url'], 'string'],
            [['status', 'is_video_only'], 'integer'],
            [['created_at'], 'safe'],
            [['e_title'], 'string', 'max' => 250],
            [['e_image'], 'file','extensions' => 'jpg,png,jpeg'],
            [['featured_video_upload'], 'file','extensions' => 'mp4,mov,wmv', 'maxSize' => 10240000, 'skipOnEmpty' => true],
            /*['featured_video_url', 'required', 'when' => function($model) {
                    return $model->is_video_only == 1;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#video_check').val() == 1;
                }"
            ]*/
        ];
    }

	
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update_by_editorial_admin'] = ['e_title','e_text','e_image','is_video_only','featured_video_url']; //Scenario Values Only Accepted
        return $scenarios;
    } 
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'e_id' => 'E ID',
            'e_title' => 'Title',
            'e_text' => 'Text',
            'e_image' => 'Image',
            'status' => 'Status',
            'is_video_only' => 'Is Video Only',
            'created_at' => 'Created At',
            'featured_video_url' => 'Insert the video url in order to upload',
            'featured_video_upload' => 'Upload from your Files',
        ];
    }
	
    public function uploadImage(){	
        $this->e_image->saveAs('web/uploads/editorial/' . $this->e_image->baseName . '.' .$this->e_image->extension);
        $this->e_image = 'uploads/editorial/'.$this->e_image->baseName .'.'.$this->e_image->extension;
        return true;	
    }
    
    public function isLiked($byUser){
            if(EditorialActivity::find()->where(['e_id'=>$this->e_id,'activity'=>'like','user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }
    
    public function getLikes()
    {
        return $this->hasMany(EditorialActivity::className(), ['e_id'=>'e_id'])->andOnCondition(['activity' => 'like']);
    }
    
    public function getLikesCount()
    {
        return EditorialActivity::find()->where(['e_id'=>$this->e_id,'activity'=>'like'])->count();
    }
}
