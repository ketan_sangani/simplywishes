<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "happy_stories_comments".
 *
 * @property integer $hs_com_id
 * @property integer $hs_id
 * @property integer $user_id
 * @property string $comments
 * @property string $created_at
 * @property integer $status
 */
class HappyStoriesComments extends BaseModel// \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'happy_stories_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hs_id', 'user_id', 'comments'], 'required'],
            [['hs_id', 'user_id', 'status','parent_id'], 'integer'],
            [['comments'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'hs_comment_id' => 'Hs Com ID',
            'parent_id' => 'Parent ID',
            'hs_id' => 'HS ID',
            'user_id' => 'User ID',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }


    public function getLikesCount()
    {
        return HappyStoriesCommentActivity::find()->where(['hs_comment_id'=>$this->hs_comment_id])->count();
    }
    
    public function getLikes()
    {
        return $this->hasMany(HappyStoriesCommentActivity::className(), ['hs_comment_id'=>'hs_comment_id']);
    }

    public function getCommenterModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }
    public function getCommentReply()
    {
        return $this->hasMany(HappyStoriesComments::className(), ['parent_id' => 'hs_comment_id']);
    }
    
    public function isLiked($byUser){
        if(HappyStoriesCommentActivity::find()->where(['hs_comment_id'=>$this->hs_comment_id,'user_id'=>$byUser])->one()!= null)
                return true;
        else return false;
    }
}
