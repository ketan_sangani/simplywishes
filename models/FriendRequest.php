<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "friend_request".
 *
 * @property integer $f_id
 * @property integer $requested_by
 * @property integer $requested_to
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class FriendRequest extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'friend_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requested_by', 'requested_to'], 'required'],
            [['requested_by', 'requested_to', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'f_id' => 'F ID',
            'requested_by' => 'Requested By',
            'requested_to' => 'Requested To',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUserModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'requested_by']);
    }

    public function getOtherUserModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'requested_to']);
    }

	 public function sendEmail($to,$f_id)
    {
        /* @var $user User */
         $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $to,
        ]); 
		
		
		 $fromuser = UserProfile::findOne([
            'user_id' => \Yii::$app->user->id,
        ]); 
		
        if (!$user) {
            return false;
        }
		
		if (!$fromuser) {
            return false;
        }
        $touserprofile = UserProfile::findOne([
            'user_id' => $to,
        ]);
        $otheruserprofile = Yii::$app->urlManager->createAbsoluteUrl(['account/profile','id'=>$fromuser->user_id]);
        $frdpagelink = Yii::$app->urlManager->createAbsoluteUrl(['account/my-friend?type=requests']);
        $confirmurl = Yii::$app->urlManager->createAbsoluteUrl(['friend/request-acceptedemail','id'=>$f_id]);
        $rejecturl = Yii::$app->urlManager->createAbsoluteUrl(['friend/request-rejectedemail','id'=>$f_id]);
        $subject = "Friend Request from ".ucfirst($fromuser->firstname).' '.ucfirst($fromuser->lastname);
        $confirmlink = Html::a('Confirm', $confirmurl,['style'=>'color:blue;text-decoration: underline;']);
        $rejectlink = Html::a('Ignore', $rejecturl,['style'=>'color:blue;text-decoration: underline;']);
        $friendpageurl = Html::a('Friends Page', Yii::$app->urlManager->createAbsoluteUrl(['account/my-friend?type=requests']));
        $myprofilepageurl = Html::a('My Profile', Yii::$app->urlManager->createAbsoluteUrl(['site/index-home#edt_home']));

        $user1 = Html::a($fromuser->firstname. ' ' .$fromuser->lastname, $otheruserprofile,['style'=>'color:blue;text-decoration: underline;']);
        $frdpage = Html::a('Friends Page', $frdpagelink,['style'=>'color:blue;text-decoration: underline;']);

            $msg = $user1.' would like to add you as a Friend! Please hit '.$confirmlink.' or '.$rejectlink.'.
             By confirming their request, each of you will be added to your each other’s '.$frdpage;
        $inboxmessage = new Message();
        $inboxmessage->sender_id = $fromuser->user_id;
        $inboxmessage->recipient_id = $to;
        $inboxmessage->parent_id = 0;
        $inboxmessage->friend_request_id = $f_id;
        $inboxmessage->read_text = 0;
        $inboxmessage->subject = $subject;
        $inboxmessage->text = $msg;
        $inboxmessage->created_at =  date("Y-m-d H:i:s");
        $inboxmessage->save(false);
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'friendRequest-html'],
                ['user' => $touserprofile ,'fromuser'=>$fromuser,'f_id'=>$f_id]
                
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject('You Have a Friend Request from '.ucfirst($fromuser->firstname).' '.ucfirst($fromuser->lastname));
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
		$message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
		
		return $message->send();
    }
	
}
