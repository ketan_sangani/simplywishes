<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activities".
 *
 * @property integer $a_id
 * @property integer $story_id
 * @property integer $user_id
 * @property string $activity
 * @property string $created_at
 */
class EditorialCommentActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'editorial_comment_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_comment_id', 'user_id'], 'required'],
            [['e_comment_id', 'user_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'a_id' => 'A ID',
            'e_comment_id' => 'Editorial Comment ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
