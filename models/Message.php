<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $m_id
 * @property integer $sender_id
 * @property integer $recipient_id
 * @property integer $parent_id
 * @property string $text
 * @property string $created_at
 */
class Message extends \yii\db\ActiveRecord
{
    public $cnt;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'recipient_id', 'parent_id','text'], 'required','on'=>'sendmessage'],
            [['sender_id', 'recipient_id', 'parent_id','w_id','donation_id','friend_request_id'], 'integer'],
            [['created_at'], 'safe'],
            [['text','subject'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'm_id' => 'M ID',
            'sender_id' => 'Sender ID',
            'recipient_id' => 'Recipient ID',
            'parent_id' => 'Parent ID',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'sender_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'recipient_id']);
    }
}
