<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "donation_comments_activities".
 *
 * @property integer $d_com_id
 * @property integer $d_id
 * @property integer $user_id
 * @property string $comments
 * @property string $created_at
 * @property integer $status
 */
class DonationCommentsActivities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donation_comments_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['d_comment_id', 'user_id'], 'required'],
            [['d_comment_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'a_id' => 'A ID',
            'd_comment_id' => 'd Com ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
