<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Properties".
 *
 * @property integer $id
 * @property string $max_no_wishes
 
 */
class Properties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'properties';
    }

}
