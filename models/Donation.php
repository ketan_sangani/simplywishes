<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\models\ReportContent;
use app\models\custom\CustomWish;

/**
 * This is the model class for table "donations".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $granted_by
 * @property string $title
 * @property string $summary_title
 * @property string $description
 * @property string $image
 * @property integer $state
 * @property integer $country
 * @property integer $city
 */
class Donation extends BaseModel// \yii\db\ActiveRecord
{
    public $auto_id;
    public $image_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donations';
    }

        /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['category', 'wish_title','state', 'country', 'city','expected_cost','expected_date'], 'required'],
            [['title','acknowledgement'], 'required'],


            [['non_pay_option'], 'required','on'=>'create','message' => 'You must select one of these options.'],
            [['financial_assistance'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['expected_cost'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['way_of_donation'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }"],
            [['description_of_way'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }",'message' => 'You must fill this details.'],
            //['delivery_type', 'required', 'message' => 'You must choose at least one {attribute}', 'on'=>'update'],

            [['created_by', 'granted_by'], 'integer'],
            [['description', 'image','way_of_donation','description_of_way'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['summary_title','who_can'], 'string', 'max' => 150],
            [['i_agree_decide2','i_agree_decide'], 'integer'],
            [['auto_id','status','image_name'], 'safe'],
            [['expected_cost'], 'integer','min' => 1],
    //	[['expected_cost'], 'in','range'=>range(100,1000),'message'=>'Expected Cost(USD) Range In 100 to 1000' ],
            [['show_mail_status','show_person_status','show_other_status'], 'integer'],
            [['show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','financial_assistance','financial_assistance_other'], 'string'],
            ['show_mail','email','message' => 'Enter valid email address'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'D ID',
            'created_by' => 'Created By',
            'granted_by' => 'Granted By',
            'title' => 'Donation Title',
            // 'summary_title' => 'Wish Summary',
            'description' => 'Donation Description',
            'image' => 'Primary Image',

               'delivery_type'=>'Delivery Type ',			
  

                'show_mail_status'=>'E-mail',
                'show_person_status'=>'Postal',
                'show_other_status'=>'Other',
                'show_mail'=>'Enter your email address',
                'show_person_street'=>'Street',
                'show_person_city'=>'City',
                'show_person_state'=>'State',
                'show_person_zip'=>'Zip code',
                'show_person_country'=>'Country',
                'show_other_specify'=>'Please specify the other means of contact',
            'financial_assistance'=>'How would you like to give the financial assistance?'
        ]; 
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['title','summary_title', 'description','image','non_pay_option','expected_cost','expected_date','in_return','who_can','delivery_type','auto_id','status','show_mail_status','show_person_status','show_other_status','show_mail','show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','i_agree_decide2','i_agree_decide','image_name','financial_assistance','financial_assistance_other','way_of_donation','description_of_way'];
	    $scenarios['update'] = ['title','summary_title', 'description','non_pay_option','expected_cost','expected_date','who_can','delivery_type','auto_id','status','show_mail_status','show_person_status','show_other_status','show_mail','show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','i_agree_decide2','i_agree_decide','image_name','financial_assistance','financial_assistance_other','way_of_donation','description_of_way'];
		 
        return $scenarios;
    }

    		
	public function sendCreateSuccessEmail($donation, $id)
    {

		$mailcontent = MailContent::find()->where(['m_id'=>31])->one();
		$editmessage = $mailcontent->mail_message;		
		$subject = $mailcontent->mail_subject;
		if(empty($subject))
			$subject = 	'SimplyWishes ';
		
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
			
        if (!$user) {
            return false;
        }
        
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'donationcreateSuccess-html'],
                ['user' => $user, 'donation' => $donation, 'editmessage' => $editmessage ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            ->setSubject($subject);			
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
		$message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
		
		return $message->send();

    }

    public function sendUpdateSuccessEmail($donation, $id)
    {
		
        $mailcontent = MailContent::find()->where(['m_id'=>28])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
			
        if (!$user) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'donationupdateSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            ->setSubject($subject);			
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
		$message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
		
		return $message->send();
    }
	
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaved()
    {
        return $this->hasMany(Activity::className(), ['donation_id' => 'id']) ->andOnCondition(['activity' => 'fav']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaveddonation()
    {
        return $this->hasMany(Activity::className(), ['donation_id' => 'id']) ->andOnCondition(['activity' => 'fav']);
    }
    /**
	 * Forms th html for a particular donation
     * @return html
     */		
    public function getDonationAsCard(){
        $str = '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            $str .= '<div class="grid-item col-md-4"><div class=" smpl-wish-block1 thumbnail tplColor">';
            $str .= '<div ><a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a></div>';
            /////activities///
            $class = '';
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';
            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';


            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink" ></span>';
            else
                $str .= '<span title="You loved it" data-id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it" data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
            <div class="shareIcons hide" data_image="' . Url::base(true) . '/web/' . $this->image . '" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
            </div>';

            $str .= '<div class="smp-wish-desc">';
            if (\Yii::$app->user->isGuest)
                $str .= '<p><div class="list-icon wishCardBlk"  >
            <img src="' . $this->donorPic . '" alt="">
            <a class="wisherName" href="' . Url::to(['site/login']) . '"><span>' . $this->donorName . '</span></a>
        </div> </p>';
            else
                $str .= '<p><div class="list-icon wishCardBlk"  >
            <img src="' . $this->donorPic . '" alt="">
            <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
        </div> </p>';

            $str .= '<p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . ' </span>';
            if (!empty($this->granted_by))
                $str .= '<span><img class="pull-right" src="' . \Yii::$app->homeUrl . 'web/images/tick.png" width="30"></span></p>';
            $str .= '</div>';
            $str .= '</div></div>';
        }
        echo $str;
    }

        /**
     * @returns the name of the donor
     */
	public function getDonorName(){
        //toaddreport
        $profile = UserProfile::find()->where(['user_id'=>$this->created_by])->one();
        if (!$profile) {
            $user = User::findOne($this->created_by);
            $username = "  ";

            if (isset($user->username))
                $username = $user->username;

            return $username;
        }
        return "$profile->firstname $profile->lastname";
    }

    public function getDonorModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'created_by']);
    }

        /**
     * @returns the name of the donor
     */
	public function getDonorPic(){

		$profile = UserProfile::find()->where(['user_id'=>$this->created_by])->one();
		if($profile && $profile->profile_image!='')
			return Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now');

		else return Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png';
    }
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Activity::className(), ['donation_id' => 'id']) ->andOnCondition(['activity' => 'like']);
    }
    public function getLikeByUser()
    {
        return $this->hasMany(Activity::className(), ['donation_id' => 'id']) ->andOnCondition(['activity' => 'like']);
    }

        /**
     * @return no of likes
     */
    public function getLikesCount()
    {
        return Activity::find()->where(['donation_id'=>$this->id,'activity'=>'like'])->count();
    }

    /**
	 * Is this wish favourited by the particular user
     * @return boolean
     */
    public function isFaved($byUser){
        if(Activity::find()->where(['donation_id'=>$this->id,'activity'=>'fav','user_id'=>$byUser])->one()!= null)
                return true;
        else return false;
    }


    /**
	 * Is this wish liked by the particular user
     * @return boolean
     */
    public function isLiked($byUser){
        if(Activity::find()->where(['donation_id'=>$this->id,'activity'=>'like','user_id'=>$byUser])->one()!= null)
                return true;
        else return false;
    }


    public function sendAcceptDonationEmail($id,$model)
    {

        $mailcontent = MailContent::find()->where(['m_id'=>23])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $userDonation = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $model->created_by,
        ]);

        if (!$userDonation) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'acceptDonationContactMessage-html'],
                ['user' => $user, 'userDonation' => $userDonation, 'editmessage' => $editmessage,'model' =>$model ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
// echo " ----||| $message |||----- ";
// return $message;

        return $message->send();
    }

    public function sendFinancialAcceptDonationEmail($id,$model)
    {

        $mailcontent = MailContent::find()->where(['m_id'=>35])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $userDonation = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $model->created_by,
        ]);

        if (!$userDonation) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'acceptFinancialDonationContactMessage-html'],
                ['user' => $user, 'userDonation' => $userDonation, 'editmessage' => $editmessage,'model' =>$model ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
// echo " ----||| $message |||----- ";
// return $message;

        return $message->send();
    }

    	/**
     * @returns the name of the Granted donor
     */
	public function getGrantedDonorName(){

		$profile = UserProfile::find()->where(['user_id'=>$this->granted_by])->one();
		if(!$profile)
			return "";

		return "$profile->firstname $profile->lastname";
    }

    public function getHtmlForProfile(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc" >';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->donorPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
         </span></p>
        </div></p>';
            $str .= '</div>';
            if (Yii::$app->controller->action->id == 'my-account' || Yii::$app->controller->action->id == 'my-progress')
                echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div style="">				
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
            else
                echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div style="">				
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                    <p><a style="margin-left:10px" class="fnt-danger" href="' . Url::to(['donation/remove-donation', 'd_id' => $this->id]) . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Remove</a> </p>
                </div>
            </div>';
        }
  //height:140px;
    }
    public function getHtmlForGranted(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc" >';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->donorPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
         </span></p>
        </div></p>';
            $str .= '</div>';
            if (Yii::$app->controller->action->id == 'my-account' || Yii::$app->controller->action->id == 'my-progress')
                echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div style="">				
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
            else
                echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div style="">				
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
        }
        //height:140px;
    }
    public function getHtmlForProfileOther(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';
            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
            <div class="shareIcons hide" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
            </div>';
            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
            <img src="' . $this->donorPic . '" alt="">
            <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
            <p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
            <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
            &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
             </span></p>
            </div></p>';
            //$str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';

            echo '<div class="grid-item col-md-3 "> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                    <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '"  width="300" style="height:160px;" alt="Image"></a></div>
                ' . $str . '
                </div>
            </div>';
        }
    }
    public function getHtmlForProfilegranted(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc" >';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->donorPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
         </span></p>
        </div></p>';

            $str .= '</div>';
            echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '   
                       <p><a style="margin-left:10px" class="fnt-danger" href="' . Url::to(['donation/delete-donation', 'id' => $this->id]) . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Remove</a> </p>
                </div>
            </div>';
        }
    }

    public function getHtmlForProfileSaved(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->created_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this donation" data-w_id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love Donation" data-w_id="' . $this->id . '" data-a_type="like" class="like-wish fas fa-hand-holding-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it" data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->title . '" data_url="' . Url::to(['donation/view', 'id' => $this->id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
                    <img src="' . $this->donorPic . '" alt="">
                    <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->created_by]) . '"><span>' . $this->donorName . '</span></a>
                    <p class="desc" >' . mb_strimwidth($this->title, 0, 35, "..") . '</p>
                <p><a class="fnt-green" href="' . Url::to(['donation/view', 'id' => $this->id]) . '">Read More</a>
                &nbsp;<span data-id="' . $this->id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
                 </span></p>
                </div></p>';
            // $str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';

            echo '<div class="grid-item col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                        <div >
                        <a href="' . Url::to(['donation/view', 'id' => $this->id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->image . '?v=' . strtotime('now') . '" alt="Image" width="300" style="height:160px;" alt="Image"></a> </div>
                        ' . $str . '
                        <p><a style="margin-left:10px" class="fnt-danger" href="' . Url::to(['donation/remove-donation', 'd_id' => $this->id]) . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Remove</a> </p>
                </div>				
                </div>';
        }
        }
    public function getFollows()
    {
        return $this->hasMany(FollowRequest::className(), ['requested_to' => 'created_by']);
    }
    public function getFriends()
    {
        return $this->hasMany(FriendRequest::className(), ['requested_to' => 'created_by']);
    }
    public function getWisherName(){
        //toaddreport
        $profile = UserProfile::find()->where(['user_id'=>$this->created_by])->one();
        if (!$profile) {
            $user = User::findOne($this->created_by);
            $username = "  ";

            if (isset($user->username))
                $username = $user->username;

            return $username;
        }
        return "$profile->firstname $profile->lastname";
    }
    public function getWisherPic(){

        $profile = UserProfile::find()->where(['user_id'=>$this->created_by])->one();
        if($profile && $profile->profile_image!='')
            return Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now');

        else return Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png';
    }
}
