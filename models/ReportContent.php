<?php
//toaddreport

namespace app\models;

use Yii;
class ReportContent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_content';
    }

    public function getWish()
    {
        return $this->hasOne(Wish::className(), ['w_id' => 'content_id']); 
    } 
    
    public function getReportUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'report_user']);
    }	
    
    public function getReportedUser()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'reported_user']);
    }	

    public function getContent()
    {
        if ($this->report_type == 'wish') {
            return $this->hasOne(Wish::className(), ['w_id' => 'content_id']);
        }
    }
}
?>