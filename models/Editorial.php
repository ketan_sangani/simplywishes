<?php

namespace app\models;

use app\models\custom\CustomModel;

use Yii;

/**
 * This is the model class for table "editorial".
 *
 * @property integer $e_id
 * @property string $e_title
 * @property string $e_text
 * @property string $e_image
 * @property string $is_video_only
 * @property integer $status
 * @property string $created_at
 */
//toaddreport

class Editorial extends BaseModel // \yii\db\ActiveRecord
{
    public $featured_video_upload;
    public $articleimage;
	public $video;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'editorial';
    }
    



//    //toaddreport
//    public static function find()
//    {
//        $query =new CustomModel(get_called_class());
//         echo $query->createCommand()->getRawSql();
//        return  $query;
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['e_title','articleimage'], 'required'],
            [['class'], 'required'],
            //[['e_image'], 'required','except' => 'update_by_editorial_admin'],
            [['e_text','description'], 'string'],
            [['featured_video_url','article_image','description'], 'string'],
            [['status', 'is_video_only'], 'integer'],
            [['created_at'], 'safe'],
            [['e_title'], 'string', 'max' => 250],
            [['e_image','articleimage'], 'file','extensions' => 'jpg,png,jpeg'],
            [['featured_video_upload','video'], 'file','extensions' => 'mp4,mov,wmv', 'maxSize' => 10240000, 'skipOnEmpty' => true],
            /*['featured_video_url', 'required', 'when' => function($model) {
                    return $model->is_video_only == 1;
                }, 'whenClient' => "function (attribute, value) {
                    return $('#video_check').val() == 1;
                }"
            ]*/
        ];
    }

	
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['update_by_editorial_admin'] = ['e_title','e_text','e_image','is_video_only','featured_video_url','description']; //Scenario Values Only Accepted
        return $scenarios;
    } 
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'e_id' => 'E ID',
            'e_title' => 'Title',
            'e_text' => 'Text',
            'e_image' => 'Image',
            'articleimage'=>'Article Image',
            'status' => 'Status',
            'is_video_only' => 'Is Video Only',
            'created_at' => 'Created At',
            'featured_video_url' => 'Insert the video url in order to upload',
            'featured_video_upload' => 'Upload from your Files',
            'description'=>'Description'
        ];
    }
	
    public function uploadImage(){
        $newFileName = \Yii::$app->security
                ->generateRandomString().'.'.$this->e_image->extension;
        $this->e_image->saveAs('web/uploads/editorial/' . $newFileName,false);
        $this->e_image = 'uploads/editorial/'.$newFileName;
        return true;	
    }

    public function uploadarticleImage(){
        $newFileName = \Yii::$app->security
                ->generateRandomString().'.'.$this->articleimage->extension;
        $this->articleimage->saveAs('web/uploads/editorial/' . $newFileName,false);
        $this->article_image = 'uploads/editorial/'.$newFileName;
        return true;
    }
    public function isLiked($byUser){
            if(EditorialActivity::find()->where(['e_id'=>$this->e_id,'activity'=>'like','user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }
    
    public function getLikes()
    {
        return $this->hasMany(EditorialActivity::className(), ['e_id'=>'e_id'])->andOnCondition(['activity' => 'like']);
    }
    
    public function getLikesCount()
    {
        return EditorialActivity::find()->where(['e_id'=>$this->e_id,'activity'=>'like'])->count();
    }

    public function getAuthor()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'created_by']);
    }
    public function getlikeByUser()
    {
        return $this->hasMany(EditorialActivity::className(), ['e_id'=>'e_id'])->andOnCondition(['activity' => 'like']);
    }
    
}
