<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use app\models\ReportContent;
use app\models\custom\CustomWish;

/**
 * This is the model class for table "wishes".
 *
 * @property integer $w_id
 * @property integer $wished_by
 * @property integer $granted_by
 * @property integer $category
 * @property string $wish_title
 * @property string $summary_title
 * @property string $wish_description
 * @property string $primary_image
 * @property integer $state
 * @property integer $country
 * @property integer $city
 */
class Wish extends BaseModel// \yii\db\ActiveRecord
{
    public $auto_id; 
    public $primary_image_name;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wishes';
    }



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['category', 'wish_title','state', 'country', 'city','expected_cost','expected_date'], 'required'],
            [['wish_title','expected_date'], 'required'],
            [['wish_title','expected_date','summary_title','wish_description','expected_cost'], 'required', 'on'=>'create-api'],
            [['non_pay_option'], 'required','on'=>'create','message' => 'You must select one of these options.'],
            [['financial_assistance'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['show_mail'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['expected_cost'], 'required', 'when' => function ($model) { return $model->non_pay_option == '0'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '0';
    }"],
            [['way_of_wish'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }"],
            [['description_of_way'], 'required', 'when' => function ($model) { return $model->non_pay_option == '1'; },
                'whenClient' => "function (attribute, value) {
        return $('.nonpaydonation:checked').val() == '1';
    }",'message' => 'You must fill this details.'],
            //	['primary_image', 'required', 'message' => '{attribute} can\'t be blank', 'on'=>'create'],
            [['wished_by', 'granted_by'], 'integer'],
            [['wish_description', 'primary_image'], 'string'],       
            [['wish_title','way_of_wish'], 'string', 'max' => 100],
            [['summary_title','who_can'], 'string', 'max' => 150],
            [['expected_cost'], 'integer','min' => 1],
            [['non_pay_option','i_agree_decide2','i_agree_decide'], 'integer'],

            [['auto_id','wish_status','primary_image_name'], 'safe'],

    //	[['expected_cost'], 'in','range'=>range(100,1000),'message'=>'Expected Cost(USD) Range In 100 to 1000' ],
            [['show_mail_status','show_person_status','show_other_status'], 'integer'],
            [['show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','financial_assistance','financial_assistance_other','description_of_way'], 'string'],
            ['show_mail','email','message' => 'Enter valid email address'],
            ['expected_date', 'validateDate','on' => 'updatedraft'],

        ];
    }


    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['create'] = ['wish_title','summary_title', 'wish_description','primary_image','expected_cost','expected_date','in_return','who_can','non_pay_option','auto_id','wish_status','show_mail_status','show_person_status','show_other_status','show_mail','show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','i_agree_decide2','i_agree_decide','primary_image_name','financial_assistance','financial_assistance_other','way_of_wish','description_of_way'];
	    $scenarios['update'] = ['wish_title','summary_title', 'wish_description','expected_cost','expected_date','who_can','non_pay_option','auto_id','wish_status','show_mail_status','show_person_status','show_other_status','show_mail','show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','i_agree_decide2','i_agree_decide','primary_image_name','financial_assistance','financial_assistance_other','way_of_wish','description_of_way'];
        $scenarios['updatedraft'] = ['wish_title','summary_title', 'wish_description','expected_cost','expected_date','who_can','non_pay_option','auto_id','wish_status','show_mail_status','show_person_status','show_other_status','show_mail','show_person_street','show_person_city','show_person_state','show_person_zip','show_person_country','show_other_specify','i_agree_decide2','i_agree_decide','primary_image_name','financial_assistance','financial_assistance_other','way_of_wish','description_of_way'];

        return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'w_id' => 'W ID',
            'wished_by' => 'Wished By',
            'granted_by' => 'Granted By',
            'wish_title' => 'Wish Title',
            'summary_title' => 'Wish Summary',
            'wish_description' => 'Wish Description',
            'primary_image' => 'Primary Image',
                'expected_cost'=>'Expected Cost(USD)', 

               'expected_date'=>'Date I would like my wish to be granted',
               'non_pay_option'=>'Wishes Type ',			
               'who_can'=>'Who can potentialy help me',

                'show_mail_status'=>'E-mail',
                'show_person_status'=>'Postal',
                'show_other_status'=>'Other',
                'show_mail'=>'Enter your email address',
                'show_person_street'=>'Street',
                'show_person_city'=>'City',
                'show_person_state'=>'State',
                'show_person_zip'=>'Zip code',
                'show_person_country'=>'Country',
                'show_other_specify'=>'Please specify the other means of contact',
        ]; 
    }


    public function validateDate($attribute, $params)
    {
        /*if (!$this->hasErrors()) {
             if (date('Y-m-d',strtotime($this->expected_date)) < date("Y-m-d")) {
                 $this->addError($attribute, 'Date must be greator than or equal to today date.');

            }else{
             }
        }*/
    }
    public function uploadImage(){
        if($this->validate()) {
            $this->primary_image->saveAs('web/uploads/' .time().$this->primary_image->baseName. '.' .$this->primary_image->extension);
            $this->primary_image = 'uploads/'.time().$this->primary_image->baseName.'.'.$this->primary_image->extension;
            return true;
        }else
            return false;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWisher()
    {
        return $this->hasOne(User::className(), ['id' => 'wished_by']);
    }
    public function getWisherModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'wished_by']);
    }
    public function getGranterModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'granted_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryModel()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStateModel()
    {
        return $this->hasOne(State::className(), ['id' => 'state']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCityModel()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLikes()
    {
        return $this->hasMany(Activity::className(), ['wish_id' => 'w_id'])->andOnCondition(['activity' => 'like']);
    }
    public function getLikeByUser()
    {
        return $this->hasOne(Activity::className(), ['wish_id' => 'w_id'])->andOnCondition(['activity' => 'like']);
    }
    
    public function getFollows()
    {
        return $this->hasMany(FollowRequest::className(), ['requested_to' => 'wished_by']);
    }
    public function getFriends()
    {
        return $this->hasMany(FriendRequest::className(), ['requested_to' => 'wished_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaved()
    {
        return $this->hasMany(Activity::className(), ['wish_id' => 'w_id']) ->andOnCondition(['activity' => 'fav']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSavedw()
    {
        return $this->hasMany(Activity::className(), ['wish_id' => 'w_id']) ->andOnCondition(['activity' => 'fav']);
    }
    /**
     * @return no|bool|int|string
     */
    public function getLikesCount()
    {
        return Activity::find()->where(['wish_id'=>$this->w_id,'activity'=>'like'])->count();
    }	
	
    /**
	 * Is this wish liked by the particular user
     * @return boolean
     */	
    public function isLiked($byUser){
            if(Activity::find()->where(['wish_id'=>$this->w_id,'activity'=>'like','user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }
	
    /**
	 * Is this wish favourited by the particular user
     * @return boolean
     */		
    public function isFaved($byUser){
            if(Activity::find()->where(['wish_id'=>$this->w_id,'activity'=>'fav','user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }
	
    /**
	 * Forms th html for a particular wish
     * @return html
     */		
    public function getWishAsCard(){
        $str = '';
        $userexist = User::findOne($this->wished_by);
        if(!empty($userexist)) {
            $str .= '<div class="grid-item col-md-4"><div class=" smpl-wish-block1 thumbnail tplColor">';
            $str .= '<div><a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a></div>';
            /////activities///
            $class = '';
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';
            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this wish" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" ></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it" data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
            <div class="shareIcons hide" data_image="' . Url::base(true) . '/web/' . $this->primary_image . '" data_text="' . $this->wish_title . '" data_url="' . Url::to(['wish/view', 'id' => $this->w_id], true) . '" ></div>
            </div>';

            $str .= '<div class="smp-wish-desc">';
            if (\Yii::$app->user->isGuest)
                $str .= '<p>
                    <div class="list-icon wishCardBlk"><img src="' . $this->wisherPic . '" alt="">
                        <a class="wisherName" href="' . Url::to(['site/login']) . '">
                            <span>' . $this->wisherName . '</span>
                        </a>
                    </div>
                   </p>';
            else
                $str .= '<p>
                    <div class="list-icon wishCardBlk"><img src="' . $this->wisherPic . '" alt="">
                        <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->wished_by]) . '">
                            <span>' . $this->wisherName . '</span>
                        </a>
                    </div>
                   </p>';
            $str .= '<p class="desc" >' . mb_strimwidth($this->wish_title, 0, 35, "..") . '</p>


        <p><a class="fnt-green" href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->w_id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->w_id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . ' </span>';
            if (!empty($this->granted_by))
                $str .= '<span><img class="pull-right" src="' . \Yii::$app->homeUrl . 'web/images/tick.png" width="30"></span></p>';
            $str .= '</div>';
            $str .= '</div></div>';
        }
        echo $str;
    }

    public function getHtmlForProfile(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->wished_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this wish" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->wish_title . '" data_url="' . Url::to(['wish/view', 'id' => $this->w_id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->wisherPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->wished_by]) . '"><span>' . $this->wisherName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->wish_title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->w_id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->w_id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
         </span></p>
        </div></p>';
            // $str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';
            if (Yii::$app->controller->action->id == 'my-account' || Yii::$app->controller->action->id == 'my-progress')
                echo '<div class="col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                        <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
            else
                echo '<div class="col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                        <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                    <p><a style="margin-left:10px" class="fnt-danger" href="' . Url::to(['wish/delete-wish', 'id' => $this->w_id]) . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Remove</a> </p>
                </div>
            </div>';
        }

    }
    public function getHtmlForGranted(){
        $str = "";
        $class  =   '';
        $userexist = User::findOne($this->wished_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this wish" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
              <div class="shareIcons hide" data_text="' . $this->wish_title . '" data_url="' . Url::to(['wish/view', 'id' => $this->w_id], true) . '" ></div>
              </div>';

            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->wisherPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->wished_by]) . '"><span>' . $this->wisherName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->wish_title, 0, 35, "..") . '</p>
        <p><a class="fnt-green" href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '">Read More</a>
        &nbsp;<span data-id="' . $this->w_id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->w_id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
         </span></p>
        </div></p>';
            // $str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';
            if (Yii::$app->controller->action->id == 'my-account' || Yii::$app->controller->action->id == 'my-progress')
                echo '<div class="col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                        <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
            else
                echo '<div class="col-md-4"> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                        <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" width="300" style="height:160px;" alt="Image"></a>
                    </div>
                    ' . $str . '
                </div>
            </div>';
        }

    }

    public function getHtmlForProfileOther(){
	$str = "";
	$class  =   '';
        $userexist = User::findOne($this->wished_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this wish" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon glyphicon-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it"  data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
            <div class="shareIcons hide" data_text="' . $this->wish_title . '" data_url="' . Url::to(['wish/view', 'id' => $this->w_id], true) . '" ></div>
            </div>';
            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
            <img src="' . $this->wisherPic . '" alt="">
            <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->wished_by]) . '"><span>' . $this->wisherName . '</span></a>
            <p class="desc" >' . mb_strimwidth($this->wish_title, 0, 35, "..") . '</p>
            <p><a class="fnt-green" href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '">Read More</a>
            &nbsp;<span data-id="' . $this->w_id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->w_id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
             </span></p>
            </div></p>';
            //$str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';

            echo '<div class="grid-item col-md-3 "> 
                <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>				
                    <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '"  width="300" style="height:160px;" alt="Image"></a></div>
                ' . $str . '
                </div>
            </div>';
        }
    }

    public function getHtmlForProfileSaved(){
	$str = "";
	$class  =   '';
        $userexist = User::findOne($this->wished_by);
        if(!empty($userexist)) {
            if ($this->likesCount > 0)
                $class = 'likesView';
            else
                $class = 'hide';

            if (!$this->isFaved(\Yii::$app->user->id))
                $str .= '<div class="smp-links sharefull-list"><span title="Save this wish" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-orange"></span></br>';
            else
                $str .= '<div class="smp-links sharefull-list"><span title="You saved it" data-w_id="' . $this->w_id . '" data-a_type="fav" class="fav-wish fa fa-save txt-smp-blue"></span></br>';

            if (!$this->isLiked(\Yii::$app->user->id))
                $str .= '<span title="Love it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink"></span>';
            else
                $str .= '<span title="You loved it" data-w_id="' . $this->w_id . '" data-a_type="like" class="like-wish glyphicon glyphicon-heart txt-smp-pink" style="color:#B23535;"></span>';

            $str .= '<br><span title="Share it" data-placement="right"  data-popover-content=""><img data-placement="right" class="listesinside"  src="' . \Yii::$app->homeUrl . 'web/' . 'images/Share-Icon.png"  /></span>
          <div class="shareIcons hide" data_text="' . $this->wish_title . '" data_url="' . Url::to(['wish/view', 'id' => $this->w_id], true) . '" ></div>
          </div>';

            $str .= '<div class="smp-wish-desc">';
            $str .= '<p><div class="list-icon wishCardBlk">
                <img src="' . $this->wisherPic . '" alt="">
                <a class="wisherName" href="' . Url::to(['account/profile', 'id' => $this->wished_by]) . '"><span>' . $this->wisherName . '</span></a>
                <p class="desc" >' . mb_strimwidth($this->wish_title, 0, 35, "..") . '</p>
            <p><a class="fnt-green" href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '">Read More</a>
            &nbsp;<span data-id="' . $this->w_id . '" class="likesBlk ' . $class . '"> <span id="likecmt_' . $this->w_id . '"  >' . $this->likesCount . '</span> ' . ($this->likesCount > 1 ? "Loves" : "Love") . '
             </span></p>
            </div></p>';
            // $str .=  '<div class="shareIcons" data_text="'.$this->wish_title.'" data_url="'.Url::to(['wish/view','id'=>$this->w_id],true).'" ></div>';
            $str .= '</div>';

            echo '<div class="grid-item col-md-4"> 
            <div class="smpl-wish-block1 thumbnail tplColor">	
                    <div>
                    <a href="' . Url::to(['wish/view', 'id' => $this->w_id]) . '"><img src="' . \Yii::$app->homeUrl . 'web/' . $this->primary_image . '?v=' . strtotime('now') . '" alt="Image" width="300" style="height:160px;" alt="Image"></a> </div>
                    ' . $str . '
                    <p><a style="margin-left:10px" class="fnt-danger" href="' . Url::to(['wish/remove-wish', 'wish_id' => $this->w_id]) . '"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i> Remove</a> </p>
            </div>				
            </div>';
        }
    }

    /**
     * @returns the location of the wish
     */	
    public function getLocation(){
        $country = Country::findOne($this->country);
        $state = State::findOne($this->state);
        $city = City::findOne($this->city);
        if(!$country && !$state && !$city)
                return "Unknown";

        $location  = "";

        if(!empty($city))
                $location  .= $city->name;
        if(!empty($state) && !empty($city))
                $location  .= ", ".$state->name;
        else if(!empty($state))
                $location  .= $state->name;

        if((!empty($state) || !empty($city)) && !empty($country))
                $location  .= ", ".$country->name;
        else if(!empty($country))
                $location  .= $country->name; 

        return "$location";

        //else return "$state->name , $country->name";
    }
    /**
     * @returns the location of the wish
     */	
    public function getCategoryName(){

        $Category = Category::findOne($this->category);
        if(!$Category)
                return "Unknown";
        else return "$Category->title";
    }
    /**
     * @returns the name of the wisher
     */	
	public function getWisherName(){
            //toaddreport
            $profile = UserProfile::find()->where(['user_id'=>$this->wished_by])->one();
            if (!$profile) {
                $user = User::findOne($this->wished_by);
                $username = "  ";
                
                if (isset($user->username))
                    $username = $user->username;
                
                return $username;
            }
            return "$profile->firstname $profile->lastname";
	}	
    /**
     * @returns the name of the wisher
     */	
	public function getWisherPic(){
		
		$profile = UserProfile::find()->where(['user_id'=>$this->wished_by])->one();
		if($profile && $profile->profile_image!='')
			return Yii::$app->homeUrl.'web/uploads/users/'.$profile->profile_image.'?v='.strtotime('now');
		
		else return Yii::$app->homeUrl.'web/uploads/users/images/default_profile.png';
	}	

	/**
     * @returns the name of the Granted wisher
     */	
	public function getGrantedWisherName(){
		
		$profile = UserProfile::find()->where(['user_id'=>$this->granted_by])->one();
		if(!$profile)
			return "";
		
		return "$profile->firstname $profile->lastname";
	}		
	
	
			
	public function sendCreateSuccessEmail($wish, $id)
    {
		
		$mailcontent = MailContent::find()->where(['m_id'=>9])->one();
		$editmessage = $mailcontent->mail_message;		
		$subject = $mailcontent->mail_subject;
		if(empty($subject))
			$subject = 	'SimplyWishes ';
		
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
			
        if (!$user) {
            return false;
        }
        
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishcreateSuccess-html'],
                ['user' => $user, 'wish' => $wish, 'editmessage' => $editmessage ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            ->setSubject($subject);			
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
		$message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
		
		return $message->send();
    }
	
    public function sendUpdateSuccessEmail($wish, $id)
    {
		
        $mailcontent = MailContent::find()->where(['m_id'=>10])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
			
        if (!$user) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishupdateSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            ->setSubject($subject);			
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
		$message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
		
		return $message->send();
    }
	
	
    public function sendGrantWishNonFinancialEmail($id,$model)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>12])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $model->wished_by,
        ]);
			
        if (!$user) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'grantwishcontactMessage-html'],
                ['user' => $user,'user2' => $user2, 'editmessage' => $editmessage,'model' =>$model ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo( $user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send(); 
    }

    public function sendGrantWishFinancialEmail($id,$model){
        $mailcontent = MailContent::find()->where(['m_id'=>32])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $model->wished_by,
        ]);

        if (!$user) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'grantfinancialwishcontactMessage-html'],
                ['user' => $user,'user2' => $user2, 'editmessage' => $editmessage,'model' =>$model ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
	
	
}
