<?php 
namespace app\models\custom;

use yii\db\ActiveQuery;
use app\models\Wish;
use app\models\ReportContent;


//toaddreport
class CustomWish extends ActiveQuery
{
    // conditions appended by default (can be skipped)
    public function init()
    {
        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => \Yii::$app->user->id
            ]));
        return $this->andOnCondition(['not in','wishes.wished_by', $selectReported]);
        parent::init();
    }

    // ... add customized query methods here ...

    public function active($state = true)
    {
        return $this->andOnCondition(['active' => $state]);
    }

    public function notReported($state = true)
    {
        //and wishes.`wished_by` not in (SELECT `reported_user` FROM `report_content` WHERE `report_user`=373)
        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => \Yii::$app->user->id
            ]));
        return $this->andOnCondition(['not in','wishes.wished_by', $selectReported]);
    }
}