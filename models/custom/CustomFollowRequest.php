<?php 
namespace app\models\custom;

use yii\db\ActiveQuery;
use app\models\Wish;
use app\models\ReportContent;

class CustomFollowRequest extends ActiveQuery
{
    // conditions appended by default (can be skipped)
    public function init()
    {
        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => \Yii::$app->user->id
            ]
        ));
        return $this->andOnCondition(['not in','follow_request.requested_to', $selectReported]);
        parent::init();
    }
}