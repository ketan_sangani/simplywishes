<?php 
namespace app\models\custom;

use yii\db\ActiveQuery;
use app\models\Wish;
use app\models\ReportContent;
use app\models\Properties;



class CustomModel extends ActiveQuery
{

    public $model = "model";
    public $field = "field";

    public function initModel($modelName = "", $fieldToCheck){
        $model = $modelName;
        $field = $fieldToCheck;
    }

    // conditions appended by default (can be skipped)
    public function init()
    {
        if(\Yii::$app->user->id==null){
            $user_id = \Yii::$app->params['apiuser_id'];
        }else{
            $user_id = \Yii::$app->user->id;
        }

        $model = new $this->modelClass;
        $modelName = $model->tableName();
    
    
        $selectReported = (ReportContent::find()->select('reported_user')->where(['report_type' => 'user', 'report_user' => $user_id]));
                // $selectReported = (ReportContent::find()->select('reported_user')->where(['blocked' => 1]));//->orWhere(['and', ['report_type'=>'user']]));


        switch ($modelName){
            case 'editorial':
                $this->field = 'created_by';
                break;
            case 'follow_request':
                $this->field = 'requested_to';
                break;
            case 'wishes':
                $this->field = 'wished_by';
                break;
            case 'happy_stories':
                $this->field = 'user_id';
                break;
            case 'editorial_comments':
                $this->field = 'user_id';
                break;
            case 'happy_stories_comments':
                $this->field = 'user_id';
                break;                
            case 'story_activities':
                $this->field = 'user_id';
                break;
            case 'activities':
                $this->field = 'user_id';
                break;       
            case 'friend_request':
                $this->field = 'requested_by';
                break;    
            case 'user_profile':
                $this->field = 'user_id';
                break;     
            case 'donations':
                $this->field = 'created_by';
                break;                                                    
            default:
                echo "default";
                break;
        }

        if (\Yii::$app->user->id != 1) {
            $returnReported = $this->andOnCondition(['not in',$model->tableName().'.'.$this->field, $selectReported]);

            return $returnReported;
        }
        parent::init();
    }

    public function getProperties(){
        $properties = Properties::find()->all();
        return $properties;
    }
}
