<?php 
namespace app\models\custom;

use yii\db\ActiveQuery;
use app\models\Wish;
use app\models\ReportContent;

class CustomHappyStories extends ActiveQuery
{
    // conditions appended by default (can be skipped)
    public function init()
    {
        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => \Yii::$app->user->id
            ]
        ));
        return $this->andOnCondition(['not in','happy_stories.user_id', $selectReported]);
        parent::init();
    }
}