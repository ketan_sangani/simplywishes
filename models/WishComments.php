<?php

namespace app\models;



use Yii;
use app\models\ReportContent;

/**
 * This is the model class for table "wish_comments".
 *
 * @property integer $w_com_id
 * @property integer $w_id
 * @property integer $user_id
 * @property string $comments
 * @property string $created_at
 * @property integer $status
 */
class WishComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wish_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['w_id', 'user_id', 'comments'], 'required'],
            [['w_id', 'user_id', 'status','parent_id'], 'integer'],
            [['comments'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'w_comment_id' => 'w Com ID',
            'parent_id' => 'Parent ID',
            'w_id' => 'w ID',
            'user_id' => 'User ID',
            'comments' => 'Comments',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
    
    //toaddreport
    // public  static function find() {
    //     $wishId = 0;
    //     if (isset(\Yii::$app->controller->actionParams['id'])) {
    //         $wishId = \Yii::$app->controller->actionParams['id'];
    //     }

    //     $selectReported = (ReportContent::find()->select('reported_user')->where(
    //         [
    //             'report_user' => \Yii::$app->user->id
    //         ]));

    //     $query1=  parent::find ()
    //         ->onCondition ( [ 'and' ,
    //             [ 'not in' , 'wish_comments.user_id', $selectReported ],
    //             [ '=' , 'wish_comments.w_id' , $wishId ]
    //         ]);
         
    //     return $query1;
    //  }

    public function getLikes()
    {
        return $this->hasMany(WishCommentsActivities::className(), ['w_comment_id' => 'w_comment_id']);
    }
    
    public function getLikesCount()
    {
        return WishCommentsActivities::find()->where(['w_comment_id'=>$this->w_comment_id])->count();
    }

    public function getCommenterModel()
    {
        return $this->hasOne(UserProfile::className(), ['user_id' => 'user_id']);
    }
    public function getCommentReply()
    {
        return $this->hasMany(WishComments::className(), ['parent_id' => 'w_comment_id']);
    }
    
    public function isLiked($byUser){
            if(WishCommentsActivities::find()->where(['w_comment_id'=>$this->w_comment_id,'user_id'=>$byUser])->one()!= null)
                    return true;
            else return false;
    }


}
