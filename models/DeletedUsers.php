<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "messages".
 *
 * @property integer $id
 * @property string $email
 * @property date $deleted_at
 */
class DeletedUsers extends \yii\db\ActiveRecord
{
    public $cnt;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deleted_users';
    }
        
    public static function findIfDeleted($email)
    {
        $val = static::find()->where("email = '$email'")->one();		
        return $val; 
    }
}