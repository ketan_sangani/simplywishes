<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Wish;
use app\models\UserProfile;

/**
 * SearchWish represents the model behind the search form about `app\models\Wish`.
 */
class SearchWish extends Wish
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['w_id', 'wished_by', 'granted_by'], 'integer'],
            [['wish_title', 'summary_title', 'wish_description', 'primary_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
	
	/* Custom single field search for wishes
	 * Currently searching only wish title, country,state and city
	 * @param array $params
	 * @return ActiveDataProvider
	 */
    public function searchCustom($params)
    {
        $keywords = explode(",",$params['match']);
        $query = Wish::find()->orderBy('w_id DESC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
        
        $query->innerJoinWith('wisherModel as wisher');
        $query->leftJoin('countries', '`countries`.`id` = `wisher`.`country`');
        $query->leftJoin('states', '`states`.`id` = `wisher`.`state`');
        $query->leftJoin('cities', '`cities`.`id` = `wisher`.`city`');

        foreach($keywords as $key=>$search){
            $search = trim($search);
            //if there are multiple searches, we are `and`ing the `or` queries

            if($key>0)
            {
                $query->andFilterWhere(['or',
                    ['like', 'wish_title',$search],
                    ['like', 'wish_description',$search],
                    ['like', 'countries.name', $search],
                    ['like', 'states.name', $search],
                    ['like', 'cities.name', $search],
                    ['like', 'wisher.firstname', $search],
                    ['like', 'wisher.lastname', $search]
                ]);
            }
            else
            {
                $query->where(['like', 'wish_title',$search])
                    ->orFilterWhere(['like', 'wish_description', $search])
                    ->orFilterWhere(['like', 'countries.name', $search])
                    ->orFilterWhere(['like', 'states.name', $search])
                    ->orFilterWhere(['like', 'cities.name', $search])
                    ->orFilterWhere(['like', 'wisher.firstname', $search])
                    ->orFilterWhere(['like', 'wisher.lastname', $search]);
            }

        }

        return $dataProvider;
    }
    
    public function searchCustomUser($params)
    {   
        $keywords = explode(",",$params['match']);
        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);

        $query->innerJoinWith('wisherModel as wisher');
        $query->leftJoin('countries', '`countries`.`id` = `wisher`.`country`');
        $query->leftJoin('states', '`states`.`id` = `wisher`.`state`');
        $query->leftJoin('cities', '`cities`.`id` = `wisher`.`city`');

        foreach($keywords as $key=>$search){
            $search = trim($search);
            //if there are multiple searches, we are `and`ing the `or` queries

            if($key>0)
            {
                $query->andFilterWhere(['or',
                    ['like', 'wish_title',$search],
                    ['like', 'wish_description',$search],
                    ['like', 'countries.name', $search],
                    ['like', 'states.name', $search],
                    ['like', 'cities.name', $search],
                    ['like', 'wisher.firstname', $search],
                    ['like', 'wisher.lastname', $search]
                ]);
            }
            else
            {
                $query->where(['like', 'wish_title',$search])
                    ->orFilterWhere(['like', 'wish_description', $search])
                    ->orFilterWhere(['like', 'countries.name', $search])
                    ->orFilterWhere(['like', 'states.name', $search])
                    ->orFilterWhere(['like', 'cities.name', $search])
                    ->orFilterWhere(['like', 'wisher.firstname', $search])
                    ->orFilterWhere(['like', 'wisher.lastname', $search]);
            }

        }

        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$cat_id=null)
    {
        $query = Wish::find()->where(['granted_by' => null,'wish_status'=>0,'process_status'=>0])->orderBy('date_updated DESC');
        if($cat_id === 'financial')
            $query->andWhere(['non_pay_option'=>0]);
        else if($cat_id === 'non-financial')
            $query->andWhere(['non_pay_option'=>1]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;
    }
	

    public function searchPopular($params){
        $query = Wish::find()->select(['wishes.*,count(a_id) as likes' ])->where(['wish_status'=>0])->orderBy('likes DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
		$query->joinWith(['likes as activity']);
		$query->groupBy('w_id');
		//echo $query->createCommand()->getRawSql();die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;		
    }
    
    public function searchProgress($params){
        $query = Wish::find()->where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])->orderBy('w_id DESC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
        $query->joinWith(['likes as activity']);
        $query->groupBy('w_id');
        //echo $query->createCommand()->getRawSql();die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;		
    }
    
    public function searchGranted($params){
        $query = Wish::find()->where(['not', ['granted_by' => null]])->orderBy('w_id DESC');
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);
			
        return $dataProvider;			
	}
        
    public function searchUserWishes($params, $user_id, $type){
		
        if($type === 'fullfilledGrant')
            $query = Wish::find()->where(['granted_by'=>$user_id,'wish_status'=>0,'process_status'=>0])->orderBy('w_id DESC');
        else 
           $query = Wish::find()->where(['wished_by'=>$user_id,'wish_status'=>0,'process_status'=>0])->orderBy('w_id DESC');
         
        // add conditions that should always apply here
        if($type == 'fullfilled' || $type == 'fullfilledGrant')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);
        
        return $dataProvider;		
    }

	public function searchSavedWishes($params, $user_id){
		
		$query = Wish::find()->where(['activity.user_id'=>$user_id])->orderBy('w_id DESC');
		$query->innerJoinWith(['saved as activity']);
		$query->groupBy('w_id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
		return $dataProvider;	
	}
	
	
	 /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchReport($params)
    {
        $query = Wish::find()->all();
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id     
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title]);

        return $dataProvider;
    }
	

	public function searchDrafts($params)
    {
        $query = Wish::find()->where(['wish_status' => 1,'wished_by' =>Yii::$app->user->id ])->orderBy('w_id DESC');
	
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;
    }
	
    public function searchProcessWishes($params, $user_id, $type){
		
        if($type === 'inprogressGrant')
            $query = Wish::find()->where(['process_granted_by'=>$user_id,'wish_status'=>0,'process_status'=>1 ])->orderBy('w_id DESC');
        else
            $query = Wish::find()->where(['wished_by'=>$user_id,'wish_status'=>0,'process_status'=>1 ])->orderBy('w_id DESC');
        // add conditions that should always apply here
        if($type == 'fullfilled')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;		
    }
    
    public function searchFriendProcessWishes($params, $user_id, $type){
        //SELECT w_id FROM wishes AS w INNER JOIN follow_request AS f ON f.requested_to = w.wished_by WHERE f.requested_by = 1
        
        $query = Wish::find()->orderBy('w_id DESC');
        $query->joinwith('follows as f');
        $query->andwhere(['f.requested_by' => \Yii::$app->user->id, 'wish_status'=>0]);
        $query->orWhere(['wished_by' => 1, 'wish_status'=>0]);
        
        if($type == 'inprogress')
            $query->andWhere(['process_status'=>1]);
        else
            $query->andWhere(['process_status'=>0]);
        
        if($type == 'fullfilled')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'w_id' => $this->w_id,
            'wished_by' => $this->wished_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'wish_title', $this->wish_title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'wish_description', $this->wish_description])
            ->andFilterWhere(['like', 'primary_image', $this->primary_image]);

        return $dataProvider;		
    }
}
