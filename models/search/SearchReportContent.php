<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReportContent;
// use app\models\Wish;

/**
 * SearchEditorial represents the model behind the search form about `app\models\ReportContent`.
 */
class SearchReportContent extends ReportContent
{
	
public $wishtitle;
public $fullname;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'content_id'], 'integer'],
            [['report_type', 'comment','blocked','report_user','reported_user'], 'string'],
			[['wishtitle','fullname'], 'safe'],			
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // print_r($params);
        $query = ReportContent::find()->orderBy('id Desc');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
         
        $query->joinWith(['wish as wishes']);//->where(['=',['report_content.report_type' => 'wish']]);
        $query->joinWith(['reportUser as user_profile']);
        // $query->joinWith(['reportedUser as user_profile']);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'report_user' => $this->report_user,
            // 'reported_user' => $this->reported_user,     
            'report_type' => $this->report_type,   
            'comment' => $this->comment,     
            'report_type' => $this->report_type,       
            'blocked' => $this->blocked,      
        ])
        ->andFilterWhere(['or',
        ['like','user_profile.firstname',$this->fullname],
        ['like','user_profile.lastname',$this->fullname]]);
        ;
        // echo $query->createCommand()->getRawSql();

        return $dataProvider;
    }
    /**
     * @return \yii\db\ActiveQuery
     */

}
