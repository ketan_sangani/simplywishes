<?php

namespace app\models\search;

use app\models\FriendRequest;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Donation;
use app\models\UserProfile;
use app\controllers\Utils;

/**
 * SearchDonation represents the model behind the search form about `app\models\Donation`.
 */
class SearchDonation extends Donation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'granted_by'], 'integer'],
            [['title', 'summary_title', 'description', 'image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
	
	/* Custom single field search for donations
	 * Currently searching only donation title, country,state and city
	 * @param array $params
	 * @return ActiveDataProvider
	 */
    public function searchCustom($params)
    {
        $keywords = explode(",",$params['match']);
        $query = Donation::find()->orderBy('id DESC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
        
        $query->innerJoinWith('donorModel as user_profile');
        $query->leftJoin('countries', '`countries`.`id` = `user_profile`.`country`');
        $query->leftJoin('states', '`states`.`id` = `user_profile`.`state`');
        $query->leftJoin('cities', '`cities`.`id` = `user_profile`.`city`');

        foreach($keywords as $key=>$search){
            $search = trim($search);
            //if there are multiple searches, we are `and`ing the `or` queries

            if($key>0)
            {
                $query->andFilterWhere(['or',
                    ['like', 'title',$search],
                    ['like', 'description',$search],
                    ['like', 'countries.name', $search],
                    ['like', 'states.name', $search],
                    ['like', 'cities.name', $search],
                    ['like', 'user_profile.firstname', $search],
                    ['like', 'user_profile.lastname', $search]
                ]);
            }
            else
            {
                $query->where(['like', 'title',$search])
                    ->orFilterWhere(['like', 'description', $search])
                    ->orFilterWhere(['like', 'countries.name', $search])
                    ->orFilterWhere(['like', 'states.name', $search])
                    ->orFilterWhere(['like', 'cities.name', $search])
                    ->orFilterWhere(['like', 'user_profile.firstname', $search])
                    ->orFilterWhere(['like', 'user_profile.lastname', $search]);
            }

        }

        return $dataProvider;
    }
    
    public function searchCustomUser($params)
    {   
        $keywords = explode(",",$params['match']);
        $query = Donation::find()->select(['donations.created_by,count(id) as total_donations'])->orderBy('total_donations DESC');
        $query->groupBy('created_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);

        $query->innerJoinWith('donationModel as donor');
        $query->leftJoin('countries', '`countries`.`id` = `donor`.`country`');
        $query->leftJoin('states', '`states`.`id` = `donor`.`state`');
        $query->leftJoin('cities', '`cities`.`id` = `donor`.`city`');

        foreach($keywords as $key=>$search){
            $search = trim($search);
            //if there are multiple searches, we are `and`ing the `or` queries

            if($key>0)
            {
                $query->andFilterWhere(['or',
                    ['like', 'title',$search],
                    ['like', 'description',$search],
                    ['like', 'countries.name', $search],
                    ['like', 'states.name', $search],
                    ['like', 'cities.name', $search],
                    ['like', 'donor.firstname', $search],
                    ['like', 'donor.lastname', $search]
                ]);
            }
            else
            {
                $query->where(['like', 'title',$search])
                    ->orFilterWhere(['like', 'description', $search])
                    ->orFilterWhere(['like', 'countries.name', $search])
                    ->orFilterWhere(['like', 'states.name', $search])
                    ->orFilterWhere(['like', 'cities.name', $search])
                    ->orFilterWhere(['like', 'donor.firstname', $search])
                    ->orFilterWhere(['like', 'donor.lastname', $search]);
            }

        }

        return $dataProvider;
    }
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$cat_id=null)
    {
        //toaddreport
        $query = Donation::find()->andOnCondition(['granted_by' => null,'status'=>0,'process_status'=>0])->orderBy('created_at DESC');
        // add conditions that should always apply here
        if($cat_id === 'financial')
            $query->andWhere(['non_pay_option'=>0]);
        else if($cat_id === 'non-financial')
            $query->andWhere(['non_pay_option'=>1]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
	

    public function searchPopular($params){
        $query = Donation::find()->select(['donations.*,count(a_id) as likes' ])->where(['status'=>0])->orderBy('likes DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
		$query->joinWith(['likes as activities']);
		$query->groupBy('id');
		//echo $query->createCommand()->getRawSql();die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;		
    }
    
    public function searchProgress($params){
        $query = Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->orderBy('process_granted_date DESC');
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
        $query->joinWith(['likes as activities']);
        $query->groupBy('id');
        //echo $query->createCommand()->getRawSql();die;
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;		
    }
    
    public function searchGranted($params){
        $query = Donation::find()->select(['*'])->where(['not', ['granted_by' => null]])->orderBy('granted_date DESC');
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);
			
        return $dataProvider;			
	}
        
    public function searchUserDonations($params, $user_id, $type){
		
        if($type === 'fullfilledGrant')
            $query = Donation::find()->where(['granted_by'=>$user_id,'status'=>0,'process_status'=>0])->orderBy('id DESC');
        else 
           $query = Donation::find()->where(['created_by'=>$user_id,'status'=>0,'process_status'=>0])->orderBy('id DESC');
        // echo   $query->createCommand()->getRawSql();exit;
        // add conditions that should always apply here
        if($type == 'fullfilled' || $type == 'fullfilledGrant')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        if($type == 'fullfilled' || $type == 'fullfilledGrant'){
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination'=>false
            ]);
        }else {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 9
                ]
            ]);
        }
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title',
            $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

            // print_r($query);die();
        
        return $dataProvider;		
    }

	public function searchSavedDonations($params, $user_id){
		
		$query = Donation::find()->where(['activities.user_id'=>$user_id])->orderBy('id DESC');
		$query->innerJoinWith(['saved as activities']);
		$query->groupBy('id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);
		return $dataProvider;	
	}
	
	
	 /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchReport($params)
    {
        $query = Donation::find()->all();
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id     
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
	

	public function searchDrafts($params)
    {
        $query = Donation::find()->where(['status' => 1,'created_by' =>Yii::$app->user->id ])->orderBy('id DESC');
	
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            // ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        
        return $dataProvider;
    }
	
    public function searchProcessDonations($params, $user_id, $type){
		
        if($type === 'inprogressGrant')
            $query = Donation::find()->where(['process_granted_by'=>$user_id,'status'=>0,'process_status'=>1 ])->orderBy('id DESC');
        else
            $query = Donation::find()->where(['created_by'=>$user_id,'status'=>0,'process_status'=>1 ])->orderBy('id DESC');
        // add conditions that should always apply here
        if($type == 'fullfilled')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;		
    }
    
    public function searchFriendProcessDonations($params, $user_id, $type){
        //SELECT id FROM donations AS w INNER JOIN follow_request AS f ON f.requested_to = w.created_by WHERE f.requested_by = 1
        $userid=\Yii::$app->user->id;
        $myfriends = FriendRequest::find()->select(['requested_by','requested_to'])->where("(friend_request.requested_by = ".$userid."  OR friend_request.requested_to = ".$userid.") and (friend_request.status = 1)")->asArray()->all();

        $friends = array();
        if(!empty($myfriends)){
            foreach ($myfriends as $myfriend){
                if ($myfriend['requested_by']==$userid){
                    $friends[] = $myfriend['requested_to'];
                }else{
                    $friends[] = $myfriend['requested_by'];
                }
            }
        }
        $query = Donation::find()->orderBy('id DESC');
        //$query->joinwith('friends as friend_request');
        //$query->andwhere(['friend_request.requested_by' => \Yii::$app->user->id, 'donations.status'=>0]);
        if(!empty($friends)){
            $query->where(['in','created_by',$friends])->andWhere(['donations.status'=>0]);
        }
        $query->orWhere(['created_by' => 1, 'donations.status'=>0]);
        
        if($type == 'inprogress')
            $query->andWhere(['process_status'=>1]);
        else
            $query->andWhere(['process_status'=>0]);
        
        if($type == 'fullfilled')
            $query->andWhere(['not', ['granted_by' => null]]);
        else
            $query->andWhere(['granted_by' => null]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>9
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_by' => $this->created_by,
            'granted_by' => $this->granted_by
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'summary_title', $this->summary_title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;		
    }
}
