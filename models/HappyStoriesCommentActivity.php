<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "happy_stories_comment_activities".
 *
 * @property integer $a_id
 * @property integer $hs_id
 * @property integer $user_id
 * @property string $activity
 * @property string $created_at
 */
class HappyStoriesCommentActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'happy_stories_comment_activities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hs_comment_id', 'user_id'], 'required'],
            [['hs_comment_id', 'user_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'a_id' => 'A ID',
            'hs_comment_id' => 'HS Comment ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }
}
