<?php

namespace app\controllers;

use app\models\Donation;
use http\Url;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\DeletedUsers;
use app\models\User;
use app\models\UserProfile;
use yii\data\ActiveDataProvider;
use app\models\Wish;
use app\models\HappyStories;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Properties;
use app\controllers\Utils;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        $sendmail =	Yii::$app->mailer->compose()
//            ->setTo('ketansangani12@gmail.com')
//            ->setFrom(['admin@simplywishes.com' => 'Dency G B'])
//            ->setSubject('Test mail from simplywishes')
//            ->setTextBody('Regards')
//            ->send();
//        var_dump($sendmail);exit;
        $this->layout = "home";
        $curr_query = Wish::find()->where(['granted_by' => null,'wish_status'=>0,'process_status'=>0])->orderBy('created_at DESC');
        $curr_dataProvider = new ActiveDataProvider([
            'query' => $curr_query,
            'pagination' => [
                'pageSize'=>3
            ]
        ]);

        $donations_query = Donation::find()->where(['granted_by' => null,'status'=>0,'process_status'=>0])->orderBy('created_at DESC');
        $donations_dataProvider = new ActiveDataProvider([
            'query' => $donations_query,
            'pagination' => [
                'pageSize'=>3
            ]
        ]);

        $query_wish_mix = Wish::find()->where(['not', ['granted_by' => null]])->orderBy('w_id DESC');
        $dataProvider_wish_mix = new ActiveDataProvider([
            'query' => $query_wish_mix,
            'pagination' => [
                'pageSize'=>2
            ]
        ]);

        $donations_query_donations_mix = Donation::find()->where(['not', ['granted_by' => null]])->orderBy('id DESC');
        $dataProvider_donations_mix = new ActiveDataProvider([
            'query' => $donations_query_donations_mix,
            'pagination' => [
                'pageSize'=>1
            ]
        ]);


        $happyquery = HappyStories::find()->where(['status'=>0])->orderBy('hs_id Desc')->limit(3)->all();
        $happy_dataProvider = new ActiveDataProvider([
            'query' => $happyquery,
             'pagination' => [
                'pageSize'=>3
            ] 
        ]);

        $forum_query = \app\models\Editorial::find()->where(['is_video_only' => 0,'status'=>0])->orderBy('updated_at DESC');
        $forum_dataProvider = new ActiveDataProvider([
            'query' => $forum_query,
            'pagination' => [
                'pageSize'=>3
            ]
        ]);

        $query = Wish::find()->where(['not', ['granted_by' => null]])->orderBy('w_id DESC');	
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                'pageSize'=>3
            ] 
        ]);
        





// print_r($dataProvider->models);
// print_r($happy_dataProvider->models);
// exit;

        return $this->render('index',
            [
                'current'=>$curr_dataProvider->models,
                'donation'=>$donations_dataProvider->models,
                'wish_mix'=>$dataProvider_wish_mix->models,
                'donations_mix'=>$dataProvider_donations_mix->models,
                    'happy'=>$happyquery,
                'forums' => $forum_dataProvider->models,
                'models'=>$dataProvider->models,
                // 'happy' => 'test',
            ]
        );
    }

	/**
     * Lists all Wish models.
     * @return mixed
     */
    public function actionIndexHome()
    {

	  $user = User::findOne(\Yii::$app->user->id);
	  $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
      $properties = Utils::getUserProperties($user->id);
		
	  return $this->render('index_home',['user' => $user, 'properties' => $properties,
			'profile' => $profile ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin($red_url=null)
    {	
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login())
        {
            if($red_url)
		return $this->redirect($red_url);
            
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
		
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

			$model->contact();
			$model->admincontact();
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
	 
    public function actionAbout()
    {
        $model = \app\models\Page::find()->where(['p_id'=>4])->one();

        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
	         'pagination' => [
                'pageSize'=>10
            ] 
        ]);
        return $this->render('about',[
			'dataProvider' => $dataProvider,
			'model' => $model,
		]);
    }

    public function actionFbSignUp($res)
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $result =   json_decode($res);

        if( ! empty($result) && ! empty($result->email))
        {

            $user = new User();
            
            $user_fb    =   $user->findByFbId($result->id);
            
            if( ! empty($user_fb) && ! empty($user_fb->email))
            {
                $userEmail   =   User::findByEmail($user_fb->email);
                
                if(Yii::$app->user->login($userEmail, TRUE ? 3600*24*30 : 0))
                    return $this->redirect(['/']);
                else
                    return $this->redirect(['login']);
            }
            $email_exist =  User::findByEmail($result->email);

            if( ! empty($email_exist))
            {
                $email_exist->fb_id    =   $result->id;
                if($email_exist->save()){

                    $userEmail   =   User::findByEmail($result->email);
                    if(Yii::$app->user->login($userEmail, TRUE ? 3600*24*30 : 0))
                    return $this->redirect(['/']);

                }

            }
            
            //$user->scenario = 'sign-up';
            $profile = new UserProfile();
            
            $user->fb_id    =   $result->id;
            $user->username    =   $result->email;
            $user->generateAuthKey();
            $user->email    =   $result->email;
            $user->status = 10;
            $user->created_at = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");
            
            if($user->save()){
                $sendBirdUserId = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) ).$user->id;
                $profile->user_id = $user->id;
                $profile->save_id       =   1;
                $profile->firstname =   $result->first_name;
                $profile->lastname  =   $result->last_name;
                
                if( ! empty($result->picture) && ! empty($result->picture->data->url))
                {
                    $imgdata   =   file_get_contents($result->picture->data->url, false);
                    
                    if( ! empty($imgdata))
                    {
                        file_put_contents('web/uploads/users/'.$profile->user_id.'.jpg', $imgdata);
                        
                        $profile->profile_image =   $profile->user_id.'.jpg';
                        $profile->save_id       =   0;
                    }
                }
                else
                {
                    $profile->profile_image =   'images/img1.jpg';
                }
                
                $profile->save();

//                $userArray['user_id'] = $sendBirdUserId;
//                $userArray['nickname'] = $profile->firstname." ".$profile->lastname;
//                $userArray['profile_url'] = \yii\helpers\Url::base('https').'/web/uploads/users/'.$profile->profile_image;
//                $chatUsers = json_encode($userArray);
//                $curl = curl_init();
//
//                curl_setopt_array($curl, array(
//                    CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users',
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_ENCODING => '',
//                    CURLOPT_MAXREDIRS => 10,
//                    CURLOPT_TIMEOUT => 0,
//                    CURLOPT_FOLLOWLOCATION => true,
//                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                    CURLOPT_CUSTOMREQUEST => 'POST',
//                    CURLOPT_POSTFIELDS => $chatUsers,
//                    CURLOPT_HTTPHEADER => array(
//                        'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
//                        'Content-Type: application/json',
//                        'Accept: application/json'
//                    ),
//                ));
//
//                $response = curl_exec($curl);
//
//                curl_close($curl);
//                $user->sendbird_user_id = $sendBirdUserId;
//                $user->save(false);
                
                $userEmail   =   User::findByEmail($user->email);
                
                if(Yii::$app->user->login($userEmail, TRUE ? 3600*24*30 : 0))
                    return $this->redirect(['/']);
            }
        }
    }
    
    public function actionSignUp()
    {

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $user = new User();
        $user->scenario = 'sign-up';
        $profile = new UserProfile();
        $profile->scenario = 'signup';
        $countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');	

        $privacy_policy = \app\models\Page::find()->where(['p_id'=>1])->one();		
        $terms = \app\models\Page::find()->where(['p_id'=>2])->one();		
        $community_guidelines = \app\models\Page::find()->where(['p_id'=>3])->one();		


        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())){	
            		
            $user->setPassword($user->password);
            $user->generateAuthKey();
            $user->status = 13;
            $user->created_at = date("Y-m-d H:i:s");
            $user->updated_at = date("Y-m-d H:i:s");

            if (!empty(DeletedUsers::findIfDeleted($user->email))){
                //todo propper error UI 
                // return ;
                return $this->render('sign_up', [
                    'user' => $user,
                    'profile' => $profile,
                    'countries' => $countries,
                    'privacy_policy' => $privacy_policy,
                    'terms' => $terms,
                    'error' => 'This email is flagged and cannot be used anymore',
                    'community_guidelines' => $community_guidelines,
                    ]);
            }

            if( ! empty(User::findByEmail($user->email)))
            {    
                return $this->render('sign_up', [
                    'user' => $user,
                    'profile' => $profile,
                    'countries' => $countries,
                    'privacy_policy' => $privacy_policy,
                    'terms' => $terms,
                    'error' => 'Email already exists! Please try different.',
                    'community_guidelines' => $community_guidelines, 
                    ]);
                // return 'Email already Exists! Try loging in with username and Password.';
            }




            if($user->save()){
                $sendBirdUserId = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) ).$user->id;

                $profile->user_id = $user->id;
                $profile->save_id   =   1;

                if(empty(Yii::$app->request->post()['UserProfile']['profile_image']))
                {
                    $profile->profile_image = $profile->dulpicate_image;
                }
                else
                {
                    if (!empty(Yii::$app->request->post()['UserProfile']['dulpicate_image'])) {
                        $profile->profile_image = Yii::$app->request->post()['UserProfile']['dulpicate_image'];
                    } elseif (isset(Yii::$app->request->post()['UserProfile']['profile_image'])) {
                        $data = Yii::$app->request->post()['UserProfile']['profile_image'];
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        file_put_contents('web/uploads/users/' . $profile->user_id . '.jpg', $data);

                        $profile->profile_image = $profile->user_id . '.jpg';
                        $profile->save_id = 0;
                    }
                }

                $profile->save();


//                $userArray['user_id'] = $sendBirdUserId;
//                $userArray['nickname'] = $profile->firstname." ".$profile->lastname;
//                $userArray['profile_url'] = \yii\helpers\Url::base('https').'/web/uploads/users/'.$profile->profile_image;
//                $chatUsers = json_encode($userArray);
//                $curl = curl_init();
//
//                curl_setopt_array($curl, array(
//                    CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users',
//                    CURLOPT_RETURNTRANSFER => true,
//                    CURLOPT_ENCODING => '',
//                    CURLOPT_MAXREDIRS => 10,
//                    CURLOPT_TIMEOUT => 0,
//                    CURLOPT_FOLLOWLOCATION => true,
//                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                    CURLOPT_CUSTOMREQUEST => 'POST',
//                    CURLOPT_POSTFIELDS => $chatUsers,
//                    CURLOPT_HTTPHEADER => array(
//                        'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
//                        'Content-Type: application/json',
//                        'Accept: application/json'
//                    ),
//                ));
//
//                $response = curl_exec($curl);
//
//                curl_close($curl);
//                $user->sendbird_user_id = $sendBirdUserId;
//                $user->save(false);

                // echo $user->email."aici";
                $profile->sendVAlidationEmail($user->email);
                // exit;
                // $profile->sendVAlidationEmail("alexcapitaneanu@gmail.com");
                Yii::$app->session->setFlash('RegisterFormSubmitted');
                //return $this->redirect(['success-signup', 'profile' => $profile]);
                return $this->render('success_signup', [
                    'user' => $user,
                    'profile'=>$profile
                ]);
            } else 
            { 
                return $this->render('sign_up', [
                'user' => $user,
                'profile' => $profile,
                'countries' => $countries,
                'privacy_policy' => $privacy_policy,
                'terms' => $terms,
                'community_guidelines' => $community_guidelines,
                ]);
            }
        }
        else return $this->render('sign_up', [
            'user' => $user,
            'profile' => $profile,
            'countries' => $countries,
            'privacy_policy' => $privacy_policy,
            'terms' => $terms,
            'community_guidelines' => $community_guidelines,
        ]);
    }
	public function actionGetStates($country_id){
		$states = \app\models\State::find()->where(['country_id'=>$country_id])->all();
		if(count($states)>0){
			echo "<option value=''>--Select State--</option>";
			foreach($states as $state){
				echo "<option value='".$state->id."'>".$state->name."</option>";
			}			
		}
		else{
			echo "<option value=''>-</option>";
		}
	}
	public function actionGetCities($state_id){
		$cities = \app\models\City::find()->where(['state_id'=>$state_id])->all();
		if(count($cities)>0){
			echo "<option value=''>--Select City--</option>";
			foreach($cities as $city){
				echo "<option value='".$city->id."'>".$city->name."</option>";
			}			
		}
		else{
			echo "<option value=''>-</option>";
		}
	}


    public function actionResendverificationlink($user)
    {
        $usermodel = User::findOne([
            'status' => User::STATUS_INACTIVE,
            'email' => urldecode($user),
        ]);
        if(!empty($usermodel)){
            $profile = new UserProfile();
            if(!empty($profile)){
                $profile->sendVAlidationEmail($usermodel->email);
                echo "<script>
alert('We have resent verification email.Please check your inbox.');</script>";
                return $this->redirect(['login']);

            }
        }else{

        }

        //return $this->render('requestPasswordResetToken', ['model' => $model,]);
    }
	public function actionEditorial(){
		$model = \app\models\Editorial::find()->where(['status'=>0])->orderBy()->all();		
		return $this->render('', [
            'model' => $model,	
			]);
	}
	
    public function actionRequestPasswordReset()
    {		
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->redirect(['login']);
            } else { 
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
				
            }			   
        } 
        return $this->render('requestPasswordResetToken', ['model' => $model,]);
    }
	
    public function actionResetPassword($token)
    {
        try {         
            $user = User::findByPasswordResetToken($token);

            if(!$user)
            {
                    return $this->goHome();
            }
             $model = new ResetPasswordForm($token);
			
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
			
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');			
            $model->sendEmailResetSuccess();			
             return $this->redirect(['login']);
			
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    } 

	/**
	 * Send test mail
	 * Only for debugging purposes
	 */
	public function actionTestMail(){
	$sendmail =	Yii::$app->mailer->compose()
			->setTo('ketansangani12@gmail.com')
			->setFrom(['admin@simplywishes.com' => 'Dency G B'])
			->setSubject('Test mail from simplywishes')
			->setTextBody('Regards')
			->send();
    var_dump($sendmail);exit;
	}

	
	/**
     * Lists all Wish models.
     * @return mixed
     */
    public function ddexHome()
    {

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $userWishes = Wish::find()->where(['wished_by'=>$user->id])->all();

        $properties = Utils::getUserProperties($user->id);

        return $this->render('index_home',[
              'user' => $user,
              'properties' => $properties,
              'userWishes' => $userWishes,
              'profile' => $profile 
          ]);
    }
	
	
	public function actionTest(){
		
		/// Cron JOb Function mail
		$oldApp = \Yii::$app;		
		$config  = require(__DIR__ . '/../config/console.php');
		$console = new \yii\console\Application($config);
		\Yii::$app->runAction('onemonth/index');
		\Yii::$app = $oldApp;
	}
	
	
	public function actionUserValidation($auth_key)
    {
		
		if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
		
        try { 
        
			$user = User::findByAuthKeyValidation($auth_key);			
			if($user)
			{
				
				 $model = User::findOne($user->id);
				 $model->status = 10;
				 if($model->save())
				 {
					 $profile = UserProfile::find()->where(['user_id'=>$user->id])->one();
					 $profile->sendEmail($user->email);
					 Yii::$app->session->setFlash('activeCheckmail');
				 }	 
			} 	 
				return $this->redirect(['login']);
			 							
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
			              
    }

	
}
