<?php

namespace app\controllers;

use app\models\Message;
use app\models\Wish;
use Yii;
use app\models\Donation;
// use app\models\Wish;    
use app\models\WishComments;
use app\models\DonationComments;
use app\models\DonationCommentsActivities;
use app\models\Activity;
use app\models\ReportWishes;
use app\models\ReportContent;
use app\models\search\SearchWish;
use app\models\search\SearchDonation;
use app\models\search\SearchReportWishes;
use app\models\User;
use app\models\UserProfile;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\MailContent;
use app\controllers\Utils;



use app\models\Payment;
use app\models\WishCommentsActivities;
use yii\widgets\ActiveForm;

/**
 * DonationController implements the CRUD actions for Donation model.
 */
class DonationController extends Controller
{
	public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * Creates a new Donation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = User::findOne(Yii::$app->user->id);
        $properties = Utils::getUserProperties(Yii::$app->user->id);
        if (!$properties['can_create_donation']){
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }


        $model = new Donation();

        $model->scenario = 'create';	

        
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        // die("create");

        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        if ($model->load(Yii::$app->request->post()))
        {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            //echo "<pre>";print_r($_POST);exit;
            $model->created_by = \Yii::$app->user->id;
            $model->status = 0;


            if(!empty($model->auto_id))
            {
                $model1 = Donation::findOne($model->auto_id);
                $model1->id = $model->auto_id;
                $model1->created_by = \Yii::$app->user->id;
                $model1->title = $model->title;
                $model1->description = strip_tags($model->description);
                $model1->image = $model->image;
                $model1->expected_cost = $model->expected_cost;
                $model1->expected_date = $model->expected_date;
                $model1->who_can = $model->who_can;
                $model1->non_pay_option = $model->non_pay_option;
                $model1->status = $model->status;
                $model1->way_of_donation = $model->way_of_donation;
                $model1->description_of_way = $model->description_of_way;
                $model1->financial_assistance = $model->financial_assistance;
                $model1->financial_assistance_other = $model->financial_assistance_other;
                //$model1->delivery_type = $model->delivery_type;
                //$model1->delivery_other = $model->delivery_other;
                $model1->acknowledgement = $model->acknowledgement;
                
                
                $model1->show_mail_status = $model->show_mail_status;
                $model1->show_mail = $model->show_mail;
                $model1->show_person_status = $model->show_person_status;
                $model1->show_person_street = $model->show_person_street;
                $model1->show_person_city = $model->show_person_city;
                $model1->show_person_state = $model->show_person_state;
                $model1->show_person_zip = $model->show_person_zip;
                $model1->show_person_country = $model->show_person_country;
                $model1->show_other_status = $model->show_other_status;
                $model1->show_other_specify = $model->show_other_specify;
                $model1->i_agree_decide = $model->i_agree_decide;
                $model1->i_agree_decide2 = $model->i_agree_decide2;
                $model1->process_status = $model->process_status;
                $model1->process_granted_by = $model->process_granted_by;
                $model1->process_granted_date= $model->process_granted_date;			

                $model1->update(false);
            }
            else
            {
                $model->image = null;
                $model->created_at = date("Y-m-d H:i:s");
                // $model1->description = strip_tags($model->description);
                //$model->delivery_type = implode(',', $model->delivery_type);
                //$model->delivery_other = Yii::$app->request->post()['Donation']['delivery_other'];

                $model->save();
            }
            if(!empty(Yii::$app->request->post()['Donation']['image']) && strpos(Yii::$app->request->post()['Donation']['image'], 'base64') !== false)
            {
                $data   =   Yii::$app->request->post()['Donation']['image'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents('web/uploads/donations/' . $model->id . '.' . 'jpg', $data);

                $model->image = 'uploads/donations/' . $model->id . '.jpg';

            }
            else
            {
                if($model->image_name!='') {
                    $model->image = $model->image_name;
                }else{
                    $model->image = 'images/wish_default/2.jpg';///web/images/wish_default/2.jpg
                }
            }

//            if(!empty(Yii::$app->request->post()['Donation']['image_name']))
//            {
//               // echo "sdsd";exit;
//                if(empty($model->image_name))
//                {
//                    $model->image =   'images/wish_default/1.jpg';
//                }
//                else {
//                    $model->image = $model->image_name;
//                }
//            }
//            else
//            {
//                //echo "sdsd";exit;
//                $data   =   Yii::$app->request->post()['Donation']['image'];
//                if (strpos($data, 'base64') !== false) {
//
//                    list($type, $data) = explode(';', $data);
//                    list(, $data) = explode(',', $data);
//                    $data = base64_decode($data);
//                    $filename = $model->id.'.' . 'jpg';
//
//                    file_put_contents('web/uploads/donations/' . $filename, $data);
//
//                    $model->image = 'uploads/donations/' . $filename;
//                }
//            }
           $model->save();
          $model->sendCreateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['account/my-account']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'profile' => $profile
            ]);
        }
    }

    public function actionSearch(){
        echo "search";
        $searchModel = new SearchDonation();
        //$cat_id = null;
        //$searchModel->wish_title = Yii::$app->request->queryParams['match'];
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        return $this->render('searched_donations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'  =>  $user,
            'profile' => $profile
        ]);		
        
    }
    
    public function actionSearchScroll($page){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);

        $dataProvider->pagination->page = $page;
        $str = '';
		
        foreach($dataProvider->models as $wish){
            $str .= $wish->donationAsCard;
        }
        return $str;
        
    }
    
    public function actionSearchUser(){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustomUser(Yii::$app->request->queryParams);

        return $this->render('searched_user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);		
        
    }
    /**
     * Lists all Wish models.
     * @return mixed
     */
    public function actionIndex($cat_id=null)
    {
        $searchModel = new SearchDonation();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        // print_r($dataProvider);
        return $this->render('current_donations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cat_id' => $cat_id,
            'user'  =>  $user,
            'profile' => $profile,
            'type'=>'donation'
        ]);
    }
    /**
     * Lists all Donation models when scrolls.
     * @return mixed
     */
    public function actionScroll($page,$cat_id=null)
    {
        $searchModel = new SearchDonation();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
		$dataProvider->pagination->page = $page;
        $str = '';
		//if ($dataProvider->totalCount > 0) {
        foreach($dataProvider->models as $donation){
				$str .= $donation->donationAsCard;
        }
		//}
        return $str;
    }
    
    public function actionProgress()
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchProgress(Yii::$app->request->queryParams);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        return $this->render('progress_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'  =>  $user,
            'profile' => $profile
        ]);
    }
    
    public function actionScrollProgress($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchProgress(Yii::$app->request->queryParams);
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->donationAsCard;
        }
        return $str;
    }
    
    /**
     * Lists all Wish models according to the number of likes.
     * @return mixed
     */
    public function actionPopular()
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        return $this->render('popular_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'  =>  $user,
            'profile'=> $profile
        ]);
		
    }

        
    public function actionReportContentPost(){
        $report = new ReportContent();
        $postData = Yii::$app->request->post();
        // print_r($postData);exit;
        $report->content_id = $postData['comment_id'];
        $report->report_user = \Yii::$app->user->id;
        $report->reported_user = $postData['reported_user'];
        $report->report_type= $postData['report_type'];
        $report->comment=$postData['comment'];
        $report->date = date('Y-m-d H:i:s');
        
        // print_r($report);exit;
        if ($report->save(false)){
            Utils::sendEmail(1,'',21);
            return true;
        } 
        return false;
    }

    public function actionScrollPopular($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
		$dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->donationAsCard;
        }
        return $str;
    }
	public function actionGranted(){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);

        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        return $this->render('fullfilled_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user'  =>  $user,
            'profile'   =>  $profile
        ]);		
	}
    public function actionScrollGranted($page)
    {
        $searchModel = new SearchDonation();
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
			$str .= $wish->donationAsCard;
        }
        return $str;
    }
    /**
     * Displays a single Wish model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        //->createCommand()->getRawSql()
        $donation = $this->findModel($id);
        $loggedUserId = \Yii::$app->user->id;
        $listcomments = new DonationComments();
        $comments = DonationComments::find()->where(['d_id'=>$id,'parent_id'=>0,'status'=>0])->orderBy('d_comment_id Desc')->all();		 
        // print_r($donation);
        // die("here");      
        $isReported = Utils::checkIsReported($id, 'donation');
        return $this->render('view', [
            'model' => $donation, 'comments'=>$comments,'listcomments'=>$listcomments, "isReported" => $isReported
        ]);
    }
    
    public function actionDonationComments()
    {

        $model = new DonationComments();

        if($model->load(Yii::$app->request->post()))
        {


            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }
            // print_r($model);
            // die("here");
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                $donation = $this->findModel($model->d_id);
                 
                if($donation->created_by != \Yii::$app->user->id)
                    $this->sendCommentEmail($donation, $model->comments);


                $get_user_id  = \app\models\Donation::findOne([
                    'id' => $model->d_id,
                ]);

                $wish_creater = \app\models\UserProfile::findOne([
                    'user_id' => $get_user_id->created_by,
                ]);
                $comment_creater = \app\models\UserProfile::findOne([
                    'user_id' => \Yii::$app->user->id,
                ]);


                $user1url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile','id'=>\Yii::$app->user->id]);
                $user1 = Html::a($comment_creater->firstname, $user1url,['style'=>'color:blue;text-decoration: underline;']);


                $donationurl = Yii::$app->urlManager->createAbsoluteUrl(['donation/view','id'=>$model->d_id]);
                $donationlink = Html::a($get_user_id->title, $donationurl,['style'=>'color:blue;text-decoration: underline;']);


                $details ="Hello ".$wish_creater->firstname.",
                
                You’ve received a public comment on ".$donationlink." from ".$user1." 
                     
                ".$model->comments;

                $msg = $details;
                if(\Yii::$app->user->id != '' && $get_user_id->created_by != '' && $msg != '') {
                    $message = new Message();
                    $message->sender_id = \Yii::$app->user->id;
                    $message->recipient_id = $get_user_id->created_by;
                    $message->parent_id = 0;
                    $message->read_text = 0;
                    $message->text = $msg;
                    $message->created_at = date("Y-m-d H:i:s");
                    if ($message->save()) {
                        Yii::$app->session->setFlash('messageSent');
                    }
                }

                return $this->redirect(['donation/view?id='.$model->d_id]);
            }else{
                
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }
        }
    }
    
    public function actionCommentLikesView($id)
    {
        $comment = DonationComments::find()->where(['d_comment_id'=>$id])->one();
        $likedUser  =   [];
        if( ! empty($comment->likes))
        {
            foreach($comment->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();

                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }

        return json_encode($likedUser);
    }

    public function actionLikeComment($s_id)
    {
        
        if(\Yii::$app->user->isGuest)
            return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
            
        $comment = DonationComments::find()->where(['d_comment_id'=>$s_id])->one();
        
        $activity = DonationCommentsActivities::find()->where(['d_comment_id'=>$comment->d_comment_id,'user_id'=>\Yii::$app->user->id])->one();
        
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        
        $activity = new DonationCommentsActivities();
        $activity->d_comment_id = $comment->d_comment_id;
        $activity->user_id = \Yii::$app->user->id;

        if($activity->save())
            return "added"; 
        else return false;
    }
    
    public function actionCommentreply()
    {		
        
        $model = new DonationComments();
        if($model->load(Yii::$app->request->post()))
        {		
            
            if(\Yii::$app->user->isGuest){			
                die($model->d_id);
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }

            
            $model->user_id = \Yii::$app->user->id;
            
            
            if($model->save())
            {
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }else{
             
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            } 
        }
    }
    
    public function actionUpdateComment($id)
    {
        $model = DonationComments::findOne($id);
        
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['donation/view?id='.$model->d_id]);
            }
        }
    }
    
    public function actionDeleteComment($id)
    {
        $model  =   DonationComments::findOne($id);
        $model->status  =   1;
        
        if($model->parent_id == 0)
        {
            $replycomments = DonationComments::find()->where(['parent_id'=>$id])->all();
            
            foreach($replycomments as $reply)
            {
                $reply->status  =   1;
                $reply->save();
            }
        }
        
        $model->save();
        return $this->redirect(['donation/view?id='.$model->d_id]);
    }

   

    /**
     * Updates an existing Wish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); 
        $model->scenario = 'update';

        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->image;
        
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post())) {

            //check for a new image
            $model->created_by = \Yii::$app->user->id;
            //$delivery_type = implode(',', Yii::$app->request->post()['Donation']['delivery_type']);
            //$model->delivery_type = $delivery_type;
            //$model->delivery_other = Yii::$app->request->post()['Donation']['delivery_other'];

            $model->save();

            
            if(!empty(Yii::$app->request->post()['Donation']['image_name']))
            {
                if(empty($model->image_name))
                {
                    $model->image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->image_name) && ($model->image_name != $current_image ))
                {
                        $model->image = $model->image_name;
                } else {
                        $model->image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Donation']['image'])
                {
                    $data   =   Yii::$app->request->post()['Donation']['image'];
                    if (strpos($data, 'base64') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        $filename = time().uniqid().'.' . 'jpg';
                        file_put_contents('web/uploads/donations/' . $filename, $data);

                        $model->image = 'uploads/donations/' . $filename;
                    }
                }
                else
                {
                    if(! empty($model->image_name))
                        $model->image = $model->image_name;
                    else
                        $model->image = $current_image;
                }
            }
            
            //save model
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user ,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Deletes an existing Wish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
         $this->findModel($id)->delete();
        return $this->redirect(['index']); 
    }

	
	public function actionAjaxDelete()
    {
		$id = Yii::$app->request->post('id');
		$this->findModel($id)->delete();
    }
	
	    /**
     * Like a wish
     * User has to be logged in to like a wish
     * Param: wish id
     * @return boolean
     */
    public function actionReportWish()
    {
        echo "report";
    }
	
    /**
     * Like a wish
     * User has to be logged in to like a wish
     * Param: wish id
     * @return boolean
     */
    public function actionLike($id,$type)
    {
        if(\Yii::$app->user->isGuest)
            return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
        $donation = $this->findModel($id);
        $activity = Activity::find()->where(['donation_id'=>$donation->id,'activity'=>$type,'user_id'=>\Yii::$app->user->id])->one();
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        $activity = new Activity();
        $activity->donation_id = $donation->id;
        $activity->activity = $type;
        $activity->user_id = \Yii::$app->user->id;

        try{
            if($activity->save())
            { 
                $this->sendLikedEmail($donation);
                return "added";
            }
            else {
                echo "else";
            }
               
        } catch (yii\db\Exception $e) {
            echo $e;
        }
        return false;
    } 

    public function actionFullfilled($w_id){
        $wish = $this->findModel($w_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.

        $wish->granted_by = \Yii::$app->user->id;
        $wish->granted_date = date('Y-m-d H:i:s');

        if($wish->save(false))
        {			
            $this->sendEmail($wish);		
            return $this->redirect(['donation/view','id'=>$w_id]);
        }
    }
    /**
     * Finds the Wish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Wish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Donation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionTopWishers(){
        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
        
        return $this->render('iWish', [
            'dataProvider' => $dataProvider,
        ]);		
    }
	
	public function actionTopGranters(){
		$query = Wish::find()->select(['wishes.granted_by,count(w_id) as total_wishes'])->where(['not', ['granted_by' => null]])->orderBy('total_wishes DESC');
		$query->groupBy('granted_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
		return $this->render('iGrant', [
			'dataProvider' => $dataProvider,
		]);		
	}
	
	/**
	 * IPN listener for paypal
	 * Ref: http://stackoverflow.com/questions/14015144/sample-php-code-to-integrate-paypal-ipn
	 * Change the status back to not paid if not veified
	 */
	public function actionVerifyGranted(){
		// STEP 1: Read POST data

		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$raw_post_data = file_get_contents('php://input');
			$fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $raw_post_data);
			  fclose($fh);
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
				$value = urlencode(stripslashes($value)); 
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}


		// STEP 2: Post IPN data back to paypal to validate

		//https://www.sandbox.paypal.com/cgi-bin/webscr
		//$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
		$ch = curl_init('https://ipnpb.paypal.com/cgi-bin/webscr');

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if( !($res = curl_exec($ch)) ) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);


		// STEP 3: Inspect IPN validation result and act accordingly

		if (strcmp ($res, "VERIFIED") == 0) {
			// check whether the payment_status is 
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
			$payment = new Payment();
			// assign posted variables to local variables
			$payment->item_name = $_POST['item_name'];
			$payment->item_number = $_POST['item_number'];
			$payment->payment_status = $_POST['payment_status'];
			$payment->payment_amount = $_POST['mc_gross'];
			$payment->payment_currency = $_POST['mc_currency'];
			$payment->txn_id = $_POST['txn_id'];
			$payment->receiver_email = $_POST['receiver_email'];
			$payment->payer_email = $_POST['payer_email'];
			$payment->payment_date = $_POST['payment_date'];
			$payment->save();
			//check if success
			$wish = $this->findModel($_POST['item_number']);
			//if not fully paid or if not successful, revert the granted status
			if($payment->payment_status != "Completed"){
				$wish->granted_by = NULL;
				$wish->save();
			}

		} else if (strcmp ($res, "INVALID") == 0) {
			  // Save the output (to append or create file)
			  $fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $res);
			  fclose($fh);
			// log for manual investigation
		}		
	}
	
	public function actionRemoveDonation($d_id)
	{

		if(\Yii::$app->user->isGuest)
			return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
		$donation = $this->findModel($d_id);
		$activity = Activity::find()->where(['donation_id'=>$donation->id,'activity'=>'fav','user_id'=>\Yii::$app->user->id])->one();
		if($activity != null){
			$activity->delete();
		}
		
		 return $this->redirect(['account/my-saved']);
	}
    
    public function sendEmail($donation)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>25])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes sendEmail';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->created_by,
        ]);
        
        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->process_granted_by,
        ]);
		
        if (!$user) {
            return false;
        }
        if (!$user2) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'acceptedDonationSuccess-html'],
                ['user' => $user, 'user2'=>$user2, 'editmessage' => $editmessage, 'donation' => $donation ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                // die($message);
        return $message->send();
    }
    
    public function sendLikedEmail($donation)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>27])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->created_by,
        ]);
        
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
		
        if (!$user || !$likedUser || $user->id === $likedUser->id) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'donationLike-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation, 'likedUser' =>  $likedUser]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            // ->setTo($user->email)
            ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendCommentEmail($donation, $comment)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>26])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->created_by,
        ]);
         
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
        
        if (!$user || !$likedUser) {
            return false;
        }
        $email_subject = str_replace("##USERNAME2##", $likedUser->username,$mailcontent->mail_subject);
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'donationComment-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation, 'likedUser' =>  $likedUser, 'comment' => $comment]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($email_subject);
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendEmailToAccepted($id, $donation)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>24])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
        

        if (!$user) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccessDonation-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);			
            // print_r($donation);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        
        return $message->send();
    }

    public function sendEmailToCompleteDonation($id, $donation)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>34])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);


        if (!$user) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'completeDonation-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);
        // print_r($donation);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
	
    public function actionLikesView($w_id)
    {
        $wish = $this->findModel($w_id);
        $likedUser  =   [];
        if( ! empty($wish->likes))
        {
            foreach($wish->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();
                
                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }
        
        return json_encode($likedUser);
    }
    
    //toaddreport
    public function actionReportContent($w_id, $content_type, $content_id='', $report_user='', $reported_user='', $comment ='')
    {

        if(\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login', 'red_url' => Yii::$app->request->referrer]);
        }
                
        $content_type_arr=array("article","article_comment","wish","wish_comment","message","user");
        
        if ($content_type == "wish"){
            $this->actionReport($w_id);
            $wish = $this->findModel($w_id);

            $activity = new ReportContent();
            // $activity->id = 43;
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = $comment;
            $activity->content_id = $w_id;
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            if ($activity->save()){
                echo "saved";
            } else {
                echo "not saved";
            }
        // left explicit else for clarity    
        }else if ($content_type == "donation"){
        $wish = Donation::findOne($w_id);

        $activity = new ReportContent();
        // $activity->id = 43;
        $activity->report_user = $report_user;
        $activity->reported_user = $reported_user;
        $activity->report_type = $content_type;
        $activity->comment = $comment;
        $activity->content_id = $w_id;
        $activity->date = date('Y-m-d H:i:s');
        $activity->blocked = "0";

        if ($activity->save()){
            echo "saved";
        } else {
            echo "not saved";
        }
        // left explicit else for clarity
    } else if ($content_type == "user" || $content_type == "wish_comment" || $content_type == "message"){
            $activity = new ReportContent();
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = $comment;
            $activity->content_id = $w_id;
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            // print_r($activity);
            // exit;
            if ($activity->save()){
                echo "saved";
            } else {
                echo "not saved";
            }
        }
        return "ok";
    }

    public function actionReport($w_id)
    {
        if(\Yii::$app->user->isGuest)
                return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

        $wish = $this->findModel($w_id);
        $activity = ReportWishes::find()->where(['w_id'=>$wish->w_id])->one();
        if($activity){
                $activity->count = $activity->count + 1;
                $activity->save();
                return "added";			
        } else {
            $activity = new ReportWishes();
            $activity->w_id = $wish->w_id;
            $activity->count = 1;	
            $activity->save();
                    return "added";
        }
    }	
	
	public function actionReportAction()
	{
	 if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
		$searchModel = new SearchReportWishes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('report_view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
	 } else {
		 return $this->redirect(['site/index']);
	 }
	}
	
	
	public function actionReportActionView($id)
    {
		if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$wish = Wish::find()->where(['w_id'=>$id])->one();		
			if($wish)
			{			
				return $this->render('report_full_view', ['model' => $this->findModel($id) ]);
			} else {
					return $this->redirect('report-action');
			}	
		} else {
				 return $this->redirect(['site/index']);
			 }		
    }

	
	public function actionReportDelete()
    {
	  if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$id = Yii::$app->request->post('id');
			$this->findModel($id)->delete();
			$model = ReportWishes::find()->where(['w_id'=>$id])->one();
			$model->delete();
		 } else {
		 return $this->redirect(['site/index']);
	  }
    }
	
    public function actionUploadFile(){
        if( ! empty(Yii::$app->request->post()['image']) && ! empty(Yii::$app->request->post()['id']))
        {
            $data   =   Yii::$app->request->post()['image'];

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            file_put_contents('web/uploads/donations/'.Yii::$app->request->post()['id']. '.' .'jpg', $data);

            return 'uploads/donations/'.Yii::$app->request->post()['id'].'.jpg';
        }
   }
	
    public function actionDonationAutosave()
    {	

        if(Yii::$app->request->post())
        {

            $models2 = Yii::$app->request->post();
            if($models2['Donation']['id']=='')
            {
                $models = new Donation();
                $models->title = $models2['Donation']['title'];
                $models->created_by = \Yii::$app->user->id;
                $models->status = 1;
                $models->save(false);
                $models2['Donation']['id'] =   $models->id;
            }

            $models =  Donation::find()->where(['id'=>$models2['Donation']['id']])->one();

            $models->title = $models2['Donation']['title'];		
            $models->description = $models2['Donation']['description'];
            
            /*if(empty($models2['Donation']['image']) && empty($models2['Donation']['image_name']))
                $models->image = 'images/wish_default/1.jpg';
            else if( ! empty($models2['Donation']['image_name']))
                $models->image = $models2['Donation']['image_name'];
            else
                $models->image = $models2['Donation']['image'];*/

            $models->expected_cost = (isset($models2['Donation']['expected_cost']))?$models2['Donation']['expected_cost']:'';
            $models->expected_date = (isset($models2['Donation']['expected_date']))?$models2['Donation']['expected_date']:'';
            $models->who_can = (isset($models2['Donation']['who_can']))?$models2['Donation']['who_can']:'';
            $models->non_pay_option = (isset($models2['Donation']['non_pay_option']))?$models2['Donation']['non_pay_option']:'';
            $models->financial_assistance = (isset($models2['Donation']['financial_assistance']))?$models2['Donation']['financial_assistance']:'';
            $models->financial_assistance_other = (isset($models2['Donation']['financial_assistance_other']))?$models2['Donation']['financial_assistance_other']:'';
            $models->expected_cost = (isset($models2['Donation']['expected_cost']))?$models2['Donation']['expected_cost']:'';
            $models->way_of_donation = (isset($models2['Donation']['way_of_donation']))?$models2['Donation']['way_of_donation']:'';
            $models->description_of_way = (isset($models2['Donation']['description_of_way']))?$models2['Donation']['description_of_way']:'';

            $models->show_mail_status = (isset($models2['Donation']['show_mail_status']))?$models2['Donation']['show_mail_status']:'';
            $models->show_mail = (isset($models2['Donation']['show_mail']))?$models2['Donation']['show_mail']:'';
            $models->show_person_status = (isset($models2['Donation']['show_person_status']))?$models2['Donation']['show_person_status']:'';
            $models->show_person_street = (isset($models2['Donation']['show_person_street']))?$models2['Donation']['show_person_street']:'';
            $models->show_person_city = (isset($models2['Donation']['show_person_city']))?$models2['Donation']['show_person_city']:'';
            $models->show_person_state = (isset($models2['Donation']['show_person_state']))?$models2['Donation']['show_person_state']:'';
            $models->show_person_zip = (isset($models2['Donation']['show_person_zip']))?$models2['Donation']['show_person_zip']:'';
            $models->show_person_country = (isset($models2['Donation']['show_person_country']))?$models2['Donation']['show_person_country']:'';
            $models->show_other_status = (isset($models2['Donation']['show_other_status']))?$models2['Donation']['show_other_status']:'';
            $models->show_other_specify = (isset($models2['Donation']['show_other_specify']))?$models2['Donation']['show_other_specify']:'';
            $models->status = 1;



            if(!empty(Yii::$app->request->post()['Donation']['image']) && strpos(Yii::$app->request->post()['Donation']['image'], 'base64') !== false)
            {
                $data   =   Yii::$app->request->post()['Donation']['image'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents('web/uploads/donations/' . $models->id . '.' . 'jpg', $data);

                $models->image = 'uploads/donations/' . $models->id . '.jpg';

            }
            else
            {
                if($models2['Donation']['image_name'] != '') {
                    $models->image = $models2['Donation']['image_name'];
                }else{
                    $models->image = 'images/wish_default/1.jpg';
                }
            }

            //$models->delivery_type = (isset(Yii::$app->request->post()['Donation']['delivery_type']) && !empty(Yii::$app->request->post()['Donation']['delivery_type']))?implode(',', Yii::$app->request->post()['Donation']['delivery_type']):'';

           // $models->delivery_other = Yii::$app->request->post()['Donation']['delivery_other'];
            
            
            if($models->save(false))
            {
                return $this->redirect(['my-drafts']);
            }
            else
            {	
                echo "failed2";
            }

        }	
        
    }
	
	
		/**
     * Lists all Editorial models.
     * @return mixed
     */
    public function actionMyDrafts()
    {
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $searchModel = new SearchDonation();
        $dataProvider = $searchModel->searchDrafts(Yii::$app->request->queryParams);

        return $this->render('my_drafts', [
            'user' => $user,
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
    public function actionDeleteDraft($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['my-drafts']); 
    }
    
    public function actionDeleteDonation($id)
    {

        $this->findModel($id)->delete();
//        echo '<script>
//                window.history.back();
//
//              </script>';

        return $this->redirect(['account/my-account']);
    }
	
    public function actionViewDraft($id)
    {
        return $this->render('view_draft', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
    public function actionUpdateDraft($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');	
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->image;

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        //print_r($_POST);exit;

        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            //check for a new image
            //$model->delivery_type = (isset(Yii::$app->request->post()['Donation']['delivery_type']) && !empty(Yii::$app->request->post()['Donation']['delivery_type']))?implode(',', Yii::$app->request->post()['Donation']['delivery_type']):'';

            $model->created_by = \Yii::$app->user->id;
            $model->save(false);
            if(!empty(Yii::$app->request->post()['Donation']['image_name']))
            {
                if(empty($model->image_name))
                {
                    $model->image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->image_name) && ($model->image_name != $current_image ))
                {
                    $model->image = $model->image_name;
                } else {
                    $model->image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Donation']['image'])
                {
                    $data   =   Yii::$app->request->post()['Donation']['image'];
                    if (strpos($data, 'base64') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);
                        $filename = time().uniqid().'.' . 'jpg';
                        file_put_contents('web/uploads/donations/' . $filename, $data);

                        $model->image = 'uploads/donations/' . $filename;
                    }
                }
                else
                {
                    if(! empty($model->image_name))
                        $model->image = $model->image_name;
                    else
                        $model->image = $current_image;
                }
            }

            $model->created_at = date('Y-m-d h:i:s');
            $model->status = 0;
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['account/my-account']);
        } else {

            $properties = Utils::getUserProperties($user->id);
            
            return $this->render('update_draft', [
                'model' => $model,
                'user' => $user ,
                'can_create' => $properties['can_create_donation'],
                'profile' => $profile
            ]);
        }
		
    }

    public function actionComplete(){

        $donation_id = \Yii::$app->request->post()['donation_id'];
   
        $donation = $this->findModel($donation_id);
        $donation->granted_by = $donation->process_granted_by;
        $donation->granted_date = date('Y-m-d H:i:s');
        $donation->process_granted_by = "";
        //$donation->process_granted_date = date('Y-m-d');
        $donation->process_status = 0;
        
        // print_r($donation);
        if($donation->save(false))
        {
            $this->sendEmailToAccepted($donation->granted_by, $donation);		
            $this->sendEmailToCompleteDonation($donation->created_by, $donation);
        }

        // exit;
    }

    public function actionTest()
    {

        $donations = Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->all();

        foreach($donations as $singledonation=>$value)
        {
            $now = time(); // or your date as well
            $your_date =  strtotime($value->process_granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));

            if ($day == 14){
                $value->granted_by = $value->process_granted_by;
                $value->granted_date = date('Y-m-d H:i:s');
                $value->process_granted_by = "";
                $value->process_status = 0;

                if($value->save(false))
                {
                    $this->sendEmailToAccepted($value->granted_by, $value);
                    //return $this->redirect(['wish/view','id'=>$w_id]);
                }
            }
        }

    }
    
    public function actionProcessDonation()
    { 
        // die("actionProcessDonation");
        $donation_id = \Yii::$app->request->post()['donation_id'];
        $processstatus = \Yii::$app->request->post()['processstatus'];
        $send_message = \Yii::$app->request->post()['send_message'];

        $donation = $this->findModel($donation_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.		
        $donation->process_granted_by = \Yii::$app->user->id;
        $donation->process_granted_date = date('Y-m-d H:i:s');
        //$donation->granted_date = date('m-d-Y');

        $donation->process_status = $processstatus;

//         print_r(\Yii::$app->request->post());
//         print_r($donation);
// exit; 
        if($donation->save(false))
        {
            if($send_message == 1){

                if ($donation->non_pay_option == 0){
                    $result = $donation->sendFinancialAcceptDonationEmail($donation->process_granted_by,$donation);
                }else{
                    $result = $donation->sendAcceptDonationEmail($donation->process_granted_by,$donation);
                }

            }
            // print_r($result);
            $res    =   $this->sendEmail($donation);
        }	
    }
		
	 public function actionResubmitProcessWish()
    {
		
        $w_id = \Yii::$app->request->post()['wish_id'];
		$userid = \Yii::$app->request->post()['userid'];
		
	if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin'))		
		$wish =  Wish::find()->where(['w_id'=>$w_id])->one();
	else
		$wish =  Wish::find()->where(['w_id'=>$w_id,'wished_by'=>$userid])->one();

		if($wish)
		{
			$wish->process_granted_by = "";
			$wish->process_granted_date = NULL;
			$wish->process_status = 0;
			$wish->email_status = 0;

			if($wish->save(false))
			{		
				echo "Success";
			}
		}
		
    }

	
	 public function actionMultiDeleteDonations()
	 {    

			$d_id = Yii::$app->request->post()['id'];	
			if($d_id)
			{
				 foreach($d_id as $tmp)
				 {										
					$this->findModel($tmp)->delete();
				 }
			}  			
	}

    public function actionSendmessage($id){
        $donationmodel = Donation::findOne($id);
        $from = \Yii::$app->user->id;
        $to = $donationmodel->created_by;
        $msg = $donationmodel->title;

        if($from != '' && $to != '' && $msg != ''){
            $message = new Message();
            $message->donation_id = $id;
            $message->sender_id = $from;
            $message->recipient_id = $to;
            $message->parent_id = 0;
            $message->read_text = 0;
            $message->subject = $msg;
            $message->created_at =  date("Y-m-d H:i:s");

            if($message->save()){
                $mailcontent = MailContent::find()->where(['m_id'=>30])->one();
                $editmessage = $mailcontent->mail_message;

                if(empty($subject))
                    $subject = 	'SimplyWishes ';
                $user = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $from,
                ]);

                $user2 = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $to,
                ]);

                $email_subject = str_replace("##USERNAME2##", $user->username,$mailcontent->mail_subject);
                $subject = $mailcontent->mail_subject;
                $mail = Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'inboxmessage'],
                        ['user' => $user, 'user2'=>$user2, 'editmessage' => $editmessage,'message'=>$message]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                    ->setTo($user2->email)
                    ->setSubject($email_subject);

                $mail->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                $mail->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                $mail->send();
                //return $this->redirect(['wish/index']);
            }else{
                print_r($message->getErrors());exit;
            }
        }
    }
	
}
