<?php

namespace app\controllers;
use Yii;
use app\models\FriendRequest;
use app\models\Message;
use app\models\User;
use app\models\Properties;
use app\models\ReportContent;
use app\models\Wish;
use app\models\Donation;
use app\models\UserProfile;
use app\models\MailContent;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use yii\helpers\Url;

class  Utils  
{

    public static function excludeReported(){

        // return   "->onCondition ( [ 'and' ,
        // [ 'not in' , 'wish_comments.user_id', $selectReported ],
        // [ '=' , 'wish_comments.w_id' , $wishId ]
        // ])";                                                                                                                                                      
    }

    public static function mapDeliveryType($develiveryTypes, $deliveryOther){
        // exit;
        if (strlen($develiveryTypes) > 1){
            $develiveryTypes = explode(",", $develiveryTypes);
        }
        // print_r($develiveryTypes);

        $mapDeliveryTypes = [
            0=>'Delivery service (USPS, FedEx, UPS)', 
            1=>'Meet in public', 
            2=>'You can drop it off at a location',
            3=>'They can pick it up at a location',
            4=>'Other'];

        $returnDeliveryType = "";
        if (is_array($develiveryTypes)){
            foreach ($develiveryTypes as $develiveryType){
                $returnDeliveryType .= " $mapDeliveryTypes[$develiveryType],";
            }
        } else {
            $returnDeliveryType .= " $mapDeliveryTypes[$develiveryTypes]";
        }
        //$returnDeliveryType = substr($returnDeliveryType,0,-1);

        if ($deliveryOther != '') {
            $returnDeliveryType .= ": $deliveryOther";
        }
        return $returnDeliveryType;
    }

    public static function getUserProperties($uid){
        $properties=[];
        $properties = Properties::find()->one()->toArray();
        $userWishes = Wish::find()->where(['wished_by'=>$uid, 'wish_status'=>0, 'granted_by'=>null])
                        ->all();
        $userDonations = Donation::find()->where(['created_by'=>$uid, 'status'=>0, 'granted_by'=>null])
                        ->all();

        $canCreateWishes = true;
        $canCreateDonations = true;

        if (count($userWishes) >= $properties['max_no_wishes']) {
            $canCreateWishes = false;
        }

        if (count($userDonations) >= $properties['max_no_donations']) {
            $canCreateDonations = false;
        }

        $properties['can_create_wish'] = $canCreateWishes;
        $properties['can_create_donation'] = $canCreateDonations;
        
        return $properties;
    }
        


    public static function isUserReportedBy($reported, $byUser){
        $reported = ReportContent::find()->where(['reported_user'=>$reported, 'report_user'=>$byUser])->count();
       // print_r($reported);
        if ($reported > 0){
            return true;
        }
        return false;
    }

    public static function checkIsReported($id, $type){
        $reported = ReportContent::find()->where(['content_id'=>$id, 'report_type'=>$type])->one();
        
        if ($reported != null){
            return true;
        }
        return false;
    }

    public static function sendEmail($userId = 0, $email = '', $template)
    {
		$mailcontent = MailContent::find()->where(['m_id'=>$template])->one();
		$editmessage = $mailcontent->mail_message;		
		$subject = $mailcontent->mail_subject;
		if(empty($subject))
            $subject = 	'SimplyWishes'; 
		
		
        /* @var $user User */
        $user = User::find()->where(['id'=>$userId])->one();
        $profile = UserProfile::find()->where(['user_id'=>$userId])->one();

        if (!$user && $userId != 0) {
            return false;
        }
      
        
        if ($email == ""){
            $email = $user->email;
        }

        if ($userId == 0){
            $username = "Admin";
        } else {
            $username = $user->username;
        }

        

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'report'],
                ['user' => $username, 'editmessage' => $editmessage,'profile'=>$profile->firstname ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($email)
            ->setSubject($subject);			
            
		$message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        // print_r($user);
        // die($message);
		return $message->send();
    }


    public static function trunc($phrase, $max_words) {
        $phrase_array = explode(' ',$phrase);
        if(count($phrase_array) > $max_words && $max_words > 0)
           $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).'...';
        return $phrase;
     }

    public static function getInboxThreads($user_id){
        //first send
        $threads = [];

        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => $user_id,
                'report_type' => 'user'
            ]
        ));
        //echo $deleteduser;exit;
        $inbox_messages = Message::find()
            ->With(['sender'=>function(ActiveQuery $query){
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['recipient'=>function(ActiveQuery $query){
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->where(['parent_id'=>0])
            //->andwhere(['NOT LIKE','delete_status',$deleteduser])
            ->andwhere(['OR',['reply_recipient_id' => $user_id],['reply_sender_id' => $user_id],['and',['recipient_id'=>$user_id],['reply_recipient_id' => 0 ]]])
            ->andwhere(['not in','recipient_id', $selectReported])
            ->orderBy('created_at DESC')
            //echo  $inbox_messages->createCommand()->getRawSql();exit;
            ->all();

        foreach($inbox_messages as $messages){
            $deleteusers1 = $messages->delete_status;
            $deleteusers = explode(',',$messages->delete_status);

            if($deleteusers1=='') {
                $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                $threadsmsg = [];
                if (isset($thread_messages) && !empty($thread_messages)) {
                    foreach ($thread_messages as $messages2) {
                        $threadsmsg[$messages2->m_id] = [
                            'm_id' => $messages2->m_id,
                            'text' => $messages2->text,
                            'created_at' => $messages2->created_at,
                            'send_by' => $messages2->sender_id,
                            'sender'  => ($messages2->sender_id==$messages->sender_id)?$messages->sender:$messages->recipient
                        ];
                    }
                }
                $other_id = $messages->sender_id;
                if ($messages->reply_recipient_id != 0) {
                    if ($messages->reply_recipient_id != $user_id)
                        $other_id = $messages->reply_recipient_id;
                    else if ($messages->reply_sender_id != $user_id)
                        $other_id = $messages->reply_sender_id;
                }

                $threads[$messages->m_id] = [
                    'm_id' => $messages->m_id,
                    'w_id'=>$messages->w_id,
                    'donation_id'=>$messages->donation_id,
                    'read_text' => ($messages->read_text == 0 && ($messages->reply_recipient_id == $user_id || $messages->reply_recipient_id == 0))?1:0,
                    'subject'=>$messages->subject,
                    'text' => $messages->text,
                    'created_at' => $messages->created_at,
                    'sender_id' => $messages->sender_id,
                    'recipient_id' => $other_id,
                    'reply_recipient_id' => $messages->reply_recipient_id,
                    'sender'=>$messages->sender,
                    'recipient'=>$messages->recipient
                ];

                if (isset($thread_messages) && !empty($thread_messages)) {
                    //$threads[$messages->m_id]['threads'] = $threadsmsg;
                }
            }else{
                if (!in_array($user_id, $deleteusers)) {
                    $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                    $threadsmsg = [];
                    if (isset($thread_messages) && !empty($thread_messages)) {
                        foreach ($thread_messages as $messages2) {
                            $threadsmsg[$messages2->m_id] = [
                                'm_id' => $messages2->m_id,
                                'text' => $messages2->text,
                                'created_at' => $messages2->created_at,
                                'send_by' => $messages2->sender_id,
                                'sender'  => ($messages2->sender_id==$messages->sender_id)?$messages->sender:$messages->recipient
                            ];
                        }
                    }
                    $other_id = $messages->sender_id;
                    if ($messages->reply_recipient_id != 0) {
                        if ($messages->reply_recipient_id != $user_id)
                            $other_id = $messages->reply_recipient_id;
                        else if ($messages->reply_sender_id != $user_id)
                            $other_id = $messages->reply_sender_id;
                    }

                    $threads[$messages->m_id] = [
                        'm_id' => $messages->m_id,
                        'w_id'=>$messages->w_id,
                        'donation_id'=>$messages->donation_id,
                        'subject'=>$messages->subject,
                        'read_text' => ($messages->read_text == 0 && ($messages->reply_recipient_id == $user_id || $messages->reply_recipient_id == 0))?1:0,
                        'text' => $messages->text,
                        'created_at' => $messages->created_at,
                        'sender_id' => $messages->sender_id,
                        'recipient_id' => $other_id,
                        'reply_recipient_id' => $messages->reply_recipient_id,
                        'sender'=>$messages->sender,
                        'recipient'=>$messages->recipient
                    ];

                    if (isset($thread_messages) && !empty($thread_messages)) {
                        //$threads[$messages->m_id]['threads'] = $threadsmsg;
                    }
                }


            }

        }
        return $threads;
    }
    public static function getChatMessages($parent_id,$user_id){
        //first send
        $threads = [];

        $selectReported = (ReportContent::find()->select('reported_user')->where(
            [
                'report_user' => $user_id,
                'report_type' => 'user'
            ]
        ));
        //echo $deleteduser;exit;
        $inbox_messages = Message::find()
            ->With(['sender'=>function(ActiveQuery $query){
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['recipient'=>function(ActiveQuery $query){
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->where(['parent_id'=>0])
            ->andWhere(['m_id'=>$parent_id])
            //->andwhere(['NOT LIKE','delete_status',$deleteduser])
            ->andwhere(['OR',['reply_recipient_id' => $user_id],['reply_sender_id' => $user_id],['and',['recipient_id'=>$user_id],['reply_recipient_id' => 0 ]]])
            ->andwhere(['not in','recipient_id', $selectReported])
            ->orderBy('created_at DESC')
            ->all();

        foreach($inbox_messages as $messages){
            $deleteusers1 = $messages->delete_status;
            $deleteusers = explode(',',$messages->delete_status);

            if($deleteusers1=='') {
                $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                $threadsmsg = [];
                if (isset($thread_messages) && !empty($thread_messages)) {
                    foreach ($thread_messages as $messages2) {
                        $threadsmsg[$messages2->m_id] = [
                            'm_id' => $messages2->m_id,
                            'text' => $messages2->text,
                            'created_at' => $messages2->created_at,
                            'send_by' => $messages2->sender_id,
                            'sender'  => ($messages2->sender_id==$messages->sender_id)?$messages->sender:$messages->recipient
                        ];
                    }
                }
                $other_id = $messages->sender_id;
                if ($messages->reply_recipient_id != 0) {
                    if ($messages->reply_recipient_id != $user_id)
                        $other_id = $messages->reply_recipient_id;
                    else if ($messages->reply_sender_id != $user_id)
                        $other_id = $messages->reply_sender_id;
                }

                $threads[$messages->m_id] = [
                    'm_id' => $messages->m_id,
                    'w_id'=>$messages->w_id,
                    'donation_id'=>$messages->donation_id,
                    'read_text' => $messages->read_text,
                    'subject'=>$messages->subject,
                    'text' => $messages->text,
                    'created_at' => $messages->created_at,
                    'sender_id' => $messages->sender_id,
                    'recipient_id' => $other_id,
                    'reply_recipient_id' => $messages->reply_recipient_id,
                    'sender'=>$messages->sender,
                    'recipient'=>$messages->recipient
                ];

                if (isset($thread_messages) && !empty($thread_messages)) {
                    $threads[$messages->m_id]['threads'] = $threadsmsg;
                }
            }else{
                if (!in_array($user_id, $deleteusers)) {
                    $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                    $threadsmsg = [];
                    if (isset($thread_messages) && !empty($thread_messages)) {
                        foreach ($thread_messages as $messages2) {
                            $threadsmsg[$messages2->m_id] = [
                                'm_id' => $messages2->m_id,
                                'text' => $messages2->text,
                                'created_at' => $messages2->created_at,
                                'send_by' => $messages2->sender_id,
                                'sender'  => ($messages2->sender_id==$messages->sender_id)?$messages->sender:$messages->recipient
                            ];
                        }
                    }
                    $other_id = $messages->sender_id;
                    if ($messages->reply_recipient_id != 0) {
                        if ($messages->reply_recipient_id != $user_id)
                            $other_id = $messages->reply_recipient_id;
                        else if ($messages->reply_sender_id != $user_id)
                            $other_id = $messages->reply_sender_id;
                    }

                    $threads[$messages->m_id] = [
                        'm_id' => $messages->m_id,
                        'w_id'=>$messages->w_id,
                        'donation_id'=>$messages->donation_id,
                        'subject'=>$messages->subject,
                        'read_text' => $messages->read_text,
                        'text' => $messages->text,
                        'created_at' => $messages->created_at,
                        'sender_id' => $messages->sender_id,
                        'recipient_id' => $other_id,
                        'reply_recipient_id' => $messages->reply_recipient_id,
                        'sender'=>$messages->sender,
                        'recipient'=>$messages->recipient
                    ];

                    if (isset($thread_messages) && !empty($thread_messages)) {
                        $threads[$messages->m_id]['threads'] = $threadsmsg;
                    }
                }


            }

        }
        return $threads;
    }
}

?>