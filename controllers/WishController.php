<?php

namespace app\controllers;

use app\models\Donation;
use app\models\HappyStories;
use app\models\Message;
use phpDocumentor\Reflection\Types\Expression;
use Yii;
use app\models\Wish;
use app\models\WishComments;
use app\models\Activity;
use app\models\ReportWishes;
use app\models\ReportContent;
use app\models\search\SearchWish;
use app\models\search\SearchDonation;
use app\models\search\SearchReportWishes;
use app\models\User;
use app\models\UserProfile;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use app\models\MailContent;
use app\controllers\Utils;

use app\models\Payment;
use app\models\WishCommentsActivities;
use yii\widgets\ActiveForm;

/**
 * WishController implements the CRUD actions for Wish model.
 */
class WishController extends Controller
{
	public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionSearch(){
        $searchModel = new SearchWish();
        //$cat_id = null;
        //$searchModel->wish_title = Yii::$app->request->queryParams['match'];
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);
        
        $searchModelDonation = new SearchDonation();
        $dataProviderDonations = $searchModelDonation->searchCustom(Yii::$app->request->queryParams);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        return $this->render('searched_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDonations' => $dataProviderDonations,
            'user'  =>  $user,
            'profile' => $profile
        ]);		
        
    }
    
    public function actionSearchScroll($page){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustom(Yii::$app->request->queryParams);

        $searchModelDonation = new SearchDonation();
        $dataProviderDonations = $searchModelDonation->searchCustom(Yii::$app->request->queryParams);

        $dataProvider->pagination->page = $page;
        $dataProviderDonations->pagination->page = $page;
        $str = '';
		
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }

        foreach($dataProviderDonations->models as $donation){
            $str .= $donation->donationhAsCard;
        }
        return $str;
        
    }
    
    public function actionSearchUser(){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchCustomUser(Yii::$app->request->queryParams);



        return $this->render('searched_user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);		
        
    }
    /**
     * Lists all Wish models.
     * @return mixed
     */
    public function actionIndex($cat_id=null)
    {


//        $allwishes = Donation::find()->all();
//        if(!empty($allwishes)){
//            foreach ($allwishes as $wish){
//                if($wish->granted_date!=''){
//                    $date = explode('-',$wish->granted_date);
//                    $wish->granted_date = ($wish->granted_date!='00-00-0000')?date('Y-m-d',strtotime($date[2].'-'.$date[0].'-'.$date[1])):NULL;
//                }
//                if($wish->process_granted_date!='') {
//                    $date1 = explode('-', $wish->process_granted_date);
//                    $wish->process_granted_date = ($wish->process_granted_date!='00-00-0000')?date('Y-m-d', strtotime($date1[2] . '-' . $date1[0] . '-' . $date1[1])):NULL;
//                }
//                $wish->save(false);
//            }
//        }

        $searchModel = new SearchWish();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }

        return $this->render('current_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cat_id' => $cat_id,
            'user'  =>  $user,
            'profile' => $profile,
            'type'=>'wish'
        ]);
    }

    public function actionIndexDonations($cat_id=null)
    {
        $searchModel = new SearchDonation();
        
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
        
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        
        // print_r($dataProvider);exit;
        return $this->render('current_donations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cat_id' => $cat_id,
            'user'  =>  $user,
            'profile' => $profile
        ]);
    }
    /**
     * Lists all Wish models when scrolls.
     * @return mixed
     */
    public function actionScroll($page,$cat_id=null)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$cat_id);
		$dataProvider->pagination->page = $page;
        $str = '';
		//if ($dataProvider->totalCount > 0) {
        foreach($dataProvider->models as $wish){
				$str .= $wish->wishAsCard;
        }
		//}
        return $str;
    }


    public function actionProgress(){
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchProgress(Yii::$app->request->queryParams);

        $wishes = array();

        if(!empty($dataProvider->models)){
            foreach ($dataProvider->models as $wishmodel){


                $wishes[] = $wishmodel;
            }
        }


        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }

        $donations = array();
        $searchModelDonations = new SearchDonation();
        $dataProviderDonations = $searchModelDonations->searchProgress(Yii::$app->request->queryParams);

        if(!empty($dataProviderDonations->models)){
            foreach ($dataProviderDonations->models as $donationmodel){


                $donations[] = $donationmodel;
            }
        }
        $data = array_merge($wishes,$donations);
        $price = array_column($data, 'process_granted_date');

        array_multisort($price, SORT_DESC, $data);

        return $this->render('progress_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDonations' => $dataProviderDonations,
            'user'  =>  $user,
            'profile' => $profile,
            'data'=>$data
        ]);
    }
    
    public function actionScrollProgress($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchProgress(Yii::$app->request->queryParams);
        $dataProvider->pagination->page = $page;
        $str = '';
        $searchModelDonations = new SearchDonation();
        $dataProviderDonations = $searchModelDonations->searchProgress(Yii::$app->request->queryParams);
        $dataProviderDonations->pagination->page = $page;
        $str = '';
        $wishes = array();
        if(!empty($dataProvider->models)) {
            foreach ($dataProvider->models as $wish) {

                $wishes[] = $wish;
                //$str .= $wish->wishAsCard;
            }
        }
        $donations = array();
        if(!empty($dataProviderDonations->models)){
            foreach ($dataProviderDonations->models as $donationmodel){


                $donations[] = $donationmodel;
            }
        }
        $data = array_merge($wishes,$donations);
        $price = array_column($data, 'process_granted_date');

        array_multisort($price, SORT_DESC, $data);
        if(!empty($data)){
            foreach ($data as $dt){
                if(isset($dt->w_id)){
                    $str .= $dt->wishAsCard;
                }else{
                    $str .= $dt->donationAsCard;

                }
            }
        }
        return $str;
        //return $str;
    }
    
    /**
     * Lists all Wish models according to the number of likes.
     * @return mixed
     */
    public function actionPopular()
    {
        $searchModel = new SearchWish();

        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
        $wishes = array();
        //echo "<pre>";print_r($dataProvider->models);exit;
        if(!empty($dataProvider->models)){
            foreach ($dataProvider->models as $wishmodel){
                $wishes[] = $wishmodel;


            }
        }
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }


        $donations = array();
        $searchModelDonations = new SearchDonation();
        $dataProviderDonations = $searchModelDonations->searchPopular(Yii::$app->request->queryParams);

        if(!empty($dataProviderDonations->models)){
            foreach ($dataProviderDonations->models as $donationmodel){

                $donations[] = $donationmodel;
            }
        }
        $data = array_merge($wishes,$donations);

        // print_r($dataProvider);
        // print_r($dataProviderDonations);

        return $this->render('popular_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDonations' => $dataProviderDonations,
            'user'  =>  $user,
            'profile'=> $profile,
            'data'=>$data
        ]);
		
    }

        
    public function actionReportContentPost(){
        $report = new ReportContent();
        $postData = Yii::$app->request->post();
        // print_r($postData);exit;
        $report->content_id = $postData['comment_id'];
        $report->report_user = \Yii::$app->user->id;
        $report->reported_user = $postData['reported_user'];
        $report->report_type= $postData['report_type'];
        $report->comment=$postData['comment'];
        $report->date = date('Y-m-d H:i:s');
        
        // print_r($report);exit;
        if ($report->save()){
            Utils::sendEmail(1,'',21);
            return true;
        } 
        return false;
    }

    public function actionScrollPopular($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchPopular(Yii::$app->request->queryParams);
		$dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }


        // $searchModelDonations = new SearchDonation();
        // $dataProviderDonations = $searchModelDonations->searchPopular(Yii::$app->request->queryParams); 
        // foreach($dataProviderDonations->models as $donation){
        //     $str .= $donation->donationAsCard;
        // }
        return $str;
    }
	public function actionGranted(){
        $searchModel = new SearchWish();
        //$dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        $wishes = array();

        if(!empty($dataProvider->models)){
            foreach ($dataProvider->models as $wishmodel){

                $wishes[] = $wishmodel;
            }
        }
        //echo "<pre>";print_r($wishes);exit;
       // exit;
        if( ! \Yii::$app->user->id)
        {
            $user   =   null;
            $profile    =   null;
        }
        else
        {
            $user = User::findOne(\Yii::$app->user->id);
            $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        }
        $donations = array();
        $searchModelDonations = new SearchDonation();
        $dataProviderDonations = $searchModelDonations->searchGranted(Yii::$app->request->queryParams);
        //echo "<pre>";print_r($dataProviderDonations->models);exit;

        if(!empty($dataProviderDonations->models)){
            foreach ($dataProviderDonations->models as $donationmodel){


                $donations[] = $donationmodel;
            }
        }
        $data = array_merge($wishes,$donations);
        $price = array_column($data, 'granted_date');

        array_multisort($price, SORT_DESC, $data);

        //echo "<pre>";print_r($data);exit;
        return $this->render('fullfilled_wishes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDonations' => $dataProviderDonations,
            'user'  =>  $user,
            'profile'   =>  $profile,
            'data'=>$data
        ]);		
	}
    public function actionScrollGranted($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchGranted(Yii::$app->request->queryParams);
        $dataProvider->pagination->page = $page;

        $searchModelDonations = new SearchDonation();
        $dataProviderDonations = $searchModelDonations->searchGranted(Yii::$app->request->queryParams);
        $dataProviderDonations->pagination->page = $page;
        $str = '';
        $wishes = array();
        if(!empty($dataProvider->models)) {
            foreach ($dataProvider->models as $wish) {

                $wishes[] = $wish;
                //$str .= $wish->wishAsCard;
            }
        }
        $donations = array();
        if(!empty($dataProviderDonations->models)){
            foreach ($dataProviderDonations->models as $donationmodel){

                $donations[] = $donationmodel;
            }
        }
        $data = array_merge($wishes,$donations);
        $price = array_column($data, 'granted_date');

        array_multisort($price, SORT_DESC, $data);
        if(!empty($data)){
            foreach ($data as $dt){
                if(isset($dt->w_id)){
                    $str .= $dt->wishAsCard;
                }else{
                    $str .= $dt->donationAsCard;

                }
            }
        }
        return $str;
    }
    /**
     * Displays a single Wish model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        //->createCommand()->getRawSql()
        $wish = $this->findModel($id);
        $loggedUserId = \Yii::$app->user->id;
        $listcomments = new WishComments();
        $comments = WishComments::find()->where(['w_id'=>$id,'parent_id'=>0,'status'=>0])->orderBy('w_comment_id Desc')->all();		 
        
        $isReported = Utils::checkIsReported($id, 'wish');
        return $this->render('view', [
            'model' => $wish, 'comments'=>$comments,'listcomments'=>$listcomments, "isReported" => $isReported
        ]);
    }
    
    public function actionWishComments()
    {
        $model = new WishComments();
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                $wish = $this->findModel($model->w_id);

                $get_user_id  = \app\models\Wish::findOne([
                    'w_id' => $model->w_id,
                ]);

                $wish_creater = \app\models\UserProfile::findOne([
                    'user_id' => $get_user_id->wished_by,
                ]);
                $comment_creater = \app\models\UserProfile::findOne([
                    'user_id' => \Yii::$app->user->id,
                ]);

                if($wish->wished_by != \Yii::$app->user->id)
                    $this->sendCommentEmail($wish, $model->comments);


                $user1url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile','id'=>\Yii::$app->user->id]);
                $user1 = Html::a($comment_creater->firstname, $user1url,['style'=>'color:blue;text-decoration: underline;']);


                $wishurl = Yii::$app->urlManager->createAbsoluteUrl(['wish/view','id'=>$model->w_id]);
                $wishlink = Html::a($get_user_id->wish_title, $wishurl,['style'=>'color:blue;text-decoration: underline;']);

                $details ="Hello ".$wish_creater->firstname.",
                
                You’ve received a public comment on ".$wishlink." from ".$user1."
                
                ".$model->comments."";

                $msg = $details;
                if(\Yii::$app->user->id != '' && $get_user_id->wished_by != '' && $msg != '') {
                    $message = new Message();
                    $message->sender_id = \Yii::$app->user->id;
                    $message->recipient_id = $get_user_id->wished_by;
                    $message->parent_id = 0;
                    $message->read_text = 0;
                    $message->text = $msg;
                    $message->created_at = date("Y-m-d H:i:s");
                    if ($message->save()) {
                        Yii::$app->session->setFlash('messageSent');
                    }
                }


                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
        }
    }
    
    public function actionCommentLikesView($w_id)
    {
        $comment = WishComments::find()->where(['w_comment_id'=>$w_id])->one();
        $likedUser  =   [];
        if( ! empty($comment->likes))
        {
            foreach($comment->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();

                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }

        return json_encode($likedUser);
    }

    public function actionLikeComment($s_id)
    {
        if(\Yii::$app->user->isGuest)
            return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

        $comment = WishComments::find()->where(['w_comment_id'=>$s_id])->one();
        $activity = WishCommentsActivities::find()->where(['w_comment_id'=>$comment->w_comment_id,'user_id'=>\Yii::$app->user->id])->one();
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        $activity = new WishCommentsActivities();
        $activity->w_comment_id = $comment->w_comment_id;
        $activity->user_id = \Yii::$app->user->id;

        if($activity->save())
            return "added";
        else return false;
    }
    
    public function actionCommentreply()
    {		
        $model = new WishComments();
        if($model->load(Yii::$app->request->post()))
        {				
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            $model->user_id = \Yii::$app->user->id;
            if($model->save())
            {
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            } 
        }
    }
    
    public function actionUpdateComment($id)
    {
        $model = WishComments::findOne($id);
        if (!isset($model)){
            return;
        }
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['wish/view?id='.$model->w_id]);
            }
        }
    }
    
    public function actionDeleteComment($id)
    {
        $model  =   WishComments::findOne($id);
        $model->status  =   1;
        
        if($model->parent_id == 0)
        {
            $replycomments = WishComments::find()->where(['parent_id'=>$id])->all();
            
            foreach($replycomments as $reply)
            {
                $reply->status  =   1;
                $reply->save();
            }
        }
        
        $model->save();
        return $this->redirect(['wish/view?id='.$model->w_id]);
    }

    /**
     * Creates a new Wish model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $user = User::findOne(Yii::$app->user->id);
        $properties = Utils::getUserProperties(Yii::$app->user->id);
        if (!$properties['can_create_wish']){
            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
        }

        $model = new Wish();

        $model->scenario = 'create';	

        
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
     //    echo "<pre>";print_r(Yii::$app->request->post());exit;
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        if ($model->load(Yii::$app->request->post()))
        {

            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }

            if(Yii::$app->request->post()['Wish']['primary_image']!=''){
                $primary_image = Yii::$app->request->post()['Wish']['primary_image'];
            }


            $model->wished_by = \Yii::$app->user->id;
            $model->wish_status = 0;
            $model->created_at = date('Y-m-d H:i:s');
            if(!empty($model->auto_id))
            {
                $model1 = Wish::findOne($model->auto_id);
                $model1->w_id = $model->auto_id;
                $model1->wished_by = \Yii::$app->user->id;
                $model1->wish_title = $model->wish_title;
                $model1->wish_description = $model->wish_description;
                $model1->primary_image = $model->primary_image;
                $model1->expected_cost = $model->expected_cost;
                $model1->way_of_wish = $model->way_of_wish;
                $model1->description_of_way = $model->description_of_way;
                $model1->financial_assistance = $model->financial_assistance;
                $model1->financial_assistance_other = $model->financial_assistance_other;
                $model1->expected_date = $model->expected_date;
                $model1->who_can = $model->who_can;
                $model1->non_pay_option = $model->non_pay_option;
                $model1->wish_status = $model->wish_status;				
                $model1->show_mail_status = $model->show_mail_status;
                $model1->show_mail = $model->show_mail;
                $model1->show_person_status = $model->show_person_status;
                $model1->show_person_street = $model->show_person_street;
                $model1->show_person_city = $model->show_person_city;
                $model1->show_person_state = $model->show_person_state;
                $model1->show_person_zip = $model->show_person_zip;
                $model1->show_person_country = $model->show_person_country;
                $model1->show_other_status = $model->show_other_status;
                $model1->show_other_specify = $model->show_other_specify;
                $model1->i_agree_decide = $model->i_agree_decide;
                $model1->i_agree_decide2 = $model->i_agree_decide2;
                $model1->process_status = $model->process_status;
                $model1->process_granted_by = $model->process_granted_by;
                $model1->process_granted_date= $model->process_granted_date;			

                $model1->update(false);
            }
            else
            {
                $model->primary_image = null;
                $model->created_at = date('Y-m-d H:i:s');
                $model->save();
            }
            
            if(!empty(Yii::$app->request->post()['Wish']['primary_image']) && strpos(Yii::$app->request->post()['Wish']['primary_image'], 'base64') !== false)
            {
                $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents('web/uploads/wishes/' . $model->w_id . '.' . 'jpg', $data);

                $model->primary_image = 'uploads/wishes/' . $model->w_id . '.jpg';

            }
            else
            {
                if($model->primary_image_name!='') {
                    $model->primary_image = $model->primary_image_name;
                }else{
                    $model->primary_image = 'images/wish_default/2.jpg';///web/images/wish_default/2.jpg
                }
            }

            $model->save();
            $model->sendCreateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['account/my-account']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Updates an existing Wish model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->primary_image;

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post())) {
            //check for a new image
            $model->wished_by = \Yii::$app->user->id;
            $model->save();

            if(!empty(Yii::$app->request->post()['Wish']['primary_image_name']))
            {
                if(empty($model->primary_image_name))
                {
                    $model->primary_image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->primary_image_name) && ($model->primary_image_name != $current_image ))
                {
                        $model->primary_image = $model->primary_image_name;
                } else {
                        $model->primary_image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Wish']['primary_image'])
                {
                    $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                    if (strpos($data, 'base64') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents('web/uploads/wishes/' . $model->w_id . '.' . 'jpg', $data);

                        $model->primary_image = 'uploads/wishes/' . $model->w_id . '.jpg';
                    }
                }
                else
                {
                    if(! empty($model->primary_image_name))
                        $model->primary_image = $model->primary_image_name;
                    else
                        $model->primary_image = $current_image;
                }
            }
            
            //save model
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['view', 'id' => $model->w_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'user' => $user ,
                'profile' => $profile
            ]);
        }
    }

    /**
     * Deletes an existing Wish model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $happystories = HappyStories::find()->where(['wish_id'=>$id])->one();
        $happystories->delete();
         $this->findModel($id)->delete();
        return $this->redirect(['index']); 
    }

	
	public function actionAjaxDelete()
    {
		$id = Yii::$app->request->post('id');
		$this->findModel($id)->delete();
    }
	
	    /**
     * Like a wish
     * User has to be logged in to like a wish
     * Param: wish id
     * @return boolean
     */
    public function actionReportWish()
    {
        echo "report";
    }
	
    /**
     * Like a wish
     * User has to be logged in to like a wish
     * Param: wish id
     * @return boolean
     */
    public function actionLike($w_id,$type)
    {
        // echo "muie like";exit;
        if(\Yii::$app->user->isGuest) {
            return $this->redirect(['site/login', 'red_url' => Yii::$app->request->referrer]);
        }
        $wish = $this->findModel($w_id);
        $activity = Activity::find()->where(['wish_id'=>$wish->w_id,'activity'=>$type,'user_id'=>\Yii::$app->user->id])->one();
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        $activity = new Activity();
        $activity->wish_id = $wish->w_id;
        $activity->activity = $type;
        $activity->user_id = \Yii::$app->user->id;
        if($activity->save())
        {
            $this->sendLikedEmail($wish);
            return "added";
        }
        else 
            return false;
    }

    public function actionFullfilled($w_id){
        $wish = $this->findModel($w_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.

        $wish->granted_by = \Yii::$app->user->id;
        $wish->granted_date = date('Y-m-d H:i:s');

        if($wish->save(false))
        {			
            $this->sendEmail($wish);		
            return $this->redirect(['wish/view','id'=>$w_id]);
        }
    }
    /**
     * Finds the Wish model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Wish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Wish::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionTopWishers(){
        $query = Wish::find()->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $query->groupBy('wished_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
        
        return $this->render('iWish', [
            'dataProvider' => $dataProvider,
        ]);		
    }
	
	public function actionTopGranters(){
		$query = Wish::find()->select(['wishes.granted_by,count(w_id) as total_wishes'])->where(['not', ['granted_by' => null]])->orderBy('total_wishes DESC');
		$query->groupBy('granted_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
		return $this->render('iGrant', [
			'dataProvider' => $dataProvider,
		]);		
	}

    public function actionTopDonors(){
        $query =  Donation::find()->select(['donations.created_by,count(id) as total_donations'])->orderBy('total_donations DESC');
        $query->groupBy('created_by');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize'=>50
            ]
        ]);
        return $this->render('iDonor', [
            'dataProvider' => $dataProvider,
        ]);
    }
	
	/**
	 * IPN listener for paypal
	 * Ref: http://stackoverflow.com/questions/14015144/sample-php-code-to-integrate-paypal-ipn
	 * Change the status back to not paid if not veified
	 */
	public function actionVerifyGranted(){
		// STEP 1: Read POST data

		// reading posted data from directly from $_POST causes serialization 
		// issues with array data in POST
		// reading raw POST data from input stream instead. 
		$raw_post_data = file_get_contents('php://input');
			$fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $raw_post_data);
			  fclose($fh);
		$raw_post_array = explode('&', $raw_post_data);
		$myPost = array();
		foreach ($raw_post_array as $keyval) {
		  $keyval = explode ('=', $keyval);
		  if (count($keyval) == 2)
			 $myPost[$keyval[0]] = urldecode($keyval[1]);
		}
		// read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if(function_exists('get_magic_quotes_gpc')) {
		   $get_magic_quotes_exists = true;
		} 
		foreach ($myPost as $key => $value) {        
		   if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) { 
				$value = urlencode(stripslashes($value)); 
		   } else {
				$value = urlencode($value);
		   }
		   $req .= "&$key=$value";
		}


		// STEP 2: Post IPN data back to paypal to validate

		//https://www.sandbox.paypal.com/cgi-bin/webscr
		//$ch = curl_init('https://www.sandbox.paypal.com/cgi-bin/webscr');
		$ch = curl_init('https://ipnpb.paypal.com/cgi-bin/webscr');

		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

		// In wamp like environments that do not come bundled with root authority certificates,
		// please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path 
		// of the certificate as shown below.
		// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
		if( !($res = curl_exec($ch)) ) {
			// error_log("Got " . curl_error($ch) . " when processing IPN data");
			curl_close($ch);
			exit;
		}
		curl_close($ch);


		// STEP 3: Inspect IPN validation result and act accordingly

		if (strcmp ($res, "VERIFIED") == 0) {
			// check whether the payment_status is Completed
			// check that txn_id has not been previously processed
			// check that receiver_email is your Primary PayPal email
			// check that payment_amount/payment_currency are correct
			// process payment
			$payment = new Payment();
			// assign posted variables to local variables
			$payment->item_name = $_POST['item_name'];
			$payment->item_number = $_POST['item_number'];
			$payment->payment_status = $_POST['payment_status'];
			$payment->payment_amount = $_POST['mc_gross'];
			$payment->payment_currency = $_POST['mc_currency'];
			$payment->txn_id = $_POST['txn_id'];
			$payment->receiver_email = $_POST['receiver_email'];
			$payment->payer_email = $_POST['payer_email'];
			$payment->payment_date = $_POST['payment_date'];
			$payment->save();
			//check if success
			$wish = $this->findModel($_POST['item_number']);
			//if not fully paid or if not successful, revert the granted status
			if($payment->payment_status != "Completed"){
				$wish->granted_by = NULL;
				$wish->save();
			}

		} else if (strcmp ($res, "INVALID") == 0) {
			  // Save the output (to append or create file)
			  $fh = fopen(Yii::$app->basePath."/web/uploads/paypal_log.txt", "a");
			  fwrite($fh, $res);
			  fclose($fh);
			// log for manual investigation
		}		
	}
	
	public function actionRemoveWish($wish_id)
	{
		if(\Yii::$app->user->isGuest)
			return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
		$wish = $this->findModel($wish_id);
		$activity = Activity::find()->where(['wish_id'=>$wish->w_id,'activity'=>'fav','user_id'=>\Yii::$app->user->id])->one();
		if($activity != null){
			$activity->delete();
		}
		
		 return $this->redirect(['account/my-saved']);
	}
	
    public function sendEmail($wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>5])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
        
        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->process_granted_by,
        ]);
		
        if (!$user) {
            return false;
        }
        if (!$user2) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'grantedSuccess-html'],
                ['user' => $user, 'user2'=>$user2, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendLikedEmail($wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>15])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
        
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
		
        if (!$user || !$likedUser || $user->id === $likedUser->id) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishLike-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish, 'likedUser' =>  $likedUser]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendCommentEmail($wish, $comment)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>16])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);
        
        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);
        
        if (!$user || !$likedUser) {
            return false;
        }
        $email_subject = str_replace("##USERNAME2##", $likedUser->username,$mailcontent->mail_subject);
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishComment-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish, 'likedUser' =>  $likedUser, 'comment' => $comment]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($email_subject);
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
    
    public function sendEmailToGranter($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>14])->one();
        
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);
		
        if (!$user) {
            return false;
        }
        
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);			
        
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        
        return $message->send();
    }


    /* It’s Been 10 Days Since Your Wish Was Accepted */
    public function actionTendayswishinprogress(){
        //where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])
        $wishes = Wish::find()->where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])->all();
        $wishesdata = array();
        foreach($wishes as $singlewish=>$value) {
            $now = time(); // or your date as well
            $your_date = strtotime($value->granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day == 10) {


                //$mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();


                /* @var $user User */
                $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'id' => $value->wished_by,
                ]);
                $user2 = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $value->granted_by,
                ]);

                if (!empty($user)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 36])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject = $mailcontent->mail_subject;
                    if (empty($subject))
                        $subject = 'SimplyWishes ';
                    $user_email = $user->email;
                    $message = Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourwishaccepted-html'],
                            ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'wish' => $value]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email)
                        ->setSubject($subject);
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message->send();

                }
                if(!empty($user2)){
                    $mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();
                    $editmessage1 = $mailcontent1->mail_message;
                    $subject1 = $mailcontent1->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user_email1 = $user2->email;
                    $message1 = Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourwishaccepted-html'],
                            ['user' => $user2, 'user2' => $user, 'editmessage' => $editmessage1, 'wish' => $value]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message1->send();
                }
                //  return $message->send();
            }
        }
        //echo "<pre>";print_r($wishesdata);exit;
    }
    public function actionTendaysdonationinprogress(){
        //where(['status'=>0,'process_status'=>1,'granted_by'=> null])
        $wishes = Donation::find()->where(['status'=>0,'process_status'=>1,'granted_by'=> null])->all();
        $wishesdata = array();
        foreach($wishes as $singlewish=>$value) {
            $now = time(); // or your date as well
            $your_date = strtotime($value->granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));
            if ($day == 10) {


                //$mailcontent1 = MailContent::find()->where(['m_id' => 37])->one();


                /* @var $user User */
                $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'id' => $value->created_by,
                ]);
                $user2 = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $value->granted_by,
                ]);

                if (!empty($user)) {
                    $mailcontent = MailContent::find()->where(['m_id' => 38])->one();
                    $editmessage = $mailcontent->mail_message;
                    $subject = $mailcontent->mail_subject;
                    if (empty($subject))
                        $subject = 'SimplyWishes ';
                    $user_email = $user->email;
                    $message = Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourdonationaccepted-html'],
                            ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'wish' => $value]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email)
                        ->setSubject($subject);
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message->send();

                }
                if(!empty($user2)){
                    $mailcontent1 = MailContent::find()->where(['m_id' => 39])->one();
                    $editmessage1 = $mailcontent1->mail_message;
                    $subject1 = $mailcontent1->mail_subject;
                    if (empty($subject1))
                        $subject1 = 'SimplyWishes ';
                    $user_email1 = $user2->email;
                    $message1 = Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => '10dayofyourdonationaccepted-html'],
                            ['user' => $user2, 'user2' => $user, 'editmessage' => $editmessage1, 'wish' => $value]
                        )
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes'])
                        ->setTo($user_email1)
                        ->setSubject($subject1);
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                    $message1->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                    $message1->send();
                }
                //  return $message->send();
            }
        }
        //echo "<pre>";print_r($wishesdata);exit;
    }


    public function sendEmailToWisher($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>33])->one();

        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccesstowisher-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
	
    public function actionLikesView($w_id)
    {
        $wish = $this->findModel($w_id);
        $likedUser  =   [];
        if( ! empty($wish->likes))
        {
            foreach($wish->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();
                
                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }
        
        return json_encode($likedUser);
    }
    
    //toaddreport
    public function actionReportContent($w_id, $content_type, $content_id='', $report_user='', $reported_user='', $comment ='')
    {
        if(\Yii::$app->user->isGuest){
                return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
        }

                
        $content_type_arr=array("article","article_comment","wish","wish_comment","message","user");
        
        if ($content_type == "wish"){
            $this->actionReport($w_id);
            $wish = $this->findModel($w_id);

            $activity = new ReportContent();
            // $activity->id = 43;
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = $comment;
            $activity->content_id = $w_id;
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            if ($activity->save()){
                Utils::sendEmail(1,'',21);
                echo "saved";
            } else {
                echo "not saved";
            }
        // left explicit else for clarity    
        } else if ($content_type == "user" || $content_type == "wish_comment" || $content_type == "donation" || $content_type == "donation_comment"|| $content_type == "message"){
            $activity = new ReportContent();
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = $comment;
            $activity->content_id = $w_id;
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            // print_r($activity);
            // exit;
            if ($activity->save()){
                Utils::sendEmail(1,'',21);
                echo "saved";
            } else {
                echo "not saved"; 
            }
        }
        return "ok";
    }

    public function actionReport($w_id)
    {
        if(\Yii::$app->user->isGuest)
                return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

        $wish = $this->findModel($w_id);
        $activity = ReportWishes::find()->where(['w_id'=>$wish->w_id])->one();
        if($activity){
                $activity->count = $activity->count + 1;
                $activity->save();
                return "added";			
        } else {
            $activity = new ReportWishes();
            $activity->w_id = $wish->w_id;
            $activity->count = 1;	
            $activity->save();
                    return "added";
        }
    }	
	
	public function actionReportAction()
	{
	 if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
		$searchModel = new SearchReportWishes();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('report_view', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
	 } else {
		 return $this->redirect(['site/index']);
	 }
	}
	
	
	public function actionReportActionView($id)
    {
		if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$wish = Wish::find()->where(['w_id'=>$id])->one();		
			if($wish)
			{			
				return $this->render('report_full_view', ['model' => $this->findModel($id) ]);
			} else {
					return $this->redirect('report-action');
			}	
		} else {
				 return $this->redirect(['site/index']);
			 }		
    }

	
	public function actionReportDelete()
    {
	  if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin')){ 
			$id = Yii::$app->request->post('id');
			$this->findModel($id)->delete();
			$model = ReportWishes::find()->where(['w_id'=>$id])->one();
			$model->delete();
		 } else {
		 return $this->redirect(['site/index']);
	  }
    }
	
    public function actionUploadFile(){
        if( ! empty(Yii::$app->request->post()['image']) && ! empty(Yii::$app->request->post()['id']))
        {
            $data   =   Yii::$app->request->post()['image'];

            list($type, $data) = explode(';', $data);
            list(, $data)      = explode(',', $data);
            $data = base64_decode($data);

            file_put_contents('web/uploads/wishes/'.Yii::$app->request->post()['id']. '.' .'jpg', $data);

            return 'uploads/wishes/'.Yii::$app->request->post()['id'].'.jpg';
        }
   }
	
    public function actionWishAutosave()
    {
        if(Yii::$app->request->post())
        {
            $models2 = Yii::$app->request->post();

            if( ! $models2['Wish']['w_id'])
            {
                $models = new Wish();
                $models->wished_by = \Yii::$app->user->id;
                $models->wish_status = 1;
                $models->primary_image = null;
                $models->save(false);
                $models2['Wish']['w_id'] =   $models->w_id;
            }
            
            $models =  Wish::find()->where(['w_id'=>$models2['Wish']['w_id']])->one();
            
            //$models->category = $models2['Wish']['category'];
            $models->wish_title = $models2['Wish']['wish_title'];		
            $models->wish_description = $models2['Wish']['wish_description'];

            /*if(empty($models2['Wish']['primary_image']) && empty($models2['Wish']['primary_image_name']))
                $models->primary_image = 'images/wish_default/1.jpg';
            else if( ! empty($models2['Wish']['primary_image_name']))
                $models->primary_image = $models2['Wish']['primary_image_name'];
            else
                $models->primary_image = $models2['Wish']['primary_image'];*/

            if(!empty(Yii::$app->request->post()['Wish']['primary_image']) && strpos(Yii::$app->request->post()['Wish']['primary_image'], 'base64') !== false)
            {
                $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                list($type, $data) = explode(';', $data);
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                file_put_contents('web/uploads/wishes/' . $models->w_id . '.' . 'jpg', $data);

                $models->primary_image = 'uploads/wishes/' . $models->w_id . '.jpg';

            }
            else
            {
                if($models2['Wish']['primary_image_name'] != '') {
                    $models->primary_image = $models2['Wish']['primary_image_name'];
                }else{
                    $models->primary_image = 'images/wish_default/1.jpg';
                }
            }


            $models->expected_cost = $models2['Wish']['expected_cost'];
            $models->expected_date = $models2['Wish']['expected_date'];
            //$models->who_can = $models2['Wish']['who_can'];
            $models->non_pay_option = $models2['Wish']['non_pay_option'];

           // $models->show_mail_status = $models2['Wish']['show_mail_status'];
            $models->show_mail = $models2['Wish']['show_mail'];
            $models->financial_assistance = $models2['Wish']['financial_assistance'];
            $models->financial_assistance_other = (isset($models2['Wish']['financial_assistance_other']))?$models2['Wish']['financial_assistance_other']:'';
            $models->way_of_wish = (isset($models2['Wish']['way_of_wish']))?$models2['Wish']['way_of_wish']:'';
            $models->description_of_way = (isset($models2['Wish']['description_of_way']))?$models2['Wish']['description_of_way']:'';
           // $models->show_person_status = $models2['Wish']['show_person_status'];
           // $models->show_person_street = $models2['Wish']['show_person_street'];
            //$models->show_person_city = $models2['Wish']['show_person_city'];
           /// $models->show_person_state = $models2['Wish']['show_person_state'];
            //$models->show_person_zip = $models2['Wish']['show_person_zip'];
           // $models->show_person_country = $models2['Wish']['show_person_country'];
           // $models->show_other_status = $models2['Wish']['show_other_status'];
            //$models->show_other_specify = $models2['Wish']['show_other_specify'];
            $models->wish_status = 1;
            
            if($models->save(false))
            {
                return $this->redirect(['my-drafts']);
            }
            else
            {				
                echo "failed2";
            }

        }	
    }
	
	
		/**
     * Lists all Editorial models.
     * @return mixed
     */
    public function actionMyDrafts()
    {
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchDrafts(Yii::$app->request->queryParams);

        return $this->render('my_drafts', [
            'user' => $user,
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	
    public function actionDeleteDraft($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['my-drafts']); 
    }
    
    public function actionDeleteWish($id)
    {
        $happystories = HappyStories::find()->where(['wish_id'=>$id])->one();
        if(!empty($happystories)){
            $happystories->delete();
        }
        $this->findModel($id)->delete();
//        echo '<script>
//                window.history.back();
//
//              </script>';
        return $this->redirect(['account/my-account']);
    }
	
    public function actionViewDraft($id)
    {
        return $this->render('view_draft', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
    public function actionUpdateDraft($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'updatedraft';
        //$categories =  ArrayHelper::map(Category::find()->all(), 'cat_id', 'title');
        //$countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(),'id','name');	
        //$states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id'=>$model->country ])->all(),'id','name');	
        //$cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id'=>$model->state])->all(),'id','name');	
        $current_image = $model->primary_image;

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post()) ) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
            //check for a new image
            $model->wished_by = \Yii::$app->user->id;
            $model->save();

            if(!empty(Yii::$app->request->post()['Wish']['primary_image_name']))
            {
                if(empty($model->primary_image_name))
                {
                    $model->primary_image =   'images/wish_default/1.jpg';
                }
                else if(!empty($model->primary_image_name) && ($model->primary_image_name != $current_image ))
                {
                    $model->primary_image = $model->primary_image_name;
                } else {
                    $model->primary_image = $current_image;
                }
            }
            else
            {
                if($current_image !== Yii::$app->request->post()['Wish']['primary_image'])
                {
                    $data   =   Yii::$app->request->post()['Wish']['primary_image'];
                    if (strpos($data, 'base64') !== false) {
                        list($type, $data) = explode(';', $data);
                        list(, $data) = explode(',', $data);
                        $data = base64_decode($data);

                        file_put_contents('web/uploads/wishes/' . $model->w_id . '.' . 'jpg', $data);

                        $model->primary_image = 'uploads/wishes/' . $model->w_id . '.jpg';
                    }
                }
                else
                {
                    if(! empty($model->primary_image_name))
                        $model->primary_image = $model->primary_image_name;
                    else
                        $model->primary_image = $current_image;
                }
            }
            $model->created_at = date('Y-m-d h:i:s');
            $model->wish_status = 0;
            $model->save();
            $model->sendUpdateSuccessEmail($model, \Yii::$app->user->id);
            return $this->redirect(['account/my-account']);
        } else {
            return $this->render('update_draft', [
                'model' => $model,
                'user' => $user ,
                'profile' => $profile
            ]);
        }
		
    }
    
    public function actionProcessWish()
    {
        $w_id = \Yii::$app->request->post()['wish_id'];
        $processstatus = \Yii::$app->request->post()['processstatus'];
        $send_message = \Yii::$app->request->post()['send_message'];

        $wish = $this->findModel($w_id);
        //explicitly set up the granted_by to the user id
        //listen to the IPN and change back to NULL if not success.
        $wish->process_granted_by = \Yii::$app->user->id;
        $wish->process_granted_date = date('Y-m-d H:i:s');
        $wish->process_status = $processstatus;

        if($wish->save(false))
        {
            if($send_message == 1)
                if ($wish->non_pay_option == 0){
                      $result = $wish->sendGrantWishFinancialEmail($wish->process_granted_by,$wish);
                }else{
                    $result = $wish->sendGrantWishNonFinancialEmail($wish->process_granted_by,$wish);
                }
            $res    =   $this->sendEmail($wish);
        }
    }
		
	 public function actionResubmitProcessWish()
    {
		
        $w_id = \Yii::$app->request->post()['wish_id'];
		$userid = \Yii::$app->request->post()['userid'];
		
	if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin'))		
		$wish =  Wish::find()->where(['w_id'=>$w_id])->one();
	else
		$wish =  Wish::find()->where(['w_id'=>$w_id,'wished_by'=>$userid])->one();

		if($wish)
		{
			$wish->process_granted_by = "";
			//$wish->process_granted_date = NULL;
			$wish->process_status = 0;
			$wish->email_status = 0;

			if($wish->save(false))
			{		
				echo "Success";
			}
		}
		
    }
	
    public function actionGrantProcessWish()
    {
        $w_id = \Yii::$app->request->post()['wish_id'];
        $userid = \Yii::$app->request->post()['userid'];
        
        if(isset(\Yii::$app->user->identity->role) && (\Yii::$app->user->identity->role == 'admin'))		
            $wish =  Wish::find()->where(['w_id'=>$w_id])->one();
        else
            $wish =  Wish::find()->where(['w_id'=>$w_id,'wished_by'=>$userid])->one();

        if($wish)
        {
            $wish->granted_by = $wish->process_granted_by;
            $wish->granted_date = date('Y-m-d H:i:s');
            $wish->process_granted_by = "";
            //$wish->process_granted_date = "00-00-0000";
            $wish->process_status = 0;
            
            if($wish->save(false))
            {
                $this->sendEmailToGranter($wish->wished_by, $wish);
                $this->sendEmailToWisher($wish->granted_by, $wish);
                //return $this->redirect(['wish/view','id'=>$w_id]);
                $model = Wish::findOne($w_id);

                $resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $model->w_id]);
                $profilelink1 = Html::a($model->wish_title, $resetLink1,['style'=>'color: blue;text-decoration: underline;']);

                $happystories = Yii::$app->urlManager->createAbsoluteUrl(['happy-stories/index']);
                $url = Html::a('Happy Stories', $happystories,['style'=>'color: blue;text-decoration: underline;']);
                $details = "Congratulations! Your wish ,".$profilelink1." has been fulfilled and is now considered granted.
                            Consider sharing your story on our ".$url." .";
                $msg = $details;

                if($userid != '' && $model->granted_by != '' && $msg != ''){
                    $message = new Message();
                    $message->sender_id =  $model->granted_by;
                    $message->recipient_id =  $userid;
                    $message->parent_id = 0;
                    $message->read_text = 0;
                    $message->text = $msg;
                    $message->created_at = date("Y-m-d H:i:s");
                    if($message->save()){
                        Yii::$app->session->setFlash('messageSent');
                    }
                }

            }
        }	
    }

    public function actionTest()
    {

        $wishes = Wish::find()->where(['wish_status'=>0,'process_status'=>1,'granted_by'=> null])->all();

        foreach($wishes as $singlewish=>$value)
        {
            $now = time(); // or your date as well
            $your_date =  strtotime($value->process_granted_date);
            $datediff = $now - $your_date;
            $day = round($datediff / (60 * 60 * 24));

            if ($day == 14){
                $value->granted_by = $value->process_granted_by;
                $value->granted_date = date('Y-m-d H:i:s');
                $value->process_granted_by = "";
                //$wish->process_granted_date = "00-00-0000";
                $value->process_status = 0;

                if($value->save(false))
                {
                    $this->sendEmailToGranter($value->granted_by, $value);
                    //return $this->redirect(['wish/view','id'=>$w_id]);
                }
            }
        }
    }

	 public function actionMultiDeleteWishes()
	 {    

			$w_id = Yii::$app->request->post()['w_id'];	
			if($w_id)
			{
				 foreach($w_id as $tmp)
				 {
                     $happystories = HappyStories::find()->where(['wish_id'=>$tmp])->one();
                     if(!empty($happystories)){
                         $happystories->delete();
                     }
                     //$happystories->delete();
					$this->findModel($tmp)->delete();
				 }
			}  			
	}

    public function actionSendmessage($id){
        $wishmodel = Wish::findOne($id);
        $from = \Yii::$app->user->id;
        $to = $wishmodel->wished_by;
        $msg = $wishmodel->wish_title;

        if($from != '' && $to != '' && $msg != ''){
            $message = new Message();
            $message->w_id = $id;
            $message->sender_id = $from;
            $message->recipient_id = $to;
            $message->parent_id = 0;
            $message->read_text = 0;
            $message->subject = $msg;
            $message->created_at =  date("Y-m-d H:i:s");

            if($message->save()){
                $mailcontent = MailContent::find()->where(['m_id'=>30])->one();
                $editmessage = $mailcontent->mail_message;

                if(empty($subject))
                    $subject = 	'SimplyWishes ';
                $user = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $from,
                ]);

                $user2 = User::findOne([
                    //'status' => User::STATUS_ACTIVE,
                    'id' => $to,
                ]);

               $email_subject = str_replace("##USERNAME2##", $user->username,$mailcontent->mail_subject);
                //$subject = $mailcontent->mail_subject;
                $mail = Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'inboxmessage'],
                        ['user' => $user, 'user2'=>$user2, 'editmessage' => $editmessage,'message'=>$message]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                    ->setTo($user2->email)
                    ->setSubject($email_subject);

                $mail->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                $mail->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
               $send = $mail->send();
                return $this->redirect(['wish/index']);
            }
        }
    }
	
}
