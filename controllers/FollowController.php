<?php

namespace app\controllers;
use app\models\search\SearchDonation;
use Yii;
use app\models\FollowRequest;
use app\models\Message;
use app\models\User;
use app\models\UserProfile;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MailContent;
use app\models\search\SearchWish;

class FollowController extends \yii\web\Controller
{
    public function actionFollowRequest()
    {		
        $model = new FollowRequest();
        $from = \Yii::$app->request->post()['send_from'];
        $to = \Yii::$app->request->post()['send_to'];

        if($from != '' && $to != '' ){
                $checkdata = FollowRequest::find()->where(["requested_by"=>$from,"requested_to"=>$to])->one();				
                if($checkdata)
                {				
                        $checkdata->delete();
                        echo "unfollow";
                }	
                else
                {
                        $request = new FollowRequest();
                        $request->requested_by = $from;
                        $request->requested_to = $to ;			
                        $request->save();
                        $this->sendEmail($from, $to);
                        echo "follow";				
                }	
        }
    }

    public function actionCancelFollow()
    {	
		
	  $requestid = \Yii::$app->request->post()['requestid'];
	  if($requestid)
	  {
		$user_id = \Yii::$app->user->id;
		$checkdata = FollowRequest::find()->where(["requested_to"=>$requestid ,"requested_by" =>$user_id ])->one();	
		if($checkdata)
		{
			$checkdata->delete();
			return true;
		}			
		
	  }	
	}
        
    public function sendEmail($from, $to)
    {
        $mailcontent = MailContent::find()->where(['m_id'=>17])->one();
        $editmessage = $mailcontent->mail_message;		
        $subject = $mailcontent->mail_subject;
        if(empty($subject))
            $subject = 	'SimplyWishes ';
		
        /* @var $user User */
        $userFrom = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $from,
        ]);
        
        $userTo = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $to,
        ]);
		
        if (!$userFrom || !$userTo) {
            return false;
        }
      
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'followRequest-html'],
                ['from' => $userFrom, 'editmessage' => $editmessage, 'to' => $userTo ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($userTo->email)
            ->setSubject($subject);			
            
        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }
	
    public function actionProgress(){
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        $searchModelwish = new SearchWish();
        $dataProviderwish = $searchModelwish->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'inprogress');

        $searchModeldonation = new SearchDonation();
        $dataProviderdonation = $searchModeldonation->searchFriendProcessDonations(Yii::$app->request->queryParams,\Yii::$app->user->id,'inprogress');
        
        return $this->render('progress', [
            'user' => $user,
            'profile' => $profile,
            'searchModelwish' => $searchModelwish,
            'dataProviderwish' => $dataProviderwish,
            'searchModeldonation' => $searchModeldonation,
            'dataProviderdonation' => $dataProviderdonation
        ]);		
    }
    
    public function actionFulfilled(){
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        $searchModelwish = new SearchWish();
        $dataProviderwish = $searchModelwish->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'fullfilled');

        $searchModeldonation = new SearchDonation();
        $dataProviderdonation = $searchModeldonation->searchFriendProcessDonations(Yii::$app->request->queryParams,\Yii::$app->user->id,'fullfilled');

        return $this->render('fulfilled', [
            'user' => $user,
            'profile' => $profile,
            'searchModelwish' => $searchModelwish,
            'dataProviderwish' => $dataProviderwish,
            'searchModeldonation' => $searchModeldonation,
            'dataProviderdonation' => $dataProviderdonation
        ]);		
    }
    
    public function actionActive(){

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        $searchModelwish = new SearchWish();
        $dataProviderwish = $searchModelwish->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'active');

        $searchModeldonation = new SearchDonation();

        $dataProviderdonation = $searchModeldonation->searchFriendProcessDonations(Yii::$app->request->queryParams,\Yii::$app->user->id,'active');

        return $this->render('active', [
            'user' => $user,
            'profile' => $profile,
            'searchModelwish' => $searchModelwish,
            'dataProviderwish' => $dataProviderwish,
            'searchModeldonation' => $searchModeldonation,
            'dataProviderdonation' => $dataProviderdonation

        ]);		
    }
    
    public function actionScrollActive($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'active');
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }
        return $str;
    }
    
    public function actionScrollFulfilled($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'fullfilled');
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }
        return $str;
    }
    
    public function actionScrollProgress($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchFriendProcessWishes(Yii::$app->request->queryParams,\Yii::$app->user->id,'inprogress');
        $dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $wish){
            $str .= $wish->wishAsCard;
        }
        return $str;
    }
}
