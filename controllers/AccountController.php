<?php

namespace app\controllers;

use app\models\MailContent;
use yii\base\BaseObject;
use yii\db\ActiveQuery;
use yii\helpers\Html;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\User;
use app\models\UserProfile;
use app\models\search\SearchWish;
use app\models\search\SearchDonation;
use app\models\Message;
use app\models\Wish;
use app\models\Donation;
use app\models\FriendRequest;
use app\models\FollowRequest;
use app\models\ReportContent;


class AccountController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['edit-account', 'my-account', 'inbox', 'inboxmessage'],
                'rules' => [
                    [
                        'actions' => ['edit-account', 'my-account', 'inbox', 'inboxmessage'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if ($action->id == 'chat-users') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    public function actionProfile($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['site/login']);
        }

        if ($id == \Yii::$app->user->id)
            return $this->redirect('my-account');
        $user = User::findOne($id);
        $profile = UserProfile::find()->where(['user_id' => $id])->one();

        $isReported = Utils::checkIsReported($id, 'wish');

        if (empty($profile)) {
            return $this->redirect('my-account');
        } else {

            $searchModel = new SearchWish();
            $dataProvider = $searchModel->searchUserWishes(Yii::$app->request->queryParams, $id, 'active');

            $searchModel1 = new SearchDonation();

            // echo \Yii::$app->user->id;
            $dataProvider1 = $searchModel1->searchUserDonations(Yii::$app->request->queryParams, $id, 'active');

            $isUserReportedByMe = Utils::isUserReportedBy($id, \Yii::$app->user->id);

            return $this->render('other_profile', [
                'user' => $user,
                'profile' => $profile,
                'isUserReportedByMe' => $isUserReportedByMe,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProvider1' => $dataProvider1
            ]);
        }
    }
    public function actionMyAccount()
    {
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();

        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchUserWishes(Yii::$app->request->queryParams, \Yii::$app->user->id, 'active');

        $searchModelDonations = new SearchDonation();
        $dataProviderDonation = $searchModelDonations->searchUserDonations(Yii::$app->request->queryParams, \Yii::$app->user->id, 'active');

        return $this->render('profile', [
            'user' => $user,
            'profile' => $profile,
            // 'properties' => $properties,
            'searchModel' => $searchModel,
            'searchModelDonations' => $searchModelDonations,
            'dataProvider' => $dataProvider,
            'dataProviderDonation' => $dataProviderDonation,
        ]);
    }
    public function actionAddUsers()
    {

        $users = User::find()->joinWith(['userProfile'])->select(['user.id', 'user_profile.firstname', 'user_profile.lastname', 'user_profile.profile_image'])->where(['!=', 'id', 1])->limit(1)->asArray()->all();
        $usersArray = array();
        $userArr = array();
        foreach ($users as $user) {
            $sendBirdUserId = vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4)) . $user['id'];
            $userArray = array();
            $userArrs = array();
            $userArray['user_id'] = $sendBirdUserId;
            $userArray['nickname'] = $user['firstname'] . " " . $user['lastname'];
            $userArray['profile_url'] = \yii\helpers\Url::base('https') . '/web/uploads/users/' . $user['profile_image'];
            $usersArray[] = $userArray;
            $userArrs['sendbird_user_id'] = $sendBirdUserId;
            $userArrs['id'] = $user['id'];
            $userArr[] = $userArrs;
        }

        $chatUsers = json_encode($usersArray);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $chatUsers,
            CURLOPT_HTTPHEADER => array(
                'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
                'Content-Type: application/json',
                'Accept: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        foreach ($userArr as $userm) {
            $user = User::findOne($userm['id']);
            $user->sendbird_user_id = $userm['sendbird_user_id'];
            $user->save(false);
        }
        echo $response;
        exit;
    }

    public function actionMyFullfilled($id = null)
    {
        if (!$id)
            $id = \Yii::$app->user->id;

        $user = User::findOne($id);
        $profile = UserProfile::find()->where(['user_id' => $id])->one();

        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchUserWishes(Yii::$app->request->queryParams, $id, 'fullfilled');

        $dataProvider2 = $searchModel->searchUserWishes(Yii::$app->request->queryParams, $id, 'fullfilledGrant');

        $searchModelDonations = new SearchDonation();
        $dataProviderDonation = $searchModelDonations->searchUserDonations(Yii::$app->request->queryParams, \Yii::$app->user->id, 'fullfilled');

        $dataProviderDonationGrants = $searchModelDonations->searchUserDonations(Yii::$app->request->queryParams, \Yii::$app->user->id, 'fullfilledGrant');
        $isUserReportedByMe = Utils::isUserReportedBy($id, \Yii::$app->user->id);
        if ($id == \Yii::$app->user->id) {
            return $this->render('my_fullfilled', [
                'user' => $user,
                'profile' => $profile,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProvider2' => $dataProvider2,
                'dataProviderDonation' => $dataProviderDonation,
                'dataProviderDonationGrants' => $dataProviderDonationGrants,
                'isUserReportedByMe' => $isUserReportedByMe,

            ]);
        } else {
            // echo "<pre>";print_r($dataProviderDonation->getModels());exit;
            return $this->render('other_fullfilled', [
                'user' => $user,
                'profile' => $profile,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'dataProviderDonation' => $dataProviderDonation,
                'isUserReportedByMe' => $isUserReportedByMe,
            ]);
        }
    }

    public function actionMySaved($id = null)
    {
        if (!$id)
            $id = \Yii::$app->user->id;
        $user = User::findOne($id);
        $profile = UserProfile::find()->where(['user_id' => $id])->one();

        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchSavedWishes(Yii::$app->request->queryParams, \Yii::$app->user->id);

        $searchModelDonations = new SearchDonation();
        $dataProviderDonation = $searchModelDonations->searchSavedDonations(Yii::$app->request->queryParams, \Yii::$app->user->id);

        return $this->render('my_saved', [
            'user' => $user,
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProviderDonation' => $dataProviderDonation,
        ]);
    }

    public function actionMyProgress()
    {

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();

        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchProcessWishes(Yii::$app->request->queryParams, \Yii::$app->user->id, 'active');
        $dataProvider2 = $searchModel->searchProcessWishes(Yii::$app->request->queryParams, \Yii::$app->user->id, 'inprogressGrant');

        $searchModelDonations = new SearchDonation();
        $dataProviderDonation = $searchModelDonations->searchProcessDonations(Yii::$app->request->queryParams, \Yii::$app->user->id, 'active');
        $dataProviderDonationGrants = $searchModelDonations->searchProcessDonations(Yii::$app->request->queryParams, \Yii::$app->user->id, 'inprogressGrant');


        return $this->render('my_progress', [
            'user' => $user,
            'profile' => $profile,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider2' => $dataProvider2,
            'dataProviderDonation' => $dataProviderDonation,
            'dataProviderDonationGrants' => $dataProviderDonationGrants,
        ]);
    }

    public function actionScrollMyActive($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchUserWishes(Yii::$app->request->queryParams, \Yii::$app->user->id, 'active');

        $dataProvider->pagination->page = $page;
        $str = '';
        foreach ($dataProvider->models as $wish) {
            $str .= $wish->wishAsCard;
        }
        return $str;
    }

    public function actionScrollMySaved($page)
    {
        $searchModel = new SearchWish();
        $dataProvider = $searchModel->searchSavedWishes(Yii::$app->request->queryParams, \Yii::$app->user->id);

        $dataProvider->pagination->page = $page;
        $str = '';
        foreach ($dataProvider->models as $wish) {
            $str .= $wish->wishAsCard;
        }
        return $str;
    }

    public function actionEditAccount()
    {
        $id = \Yii::$app->user->id;
        $user = User::findOne(\Yii::$app->user->id);

        $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
        // echo $profile->createCommand()->getRawSql();

        // echo \Yii::$app->user->id;exit;
        // print_r($profile);exit;
        $countries = \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all(), 'id', 'name');
        // print_r($countries);exit;

        $states = \yii\helpers\ArrayHelper::map(\app\models\State::find()->where(['country_id' => $profile->country])->all(), 'id', 'name');
        $cities = \yii\helpers\ArrayHelper::map(\app\models\City::find()->where(['state_id' => $profile->state])->all(), 'id', 'name');
        // die("1");
        $user->scenario = 'updatecheck';
        $current_image = $profile->profile_image;

        // die("2");
        if (($user->load(Yii::$app->request->post())  && $user->validate()) && $profile->load(Yii::$app->request->post())) {
            //echo "<pre>";print_r($_POST);exit;


            if (isset(Yii::$app->request->post()['delete-button'])) {
                // echo " delete";
                // exit;
                if ($user->delete()) {
                    Yii::$app->user->logout();

                    return $this->goHome();
                }
            } else {
                // echo "not delete";

                if ($user->password) {
                    $user->setPassword($user->password);
                }

                $user->updated_at = date("Y-m-d H:i:s");
                if ($user->save()) {
                    $profile->user_id = $user->id;
                    $profile->save_id       =   1;
                    /*echo Yii::$app->request->post()['UserProfile']['profile_image'];*/
                    /*if (empty(Yii::$app->request->post()['UserProfile']['profile_image'])) {
                                if (empty($profile->dulpicate_image)) {
                                    $profile->profile_image =   'images/img1.jpg';
                                } elseif (!empty($profile->dulpicate_image) && $profile->dulpicate_image != $current_image) {
                                    $profile->profile_image = $profile->dulpicate_image;
                                } else {
                                    $profile->profile_image = $current_image;
                                }
                            } else {*/
                    /*echo $current_image; $profile->profile_image;*/ ?><!--<br>--><?php /*echo Yii::$app->request->post()['UserProfile']['profile_image'];exit;*/
                                                                                    /*if ($current_image != Yii::$app->request->post()['UserProfile']['profile_image'] && empty($profile->dulpicate_image)) {
                                $data   =   Yii::$app->request->post()['UserProfile']['profile_image'];

                                if (strpos($data, 'base64') !== false) {
                                    list($type, $data) = explode(';', $data);
                                    list(, $data) = explode(',', $data);
                                    $data = base64_decode($data);
                                    $filename = 'web/uploads/users/' . $profile->user_id . '.jpg';
                                    file_put_contents($filename, $data);
                                    chmod($filename, 0664);
                                    $profile->profile_image = $profile->user_id . '.jpg';
                                    $profile->save_id = 0;
                                }else {
                                    $profile->profile_image = $current_image;
                                }
                            } else {
                                if (! empty($profile->dulpicate_image)) {
                                    $profile->profile_image = $profile->dulpicate_image;
                                } else {
                                    $profile->profile_image = $current_image;
                                }
                            }
                            }*/
                                                                                    if (!empty(Yii::$app->request->post()['UserProfile']['profile_image'])) {
                                                                                        /*echo $current_image; $profile->profile_image;*/ ?><!--<br>--><?php /*echo Yii::$app->request->post()['UserProfile']['profile_image'];exit;*/
                                                                                                                                                                if ($current_image != Yii::$app->request->post()['UserProfile']['profile_image'] && empty($profile->dulpicate_image)) {
                                                                                                                                                                    $data   =   Yii::$app->request->post()['UserProfile']['profile_image'];
                                                                                                                                                                    $unique = rand(10, 500) . time();
                                                                                                                                                                    if (strpos($data, 'base64') !== false) {
                                                                                                                                                                        list($type, $data) = explode(';', $data);
                                                                                                                                                                        list(, $data) = explode(',', $data);
                                                                                                                                                                        $data = base64_decode($data);
                                                                                                                                                                        $filename = 'web/uploads/users/' . $unique . '.jpg';
                                                                                                                                                                        file_put_contents($filename, $data);
                                                                                                                                                                        chmod($filename, 0664);
                                                                                                                                                                        $profile->profile_image = $unique . '.jpg';
                                                                                                                                                                        $profile->save_id = 0;
                                                                                                                                                                        $profileUrl = \yii\helpers\Url::base('https') . '/web/uploads/users/' . $unique . '.jpg';
                                                                                                                                                                        $curl = curl_init();
                                                                                                                                                                        $profileData['profile_url'] = $profileUrl;
                                                                                                                                                                        $sendbirdProfile = json_encode($profileData);

                                                                                                                                                                        curl_setopt_array($curl, array(
                                                                                                                                                                            CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users/' . $user->sendbird_user_id,
                                                                                                                                                                            CURLOPT_RETURNTRANSFER => true,
                                                                                                                                                                            CURLOPT_ENCODING => '',
                                                                                                                                                                            CURLOPT_MAXREDIRS => 10,
                                                                                                                                                                            CURLOPT_TIMEOUT => 0,
                                                                                                                                                                            CURLOPT_FOLLOWLOCATION => true,
                                                                                                                                                                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                                                                                                                                                            CURLOPT_CUSTOMREQUEST => 'PUT',
                                                                                                                                                                            CURLOPT_POSTFIELDS => $sendbirdProfile,
                                                                                                                                                                            CURLOPT_HTTPHEADER => array(
                                                                                                                                                                                'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
                                                                                                                                                                                'Content-Type: application/json',
                                                                                                                                                                                'Accept: application/json'
                                                                                                                                                                            ),
                                                                                                                                                                        ));

                                                                                                                                                                        $response = curl_exec($curl);

                                                                                                                                                                        curl_close($curl);
                                                                                                                                                                    } else {
                                                                                                                                                                        $profile->profile_image = $current_image;
                                                                                                                                                                    }
                                                                                                                                                                } else {
                                                                                                                                                                    if (! empty($profile->dulpicate_image)) {
                                                                                                                                                                        $profile->profile_image = $profile->dulpicate_image;
                                                                                                                                                                        $profileUrl = \yii\helpers\Url::base('https') . '/web/uploads/users/' . $profile->dulpicate_image;
                                                                                                                                                                        $curl = curl_init();
                                                                                                                                                                        $profileData['profile_url'] = $profileUrl;
                                                                                                                                                                        $sendbirdProfile = json_encode($profileData);

                                                                                                                                                                        curl_setopt_array($curl, array(
                                                                                                                                                                            CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users/' . $user->sendbird_user_id,
                                                                                                                                                                            CURLOPT_RETURNTRANSFER => true,
                                                                                                                                                                            CURLOPT_ENCODING => '',
                                                                                                                                                                            CURLOPT_MAXREDIRS => 10,
                                                                                                                                                                            CURLOPT_TIMEOUT => 0,
                                                                                                                                                                            CURLOPT_FOLLOWLOCATION => true,
                                                                                                                                                                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                                                                                                                                                                            CURLOPT_CUSTOMREQUEST => 'PUT',
                                                                                                                                                                            CURLOPT_POSTFIELDS => $sendbirdProfile,
                                                                                                                                                                            CURLOPT_HTTPHEADER => array(
                                                                                                                                                                                'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
                                                                                                                                                                                'Content-Type: application/json',
                                                                                                                                                                                'Accept: application/json'
                                                                                                                                                                            ),
                                                                                                                                                                        ));

                                                                                                                                                                        $response = curl_exec($curl);
                                                                                                                                                                    } else {
                                                                                                                                                                        $profile->profile_image = $current_image;
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }

                                                                                                                                                            $profile->save();
                                                                                                                                                            // $profile->sendProfileEmail(\Yii::$app->user->id);
                                                                                                                                                            /* \Yii::$app->getSession()->setFlash('success', 'Account details have been changed successfully');
                        return $this->redirect('my-account'); */

                                                                                                                                                            //\Yii::$app->getSession()->setFlash('success', 'Account details have been changed successfully');

                                                                                                                                                            return $this->render('my_account', [
                                                                                                                                                                'user' => $user,
                                                                                                                                                                'profile' => $profile,
                                                                                                                                                                'countries' => $countries,
                                                                                                                                                                'states' => $states,
                                                                                                                                                                'cities' => $cities
                                                                                                                                                            ]);
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                } else {

                                                                                                                                                    // die("3");
                                                                                                                                                    return $this->render('my_account', [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'countries' => $countries,
                                                                                                                                                        'states' => $states,
                                                                                                                                                        'cities' => $cities
                                                                                                                                                    ]);
                                                                                                                                                }
                                                                                                                                            }

                                                                                                                                            public function actionSendMessage()
                                                                                                                                            {

                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $msg = \Yii::$app->request->post()['msg'];
                                                                                                                                                $subject = \Yii::$app->request->post()['subject'];

                                                                                                                                                if ($from != '' && $to != '' && $msg != '') {
                                                                                                                                                    $message = new Message();
                                                                                                                                                    $message->sender_id = $from;
                                                                                                                                                    $message->recipient_id = $to;
                                                                                                                                                    $message->parent_id = 0;
                                                                                                                                                    $message->subject = $subject;
                                                                                                                                                    $message->text = $msg;
                                                                                                                                                    $message->created_at = date("Y-m-d H:i:s");
                                                                                                                                                    if ($message->save()) {
                                                                                                                                                        $mailcontent = MailContent::find()->where(['m_id' => 30])->one();
                                                                                                                                                        $editmessage = $mailcontent->mail_message;
                                                                                                                                                        $mailsubject = $mailcontent->mail_subject;
                                                                                                                                                        if (empty($mailsubject))
                                                                                                                                                            $subject =     'SimplyWishes ';
                                                                                                                                                        $user = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $from,
                                                                                                                                                        ]);

                                                                                                                                                        $user2 = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $to,
                                                                                                                                                        ]);

                                                                                                                                                        $email_subject = str_replace("##USERNAME2##", $user->username, $mailcontent->mail_subject);
                                                                                                                                                        $subject = $mailcontent->mail_subject;
                                                                                                                                                        $message = Yii::$app
                                                                                                                                                            ->mailer
                                                                                                                                                            ->compose(
                                                                                                                                                                ['html' => 'inboxmessage'],
                                                                                                                                                                ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'message' => $message]
                                                                                                                                                            )
                                                                                                                                                            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                                                                                                                                                            ->setTo($user2->email)
                                                                                                                                                            ->setSubject($email_subject);

                                                                                                                                                        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                                                                                                                                                        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                                                                                                                                                        $message->send();
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                    }
                                                                                                                                                    return $this->redirect(['profile', 'id' => $to]);
                                                                                                                                                }
                                                                                                                                            }


                                                                                                                                            public function actionSendMessageWishes()
                                                                                                                                            {

                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $msg = \Yii::$app->request->post()['msg'];

                                                                                                                                                if ($from != '' && $to != '' && $msg != '') {
                                                                                                                                                    $message = new Message();
                                                                                                                                                    $message->sender_id = $from;
                                                                                                                                                    $message->recipient_id = $to;
                                                                                                                                                    $message->parent_id = 0;
                                                                                                                                                    $message->text = $msg;
                                                                                                                                                    $message->created_at = date("Y-m-d H:i:s");
                                                                                                                                                    if ($message->save()) {
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }

                                                                                                                                            public function actionSendMessageWishesContactDetails()
                                                                                                                                            {

                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $wish_id = \Yii::$app->request->post()['wish_id'];
                                                                                                                                                $username1 = \app\models\User::findOne([
                                                                                                                                                    'id' => $to,
                                                                                                                                                ]);
                                                                                                                                                $username2 = \app\models\UserProfile::findOne([
                                                                                                                                                    'user_id' => $to,
                                                                                                                                                ]);
                                                                                                                                                $model = Wish::findOne($wish_id);
                                                                                                                                                $otheruserprofile = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $to]);
                                                                                                                                                $resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $model->w_id]);
                                                                                                                                                $profilelink1 = Html::a($model->wish_title, $resetLink1, ['style' => 'color: blue;text-decoration: underline;']);
                                                                                                                                                $profilelink = Html::a(Html::encode('sending a message'), $otheruserprofile, ['style' => 'color: blue;text-decoration: underline;']);
                                                                                                                                                $url = Html::a(Html::encode(ucfirst($username2->firstname)), $otheruserprofile);
                                                                                                                                                if ($model->non_pay_option == 0) {
                                                                                                                                                    $details = "Thank you for accepting to grant the wish," . $model->wish_title . ",The following information has been provided by " . $url . " :
                Date they would like their wish to be granted by: " . $model->expected_date . "
                Method of receiving financial assistance: " . $model->financial_assistance . "
                Email or username associated with account:" . $username1->email . "
                Amount requested (USD): " . $model->expected_cost . "
                You will have 14 days to fulfill this wish before it’s consider granted. If you fulfill this wish sooner than 14 days, " . $url . " may mark it as fulfilled. For more information, please contact " . ucfirst($username2->firstname) . "  by " . $profilelink . "  to their SimplyWishes inbox.
                Remember that any financial transaction arranged between you and the person who is granting your wish must happen outside of our website.";
                                                                                                                                                    if (($model->show_mail_status == 1) && ($model->show_person_status == 1)) {
                                                                                                                                                        $details .= '<p>or</p>';
                                                                                                                                                    }
                                                                                                                                                    if ($model->show_person_status == 1) {
                                                                                                                                                        $details .= '<p>In Person at this location :  <b>' . $model->show_person_street . ' ' . $model->show_person_city . ' ' . $model->show_person_state . ' ' . $model->show_person_zip . ' ' . $model->show_person_country . '</b></p>';
                                                                                                                                                    }
                                                                                                                                                    if ((($model->show_mail_status == 1) || ($model->show_person_status == 1)) && ($model->show_other_status == 1)) {
                                                                                                                                                        $details .= '<p>or</p>';
                                                                                                                                                    }
                                                                                                                                                    if ($model->show_other_status == 1) {
                                                                                                                                                        $details .= '<p>Other : <b>' . $model->show_other_specify . '</b></p>';
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    $details = "Thank you for accepting to grant the wish," . $model->wish_title . ",The following information has been provided by " . $url . " :
                Date they would like their wish to be granted by: " . $model->expected_date . "
                How they would you like to receive this Wish: " . $model->way_of_wish . "
                You will have 14 days to fulfill this wish before it’s consider granted. If you fulfill this wish sooner than 14 days, " . $url . " may mark it as fulfilled. For more information, please contact " . ucfirst($username2->firstname) . "  by " . $profilelink . "  to their SimplyWishes inbox.
                Remember that any financial transaction arranged between you and the person who is granting your wish must happen outside of our website.";
                                                                                                                                                    if (($model->show_mail_status == 1) && ($model->show_person_status == 1)) {
                                                                                                                                                        $details .= '<p>or</p>';
                                                                                                                                                    }
                                                                                                                                                    if ($model->show_person_status == 1) {
                                                                                                                                                        $details .= '<p>In Person at this location :  <b>' . $model->show_person_street . ' ' . $model->show_person_city . ' ' . $model->show_person_state . ' ' . $model->show_person_zip . ' ' . $model->show_person_country . '</b></p>';
                                                                                                                                                    }
                                                                                                                                                    if ((($model->show_mail_status == 1) || ($model->show_person_status == 1)) && ($model->show_other_status == 1)) {
                                                                                                                                                        $details .= '<p>or</p>';
                                                                                                                                                    }
                                                                                                                                                    if ($model->show_other_status == 1) {
                                                                                                                                                        $details .= '<p>Other : <b>' . $model->show_other_specify . '</b></p>';
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                $details1 = "I have accepted your wish," . $profilelink1 . ",to grant within the next 14 days. Please send me a message if are any changes to your wish, such as delivery preference or contact information.";
                                                                                                                                                if (($model->show_mail_status == 1) && ($model->show_person_status == 1)) {
                                                                                                                                                    $details1 .= '<p>or</p>';
                                                                                                                                                }
                                                                                                                                                if ($model->show_person_status == 1) {
                                                                                                                                                    $details1 .= '<p>In Person at this location :  <b>' . $model->show_person_street . ' ' . $model->show_person_city . ' ' . $model->show_person_state . ' ' . $model->show_person_zip . ' ' . $model->show_person_country . '</b></p>';
                                                                                                                                                }
                                                                                                                                                if ((($model->show_mail_status == 1) || ($model->show_person_status == 1)) && ($model->show_other_status == 1)) {
                                                                                                                                                    $details1 .= '<p>or</p>';
                                                                                                                                                }
                                                                                                                                                if ($model->show_other_status == 1) {
                                                                                                                                                    $details1 .= '<p>Other : <b>' . $model->show_other_specify . '</b></p>';
                                                                                                                                                }

                                                                                                                                                $msg = $details;
                                                                                                                                                $msg1 = $details1;

                                                                                                                                                if ($from != '' && $to != '' && $msg != '') {
                                                                                                                                                    $message = new Message();
                                                                                                                                                    $message->sender_id = $to;
                                                                                                                                                    $message->recipient_id = $from;
                                                                                                                                                    $message->parent_id = 0;
                                                                                                                                                    $message->read_text = 0;
                                                                                                                                                    $message->text = $msg;
                                                                                                                                                    $message->created_at = date("Y-m-d H:i:s");
                                                                                                                                                    if ($message->save()) {
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                    }

                                                                                                                                                    $message1 = new Message();
                                                                                                                                                    $message1->sender_id =  $from;
                                                                                                                                                    $message1->recipient_id = $to;
                                                                                                                                                    $message1->parent_id = 0;
                                                                                                                                                    $message1->read_text = 0;
                                                                                                                                                    $message1->text = $msg1;
                                                                                                                                                    $message1->created_at = date("Y-m-d H:i:s");
                                                                                                                                                    if ($message1->save()) {
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }


                                                                                                                                            public function actionSendMessageDonationsContactDetails()
                                                                                                                                            {
                                                                                                                                                /*print_r(\Yii::$app->request->post());
        die("send message");*/
                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $donation_id = \Yii::$app->request->post()['donation_id'];
                                                                                                                                                // echo "1";
                                                                                                                                                // echo $to;
                                                                                                                                                $user = User::find()->where(['id' => $to])->one();
                                                                                                                                                // print_r($user);
                                                                                                                                                // exit;
                                                                                                                                                $username2 = \app\models\UserProfile::findOne([
                                                                                                                                                    'user_id' => $from,
                                                                                                                                                ]);
                                                                                                                                                $username1 = \app\models\UserProfile::findOne([
                                                                                                                                                    'user_id' => $to,
                                                                                                                                                ]);
                                                                                                                                                $email = $user->email;
                                                                                                                                                // echo "2";
                                                                                                                                                $model = Donation::findOne($donation_id);
                                                                                                                                                // echo "3";
                                                                                                                                                $details = 'I will contact you on the below details. Please specify if there is any change.';

                                                                                                                                                $details .= '<p>Mail to this address : <b>' . $email . '</b></p>';
                                                                                                                                                //              } 
                                                                                                                                                //             if(($model->show_mail_status == 1) && ($model->show_person_status == 1)){ 
                                                                                                                                                //     $details .='<p>or</p>';
                                                                                                                                                //              }	
                                                                                                                                                //             if($model->show_person_status == 1){ 
                                                                                                                                                // $details .='<p>In Person at this location :  <b>'.$model->show_person_street.' '.$model->show_person_city.' '.$model->show_person_state.' '.$model->show_person_zip.' '.$model->show_person_country.'</b></p>';
                                                                                                                                                //      } 	
                                                                                                                                                //              if((($model->show_mail_status == 1) || ($model->show_person_status == 1))&& ($model->show_other_status == 1)){ 
                                                                                                                                                //             $details .='<p>or</p>';
                                                                                                                                                //              } 	
                                                                                                                                                //              if($model->show_other_status == 1){ 
                                                                                                                                                //     $details .='<p>Other : <b>'.$model->show_other_specify.'</b></p>';
                                                                                                                                                //              } 

                                                                                                                                                $user1url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $from]);
                                                                                                                                                $user1 = Html::a($username2->firstname, $user1url, ['style' => 'color:blue;text-decoration: underline;']);
                                                                                                                                                $msglink1 = Html::a('sending a message', $user1url, ['style' => 'color:blue;text-decoration: underline;']);

                                                                                                                                                $user2url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $to]);
                                                                                                                                                $user2 = Html::a($username1->firstname, $user2url, ['style' => 'color:blue;text-decoration: underline;']);
                                                                                                                                                $msglink = Html::a('sending a message', $user2url, ['style' => 'color:blue;text-decoration: underline;']);

                                                                                                                                                $resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $model->id]);
                                                                                                                                                $donation_link = Html::a($model->title, $resetLink1, ['style' => 'color:blue;text-decoration: underline;']);

                                                                                                                                                $complatedonationurl = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $donation_id]);
                                                                                                                                                $complatedonationlink = Html::a('complete', $complatedonationurl, ['style' => 'color:blue;text-decoration: underline;']);

                                                                                                                                                $postagainurl = Yii::$app->urlManager->createAbsoluteUrl((['donation/create']));
                                                                                                                                                $postagainlink = Html::a('post it again', $postagainurl, ['style' => 'color:blue;text-decoration: underline;']);


                                                                                                                                                if ($model->non_pay_option == 0) {
                                                                                                                                                    $details1 = "You have accepted the donation," . $donation_link . ",The following information has been provided by " . $user2 . " :
                Method of giving financial assistance: " . $model->financial_assistance . "
                Amount being donated (USD): " . $model->expected_cost . "
                The donor will have 14 days to fulfill this donation before it’s consider granted. You can mark this donation as completed any time before then. For more information, please contact " . $username1->firstname . " directly by " . $msglink . " to their SimplyWishes inbox.
                Remember that any financial transaction arranged between you and the person who is giving the donation must happen outside of our website.";
                                                                                                                                                } else {
                                                                                                                                                    $details1 = "You have accepted the donation, " . $donation_link . ",The following information has been provided by " . $user2 . " :
                Preferred method of delivery:" . $model->way_of_donation;
                                                                                                                                                    $details1 .= "<br><i>*We recommend to always meet in a public location and not to go alone.</i><br>";

                                                                                                                                                    $details1 .= "The donor will have 14 days to fulfill this donation before it’s consider granted. You can mark this donation as completed any time before then. For more information, please contact " . $username1->firstname . " directly by " . $msglink . " to their SimplyWishes inbox. 
                Remember that any financial transaction arranged between you and the person who is giving the donation must happen outside of our website.";
                                                                                                                                                }

                                                                                                                                                $details3 = "Congratulations, your donation, " . $donation_link . ", has been accepted by " . $user1 . "
        You have 14 days to fulfill this donation before it’s consider granted. If your donation is granted sooner than 14 days, you may mark it as " . $complatedonationlink . ", however, this donation may no longer be updated or deleted. And, for whatever reason, 
        if you cannot fulfill this donation, you can " . $postagainlink . "! 
        Contact " . $username2->firstname . " directly for more details by " . $msglink1 . " to their SimplyWishes inbox.
        Finally, remember that any financial transaction arranged between you and the person who is granting your wish must happen outside of our website.";

                                                                                                                                                $msg = $details;
                                                                                                                                                $msg1 = $details1;
                                                                                                                                                $msg3 = $details3;

                                                                                                                                                // die($msg);
                                                                                                                                                /*if($from != '' && $to != '' && $msg != ''){
            $message = new Message();
            $message->sender_id = $from;
            $message->recipient_id = $to;
            $message->parent_id = 0;
            $message->read_text = 0;
            $message->text = $msg;
            $message->created_at = date("Y-m-d H:i:s");
            if($message->save()){
                    Yii::$app->session->setFlash('messageSent');
            }*/


                                                                                                                                                $message1 = new Message();
                                                                                                                                                $message1->sender_id = $to;
                                                                                                                                                $message1->recipient_id = $from;
                                                                                                                                                $message1->parent_id = 0;
                                                                                                                                                $message1->read_text = 0;
                                                                                                                                                $message1->text = $msg1;
                                                                                                                                                $message1->created_at = date("Y-m-d H:i:s");
                                                                                                                                                if ($message1->save()) {
                                                                                                                                                    //Yii::$app->session->setFlash('messageSent');
                                                                                                                                                }

                                                                                                                                                $message = new Message();
                                                                                                                                                $message->sender_id = $from;
                                                                                                                                                $message->recipient_id = $to;
                                                                                                                                                $message->parent_id = 0;
                                                                                                                                                $message->read_text = 0;
                                                                                                                                                $message->text = $msg3;
                                                                                                                                                $message->created_at = date("Y-m-d H:i:s");
                                                                                                                                                if ($message->save()) {
                                                                                                                                                    // Yii::$app->session->setFlash('messageSent');
                                                                                                                                                }
                                                                                                                                                /*}*/
                                                                                                                                            }

                                                                                                                                            public function actionMyFriend($findfriends = "", $type = "")
                                                                                                                                            {

                                                                                                                                                $user = User::findOne(\Yii::$app->user->id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                                                                                                $followlist = [];
                                                                                                                                                $myfollow = [];
                                                                                                                                                if ($findfriends) {

                                                                                                                                                    $myfriends =  UserProfile::find()->select(['user_id'])->where(['!=', 'user_id', \Yii::$app->user->id])->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $findfriends])->all();
                                                                                                                                                    $searchedusersids = array();
                                                                                                                                                    if (!empty($myfollow)) {
                                                                                                                                                        foreach ($myfollow as $fol) {
                                                                                                                                                            $searchedusersids[] = $fol->user_id;
                                                                                                                                                        }
                                                                                                                                                    }

                                                                                                                                                    $foll = FriendRequest::find()->where(["requested_by" => \Yii::$app->user->id])->orWhere(['requested_to' => \Yii::$app->user->id])->andWhere(['status' => 1])->all();
                                                                                                                                                    if ($foll) {
                                                                                                                                                        foreach ($foll  as $tmp) {
                                                                                                                                                            if ($tmp->requested_to == \Yii::$app->user->id) {
                                                                                                                                                                array_push($followlist, $tmp->requested_by);
                                                                                                                                                            } else if ($tmp->requested_by == \Yii::$app->user->id) {
                                                                                                                                                                array_push($followlist, $tmp->requested_to);
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                    //$myfriends = FriendRequest::find()->where(["requested_by"=>\Yii::$app->user->id])->orWhere(['requested_to'=>\Yii::$app->user->id])->andWhere(['status'=>1])->all();
                                                                                                                                                    //                if($myfriends)
                                                                                                                                                    //                {
                                                                                                                                                    //                        foreach( $myfriends  as $key=>$tmp ) {
                                                                                                                                                    //                            if(empty($searchedusersids)){
                                                                                                                                                    //                                unset($myfriends[$key]);
                                                                                                                                                    //                            }else if($tmp->requested_to==\Yii::$app->user->id && !in_array($tmp->requested_by,$searchedusersids)) {
                                                                                                                                                    //                                unset($myfriends[$key]);
                                                                                                                                                    //                            }else if ($tmp->requested_by==\Yii::$app->user->id && !in_array($tmp->requested_to,$searchedusersids)){
                                                                                                                                                    //                                unset($myfriends[$key]);
                                                                                                                                                    //                            }
                                                                                                                                                    //                        }
                                                                                                                                                    //                }
                                                                                                                                                } else {


                                                                                                                                                    //                $foll = FriendRequest::find()->select("requested_to")->where(["requested_by"=>\Yii::$app->user->id])->all();
                                                                                                                                                    //                if($foll)
                                                                                                                                                    //                {
                                                                                                                                                    //                        foreach( $foll  as $tmp )
                                                                                                                                                    //                                array_push($followlist,$tmp->requested_to);
                                                                                                                                                    //                }
                                                                                                                                                    //echo Yii::$app->user->id;exit;
                                                                                                                                                    if ($type == 'requests') {
                                                                                                                                                        $myfriends = FriendRequest::find()->where(['!=', 'requested_by', \Yii::$app->user->id])->andWhere(['requested_to' => \Yii::$app->user->id])->andWhere(['!=', 'requested_to', 1])->andWhere(['status' => 0])->all();
                                                                                                                                                    } else {
                                                                                                                                                        $myfriends = FriendRequest::find()->where(["requested_by" => \Yii::$app->user->id])->orWhere(['requested_to' => \Yii::$app->user->id])->andWhere(['status' => 1])->all();
                                                                                                                                                    }
                                                                                                                                                }

                                                                                                                                                //echo "<pre>";print_r($followlist);exit;
                                                                                                                                                return $this->render(
                                                                                                                                                    'my_follow',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'myfollow' => $myfollow,
                                                                                                                                                        'findfriends' => $findfriends,
                                                                                                                                                        'followlist' => $myfriends,
                                                                                                                                                        'type' => $type,
                                                                                                                                                        'follows' => $followlist
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }


                                                                                                                                            public function actionFriendRequested()
                                                                                                                                            {
                                                                                                                                                $user = User::findOne(\Yii::$app->user->id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                                                                                                $myfriend = FriendRequest::find()->Where(["requested_to" => \Yii::$app->user->id])->andWhere(["status" => 0])->orderBy("f_id DESC")->all();

                                                                                                                                                return $this->render(
                                                                                                                                                    'my_friend_requested',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'myfriend' => $myfriend,
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }


                                                                                                                                            public function actionMyFollow()
                                                                                                                                            {
                                                                                                                                                $user = User::findOne(\Yii::$app->user->id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                                                                                                $myfriend = FriendRequest::find()->where(["requested_by" => \Yii::$app->user->id])->orWhere(["requested_to" => \Yii::$app->user->id])->andWhere(["status" => 1])->all();

                                                                                                                                                return $this->render(
                                                                                                                                                    'my_follow',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'myfriend' => $myfriend,
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }

                                                                                                                                            public function actionInboxMessage()
                                                                                                                                            {
                                                                                                                                                if (Yii::$app->user->isGuest) {
                                                                                                                                                    return $this->redirect(['site/login']);
                                                                                                                                                }
                                                                                                                                                $user = User::findOne(\Yii::$app->user->id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                                                                                                $messages = $this->getInboxThreads();
                                                                                                                                                return $this->render(
                                                                                                                                                    'chat_history',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        // 'messages' => $messages
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }


                                                                                                                                            public function actionInboxUserMessage($id)
                                                                                                                                            {
                                                                                                                                                $user = User::findOne($id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => $id])->one();
                                                                                                                                                $messages = $this->getInboxThreads();

                                                                                                                                                return $this->render(
                                                                                                                                                    'inbox_messages',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'messages' => $messages
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }

                                                                                                                                            public function actionSentMessage()
                                                                                                                                            {
                                                                                                                                                $user = User::findOne(\Yii::$app->user->id);
                                                                                                                                                $profile = UserProfile::find()->where(['user_id' => \Yii::$app->user->id])->one();
                                                                                                                                                //$messages = Message::find()->where(['sender_id'=>\Yii::$app->user->id,'sender_del'=>0])->orderBy('m_id DESC')->all();
                                                                                                                                                $messages = $this->getSentThreads();

                                                                                                                                                $senduser = UserProfile::find()->where(['!=', 'user_id', \Yii::$app->user->id])->all();
                                                                                                                                                // print_r($messages);die;

                                                                                                                                                return $this->render(
                                                                                                                                                    'sent_messages',
                                                                                                                                                    [
                                                                                                                                                        'user' => $user,
                                                                                                                                                        'profile' => $profile,
                                                                                                                                                        'messages' => $messages,
                                                                                                                                                        'senduser' => $senduser
                                                                                                                                                    ]
                                                                                                                                                );
                                                                                                                                            }

                                                                                                                                            /**
                                                                                                                                             * Multiple Deletes an existing User for company admin model.
                                                                                                                                             * If deletion is successful, the browser will be redirected to the 'index' page.
                                                                                                                                             * @param integer $id
                                                                                                                                             * @return mixed
                                                                                                                                             */

                                                                                                                                            public function actionMultiDeleteSendMessage()
                                                                                                                                            {
                                                                                                                                                $msg_id = Yii::$app->request->post()['msg_id'];

                                                                                                                                                if ($msg_id) {
                                                                                                                                                    foreach ($msg_id as $tmp) {
                                                                                                                                                        $message =  Message::find()->where(['m_id' => $tmp])->andwhere(['OR', ['recipient_id' => \Yii::$app->user->id], ['sender_id' => \Yii::$app->user->id]])->one();
                                                                                                                                                        if ($message) {
                                                                                                                                                            /* if($message->reply_sender_id == \Yii::$app->user->id )
							$message->sender_del = 1;
						else if ($message->reply_recipient_id == \Yii::$app->user->id )
							$message->recipient_del = 1;							
						else 							
							$message->sender_del = 1;
						 */
                                                                                                                                                            if (!empty($message->delete_status)) {
                                                                                                                                                                $message->delete_status = $message->delete_status . "," . \Yii::$app->user->id . ",";
                                                                                                                                                            } else {
                                                                                                                                                                $message->delete_status = "," . \Yii::$app->user->id . ",";
                                                                                                                                                            }

                                                                                                                                                            $message->save();
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }

                                                                                                                                            public function actionDeleteSendMessage()
                                                                                                                                            {
                                                                                                                                                $msg_id = Yii::$app->request->post()['msg_id'];
                                                                                                                                                if ($msg_id) {
                                                                                                                                                    $message =  Message::find()->where(['m_id' => $msg_id])->andwhere(['OR', ['recipient_id' => \Yii::$app->user->id], ['sender_id' => \Yii::$app->user->id]])->one();

                                                                                                                                                    if ($message) {
                                                                                                                                                        /* if($message->reply_sender_id == \Yii::$app->user->id )
						$message->sender_del = 1;
					else if ($message->reply_recipient_id == \Yii::$app->user->id )
						$message->recipient_del = 1;							
					else 							
						$message->sender_del = 1; */

                                                                                                                                                        if (!empty($message->delete_status)) {
                                                                                                                                                            $message->delete_status = $message->delete_status . "," . \Yii::$app->user->id . ",";
                                                                                                                                                        } else {
                                                                                                                                                            $message->delete_status = "," . \Yii::$app->user->id . ",";
                                                                                                                                                        }

                                                                                                                                                        $message->save();
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }


                                                                                                                                            /**
                                                                                                                                             * Multiple Deletes an existing User for company admin model.
                                                                                                                                             * If deletion is successful, the browser will be redirected to the 'index' page.
                                                                                                                                             * @param integer $id
                                                                                                                                             * @return mixed
                                                                                                                                             */

                                                                                                                                            public function actionMultiDeleteInboxMessage()
                                                                                                                                            {
                                                                                                                                                $msg_id = Yii::$app->request->post()['msg_id'];

                                                                                                                                                if ($msg_id) {
                                                                                                                                                    foreach ($msg_id as $tmp) {
                                                                                                                                                        $message =  Message::find()->where(['m_id' => $tmp])->andwhere(['OR', ['recipient_id' => \Yii::$app->user->id], ['sender_id' => \Yii::$app->user->id]])->one();
                                                                                                                                                        if ($message) {

                                                                                                                                                            if (!empty($message->delete_status)) {
                                                                                                                                                                $message->delete_status = $message->delete_status . "," . \Yii::$app->user->id . ",";
                                                                                                                                                            } else {
                                                                                                                                                                $message->delete_status = "," . \Yii::$app->user->id . ",";
                                                                                                                                                            }

                                                                                                                                                            $message->save();
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }

                                                                                                                                            public function actionDeleteInboxMessage()
                                                                                                                                            {
                                                                                                                                                $msg_id = Yii::$app->request->post()['msg_id'];
                                                                                                                                                if ($msg_id) {
                                                                                                                                                    $message =  Message::find()->where(['m_id' => $msg_id])->andwhere(['OR', ['recipient_id' => \Yii::$app->user->id], ['sender_id' => \Yii::$app->user->id]])->one();

                                                                                                                                                    if ($message) {


                                                                                                                                                        if (!empty($message->delete_status)) {
                                                                                                                                                            $message->delete_status = $message->delete_status . "," . \Yii::$app->user->id . ",";
                                                                                                                                                        } else {
                                                                                                                                                            $message->delete_status = "," . \Yii::$app->user->id . ",";
                                                                                                                                                        }


                                                                                                                                                        $message->save();
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }


                                                                                                                                            public function actionReadInboxMessage()
                                                                                                                                            {
                                                                                                                                                $msg_id = Yii::$app->request->post()['msg_id'];
                                                                                                                                                if ($msg_id) {
                                                                                                                                                    $message =  Message::find()->where(['m_id' => $msg_id])
                                                                                                                                                        ->andwhere(['Or', ['recipient_id' => \Yii::$app->user->id], ['reply_sender_id' => \Yii::$app->user->id], ['reply_recipient_id' => \Yii::$app->user->id]])->one();

                                                                                                                                                    if ($message) {
                                                                                                                                                        $message->read_text = 1;
                                                                                                                                                        $message->save();
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }


                                                                                                                                            public function getInboxThreads()
                                                                                                                                            {
                                                                                                                                                //first send
                                                                                                                                                $threads = [];
                                                                                                                                                //$deleteduser = ",".\Yii::$app->user->id.",";
                                                                                                                                                $selectReported = (ReportContent::find()->select('reported_user')->where(
                                                                                                                                                    [
                                                                                                                                                        'report_user' => \Yii::$app->user->id,
                                                                                                                                                        'report_type' => 'user'
                                                                                                                                                    ]
                                                                                                                                                ));
                                                                                                                                                //echo $deleteduser;exit;
                                                                                                                                                $inbox_messages = Message::find()
                                                                                                                                                    ->where(['parent_id' => 0])
                                                                                                                                                    //->andwhere(['NOT LIKE','delete_status',$deleteduser])
                                                                                                                                                    ->andwhere(['OR', ['reply_recipient_id' => \Yii::$app->user->id], ['reply_sender_id' => \Yii::$app->user->id], ['and', ['recipient_id' => \Yii::$app->user->id], ['reply_recipient_id' => 0]]])
                                                                                                                                                    ->andwhere(['not in', 'recipient_id', $selectReported])
                                                                                                                                                    ->orderBy('created_at DESC')
                                                                                                                                                    //echo  $inbox_messages->createCommand()->getRawSql();exit;
                                                                                                                                                    ->all();


                                                                                                                                                foreach ($inbox_messages as $messages) {
                                                                                                                                                    $deleteusers1 = $messages->delete_status;
                                                                                                                                                    $deleteusers = explode(',', $messages->delete_status);

                                                                                                                                                    if ($deleteusers1 == '') {
                                                                                                                                                        $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                                                                                                                                                        $threadsmsg = [];
                                                                                                                                                        if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                            foreach ($thread_messages as $messages2) {
                                                                                                                                                                $threadsmsg[$messages2->m_id] = [
                                                                                                                                                                    'm_id' => $messages2->m_id,
                                                                                                                                                                    'text' => $messages2->text,
                                                                                                                                                                    'created_at' => $messages2->created_at,
                                                                                                                                                                    'send_by' => $messages2->sender_id,
                                                                                                                                                                ];
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        $other_id = $messages->sender_id;
                                                                                                                                                        if ($messages->reply_recipient_id != 0) {
                                                                                                                                                            if ($messages->reply_recipient_id != \Yii::$app->user->id)
                                                                                                                                                                $other_id = $messages->reply_recipient_id;
                                                                                                                                                            else if ($messages->reply_sender_id != \Yii::$app->user->id)
                                                                                                                                                                $other_id = $messages->reply_sender_id;
                                                                                                                                                        }

                                                                                                                                                        $threads[$messages->m_id] = [
                                                                                                                                                            'm_id' => $messages->m_id,
                                                                                                                                                            'w_id' => $messages->w_id,
                                                                                                                                                            'donation_id' => $messages->donation_id,
                                                                                                                                                            'read_text' => $messages->read_text,
                                                                                                                                                            'subject' => $messages->subject,
                                                                                                                                                            'text' => $messages->text,
                                                                                                                                                            'created_at' => $messages->created_at,
                                                                                                                                                            'sender_id' => $messages->sender_id,
                                                                                                                                                            'recipient_id' => $other_id,
                                                                                                                                                            'reply_recipient_id' => $messages->reply_recipient_id
                                                                                                                                                        ];

                                                                                                                                                        if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                            $threads[$messages->m_id]['threads'] = $threadsmsg;
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        if (!in_array(\Yii::$app->user->id, $deleteusers)) {
                                                                                                                                                            $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                                                                                                                                                            $threadsmsg = [];
                                                                                                                                                            if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                                foreach ($thread_messages as $messages2) {
                                                                                                                                                                    $threadsmsg[$messages2->m_id] = [
                                                                                                                                                                        'm_id' => $messages2->m_id,
                                                                                                                                                                        'text' => $messages2->text,
                                                                                                                                                                        'created_at' => $messages2->created_at,
                                                                                                                                                                        'send_by' => $messages2->sender_id,
                                                                                                                                                                    ];
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                            $other_id = $messages->sender_id;
                                                                                                                                                            if ($messages->reply_recipient_id != 0) {
                                                                                                                                                                if ($messages->reply_recipient_id != \Yii::$app->user->id)
                                                                                                                                                                    $other_id = $messages->reply_recipient_id;
                                                                                                                                                                else if ($messages->reply_sender_id != \Yii::$app->user->id)
                                                                                                                                                                    $other_id = $messages->reply_sender_id;
                                                                                                                                                            }

                                                                                                                                                            $threads[$messages->m_id] = [
                                                                                                                                                                'm_id' => $messages->m_id,
                                                                                                                                                                'w_id' => $messages->w_id,
                                                                                                                                                                'donation_id' => $messages->donation_id,
                                                                                                                                                                'subject' => $messages->subject,
                                                                                                                                                                'read_text' => $messages->read_text,
                                                                                                                                                                'text' => $messages->text,
                                                                                                                                                                'created_at' => $messages->created_at,
                                                                                                                                                                'sender_id' => $messages->sender_id,
                                                                                                                                                                'recipient_id' => $other_id,
                                                                                                                                                                'reply_recipient_id' => $messages->reply_recipient_id
                                                                                                                                                            ];

                                                                                                                                                            if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                                $threads[$messages->m_id]['threads'] = $threadsmsg;
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                                return $threads;
                                                                                                                                            }


                                                                                                                                            public function getSentThreads()
                                                                                                                                            {

                                                                                                                                                $threads = [];

                                                                                                                                                $selectReported = (ReportContent::find()->select('reported_user')->where(
                                                                                                                                                    [
                                                                                                                                                        'report_user' => \Yii::$app->user->id,
                                                                                                                                                        'report_type' => 'user',
                                                                                                                                                    ]
                                                                                                                                                ));

                                                                                                                                                $deleteduser = "," . \Yii::$app->user->id . ",";

                                                                                                                                                $sent_messages = Message::find()->where(['parent_id' => 0])
                                                                                                                                                    ->andwhere(['NOT LIKE', 'delete_status', $deleteduser])
                                                                                                                                                    ->andwhere(['OR', ['reply_recipient_id' => \Yii::$app->user->id], ['reply_sender_id' => \Yii::$app->user->id], ['and', ['sender_id' => \Yii::$app->user->id], ['reply_sender_id' => 0]]])
                                                                                                                                                    ->andwhere(['not in', 'recipient_id', $selectReported])
                                                                                                                                                    ->orderBy('created_at DESC')->all();

                                                                                                                                                //  echo $sent_messages->createCommand()->getRawSql();
                                                                                                                                                // print_r($sent_messages);die();

                                                                                                                                                foreach ($sent_messages as $messages) {

                                                                                                                                                    $thread_messages = Message::find()->where(['parent_id' => $messages->m_id])->orderBy('created_at ASC')->all();

                                                                                                                                                    $threadsmsg = [];
                                                                                                                                                    if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                        foreach ($thread_messages as $messages2) {
                                                                                                                                                            $threadsmsg[$messages2->m_id] = [

                                                                                                                                                                'm_id' => $messages2->m_id,
                                                                                                                                                                'text' => $messages2->text,
                                                                                                                                                                'created_at' => $messages2->created_at,
                                                                                                                                                                'send_by' => $messages2->sender_id,

                                                                                                                                                            ];
                                                                                                                                                        }
                                                                                                                                                    }

                                                                                                                                                    $other_id = $messages->sender_id;
                                                                                                                                                    if ($messages->reply_recipient_id != 0) {

                                                                                                                                                        if ($messages->reply_recipient_id != \Yii::$app->user->id)
                                                                                                                                                            $other_id = $messages->reply_recipient_id;
                                                                                                                                                        else if ($messages->reply_sender_id != \Yii::$app->user->id)
                                                                                                                                                            $other_id = $messages->reply_sender_id;
                                                                                                                                                    }

                                                                                                                                                    $threads[$messages->m_id] = [
                                                                                                                                                        'm_id' => $messages->m_id,
                                                                                                                                                        'read_text' => $messages->read_text,
                                                                                                                                                        'text' => $messages->text,
                                                                                                                                                        'created_at' => $messages->created_at,
                                                                                                                                                        'sender_id' => $messages->recipient_id,
                                                                                                                                                        'recipient_id' => $other_id,
                                                                                                                                                    ];

                                                                                                                                                    if (isset($thread_messages) && !empty($thread_messages)) {
                                                                                                                                                        $threads[$messages->m_id]['threads'] = $threadsmsg;
                                                                                                                                                    }
                                                                                                                                                }

                                                                                                                                                return $threads;
                                                                                                                                            }

                                                                                                                                            public function actionSendMessageInbox()
                                                                                                                                            {
                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $msg = \Yii::$app->request->post()['msg'];
                                                                                                                                                $subject = \Yii::$app->request->post()['subject'];
                                                                                                                                                $created_at = \Yii::$app->request->post()['send_date'];
                                                                                                                                                $wish_id = (isset($_POST['wish_id']) && $_POST['wish_id'] != '') ? $_POST['wish_id'] : NULL;
                                                                                                                                                $donation_id = (isset($_POST['donation_id']) && $_POST['donation_id'] != '') ? $_POST['donation_id'] : NULL;

                                                                                                                                                if ($from != '' && $to != '' && $msg != '') {
                                                                                                                                                    $message = new Message();
                                                                                                                                                    $message->sender_id = $from;
                                                                                                                                                    $message->recipient_id = $to;
                                                                                                                                                    $message->parent_id = 0;
                                                                                                                                                    $message->read_text = 0;
                                                                                                                                                    $message->subject = $subject;
                                                                                                                                                    $message->w_id = $wish_id;
                                                                                                                                                    $message->donation_id = $donation_id;
                                                                                                                                                    $message->text = $msg;
                                                                                                                                                    $message->created_at =  date("Y-m-d H:i:s");

                                                                                                                                                    if ($message->save()) {
                                                                                                                                                        $mailcontent = MailContent::find()->where(['m_id' => 30])->one();
                                                                                                                                                        $editmessage = $mailcontent->mail_message;
                                                                                                                                                        $mailsubject = $mailcontent->mail_subject;
                                                                                                                                                        if (empty($mailsubject))
                                                                                                                                                            $subject =     'SimplyWishes ';
                                                                                                                                                        $user = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $from,
                                                                                                                                                        ]);

                                                                                                                                                        $user2 = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $to,
                                                                                                                                                        ]);
                                                                                                                                                        $fromuserprofile = UserProfile::findOne([
                                                                                                                                                            'user_id' => $from
                                                                                                                                                        ]);
                                                                                                                                                        $touserprofile = UserProfile::findOne([
                                                                                                                                                            'user_id' => $to
                                                                                                                                                        ]);
                                                                                                                                                        if (!empty($user2)) {
                                                                                                                                                            $email_subject = str_replace("##USERNAME2##", $fromuserprofile->firstname, $mailcontent->mail_subject);
                                                                                                                                                            $subject = $mailcontent->mail_subject;
                                                                                                                                                            $message = Yii::$app
                                                                                                                                                                ->mailer
                                                                                                                                                                ->compose(
                                                                                                                                                                    ['html' => 'inboxmessage'],
                                                                                                                                                                    ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'message' => $message]
                                                                                                                                                                )
                                                                                                                                                                ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                                                                                                                                                                ->setTo($user2->email)
                                                                                                                                                                ->setSubject($email_subject);

                                                                                                                                                            $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                                                                                                                                                            $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                                                                                                                                                            $message->send();
                                                                                                                                                        }
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }

                                                                                                                                            public function actionReplyMessage()
                                                                                                                                            {
                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];
                                                                                                                                                $msg = \Yii::$app->request->post()['msg'];
                                                                                                                                                $msg_id = \Yii::$app->request->post()['msg_id'];
                                                                                                                                                $created_at = \Yii::$app->request->post()['send_date'];

                                                                                                                                                if ($from != '' && $to != '' && $msg != '') {
                                                                                                                                                    $message = new Message();
                                                                                                                                                    $message->sender_id = $from;
                                                                                                                                                    $message->recipient_id = $to;
                                                                                                                                                    $message->parent_id = $msg_id;
                                                                                                                                                    $message->text = $msg;
                                                                                                                                                    $message->read_text = 0;
                                                                                                                                                    $message->delete_status = "";
                                                                                                                                                    $message->created_at = date("Y-m-d H:i:s");;

                                                                                                                                                    if ($message->save()) {
                                                                                                                                                        $parentmessages = Message::find()->where(['m_id' => $message->parent_id])->one();
                                                                                                                                                        $parentmessages->reply_sender_id = $from;
                                                                                                                                                        $parentmessages->reply_recipient_id = $to;
                                                                                                                                                        $parentmessages->read_text = 0;
                                                                                                                                                        $parentmessages->delete_status = "";
                                                                                                                                                        // $parentmessages->created_at = date("Y-m-d H:i:s");
                                                                                                                                                        $parentmessages->save();

                                                                                                                                                        $mailcontent = MailContent::find()->where(['m_id' => 30])->one();
                                                                                                                                                        $editmessage = $mailcontent->mail_message;
                                                                                                                                                        $mailsubject = $mailcontent->mail_subject;
                                                                                                                                                        if (empty($mailsubject))
                                                                                                                                                            $subject =     'SimplyWishes ';
                                                                                                                                                        $user = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $from,
                                                                                                                                                        ]);

                                                                                                                                                        $user2 = User::findOne([
                                                                                                                                                            //'status' => User::STATUS_ACTIVE,
                                                                                                                                                            'id' => $to,
                                                                                                                                                        ]);
                                                                                                                                                        if (!empty($user2)) {
                                                                                                                                                            $email_subject = str_replace("##USERNAME2##", $user->username, $mailcontent->mail_subject);
                                                                                                                                                            $subject = $mailcontent->mail_subject;
                                                                                                                                                            $message = Yii::$app
                                                                                                                                                                ->mailer
                                                                                                                                                                ->compose(
                                                                                                                                                                    ['html' => 'inboxmessage'],
                                                                                                                                                                    ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'message' => $message]
                                                                                                                                                                )
                                                                                                                                                                ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                                                                                                                                                                ->setTo($user2->email)
                                                                                                                                                                ->setSubject($email_subject);

                                                                                                                                                            $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                                                                                                                                                            $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
                                                                                                                                                            $message->send();
                                                                                                                                                        }
                                                                                                                                                        Yii::$app->session->setFlash('messageSent');
                                                                                                                                                        return json_encode([
                                                                                                                                                            'status' => true
                                                                                                                                                        ]);
                                                                                                                                                    } else return json_encode([
                                                                                                                                                        'status' => false
                                                                                                                                                    ]);
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            public function actionFriendRequest()
                                                                                                                                            {
                                                                                                                                                $model = new FriendRequest();
                                                                                                                                                $from = \Yii::$app->request->post()['send_from'];
                                                                                                                                                $to = \Yii::$app->request->post()['send_to'];

                                                                                                                                                if ($from != '' && $to != '') {
                                                                                                                                                    $checkdata = FriendRequest::find()->where("(requested_by = " . $from . " AND requested_to = " . $to . ") OR (requested_by = " . $to . " AND requested_to = " . $from . ")")->andWhere(['in', 'status', [0, 1, 2]])->orderBy(['f_id' => SORT_DESC])->one();
                                                                                                                                                    //echo $checkdata->createCommand()->getRawSql();exit;
                                                                                                                                                    //->one();

                                                                                                                                                    if (!empty($checkdata)) {
                                                                                                                                                        echo json_encode(array('status' => 0));
                                                                                                                                                        exit;
                                                                                                                                                        //$friend_request_id = $checkdata->f_id;
                                                                                                                                                    } else {
                                                                                                                                                        $request = new FriendRequest();
                                                                                                                                                        $request->requested_by = $from;
                                                                                                                                                        $request->requested_to = $to;
                                                                                                                                                        if ($request->save()) {

                                                                                                                                                            $model->sendEmail($to, $request->f_id);

                                                                                                                                                            echo json_encode(array('status' => 1));
                                                                                                                                                            exit;
                                                                                                                                                        }
                                                                                                                                                        //$friend_request_id = $request->f_id;
                                                                                                                                                    }




                                                                                                                                                    /* 			$profile = UserProfile::find()->where(['user_id'=>$to])->one();
                    $msg = $profile->firstname.' '.$profile->lastname.' Send Friend Request to you <a style="color:#337ab7 !important"  href="'.Url::to(['friend/request-accepted','id'=>$friend_request_id]).'"><u>Accept</u></a>';

                    $message = new Message();
                    $message->sender_id = $from;
                    $message->recipient_id = $to;
                    $message->parent_id = 0;
                    $message->text = $msg;
                    $message->save();
         */
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            public function actionChatUsers()
                                                                                                                                            {
                                                                                                                                                $search = (isset($_POST['search']) && $_POST['search'] != '') ? $_POST['search'] : '';
                                                                                                                                                $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : 'all';
                                                                                                                                                $offset = (isset($_POST['page']) && $_POST['page'] != '') ? ($_POST['page'] * 20)  : 0;
                                                                                                                                                $page =  20;
                                                                                                                                                $user_id = Yii::$app->user->id;
                                                                                                                                                $totalcount = 0;
                                                                                                                                                $data = [];

                                                                                                                                                if ($search != '') {
                                                                                                                                                    if ($type == 'Everyone') {
                                                                                                                                                        $selectReported = (ReportContent::find()->select('reported_user')->where(
                                                                                                                                                            [
                                                                                                                                                                'report_user' => $user_id,
                                                                                                                                                                'report_type' => 'user',
                                                                                                                                                            ]
                                                                                                                                                        ));
                                                                                                                                                        $users = UserProfile::find()->select(['firstname', 'lastname', 'user_id as id', 'profile_image', 'user.sendbird_user_id'])
                                                                                                                                                            ->With(['user' => function (ActiveQuery $query) use ($search) {
                                                                                                                                                                return $query->select('sendbird_user_id');
                                                                                                                                                            }])->where(['!=', 'user_id', $user_id])
                                                                                                                                                            ->where(['not in', 'user_id', $selectReported])->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                                                                                                                                                        $totalcount = $users->count();
                                                                                                                                                        $data = $users->limit($page)
                                                                                                                                                            ->offset($offset)->asArray()
                                                                                                                                                            ->all();
                                                                                                                                                    } else {
                                                                                                                                                        $myfriends = FriendRequest::find()
                                                                                                                                                            ->With(['userModel' => function (ActiveQuery $query) use ($search) {
                                                                                                                                                                return $query->select('user_id,firstname,lastname,profile_image,sendbird_user_id')->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                                                                                                                                                            }])
                                                                                                                                                            ->With(['otherUserModel' => function (ActiveQuery $query) use ($search) {
                                                                                                                                                                return $query->select('user_id,firstname,lastname,profile_image,sendbird_user_id')->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                                                                                                                                                            }])->where(["requested_by" => $this->user_id])->orWhere(['requested_to' => $this->user_id])->andWhere(['status' => 1]);
                                                                                                                                                        $totalcount = $myfriends->count();
                                                                                                                                                        $friends = $myfriends->limit($page)
                                                                                                                                                            ->offset($offset)->asArray()
                                                                                                                                                            ->all();

                                                                                                                                                        $followlist = [];
                                                                                                                                                        if (!empty($friends)) {
                                                                                                                                                            foreach ($friends as $key => $value) {
                                                                                                                                                                if ($value['requested_to'] == $user_id && $value['userModel'] != null) {
                                                                                                                                                                    array_push($followlist, $value['userModel']);
                                                                                                                                                                } else if ($value['requested_by'] == $user_id && $value['otherUserModel'] != null) {
                                                                                                                                                                    array_push($followlist, $value['otherUserModel']);
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        $data = $followlist;
                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    if ($type == 'Everyone') {
                                                                                                                                                        $selectReported = (ReportContent::find()->select('reported_user')->where(
                                                                                                                                                            [
                                                                                                                                                                'report_user' => $user_id,
                                                                                                                                                                'report_type' => 'user',
                                                                                                                                                            ]
                                                                                                                                                        ));
                                                                                                                                                        $users = Userprofile::find()->innerJoin('user', 'user.id = user_profile.user_id')->select(['firstname', 'lastname', 'user_id as id', 'profile_image', 'user.sendbird_user_id'])
                                                                                                                                                            ->where(['!=', 'user_id', $user_id])
                                                                                                                                                            ->where(['not in', 'user_id', $selectReported]);

                                                                                                                                                        $totalcount = $users->count();

                                                                                                                                                        $data = $users->limit($page)
                                                                                                                                                            ->offset($offset)->asArray()->all();
                                                                                                                                                    } else if ($type == 'Connections') {

                                                                                                                                                        $myfriends = FriendRequest::find()
                                                                                                                                                            ->With(['userModel' => function (ActiveQuery $query) {
                                                                                                                                                                return $query->select('user_id,firstname,lastname,profile_image,sendbird_user_id');
                                                                                                                                                            }])->With(['otherUserModel' => function (ActiveQuery $query) {
                                                                                                                                                                return $query->select('user_id,firstname,lastname,profile_image,sendbird_user_id');
                                                                                                                                                            }])->where(["requested_by" => $user_id])->orWhere(['requested_to' => $user_id])->andWhere(['status' => 1]);

                                                                                                                                                        $totalcount = $myfriends->count();
                                                                                                                                                        $friends = $myfriends->limit($page)
                                                                                                                                                            ->offset($offset)->asArray()
                                                                                                                                                            ->all();


                                                                                                                                                        $followlist = [];
                                                                                                                                                        if (!empty($friends)) {
                                                                                                                                                            foreach ($friends as $key => $value) {
                                                                                                                                                                if ($value['requested_to'] == $user_id && $value['userModel'] != null) {
                                                                                                                                                                    array_push($followlist, $value['userModel']);
                                                                                                                                                                } else if ($value['requested_by'] == $user_id && $value['otherUserModel'] != null) {
                                                                                                                                                                    array_push($followlist, $value['otherUserModel']);
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                        $data = $followlist;
                                                                                                                                                    }
                                                                                                                                                }


                                                                                                                                                return json_encode(array('status' => 1, 'totalpages' => ceil($totalcount / 20), 'data' => $data));
                                                                                                                                            }
                                                                                                                                            public function actionFetchUserDetails()
                                                                                                                                            {
                                                                                                                                                $sendbirdUserId = (isset($_POST['sendbirdUserId']) && $_POST['sendbirdUserId'] != '') ? $_POST['sendbirdUserId'] : '';
                                                                                                                                                echo $sendbirdUserId;
                                                                                                                                                exit;
                                                                                                                                            }

                                                                                                                                            public function actionReportMessage()
                                                                                                                                            {

                                                                                                                                                $message = $_POST['message'];
                                                                                                                                                $messageId = $_POST['messageId'];
                                                                                                                                                $channelUrl = $_POST['channelUrl'];
                                                                                                                                                $reportedUser = $_POST['reportedUser'];
                                                                                                                                                $userDetails = User::find()->where(['sendbird_user_id' => $reportedUser])->one();
                                                                                                                                                $alreadyReported = ReportContent::find()->where(['report_type' => 'message', 'sendbird_message' => $messageId, 'report_user' => Yii::$app->user->id, 'reported_user' => $userDetails->id])->count();
                                                                                                                                                if ($alreadyReported > 0) {
                                                                                                                                                    echo json_encode(array('status' => 2));
                                                                                                                                                    exit;
                                                                                                                                                }
                                                                                                                                                $activity = new ReportContent();
                                                                                                                                                $activity->report_user = Yii::$app->user->id;
                                                                                                                                                $activity->reported_user = $userDetails->id;
                                                                                                                                                $activity->report_type = 'message';
                                                                                                                                                $activity->comment = $message;
                                                                                                                                                $activity->content_id = NULL;
                                                                                                                                                $activity->sendbird_message = $messageId;
                                                                                                                                                $activity->sendbird_channel_url = $channelUrl;
                                                                                                                                                $activity->date = date('Y-m-d H:i:s');
                                                                                                                                                $activity->blocked = "0";
                                                                                                                                                if ($activity->save()) {
                                                                                                                                                    echo json_encode(array('status' => 1));
                                                                                                                                                    exit;
                                                                                                                                                } else {
                                                                                                                                                    echo json_encode(array('status' => 0));
                                                                                                                                                    exit;
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                            public function actionUserDetails()
    {
        if (!isset($_POST['sendbird_user_id']) || empty($_POST['sendbird_user_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('sendbird_user_id Field is Mandatory.'));
        }
        $sendbird_user_id = $_POST['sendbird_user_id'];
        $user = User::find()->where(['sendbird_user_id' => $sendbird_user_id])->one();
        if ($user) {
            $data['profile'] = UserProfile::find()->select(['user_profile.firstname','user_profile.lastname','user_profile.profile_image','user.id','user.'])
                ->innerJoin('user','user.id = user_profile.user_id')
                ->where(['user_id' => $user->id])
                ->asArray()->one();
            return json_encode($data);
        } else {
            $data['profile'] = array();
            return json_encode($data);
        }
    }
                                                                                                                                        }
                                                                                                                                        
