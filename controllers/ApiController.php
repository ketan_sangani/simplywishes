<?php

namespace app\controllers;
require_once 'vendor/firebase/php-jwt/src/JWT.php';
require_once 'vendor/firebase/php-jwt/src/SignatureInvalidException.php';
require_once 'vendor/firebase/php-jwt/src/BeforeValidException.php';
require_once 'vendor/firebase/php-jwt/src/ExpiredException.php';
require_once 'vendor/firebase/php-jwt/src/JWK.php';
require_once 'vendor/firebase/php-jwt/src/Key.php';

use app\models\Activity;
use app\models\City;
use app\models\ContactForm;
use app\models\Country;
use app\models\Donation;
use app\models\DonationComments;
use app\models\DonationCommentsActivities;
use app\models\Editorial;
use app\models\EditorialActivity;
use app\models\EditorialCommentActivity;
use app\models\EditorialComments;
use app\models\FriendRequest;
use app\models\HappyStories;
use app\models\HappyStoriesCommentActivity;
use app\models\HappyStoriesComments;
use app\models\LoginForm;
use app\models\MailContent;
use app\models\Message;
use app\models\Page;
use app\models\PasswordResetRequestForm;
use app\models\ReportContent;
use app\models\ReportWishes;
use app\models\State;
use app\models\StoryActivity;
use app\models\Wish;
use app\models\WishComments;
use app\models\WishCommentsActivities;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use phpDocumentor\Reflection\Types\Null_;
use yii\base\BaseObject;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\User;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\ContentNegotiator;
use app\models\UserProfile;
use yii\web\UploadedFile;


class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @var mixed
     */
    private $user_id;

    public function returnMessage($msg)
    {
        $mssage = [$msg];
        $mandatory = ['error' => $mssage];
        return $mandatory;
    }

    protected function findModel($id, $post_type)
    {
        if ($post_type == 'wish') {
            if (($model = Wish::findOne($id)) !== null) {
                return $model;
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Record not found'));
            }
        } else if ($post_type == 'donation') {
            if (($model = Donation::findOne($id)) !== null) {
                return $model;
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Record not found'));
            }
        } else if ($post_type == 'forum') {
            if (($model = Editorial::findOne($id)) !== null) {
                return $model;
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Record not found'));
            }
        } else if ($post_type == 'happy_story') {
            if (($model = HappyStories::findOne($id)) !== null) {
                return $model;
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Record not found'));
            }
        }
    }

    public function behaviors()
    {

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ]
            ],
        ];
    }

    public function beforeAction($action)
    {
        header('Access-Control-Allow-Origin: *');

        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

        header("Access-Control-Allow-Headers: X-Requested-With,token,user");
        parent::beforeAction($action);

        if ($action->actionMethod != 'actionLogin' && $action->actionMethod != 'actionRegister' && $action->actionMethod != 'actionSendMailForgotPassword' && $action->actionMethod != 'actionFb' && $action->actionMethod != 'actionConfig' && $action->actionMethod != 'actionDefaultimages' && $action->actionMethod != 'actionUserDetails') {
            $headers = Yii::$app->request->headers;
            if (!empty($headers) && isset($headers['token']) && $headers['token'] != '') {
                try {

                    $token = $headers['token'];

                    $key = 'simpltywishes_key';
                    try {
                        $decoded = JWT::decode($token, new Key($key, 'HS256'));

                        if (!empty($decoded) && isset($decoded->uid) && !empty($decoded->uid)) {
                            $this->user_id = $decoded->uid->id;
                            Yii::$app->params['apiuser_id'] = $this->user_id;
                            return true;
                        } else {
                            echo json_encode(array('status' => 0, 'error' => $this->returnMessage('Authentication Failed.')));
                            exit;
                        }
                    } catch (Exception $e) {

                        echo json_encode(array('status' => 0, 'error' => $this->returnMessage('Authentication Failed.')));
                        exit;
                    }


                } catch (Exception $e) {
                    echo json_encode(array('status' => 0, 'error' => $this->returnMessage('Authentication Failed.')));
                    exit;

                }

                //var_dump($token->validate($data));exit;

                //return true;
            } else {
                if ($action->actionMethod == 'actionSearch' || $action->actionMethod == 'actionPropertydetails' || $action->actionMethod == 'actionDashboard') {
                    return true;
                } else {
                    echo json_encode(array('status' => 0, 'error' => $this->returnMessage('Authentication Failed.')));
                    exit;
                }

            }
            //exit;
        }
        return true;
    }


    public function sendwishCommentEmail($wish, $comment)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 16])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);

        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);

        if (!$user || !$likedUser) {
            return false;
        }
        $email_subject = str_replace("##USERNAME2##", $likedUser->username, $mailcontent->mail_subject);
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'wishComment-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish, 'likedUser' => $likedUser, 'comment' => $comment]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($email_subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendDonationCommentEmail($donation, $comment)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 26])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->created_by,
        ]);

        $likedUser = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => \Yii::$app->user->id,
        ]);

        if (!$user || !$likedUser) {
            return false;
        }
        $email_subject = str_replace("##USERNAME2##", $likedUser->username, $mailcontent->mail_subject);
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'donationComment-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation, 'likedUser' => $likedUser, 'comment' => $comment]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($email_subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmailWish($wish)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 5])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->wished_by,
        ]);

        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $wish->process_granted_by,
        ]);

        if (!$user) {
            return false;
        }
        if (!$user2) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'grantedSuccess-html'],
                ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'wish' => $wish]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmailDonation($donation)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 25])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes sendEmail';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->created_by,
        ]);

        $user2 = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $donation->process_granted_by,
        ]);

        if (!$user) {
            return false;
        }
        if (!$user2) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'acceptedDonationSuccess-html'],
                ['user' => $user, 'user2' => $user2, 'editmessage' => $editmessage, 'donation' => $donation]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        // die($message);
        return $message->send();
    }

    public function sendForgotPasswordEmail($email)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 3])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;

        if (empty($subject))
            $subject = 'SimplyWishes';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $email,
        ]);
        //var_dump($user);exit;
        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            $user->scenario = 'apply_forgotpassword';
            if (!$user->save()) {
                return false;
            }
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html'],
                ['user' => $user, 'editmessage' => $editmessage]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendValidationEmail($email)
    {

        $mailcontent = MailContent::find()->where(['m_id' => 13])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_INACTIVE,
            'email' => $email,
        ]);

        // echo "sql: ".$user->createCommand()->sql; exit;
        //     echo "sql";
        // print_r($user);exit;
        if (!$user) {
            return false;
        }
        $profile = UserProfile::find()->where(['user_id' => $user->id])->one();

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'signupValidationRegister-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'profile' => $profile]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($email)
            ->setSubject($subject);


        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');
        // print_r($message);
        // exit;
        return $message->send();
    }

    public function sendEmailToGranter($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 14])->one();

        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccess-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmailToWisher($id, $wish)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 33])->one();

        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);

        if (!$user) {
            return false;
        }

        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccesstowisher-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'wish' => $wish]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmailToAccepted($id, $donation)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 24])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);


        if (!$user) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'fullfilledSuccessDonation-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);
        // print_r($donation);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmailToCompleteDonation($id, $donation)
    {
        $mailcontent = MailContent::find()->where(['m_id' => 34])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';

        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'id' => $id,
        ]);


        if (!$user) {
            return false;
        }


        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'completeDonation-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'donation' => $donation]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($user->email)
            // ->setTo("alexcapitaneanu@gmail.com")
            ->setSubject($subject);
        // print_r($donation);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function sendEmail($email)
    {

        $mailcontent = MailContent::find()->where(['m_id' => 2])->one();
        $editmessage = $mailcontent->mail_message;
        $subject = $mailcontent->mail_subject;
        if (empty($subject))
            $subject = 'SimplyWishes ';


        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $email,
        ]);

        if (!$user) {
            return false;
        }
        $profile = UserProfile::find()->where(['user_id' => $user->id])->one();
        $message = Yii::$app
            ->mailer
            ->compose(
                ['html' => 'signupRegister-html'],
                ['user' => $user, 'editmessage' => $editmessage, 'profile' => $profile]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'SimplyWishes '])
            ->setTo($email)
            ->setSubject($subject);

        $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
        $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n');

        return $message->send();
    }

    public function actionLogin()
    {

        if (!empty($_POST)) {
            $model = new User();
            $model->scenario = 'login';
            $model->attributes = Yii::$app->request->post();
            if ($model->validate()) {
                $userexist = User::find()
                    ->with('userProfile')
                    ->where([
                        'email' => $model->email,
                    ])->one();

                if (!empty($userexist) && $userexist->validatePassword($model->password)) {
                    if ($userexist->status == 10) {
                        $token = (string)User::generateToken(ArrayHelper::toArray($userexist));
                        $userexist1 = User::find()
                            ->with('userProfile')
                            ->where([
                                'email' => $model->email,
                            ])->asArray()->one();
                        $properties = Utils::getUserProperties($userexist1['id']);
                        $userexist1['can_create_wish'] = ($properties['can_create_wish'] == 1) ? 1 : 0;
                        $userexist1['can_create_donation'] = ($properties['can_create_donation'] == 1) ? 1 : 0;
                        $userexist1['max_no_wishes'] = $properties['max_no_wishes'];
                        $userexist1['max_no_donations'] = $properties['max_no_donations'];
                        return array('status' => 1, 'message' => 'User Logged in Successfully', 'data' => $userexist1, 'token' => $token);
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Please verify your email.'));
                    }

                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('You have entered wrong email or password.'));
                }
            } else {
                return array('status' => 0, 'error' => $model->getErrors());
            }

        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Please enter Mandatory Fields.'));
        }
    }

    public function actionFb()
    {

        if (!isset($_POST['fb_id']) || $_POST['fb_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('fb_id Field is Mandatory.'));
        }
        if (isset($_POST['email'])) {
            $user_exist = User::find()->where(['email' => $_POST['email'], 'fb_id' => $_POST['fb_id']])->exists();
        } else {
            $user_exist = User::find()->where(['email' => $_POST['email']])->exists();
        }

        if ($user_exist) {
            if (isset($_POST['email'])) {
                $user = User::find()->where(['email' => $_POST['email']])->orWhere(['fb_id' => $_POST['fb_id']])->one();

            } else {
                $user = User::find()->where(['fb_id' => $_POST['fb_id']])->one();
            }

            /*if(isset($input['fb_id'])){
                $user->fb_id = $input['fb_id'];
            }*/
            if (isset($input['email'])) {
                $user->email = $_POST['email'];
            }
            if (isset($input['username'])) {
                $user->username = $_POST['username'];
            }
            $user->fb_id = (isset($_POST['fb_id']) && $_POST['fb_id'] != '') ? $_POST['fb_id'] : null;

            if ($user->save()) {
                $user_profile = UserProfile::find()->where(['user_id' => $user->id])->one();
                if (isset($_POST['firstname'])) {
                    $user_profile->firstname = $_POST['firstname'];
                }
                if (isset($_POST['lastname'])) {
                    $user_profile->lastname = $_POST['lastname'];
                }
                if (isset($_POST['about'])) {
                    $user_profile->about = $_POST['about'];
                }
                if (isset($_POST['country'])) {
                    $user_profile->country = $_POST['country'];
                }
                if (isset($_POST['state'])) {
                    $user_profile->state = $_POST['state'];
                }
                if (isset($_POST['city'])) {
                    $user_profile->city = $_POST['city'];
                }
                if (isset($_POST['image'])) {
                    $user_profile->profile_image = $_POST['image'];
                }

                if ($user_profile->save(false)) {
                    $userexist = User::find()->where([
                        'email' => $user->email,
                    ])->one();
                    if (!empty($userexist)) {
                        $token = (string)User::generateToken(ArrayHelper::toArray($userexist));
                        $userexist1 = User::find()
                            ->with('userProfile')
                            ->where([
                                'email' => $user->email,
                            ])->asArray()->one();
                        $this->sendValidationEmail($user->email);
                        return array('status' => 1, 'data' => $userexist1, 'token' => $token);
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('You have entered wrong email or password.'));
                    }
                } else {
                    return array('status' => 0, 'error' => $user->getErrors());
                }
            }
        } else {
            $user = new User();
            if (isset($_POST['fb_id'])) {
                $user->fb_id = $_POST['fb_id'];
            }
            if (isset($_POST['email'])) {
                $user->email = $_POST['email'];
            }
            if (isset($_POST['username'])) {
                $user->username = $_POST['username'];
            }
            if ($user->save()) {
                $user_profile = new UserProfile();
                $user_profile->user_id = $user->id;
                if (isset($_POST['firstname'])) {
                    $user_profile->firstname = $_POST['firstname'];
                }
                if (isset($input['lastname'])) {
                    $user_profile->lastname = $_POST['lastname'];
                }
                if (isset($_POST['about'])) {
                    $user_profile->about = $_POST['about'];
                }
                if (isset($_POST['country'])) {
                    $user_profile->country = $_POST['country'];
                }
                if (isset($_POST['state'])) {
                    $user_profile->state = $_POST['state'];
                }
                if (isset($_POST['city'])) {
                    $user_profile->city = $_POST['city'];
                }
                if (isset($_POST['image'])) {
                    $user_profile->profile_image = $_POST['image'];
                }

                if ($user_profile->save(false)) {
                    $userexist = User::find()->where([
                        'email' => $user->email,
                    ])->one();
                    if (!empty($userexist)) {
                        $token = (string)User::generateToken(ArrayHelper::toArray($userexist));
                        $userexist1 = User::find()
                            ->with('userProfile')
                            ->where([
                                'email' => $user->email,
                            ])->asArray()->one();
                        $sendBirdUserId = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) ).$user->id;
                        $userArray['user_id'] = $sendBirdUserId;
                        $userArray['nickname'] = $user_profile->firstname." ".$user_profile->lastname;
                        $userArray['profile_url'] = \yii\helpers\Url::base('https').'/web/uploads/users/'.$user_profile->profile_image;
                        $chatUsers = json_encode($userArray);
                        $curl = curl_init();

                        curl_setopt_array($curl, array(
                            CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users',
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => '',
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 0,
                            CURLOPT_FOLLOWLOCATION => true,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => 'POST',
                            CURLOPT_POSTFIELDS => $chatUsers,
                            CURLOPT_HTTPHEADER => array(
                                'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
                                'Content-Type: application/json',
                                'Accept: application/json'
                            ),
                        ));

                        $response = curl_exec($curl);

                        curl_close($curl);
                        $user->sendbird_user_id = $sendBirdUserId;
                        $user->save(false);
                        $this->sendValidationEmail($user->email);
                        return array('status' => 1, 'data' => $userexist1, 'token' => $token);
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('You have entered wrong email or password.'));
                    }
                } else {
                    return array('status' => 0, 'error' => $user->getErrors());
                }
            }
        }

    }

    public function actionDefaultimages()
    {
        $data['profileimages'] = array(
            "uploads/users/images/img1.jpg",
            "uploads/users/images/img2.jpg",
            "uploads/users/images/img3.jpg",
            "uploads/users/images/img4.jpg",
            "uploads/users/images/img10m.png",
            "uploads/users/images/img7.jpg",
            "uploads/users/images/img8.jpg",
            "uploads/users/images/img9.jpg",
            "uploads/users/images/img10.jpg",
            "uploads/users/images/img11.jpg",
            "uploads/users/images/img12.jpg",
            "uploads/users/images/img13.jpg",
            "uploads/users/images/img14.jpg",
            "uploads/users/images/img15.jpg",
            "uploads/users/images/img16.jpg",
            "uploads/users/images/img17.jpg",
            "uploads/users/images/img18.jpg",
            "uploads/users/images/img19.jpg",
            "uploads/users/images/img20.jpg",
            "uploads/users/images/img21.jpg",
            "uploads/users/images/img22.jpg",
            "uploads/users/images/img23.jpg",
            "uploads/users/images/img24.jpg",
            "uploads/users/images/img25.jpg",
            "uploads/users/images/img26.jpg",
            "uploads/users/images/img27.jpg",
            "uploads/users/images/img28.jpg",
            "uploads/users/images/img29.jpg",
            "uploads/users/images/img31.jpg",
            "uploads/users/images/img32.jpg",
            "uploads/users/images/img33.jpg",
            "uploads/users/images/img34.jpg",
            "uploads/users/images/img35.jpg",
            "uploads/users/images/img36.jpg",
        );
        $data['content_images'] = array(
            "images/wish_default/1.jpg",
            "images/wish_default/2.jpg",
            "images/wish_default/3.jpg",
            "images/wish_default/4.jpg",
            "images/wish_default/5.jpg",
            "images/wish_default/6.jpg",
            "images/wish_default/7.jpg",
            "images/wish_default/8.jpg",
            "images/wish_default/9.jpg",
            "images/wish_default/10.jpg",
            "images/wish_default/11.jpg",
            "images/wish_default/12.jpg",
            "images/wish_default/13.jpg",
            "images/wish_default/14.jpg",
            "images/wish_default/15.jpg",
            "images/wish_default/16.jpg",
            "images/wish_default/20.jpg",
            "images/wish_default/21.jpg",
            "images/wish_default/22.jpg",
            "images/wish_default/23.jpg",
            "images/wish_default/24.jpg",
            "images/wish_default/25.jpg",
            "images/wish_default/26.jpg",
        );

        $data['happy_story_images'] = array(
            "images/happy-story/1.jpg",
            "images/happy-story/2.jpg",
            "images/happy-story/3.jpg",
            "images/happy-story/4.jpg",
            "images/happy-story/5.jpg",
            "images/happy-story/6.jpg",
            "images/happy-story/7.jpg",
            "images/happy-story/8.jpg",
            "images/happy-story/9.jpg",
            "images/happy-story/10.jpg",
            "images/happy-story/11.jpg",
            "images/happy-story/12.jpg",
            "images/happy-story/13.jpg",
            "images/happy-story/14.jpg",
            "images/happy-story/15.jpg",
            "images/happy-story/16.jpg",
            "images/happy-story/17.jpg",
            "images/happy-story/18.jpg"
        );
        return array('status' => 1, 'data' => $data);

    }

    public function actionRegister()
    {

        $user = new User();
        $user->scenario = 'signup-api';
        $profile = new UserProfile();
        $user->attributes = Yii::$app->request->post();
        $user->setPassword($user->password);
        $user->generateAuthKey();
        $user->status = 13;
        $user->created_at = date("Y-m-d H:i:s");
        $user->updated_at = date("Y-m-d H:i:s");
        //$user->image = $uploads = UploadedFile::getInstanceByName('image');
        if ($user->validate()) {
            if ($user->save(false)) {
                $profile->user_id = $user->id;
                $profile->firstname = $user->firstname;
                $profile->lastname = $user->lastname;
                $profile->about = (isset($user->about)) ? $user->about : '';
                $profile->country = $user->country;
                $profile->state = $user->state;
                $profile->city = $user->city;
                if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                    $unique = rand(10, 500) . time();
                    $user->image = $uploads = UploadedFile::getInstanceByName('image');
                    $profile->profile_image = $unique . '.jpg';
                    $user->image->saveAs('web/uploads/users/' . $unique . '.jpg');
                    $user->image = null;
                } else if (isset($_POST['default_image_name']) && $_POST['default_image_name'] != '') {
                    $profile->profile_image = $_POST['default_image_name'];

                }

                if ($profile->save(false)) {
                    $userexist = User::find()->where([
                        'email' => $user->email,
                    ])->one();
                    if (!empty($userexist)) {
                        $token = (string)User::generateToken(ArrayHelper::toArray($userexist));
                        $userexist1 = User::find()
                            ->with('userProfile')
                            ->where([
                                'email' => $user->email,
                            ])->asArray()->one();
                        $sendBirdUserId = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) ).$user->id;
                        $userArray['user_id'] = $sendBirdUserId;
                        $userArray['nickname'] = $profile->firstname." ".$profile->lastname;
                        $userArray['profile_url'] = \yii\helpers\Url::base('https').'/web/uploads/users/'.$profile->profile_image;
//                        $chatUsers = json_encode($userArray);
//                        $curl = curl_init();
//
//                        curl_setopt_array($curl, array(
//                            CURLOPT_URL => 'https://api-6198EF20-7666-4AB7-B59D-4CDCB14F91BA.sendbird.com/v3/users',
//                            CURLOPT_RETURNTRANSFER => true,
//                            CURLOPT_ENCODING => '',
//                            CURLOPT_MAXREDIRS => 10,
//                            CURLOPT_TIMEOUT => 0,
//                            CURLOPT_FOLLOWLOCATION => true,
//                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//                            CURLOPT_CUSTOMREQUEST => 'POST',
//                            CURLOPT_POSTFIELDS => $chatUsers,
//                            CURLOPT_HTTPHEADER => array(
//                                'Api-Token: 856625988402cffc13fdd45250eadd62ffc1d16c',
//                                'Content-Type: application/json',
//                                'Accept: application/json'
//                            ),
//                        ));
//
//                        $response = curl_exec($curl);
//
//                        curl_close($curl);
//                        $user->sendbird_user_id = $sendBirdUserId;
//                        $user->save(false);
                        $this->sendValidationEmail($user->email);
                        return array('status' => 1, 'message' => 'We have sent verification email.Please check your inbox.', 'data' => $userexist1, 'token' => $token);
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('You have entered wrong email or password.'));
                    }
                } else {
                    return array('status' => 0, 'message' => 'Something went wrong.');
                }
            }
        } else {
            return array('status' => 0, 'error' => $user->getErrors());
        }
    }

    public function actionConfig()
    {
        $countries = Country::find()->orderBy([new \yii\db\Expression('FIELD (id, 231) DESC')])->addOrderBy('name ASC')->all();
        $states = State::find()->all();
        $cities = City::find()->all();
        return array('status' => 1, 'countries' => $countries, 'states' => $states, 'cities' => $cities);
    }

    public function actionSendMailForgotPassword()
    {
        if (!isset($_POST['email']) || empty($_POST['email'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Email is Mandatory Fields.'));
        }

        $user_exist = User::find()->where(['email' => $_POST['email']])->exists();
        if ($user_exist) {
            if ($this->sendForgotPasswordEmail($_POST['email'])) {
                return array('status' => 1, 'message' => 'Check your email for further instructions.');
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Sorry, we are unable to reset password for email provided.'));
            }
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Email not Found'));
        }


    }

    public function actionHome()
    {
        $curr_wishes = Wish::find()
            ->With(['wisherModel' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->select(['wishes.*', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved'])
            ->where(['granted_by' => null, 'wish_status' => 0, 'process_status' => 0])
            ->orderBy('created_at DESC')
            ->limit(3)
            ->asArray()
            ->all();
        // echo "<pre>";print_r($curr_wishes);exit;
        $curr_donations = Donation::find()
            ->With(['donorModel' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->select(['donations.*', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved'])
            ->where(['granted_by' => null, 'status' => 0, 'process_status' => 0])
            ->orderBy('created_at DESC')
            ->limit(3)
            ->asArray()
            ->all();
        $granted_wishes = Wish::find()
            ->With(['wisherModel' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->select(['wishes.*', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved'])
            ->where(['not', ['granted_by' => null]])
            ->orderBy('w_id DESC')
            ->limit(2)
            ->asArray()
            ->all();
        $granted_donation = Donation::find()
            ->With(['donorModel' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->select(['donations.*', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved'])
            ->where(['not', ['granted_by' => null]])
            ->orderBy('id DESC')
            ->limit(1)
            ->asArray()
            ->all();
        $granted = ['wish' => $granted_wishes, 'donation' => $granted_donation];
        $happy_stories = HappyStories::find()
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['wish' => function (ActiveQuery $query) {
                return $query->select('w_id,wish_title');
            }])
            ->where(['status' => 0])
            ->orderBy('hs_id Desc')
            ->limit(3)
            ->asArray()
            ->all();
        $forums = Editorial::find()
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->where(['is_video_only' => 0, 'status' => 0])
            ->orderBy('updated_at DESC')
            ->limit(3)
            ->asArray()
            ->all();
        $editorial_first = ['id' => 6, 'image' => "web/uploads/editorial/Connect_Thumb.png", 'class' => 'video'];
        $editorial_second = ['id' => 5, 'image' => "web/uploads/editorial/Do_YouHaveAWish_Thumb.png", 'class' => 'video'];
        $editorial_third = ['id' => 225, 'image' => "web/uploads/editorial/-PcQBPc636kbQOWD0FKnAfotJ6mhy3u1.png", 'class' => 'video'];
        $editorial = [$editorial_first, $editorial_second, $editorial_third];
        $properties = Utils::getUserProperties($this->user_id);
        //print_r($properties);exit;
        $userexist1['can_create_wish'] = ($properties['can_create_wish'] == 1) ? 1 : 0;
        $userexist1['can_create_donation'] = ($properties['can_create_donation'] == 1) ? 1 : 0;

        $data = ['current_wishes' => $curr_wishes,
            'current_donations' => $curr_donations,
            'granted' => $granted,
            'forums' => $forums,
            'editorial' => $editorial,
            'happy_stories' => $happy_stories,
            'can_create_contents' => $userexist1];
        return array('status' => 1,
            'data' => $data,
            /*'happy_stories' => $happy_stories,*/
        );
    }

    public function actionGetEditorial()
    {
        if (!empty($_POST)) {
            if (!isset($_POST['id']) || empty($_POST['id'])) {
                return array('status' => 0, 'error' => $this->returnMessage('Id is Mandatory Fields.'));
            }
            if (!isset($_POST['class']) || empty($_POST['class'])) {
                return array('status' => 0, 'error' => 'Class is Mandatory Fields.');
            }
            $model = Editorial::find()
                ->where(['e_id' => $_POST['id'], 'status' => 0])
                ->With(['author' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['editorial.*', '(CASE WHEN (select count(*) from editorial_activities where editorial_activities.e_id = editorial.e_id and editorial_activities.activity="like" and editorial_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_activities where editorial_activities.e_id = editorial.e_id and editorial_activities.activity="like") as total_like'])
                ->asArray()
                ->one();

            if ($model['e_id'] == '6' || $model['e_id'] == '5' || $model['e_id'] == '225') {
                if ($model['e_id'] == '6') {
                    $model['e_image'] = 'uploads/editorial/Connect_Thumb.png';
                } else if ($model['e_id'] == '5') {
                    $model['e_image'] = 'uploads/editorial/Do_YouHaveAWish_Thumb.png';
                } else if ($model['e_id'] == '225') {
                    $model['e_image'] = 'uploads/editorial/-PcQBPc636kbQOWD0FKnAfotJ6mhy3u1.png';
                }
            }

            $comments = EditorialComments::find()
                ->where(['e_id' => $_POST['id'], 'parent_id' => 0, 'editorial_comments.status' => 0])
                ->With(['likes' => function (ActiveQuery $query) {
                    return $query->select('a_id,e_comment_id')->where(['user_id' => $this->user_id]);
                }])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select('e_comment_id ,parent_id ,comments,editorial_comments.user_id')->where(['editorial_comments.status' => 0])
                        ->With(['likes' => function (ActiveQuery $query) {
                            return $query->select('editorial_comment_activities.a_id,editorial_comment_activities.e_comment_id')->where(['user_id' => $this->user_id]);
                        }])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])
                ->select(['editorial_comments.*', '(CASE WHEN (select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id and editorial_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id) as total_like'])
                ->asArray()
                ->orderBy('e_comment_id Desc')
                ->all();
            return array('status' => 1,
                'model' => $model,
                'comments' => $comments,
            );
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Please enter Mandatory Fields.'));
        }

    }

    public function actionLike()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Id Field is Mandatory.'));
        }
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Class Field is Mandatory.'));
        }
        if (!isset($_POST['post_type']) || empty($_POST['post_type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Post type Field is Mandatory.'));
        }
        if ($_POST['post_type'] == 'wish') {
            $wish = $this->findModel($_POST['id'], $_POST['post_type']);
            $activity = Activity::find()->where(['wish_id' => $wish->w_id, 'activity' => $_POST['type'], 'user_id' => $this->user_id])->one();
        } else if ($_POST['post_type'] == 'donation') {
            $donation = $this->findModel($_POST['id'], $_POST['post_type']);
            $activity = Activity::find()->where(['donation_id' => $donation->id, 'activity' => $_POST['type'], 'user_id' => $this->user_id])->one();
        } else if ($_POST['post_type'] == 'forum') {
            $forum = $this->findModel($_POST['id'], $_POST['post_type']);
            $activity = EditorialActivity::find()->where(['e_id' => $forum->e_id, 'activity' => $_POST['type'], 'user_id' => $this->user_id])->one();
        } else if ($_POST['post_type'] == 'happy_story') {
            $happystory = $this->findModel($_POST['id'], $_POST['post_type']);
            $activity = StoryActivity::find()->where(['story_id' => $happystory->hs_id, 'activity' => $_POST['type'], 'user_id' => $this->user_id])->one();
        }

        if ($activity != null) {
            $activity->delete();
            return array('status' => 1, 'message' => $_POST['type'] == 'like' ? 'Like removed successfully' : 'Unsaved successfully');
        }
        if ($_POST['post_type'] == 'forum') {
            $activity = new EditorialActivity();
            $activity->e_id = $forum->e_id;
            $activity->activity = $_POST['type'];
            $activity->user_id = $this->user_id;
            if ($activity->save()) {
                /*if ($_POST['post_type'] == 'wish'){
                    WishController::sendLikedEmail($wish);

                }else{
                    DonationController::sendLikedEmail($donation);
                }*/
                return array('status' => 1, 'message' => 'Forum liked successfullly');
            }
        } else if ($_POST['post_type'] == 'happy_story') {
            $activity = new StoryActivity();
            $activity->story_id = $happystory->hs_id;
            $activity->activity = $_POST['type'];
            $activity->user_id = $this->user_id;
            if ($activity->save(false)) {
                /*if ($_POST['post_type'] == 'wish'){
                    WishController::sendLikedEmail($wish);

                }else{
                    DonationController::sendLikedEmail($donation);
                }*/
                return array('status' => 1, 'message' => 'Happy Story liked successfullly');
            }

        } else {
            $activity = new Activity();
            if ($_POST['post_type'] == 'wish') {
                $activity->wish_id = $wish->w_id;
            } else {
                $activity->donation_id = $donation->id;
            }

            $activity->activity = $_POST['type'];
            $activity->user_id = $this->user_id;
            if ($activity->save()) {
                /*if ($_POST['post_type'] == 'wish'){
                    WishController::sendLikedEmail($wish);

                }else{
                    DonationController::sendLikedEmail($donation);
                }*/
                return array('status' => 1, 'message' => $_POST['type'] == 'like' ? 'Wish Like successfully' : 'Wish save successfully');
            }
        }
    }

    public function actionWishesAndDonations()
    {
        $offset = (isset($_POST['page']) && $_POST['page'] != '') ? ($_POST['page'] * 10) / 2 : 0;
        $page = $offset + 5;

        if (!isset($_POST['type']) || empty($_POST['type'])) {
            $wishquery = Wish::find()
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                ->where(['wish_status' => 0])
                //->joinWith(['likes as activities'])
                ->orderBy('total_like DESC')
                ->groupBy('w_id');
            $totalcountwishes = $wishquery->count();
            $wishes = $wishquery->limit($page)
                ->offset($offset)->asArray()
                ->all();


            //$wish = $wishquery;//$wishactivedataprovider->models;
            $donationquery = Donation::find()
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->where(['status' => 0])
                // ->joinWith(['likes as activities'])
                ->orderBy('total_like DESC')
                ->groupBy('id');
            $totalcountdonations = $donationquery->count();
            $donations = $donationquery->limit($page)
                ->offset($offset)->asArray()
                ->all();
            $data = array_merge($wishes, $donations);
        } elseif ($_POST['type'] == "granted") {
            $wishquery = Wish::find()
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                ->where(['not', ['granted_by' => null]])
                //->joinWith(['likes as activities'])
                ->orderBy('granted_date DESC')
                ->groupBy('w_id');
            $totalcountwishes = $wishquery->count();
            $wishes = $wishquery->limit($page)
                ->offset($offset)->asArray()
                ->all();


            $donationquery = Donation::find()
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->where(['not', ['granted_by' => null]])
                //->joinWith(['likes as activities'])
                ->orderBy('granted_date DESC')
                ->groupBy('id');
            $totalcountdonations = $donationquery->count();
            $donations = $donationquery->limit($page)
                ->offset($offset)->asArray()
                ->all();
            $data = array_merge($wishes, $donations);
            $price = array_column($data, 'granted_date');

            array_multisort($price, SORT_DESC, $data);

        } elseif ($_POST['type'] == "inprogress") {
            $wishquery = Wish::find()
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.process_granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                ->where(['wish_status' => 0, 'process_status' => 1, 'granted_by' => null])
                //->joinWith(['likes as activities'])
                ->orderBy('process_granted_date DESC')
                ->groupBy('w_id');
            $totalcountwishes = $wishquery->count();
            $wishes = $wishquery->limit($page)
                ->offset($offset)->asArray()
                ->all();

            $donationquery = Donation::find()
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.process_granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->where(['status' => 0, 'process_status' => 1, 'granted_by' => null])
                //->joinWith(['likes as activities'])
                ->orderBy('process_granted_date DESC')
                ->groupBy('id');
            $totalcountdonations = $donationquery->count();
            $donations = $donationquery->limit($page)
                ->offset($offset)->asArray()
                ->all();
            $data = array_merge($wishes, $donations);
            $price = array_column($data, 'process_granted_date');

            array_multisort($price, SORT_DESC, $data);
        }
        //$array = array_merge($wishes,$donations);

        // $most_popular = array_multisort( array_column($array, "total_like"), SORT_DESC, $array );

        return array('status' => 1, 'totalpages' => ceil(($totalcountdonations + $totalcountwishes) / 10), 'data' => $data);
    }

    public function actionCurrentWishesAndDonations()
    {
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }
        $offset = (isset($_POST['page']) && $_POST['page']) ? $_POST['page'] * 10 : 0;
        $page = $offset + 10;
        if ($_POST['type'] == 'wish') {
            if (!isset($_POST['filter']) || empty($_POST['filter'])) {
                $wishes = Wish::find()->where(['granted_by' => null, 'wish_status' => 0, 'process_status' => 0])
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->orderBy('createDate DESC');
                //->groupBy('w_id');
                $totalcount = $wishes->count();
                $data = $wishes->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

            } elseif ($_POST['filter'] == 'financial') {
                $wishes = Wish::find()
                    ->where(['granted_by' => null, 'wish_status' => 0, 'process_status' => 0])
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->andWhere(['non_pay_option' => 0])
                    ->orderBy('createDate DESC')
                    ->groupBy('w_id');
                $totalcount = $wishes->count();
                $data = $wishes->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

            } elseif ($_POST['filter'] == "granted") {
                $wishquery = Wish::find()
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    ->where(['not', ['granted_by' => null]])
                    //->joinWith(['likes as activities'])
                    ->orderBy('granted_date DESC')
                    ->groupBy('w_id');
                $totalcount = $wishquery->count();
                $data = $wishquery->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } elseif ($_POST['filter'] == "inprogress") {
                $wishquery = Wish::find()
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.process_granted_date', 'wishes.process_granted_by', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    ->where(['wish_status' => 0, 'process_status' => 1, 'granted_by' => null])
                    //->joinWith(['likes as activities'])
                    ->orderBy('process_granted_date DESC')
                    ->groupBy('w_id');
                $totalcount = $wishquery->count();
                $data = $wishquery->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } elseif ($_POST['filter'] == "popular") {
                $wishes = Wish::find()->where(['wish_status' => 0])
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->orderBy('total_like DESC')
                    ->groupBy('w_id');
                $totalcount = $wishes->count();
                $data = $wishes->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } else {

                $wishes = Wish::find()
                    ->where(['granted_by' => null, 'wish_status' => 0, 'process_status' => 0])->orderBy('createDate DESC')
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->andWhere(['non_pay_option' => 1])
                    ->orderBy('createDate DESC')
                    ->groupBy('w_id');
                $totalcount = $wishes->count();

                $data = $wishes->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

            }
            return array('status' => 1, 'totalpages' => ceil($totalcount / 10), 'data' => $data);
        } else {
            if (!isset($_POST['filter']) || empty($_POST['filter'])) {
                $donations = Donation::find()
                    ->where(['granted_by' => null, 'status' => 0, 'process_status' => 0])
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    ->orderBy('createDate DESC');
                //->joinWith(['likes as activities'])
                // ->groupBy('id');
                $totalcount = $donations->count();
                $data = $donations->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

            } elseif ($_POST['filter'] == 'financial') {
                $donations = Donation::find()
                    ->where(['granted_by' => null, 'status' => 0, 'process_status' => 0])
                    ->orderBy('donations.created_at DESC')
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->andWhere(['non_pay_option' => 0])
                    ->orderBy('createDate DESC');
                //->groupBy('id');
                $totalcount = $donations->count();
                $data = $donations->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

            } elseif ($_POST['filter'] == "granted") {
                $donationquery = Donation::find()
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    ->where(['not', ['granted_by' => null]])
                    //->joinWith(['likes as activities'])
                    ->orderBy('granted_date DESC')
                    ->groupBy('id');
                $totalcount = $donationquery->count();
                $data = $donationquery->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } elseif ($_POST['filter'] == "inprogress") {
                $donationquery = Donation::find()
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.process_granted_date', 'donations.process_granted_by', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    ->where(['status' => 0, 'process_status' => 1, 'granted_by' => null])
                    //->joinWith(['likes as activities'])
                    ->orderBy('process_granted_date DESC')
                    ->groupBy('id');
                $totalcount = $donationquery->count();
                $data = $donationquery->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } elseif ($_POST['filter'] == "popular") {
                $donationquery = Donation::find()
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.process_granted_date', 'donations.process_granted_by', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    ->where(['status' => 0])
                    //->joinWith(['likes as activities'])
                    ->orderBy('total_like DESC')
                    ->groupBy('id');
                $totalcount = $donationquery->count();
                $data = $donationquery->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } else {


                $donations = Donation::find()
                    ->andOnCondition(['granted_by' => null, 'status' => 0, 'process_status' => 0])
                    ->orderBy('donations.created_at DESC')
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                    //->joinWith(['likes as activities'])
                    ->andWhere(['non_pay_option' => 1])
                    ->orderBy('createDate DESC');
                //->groupBy('id');
                $totalcount = $donations->count();
                $data = $donations->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            }
            return array('status' => 1, 'totalpages' => ceil($totalcount / 10), 'data' => $data);
        }
    }

    public function actionSaveWish()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {

            $model = new Wish();
//            if (!isset($_FILES['primary_image_name']) || !is_uploaded_file($_FILES['primary_image_name']['tmp_name'])) {
//                return array('status' => 0, 'error' => $this->returnMessage('primary_image_name Field is Mandatory.'));
//            }
            $data = Yii::$app->request->post();
            if (!isset($_POST['wish_title']) || empty($_POST['wish_title'])) {
                return array('status' => 0, 'error' => $this->returnMessage('wish_title Field is Mandatory.'));
            }
            if ($_POST['wish_status'] == 0) {
                if (!isset($_POST['non_pay_option']) || $_POST['non_pay_option'] == null) {
                    return array('status' => 0, 'error' => $this->returnMessage('non_pay_option Field is Mandatory.'));
                }
                if (!isset($_POST['wish_status']) || $_POST['wish_status'] == null) {
                    return array('status' => 0, 'error' => $this->returnMessage('wish_status Field is Mandatory.'));
                }

                if (!isset($_POST['wish_description']) || empty($_POST['wish_description'])) {
                    return array('status' => 0, 'error' => $this->returnMessage('wish_description Field is Mandatory.'));
                }
                if (!isset($_POST['expected_date']) || empty($_POST['expected_date'])) {
                    return array('status' => 0, 'error' => $this->returnMessage('expected_date Field is Mandatory.'));
                }
                if (!isset($_POST['i_agree_decide']) || empty($_POST['i_agree_decide'])) {
                    return array('status' => 0, 'error' => $this->returnMessage('i_agree_decide Field is Mandatory.'));
                }
            }
            if ($data['non_pay_option'] == 0) {
                if ($_POST['wish_status'] == 0) {
                    if (!isset($_POST['financial_assistance']) || empty($_POST['financial_assistance'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('financial_assistance Field is Mandatory.'));
                    }
                    if (!isset($_POST['expected_cost']) || empty($_POST['expected_cost'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('expected_cost Field is Mandatory.'));
                    }

                }
                if (isset($data['financial_assistance']) && $data['financial_assistance'] == "Other") {
                    if (!isset($_POST['financial_assistance_other']) || empty($_POST['financial_assistance_other'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('financial_assistance_other Field is Mandatory.'));
                    }
                    $model->financial_assistance_other = $data['financial_assistance_other'];
                }
                $model->financial_assistance = isset($data['financial_assistance']) ? $data['financial_assistance'] : '';
                $model->expected_cost = isset($data['expected_cost']) ? $data['expected_cost'] : '';
                $model->show_mail = isset($data['show_mail']) ? $data['show_mail'] : '';
            } else {
                if ($_POST['wish_status'] == 0) {
                    if (!isset($_POST['way_of_wish']) || empty($_POST['way_of_wish'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('way_of_wish Field is Mandatory.'));
                    }
                    if (!isset($_POST['description_of_way']) || empty($_POST['description_of_way'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('description_of_way Field is Mandatory.'));
                    }
                }
                $model->way_of_wish = isset($data['way_of_wish']) ? $data['way_of_wish'] : '';
                $model->description_of_way = isset($data['description_of_way']) ? $data['description_of_way'] : '';
            }
            $model->wished_by = $this->user_id;
            $model->wish_status = $_POST['wish_status'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->wish_title = $data['wish_title'];
            $model->wish_description = $data['wish_description'];
            if (isset($_FILES['primary_image_name']) && is_uploaded_file($_FILES['primary_image_name']['tmp_name'])) {
                $model->primary_image_name = $uploads = UploadedFile::getInstanceByName('primary_image_name');
                $unique = rand(10, 500) . time();
                $model->primary_image = 'uploads/wishes/' . $unique . '.jpg';
                $model->primary_image_name->saveAs('web/uploads/wishes/' . $unique . '.jpg');
                $model->primary_image_name = null;
            } else {
                if (isset($_POST['default_image_name']) && $_POST['default_image_name'] != '') {
                    $model->primary_image = $_POST['default_image_name'];
                } else {
                    $model->primary_image = 'images/wish_default/2.jpg';///web/images/wish_default/2.jpg
                }
            }
            $model->expected_date = $data['expected_date'];
            $model->who_can = null;
            $model->non_pay_option = $data['non_pay_option'];
            $model->wish_status = $data['wish_status'];

            $model->show_mail_status = 0;

            $model->show_person_status = 0;
            $model->show_person_street = null;
            $model->show_person_city = null;
            $model->show_person_state = null;
            $model->show_person_zip = null;
            $model->show_person_country = null;
            $model->show_other_status = 0;
            $model->show_other_specify = null;
            $model->i_agree_decide = isset($data['i_agree_decide']) ? $data['i_agree_decide'] : 0;
            $model->i_agree_decide2 = 0;
            $model->process_status = 0;
            $model->process_granted_by = null;
            $model->process_granted_date = null;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                return array('status' => 1, 'message' => 'Wish Created Sucessfully', 'id' => $model->w_id);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
            }
        } else {
            $data = Yii::$app->request->post();
            $user_exist = Wish::find()->where(['w_id' => $_POST['id']])->exists();
            if ($user_exist) {
                $model = Wish::find()->where(['w_id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Wish not Found'));
            }
            $model->wish_status = (isset($_POST['wish_status']) && $_POST['wish_status'] != '') ? $_POST['wish_status'] : $model->wish_status;
            $model->created_at = date('Y-m-d H:i:s');
            $model->wish_title = (isset($data['wish_title']) && $data['wish_title'] != '') ? $data['wish_title'] : $model->wish_title;
            $model->wish_description = (isset($data['wish_description']) && $data['wish_description'] != '') ? $data['wish_description'] : $model->wish_description;
            $model->non_pay_option = (isset($data['non_pay_option']) && $data['non_pay_option'] != '') ? $data['non_pay_option'] : $model->non_pay_option;
            $model->expected_date = (isset($data['expected_date']) && $data['expected_date'] != '') ? $data['expected_date'] : $model->expected_date;
            if (isset($_FILES['primary_image_name']) && is_uploaded_file($_FILES['primary_image_name']['tmp_name'])) {
                $model->primary_image_name = $uploads = UploadedFile::getInstanceByName('primary_image_name');
                $unique = rand(10, 500) . time();
                $model->primary_image = 'uploads/wishes/' . $unique . '.jpg';
                $model->primary_image_name->saveAs('web/uploads/wishes/' . $unique . '.jpg');
                $model->primary_image_name = null;
            }
            if (isset($data['non_pay_option']) && $data['non_pay_option'] == 0) {
                $model->financial_assistance_other = (isset($data['financial_assistance_other']) && $data['financial_assistance_other'] != '') ? $data['financial_assistance_other'] : $model->financial_assistance_other;
                $model->financial_assistance = (isset($data['financial_assistance']) && $data['financial_assistance'] != '') ? $data['financial_assistance'] : $model->financial_assistance;
                $model->expected_cost = (isset($data['expected_cost']) && $data['expected_cost'] != '') ? $data['expected_cost'] : $model->expected_cost;
                $model->show_mail = (isset($data['show_mail']) && $data['show_mail'] != '') ? $data['show_mail'] : $model->show_mail;
                $model->way_of_wish = (isset($data['way_of_wish']) && $data['way_of_wish'] != '') ? $data['way_of_wish'] : '';
                $model->description_of_way = (isset($data['description_of_way']) && $data['description_of_way'] != '') ? $data['description_of_way'] : '';
            }
            if (isset($data['non_pay_option']) && $data['non_pay_option'] == 1) {
                $model->way_of_wish = (isset($data['way_of_wish']) && $data['way_of_wish'] != '') ? $data['way_of_wish'] : $model->way_of_wish;
                $model->description_of_way = (isset($data['description_of_way']) && $data['description_of_way'] != '') ? $data['description_of_way'] : $model->description_of_way;
                $model->financial_assistance_other = (isset($data['financial_assistance_other']) && $data['financial_assistance_other'] != '') ? $data['financial_assistance_other'] : '';
                $model->financial_assistance = (isset($data['financial_assistance']) && $data['financial_assistance'] != '') ? $data['financial_assistance'] : '';
                $model->expected_cost = (isset($data['expected_cost']) && $data['expected_cost'] != '') ? $data['expected_cost'] : '';
                $model->show_mail = (isset($data['show_mail']) && $data['show_mail'] != '') ? $data['show_mail'] : '';
            }
            $model->date_updated = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                return array('status' => 1, 'message' => 'Wish Updated Sucessfully', 'id' => $model->w_id);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
            }
        }


    }

    public function actionSaveForum()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {

            $model = new Editorial();

        } else {
            $user_exist = Editorial::find()->where(['e_id' => $_POST['id']])->exists();
            if ($user_exist) {
                $model = Editorial::find()->where(['e_id' => $_POST['id']])->one();
            } else {

                return array('status' => 0, 'error' => $this->returnMessage('Forum not Found'));
            }
        }

        $data = Yii::$app->request->post();

        if (!isset($_POST['forum_type']) || $_POST['forum_type'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Forum Type is Mandatory.'), 'data' => $_POST);
        }

        if (!isset($_POST['title']) || $_POST['title'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Forum Title is Mandatory.'));
        }
        if ($data['forum_type'] == 'article') {
            if (isset($_FILES['primary_image_name']) && is_uploaded_file($_FILES['primary_image_name']['tmp_name'])) {
                $model->articleimage = $uploads = UploadedFile::getInstanceByName('primary_image_name');
                $unique = \Yii::$app->security
                    ->generateRandomString();
                $model->article_image = 'uploads/editorial/' . $unique . '.jpg';
                $model->articleimage->saveAs('web/uploads/editorial/' . $unique . '.jpg');
                $model->articleimage = null;
            } else {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model->article_image = 'images/wish_default/2.jpg';
                }
            }

            $model->is_video_only = 0;
        } else if ($data['forum_type'] == 'video') {
            if ((isset($_POST['video_url']) && $_POST['video_url'] != null)) {
                $model->featured_video_url = $_POST['video_url'];
            } elseif (isset($_FILES['video']) && is_uploaded_file($_FILES['video']['tmp_name'])) {
                $model->video = UploadedFile::getInstanceByName('video');
                $unique = \Yii::$app->security
                    ->generateRandomString();
                $model->video->saveAs('web/uploads/media/' . $unique . '.' . $model->video->extension);
                $model->featured_video_url = Yii::$app->urlManager->createAbsoluteUrl(['/']) . 'web/uploads/media/' . $unique . '.' . $model->video->extension;
                $model->video = null;
            }
            if (isset($_FILES['primary_image_name']) && is_uploaded_file($_FILES['primary_image_name']['tmp_name'])) {
                $e_image = $model->e_image = UploadedFile::getInstanceByName('primary_image_name');
                $unique = \Yii::$app->security
                    ->generateRandomString();
                $e_image->saveAs('web/uploads/editorial/' . $unique . '.jpg');
                $model->e_image = 'uploads/editorial/' . $unique . '.jpg';
            } else {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model->e_image = 'images/wish_default/2.jpg';
                }
            }
            $model->is_video_only = 1;
        }
        $model->e_title = $data['title'];
        $model->e_text = $data['description'];

        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $model->updated_by = $this->user_id;
            $model->created_at = date('Y-m-d H:i:s');
        } else {
            $model->created_by = $this->user_id;
            $model->updated_by = $this->user_id;

        }
        if ($model->save(false)) {
            return array('status' => 1, 'message' => 'Data saved Sucessfully', 'id' => $model->e_id);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
        }
    }


    public function actionSaveDonation()
    {


        if (!isset($_POST['id']) || empty($_POST['id'])) {
//            if (!isset($_FILES['image_name']) || !is_uploaded_file($_FILES['image_name']['tmp_name']) ) {
//                return array('status' => 0, 'error' => $this->returnMessage('image_name Field is Mandatory.'));
//            }
            $model = new Donation();
            $data = Yii::$app->request->post();
            $model->created_by = $this->user_id;
            if ($_POST['status'] == 0) {
                if (!isset($_POST['non_pay_option']) || $_POST['non_pay_option'] == null) {
                    return array('status' => 0, 'error' => $this->returnMessage('non_pay_option Field is Mandatory.'));
                }
                if (!isset($_POST['status']) || $_POST['status'] == null) {
                    return array('status' => 0, 'error' => $this->returnMessage('status Field is Mandatory.'));
                }

                if (!isset($_POST['description']) || $_POST['description'] == null) {
                    return array('status' => 0, 'error' => $this->returnMessage('description Field is Mandatory.'));
                }
            }
            if (!isset($_POST['title']) || $_POST['title'] == null) {
                return array('status' => 0, 'error' => $this->returnMessage('title Field is Mandatory.'));
            }

            if ($data['non_pay_option'] == 0) {
                if ($_POST['status'] == 0) {
                    if (!isset($_POST['financial_assistance']) || empty($_POST['financial_assistance'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('financial_assistance Field is Mandatory.'));
                    }
                    if (!isset($_POST['expected_cost']) || empty($_POST['expected_cost'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('expected_cost Field is Mandatory.'));
                    }
                }
//                if (!isset($_POST['show_mail']) || empty($_POST['show_mail'])) {
//                    return array('status' => 0, 'error' => $this->returnMessage('show_mail Field is Mandatory.'));
//                }
                if (isset($data['financial_assistance']) && $data['financial_assistance'] == "Other") {
                    if (!isset($_POST['financial_assistance_other']) || empty($_POST['financial_assistance_other'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('financial_assistance_other Field is Mandatory.'));
                    }
                    $model->financial_assistance_other = $data['financial_assistance_other'];
                }

                $model->financial_assistance = isset($data['financial_assistance']) ? $data['financial_assistance'] : '';
                $model->expected_cost = isset($data['expected_cost']) ? $data['expected_cost'] : '';
                $model->show_mail = isset($_POST['show_mail']) ? $_POST['show_mail'] : '';

            } else {
                if ($_POST['status'] == 0) {
                    if (!isset($_POST['way_of_donation']) || empty($_POST['way_of_donation'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('way_of_donation Field is Mandatory.'));
                    }
                    if (!isset($_POST['description_of_way']) || empty($_POST['description_of_way'])) {
                        return array('status' => 0, 'error' => $this->returnMessage('description_of_way Field is Mandatory.'));
                    }
                }
                $model->way_of_donation = isset($data['way_of_donation']) ? $data['way_of_donation'] : '';
                $model->description_of_way = isset($data['description_of_way']) ? $data['description_of_way'] : '';
            }
            $model->non_pay_option = isset($data['non_pay_option']) ? $data['non_pay_option'] : '';
            $model->status = $_POST['status'];
            $model->title = $data['title'];
            $model->description = isset($data['description']) ? $data['description'] : '';
            $model->created_at = date('Y-m-d H:i:s');
            if (isset($_FILES['image_name']) && is_uploaded_file($_FILES['image_name']['tmp_name'])) {
                $model->image_name = $uploads = UploadedFile::getInstanceByName('image_name');
                $unique = rand(10, 500) . time();
                $model->image = 'uploads/donations/' . $unique . '.jpg';
                $model->image_name->saveAs('web/uploads/donations/' . $unique . '.jpg');
                $model->image_name = null;
            } else {
                if (isset($_POST['default_image_name']) && $_POST['default_image_name'] != '') {
                    $model->image = $_POST['default_image_name'];
                } else {
                    $model->image = 'images/wish_default/2.jpg';///web/images/wish_default/2.jpg
                }
            }
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                return array('status' => 1, 'message' => 'Donation Created Sucessfully', 'id' => $model->id);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
            }
        } else {
            $data = Yii::$app->request->post();
            $user_exist = Donation::find()->where(['id' => $_POST['id']])->exists();
            if ($user_exist) {
                $model = Donation::find()->where(['id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Donation not Found'));
            }
            $model->non_pay_option = (isset($data['non_pay_option']) && $data['non_pay_option'] != '') ? $data['non_pay_option'] : $model->non_pay_option;
            $model->status = (isset($data['status']) && $data['status'] != '') ? $data['status'] : $model->status;
            $model->title = (isset($data['title']) && $data['title'] != '') ? $data['title'] : $model->title;
            $model->description = (isset($data['description']) && $data['description'] != '') ? $data['description'] : $model->description;
            if (isset($_FILES['image_name']) && is_uploaded_file($_FILES['image_name']['tmp_name'])) {
                $model->image_name = $uploads = UploadedFile::getInstanceByName('image_name');
                $unique = rand(10, 500) . time();
                $model->image = 'uploads/donations/' . $unique . '.jpg';
                $model->image_name->saveAs('web/uploads/donations/' . $unique . '.jpg');
                $model->image_name = null;
            }
            if (isset($data['non_pay_option']) && $data['non_pay_option'] == 0) {
                $model->financial_assistance_other = (isset($data['financial_assistance_other']) && $data['financial_assistance_other'] != '') ? $data['financial_assistance_other'] : $model->financial_assistance_other;
                $model->financial_assistance = (isset($data['financial_assistance']) && $data['financial_assistance'] != '') ? $data['financial_assistance'] : $model->financial_assistance;
                $model->expected_cost = (isset($data['expected_cost']) && $data['expected_cost'] != '') ? $data['expected_cost'] : $model->expected_cost;
                $model->show_mail = (isset($data['show_mail']) && $data['show_mail'] != '') ? $data['show_mail'] : $model->show_mail;
                $model->way_of_donation = (isset($data['way_of_donation']) && $data['way_of_donation'] != '') ? $data['way_of_donation'] : '';
                $model->description_of_way = (isset($data['description_of_way']) && $data['description_of_way'] != '') ? $data['description_of_way'] : '';
            }
            if (isset($data['non_pay_option']) && $data['non_pay_option'] == 1) {
                $model->way_of_donation = (isset($data['way_of_donation']) && $data['way_of_donation'] != '') ? $data['way_of_donation'] : $model->way_of_donation;
                $model->description_of_way = (isset($data['description_of_way']) && $data['description_of_way'] != '') ? $data['description_of_way'] : $model->description_of_way;
                $model->financial_assistance_other = (isset($data['financial_assistance_other']) && $data['financial_assistance_other'] != '') ? $data['financial_assistance_other'] : '';
                $model->financial_assistance = (isset($data['financial_assistance']) && $data['financial_assistance'] != '') ? $data['financial_assistance'] : '';
                $model->expected_cost = (isset($data['expected_cost']) && $data['expected_cost'] != '') ? $data['expected_cost'] : '';
                if (isset($data['way_of_donation']) && $data['way_of_donation'] != '' && isset($data['description_of_way']) && $data['description_of_way'] != '') {
                    //$model->financial_assistance_other = '';
                    // $model->financial_assistance = '';
                    // $model->expected_cost = '';
                }
            }
            $model->date_updated = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                return array('status' => 1, 'message' => 'Donation Updated Sucessfully', 'id' => $model->id);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
            }
        }


    }

    public function actionWishesForHappyStory()
    {
        $user_id = $this->user_id;
        $data = Wish::find()->select(['w_id', 'wish_title'])->where(['wished_by' => $user_id])->andwhere(['!=', 'granted_by', ''])->orderBy('wish_title')->asArray()->all();
        return array('status' => 1, 'data' => $data);

    }

    public function actionSaveHappyStory()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {

            $model = new HappyStories();

        } else {
            $user_exist = HappyStories::find()->where(['hs_id' => $_POST['id']])->exists();
            if ($user_exist) {
                $model = HappyStories::find()->where(['hs_id' => $_POST['id']])->one();
            } else {

                return array('status' => 0, 'error' => $this->returnMessage('Happy Story not Found'));
            }
        }

        $data = Yii::$app->request->post();

        if (!isset($_POST['wish_id']) || $_POST['wish_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Wish Id is Mandatory.'));
        }

        if (!isset($_POST['story_text']) || $_POST['story_text'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Story Text is Mandatory.'));
        }
        if (isset($_FILES['story_image']) && is_uploaded_file($_FILES['story_image']['tmp_name'])) {
            $model->story_image = UploadedFile::getInstanceByName('story_image');
            $model->uploadImage();
        } else {
            if (isset($_POST['default_image_name']) && $_POST['default_image_name'] != '') {
                $model->story_image = $_POST['default_image_name'];
            } else {
                if(!isset($_POST['id']) || empty($_POST['id'])) {
                $model->story_image = 'images/happy-story/19.jpg';///web/images/wish_default/2.jpg
                }
            }
        }

        $model->wish_id = $data['wish_id'];
        $model->story_text = $data['story_text'];
        $model->user_id = $this->user_id;
        $model->status = 0;
        $model->created_at = date('Y-m-d H:i:s');
        if ($model->save(false)) {
            return array('status' => 1, 'message' => 'Data saved Sucessfully', 'id' => $model->hs_id);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong!'));
        }


    }

    public function actionViewWishesAndDonations()
    {
        if (!isset($_POST['type']) || $_POST['type'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        if (!isset($_POST['id']) || $_POST['id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        if ($_POST['type'] == "wish") {
            $model = Wish::find()->where(['w_id' => $_POST['id']])
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.*', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                ->asArray()
                ->one();

            if ($model !== null) {
                $model['expected_date'] = ($model['expected_date'] != '') ? date('Y-m-d', strtotime($model['expected_date'])) : '';
                $comments = WishComments::find()->where(['w_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                    ->With(['likes' => function (ActiveQuery $query) {
                        return $query->select('a_id,w_comment_id')->where(['user_id' => $this->user_id]);
                    }])
                    ->With(['commenterModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->With(['commentReply' => function (ActiveQuery $query) {
                        return $query->select('wish_comments.w_comment_id ,parent_id,comments,wish_comments.user_id,created_at')->where(['status' => 0])
                            ->With(['likes' => function (ActiveQuery $query) {
                                return $query->select('wish_comments_activities.a_id,wish_comments_activities.w_comment_id')->where(['user_id' => $this->user_id]);
                            }])
                            ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                            }]);
                    }])->select(['wish_comments.*', '(CASE WHEN (select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id and wish_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id) as total_like'])
                    ->asArray()
                    ->orderBy('w_comment_id Desc')
                    ->all();
                $isReported = Utils::checkIsReported($_POST['id'], 'wish');
                return array('status' => 1, 'data' => $model, 'comment' => $comments, 'isReported' => $isReported);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Wish not found!'));
            }
        } else {
            $model = Donation::find()->where(['id' => $_POST['id']])
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.*', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->asArray()
                ->one();

            if ($model !== null) {
                $model['expected_date'] = ($model['expected_date'] != '') ? date('Y-m-d', strtotime($model['expected_date'])) : null;
                $comments = DonationComments::find()->where(['d_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                    ->With(['likes' => function (ActiveQuery $query) {
                        return $query->select('a_id,d_comment_id')->where(['user_id' => $this->user_id]);
                    }])
                    ->With(['commenterModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->With(['commentReply' => function (ActiveQuery $query) {
                        return $query->select('donation_comments.user_id,donation_comments.d_comment_id,parent_id,comments,created_at')->where(['status' => 0])
                            ->With(['likes' => function (ActiveQuery $query) {
                                return $query->select('donation_comments_activities.a_id,donation_comments_activities.d_comment_id')->where(['user_id' => $this->user_id]);
                            }])
                            ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                            }]);
                    }])
                    ->select(['donation_comments.*', '(CASE WHEN (select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id and donation_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id) as total_like'])
                    ->asArray()
                    ->orderBy('d_comment_id Desc')->all();

                $isReported = Utils::checkIsReported($_POST['id'], 'donation');
                return array('status' => 1, 'data' => $model, 'comment' => $comments, 'isReported' => $isReported);
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Donation not found!'));
            }
        }

    }

    public function actionSaveWishComment()
    {
        if (!isset($_POST['comment']) || $_POST['comment'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('comment Field is Mandatory.'));
        }
        if (!isset($_POST['wish_id']) || $_POST['wish_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('wish_id Field is Mandatory.'));
        }

        if (!isset($_POST['id']) || empty($_POST['id'])) {

            $model = new WishComments();

        } else {
            $comment_exist = WishComments::find()->where(['w_comment_id' => $_POST['id']])->exists();
            if ($comment_exist) {
                $model = WishComments::find()->where(['w_comment_id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Wish comment not Found'));
            }
        }

        $model->w_id = $_POST['wish_id'];
        $model->comments = $_POST['comment'];
        $model->user_id = $this->user_id;
        $model->created_at = date("Y-m-d H:i:s");
        if ($model->save()) {
            $wish = Wish::findOne($_POST['wish_id']);
            if (!$wish) {
                return array('status' => 0, 'error' => $this->returnMessage('Wish not Found'));
            }

            $get_user_id = \app\models\Wish::findOne([
                'w_id' => $_POST['wish_id'],
            ]);

            $wish_creater = \app\models\UserProfile::findOne([
                'user_id' => $get_user_id->wished_by,
            ]);
            $comment_creater = \app\models\UserProfile::findOne([
                'user_id' => $this->user_id,
            ]);

            if ($wish->wished_by != \Yii::$app->user->id)
                $this->sendwishCommentEmail($wish, $_POST['comment']);


            $user1url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $this->user_id]);
            $user1 = Html::a($comment_creater->firstname, $user1url, ['style' => 'color:blue;text-decoration: underline;']);


            $wishurl = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $_POST['wish_id']]);
            $wishlink = Html::a($get_user_id->wish_title, $wishurl, ['style' => 'color:blue;text-decoration: underline;']);

            $details = "Hello " . $wish_creater->firstname . ",
                
                You’ve received a public comment on " . $wishlink . " from " . $user1 . "
                
                " . $_POST['comment'] . "";

            $msg = $details;
            if ($this->user_id != '' && $get_user_id->wished_by != '' && $msg != '') {
                $message = new Message();
                $message->sender_id = $this->user_id;
                $message->recipient_id = $get_user_id->wished_by;
                $message->parent_id = 0;
                $message->read_text = 0;
                $message->text = $msg;
                $message->created_at = date("Y-m-d H:i:s");
                $message->save();
            }
            $comment = WishComments::find()->where(['w_comment_id' => $model->w_comment_id])->one();
            return array('status' => 1, 'data' => $comment, 'message' => 'Comment save successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went Wrong!'));
        }
    }

    public function actionSaveDonationComment()
    {
        if (!isset($_POST['comment']) || $_POST['comment'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('comment Field is Mandatory.'));
        }
        if (!isset($_POST['donation_id']) || $_POST['donation_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('donation_id Field is Mandatory.'));
        }

        if (!isset($_POST['id']) || empty($_POST['id'])) {

            $model = new DonationComments();

        } else {
            $comment_exist = DonationComments::find()->where(['d_comment_id' => $_POST['id']])->exists();
            if ($comment_exist) {
                $model = DonationComments::find()->where(['d_comment_id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Donation comment not Found'));
            }
        }

        $model->d_id = $_POST['donation_id'];
        $model->comments = $_POST['comment'];
        $model->user_id = $this->user_id;
        $model->created_at = date("Y-m-d H:i:s");
        if ($model->save()) {
            $donation = Donation::findOne($_POST['donation_id']);
            if (!$donation) {
                return array('status' => 0, 'error' => $this->returnMessage('Donation not Found'));
            }

            $get_user_id = \app\models\Donation::findOne([
                'id' => $_POST['donation_id'],
            ]);

            $wish_creater = \app\models\UserProfile::findOne([
                'user_id' => $get_user_id->created_by,
            ]);
            $comment_creater = \app\models\UserProfile::findOne([
                'user_id' => $this->user_id,
            ]);


            $user1url = Yii::$app->urlManager->createAbsoluteUrl(['account/profile', 'id' => $this->user_id]);
            $user1 = Html::a($comment_creater->firstname, $user1url, ['style' => 'color:blue;text-decoration: underline;']);


            $donationurl = Yii::$app->urlManager->createAbsoluteUrl(['donation/view', 'id' => $_POST['donation_id']]);
            $donationlink = Html::a($get_user_id->title, $donationurl, ['style' => 'color:blue;text-decoration: underline;']);


            $details = "Hello " . $wish_creater->firstname . ",
                
                You’ve received a public comment on " . $donationlink . " from " . $user1 . " 
                     
                " . $_POST['comment'];

            $msg = $details;
            if ($this->user_id != '' && $get_user_id->created_by != '' && $msg != '') {
                $message = new Message();
                $message->sender_id = $this->user_id;
                $message->recipient_id = $get_user_id->created_by;
                $message->parent_id = 0;
                $message->read_text = 0;
                $message->text = $msg;
                $message->created_at = date("Y-m-d H:i:s");
                $message->save();

            }
            $comment = DonationComments::find()->where(['d_comment_id' => $model->d_comment_id])->one();
            return array('status' => 1, 'data' => $comment, 'message' => 'Comment save successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went Wrong!'));
        }
    }

    public function actionViewForum()
    {

        if (!isset($_POST['id']) || $_POST['id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        $model = Editorial::find()->where(['e_id' => $_POST['id']])
            ->select(['editorial.*'])
            ->asArray()
            ->one();

        if ($model !== null) {
            $comments = EditorialComments::find()->where(['e_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                ->With(['likes' => function (ActiveQuery $query) {
                    return $query->select('a_id,e_comment_id')->where(['user_id' => $this->user_id]);
                }])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select('editorial_comments.e_comment_id,editorial_comments.parent_id,comments,editorial_comments.user_id,editorial_comments.created_at,editorial_comments.status')->where(['editorial_comments.status' => 0])
                        ->With(['likes' => function (ActiveQuery $query) {
                            return $query->select('editorial_comment_activities.a_id,editorial_comment_activities.w_comment_id')->where(['user_id' => $this->user_id]);
                        }])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])
                ->asArray()
                ->orderBy('e_comment_id Desc')
                ->all();
            $isReported = Utils::checkIsReported($_POST['id'], 'editorial');
            return array('status' => 1, 'data' => $model, 'comment' => $comments, 'isReported' => $isReported);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Wish not found!'));
        }


    }

    public function actionSaveForumComment()
    {
        if (!isset($_POST['comment']) || $_POST['comment'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('comment Field is Mandatory.'));
        }
        if (!isset($_POST['forum_id']) || $_POST['forum_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('forum_id Field is Mandatory.'));
        }
        $forumdetails = Editorial::findOne($_POST['forum_id']);
        if (empty($forumdetails)) {
            return array('status' => 0, 'error' => $this->returnMessage('Forum details not found.'));
        }
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            $model = new EditorialComments();
        } else {
            $comment_exist = EditorialComments::find()->where(['e_comment_id' => $_POST['id']])->exists();
            if ($comment_exist) {
                $model = EditorialComments::find()->where(['e_comment_id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Forum comment not Found'));
            }
        }

        $model->e_id = $_POST['forum_id'];
        $model->comments = $_POST['comment'];
        $model->user_id = $this->user_id;
        $model->created_at = date("Y-m-d H:i:s");
        if ($model->save()) {
            $comment = EditorialComments::find()->where(['e_comment_id' => $model->e_comment_id])->one();
            return array('status' => 1, 'data' => $comment, 'message' => 'Comment save successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went Wrong!'));
        }
    }

    public function actionSaveHappyStoryComment()
    {
        if (!isset($_POST['comment']) || $_POST['comment'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('comment Field is Mandatory.'));
        }
        if (!isset($_POST['happy_story_id']) || $_POST['happy_story_id'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('happy_story_id Field is Mandatory.'));
        }
        $forumdetails = HappyStories::findOne($_POST['happy_story_id']);
        if (empty($forumdetails)) {
            return array('status' => 0, 'error' => $this->returnMessage('Happy Story details not found.'));
        }
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            $model = new HappyStoriesComments();
        } else {
            $comment_exist = HappyStoriesComments::find()->where(['hs_comment_id' => $_POST['id']])->exists();
            if ($comment_exist) {
                $model = HappyStoriesComments::find()->where(['hs_comment_id' => $_POST['id']])->one();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Happy Story comment not Found'));
            }
        }

        $model->hs_id = $_POST['happy_story_id'];
        $model->comments = $_POST['comment'];
        $model->user_id = $this->user_id;
        $model->created_at = date("Y-m-d H:i:s");
        if ($model->save(false)) {
            $comment = HappyStoriesComments::find()->where(['hs_comment_id' => $model->hs_comment_id])->one();
            return array('status' => 1, 'data' => $comment, 'message' => 'Comment save successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Somthing went Wrong!'));
        }
    }

    public function actionDeleteWishAndDonation()
    {

        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        if ($_POST['type'] == 'wish') {
            $happystories = HappyStories::find()->where(['wish_id' => $_POST['id']])->one();
            if (!empty($happystories)) {
                $happystories->delete();
            }
            $wish = Wish::findOne($_POST['id']);
            if ($wish) {
                $wish->delete();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Wish not found!'));
            }
            return array('status' => 1, 'message' => 'Wish delete successfully');
        } else {
            $donation = Donation::findOne($_POST['id']);
            if ($donation) {
                $donation->delete();
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Donation not found!'));
            }
            return array('status' => 1, 'message' => 'Donation delete successfully');
        }
    }

    public function actionDeleteWishAndDonationComment()
    {
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        if ($_POST['type'] == 'wish') {
            $model = WishComments::findOne($_POST['id']);
            if ($model) {
                $model->status = 1;

                if ($model->parent_id == 0) {
                    $replycomments = WishComments::find()->where(['parent_id' => $_POST['id']])->all();

                    foreach ($replycomments as $reply) {
                        $reply->status = 1;
                        $reply->save(false);
                    }
                }
                $model->save(false);
                return array('status' => 1, 'message' => 'Comment delete successfully');
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
            }
        } else {
            $model = DonationComments::findOne($_POST['id']);
            if ($model) {
                $model->status = 1;

                if ($model->parent_id == 0) {
                    $replycomments = DonationComments::find()->where(['parent_id' => $_POST['id']])->all();

                    foreach ($replycomments as $reply) {
                        $reply->status = 1;
                        $reply->save(false);
                    }
                }
                $model->save(false);
                return array('status' => 1, 'message' => 'Comment delete successfully');
            } else {
                return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
            }
        }
    }

    public function actionDeleteForum()
    {

        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        $forum = Editorial::findOne($_POST['id']);
        if ($forum) {
            $forumcomments = EditorialComments::find()->where(['e_id' => $_POST['id']])->all();
            if (!empty($forumcomments)) {
                foreach ($forumcomments as $forumcomment) {
                    $forumcomment->delete();
                }
            }
            $forumlikes = EditorialActivity::find()->where(['e_id' => $_POST['id']])->all();
            if (!empty($forumlikes)) {
                foreach ($forumlikes as $forumlike) {
                    $forumlike->delete();
                }
            }
            $forum->delete();
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Forum not found!'));
        }
        return array('status' => 1, 'message' => 'Forum deleted successfully');

    }

    public function actionDeleteForumComment()
    {

        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        $model = EditorialComments::findOne($_POST['id']);
        if ($model) {
            $model->status = 1;

            if ($model->parent_id == 0) {
                $replycomments = EditorialComments::find()->where(['parent_id' => $_POST['id']])->all();

                foreach ($replycomments as $reply) {
                    $reply->status = 1;
                    $reply->save(false);
                }
            }
            $model->save(false);
            return array('status' => 1, 'message' => 'Comment delete successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
        }

    }

    public function actionDeleteHappyStory()
    {

        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        $happystory = HappyStories::findOne($_POST['id']);
        if ($happystory) {
            $forumcomments = HappyStoriesComments::find()->where(['hs_id' => $_POST['id']])->all();
            if (!empty($forumcomments)) {
                foreach ($forumcomments as $forumcomment) {
                    $forumcomment->delete();
                }
            }
            $forumlikes = StoryActivity::find()->where(['story_id' => $_POST['id']])->all();
            if (!empty($forumlikes)) {
                foreach ($forumlikes as $forumlike) {
                    $forumlike->delete();
                }
            }
            $happystory->delete();
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Happy Story not found!'));
        }
        return array('status' => 1, 'message' => 'Happy Story deleted successfully');

    }

    public function actionDeleteHappyStoryComment()
    {

        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        $model = HappyStoriesComments::findOne($_POST['id']);
        if ($model) {
            $model->status = 1;

            if ($model->parent_id == 0) {
                $replycomments = HappyStoriesComments::find()->where(['parent_id' => $_POST['id']])->all();

                foreach ($replycomments as $reply) {
                    $reply->status = 1;
                    $reply->save(false);
                }
            }
            $model->save(false);
            return array('status' => 1, 'message' => 'Comment delete successfully');
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
        }

    }

    public function actionCheckmywishesdonations()
    {
        $user_id = $this->user_id;
        $properties = Utils::getUserProperties($user_id);
        $data['can_create_wish'] = ($properties['can_create_wish'] == 1) ? 1 : 0;
        $data['can_create_donation'] = ($properties['can_create_donation'] == 1) ? 1 : 0;
        return array('status' => 1, 'message' => $data);
    }

    public function actionMyProfile()
    {
        $data['user'] = User::findOne($this->user_id);
        if ($data['user']) {
            $user_id = $this->user_id;
            $userexist = User::find()
                ->with('userProfile')
                ->where([
                    'id' => $this->user_id,
                ])->asArray()->one();
            $headers = Yii::$app->request->headers;
            $token = $headers['token'];
            $profile = UserProfile::find()->select(['user_profile.*', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname'])->leftJoin('countries', 'countries.id = user_profile.country')
                ->leftJoin('states', 'states.id = user_profile.state')
                ->leftJoin('cities', 'cities.id = user_profile.city')->where(['user_id' => $this->user_id])->asArray()->one();


            $userexist['countryname'] = $profile['countryname'];
            $userexist['statename'] = $profile['statename'];
            $userexist['cityname'] = $profile['cityname'];
            // $properties = Utils::getUserProperties($user->id);
            return array('status' => 1, 'data' => $userexist, 'token' => $token);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('User not found!'));
        }
    }

    public function actionMyWishesAndDonations()
    {
        $user_id = $this->user_id;
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        $type = $_POST['type'];
        $data = array();
        if ($type == 'active') {
            $wishesquery = Wish::find()->where(['wished_by' => $user_id, 'granted_by' => null, 'wish_status' => 0, 'process_status' => 0])
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                ->orderBy('w_id DESC');
            //->groupBy('w_id');
            $wishes = $wishesquery->asArray()
                ->all();

            $donationsquery = Donation::find()
                ->where(['created_by' => $user_id, 'granted_by' => null, 'status' => 0, 'process_status' => 0])
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->orderBy('id DESC');

            $donations = $donationsquery->asArray()
                ->all();
            $data = array_merge($donations, $wishes);
        } else if ($type == 'inprogress') {
            if (isset($_POST['subtype']) && $_POST['subtype'] == 'inprogress_by_me') {
                $inprogresswishesquery = Wish::find()->where(['process_granted_by' => $user_id, 'wish_status' => 0, 'process_status' => 1])
                    ->andWhere(['granted_by' => null])
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->orderBy('w_id DESC')
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

                $inprogresswishes = $inprogresswishesquery->asArray()
                    ->all();
                $inprogressdonationsquery = Donation::find()
                    ->where(['process_granted_by' => $user_id, 'status' => 0, 'process_status' => 1])
                    ->andWhere(['granted_by' => null])
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->orderBy('id DESC')
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);

                $inprogressdonations = $inprogressdonationsquery->asArray()
                    ->all();
                $allinprogressdata = array_merge($inprogressdonations, $inprogresswishes);
                $data = $allinprogressdata;
            } else {
                $progresswishesquery = Wish::find()->where(['wished_by' => $user_id, 'wish_status' => 0, 'process_status' => 1])
                    ->andWhere(['granted_by' => null])
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->orderBy('w_id DESC')
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

                $progresswishes = $progresswishesquery->asArray()
                    ->all();
                $progressdonationsquery = Donation::find()
                    ->where(['created_by' => $user_id, 'status' => 0, 'process_status' => 1])
                    ->andWhere(['granted_by' => null])
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->orderBy('id DESC')
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);

                $progressdonations = $progressdonationsquery->asArray()
                    ->all();
                $allprogressdata = array_merge($progressdonations, $progresswishes);
                $data = $allprogressdata;
            }
        } else if ($type == 'granted') {
            if (isset($_POST['subtype']) && $_POST['subtype'] == 'granted_by_me') {
                $grantedwishesquery = Wish::find()->where(['granted_by' => $user_id, 'wish_status' => 0, 'process_status' => 0])
                    ->andWhere(['not', ['granted_by' => null]])
                    ->orderBy('w_id DESC')
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

                $grantedwishes = $grantedwishesquery->asArray()
                    ->all();
                $granteddonationsquery = Donation::find()
                    ->where(['granted_by' => $user_id, 'status' => 0, 'process_status' => 0])
                    ->andWhere(['not', ['granted_by' => null]])
                    ->orderBy('id DESC')
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);

                $granteddonations = $granteddonationsquery->asArray()
                    ->all();
                $allgranteddata = array_merge($granteddonations, $grantedwishes);


                $data = $allgranteddata;
            } else {
                $processwishesquery = Wish::find()->where(['wished_by' => $user_id, 'wish_status' => 0, 'process_status' => 0])
                    ->andWhere(['not', ['granted_by' => null]])
                    ->orderBy('w_id DESC')
                    ->With(['wisherModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

                $processwishes = $processwishesquery->asArray()
                    ->all();

                $processdonationsquery = Donation::find()
                    ->where(['created_by' => $user_id, 'status' => 0, 'process_status' => 0])
                    ->andWhere(['not', ['granted_by' => null]])
                    ->orderBy('id DESC')
                    ->With(['donorModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])
                    ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);
                $processdonations = $processdonationsquery->asArray()
                    ->all();
                $allprocessdata = array_merge($processdonations, $processwishes);
                $data = $allprocessdata;
            }
        } else if ($type == 'saved') {


            $savedwishesquery = Wish::find()->where(['activities.user_id' => $user_id])->orderBy('wishes.w_id DESC')
                ->innerJoinWith(['savedw as activities'])
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->groupBy('w_id')
                ->orderBy('w_id DESC')
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

            $savedwishes = $savedwishesquery->asArray()
                ->all();
            $savedsdonationsquery = Donation::find()
                ->where(['activities.user_id' => $user_id])->orderBy('donations.id DESC')
                ->innerJoinWith(['saveddonation as activities'])
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->groupBy('id')
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);

            $saveddonations = $savedsdonationsquery->asArray()
                ->all();
            $data = array_merge($saveddonations, $savedwishes);
        }
        return array('status' => 1, 'data' => $data);
    }

    public function actionUserProfile()
    {
        if (!isset($_POST['user_id']) || empty($_POST['user_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('user_id Field is Mandatory.'));
        }
        $user_id = $_POST['user_id'];
        $data['user'] = User::findOne($_POST['user_id']);
        if ($data['user']) {
            $data['profile'] = UserProfile::find()->select(['user_profile.*','user.sendbird_user_id', 'countries.name as countryname', 'states.name as statename', 'cities.name as cityname'])
                ->innerJoin('user','user.id = user_profile.user_id')
                ->leftJoin('countries', 'countries.id = user_profile.country')
                ->leftJoin('states', 'states.id = user_profile.state')
                ->leftJoin('cities', 'cities.id = user_profile.city')
                ->where(['user_id' => $_POST['user_id']])
                ->asArray()->one();
            if( $data['profile']['countryname'] == null) {
                $data['profile']['countryname'] = '';
            }
            if( $data['profile']['statename'] == null) {
                $data['profile']['statename'] = '';
            }
            if( $data['profile']['cityname'] == null) {
                $data['profile']['cityname'] = '';
            }
            //echo "<pre>";print_r($profile);exit;
            $checkfollowlist = FriendRequest::find()->where("(requested_by = " . $this->user_id . " AND requested_to = " . $user_id . ") OR (requested_by = " . $user_id . " AND requested_to = " . $this->user_id . ")")->orderBy(['f_id' => SORT_DESC])->one();
            if (!empty($checkfollowlist)) {
                $data['friend_status'] = $checkfollowlist->status;
                $data['f_id'] = $checkfollowlist->f_id;
            } else {
                $data['friend_status'] = 2;
                $data['f_id'] = 0;
            }
            $wishesquery = Wish::find()->where(['wished_by' => $user_id, 'granted_by' => null, 'wish_status' => 0, 'process_status' => 0])
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like'])
                //->joinWith(['likes as activities'])
                ->orderBy('createDate DESC');
            //->groupBy('w_id');
            $wishes = $wishesquery->asArray()
                ->all();
            $donationsquery = Donation::find()
                ->where(['created_by' => $user_id, 'granted_by' => null, 'status' => 0, 'process_status' => 0])
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like'])
                ->orderBy('createDate DESC');

            $donations = $donationsquery->asArray()
                ->all();
            $data['activewishesdonations'] = array_merge($wishes, $donations);
            $grantedwishesquery = Wish::find()->where(['wished_by' => $user_id, 'wish_status' => 0, 'process_status' => 0])
                ->andWhere(['not', ['granted_by' => null]])
                ->With(['wisherModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['wishes.w_id', 'wishes.primary_image', 'wishes.wish_title', 'wishes.wished_by', 'wishes.created_at as createDate', 'wishes.granted_date', new Expression("'wish' AS type"), '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.wish_id = wishes.w_id and activities.activity="like") as total_like']);

            $grantedwishes = $grantedwishesquery->asArray()
                ->all();
            $granteddonationsquery = Donation::find()
                ->where(['created_by' => $user_id, 'status' => 0, 'process_status' => 0])
                ->andWhere(['not', ['granted_by' => null]])
                ->With(['donorModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->select(['donations.id', 'donations.image', 'donations.title', 'donations.created_by', 'donations.created_at as createDate', 'donations.granted_date', new Expression("'donation' AS type"), '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="like" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from activities where activities.donation_id = donations.id and activities.activity="fav" and activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from activities where activities.donation_id = donations.id and activities.activity="like") as total_like']);

            $granteddonations = $granteddonationsquery->asArray()
                ->all();
            $data['grantedwishesdonations'] = array_merge($grantedwishes, $granteddonations);
            return array('status' => 1, 'data' => $data);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('User not found!'));
        }
    }
    

    public function actionLikeComment()
    {
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }

        if ($_POST['type'] == "wish" || $_POST['type'] == "donation" || $_POST['type'] == 'forum' || $_POST['type'] == 'happy_story') {
            if ($_POST['type'] == "wish") {
                $comment = WishComments::find()->where(['w_comment_id' => $_POST['id']])->one();
                if ($comment) {
                    $activity = WishCommentsActivities::find()->where(['w_comment_id' => $comment->w_comment_id, 'user_id' => $this->user_id])->one();
                    if ($activity != null) {
                        $activity->delete();
                        return array('status' => 1, 'message' => 'Like remove successfully');
                    }
                    $activity = new WishCommentsActivities();
                    $activity->w_comment_id = $comment->w_comment_id;
                    $activity->user_id = $this->user_id;

                    if ($activity->save()) {
                        return array('status' => 1, 'message' => 'Like successfully');
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Something went wrong!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
                }

            } else if ($_POST['type'] == "donation") {
                $comment = DonationComments::find()->where(['d_comment_id' => $_POST['id']])->one();
                if ($comment) {
                    $activity = DonationCommentsActivities::find()->where(['d_comment_id' => $comment->d_comment_id, 'user_id' => $this->user_id])->one();
                    if ($activity != null) {
                        $activity->delete();
                        return array('status' => 1, 'message' => 'Like remove successfully');
                    }
                    $activity = new DonationCommentsActivities();
                    $activity->d_comment_id = $comment->d_comment_id;
                    $activity->user_id = $this->user_id;

                    if ($activity->save()) {
                        return array('status' => 1, 'message' => 'Like successfully');
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Something went wrong!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
                }
            } else if ($_POST['type'] == "forum") {
                $comment = EditorialComments::find()->where(['e_comment_id' => $_POST['id']])->one();
                if ($comment) {
                    $activity = EditorialCommentActivity::find()->where(['e_comment_id' => $comment->e_comment_id, 'user_id' => $this->user_id])->one();
                    if ($activity != null) {
                        $activity->delete();
                        return array('status' => 1, 'message' => 'Like remove successfully');
                    }
                    $activity = new EditorialCommentActivity();
                    $activity->e_comment_id = $comment->e_comment_id;
                    $activity->user_id = $this->user_id;

                    if ($activity->save(false)) {
                        return array('status' => 1, 'message' => 'Like successfully');
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Something went wrong!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
                }
            } else if ($_POST['type'] == "happy_story") {
                $comment = HappyStoriesComments::find()->where(['hs_comment_id' => $_POST['id']])->one();
                if ($comment) {
                    $activity = HappyStoriesCommentActivity::find()->where(['hs_comment_id' => $comment->hs_comment_id, 'user_id' => $this->user_id])->one();
                    if ($activity != null) {
                        $activity->delete();
                        return array('status' => 1, 'message' => 'Like remove successfully');
                    }
                    $activity = new HappyStoriesCommentActivity();
                    $activity->hs_comment_id = $comment->hs_comment_id;
                    $activity->user_id = $this->user_id;

                    if ($activity->save(false)) {
                        return array('status' => 1, 'message' => 'Like successfully');
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Something went wrong!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Comment not found!'));
                }
            }

        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Type not Found!'));
        }
    }

    public function actionCommentReply()
    {
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type Field is Mandatory.'));
        }
        /*donation or wish id*/
        if (!isset($_POST['type_id']) || empty($_POST['type_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('type_id Field is Mandatory.'));
        }
        if (!isset($_POST['parent_id']) || empty($_POST['parent_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('parent_id  Field is Mandatory.'));
        }
        if (!isset($_POST['comment']) || empty($_POST['comment'])) {
            return array('status' => 0, 'error' => $this->returnMessage('comment Field is Mandatory.'));
        }

        if ($_POST['type'] == "wish" || $_POST['type'] == "donation" || $_POST['type'] == "forum" || $_POST['type'] == "happy_story") {
            if ($_POST['type'] == "wish") {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model = new WishComments();
                } else {
                    $comment_exist = WishComments::find()->where(['w_comment_id' => $_POST['id']])->exists();
                    if ($comment_exist) {
                        $model = WishComments::find()->where(['w_comment_id' => $_POST['id']])->one();
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Comment not Found'));
                    }
                }
                $model->w_id = $_POST['type_id'];
                $model->parent_id = $_POST['parent_id'];
                $model->comments = $_POST['comment'];
                $model->user_id = $this->user_id;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $lastcomment = WishComments::find()->where(['w_comment_id' => $model->w_comment_id, 'status' => 0])
                        ->With(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_id,firstname,lastname,profile_image');
                        }])
                        ->With(['commentReply' => function (ActiveQuery $query) {
                            return $query->select(['wish_comments.w_comment_id', 'wish_comments.parent_id', 'wish_comments.comments', 'wish_comments.user_id', 'wish_comments.created_at', '(CASE WHEN (select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id and wish_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id) as total_like'])
                                ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                    return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                                }]);
                        }])->select(['wish_comments.*', '(CASE WHEN (select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id and wish_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id) as total_like'])
                        ->asArray()->one();

                    return array('status' => 1, 'message' => 'Data save successfully', 'data' => $lastcomment);
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong'));
                }
            } else if ($_POST['type'] == "donation") {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model = new DonationComments();
                } else {
                    $comment_exist = DonationComments::find()->where(['d_comment_id' => $_POST['id']])->exists();
                    if ($comment_exist) {
                        $model = DonationComments::find()->where(['d_comment_id' => $_POST['id']])->one();
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Comment not Found'));
                    }
                }
                $model->d_id = $_POST['type_id'];
                $model->parent_id = $_POST['parent_id'];
                $model->comments = $_POST['comment'];
                $model->user_id = $this->user_id;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $lastcomment = DonationComments::find()->where(['d_comment_id' => $model->d_comment_id, 'status' => 0])
                        ->With(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_id,firstname,lastname,profile_image');
                        }])
                        ->With(['commentReply' => function (ActiveQuery $query) {
                            return $query->select(['donation_comments.user_id', 'donation_comments.d_comment_id', 'parent_id', 'comments', 'created_at', '(CASE WHEN (select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id and donation_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id) as total_like'])
                                ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                    return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                                }]);
                        }])->select(['donation_comments.*', '(CASE WHEN (select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id and donation_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id) as total_like'])
                        ->asArray()->one();
                    return array('status' => 1, 'message' => 'Data save successfully', 'data' => $lastcomment);
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Somthing went wrong'));
                }
            } else if ($_POST['type'] == "forum") {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model = new EditorialComments();
                } else {
                    $comment_exist = EditorialComments::find()->where(['e_comment_id' => $_POST['id']])->exists();
                    if ($comment_exist) {
                        $model = EditorialComments::find()->where(['e_comment_id' => $_POST['id']])->one();
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Comment not Found'));
                    }
                }
                $model->e_id = $_POST['type_id'];
                $model->parent_id = $_POST['parent_id'];
                $model->comments = $_POST['comment'];
                $model->user_id = $this->user_id;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $lastcomment = EditorialComments::find()
                        ->where(['e_comment_id' => $model->e_comment_id, 'status' => 0])
                        ->With(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_id,firstname,lastname,profile_image');
                        }])
                        ->With(['commentReply' => function (ActiveQuery $query) {
                            return $query->select(['e_comment_id', 'parent_id', 'comments', 'editorial_comments.user_id', '(CASE WHEN (select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id and editorial_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id) as total_like'])
                                ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                    return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                                }]);
                        }])
                        ->select(['editorial_comments.*', '(CASE WHEN (select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id and editorial_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id) as total_like'])
                        ->asArray()->one();
                    return array('status' => 1, 'message' => 'Data save successfully', 'data' => $lastcomment);
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Something went wrong'));
                }
            } else if ($_POST['type'] == "happy_story") {
                if (!isset($_POST['id']) || empty($_POST['id'])) {
                    $model = new HappyStoriesComments();
                } else {
                    $comment_exist = HappyStoriesComments::find()->where(['hs_comment_id' => $_POST['id']])->exists();
                    if ($comment_exist) {
                        $model = HappyStoriesComments::find()->where(['hs_comment_id' => $_POST['id']])->one();
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Comment not Found'));
                    }
                }
                $model->hs_id = $_POST['type_id'];
                $model->parent_id = $_POST['parent_id'];
                $model->comments = $_POST['comment'];
                $model->user_id = $this->user_id;
                $model->created_at = date('Y-m-d H:i:s');
                if ($model->save()) {
                    $lastcomment = HappyStoriesComments::find()
                        ->where(['hs_comment_id' => $model->hs_comment_id, 'status' => 0])
                        ->With(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_id,firstname,lastname,profile_image');
                        }])
                        ->With(['commentReply' => function (ActiveQuery $query) {
                            return $query->select(['hs_comment_id', 'parent_id', 'comments', 'happy_stories_comments.user_id', '(CASE WHEN (select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id and happy_stories_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id) as total_like'])
                                ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                                    return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                                }]);
                        }])
                        ->select(['happy_stories_comments.*', '(CASE WHEN (select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id and happy_stories_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id) as total_like'])
                        ->asArray()->one();
                    return array('status' => 1, 'message' => 'Data save successfully', 'data' => $lastcomment);
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('Something went wrong'));
                }
            }

        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Type not Found!'));
        }
    }

    public function actionProcessWish()
    {

        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        $send_message = 1;
        $model = Wish::findOne($_POST['id']);


        if ($model) {
            if ($model->wished_by == $this->user_id) {
                return array('status' => 0, 'error' => $this->returnMessage('This is your wish.'));
            }
            //explicitly set up the granted_by to the user id
            //listen to the IPN and change back to NULL if not success.
            $model->process_granted_by = $this->user_id;
            $model->process_granted_date = date('Y-m-d H:i:s');
            $model->process_status = 1;

            if ($model->save(false)) {
                if ($send_message == 1)
                    if ($model->non_pay_option == 0) {
                        $result = $model->sendGrantWishFinancialEmail($model->process_granted_by, $model);
                    } else {
                        $result = $model->sendGrantWishNonFinancialEmail($model->process_granted_by, $model);
                    }
                $res = $this->sendEmailWish($model);
                return array('status' => 1, 'message' => 'Wish granted successfully');
            }
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Wish not Found!'));
        }
    }

    public function actionProcessDonation()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        $send_message = 1;

        $donation = Donation::findOne($_POST['id']);
        if ($donation) {
            if ($donation->created_by == $this->user_id) {
                return array('status' => 0, 'error' => $this->returnMessage('This is your donation.'));
            }
            //explicitly set up the granted_by to the user id
            //listen to the IPN and change back to NULL if not success.
            $donation->process_granted_by = $this->user_id;
            $donation->process_granted_date = date('Y-m-d H:i:s');
            //$donation->granted_date = date('m-d-Y');

            $donation->process_status = 1;


            if ($donation->save(false)) {
                if ($send_message == 1) {

                    if ($donation->non_pay_option == 0) {
                        $result = $donation->sendFinancialAcceptDonationEmail($donation->process_granted_by, $donation);
                    } else {
                        $result = $donation->sendAcceptDonationEmail($donation->process_granted_by, $donation);
                    }
                }
                // print_r($result);
                $res = $this->sendEmailDonation($donation);
                return array('status' => 1, 'message' => 'Donation granted successfully');
            }
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Donation not Found!'));
        }

    }

    public function actionFullfillWish()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        $wish_exist = Wish::find()->where(['w_id' => $_POST['id']])->one();
        if ($wish_exist['wished_by'] == $this->user_id) {
            if ($wish_exist['granted_by'] == Null && $wish_exist['process_granted_by'] == Null) {
                return array('status' => 0, 'error' => $this->returnMessage('This Wish is not granted!'));
            } else {
                if ($wish_exist['granted_by'] == Null && $wish_exist['process_granted_by'] != Null) {
                    if ($wish_exist) {
                        $userexist = User::find()->where(['id' => $wish_exist['wished_by']])->one();
                        if ($userexist->role == 'admin')
                            $wish = Wish::find()->where(['w_id' => $_POST['id']])->one();
                        else
                            $wish = Wish::find()->where(['w_id' => $_POST['id'], 'wished_by' => $this->user_id])->one();
                        if ($wish) {
                            $wish->granted_by = $wish->process_granted_by;
                            $wish->granted_date = date('Y-m-d H:i:s');
                            $wish->process_granted_by = "";
                            //$wish->process_granted_date = "00-00-0000";
                            $wish->process_status = 0;

                            if ($wish->save(false)) {
                                $this->sendEmailToGranter($wish->wished_by, $wish);
                                $this->sendEmailToWisher($wish->granted_by, $wish);
                                //return $this->redirect(['wish/view','id'=>$w_id]);
                                $model = Wish::findOne($_POST['id']);

                                $resetLink1 = Yii::$app->urlManager->createAbsoluteUrl(['wish/view', 'id' => $model->w_id]);
                                $profilelink1 = Html::a($model->wish_title, $resetLink1, ['style' => 'color: blue;text-decoration: underline;']);

                                $happystories = Yii::$app->urlManager->createAbsoluteUrl(['happy-stories/index']);
                                $url = Html::a('Happy Stories', $happystories, ['style' => 'color: blue;text-decoration: underline;']);
                                $details = "Congratulations! Your wish ," . $profilelink1 . " has been fulfilled and is now considered granted.
                            Consider sharing your story on our " . $url . " .";
                                $msg = $details;

                                if ($this->user_id != '' && $model->granted_by != '' && $msg != '') {
                                    $message = new Message();
                                    $message->sender_id = $model->granted_by;
                                    $message->recipient_id = $this->user_id;
                                    $message->parent_id = 0;
                                    $message->read_text = 0;
                                    $message->text = $msg;
                                    $message->created_at = date("Y-m-d H:i:s");
                                    if ($message->save()) {
                                        return array('status' => 1, 'message' => 'Wish fulfilled successfully');
                                    }
                                }
                            }
                        }
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('Wish not found!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('This wish already fulfilled!'));
                }
            }
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('This is not your wish!'));
        }
    }

    public function actionAcceptDonation()
    {
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('id Field is Mandatory.'));
        }
        $donation_exist = Donation::find()->where(['id' => $_POST['id']])->one();
        if ($donation_exist['created_by'] == $this->user_id) {
            if ($donation_exist['granted_by'] == Null && $donation_exist['process_granted_by'] == Null) {
                return array('status' => 0, 'error' => $this->returnMessage('This Wish is not granted!'));
            } else {
                if ($donation_exist['granted_by'] == Null && $donation_exist['process_granted_by'] != Null) {
                    if ($donation_exist) {
                        $userexist = User::find()->where(['id' => $donation_exist['created_by']])->one();
                        if ($userexist->role == 'admin')
                            $donation = Donation::find()->where(['id' => $_POST['id']])->one();
                        else
                            $donation = Donation::find()->where(['id' => $_POST['id'], 'created_by' => $this->user_id])->one();

                        if ($donation) {
                            $donation->granted_by = $donation->process_granted_by;
                            $donation->granted_date = date('Y-m-d H:i:s');
                            $donation->process_granted_by = "";
                            //$donation->process_granted_date = date('Y-m-d');
                            $donation->process_status = 0;

                            // print_r($donation);
                            if ($donation->save(false)) {
                                $this->sendEmailToAccepted($donation->granted_by, $donation);
                                $this->sendEmailToCompleteDonation($donation->created_by, $donation);

                                return array('status' => 1, 'message' => 'Donation accepted successfully');

                            }
                        }
                    } else {
                        return array('status' => 0, 'error' => $this->returnMessage('donation not found!'));
                    }
                } else {
                    return array('status' => 0, 'error' => $this->returnMessage('This donation already fulfilled!'));
                }
            }
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('This is not your donation!'));
        }
    }


    public function actionForums()
    {
        $offset = (isset($_POST['page']) && $_POST['page'] != '') ? $_POST['page'] * 10 : 0;
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        $page = $offset + 10;
        $forums = Editorial::find()
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->select(['editorial.*', '(CASE WHEN (select count(*) from editorial_activities where editorial_activities.e_id = editorial.e_id and editorial_activities.activity="like" and editorial_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_activities where editorial_activities.e_id = editorial.e_id and editorial_activities.activity="like") as total_like']);
        if ($type == 'article') {
            $forums->where(['is_video_only' => 0]);
        } else if ($type == 'video') {
            $forums->where(['is_video_only' => 1]);
        }
        $forums->orderBy('e_id DESC');
        $totalcount = $forums->count();
        $data = $forums->limit($page)
            ->offset($offset)->asArray()
            ->all();

        return array('status' => 1, 'totalpages' => ceil($totalcount / 10), 'data' => $data);
    }

    public function actionAllcomments()
    {
        $offset = (isset($_POST['page']) && $_POST['page']) ? $_POST['page'] * 10 : 0;
        $page = $offset + 10;
        if (!isset($_POST['id']) || empty($_POST['id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Id Field is Mandatory.'));
        }
        if (!isset($_POST['post_type']) || empty($_POST['post_type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Post type Field is Mandatory.'));
        }
        if ($_POST['post_type'] == 'wish') {
            $commentsquery = WishComments::find()->where(['w_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select(['wish_comments.w_comment_id', 'wish_comments.parent_id', 'wish_comments.comments', 'wish_comments.user_id', 'wish_comments.created_at', '(CASE WHEN (select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id and wish_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id) as total_like'])
                        ->where(['wish_comments.status' => 0])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])->select(['wish_comments.*', '(CASE WHEN (select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id and wish_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from wish_comments_activities where wish_comments_activities.w_comment_id = wish_comments.w_comment_id) as total_like']);;
            $totalcountcomments = $commentsquery->count();
            $comments = $commentsquery->limit($page)
                ->offset($offset)->orderBy('w_comment_id Desc')->asArray()
                ->all();
        } else if ($_POST['post_type'] == 'donation') {
            $commentsquery = DonationComments::find()->where(['d_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select(['donation_comments.user_id', 'donation_comments.d_comment_id', 'parent_id', 'comments', 'donation_comments.created_at', '(CASE WHEN (select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id and donation_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id) as total_like'])
                        ->where(['donation_comments.status' => 0])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])->select(['donation_comments.*', '(CASE WHEN (select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id and donation_comments_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from donation_comments_activities where donation_comments_activities.d_comment_id = donation_comments.d_comment_id) as total_like']);;
            $totalcountcomments = $commentsquery->count();
            $comments = $commentsquery->limit($page)
                ->offset($offset)->orderBy('d_comment_id Desc')->asArray()
                ->all();
        } else if ($_POST['post_type'] == 'forum') {
            $commentsquery = EditorialComments::find()
                ->where(['e_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select(['e_comment_id', 'parent_id', 'comments', 'editorial_comments.user_id', 'editorial_comments.created_at', '(CASE WHEN (select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id and editorial_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id) as total_like'])
                        ->where(['status' => 0])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])
                ->select(['editorial_comments.*', '(CASE WHEN (select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id and editorial_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from editorial_comment_activities where editorial_comment_activities.e_comment_id = editorial_comments.e_comment_id) as total_like']);
            $totalcountcomments = $commentsquery->count();
            $comments = $commentsquery->limit($page)
                ->offset($offset)->orderBy('e_comment_id Desc')->asArray()
                ->all();
        } else if ($_POST['post_type'] == 'happy_story') {
            $commentsquery = HappyStoriesComments::find()
                ->where(['hs_id' => $_POST['id'], 'parent_id' => 0, 'status' => 0])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select(['hs_comment_id', 'parent_id', 'comments', 'happy_stories_comments.user_id', 'happy_stories_comments.created_at', '(CASE WHEN (select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id and happy_stories_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id) as total_like'])
                        ->where(['status' => 0])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])
                ->select(['happy_stories_comments.*', '(CASE WHEN (select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id and happy_stories_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id) as total_like']);
            $totalcountcomments = $commentsquery->count();
            $comments = $commentsquery->limit($page)
                ->offset($offset)->orderBy('hs_comment_id Desc')->asArray()
                ->all();
        }

        return array('status' => 1, 'totalpages' => ceil($totalcountcomments / 10), 'data' => $comments);
    }

    public function actionChangepassword()
    {
        $user_id = $this->user_id;
        $usermodel = User::findOne($user_id);
        if ($usermodel === null) {
            return array('status' => 0, 'error' => $this->returnMessage('User Not Found!'));
        }
        $usermodel->attributes = Yii::$app->request->post();
        $usermodel->scenario = 'changepassword-api';
        if ($usermodel->validate()) {
            $usermodel->setPassword($usermodel->password);
            $usermodel->updated_at = date("Y-m-d H:i:s");
            if ($usermodel->save()) {
                return array('status' => 1, 'message' => 'You have changed password successfully');
            } else {
                return array('status' => 0, 'error' => $usermodel->getErrors());
            }
        } else {
            return array('status' => 0, 'error' => $usermodel->getErrors());
        }
    }

    public function actionUpdateprofile()
    {
        $user_id = $this->user_id;
        $usermodel = User::findOne($user_id);
        if ($usermodel === null) {
            return array('status' => 0, 'error' => $this->returnMessage('User Not Found!'));
        }
        $userprofilemodel = UserProfile::find()->where(['user_id' => $user_id])->one();
        if ($userprofilemodel === null) {
            return array('status' => 0, 'error' => $this->returnMessage('User Profile Not Found!'));
        }
        $userprofilemodel->attributes = Yii::$app->request->post();
        $userprofilemodel->scenario = 'updateprofile-api';
        if ($userprofilemodel->validate()) {
            if (isset($_FILES['image']) && is_uploaded_file($_FILES['image']['tmp_name'])) {
                $userprofilemodel->image = $uploads = UploadedFile::getInstanceByName('image');
                $unique = rand(10, 500) . time();
                $userprofilemodel->profile_image = $unique . '.jpg';
                $userprofilemodel->image->saveAs('web/uploads/users/' . $unique . '.jpg');
                $userprofilemodel->image = null;
            } else if (isset($_POST['default_image_name']) && $_POST['default_image_name'] != '') {
                $userprofilemodel->profile_image = $_POST['default_image_name'];
            }

            if ($userprofilemodel->save()) {
                $usermodel->updated_at = date("Y-m-d H:i:s");
                $usermodel->save();
                return array('status' => 1, 'message' => 'You have updated profile successfully');
            } else {
                return array('status' => 0, 'error' => $userprofilemodel->getErrors());
            }
        } else {
            return array('status' => 0, 'error' => $userprofilemodel->getErrors());
        }
    }

    public function actionChatList()
    {

        $user_id = $this->user_id;
        $messages = Utils::getInboxThreads($user_id);
        return array('status' => 1, 'data' => $messages);

    }

    public function actionGetMessages()
    {

        $user_id = $this->user_id;
        if (!isset($_POST['parent_id']) || empty($_POST['parent_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Id Field is Mandatory.'));
        }
        $parent_id = $_POST['parent_id'];
        $messages = Utils::getChatMessages($parent_id, $user_id);
        return array('status' => 1, 'data' => $messages);

    }

    public function actionReadMessage()
    {
        if (!isset($_POST['msg_id']) || empty($_POST['msg_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Msg Id Field is Mandatory.'));
        }
        $user_id = $this->user_id;
        $msg_id = Yii::$app->request->post()['msg_id'];
        if ($msg_id) {
            $message = Message::find()->where(['m_id' => $msg_id])
                ->andwhere(['Or', ['recipient_id' => $user_id], ['reply_sender_id' => $user_id], ['reply_recipient_id' => $user_id]])->one();

            if ($message) {
                $message->read_text = 1;
                $message->save();
                return array('status' => 1, 'message' => 'You have read message successfully');
            } else {
                return array('status' => 0, 'message' => 'Data not found.');
            }
        }
    }

    public function actionDeleteMessages()
    {
        if (!isset($_POST['msg_ids']) || empty($_POST['msg_ids'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Msg Ids Field is Mandatory.'));
        }
        $user_id = $this->user_id;
        $msg_ids = Yii::$app->request->post()['msg_ids'];
        if (!empty($msg_ids)) {
            foreach ($msg_ids as $msg_id) {
                $message = Message::find()->where(['m_id' => $msg_id])->andwhere(['OR', ['recipient_id' => $user_id], ['sender_id' => $user_id]])->one();

                if ($message) {


                    if (!empty($message->delete_status)) {
                        $message->delete_status = $message->delete_status . "," . $user_id . ",";
                    } else {
                        $message->delete_status = "," . $user_id . ",";
                    }


                    $message->save();
                } else {
                    return array('status' => 0, 'message' => 'Data not found.');
                }
            }
            return array('status' => 1, 'message' => 'You have deleted message successfully.');
        }
    }

    public function actionReportContent()
    {
        if (!isset($_POST['content_id']) || empty($_POST['content_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Content Id Field is Mandatory.'));
        }
        if (!isset($_POST['type']) || empty($_POST['type'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }

        $content_id = $_POST['content_id'];
        $content_type = $_POST['type'];

        $content_type_arr = array("article", "article_comment", "wish", "wish_comment", "message", "user");

        if ($content_type == "wish") {
            $wish = Wish::findOne($content_id);
            if (empty($wish)) {
                return array('status' => 0, 'message' => 'No wish data found.');
            }
            $reported_user = $wish->wished_by;
            $report_user = $this->user_id;
            $alreadyreported = ReportContent::find()->where(['report_user' => $report_user, 'reported_user' => $reported_user, 'report_type' => $content_type, 'content_id' => $content_id])->one();
            if (!empty($alreadyreported)) {
                return array('status' => 0, 'message' => 'You already reported this content.');
            }
            $activity = ReportWishes::find()->where(['w_id' => $wish->w_id])->one();
            if ($activity) {
                $activity->count = $activity->count + 1;
                $activity->save();

            } else {
                $activity = new ReportWishes();
                $activity->w_id = $wish->w_id;
                $activity->count = 1;
                $activity->save();
            }

            $activity = new ReportContent();
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = NULL;
            $activity->content_id = $content_id;
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            if ($activity->save()) {
                return array('status' => 1, 'message' => 'Content Reported Successfully.');
            } else {
                return array('status' => 0, 'message' => 'Something went wrong. Please try after sometimes.');
            }
            // left explicit else for clarity
        } else if ($content_type == "user" || $content_type == "wish_comment" || $content_type == "donation" || $content_type == "donation_comment" || $content_type == "message" || $content_type == 'forum' || $content_type == "happy_story") {
            if ($content_type == 'donation') {
                $donation = Donation::findOne($content_id);
                if (empty($donation)) {
                    return array('status' => 0, 'message' => 'No donation data found.');
                }
                $reported_user = $donation->created_by;
                $report_user = $this->user_id;

            } else if ($content_type == "user") {
                $user = User::findOne($content_id);
                if (empty($user)) {
                    return array('status' => 0, 'message' => 'No user data found.');
                }
                $reported_user = $content_id;
                $report_user = $this->user_id;
            } else if ($content_type == "donation_comment") {
                $donatiocomment = DonationComments::findOne($content_id);
                if (empty($donatiocomment)) {
                    return array('status' => 0, 'message' => 'No donation comment data found.');
                }
                $reported_user = $content_id;
                $report_user = $this->user_id;
            } else if ($content_type == "forum") {
                $forum = Editorial::findOne($content_id);
                if (empty($forum)) {
                    return array('status' => 0, 'message' => 'No forum data found.');
                }
                $reported_user = $forum->created_by;
                $report_user = $this->user_id;
                $content_type = 'editorial';
            } else if ($content_type == "happy_story") {
                $forum = HappyStories::findOne($content_id);
                if (empty($forum)) {
                    return array('status' => 0, 'message' => 'No happy story data found.');
                }
                $reported_user = $forum->user_id;
                $report_user = $this->user_id;
                $content_type = 'happy_stories';
            } else if ($content_type == "message") {
                $report_user = $this->user_id;
                $reported_user = $_POST['user_id'];
            }

            $alreadyreported = ReportContent::find()->where(['report_user' => $report_user, 'reported_user' => $reported_user, 'report_type' => $content_type, 'content_id' => $content_id])->one();
            if (!empty($alreadyreported)) {
                return array('status' => 0, 'message' => 'You already reported this content.');
            }
            $activity = new ReportContent();
            $activity->report_user = $report_user;
            $activity->reported_user = $reported_user;
            $activity->report_type = $content_type;
            $activity->comment = NULL;
            $activity->content_id = $content_id;
            if ($content_type == "message") {
                $activity->comment = $_POST['message'];
                $activity->sendbird_message = $_POST['message'];
            }
            $activity->date = date('Y-m-d H:i:s');
            $activity->blocked = "0";

            if ($activity->save()) {
                $contentType = 'Content';
                if ($content_type == "wish") {
                    $contentType = 'Wish';
                } else if($content_type == 'donation'){
                    $contentType = 'Donation';
                } else if($content_type == 'user'){
                    $contentType = 'User';
                } else if($content_type == 'donation_comment' || $content_type == 'wish_comment' || $content_type == 'article_comment'){
                    $contentType = 'Comment';
                } else if($content_type == 'forum'){
                    $contentType = 'Forum';
                } else if($content_type == 'happy_story'){
                    $contentType = 'Happy Story';
                } else if($content_type == 'message'){
                    $contentType = 'Message';
                }
                return array('status' => 1, 'message' => $contentType.' Reported Successfully.');
            } else {
                return array('status' => 0, 'message' => 'Something went wrong. Please try after sometimes.');
            }
        } else if ($content_type == "user") {

        }

    }

    public function actionRemove($wish_id)
    {
        if (\Yii::$app->user->isGuest)
            return $this->redirect(['site/login', 'red_url' => Yii::$app->request->referrer]);
        $wish = $this->findModel($wish_id);
        $activity = Activity::find()->where(['wish_id' => $wish->w_id, 'activity' => 'fav', 'user_id' => \Yii::$app->user->id])->one();
        if ($activity != null) {
            $activity->delete();
        }

        return $this->redirect(['account/my-saved']);
    }

    public function actionSendFriendRequest()
    {
        $model = new FriendRequest();
        $from = $this->user_id;
        if (!isset($_POST['send_to']) || empty($_POST['send_to'])) {
            return array('status' => 0, 'error' => $this->returnMessage('User Id Field is Mandatory.'));
        }
        $to = $_POST['send_to'];

        if ($from != '' && $to != '') {
            $userexist = User::findOne($to);
            if (empty($userexist)) {
                return array('status' => 0, 'message' => 'User data not found.');

            }
            $checkdata = FriendRequest::find()->where("(requested_by = " . $from . " AND requested_to = " . $to . ") OR (requested_by = " . $to . " AND requested_to = " . $from . ")")->andWhere(['in', 'status', [0, 1, 2]])->orderBy(['f_id' => SORT_DESC])->one();


            if (!empty($checkdata)) {
                return array('status' => 0, 'message' => 'You have already sent friend request.');
            } else {
                $request = new FriendRequest();
                $request->requested_by = $from;
                $request->requested_to = $to;
                if ($request->save(false)) {

                    $model->sendEmail($to, $request->f_id);

                    return array('status' => 1, 'message' => 'You have sent friend request successfully.');
                }
            }


        }
    }

    public function actionMyFriends()
    {
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        $search = (isset($_POST['search']) && $_POST['search'] != '') ? $_POST['search'] : '';
        $user_id = $this->user_id;

        $profileA = UserProfile::find()->where(['user_id' => 1])->one();

        $followlist = [];
        $myfollow = [];
        if ($search != '') {

            $users = UserProfile::find()->select(['*'])->where(['!=', 'user_id', $user_id])->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search])->asArray()->all();
            $foll = FriendRequest::find()->where(["requested_by" => $this->user_id])->orWhere(['requested_to' => $this->user_id])->andWhere(['in', 'status', [0, 1, 2]])->all();
            $followlist = [];
            if ($foll) {
                foreach ($foll as $tmp) {
                    if ($tmp->requested_to == $user_id) {
                        array_push($followlist, $tmp->requested_by);
                    } else if ($tmp->requested_by == $user_id) {
                        array_push($followlist, $tmp->requested_to);
                    }
                }
            }

            if (!empty($users)) {
                foreach ($users as $key => $user) {
                    $userid1 = $user['user_id'];
                    if (in_array($userid1, $followlist)) {

                        $friend = \app\models\FriendRequest::find()->where("(requested_by = " . $userid1 . " AND requested_to = " . $this->user_id . ") OR (requested_by = " . $this->user_id . " AND requested_to = " . $userid1 . ")")->andWhere(['in', 'status', [0, 1, 2]])->one();
                        if ($friend->status == 1) {
                            $users[$key]['friend_status'] = (string)1;
                            $users[$key]['requested_by'] = (string)$friend->requested_by;
                            $users[$key]['requested_to'] = (string)$friend->requested_to;
                        } else if ($friend->status == 0) {
                            $users[$key]['friend_status'] = (string)0;
                            $users[$key]['requested_by'] = (string)$friend->requested_by;
                            $users[$key]['requested_to'] = (string)$friend->requested_to;
                        }
                        $users[$key]['f_id'] = (string)$friend->f_id;
                    } else {
                        $users[$key]['friend_status'] = '';
                        $users[$key]['f_id'] = '';
                        $users[$key]['requested_by'] = '';
                        $users[$key]['requested_to'] = '';
                    }
                }
            }
            $myfriends = $users;

        } else {

            if ($type == 'requests') {
                $myfriends = FriendRequest::find()
                    ->With(['userModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])->With(['otherUserModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])->where(['!=', 'requested_by', $user_id])->andWhere(['requested_to' => $user_id])->andWhere(['!=', 'requested_to', 1])->andWhere(['status' => 0])->asArray()->all();
            } else if ($type == 'sent_requests') {
                $myfriends = FriendRequest::find()
                    ->With(['userModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])->With(['otherUserModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image');
                    }])->where(['requested_by' => $user_id])->andWhere(['!=', 'requested_to', $user_id])->andWhere(['!=', 'requested_to', 1])->andWhere(['status' => 0])->asArray()->all();
            } else {
                $data['profileA'] = $profileA;
                $myfriends = FriendRequest::find()->With(['userModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])->With(['otherUserModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                    ->where(["requested_by" => $user_id])->orWhere(['requested_to' => $user_id])->andWhere(['status' => 1])->asArray()->all();
            }
        }
        $data['friends'] = $myfriends;

        return array('status' => 1, 'data' => $data);

    }

    public function actionAcceptFriendRequest()
    {
        if (!isset($_POST['requestid']) || empty($_POST['requestid'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Request Id Field is Mandatory.'));
        }
        $requestid = $_POST['requestid'];
        if ($requestid) {
            $user_id = $this->user_id;
            $checkdata = FriendRequest::find()->where(["f_id" => $requestid])->one();

            if (!empty($checkdata) && (($checkdata->requested_by == $user_id) || ($checkdata->requested_to == $user_id))) {
                $checkdata->status = 1;
                $checkdata->updated_at = date('Y-m-d H:i:s');
                if ($checkdata->save(false)) {
                    return array('status' => 1, 'message' => 'You have accepted friend request successfully.');
                } else {
                    return array('status' => 0, 'message' => 'Something went wrong.Please try after sometimes.');
                }

            } else {
                return array('status' => 0, 'message' => 'Something went wrong.Please try after sometimes.');
            }

        }
    }

    public function actionRejectFriendRequest()
    {
        if (!isset($_POST['requestid']) || empty($_POST['requestid'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Request Id Field is Mandatory.'));
        }
        $requestid = $_POST['requestid'];
        if ($requestid) {
            $user_id = $this->user_id;
            $checkdata = FriendRequest::find()->where(["requested_to" => $user_id, "status" => 0, "f_id" => $requestid])->one();
            if ($checkdata) {
                $checkdata->status = 3;
                $checkdata->save(false);
                $inboxmessage = Message::find()->where(['friend_request_id' => $requestid])->one();
                if (!empty($inboxmessage)) {
                    $inboxmessage->delete();
                }
                return array('status' => 1, 'message' => 'You have rejected friend request successfully.');
            } else {
                return array('status' => 0, 'message' => 'Data not found.');
            }
        }
    }

    public function actionUnfriend()
    {

        if (!isset($_POST['requestid']) || empty($_POST['requestid'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Request Id Field is Mandatory.'));
        }
        $requestid = $_POST['requestid'];
        if ($requestid) {
            $user_id = $this->user_id;
            $checkdata = FriendRequest::find()->where(["f_id" => $requestid])->one();
            if (!empty($checkdata) && (($checkdata->requested_by == $user_id) || ($checkdata->requested_to == $user_id))) {
                $checkdata->delete();
                $inboxmessage = Message::find()->where(['friend_request_id' => $requestid])->one();
                if (!empty($inboxmessage)) {
                    $inboxmessage->delete();
                }
                return array('status' => 1, 'message' => 'You have removed friend successfully.');
            } else {
                return array('status' => 0, 'message' => 'Something went wrong.Please try after sometimes.');
            }

        }
    }

    public function actionHappyStories()
    {

        $offset = (isset($_POST['page']) && $_POST['page']) ? $_POST['page'] * 10 : 0;
        $page = $offset + 10;
        $happy_stories = HappyStories::find()
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['wish' => function (ActiveQuery $query) {
                return $query->select('w_id,wish_title');
            }])
            ->select(['happy_stories.*', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="fav" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like") as total_like'])
            ->where(['status' => 0])
            ->orderBy('hs_id Desc');
        $totalcount = $happy_stories->count();
        $data = $happy_stories->limit($page)
            ->offset($offset)->asArray()
            ->all();
        return array('status' => 1, 'totalpages' => ceil($totalcount / 10), 'data' => $data);

    }

    public function actionViewHappyStory()
    {

        if (!isset($_POST['happy_story_id']) || empty($_POST['happy_story_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Happy Story Id Field is Mandatory.'));
        }
        $happy_story_id = $_POST['happy_story_id'];

        $dataquery = HappyStories::find()->where(['hs_id' => $happy_story_id])
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['wish' => function (ActiveQuery $query) {
                return $query->select('w_id,wish_title');
            }])
            ->select(['happy_stories.*', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="fav" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like") as total_like'])
            ->orderBy('hs_id Desc');
        $model = $dataquery->asArray()
            ->one();
        if ($model !== null) {
            $comments = HappyStoriesComments::find()->where(['hs_id' => $_POST['happy_story_id'], 'parent_id' => 0, 'status' => 0])
                ->With(['likes' => function (ActiveQuery $query) {
                    return $query->select('a_id,hs_comment_id')->where(['user_id' => $this->user_id]);
                }])
                ->With(['commenterModel' => function (ActiveQuery $query) {
                    return $query->select('user_id,firstname,lastname,profile_image');
                }])
                ->With(['commentReply' => function (ActiveQuery $query) {
                    return $query->select('happy_stories_comments.hs_comment_id ,parent_id,comments,happy_stories_comments.user_id,created_at')->where(['status' => 0])
                        ->With(['likes' => function (ActiveQuery $query) {
                            return $query->select('happy_stories_comment_activities.a_id,happy_stories_comment_activities.hs_comment_id')->where(['user_id' => $this->user_id]);
                        }])
                        ->joinWith(['commenterModel' => function (ActiveQuery $query) {
                            return $query->select('user_profile.user_id,firstname,lastname,profile_image');
                        }]);
                }])
                ->select(['happy_stories_comments.*', '(CASE WHEN (select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id and happy_stories_comment_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(select count(*) from happy_stories_comment_activities where happy_stories_comment_activities.hs_comment_id = happy_stories_comments.hs_comment_id) as total_like'])
                ->asArray()
                ->orderBy('hs_comment_id Desc')
                ->all();
            $isReported = Utils::checkIsReported($_POST['happy_story_id'], 'happy_stories');
            return array('status' => 1, 'data' => $model, 'comment' => $comments, 'isReported' => $isReported);
        } else {
            return array('status' => 0, 'error' => $this->returnMessage('Happy story not found!'));
        }
        return array('status' => 1, 'data' => $data);

    }

    public function actionMyHappyStories()
    {
        $user_id = $this->user_id;

        $happy_stories = HappyStories::find()
            ->With(['author' => function (ActiveQuery $query) {
                return $query->select('user_id,firstname,lastname,profile_image');
            }])
            ->With(['wish' => function (ActiveQuery $query) {
                return $query->select('w_id,wish_title');
            }])
            ->select(['happy_stories.*', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as likeByUser', '(CASE WHEN (select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="fav" and story_activities.user_id = "' . $this->user_id . '") > 0 THEN 1 ELSE 0 END) as saved', '(select count(*) from story_activities where story_activities.story_id = happy_stories.hs_id and story_activities.activity="like") as total_like'])
            ->where(['user_id' => $user_id])
            ->orderBy('hs_id Desc');
        //$totalcount = $happy_stories->count();
        $data = $happy_stories->asArray()
            ->all();
        return array('status' => 1, 'data' => $data);

    }

    public function actionWishersAndDonors()
    {
        $user_id = $this->user_id;
        $query = Wish::find()->innerJoinWith(['wisherModel' => function (ActiveQuery $query) {
            return $query->select('user_id,firstname,lastname,profile_image');
        }])->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
        $data['wishers'] = $query->groupBy('wished_by')->limit(3)->asArray()->all();
        $query1 = Wish::find()->innerJoinWith(['granterModel' => function (ActiveQuery $query1) {
            return $query1->select('user_id,firstname,lastname,profile_image');
        }])->select(['wishes.granted_by,count(w_id) as total_wishes'])->where(['not', ['granted_by' => null]])->orderBy('total_wishes DESC');
        $data['granters'] = $query1->groupBy('granted_by')->limit(3)->asArray()->all();
        $query2 = Donation::find()->innerJoinWith(['donorModel' => function (ActiveQuery $query2) {
            return $query2->select('user_id,firstname,lastname,profile_image');
        }])->select(['donations.created_by,count(id) as total_donations'])->orderBy('total_donations DESC');
        $data['donors'] = $query2->groupBy('donations.created_by')->limit(3)->asArray()->all();;
        //print_r($data['granters']);exit;

        return array('status' => 1, 'data' => $data);

    }

    public function actionAllWishersAndDonors()
    {
        $user_id = $this->user_id;
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        if ($type == '') {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }
        $search = (isset($_POST['search']) && $_POST['search'] != '') ? $_POST['search'] : '';

        if ($type == 'wishers') {
            $query = Wish::find()->innerJoinWith(['wisherModel' => function (ActiveQuery $query) use ($search) {
                return $query->select('user_id,firstname,lastname,profile_image')->where(['like', 'user_profile.firstname', $search])->orWhere(['like', 'user_profile.firstname', $search]);
            }])->select(['wishes.wished_by,count(w_id) as total_wishes'])->orderBy('total_wishes DESC');
            $data = $query->groupBy('wished_by')->asArray()->all();
        } elseif ($type == 'granters') {
            $query1 = Wish::find()->innerJoinWith(['granterModel' => function (ActiveQuery $query1) use ($search) {
                return $query1->select('user_id,firstname,lastname,profile_image')->where(['like', 'user_profile.firstname', $search])->orWhere(['like', 'user_profile.firstname', $search]);;
            }])->select(['wishes.granted_by,count(w_id) as total_wishes'])->where(['not', ['granted_by' => null]])->orderBy('total_wishes DESC');

            $data = $query1->groupBy('granted_by')->asArray()->all();
        } else if ($type == 'donors') {
            $query2 = Donation::find()->innerJoinWith(['donorModel' => function (ActiveQuery $query2) use ($search) {
                return $query2->select('user_id,firstname,lastname,profile_image')->where(['like', 'user_profile.firstname', $search])->orWhere(['like', 'user_profile.firstname', $search]);;
            }])->select(['donations.created_by,count(id) as total_donations'])->orderBy('total_donations DESC');

            $data = $query2->groupBy('donations.created_by')->asArray()->all();
        }

        return array('status' => 1, 'data' => $data);

    }

    public function actionMyDrafts()
    {
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        if ($type == '') {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }

        if ($type == 'wishes') {

            $wishesquery = Wish::find()->where(['wish_status' => 1, 'wished_by' => $this->user_id])
                ->select(['wishes.*'])
                ->orderBy('w_id DESC');
            $data = $wishesquery->asArray()
                ->all();
        } else if ($type == 'donations') {
            $wishesquery = Donation::find()->where(['status' => 1, 'created_by' => $this->user_id])
                ->select(['donations.*'])
                ->orderBy('id DESC');
            $data = $wishesquery->asArray()->all();
        }
        return array('status' => 1, 'data' => $data);

    }

    public function actionDeleteDraft()
    {
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        if ($type == '') {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }
        $content_ids = (isset($_POST['content_ids']) && !empty($_POST['content_ids'])) ? explode(',', $_POST['content_ids']) : '';
        if ($type == 'wishes') {
            foreach ($content_ids as $content_id) {
                Wish::find()->where(['wish_status' => 1, 'wished_by' => $this->user_id])
                    ->where(['w_id' => $content_id])->one()->delete();
            }
        } else if ($type == 'donations') {
            foreach ($content_ids as $content_id) {
                Donation::find()->where(['status' => 1, 'created_by' => $this->user_id])
                    ->where(['id' => $content_id])->one()->delete();
            }

        }
        return array('status' => 1, 'message' => 'You have removed drafts successfully.');

    }

    public function actionPrivacyPolicy()
    {
        $data = Page::findOne(1);
        return array('status' => 1, 'data' => $data);

    }

    public function actionTerms()
    {
        $data = Page::findOne(2);
        return array('status' => 1, 'data' => $data);

    }

    public function actionGuides()
    {
        $data = Page::findOne(3);
        return array('status' => 1, 'data' => $data);
    }

    public function actionAbout()
    {
        $data = Page::findOne(4);
        return array('status' => 1, 'data' => $data);
    }

    public function actionContact()
    {

        if (!isset($_POST['name']) || $_POST['name'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Name Field is Mandatory.'));
        }
        if (!isset($_POST['email']) || $_POST['email'] == null) {
            return array('status' => 0, 'error' => $this->returnMessage('Email Field is Mandatory.'));
        }
        if (!isset($_POST['subject']) || empty($_POST['subject'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Subject Field is Mandatory.'));
        }
        if (!isset($_POST['body']) || empty($_POST['body'])) {
            return array('status' => 0, 'error' => $this->returnMessage('Body Field is Mandatory.'));
        }
        $model = new ContactForm();
        $model->name = $_POST['name'];
        $model->email = $_POST['email'];
        $model->subject = $_POST['subject'];
        $model->body = $_POST['body'];
        if (isset($_POST['phone_number']) && empty($_POST['phone_number'])) {
            $model->phone_number = $_POST['phone_number'];
        }
        if ($model->save(false)) {
            $model->contact();
            $model->admincontact();
            return array('status' => 1, 'message' => 'Data saved successfully.');
        } else {
            return array('status' => 0, 'message' => 'Something went wrong.Please try after sometimes.');
        }

    }

    public function actionUpdatePrivacy()
    {
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : '';
        if ($type == '') {
            return array('status' => 0, 'error' => $this->returnMessage('Type Field is Mandatory.'));
        }
        $userId = $this->user_id;
        $usermodel = User::findOne($userId);
        $usermodel->message_privacy = $_POST['type'];
        $usermodel->updated_at = date('Y-m-d H:i:s');
        $usermodel->save(false);
        return array('status' => 1, 'message' => 'You have updated privacy successfully.');

    }

    public function actionChatUsers()
    {
        $search = (isset($_POST['search']) && $_POST['search'] != '') ? $_POST['search'] : '';
        $type = (isset($_POST['type']) && $_POST['type'] != '') ? $_POST['type'] : 'all';
         $offset = (isset($_POST['page']) && $_POST['page'] != '') ? ($_POST['page'] * 20)  : 0;
        $page =  20;
        $user_id = $this->user_id;
        $totalcount = 0;
        $data = [];

        if ($search != '') {
            if ($type == 'Everyone') {
                $selectReported = (ReportContent::find()->select('reported_user')->where(
                    [
                        'report_user' => $user_id,
                        'report_type' => 'user',
                    ]
                ));
                $users = UserProfile::find()->select(['firstname', 'lastname', 'user_id as id', 'profile_image','user.sendbird_user_id'])
                    ->innerJoin('user','user.id = user_profile.user_id')->where(['!=', 'user_id', $user_id])
                    ->where(['not in', 'user_id', $selectReported])->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                $totalcount = $users->count();
                $data = $users->limit($page)
                    ->offset($offset)->asArray()
                    ->all();
            } else {
                $myfriends = FriendRequest::find()
                    ->With(['userModel' => function (ActiveQuery $query) use ($search) {
                        return $query->select('user_id,firstname,lastname,profile_image,user.sendbird_user_id')->innerJoin('user','user.id = user_profile.user_id')->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                    }])
                    ->With(['otherUserModel' => function (ActiveQuery $query) use ($search) {
                        return $query->select('user_id,firstname,lastname,profile_image,user.sendbird_user_id')->innerJoin('user','user.id = user_profile.user_id')->andWhere(['like', "CONCAT(firstname, ' ', lastname)", $search]);
                    }])->where(["requested_by" => $this->user_id])->orWhere(['requested_to' => $this->user_id])->andWhere(['status' => 1]);
                $totalcount = $myfriends->count();
                $friends = $myfriends->limit($page)
                    ->offset($offset)->asArray()
                    ->all();

                $followlist = [];
                if (!empty($friends)) {
                    foreach ($friends as $key => $value) {
                        if ($value['requested_to'] == $user_id && $value['userModel'] != null) {
                            array_push($followlist, $value['userModel']);
                        } else if ($value['requested_by'] == $user_id && $value['otherUserModel'] != null) {
                            array_push($followlist, $value['otherUserModel']);
                        }
                    }
                }
                $data = $followlist;
            }


        } else {
            if ($type == 'Everyone') {
                $selectReported = (ReportContent::find()->select('reported_user')->where(
                    [
                        'report_user' => $user_id,
                        'report_type' => 'user',
                    ]
                ));
                $users = Userprofile::find()->select(['firstname', 'lastname', 'user_id as id', 'profile_image','user.sendbird_user_id'])
                    ->innerJoin('user','user.id = user_profile.user_id')
                    ->where(['!=', 'user_id', $user_id])
                    ->where(['not in', 'user_id', $selectReported]);

                $totalcount = $users->count();

                $data = $users->limit($page)
                    ->offset($offset)->asArray()->all();

            } else if ($type == 'Connections') {

                $myfriends = FriendRequest::find()
                    ->With(['userModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image,user.sendbird_user_id')->innerJoin('user','user.id = user_profile.user_id');
                    }])->With(['otherUserModel' => function (ActiveQuery $query) {
                        return $query->select('user_id,firstname,lastname,profile_image,user.sendbird_user_id')->innerJoin('user','user.id = user_profile.user_id');
                    }])->where(["requested_by" => $user_id])->orWhere(['requested_to' => $user_id])->andWhere(['status' => 1]);

                $totalcount = $myfriends->count();
                $friends = $myfriends->limit($page)
                    ->offset($offset)->asArray()
                    ->all();


                $followlist = [];
                if (!empty($friends)) {
                    foreach ($friends as $key => $value) {
                        if ($value['requested_to'] == $user_id && $value['userModel'] != null) {
                            array_push($followlist, $value['userModel']);
                        } else if ($value['requested_by'] == $user_id && $value['otherUserModel'] != null) {
                            array_push($followlist, $value['otherUserModel']);
                        }
                    }
                }
                $data = $followlist;
            }

        }


        return array('status' => 1, 'totalpages' => ceil($totalcount / 20), 'data' => $data);

    }
    public function actionUserDetails()
    {
        if (!isset($_POST['sendbird_user_id']) || empty($_POST['sendbird_user_id'])) {
            return array('status' => 0, 'error' => $this->returnMessage('sendbird_user_id Field is Mandatory.'));
        }
        $sendbird_user_id = $_POST['sendbird_user_id'];
        $user = User::find()->where(['sendbird_user_id' => $sendbird_user_id])->one();
        if ($user) {
            $data['profile'] = UserProfile::find()->select(['user_profile.firstname','user_profile.lastname','user_profile.profile_image','user.id','user.sendbird_user_id'])
                ->innerJoin('user','user.id = user_profile.user_id')
                ->where(['user_id' => $user->id])
                ->asArray()->one();
                return array('status' => 1, 'data' => $data);
        } else {
            $data['profile'] = array();
            return array('status' => 1, 'data' => $data);
        }
    }
}