<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Wish;
use app\models\User;
use app\models\MailContent;
use yii\db\Query;

class CronController extends Controller {
    public function actionIndex() {	
        $connection = \Yii::$app->db;
        $model = $connection->createCommand('SELECT * FROM wishes 
                WHERE ((process_status = 1) 
                AND (email_status = 0)
                AND (str_to_date(process_granted_date, "%m-%d-%Y") < (CURRENT_DATE() - INTERVAL 1 MONTH)))');
        $quickemail = $model->queryAll();
	
        if($quickemail)
        {								
            foreach($quickemail as $tmp)
            {					
                $mailcontent = MailContent::find()->where(['m_id'=>11])->one();
                $editmessage = $mailcontent->mail_message;		
                $subject = $mailcontent->mail_subject;
                if(empty($subject))
                    $subject = 	'SimplyWishes '; 
				
				 
                $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'id' => $tmp['wished_by'],
                ]);
					
                if (!$user) {
                        return false;
                }

                $message = \Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'cronalertwishSuccess-html'],
                        ['user' => $user, 'editmessage' => $editmessage,
                            'wish_title' => $tmp['wish_title'], 'wish_id' => $tmp['w_id'] ]
                    )
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                    ->setTo( $user->email)
                    ->setSubject($subject);			

                $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n'); 

                $message->send(); 
				
                $model = Wish::findOne($tmp['w_id']);
                $model->email_status = 1;
                $model->save(false);  	
            } 
            
            echo 'Success';
        }
        else {
            echo 'No Users';
        }
    }
    
    public function actionWishExpired() {	
        $connection = \Yii::$app->db;
        $model = $connection->createCommand('SELECT * FROM wishes 
                WHERE ((process_status = 0)
                AND (granted_by IS NULL) 
                AND (wish_email_status = 0)
                AND (str_to_date(expected_date, "%m-%d-%Y") < CURRENT_DATE))');
        
        $quickemail = $model->queryAll();
	
        if($quickemail)
        {								
            foreach($quickemail as $tmp)
            {					
                $mailcontent = MailContent::find()->where(['m_id'=>18])->one();
                $editmessage = $mailcontent->mail_message;		
                $subject = $mailcontent->mail_subject;
                
                if(empty($subject))
                    $subject = 	'SimplyWishes '; 
				
                $user = User::findOne([
                    'status' => User::STATUS_ACTIVE,
                    'id' => $tmp['wished_by'],
                ]);
					
                if (!$user) {
                    return false;
                }

                $message = \Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'cronalertwishSuccess-html'],
                        ['user' => $user, 'editmessage' => $editmessage,
                            'wish_title' => $tmp['wish_title'], 'wish_id' => $tmp['w_id'] ]
                    )
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'SimplyWishes '])
                    ->setTo( $user->email)
                    ->setSubject($subject);			

                $message->getSwiftMessage()->getHeaders()->addTextHeader('MIME-version', '1.0\n');
                $message->getSwiftMessage()->getHeaders()->addTextHeader('charset', ' iso-8859-1\n'); 

                $message->send(); 
				
                $model = Wish::findOne($tmp['w_id']);
                $model->wish_email_status = 1;
                $model->save(false);  	
            } 
            
            echo 'Success';
        }
        else {
            echo 'No Users';
        }
    }
}



