<?php

namespace app\controllers;

use Yii;
use yii\web\UploadedFile;
use app\models\HappyStories;
use app\models\StoryActivity;
use app\models\ReportContent;
use app\models\User;
use app\models\UserProfile;
use app\models\search\SearchHappyStories;
use app\models\search\SearchReportContent;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use app\models\HappyStoriesComments;
use app\models\HappyStoriesCommentActivity;

class HappyStoriesController extends \yii\web\Controller
{
	 /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(), 
                'except' => ['index','story-details','scroll-happy','likes-view'],	
                'rules' => [
                    [
                        'actions' => ['create','update','my-story','delete','permission','view','update-new',
                            'scroll-my-happy','like','happy-stories-comments','update-comment','delete-comment',
                            'commentreply','comment-likes-view','like-comment','report-story','content-management'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
	
    public function actionIndex()
    {		
        /* $stories = HappyStories::find()->where(['status'=>0])->orderBy('hs_id Desc')->all();				
        return $this->render('index', ['stories' => $stories]); */
				
        $searchModel = new SearchHappyStories();
        $dataProvider = $searchModel->searchLive(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);	
    }

    
    public function actionReportStory(){

        $report = new ReportContent();
        $postData = Yii::$app->request->post();
        // print_r($postData);exit;
        $report->content_id = $postData['hs_id'];
        $report->report_user = \Yii::$app->user->id;
        $report->reported_user = $postData['reported_user'];
        $report->report_type="happy_stories";
        $report->date = date('Y-m-d H:i:s'); 

        if ($report->save()){
            Utils::sendEmail(1,'',21);
            return true;
        } 
        return false;
    }
        	
	 public function actionContentManagement()
     {		
     
         $searchModel = new SearchReportContent();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         return $this->render('content-management', [
             'searchModel' => $searchModel,
             'dataProvider' => $dataProvider,
         ]);
         
     }
	

    public function actionScrollHappy($page)
    {
        $searchModel = new SearchHappyStories();
        $dataProvider = $searchModel->searchLive(Yii::$app->request->queryParams);
	$dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $story){
            $str .= $story->happyAsCard;
        }
        return $str;
    }
	
	
	 public function actionMyStory()
    {			
		$user = User::findOne(\Yii::$app->user->id);
		$profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
	
		$searchModel = new SearchHappyStories();
        $dataProvider = $searchModel->searchMystories(Yii::$app->request->queryParams);
		
        return $this->render('my-story', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'user' => $user,
			'profile' => $profile
        ]);
		
    }
    
    public function actionLikesView($w_id)
    {
        $story = $this->findModel($w_id);
        $likedUser  =   [];
        if( ! empty($story->likes))
        {
            foreach($story->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();
                
                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }
        
        return json_encode($likedUser);
    }
	
	
    public function actionScrollMyHappy($page)
    {
        $searchModel = new SearchHappyStories();
        $dataProvider = $searchModel->searchMystories(Yii::$app->request->queryParams);
		$dataProvider->pagination->page = $page;
        $str = '';
        foreach($dataProvider->models as $story){
			$str .= $story->myHappyAsCard;
        }
        return $str;
    }
	
    public function actionCreate()
    {
        $model = new HappyStories();
        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
		  
        if ($model->load(Yii::$app->request->post())) {
            
            $model->user_id = \Yii::$app->user->id;
            $model->story_image = UploadedFile::getInstance($model, 'story_image');
            
            if(!empty($model->story_image)) {
                    if(!$model->uploadImage())
                            return;
            }else 
            {
                    $model->story_image = $model->dulpicate_image;
            }

            if($model->save())
            {
                Yii::$app->session->setFlash('success_adminhappystory');
                $model->sendSuccessEmail(\Yii::$app->user->id);
                return $this->redirect(['my-story']);
            }	
            else
            {					
                return $this->render('create', ['model' => $model,'user' => $user,'profile' => $profile]);
            }	
				
        } else {
            return $this->render('create', [
                'model' => $model,
                'user' => $user,
                'profile' => $profile
            ]);
        }
    }
	
    public function actionStoryDetails($id)
    {		
        $model = HappyStories::findOne($id);	
        
        if( ! $model)
            return $this->goHome();
        
        $listcomments = new HappyStoriesComments();
        $comments = $listcomments->find()->where(['hs_id'=>$id,'parent_id'=>0, 'status'=>0])->orderBy('hs_comment_id Desc')->all();		 
        
        return $this->render('story_full', [
            'model' =>$model,'comments'=>$comments,'listcomments'=>$listcomments
        ]); 
        
        return $this->render('story_full', ['model' => $model]);
    }
	
    public function actionUpdate($id)
    {
        $model = HappyStories::findOne($id);
        $current_image = $model->story_image;
        //$model->scenario = 'update_by_happystory_user';

        $user = User::findOne(\Yii::$app->user->id);
        $profile = UserProfile::find()->where(['user_id'=>\Yii::$app->user->id])->one();
		
        if ($model->load(Yii::$app->request->post())){
			
        /**		Image Uploaded for Update function Line 
        **/		
        $model->story_image = UploadedFile::getInstance($model, 'story_image');										
        if(!empty($model->story_image)){ 
                if(!$model->uploadImage())
                        return;
        }
         else
        {					
                if(!empty($model->dulpicate_image) && ($model->dulpicate_image != $current_image ))
                {
                        $model->story_image = $model->dulpicate_image;
                } else {
                        $model->story_image = $current_image;
                }					
        }

        if($model->save())
        {	Yii::$app->session->setFlash('success_happystory');
                //return $this->redirect(['story-details', 'id' => $model->hs_id]);
                return $this->redirect(['my-story']);
        } else {

                return $this->render('update', ['model' => $model,'user' => $user,
        'profile' => $profile,]);
        }
        } else {
            return $this->render('update', [
                'model' => $model,
				'user' => $user,
				'profile' => $profile,
            ]);
        }
    }
	
	/**
	 * Like a story
	 * User has to be logged in to like a wish
	 * Param: wish id
	 * @return boolean
	 */
	public function actionLike($s_id,$type)
	{
		if(\Yii::$app->user->isGuest)
			return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);
		$story = $this->findModel($s_id);
		$activity = StoryActivity::find()->where(['story_id'=>$story->hs_id,'activity'=>$type,'user_id'=>\Yii::$app->user->id])->one();
		if($activity != null){
			$activity->delete();
			return "removed";
		}
			$activity = new StoryActivity();
		$activity->story_id = $story->hs_id;
		$activity->activity = $type;
		$activity->user_id = \Yii::$app->user->id;
		if($activity->save())
			return "added";
		else return false;
	}
	
    /**
     * Finds the story model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Wish the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HappyStories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['happy-stories/my-story']);  
    }
	
	 public function actionPermission()
    {		
		/* $stories = HappyStories::find()->orderBy('hs_id Desc')->all();				
        return $this->render('index', ['stories' => $stories]); */
	
		$searchModel = new SearchHappyStories();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
        return $this->render('index_new', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
		
    }
	
    public function actionView($id)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	
	public function actionUpdateNew($id)
    {
        $model = HappyStories::findOne($id);
		$model->scenario = 'update_by_happystory_adminuser';
		
        if ($model->load(Yii::$app->request->post())){
		
				if($model->save())
				{	
					$model->sendAdminSuccessEmail( $model->user_id);
					return $this->redirect(['view', 'id' => $model->hs_id]);
				} else {
			
				return $this->render('update_new', ['model' => $model]);
			}
			
			
        } else {
            return $this->render('update_new', ['model' => $model]);
        }
    }
	
    public function actionHappyStoriesComments()
    {
        $model = new HappyStoriesComments();
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }
        }
    }
    
    public function actionUpdateComment($id)
    {
        $model = HappyStoriesComments::findOne($id);
        
        if($model->load(Yii::$app->request->post()))
        {
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }
            
            $model->user_id = \Yii::$app->user->id;
            
            if($model->save())
            {
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }
        }
    }
    
    public function actionDeleteComment($id)
    {
        $model  =   HappyStoriesComments::findOne($id);
        $model->status  =   1;
        
        if($model->parent_id == 0)
        {
            $replycomments = HappyStoriesComments::find()->where(['parent_id'=>$id])->all();
            
            foreach($replycomments as $reply)
            {
                $reply->status  =   1;
                $reply->save();
            }
        }
        
        $model->save();
        return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
    }
	
    public function actionCommentreply()
    {		
        $model = new HappyStoriesComments();
        
        if($model->load(Yii::$app->request->post()))
        {				
            if(\Yii::$app->user->isGuest){			
                Yii::$app->session->setFlash('login_to_comment');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }
            $model->user_id = \Yii::$app->user->id;
            if($model->save())
            {
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            }else{
                Yii::$app->session->setFlash('error_comments');
                return $this->redirect(['happy-stories/story-details?id='.$model->hs_id]);
            } 
        }
    }
    
    public function actionCommentLikesView($w_id)
    {

        $comment = HappyStoriesComments::find()->where(['hs_comment_id'=>$w_id])->one();
        $likedUser  =   [];
        if( ! empty($comment->likes))
        {
            foreach($comment->likes as $key => $like)
            {
                $profile = UserProfile::find()->where(['user_id'=>$like->user_id])->one();

                $likedUser[$key]['user_id'] =   $profile->user_id;
                $likedUser[$key]['name'] =   $profile->firstname.' '.$profile->lastname;
                $likedUser[$key]['image'] =   $profile->profile_image;
            }
        }

        return json_encode($likedUser);
    }

    public function actionLikeComment($s_id)
    {

        if(\Yii::$app->user->isGuest)
            return $this->redirect(['site/login','red_url'=>Yii::$app->request->referrer]);

        $comment = HappyStoriesComments::find()->where(['hs_comment_id'=>$s_id])->one();
        $activity = HappyStoriesCommentActivity::find()->where(['hs_comment_id'=>$comment->hs_comment_id,'user_id'=>\Yii::$app->user->id])->one();
        if($activity != null){
            $activity->delete();
            return "removed";
        }
        $activity = new HappyStoriesCommentActivity();
        $activity->hs_comment_id = $comment->hs_comment_id;
        $activity->user_id = \Yii::$app->user->id;

        if($activity->save())
            return "added";
        else return false;
    }
        
}
